module.exports = {
  data () {
    return {
      typeAheadPointer: -1
    }
  },

  watch: {
    filteredOptions () {
      this.typeAheadPointer = 0
    }
  },

  methods: {
    typeAheadUp () {
      if (this.typeAheadPointer > 0) {
        this.typeAheadPointer--
        if (this.maybeAdjustScroll) {
          this.maybeAdjustScroll()
        }
      }
    },
    typeAheadDown () {
      if (this.typeAheadPointer < this.filteredOptions.length - 1) {
        this.typeAheadPointer++
        if (this.maybeAdjustScroll) {
          this.maybeAdjustScroll()
        }
      }
    },
    typeAheadSelect () {
      if (this.filteredOptions[this.typeAheadPointer]) {
        this.select(this.filteredOptions[this.typeAheadPointer])
      } else if (this.taggable && this.search.length) {
        this.select(this.search)
      }

      if (this.clearSearchOnSelect) {
        this.search = ''
      }
    }
  }
}
