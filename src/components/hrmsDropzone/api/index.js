import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  postRemovedFile ({ file, fileDir }) {
    if (isMock) return generator(mocker.removedFile())
    return instance().post(endpoint + '/uploader/removedFile', { fileName: file.name, fileDir: fileDir })
  }
}
