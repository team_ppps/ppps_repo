import assetsLabels from 'assets/js/labels'

export const mocker = {
  removedFile () {
    return assetsLabels.mockPostMessage
  }
}
