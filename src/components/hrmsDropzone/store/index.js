import * as types from './mutation-types'
import hrmsDropzone from '../api'
import swal from 'sweetalert'

export default {
  actions: {
    [types.POST_HRMS_DROPZONE_REMOVED_FILE] ({ commit }, { file, fileDir }) {
      hrmsDropzone.postRemovedFile({ file, fileDir })
        .then((response) => {
          // const data = JSON.parse(response.request.response)
          // swal({title: '', text: data.response, html: true, type: 'success'})
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    }
  }
}

