import assetsLabels from 'assets/js/labels'

export function findLovType (boxType) {
  return (boxType.indexOf('codempid') >= 0) ? 'codempid'
         : (boxType.indexOf('codpos') >= 0) ? 'codpos'
         : (boxType.indexOf('codapp') >= 0) ? 'codapp'
         : (boxType.indexOf('codcalen') >= 0) ? 'codcalen'
         : (boxType.indexOf('codcours') >= 0) ? 'codcours'
         : (boxType.indexOf('nummemo') >= 0) ? 'nummemo'
         : (boxType.indexOf('numreq') >= 0) ? 'numreq'
         : (boxType.indexOf('codaplvl') >= 0) ? 'codaplvl'
         : (boxType.indexOf('grdscor') >= 0) ? 'grdscor'
         : (boxType.indexOf('codshift') >= 0) ? 'codshift'
         : (boxType.indexOf('codroom') >= 0) ? 'codroom'
         : (boxType.indexOf('codasset') >= 0) ? 'codasset'
         : (boxType.indexOf('codform') >= 0) ? 'codform'
         : (boxType.indexOf('codcompy') >= 0) ? 'codcompy'
         : (boxType.indexOf('numappl') >= 0) ? 'numappl'
         : (boxType.indexOf('coduser') >= 0) ? 'coduser'
         : (boxType.indexOf('codsecu') >= 0) ? 'codsecu'
         : (boxType.indexOf('codproc') >= 0) ? 'codproc'
         : (boxType.indexOf('codtable') >= 0) ? 'codtable'
         : (boxType.indexOf('codval') >= 0) ? 'codval'
         : (boxType.indexOf('codincom') >= 0) ? 'codincom'
         : (boxType.indexOf('codretro') >= 0) ? 'codretro'
         : (boxType.indexOf('codtency') >= 0) ? 'codtency'
         : (boxType.indexOf('codoccup') >= 0) ? 'codoccup'
         : (boxType.indexOf('codhosp') >= 0) ? 'codhosp'
         : (boxType.indexOf('numotreq') >= 0) ? 'numotreq'
         : (boxType.indexOf('codrem') >= 0) ? 'codrem'
         : (boxType.indexOf('typeleave') >= 0) ? 'typeleave'
         : (boxType.indexOf('codpay') >= 0) ? 'codpay'
         : (boxType.indexOf('codgrpwork') >= 0) ? 'codgrpwork'
         : (boxType.indexOf('typrewd') >= 0) ? 'typrewd'
         : (boxType.indexOf('codedlv') >= 0) ? 'codedlv'
         : (boxType.indexOf('codnatnl') >= 0) ? 'codnatnl'
         : (boxType.indexOf('codrelgn') >= 0) ? 'codrelgn'
         : (boxType.indexOf('coddist') >= 0) ? 'coddist'
         : (boxType.indexOf('compy') >= 0) ? 'compy'
         : (boxType.indexOf('codgl') >= 0) ? 'codgl'
         : (boxType.indexOf('codsecu') >= 0) ? 'codsecu'
         : (boxType.indexOf('codaward') >= 0) ? 'codaward'
         : (boxType.indexOf('codfunction') >= 0) ? 'codfunction'
         : (boxType.indexOf('typpayroll') >= 0) ? 'typpayroll'
         : (boxType.indexOf('codchng') >= 0) ? 'codchng'
         : (boxType.indexOf('numlereq') >= 0) ? 'numlereq'
         : ''
}

export function checkBoxType (type, boxType) {
  switch (type) {
    case 'DATE':
      return boxType === 'dtestrt' || boxType === 'dtereqst' || boxType === 'dteapprst' || boxType === 'dteworkst' ||
             boxType === 'dteend' || boxType === 'dtereqen' || boxType === 'dteappren' || boxType === 'dteworken' ||
             boxType === 'dtetarg' || boxType === 'dteappr' || boxType === 'dtereserv' || boxType === 'dteappoist' ||
             boxType === 'dteappoien' || boxType === 'dteeffest' || boxType === 'dteeffeen' || boxType === 'dteeffect' ||
             boxType === 'dtelockst' || boxType === 'dtelocken' || boxType === 'dtestrt1' || boxType === 'stdate' ||
             boxType === 'endate' || boxType === 'stdate1' || boxType === 'stdate2' || boxType === 'endate2' ||
             boxType === 'datest' || boxType === 'dateend' || boxType === 'stdate3' || boxType === 'stdate4' ||
             boxType === 'endate4' || boxType === 'stdate5'
    case 'TIME':
      return boxType === 'timstrt' || boxType === 'timend'
    case 'INPUTTEXT':
      return boxType === 'target' || boxType === 'mtrvalue' || boxType === 'numcard'
    case 'INPUTNUMBER':
      return boxType === 'numclseq' || boxType === 'numtime' || boxType === 'kpiitem' ||
             boxType === 'kpiwgt' || boxType === 'yearstrt' || boxType === 'yearend' || boxType === 'numperiod'
    case 'LOVCODCOMP':
      return boxType === 'codcomp' || boxType === 'codcomp2'
    case 'LOV':
      return boxType === 'codempid' || boxType === 'codpos' || boxType === 'codapp' ||
             boxType === 'codcalen' || boxType === 'codcours' || boxType === 'codcours2' || boxType === 'nummemo' ||
             boxType === 'numreq' || boxType === 'codaplvl' || boxType === 'grdscor' ||
             boxType === 'codempid2' || boxType === 'codempid3' || boxType === 'codshift' || boxType === 'codroom' ||
             boxType === 'codasset' || boxType === 'codform' || boxType === 'codcompy' || boxType === 'numappl' ||
             boxType === 'numapplst' || boxType === 'numapplen' || boxType === 'coduser' || boxType === 'codsecu' ||
             boxType === 'codproc' || boxType === 'codtable' || boxType === 'codval' || boxType === 'codincom' ||
             boxType === 'codretro' || boxType === 'codtency' || boxType === 'codoccup' || boxType === 'codhosp' ||
             boxType === 'codgrpwork' || boxType === 'numotreq' || boxType === 'codrem' || boxType === 'typeleave' ||
             boxType === 'codpay' || boxType === 'typrewd' || boxType === 'codedlv' || boxType === 'codnatnl' ||
             boxType === 'codrelgn' || boxType === 'coddist' || boxType === 'codgl' || boxType === 'codsecust' ||
             boxType === 'codsecuen' || boxType === 'codfunction' || boxType === 'typpayroll' || boxType === 'codaward' ||
             boxType === 'codchng' || boxType === 'numlereq'
    case 'DROPDOWN':
      return boxType === 'staappr' || boxType === 'staapprHres3cu' || boxType === 'mtrunit' || boxType === 'period' ||
             boxType === 'typeauth' || boxType === 'typeuser' || boxType === 'flgact' || boxType === 'typincom' ||
             boxType === 'typproba' || boxType === 'evastat' || boxType === 'typabs' || boxType === 'flgcanc'
    case 'DROPDOWNYEAR':
      return boxType === 'year'
    case 'DROPDOWNMONTH':
      return boxType === 'month' || boxType === 'month1' || boxType === 'month2'
    case 'LOGICALSTMT':
      return boxType === 'syncond'
    case 'HCMHRAL5OU':
      return boxType === 'hcmhral5ou'
    case 'HCMHRAL4KE':
      return boxType === 'hcmhral4ke'
    case 'HCMHRSC11X':
      return boxType === 'hcmhrsc11x'
    default:
      return false
  }
}

export function genDropdowns (boxType) {
  let dropdowns = {}
  switch (boxType) {
    case 'staappr':
      dropdowns = assetsLabels.statusApprove[this.nowLang]
      break
    case 'staapprHres3cu':
      dropdowns = assetsLabels.statusApproveHres3cu[this.nowLang]
      break
    case 'period':
      dropdowns = assetsLabels.period[this.nowLang]
      break
    case 'typeauth':
      dropdowns = assetsLabels.typeauth[this.nowLang]
      break
    case 'typeuser':
      dropdowns = assetsLabels.typeuser[this.nowLang]
      break
    case 'flgact':
      dropdowns = assetsLabels.flgact[this.nowLang]
      break
    case 'typincom':
      dropdowns = assetsLabels.typincom[this.nowLang]
      break
    case 'typproba':
      dropdowns = assetsLabels.typproba[this.nowLang]
      break
    case 'evastat':
      dropdowns = assetsLabels.evastat[this.nowLang]
      break
    case 'typabs':
      dropdowns = assetsLabels.typabs[this.nowLang]
      break
    case 'flgcanc':
      dropdowns = assetsLabels.flgcanc[this.nowLang]
      break
  }
  return dropdowns
}

export const searchBoxLabels = {
  en: {
    dtereqst: 'Date Request From',
    dtereqen: 'to',
    dteapprst: 'Date Approve From',
    dteappren: 'to',
    staappr: 'Approve Status',
    staapprHres3cu: 'Approve Status',
    dtestrt: 'Date Start From',
    dteend: 'to',
    yearstrt: 'Start year',
    yearend: 'End of year',
    year: 'For Year',
    month: 'For Month',
    month1: 'From Month',
    month2: 'To Month',
    codcomp: 'Department',
    codcomp2: 'Or Department',
    codpos: 'Position',
    codempid: 'Employee Code',
    codempid3: 'หรือ Employee Code',
    codcalen: 'Group Code',
    syncond: 'Search condition',
    codapp: 'Type of function',
    nummemo: 'For Memo. No.',
    codcours: 'or Course Code',
    codcours2: 'Course Code',
    numclseq: 'Class No.',
    numreq: 'Personnel req. no.',
    numtime: 'App. No.',
    kpiitem: 'Description of KPI',
    kpiwgt: 'Weight',
    target: 'Target Value',
    dtetarg: 'Target Date',
    mtrunit: 'Units',
    mtrvalue: 'Metric Value',
    codaplvl: 'Appraisal Group',
    period: 'Period',
    codempid2: 'Approve By',
    dteappr: 'Approve Date',
    codshift: 'Shift Code',
    dteworkst: 'From Date',
    dteworken: 'To Date',
    dtereserv: 'For Date',
    timstrt: 'Available Time From',
    timend: 'To',
    codroom: 'Room No.',
    codasset: 'Asset Type',
    codcompy: 'Codcompy',
    dteappoist: 'Interview Date',
    numappl: 'Application No.',
    numapplst: 'Application No.',
    dteappoien: 'To',
    numapplen: 'To',
    codform: 'Assessment Form',
    dteeffest: 'Date Effect From',
    dteeffeen: 'To',
    coduser: 'User id.',
    dteeffect: 'Date Effect',
    typeuser: 'User Type',
    typeauth: 'User rights',
    flgact: 'Status',
    codtable: 'Table Code',
    codval: 'Value Code',
    dtelockst: 'Suspension Period Date',
    dtelocken: 'To',
    codproc: 'For Module',
    codoccup: 'Occupation No',
    codhosp: 'Hospital',
    dtestrt1: 'Date Start OT From',
    codgrpwork: 'Group work',
    numotreq: 'O.T. Request No.',
    codrem: 'Reason of Request',
    stdate: 'From date OT',
    stdate3: 'From date',
    endate: 'To date',
    codsecust: 'Security Code From',
    codsecuen: 'Security Code To',
    typincom: 'Income type',
    codfunction: 'Function Code',
    stdate1: 'Use from date',
    stdate2: 'Update date',
    endate2: 'To date',
    typpayroll: 'Payroll Group',
    codaward: 'Type of Award',
    datest: 'Prob. due Date From',
    dateend: 'To',
    typproba: 'Type of Probation',
    evastat: 'Evaluation Status',
    numcard: 'or Timecard no.',
    typeleave: 'Type of Leave',
    typabs: 'Type of Data Required',
    numperiod: 'For Period',
    codpay: 'Income Code',
    stdate4: 'From Date',
    endate4: 'To date',
    stdate5: 'Interval Date Cancel',
    flgcanc: 'Status Calculate'
  },
  th: {
    dtereqst: 'วันที่ขอ ตั้งแต่',
    dtereqen: 'ถึง',
    dteapprst: 'วันที่อนุมัติ ตั้งแต่',
    dteappren: 'ถึง',
    staappr: 'สถานะการอนุมัติ',
    staapprHres3cu: 'สถานะ',
    dtestrt: 'วันที่เริ่มต้น',
    dteend: 'ถึงวันที่',
    yearstrt: 'เริ่มต้นปี',
    yearend: 'สิ้นสุดปี',
    year: 'สำหรับปี',
    month: 'สำหรับเดือน',
    month1: 'ตั้งแต่เดือน',
    month2: 'ถึงเดือน',
    codcomp: 'รหัสหน่วยงาน',
    codcomp2: 'หรือ รหัสหน่วยงาน',
    codpos: 'ตำแหน่ง',
    codempid: 'รหัสพนักงาน',
    codempid3: 'หรือ รหัสพนักงาน',
    codcalen: 'กลุ่มพนักงาน',
    syncond: 'Criteria ในการเลือกพนักงาน',
    codapp: 'ประเภทเรื่อง',
    nummemo: 'อ้างถึง Memo. No.',
    codcours: 'หรือ หลักสูตร',
    codcours2: 'หลักสูตร',
    numclseq: 'รุ่นที่',
    numreq: 'เลขที่ใบขออนุมัติ',
    numtime: 'ครั้งที่',
    kpiitem: 'รายละเอียด KPI',
    kpiwgt: 'น้ำหนัก',
    target: 'เป้าหมายที่กำหนด',
    dtetarg: 'วันที่กำหนดแล้วเสร็จ',
    mtrunit: 'หน่วย',
    mtrvalue: 'มูลค่า',
    codaplvl: 'กลุ่มประเมิน',
    period: 'งวด',
    codempid2: 'อนุมัติโดย',
    dteappr: 'วันที่อนุมัติ',
    codshift: 'รหัสกะ',
    dteworkst: 'ตั้งแต่วันที่',
    dteworken: 'ถึงวันที่',
    dtereserv: 'สำหรับวัน',
    timstrt: 'ช่วงเวลาที่ว่าง ตั้งแต่',
    timend: 'ถึง',
    codroom: 'ห้องประชุม',
    codasset: 'ประเภททรัพย์สิน',
    codcompy: 'รหัสบริษัท',
    dteappoist: 'วันที่สัมภาษณ์',
    numappl: 'เลขที่ใบสมัคร',
    numapplst: 'เลขที่ใบสมัคร',
    dteappoien: 'ถึงวันที่',
    numapplen: 'ถึง',
    codform: 'แบบฟอร์มการประเมิน',
    dteeffest: 'วันที่ทดลอง ตั้งแต่',
    dteeffeen: 'ถึง',
    coduser: 'รหัสผู้ใช้ระบบ',
    dteeffect: 'วันที่มีผลบังคับใช้',
    typeuser: 'ประเภทผู้ใช้งาน',
    typeauth: 'ประเภทสิทธิผู้ใช้งาน',
    flgact: 'สถานะ',
    codtable: 'ชื่อ Table',
    codval: 'ค่าข้อมูล',
    dtelockst: 'ช่วงวันระงับการใช้งานตั้งแต่วันที่',
    dtelocken: 'ถึงวันที่',
    codproc: 'สำหรับ Module',
    codoccup: 'อาชีพ',
    codhosp: 'โรงพยาบาล',
    dtestrt1: 'ช่วงวันที่ทำ OT ตั้งแต่',
    codgrpwork: 'กลุ่มทำงาน',
    numotreq: 'เลขที่ใบอนุมัติ',
    codrem: 'สาเหตุการขอ',
    stdate: 'ช่วงวันที่ทำ O.T.',
    stdate3: 'ตั้งแต่วันที่',
    endate: 'ถึงวันที่',
    codsecust: 'รหัสกลุ่ม Security ตั้งแต่ ',
    codsecuen: 'ถึง รหัสกลุ่ม Security',
    typincom: 'ประเภทเงินได้',
    codfunction: 'รหัสฟังก์ชั่น',
    stdate1: 'ช่วงวันที่ใช้งานตั้งแต่',
    stdate2: 'ช่วงวันที่แก้ไข',
    endate2: 'ถึงวันที่',
    typpayroll: 'กลุ่มการจ่ายเงินเดือน',
    codaward: 'ประเภทเบี้ยขยัน',
    datest: 'วันที่ครบกำหนดตั้งแต่',
    dateend: 'ถึง',
    typproba: 'ประเภทการทดลองงาน',
    evastat: 'สถานะของข้อมูล',
    numcard: 'หรือเลขที่บัตร',
    typeleave: 'ประเภทการลา',
    typabs: 'ประเภทความผิดปกติ',
    numperiod: 'สำหรับงวดที่',
    codpay: 'รหัสกลุ่มรายได้',
    stdate4: 'วันที่ลาเริ่มต้น',
    endate4: 'วันที่ลาสิ้นสุด',
    stdate5: 'ช่วงวันลาที่ขอยกเลิก',
    flgcanc: 'สถานะการคำนวณ'
  }
}

export function searchComponentGenerator (codapp) {
  let arraySearchs = []
  switch (codapp) {
    case 'demo-searchbox-box-type':
      arraySearchs = [
        'dtestrt',
        'dtereqst',
        'dteapprst',
        'dteend',
        'dtereqen',
        'dteappren',
        'codpos',
        'codempid',
        'staappr',
        'staapprHres3cu',
        'yearstrt',
        'yearend',
        'year',
        'month',
        'month1',
        'codcomp',
        'codapp',
        'nummemo',
        'codcours',
        'numclseq',
        'numreq',
        'numtime',
        'codaplvl',
        'period',
        'codempid2',
        'codcompy',
        'dteappoist',
        'dteappoien',
        'numappl',
        'dteeffest',
        'dteeffeen',
        'codtable',
        'codval',
        'codoccup',
        'codhosp',
        'codgrpwork',
        'stdate',
        'endate',
        'codfunction',
        'codaward',
        'typeleave'
      ]
      break
    case 'demo-searchbox-rules':
      arraySearchs = [
        'dtestrt@requiredWhenExist:dteend|date_format:DD/MM/YYYY',
        'dtereqst@requiredWhenExist:dtereqen|date_format:DD/MM/YYYY',
        'dteapprst@requiredWhenExist:dteappren|date_format:DD/MM/YYYY',
        'dteappoist@requiredWhenExist:dteappoien|date_format:DD/MM/YYYY',
        'dteend@requiredWhenExist:dtestrt|date_format:DD/MM/YYYY|after:dtestrt',
        'dtereqen@requiredWhenExist:dtereqst|date_format:DD/MM/YYYY|after:dtereqst',
        'dteappren@requiredWhenExist:dteapprst|date_format:DD/MM/YYYY|after:dteapprst',
        'dteappoien@requiredWhenExist:dteappoist|date_format:DD/MM/YYYY|after:dteappoist',
        'codpos@required',
        'codempid@required',
        'codoccup@required',
        'staappr@required',
        'staapprHres3cu@required',
        'year@required',
        'month@required',
        'codapp@required',
        'nummemo@required',
        'codcours@required',
        'numclseq@required',
        'numreq@required',
        'numtime@required',
        'codaplvl@required',
        'period@required',
        'codaward@required',
        'codgrpwork'
      ]
      break
    case 'demo-searchbox-condition':
      arraySearchs = [
        'dtestrt@@demo-searchbox-condition',
        'dtereqst@@demo-searchbox-condition',
        'dteapprst@@demo-searchbox-condition',
        'dteend@@demo-searchbox-condition',
        'dtereqen@@demo-searchbox-condition',
        'dteappren@@demo-searchbox-condition',
        'codpos@@demo-searchbox-condition',
        'codempid@@demo-searchbox-condition',
        'staappr@@demo-searchbox-condition',
        'staapprHres3cu@@demo-searchbox-condition',
        'year@@demo-searchbox-condition',
        'yearstrt@@demo-searchbox-condition',
        'yearend@@demo-searchbox-condition',
        'month@@demo-searchbox-condition',
        'codapp@@demo-searchbox-condition',
        'nummemo@@demo-searchbox-condition',
        'codcours@@demo-searchbox-condition',
        'numclseq@@demo-searchbox-condition',
        'numreq@@demo-searchbox-condition',
        'numtime@@demo-searchbox-condition',
        'codaplvl@@demo-searchbox-condition',
        'period@@demo-searchbox-condition',
        'numappl@@demo-searchbox-condition',
        'codtable@@demo-searchbox-condition',
        'codval@@demo-searchbox-condition',
        'codoccup@@demo-searchbox-condition',
        'codhosp@@demo-searchbox-condition',
        'codgrpwork@@demo-searchbox-condition',
        'codaward@@demo-searchbox-condition'
      ]
      break
    case 'demo-lov-codempid':
      arraySearchs = [
        'codempid'
      ]
      break
    case 'demo-lov-codpos':
      arraySearchs = [
        'codpos'
      ]
      break
    case 'demo-lov-codcomp':
      arraySearchs = [
        'codcomp'
      ]
      break
    case 'demo-lov-nummemo':
      arraySearchs = [
        'nummemo'
      ]
      break
    case 'demo-lov-codcours':
      arraySearchs = [
        'codcours'
      ]
      break
    case 'demo-lov-numreq':
      arraySearchs = [
        'numreq'
      ]
      break
    case 'demo-lov-codaplvl':
      arraySearchs = [
        'codaplvl'
      ]
      break
    case 'request':
      arraySearchs = [
        'dtereqst@requiredWhenExist:dtereqen|date_format:DD/MM/YYYY',
        'dtereqen@requiredWhenExist:dtereqst|date_format:DD/MM/YYYY|after:dtereqst'
      ]
      break
    case 'request-subrogate':
      arraySearchs = [
        'codcomp',
        'codempid',
        'dtereqst@requiredWhenExist:dtereqen|date_format:DD/MM/YYYY',
        'dtereqen@requiredWhenExist:dtereqst|date_format:DD/MM/YYYY|after:dtereqst'
      ]
      break
    case 'approve':
      arraySearchs = [
        'dteapprst@requiredWhenExist:dteappren|date_format:DD/MM/YYYY@approve',
        'dteappren@requiredWhenExist:dteapprst|date_format:DD/MM/YYYY|after:dteapprst@approve',
        'staappr@required@approve',
        'codempid'
      ]
      break
    case 'hrco06e':
      arraySearchs = [
        'codtable',
        'codval'
      ]
      break
    case 'hrsc01e':
      arraySearchs = [
        'codcomp',
        'coduser',
        'typeauth@required',
        'typeuser@required'
      ]
      break
    case 'hrsc06e':
      arraySearchs = [
        'codsecust',
        'codsecuen'
      ]
      break
    case 'hrsc11x':
      arraySearchs = [
        'coduser@required',
        'codempid',
        'codfunction',
        'hcmhrsc11x'
      ]
      break
    case 'hrco09x':
      arraySearchs = [
        'codempid@required',
        'stdate2@required|date_format:DD/MM/YYYY',
        'endate2@required|date_format:DD/MM/YYYY|after:stdate2'
      ]
      break
    case 'hrsc16e':
      arraySearchs = [
        'codcomp',
        'coduser',
        'codempid',
        'typeuser@required',
        'flgact@required'
      ]
      break
    case 'hrsc17e':
      arraySearchs = [
        'dtelockst@required|date_format:DD/MM/YYYY',
        'dtelocken@required|date_format:DD/MM/YYYY|after:dtelockst',
        'codapp',
        'codproc'
      ]
      break
    case 'hrpm1ee':
      arraySearchs = [
        'codasset@required'
      ]
      break
    case 'hrpm4ce':
      arraySearchs = [
        'codempid',
        'codcomp2',
        'codpos'
      ]
      break
    case 'hrpmb2e':
      arraySearchs = [
        'codempid@required',
        'yearstrt',
        'yearend'
      ]
      break
    case 'hrpmb5e':
      arraySearchs = [
        'codcompy@required',
        'dteeffect@required'
      ]
      break
    case 'hrpmb8e':
      arraySearchs = [
        'codcomp',
        'dteeffect'
      ]
      break
    case 'hrpm36x':
      arraySearchs = [
        'codcomp@required',
        'datest@required|date_format:DD/MM/YYYY',
        'dateend@required|date_format:DD/MM/YYYY|after:datest',
        'typproba',
        'evastat'
      ]
      break
    case 'hrpm33r':
      arraySearchs = [
        'codcomp@required',
        'codempid3',
        'month',
        'year'
      ]
      break
    case 'hral1ke':
      arraySearchs = [
        'codcomp@required'
      ]
      break
    case 'hral2hx':
      arraySearchs = [
        'codcomp@required',
        'typabs',
        'stdate3@required|date_format:DD/MM/YYYY',
        'endate@required|date_format:DD/MM/YYYY|after:stdate3'
      ]
      break
    case 'hral3cu':
      arraySearchs = [
        'codempid@required',
        'stdate3@required|date_format:DD/MM/YYYY',
        'endate@required|date_format:DD/MM/YYYY|after:stdate3'
      ]
      break
    case 'hral3ax':
      arraySearchs = [
        'codcomp@required',
        'year',
        'month1',
        'month2'
      ]
      break
    case 'hral4ex':
      arraySearchs = [
        'stdate3@required|date_format:DD/MM/YYYY',
        'endate@required|date_format:DD/MM/YYYY|after:stdate3',
        'codcomp'
      ]
      break
    case 'hral4dx':
      arraySearchs = [
        'codcomp@required',
        'year',
        'month'
      ]
      break
    case 'hral4hx':
      arraySearchs = [
        'stdate3@required|date_format:DD/MM/YYYY',
        'endate@required|date_format:DD/MM/YYYY|after:stdate3',
        'codcomp'
      ]
      break
    case 'hral5ou':
      arraySearchs = [
        'codcomp',
        'codempid3',
        'hcmhral5ou'
      ]
      break
    case 'hral5dx':
      arraySearchs = [
        'codempid@required',
        'codcomp2',
        'dtestrt@required|date_format:DD/MM/YYYY',
        'dteend@required|date_format:DD/MM/YYYY|after:dtestrt'
      ]
      break
    case 'hral5rx':
      arraySearchs = [
        'codcomp@required',
        'codempid3',
        'flgcanc',
        'stdate5@required|date_format:DD/MM/YYYY',
        'endate@required|date_format:DD/MM/YYYY|after:stdate5'
      ]
      break
    case 'hral5px':
      arraySearchs = [
        'codempid@required',
        'codcomp2',
        'dtestrt@required|date_format:DD/MM/YYYY',
        'dteend@required|date_format:DD/MM/YYYY|after:dtestrt'
      ]
      break
    case 'hral22e':
      arraySearchs = [
        'codempid',
        'dtestrt@requiredWhenExist:dtereqst|date_format:DD/MM/YYYY',
        'dteend@requiredWhenExist:dtereqst|date_format:DD/MM/YYYY|after:dtestrt'
      ]
      break
    case 'hral48x':
      arraySearchs = [
        'codcomp@required',
        'codempid3',
        'dtestrt@required|date_format:DD/MM/YYYY',
        'dteend@required|date_format:DD/MM/YYYY|after:dtestrt'
      ]
      break
    case 'hral55x':
      arraySearchs = [
        'codempid@required',
        'codcomp2',
        'typeleave',
        'stdate3@required|date_format:DD/MM/YYYY',
        'endate@required|date_format:DD/MM/YYYY|after:stdate3'
      ]
      break
    case 'hral42u':
      arraySearchs = [
        'codempid@required',
        'stdate3@required|date_format:DD/MM/YYYY',
        'endate@required|date_format:DD/MM/YYYY|after:stdate3'
      ]
      break
    case 'hral52u':
      arraySearchs = [
        'codempid@required',
        'stdate3@required|date_format:DD/MM/YYYY',
        'endate@required|date_format:DD/MM/YYYY|after:stdate3'
      ]
      break
    case 'hral57x':
      arraySearchs = [
        'codempid@required',
        'codcomp2',
        'year'
      ]
      break
    case 'hral68x':
      arraySearchs = [
        'codcomp@required',
        'codaward@required',
        'month1',
        'year',
        'month2',
        'year'
      ]
      break
    case 'hral74d':
      arraySearchs = [
        'codcomp@required',
        'codempid3',
        'typpayroll',
        'numperiod',
        'month',
        'year'
      ]
      break
    case 'hral75x':
      arraySearchs = [
        'codcomp@required',
        'codempid3',
        'typpayroll',
        'numperiod',
        'month',
        'year'
      ]
      break
    case 'hral96e':
      arraySearchs = [
        'codcomp@required',
        'codaward@required'
      ]
      break
    case 'hral4ke':
      arraySearchs = [
        'codcompy',
        'codgrpwork',
        'codempid',
        'stdate@required|date_format:DD/MM/YYYY',
        'endate@required|date_format:DD/MM/YYYY|after:stdate',
        'hcmhral4ke'
      ]
      break
    case 'hral41e':
      arraySearchs = [
        'dtestrt1@required|date_format:DD/MM/YYYY',
        'dteend@required|date_format:DD/MM/YYYY|after:dtestrt1',
        'codcomp',
        'codcalen',
        'codempid3'
      ]
      break
    case 'hral9de':
      arraySearchs = [
        'codcompy@required'
      ]
      break
    case 'hrpy29e':
      arraySearchs = [
        'codcompy@required'
      ]
      break
    case 'hral13e':
      arraySearchs = [
        'codcompy@required'
      ]
      break
    case 'hral27x':
      arraySearchs = [
        'codcompy@required',
        'codshift',
        'codcalen',
        'dtestrt@required|date_format:DD/MM/YYYY',
        'dteend@required|date_format:DD/MM/YYYY|after:dtestrt'
      ]
      break
    case 'hral44x':
      arraySearchs = [
        'codcomp',
        'codcalen',
        'codempid3',
        'dtestrt@required|date_format:DD/MM/YYYY',
        'dteend@required|date_format:DD/MM/YYYY|after:dtestrt'
      ]
      break
    case 'hrsc04e':
      arraySearchs = [
        'codcomp',
        'codpos',
        'codempid3',
        'dtestrt@required|date_format:DD/MM/YYYY',
        'dteend@required|date_format:DD/MM/YYYY|after:dtestrt'
      ]
      break
    case 'hrpy11e':
      arraySearchs = [
        'codcompy',
        'year',
        'typincom'
      ]
      break
    case 'hrsc10x':
      arraySearchs = [
        'dtestrt@required|date_format:DD/MM/YYYY',
        'dteend@required|date_format:DD/MM/YYYY|after:dtestrt'
      ]
      break
    case 'hrsc05x':
      arraySearchs = [
        'coduser',
        'stdate1@required|date_format:DD/MM/YYYY',
        'dteend@required|date_format:DD/MM/YYYY|after:stdate1'
      ]
      break
    case 'hral3fe':
      arraySearchs = [
        'codempid@requiredWhenNotExist:numcard',
        'numcard@requiredWhenNotExist:codempid'
      ]
      break
    case 'hral51e':
      arraySearchs = [
        'codempid@requiredWhenNotExist:codcomp2',
        'codcomp2@requiredWhenNotExist:codempid',
        'stdate4@required|date_format:DD/MM/YYYY',
        'endate4@required|date_format:DD/MM/YYYY|after:stdate4'
      ]
      break
    case 'hral52e':
      arraySearchs = [
        'stdate4@required|date_format:DD/MM/YYYY',
        'endate4@required|date_format:DD/MM/YYYY|after:stdate4',
        'codcomp'
      ]
      break
  }
  return generateSearchComponentsByArraySearch(arraySearchs)
}

function generateSearchComponentsByArraySearch (arraySearchs) {
  let searchComponents = []
  for (var search of arraySearchs) {
    var searchArr = search.split('@')
    searchComponents.push({
      boxType: searchArr[0],
      rules: (typeof searchArr[1] !== 'undefined') ? searchArr[1] : '',
      condition: (typeof searchArr[2] !== 'undefined') ? searchArr[2] : ''
    })
  }
  return searchComponents
}
