import * as types from './mutation-types'
import hrmsCalendarWorkTime from '../api'
import swal from 'sweetalert'
import moment from 'moment'
import _ from 'lodash'

function sumDayInMonth (year, month) {
  return moment(moment().format(year + '-' + _.padStart(month, 2, '0') + '-01')).daysInMonth()
}

export default {
  state: {
    typwork: [[]]
  },
  getters: {
    [types.GET_HRMS_CALENDAR_WORKTIME_TYPWORK] (state) {
      return state.typwork
    }
  },
  mutations: {
    [types.SET_HRMS_CALENDAR_WORKTIME_TYPWORK] (state, { data, year }) {
      let typworkYear = []
      let typworkMonth = []
      let rowIndex = 0
      for (let month = 1; month <= 12; month++) {
        typworkMonth = []
        for (let day = 1; day <= sumDayInMonth(year, month); day++) {
          if (data.rows[rowIndex]) {
            typworkMonth.push(data.rows[rowIndex].typwork.toLowerCase())
          } else {
            typworkMonth.push('w')
          }
          rowIndex++
        }
        typworkYear.push(typworkMonth)
      }
      state.typwork = typworkYear
    }
  },
  actions: {
    [types.RECEIVED_HRMS_CALENDAR_WORKTIME_TYPWORK] ({ commit }, { codempid, year }) {
      hrmsCalendarWorkTime.getTypwork({ codempid, year })
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRMS_CALENDAR_WORKTIME_TYPWORK, { data, year })
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    }
  }
}

