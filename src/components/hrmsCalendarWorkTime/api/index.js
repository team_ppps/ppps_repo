import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getTypwork ({ codempid, year }) {
    if (isMock) return generator(mocker.typwork({ codempid, year }))
    return instance().get(endpoint + '/hrmsCalendarWorkTime/getTypwork/' + codempid + '/' + year)
  }
}
