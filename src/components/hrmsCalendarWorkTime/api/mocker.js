import moment from 'moment'
import _ from 'lodash'

function sumDayInMonth (year, month) {
  return moment(moment().format(year + '-' + _.padStart(month, 2, '0') + '-01')).daysInMonth()
}

export const mocker = {
  typwork ({ codempid, year }) {
    let mockRows = []
    let typworkArr = ['w', 'h', 't', 's', 'l']
    for (let month = 1; month <= 12; month++) {
      for (let day = 1; day <= sumDayInMonth(year, month); day++) {
        mockRows.push({
          typwork: typworkArr[day % 5],
          day: day,
          month: month,
          year: year
        })
      }
    }
    return {
      rows: mockRows
    }
  }
}
