import * as types from './mutation-types'
import hrmsLovCodcomp from '../api'
import swal from 'sweetalert'
import hrmsLovCodcompLabels from '../assets/labels'
import { hrmsLovCodcompColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'

export default {
  state: {
    popupLovCodcomp: {
      total: 0,
      rows: [],
      lang: window.localStorage.getItem('lang')
    },
    lovCodcompCurrentDesc: function (code) {
      let rowIndex = this.$store.getters[types.GET_LOV_CODCOMP].rows.map(function (lov) {
        return lov.codcomp
      }).indexOf(code)
      if (rowIndex >= 0) {
        if (typeof this.$store.getters[types.GET_LOV_CODCOMP].rows[rowIndex] !== 'undefined') {
          return this.$store.getters[types.GET_LOV_CODCOMP].rows[rowIndex]['desc_codcomp']
        }
      }
      return ''
    },
    popupLovCodcompColumns: hrmsLovCodcompColumns(),
    popupLovCodcompLabels: hrmsLovCodcompLabels
  },
  getters: {
    [types.GET_LOV_CODCOMP_COLUMNS] (state) {
      return state.popupLovCodcompColumns
    },
    [types.GET_LOV_CODCOMP_LABELS] (state) {
      return state.popupLovCodcompLabels
    },
    [types.GET_LOV_CODCOMP] (state) {
      return state.popupLovCodcomp
    },
    [types.GET_LOV_CODCOMP_CURRENT_DESC] (state) {
      return state.lovCodcompCurrentDesc
    }
  },
  mutations: {
    [types.SET_LOV_CODCOMP_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.popupLovCodcompColumns[keyLang].popup = assetsLabels.replaceLabelToColumns(state.popupLovCodcompColumns[keyLang].popup, labels[keyLang])
      }
    },
    [types.SET_LOV_CODCOMP_LABELS] (state, labels) {
      state.popupLovCodcompLabels = labels
    },
    [types.SET_LOV_CODCOMP] (state, popupLovCodcomp) {
      let nowLang = window.localStorage.getItem('lang')
      popupLovCodcomp.lang = nowLang
      for (var i = 0; i < popupLovCodcomp.rows.length; i++) {
        let lovCodcomp = popupLovCodcomp.rows[i]
        popupLovCodcomp.rows[i].v_codcomp = lovCodcomp.codcomp.substring(0, lovCodcomp.comlevel * 3)
        switch (nowLang) {
          case 'en':
            popupLovCodcomp.rows[i].desc_codcomp = lovCodcomp.namcenteng
            break
          case 'th':
            popupLovCodcomp.rows[i].desc_codcomp = lovCodcomp.namcenttha
            break
          case '103':
            popupLovCodcomp.rows[i].desc_codcomp = lovCodcomp.namcent3
            break
          case '104':
            popupLovCodcomp.rows[i].desc_codcomp = lovCodcomp.namcent4
            break
          case '105':
            popupLovCodcomp.rows[i].desc_codcomp = lovCodcomp.namcent5
            break
        }
      }
      state.popupLovCodcomp = popupLovCodcomp
    }
  },
  actions: {
    [types.RECEIVED_LOV_CODCOMP_COLUMNS_LABELS] ({ state, commit }) {
      if (state.popupLovCodcomp.total <= 0) {
        hrmsLovCodcomp.getLovCodcompLabels()
          .then((response) => {
            const data = JSON.parse(response.request.response)
            commit(types.SET_LOV_CODCOMP_COLUMNS, data.labels)
            commit(types.SET_LOV_CODCOMP_LABELS, data.labels)
          })
          .catch((error) => {
            const data = error.response.data
            swal({title: '', text: data.response, html: true, type: 'error'})
          })
      }
    },
    [types.RECEIVED_LOV_CODCOMP] ({ state, commit }) {
      if (state.popupLovCodcomp.total <= 0) {
        hrmsLovCodcomp.getLovCodcomp()
          .then((response) => {
            const data = JSON.parse(response.request.response)
            commit(types.SET_LOV_CODCOMP, data)
          })
          .catch((error) => {
            const data = error.response.data
            swal({title: '', text: data.response, html: true, type: 'error'})
          })
      } else if (state.popupLovCodcomp.lang !== window.localStorage.getItem('lang')) {
        commit(types.SET_LOV_CODCOMP, state.popupLovCodcomp)
      }
    }
  }
}
