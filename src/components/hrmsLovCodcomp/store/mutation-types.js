export const GET_LOV_CODCOMP = 'GET_LOV_CODCOMP'
export const SET_LOV_CODCOMP = 'SET_LOV_CODCOMP'
export const GET_LOV_CODCOMP_COLUMNS = 'GET_LOV_CODCOMP_COLUMNS'
export const SET_LOV_CODCOMP_COLUMNS = 'SET_LOV_CODCOMP_COLUMNS'
export const GET_LOV_CODCOMP_LABELS = 'GET_LOV_CODCOMP_LABELS'
export const SET_LOV_CODCOMP_LABELS = 'SET_LOV_CODCOMP_LABELS'
export const RECEIVED_LOV_CODCOMP_COLUMNS_LABELS = 'RECEIVED_LOV_CODCOMP_COLUMNS_LABELS'
export const RECEIVED_LOV_CODCOMP = 'RECEIVED_LOV_CODCOMP'

export const GET_LOV_CODCOMP_CURRENT_DESC = 'GET_LOV_CODCOMP_CURRENT_DESC'
