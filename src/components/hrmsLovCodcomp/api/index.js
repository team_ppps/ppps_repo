import { instance, isMock, endpoint, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLovCodcompLabels () {
    if (isMock) return generator(mocker.lovCodcompLabels())
    return instance().get(endpoint + '/hrmsLovCodcomp/getLabels')
  },
  getLovCodcomp () {
    if (isMock) return generator(mocker.lovCodcomp())
    return instance().get(endpoint + '/hrmsLovCodcomp/getCodcomp')
  }
}
