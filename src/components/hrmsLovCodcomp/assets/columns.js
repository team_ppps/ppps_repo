export let hrmsLovCodcompColumns = () => {
  let popupColumns = [
    {
      key: 'v_codcomp',
      name: '',
      labelCodapp: 'LOV_COMP',
      labelIndex: '2',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codcomp',
      name: '',
      labelCodapp: 'LOV_COMP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]

  return {
    en: {
      popup: popupColumns
    },
    th: {
      popup: popupColumns
    }
  }
}
