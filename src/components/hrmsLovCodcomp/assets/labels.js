module.exports = {
  'en': {
    'LOV_COMP': {
      '0': 'List Of Company Name',
      '1': 'List Of Company Name',
      '2': 'Code of Company',
      '3': 'Name of Company'
    },
    'LOV_EMP': {
      '0': 'List of All Employee',
      '1': 'List of All Employee',
      '2': 'Employee Code',
      '3': 'Employee Name',
      '4': 'Status of Emp.',
      '5': 'Security'
    },
    'LOV_POS': {
      '0': 'List of Position Code',
      '1': 'List of Position Code',
      '2': 'Position Code',
      '3': 'Description'
    },
    'LOV_CODAPP': {
      '0': 'List of Appl. Name',
      '1': 'List of Appl. Name',
      '2': 'Code Appl. Name',
      '3': 'Description'
    },
    'LOV_MEMO': {
      '0': 'List Of Memo No.',
      '1': 'Memo No.',
      '2': 'Year',
      '3': 'Month',
      '4': 'Class',
      '5': 'Course',
      '6': 'Description'
    },
    'LOV_CORS': {
      '1': 'List of Course Training Code',
      '2': 'Course Code',
      '3': 'Course Name'
    },
    'LOV_FORM': {
      '0': 'List of Appraisal Form',
      '1': 'List of Appraisal Form',
      '2': 'App. Form No.',
      '3': 'Description'
    },
    'LOV_APP3': {
      '0': 'List of All Application No.',
      '1': 'List of All Application No.',
      '2': 'Applicant No.',
      '3': 'Appl Name.',
      '4': 'Status'
    },
    'LOV_APLV': {
      '0': 'Type of Appraisal group',
      '1': 'Type of Appraisal group',
      '2': 'Type of Appraisal group',
      '3': 'Description'
    },
    'LOV_REQ3': {
      '0': 'List of Personnal Request',
      '1': 'List of Personnal Request',
      '2': 'Request No',
      '3': 'Department Req.',
      '4': 'Position Req.',
      '5': 'Description'
    },
    'LOV_CODAPPR': {
      '0': 'List of All Employee',
      '1': 'List of All Employee',
      '2': 'Employee Code',
      '3': 'Employee Name',
      '4': 'Status of Emp.',
      '5': 'Security'
    },
    'LOV_SHIF': {
      '0': 'List of Shift Code',
      '1': 'List of Shift Code',
      '2': 'Shift Code',
      '3': 'Description',
      '4': 'Schedule Time'
    },
    'LOV_ROOM': {
      '0': 'List of Room Number',
      '1': 'List of Room Number',
      '2': 'Room Number',
      '3': 'Room Name'
    },
    'LOV_ASSET': {
      '0': 'List of Company Asset Code',
      '1': 'List of Company Asset Code',
      '2': 'Company Asset Code',
      '3': 'Description'
    },
    'LOV_WORK': {
      '0': 'List of Work Group Code',
      '1': 'List of Work Group Code',
      '2': 'Work Group Code',
      '3': 'Description'
    },
    'HRAP31EC6': {
      '250': '',
      '260': '',
      '270': ''
    },
    'LOV_CODUSER': {
      '0': 'List of User Name',
      '1': 'List of User Name',
      '2': 'Code of User',
      '3': 'Name of User'
    },
    'LOV_CODSECU': {
      '0': 'List of Security Group',
      '1': 'List of Security Group',
      '2': 'Security Group Code',
      '3': 'Security Group Name'
    },
    'LOV_CODPROC': {
      '0': 'List of Work Process',
      '1': 'List of Work Process',
      '2': 'Code of Process',
      '3': 'Description'
    },
    'LOV_CODINCOM': {
      '0': 'List of Fixed Income Code',
      '1': 'List of Fixed Income Code',
      '2': 'Income Code',
      '3': 'Description'
    },
    'LOV_CODRETRO': {
      '0': 'List Of Other Income Code',
      '1': 'List Of Other Income Code',
      '2': 'Income Code',
      '3': 'Description'
    },
    'LOV_CODTABLE': {
      '0': 'List Of Table Code',
      '1': 'List Of Table Code',
      '2': 'Table Code',
      '3': 'Description'
    },
    'LOV_CODVAL': {
      '0': 'List Of Value Code',
      '1': 'List Of Value Code',
      '2': 'Value Code',
      '3': 'Description'
    }
  },
  'th': {
    'LOV_COMP': {
      '0': 'รายชื่อบริษัท',
      '1': 'รายชื่อบริษัท',
      '2': 'รหัสบริษัท',
      '3': 'ชือบริษัท'
    },
    'LOV_EMP': {
      '0': 'รายชื่อพนักงานทั้งหมด',
      '1': 'รายชื่อพนักงานทั้งหมด',
      '2': 'รหัสพนักงาน',
      '3': 'ชื่อ - นามสกุล',
      '4': 'สถานะพนักงาน',
      '5': 'Security'
    },
    'LOV_POS': {
      '0': 'รายชื่อตำแหน่งงาน',
      '1': 'รายชื่อตำแหน่งงาน',
      '2': 'รหัสตำแหน่ง',
      '3': 'รายละเอียด'
    },
    'LOV_CODAPP': {
      '0': 'รายชื่อของโปรแกรม',
      '1': 'รายชื่อของโปรแกรม',
      '2': 'รหัสโปรแกรม',
      '3': 'รายละเอียด'
    },
    'LOV_MEMO': {
      '0': 'รายการรหัส Memo',
      '1': 'รหัส Memo',
      '2': 'ปี',
      '3': 'เดือน',
      '4': 'รุ่น',
      '5': 'หลักสูตร',
      '6': 'รายละเอียด'
    },
    'LOV_CORS': {
      '1': 'รายชื่อหลักสูตรการอบรมพนักงาน',
      '2': 'รหัสหลักสูตร',
      '3': 'ชื่อหลักสูตร'
    },
    'LOV_FORM': {
      '0': 'รายการเลชที่แบบฟอร์มประมเินผล',
      '1': 'รายการเลชที่แบบฟอร์มประมเินผล',
      '2': 'เลขที่แบบฟอร์ม',
      '3': 'รายละเอียด'
    },
    'LOV_APP3': {
      '0': 'รายชื่อเลขที่ใบสมัคร',
      '1': 'รายการเลขที่ใบสมัคร',
      '2': 'เลขที่ใบสมัคร',
      '3': 'ชื่อผู้สมัคร',
      '4': 'สถานะ'
    },
    'LOV_APLV': {
      '0': 'รายชื่อประเภทกลุ่มการประเมิน',
      '1': 'รายชื่อประเภทกลุ่มการประเมิน',
      '2': 'ประเภทกลุ่มการประเมิน',
      '3': 'รายละเอียด'
    },
    'LOV_NUMREQ': {
      '0': 'รายการเลขที่ใบขออนุมัติจ้างงาน',
      '1': 'รายการเลขที่ใบขออนุมัติจ้างงาน',
      '2': 'เลขที่ใบขออนุมัติ',
      '3': 'หน่วยงานที่ขอ',
      '4': 'ตำแหน่งที่ขอ',
      '5': 'รายละเอียด'
    },
    'LOV_CODAPPR': {
      '0': 'รายชื่อพนักงานทั้งหมด',
      '1': 'รายชื่อพนักงานทั้งหมด',
      '2': 'รหัสพนักงาน',
      '3': 'ชื่อ - นามสกุล',
      '4': 'สถานะพนักงาน',
      '5': 'Security'
    },
    'LOV_SHIF': {
      '0': 'รายการรหัสกะ',
      '1': 'รายการรหัสกะ',
      '2': 'รหัสกะ',
      '3': 'รายละเอียด',
      '4': 'ตารางเวลา'
    },
    'LOV_ROOM': {
      '0': 'รายการห้องประชุม',
      '1': 'รายการห้องประชุม',
      '2': 'เลขที่ห้อง',
      '3': 'ชื่อห้อง'
    },
    'LOV_ASSET': {
      '0': 'รายการรหัสทรัพย์สินของบริษัท',
      '1': 'รายการรหัสทรัพย์สินของบริษัท',
      '2': 'รหัสทรัพย์สินของบริษัท',
      '3': 'รายละเอียด'
    },
    'LOV_WORK': {
      '0': 'รายการรหัสกลุ่มการทำงาน',
      '1': 'รายการรหัสกลุ่มการทำงาน',
      '2': 'รหัสกลุ่มการทำงาน',
      '3': 'รายละเอียด'
    },
    'HRAP31EC6': {
      '250': '',
      '260': '',
      '270': ''
    },
    'LOV_CODUSER': {
      '0': 'รายชื่อผู้ใช้ระบบ',
      '1': 'รายชื่อผู้ใช้ระบบ',
      '2': 'รหัสผู้ใช้ระบบ',
      '3': 'ชื่อของผู้ใช้ระบบ'
    },
    'LOV_CODSECU': {
      '0': 'รายชื่อรหัสกลุ่ม Security',
      '1': 'รายชื่อรหัสกลุ่ม Security',
      '2': 'รหัสกลุ่ม Security',
      '3': 'ชื่อกลุ่ม Security'
    },
    'LOV_CODPROC': {
      '0': 'รายชื่อ Work Process',
      '1': 'รายชื่อ Work Process',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_CODINCOM': {
      '0': 'รหัสประเภทการจ้างงาน',
      '1': 'รหัสประเภทการจ้างงาน',
      '2': 'รหัสประเภทการจ้างงาน',
      '3': 'รายละเอียด'
    },
    'LOV_CODRETRO': {
      '0': 'รายการรหัสเงินได้อื่นๆ',
      '1': 'รายการรหัสเงินได้อื่นๆ',
      '2': 'รหัสตกเบิก',
      '3': 'รายละเอียด'
    },
    'LOV_CODTABLE': {
      '0': 'รายการรหัสตาราง',
      '1': 'รายการรหัสตาราง',
      '2': 'รหัสตาราง',
      '3': 'รายละเอียด'
    },
    'LOV_CODVAL': {
      '0': 'รายการรหัสค่าข้อมูล',
      '1': 'รายการรหัสค่าข้อมูล',
      '2': 'รหัสค่าข้อมูล',
      '3': 'รายละเอียด'
    }
  }
}
