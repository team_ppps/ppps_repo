module.exports = [
  {
    code: 'PPS',
    name: 'People plus Co.,Ltd.',
    level: 1,
    title: 'Company',
    codcomp: 'PPS',
    children: [
      {
        code: 'DEV',
        name: 'Developer',
        level: 2,
        title: 'Department',
        codcomp: 'PPS-DEV',
        children: [
          {
            code: 'PHP',
            name: 'Php Developer',
            level: 3,
            title: 'Unit',
            codcomp: 'PPS-DEV-PHP',
            children: [
              {
                code: 'FRONT',
                name: 'Php Frontend Developer',
                level: 4,
                title: 'Group',
                codcomp: 'PPS-DEV-PHP-FRONT',
                children: [
                  {
                    code: 'V11',
                    name: 'Php Frontend Version 11',
                    level: 5,
                    title: 'Team',
                    codcomp: 'PPS-DEV-PHP-FRONT-V11',
                    children: [
                      {
                        code: 'PM',
                        name: 'Php Project manager Frontend V11',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-FRONT-V11-PM'
                      }, {
                        code: 'SENIOR',
                        name: 'Php Senior Frontend V11',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-FRONT-V11-SENIOR'
                      }, {
                        code: 'JUNIOR',
                        name: 'Php Junior Frontend V11',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-FRONT-V11-JUNIOR'
                      }
                    ]
                  }, {
                    code: 'V104',
                    name: 'Php Frontend Version 10.4',
                    level: 5,
                    title: 'Team',
                    codcomp: 'PPS-DEV-PHP-FRONT-V104',
                    children: [
                      {
                        code: 'PM',
                        name: 'Php Project manager Frontend V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-FRONT-V104-PM'
                      }, {
                        code: 'SENIOR',
                        name: 'Php Senior Frontend V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-FRONT-V104-SENIOR'
                      }, {
                        code: 'JUNIOR',
                        name: 'Php Junior Frontend V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-FRONT-V104-JUNIOR'
                      }
                    ]
                  }
                ]
              }, {
                code: 'BACK',
                name: 'Php Backend Developer',
                level: 4,
                title: 'Group',
                codcomp: 'PPS-DEV-PHP-BACK',
                children: [
                  {
                    code: 'V11',
                    name: 'Php Backend Version 11',
                    level: 5,
                    title: 'Team',
                    codcomp: 'PPS-DEV-PHP-BACK-V11',
                    children: [
                      {
                        code: 'SA',
                        name: 'Php System Analyst Backend V11',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-BACK-V11-SA'
                      }, {
                        code: 'SENIOR',
                        name: 'Php Senior Backend V11',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-BACK-V11-SENIOR'
                      }, {
                        code: 'JUNIOR',
                        name: 'Php Junior Backend V11',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-BACK-V11-JUNIOR'
                      }
                    ]
                  }, {
                    code: 'V104',
                    name: 'Php Backend Version 10.4',
                    level: 5,
                    title: 'Team',
                    codcomp: 'PPS-DEV-PHP-BACK-V104',
                    children: [
                      {
                        code: 'SA',
                        name: 'Php System Analyst Backend V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-BACK-V104-SA'
                      }, {
                        code: 'SENIOR',
                        name: 'Php Senior Backend V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-BACK-V104-SENIOR'
                      }, {
                        code: 'JUNIOR',
                        name: 'Php Junior Backend V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-BACK-V104-JUNIOR'
                      }
                    ]
                  }
                ]
              }, {
                code: 'DESIGN',
                name: 'Php Desginer',
                level: 4,
                title: 'Group',
                codcomp: 'PPS-DEV-PHP-DESIGN',
                children: [
                  {
                    code: 'V11',
                    name: 'Php Design Version 11',
                    level: 5,
                    title: 'Team',
                    codcomp: 'PPS-DEV-PHP-DESIGN-V11',
                    children: [
                      {
                        code: 'SENIOR',
                        name: 'Php Senior Design V11',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-DESIGN-V11-SENIOR'
                      }, {
                        code: 'JUNIOR',
                        name: 'Php Junior Design V11',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-DESIGN-V11-JUNIOR'
                      }
                    ]
                  }, {
                    code: 'V104',
                    name: 'Php Design Version 10.4',
                    level: 5,
                    title: 'Team',
                    codcomp: 'PPS-DEV-PHP-DESIGN-V104',
                    children: [
                      {
                        code: 'SENIOR',
                        name: 'Php Senior Design V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-DESIGN-V104-SENIOR'
                      }, {
                        code: 'JUNIOR',
                        name: 'Php Junior Design V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-PHP-DESIGN-V104-JUNIOR'
                      }
                    ]
                  }
                ]
              }
            ]
          }, {
            code: 'ORA',
            name: 'Oracle Developer',
            level: 3,
            title: 'Unit',
            codcomp: 'PPS-DEV-ORA',
            children: [
              {
                code: 'FORM',
                name: 'Oracle Form Developer',
                level: 4,
                title: 'Group',
                codcomp: 'PPS-DEV-ORA-FORM',
                children: [
                  {
                    code: 'V104',
                    name: 'Oracle Form Version 10.4',
                    level: 5,
                    title: 'Team',
                    codcomp: 'PPS-DEV-ORA-FORM-V104',
                    children: [
                      {
                        code: 'PM',
                        name: 'Oracle Form Project Manager V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-FORM-V104-PM'
                      }, {
                        code: 'SA',
                        name: 'Oracle Form System Analyst V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-FORM-V104-SA'
                      }, {
                        code: 'SENIOR',
                        name: 'Oracle Form Senior V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-FORM-V104-SENIOR'
                      }, {
                        code: 'JUNIOR',
                        name: 'Oracle Form Junior V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-FORM-V104-JUNIOR'
                      }
                    ]
                  }, {
                    code: 'V1032',
                    name: 'Oracle Form Version 10.3.2',
                    level: 5,
                    title: 'Team',
                    codcomp: 'PPS-DEV-ORA-FORM-V1032',
                    children: [
                      {
                        code: 'PM',
                        name: 'Oracle Form Project Manager V1032',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-FORM-V1032-PM'
                      }, {
                        code: 'SA',
                        name: 'Oracle Form System Analyst V1032',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-FORM-V1032-SA'
                      }, {
                        code: 'SENIOR',
                        name: 'Oracle Form Senior V1032',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-FORM-V1032-SENIOR'
                      }, {
                        code: 'JUNIOR',
                        name: 'Oracle Form Junior V1032',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-FORM-V1032-JUNIOR'
                      }
                    ]
                  }
                ]
              }, {
                code: 'REPORT',
                name: 'Oracle Report Developer',
                level: 4,
                title: 'Group',
                codcomp: 'PPS-DEV-ORA-REPORT',
                children: [
                  {
                    code: 'V104',
                    name: 'Oracle Report Version 10.4',
                    level: 5,
                    title: 'Team',
                    codcomp: 'PPS-DEV-ORA-REPORT-V104',
                    children: [
                      {
                        code: 'PM',
                        name: 'Oracle Report Project Manager V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-REPORT-V104-PM'
                      }, {
                        code: 'SENIOR',
                        name: 'Oracle Report Senior V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-REPORT-V104-SENIOR'
                      }, {
                        code: 'JUNIOR',
                        name: 'Oracle Report Junior V104',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-REPORT-V104-JUNIOR'
                      }
                    ]
                  }, {
                    code: 'V1032',
                    name: 'Oracle Report Version 10.3.2',
                    level: 5,
                    title: 'Team',
                    codcomp: 'PPS-DEV-ORA-REPORT-V1032',
                    children: [
                      {
                        code: 'PM',
                        name: 'Oracle Report Project Manager V1032',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-REPORT-V104-PM'
                      }, {
                        code: 'SENIOR',
                        name: 'Oracle Report Senior V1032',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-REPORT-V104-SENIOR'
                      }, {
                        code: 'JUNIOR',
                        name: 'Oracle Report Junior V1032',
                        level: 6,
                        title: 'Position',
                        codcomp: 'PPS-DEV-ORA-REPORT-V104-JUNIOR'
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }, {
        code: 'IMP',
        name: 'Implement',
        level: 2,
        title: 'Department',
        codcomp: 'PPS-IMP'
      }, {
        code: 'SUP',
        name: 'Support',
        level: 2,
        title: 'Department',
        codcomp: 'PPS-SUP'
      }
    ]
  }
]
