import hrmsLovCodcompLabels from '../assets/labels'

export const mocker = {
  lovCodcompLabels () {
    return {
      labels: hrmsLovCodcompLabels
    }
  },
  baseDepartments () {
    return require('../assets/baseDepartments')
  }
}
