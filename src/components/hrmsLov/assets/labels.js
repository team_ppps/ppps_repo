module.exports = {
  'en': {
    'LOV_COMP': {
      '0': 'List Of Company Name',
      '1': 'List Of Company Name',
      '2': 'Code of Company',
      '3': 'Name of Company'
    },
    'LOV_EMP': {
      '0': 'List of All Employee',
      '1': 'List of All Employee',
      '2': 'Employee Code',
      '3': 'Employee Name',
      '4': 'Status of Emp.',
      '5': 'Security'
    },
    'LOV_POS': {
      '0': 'List of Position Code',
      '1': 'List of Position Code',
      '2': 'Position Code',
      '3': 'Description'
    },
    'LOV_CODAPP': {
      '0': 'List of Appl. Name',
      '1': 'List of Appl. Name',
      '2': 'Code Appl. Name',
      '3': 'Description'
    },
    'LOV_MEMO': {
      '0': 'List Of Memo No.',
      '1': 'Memo No.',
      '2': 'Year',
      '3': 'Month',
      '4': 'Class',
      '5': 'Course',
      '6': 'Description'
    },
    'LOV_CORS': {
      '1': 'List of Course Training Code',
      '2': 'Course Code',
      '3': 'Course Name'
    },
    'LOV_FORM': {
      '0': 'List of Appraisal Form',
      '1': 'List of Appraisal Form',
      '2': 'App. Form No.',
      '3': 'Description'
    },
    'LOV_APP3': {
      '0': 'List of All Application No.',
      '1': 'List of All Application No.',
      '2': 'Applicant No.',
      '3': 'Appl Name.',
      '4': 'Status'
    },
    'LOV_APLV': {
      '0': 'Type of Appraisal group',
      '1': 'Type of Appraisal group',
      '2': 'Type of Appraisal group',
      '3': 'Description'
    },
    'LOV_REQ3': {
      '0': 'List of Personnal Request',
      '1': 'List of Personnal Request',
      '2': 'Request No',
      '3': 'Department Req.',
      '4': 'Position Req.',
      '5': 'Description'
    },
    'LOV_CODAPPR': {
      '0': 'List of All Employee',
      '1': 'List of All Employee',
      '2': 'Employee Code',
      '3': 'Employee Name',
      '4': 'Status of Emp.',
      '5': 'Security'
    },
    'LOV_SHIF': {
      '0': 'List of Shift Code',
      '1': 'List of Shift Code',
      '2': 'Shift Code',
      '3': 'Description',
      '4': 'Schedule Time'
    },
    'LOV_ROOM': {
      '0': 'List of Room Number',
      '1': 'List of Room Number',
      '2': 'Room Number',
      '3': 'Room Name'
    },
    'LOV_ASSET': {
      '0': 'List of Company Asset Code',
      '1': 'List of Company Asset Code',
      '2': 'Company Asset Code',
      '3': 'Description'
    },
    'LOV_WORK': {
      '0': 'List of Work Group Code',
      '1': 'List of Work Group Code',
      '2': 'Work Group Code',
      '3': 'Description'
    },
    'HRAP31EC6': {
      '250': '',
      '260': '',
      '270': ''
    },
    'LOV_CODUSER': {
      '0': 'List of User Name',
      '1': 'List of User Name',
      '2': 'Code of User',
      '3': 'Name of User'
    },
    'LOV_CODSECU': {
      '0': 'List of Security Group',
      '1': 'List of Security Group',
      '2': 'Security Group Code',
      '3': 'Security Group Name'
    },
    'LOV_CODPROC': {
      '0': 'List of Work Process',
      '1': 'List of Work Process',
      '2': 'Code of Process',
      '3': 'Description'
    },
    'LOV_CODINCOM': {
      '0': 'List of Fixed Income Code',
      '1': 'List of Fixed Income Code',
      '2': 'Income Code',
      '3': 'Description'
    },
    'LOV_CODRETRO': {
      '0': 'List Of Other Income Code',
      '1': 'List Of Other Income Code',
      '2': 'Income Code',
      '3': 'Description'
    },
    'LOV_CODTABLE': {
      '0': 'List Of Table Code',
      '1': 'List Of Table Code',
      '2': 'Table Code',
      '3': 'Description'
    },
    'LOV_CODVAL': {
      '0': 'List Of Value Code',
      '1': 'List Of Value Code',
      '2': 'Value Code',
      '3': 'Description'
    },
    'LOV_COSTCENT': {
      '0': 'รายการรหัส Cost Center',
      '1': 'รายการรหัส Cost Center',
      '2': 'รหัสค่าข้อมูล',
      '3': 'รายละเอียด'
    },
    'LOV_COMPGRP': {
      '0': 'รายการรหัสกลุ่มบริษัท',
      '1': 'รายการรหัสกลุ่มบริษัท',
      '2': 'รหัสค่าข้อมูล',
      '3': 'รายละเอียด'
    },
    'LOV_CODTENCY': {
      '0': 'Type of Special Skills/Exp.',
      '1': 'Type of Special Skills/Exp.',
      '2': 'Code of Special Skills/Exp.',
      '3': 'Description'
    },
    'LOV_CODOCCUP': {
      '0': 'List Of Occupation Code',
      '1': 'List Of Occupation Code',
      '2': 'Occupation Code',
      '3': 'Description'
    },
    'LOV_CODHOSP': {
      '0': 'List Of Hospital Code',
      '1': 'List Of Hospital Code',
      '2': 'Hospital Code',
      '3': 'Description'
    },
    'LOV_CODPROV': {
      '0': 'List Of Province',
      '1': 'List Of Province',
      '2': 'Province Code',
      '3': 'Description'
    },
    'LOV_CODGRPWORK': {
      '0': 'List Of Group Work',
      '1': 'List Of Group Work',
      '2': 'Group Work Code',
      '3': 'Description'
    },
    'LOV_CODCARCAB': {
      '0': 'List Of Car Cable',
      '1': 'List Of Car Cable',
      '2': 'Car Cable Code',
      '3': 'Description'
    },
    'LOV_CODTYPINC': {
      '0': 'List Of Type Inc',
      '1': 'List Of Type Inc',
      '2': 'Type Inc Code',
      '3': 'Description'
    },
    'LOV_CODTYPPAYR': {
      '0': 'List Of Pay Slip',
      '1': 'List Of Pay Slip',
      '2': 'Pay Slip Code',
      '3': 'Description'
    },
    'LOV_CODTYPPAYT': {
      '0': 'List Of Tax Certificate',
      '1': 'List Of Tax Certificate',
      '2': 'Tax Certificate Code',
      '3': 'Description'
    },
    'LOV_CODTAX': {
      '0': 'List Of Tax Code',
      '1': 'List Of Tax Code',
      '2': 'Tax Code',
      '3': 'Description'
    },
    'LOV_NUMOTREQ': {
      '0': 'No. of OT Request',
      '1': 'No. of OT Request',
      '2': 'Request No.',
      '3': 'Employee Name',
      '4': 'Department',
      '5': 'Employee Group'
    },
    'LOV_CODREM': {
      '0': 'Reasons for Overtime Request',
      '1': 'Reasons for Overtime Request',
      '2': 'Code of Reasons',
      '3': 'Description'
    },
    'LOV_TYPELEAVE': {
      '0': 'List of Type of Leave',
      '1': 'List of Type of Leave',
      '2': 'Type of Leave',
      '3': 'Description'
    },
    'LOV_CODPAY': {
      '0': 'List of Deduct Code',
      '1': 'List of Deduct Code',
      '2': 'Deduct Code',
      '3': 'Description'
    },
    'LOV_CODPAYROL': {
      '0': 'List of Payroll Group',
      '1': 'List of Payroll Group',
      '2': 'Payroll Group Code',
      '3': 'Description'
    },
    'LOV_CODFUND': {
      '0': 'List of Fund Code',
      '1': 'List of Fund Code',
      '2': 'Fund Code',
      '3': 'Description'
    },
    'LOV_CODPLAN': {
      '0': 'List of Plan Code',
      '1': 'List of Plan Code',
      '2': 'Plan Code',
      '3': 'Description'
    },
    'LOV_CODCAUSE': {
      '0': 'List of Cause Code',
      '1': 'List of Cause Code',
      '2': 'Cause Code',
      '3': 'Description'
    },
    'LOV_CODEDLV': {
      '0': 'List of Education Code',
      '1': 'List of Education Code',
      '2': 'Education Code',
      '3': 'Description'
    },
    'LOV_CODMAJSB': {
      '0': 'List of Major Code',
      '1': 'List of Major Code',
      '2': 'Major Code',
      '3': 'Description'
    },
    'LOV_CODAWARD': {
      '0': 'List of Award Code',
      '1': 'List of Award Code',
      '2': 'Award Code',
      '3': 'Description'
    }
  },
  'th': {
    'LOV_COMP': {
      '0': 'รายชื่อบริษัท',
      '1': 'รายชื่อบริษัท',
      '2': 'รหัสบริษัท',
      '3': 'ชือบริษัท'
    },
    'LOV_EMP': {
      '0': 'รายชื่อพนักงานทั้งหมด',
      '1': 'รายชื่อพนักงานทั้งหมด',
      '2': 'รหัสพนักงาน',
      '3': 'ชื่อ - นามสกุล',
      '4': 'สถานะพนักงาน',
      '5': 'Security'
    },
    'LOV_POS': {
      '0': 'รายชื่อตำแหน่งงาน',
      '1': 'รายชื่อตำแหน่งงาน',
      '2': 'รหัสตำแหน่ง',
      '3': 'รายละเอียด'
    },
    'LOV_CODAPP': {
      '0': 'รายชื่อของโปรแกรม',
      '1': 'รายชื่อของโปรแกรม',
      '2': 'รหัสโปรแกรม',
      '3': 'รายละเอียด'
    },
    'LOV_MEMO': {
      '0': 'รายการรหัส Memo',
      '1': 'รหัส Memo',
      '2': 'ปี',
      '3': 'เดือน',
      '4': 'รุ่น',
      '5': 'หลักสูตร',
      '6': 'รายละเอียด'
    },
    'LOV_CORS': {
      '1': 'รายชื่อหลักสูตรการอบรมพนักงาน',
      '2': 'รหัสหลักสูตร',
      '3': 'ชื่อหลักสูตร'
    },
    'LOV_FORM': {
      '0': 'รายการเลชที่แบบฟอร์มประมเินผล',
      '1': 'รายการเลชที่แบบฟอร์มประมเินผล',
      '2': 'เลขที่แบบฟอร์ม',
      '3': 'รายละเอียด'
    },
    'LOV_APP3': {
      '0': 'รายชื่อเลขที่ใบสมัคร',
      '1': 'รายการเลขที่ใบสมัคร',
      '2': 'เลขที่ใบสมัคร',
      '3': 'ชื่อผู้สมัคร',
      '4': 'สถานะ'
    },
    'LOV_APLV': {
      '0': 'รายชื่อประเภทกลุ่มการประเมิน',
      '1': 'รายชื่อประเภทกลุ่มการประเมิน',
      '2': 'ประเภทกลุ่มการประเมิน',
      '3': 'รายละเอียด'
    },
    'LOV_NUMREQ': {
      '0': 'รายการเลขที่ใบขออนุมัติจ้างงาน',
      '1': 'รายการเลขที่ใบขออนุมัติจ้างงาน',
      '2': 'เลขที่ใบขออนุมัติ',
      '3': 'หน่วยงานที่ขอ',
      '4': 'ตำแหน่งที่ขอ',
      '5': 'รายละเอียด'
    },
    'LOV_CODAPPR': {
      '0': 'รายชื่อพนักงานทั้งหมด',
      '1': 'รายชื่อพนักงานทั้งหมด',
      '2': 'รหัสพนักงาน',
      '3': 'ชื่อ - นามสกุล',
      '4': 'สถานะพนักงาน',
      '5': 'Security'
    },
    'LOV_SHIF': {
      '0': 'รายการรหัสกะ',
      '1': 'รายการรหัสกะ',
      '2': 'รหัสกะ',
      '3': 'รายละเอียด',
      '4': 'ตารางเวลา'
    },
    'LOV_ROOM': {
      '0': 'รายการห้องประชุม',
      '1': 'รายการห้องประชุม',
      '2': 'เลขที่ห้อง',
      '3': 'ชื่อห้อง'
    },
    'LOV_ASSET': {
      '0': 'รายการรหัสทรัพย์สินของบริษัท',
      '1': 'รายการรหัสทรัพย์สินของบริษัท',
      '2': 'รหัสทรัพย์สินของบริษัท',
      '3': 'รายละเอียด'
    },
    'LOV_WORK': {
      '0': 'รายการรหัสกลุ่มการทำงาน',
      '1': 'รายการรหัสกลุ่มการทำงาน',
      '2': 'รหัสกลุ่มการทำงาน',
      '3': 'รายละเอียด'
    },
    'HRAP31EC6': {
      '250': '',
      '260': '',
      '270': ''
    },
    'LOV_CODUSER': {
      '0': 'รายชื่อผู้ใช้ระบบ',
      '1': 'รายชื่อผู้ใช้ระบบ',
      '2': 'รหัสผู้ใช้ระบบ',
      '3': 'ชื่อของผู้ใช้ระบบ'
    },
    'LOV_CODSECU': {
      '0': 'รายชื่อรหัสกลุ่ม Security',
      '1': 'รายชื่อรหัสกลุ่ม Security',
      '2': 'รหัสกลุ่ม Security',
      '3': 'ชื่อกลุ่ม Security'
    },
    'LOV_CODPROC': {
      '0': 'รายชื่อ Work Process',
      '1': 'รายชื่อ Work Process',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_CODINCOM': {
      '0': 'รหัสประเภทการจ้างงาน',
      '1': 'รหัสประเภทการจ้างงาน',
      '2': 'รหัสประเภทการจ้างงาน',
      '3': 'รายละเอียด'
    },
    'LOV_CODRETRO': {
      '0': 'รายการรหัสเงินได้อื่นๆ',
      '1': 'รายการรหัสเงินได้อื่นๆ',
      '2': 'รหัสตกเบิก',
      '3': 'รายละเอียด'
    },
    'LOV_CODTABLE': {
      '0': 'รายการรหัสตาราง',
      '1': 'รายการรหัสตาราง',
      '2': 'รหัสตาราง',
      '3': 'รายละเอียด'
    },
    'LOV_CODVAL': {
      '0': 'รายการรหัสค่าข้อมูล',
      '1': 'รายการรหัสค่าข้อมูล',
      '2': 'รหัสค่าข้อมูล',
      '3': 'รายละเอียด'
    },
    'LOV_COSTCENT': {
      '0': 'รายการรหัส Cost Center',
      '1': 'รายการรหัส Cost Center',
      '2': 'รหัส Cost Center',
      '3': 'รายละเอียด'
    },
    'LOV_COMPGRP': {
      '0': 'รายการรหัสกลุ่มบริษัท',
      '1': 'รายการรหัสกลุ่มบริษัท',
      '2': 'รหัสกลุ่มบริษัท',
      '3': 'รายละเอียด'
    },
    'LOV_CODTENCY': {
      '0': 'รหัสความสามารถ',
      '1': 'รหัสความสามารถ',
      '2': 'รหัสความสามารถ',
      '3': 'รายละเอียด'
    },
    'LOV_CODOCCUP': {
      '0': 'รายการรหัสอาชีพ',
      '1': 'รายการรหัสอาชีพ',
      '2': 'รหัสอาชีพ',
      '3': 'รายละเอียด'
    },
    'LOV_CODHOSP': {
      '0': 'รายการรหัสโรงพยาบาล',
      '1': 'รายการรหัสโรงพยาบาล',
      '2': 'รหัสโรงพยาบาล',
      '3': 'รายละเอียด'
    },
    'LOV_CODPROV': {
      '0': 'รายการรหัสจังหวัด',
      '1': 'รายการรหัสจังหวัด',
      '2': 'รหัสจังหวัด',
      '3': 'รายละเอียด'
    },
    'LOV_CODGRPWORK': {
      '0': 'รายการรหัสกลุ่มทำงาน',
      '1': 'รายการรหัสกลุ่มทำงาน',
      '2': 'รหัสกลุ่มทำงาน',
      '3': 'รายละเอียด'
    },
    'LOV_CODCARCAB': {
      '0': 'รายการรหัสสายรถ',
      '1': 'รายการรหัสสายรถ',
      '2': 'รหัสสายรถ',
      '3': 'รายละเอียด'
    },
    'LOV_CODTYPINC': {
      '0': 'รายการรหัสประเภทเงินได้',
      '1': 'รายการรหัสประเภทเงินได้',
      '2': 'รหัสประเภทเงินได้',
      '3': 'รายละเอียด'
    },
    'LOV_CODTYPPAYR': {
      '0': 'รายการรหัสใบจ่ายเงินเดือน',
      '1': 'รายการรหัสใบจ่ายเงินเดือน',
      '2': 'รหัสใบจ่ายเงินเดือน',
      '3': 'รายละเอียด'
    },
    'LOV_CODTYPPAYT': {
      '0': 'รายการรหัสหนังสือรับรองภาษี',
      '1': 'รายการรหัสหนังสือรับรองภาษี',
      '2': 'รหัสหนังสือรับรองภาษี',
      '3': 'รายละเอียด'
    },
    'LOV_CODTAX': {
      '0': 'รายการรหัสภาษี',
      '1': 'รายการรหัสภาษี',
      '2': 'รหัสภาษี',
      '3': 'รายละเอียด'
    },
    'LOV_NUMOTREQ': {
      '0': 'เลขที่ใบขออนุมัติการทำงานล่วงเวลา',
      '1': 'เลขที่ใบขออนุมัติการทำงานล่วงเวลา',
      '2': 'เลขที่ใบขออนุมัติ',
      '3': 'ชื่อพนักงาน',
      '4': 'หน่วยงาน',
      '5': 'กลุ่มพนักงาน'
    },
    'LOV_CODREM': {
      '0': 'สาเหตุการขออนุมัติทำล่วงเวลา',
      '1': 'สาเหตุการขออนุมัติทำล่วงเวลา',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_TYPELEAVE': {
      '0': 'รหัสประเภทการลา',
      '1': 'รหัสประเภทการลา',
      '2': 'ประเภทการลา',
      '3': 'รายละเอียด'
    },
    'LOV_CODPAY': {
      '0': 'รายการรหัสส่วนหัก',
      '1': 'รายการรหัสส่วนหัก',
      '2': 'รหัสส่วนหัก',
      '3': 'รายละเอียด'
    },
    'LOV_CODPAYROL': {
      '0': 'รายการกลุ่มการจ่ายเงินเดือน',
      '1': 'รายการกลุ่มการจ่ายเงินเดือน',
      '2': 'รหัสกลุ่มการจ่ายเงินเดือน',
      '3': 'รายละเอียด'
    },
    'LOV_CODFUND': {
      '0': 'รายการรหัสกองทุน',
      '1': 'รายการรหัสกองทุน',
      '2': 'รหัสกองทุน',
      '3': 'รายละเอียด'
    },
    'LOV_CODPLAN': {
      '0': 'รายการนำส่งกองทุน',
      '1': 'รายการนำส่งกองทุน',
      '2': 'รหัสนำส่งกองทุน',
      '3': 'รายละเอียด'
    },
    'LOV_CODCAUSE': {
      '0': 'รายการรหัสสาเหตุ',
      '1': 'รายการรหัสสาเหตุ',
      '2': 'รหัสสาเหตุ',
      '3': 'รายละเอียด'
    },
    'LOV_CODEDLV': {
      '0': 'รายการรหัสการศึกษา',
      '1': 'รายการรหัสการศึกษา',
      '2': 'รหัสการศึกษา',
      '3': 'รายละเอียด'
    },
    'LOV_CODMAJSB': {
      '0': 'รายการรหัสวิชาหลัก',
      '1': 'รายการรหัสวิชาหลัก',
      '2': 'รหัสวิชาหลัก ',
      '3': 'รายละเอียด'
    },
    'LOV_CODAWARD': {
      '0': 'รายการรหัสประเภทเบี้ยขยัน',
      '1': 'รายการรหัสประเภทเบี้ยขยัน',
      '2': 'รหัสประเภทเบี้ยขยัน ',
      '3': 'รายละเอียด'
    }
  }
}
