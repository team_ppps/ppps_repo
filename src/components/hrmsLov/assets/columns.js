export let hrmsLovColumns = () => {
  let codempidColumns = [
    {
      key: 'codempid',
      name: '',
      labelCodapp: 'LOV_EMP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codempid',
      name: '',
      labelCodapp: 'LOV_EMP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_staemp',
      name: '',
      labelCodapp: 'LOV_EMP',
      labelIndex: '4',
      style: 'min-width: 150px;'
    }
  ]

  let codposColumns = [
    {
      key: 'codpos',
      name: '',
      labelCodapp: 'LOV_POS',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codpos',
      name: '',
      labelCodapp: 'LOV_POS',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]

  let codappColumns = [
    {
      key: 'codapp',
      name: '',
      labelCodapp: 'LOV_CODAPP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codapp',
      name: '',
      labelCodapp: 'LOV_CODAPP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]

  let nummemoColumns = [
    {
      key: 'nummemo',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '1',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'dteyear',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'dtemonth',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '3',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'numclseq',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '4',
      class: 'align-center',
      style: 'min-width: 80px;'
    }, {
      key: 'codcours',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '5',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'namcours',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '6',
      style: 'min-width: 200px;'
    }
  ]

  let codcoursColumns = [
    {
      key: 'codcours',
      name: '',
      labelCodapp: 'LOV_CORS',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codcours',
      name: '',
      labelCodapp: 'LOV_CORS',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]

  let codformColumns = [
    {
      key: 'codform',
      name: '',
      labelCodapp: 'LOV_FORM',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codform',
      name: '',
      labelCodapp: 'LOV_FORM',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]

  let numapplColumns = [
    {
      key: 'numappl',
      name: '',
      labelCodapp: 'LOV_APP3',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_numappl',
      name: '',
      labelCodapp: 'LOV_APP3',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_statappl',
      name: '',
      labelCodapp: 'LOV_APP3',
      labelIndex: '4',
      style: 'min-width: 200px;'
    }
  ]

  let numreqColumns = [
    {
      key: 'numreq',
      name: '',
      labelCodapp: 'LOV_REQ3',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'codcomp',
      name: '',
      labelCodapp: 'LOV_REQ3',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }, {
      key: 'codpos',
      name: '',
      labelCodapp: 'LOV_REQ3',
      labelIndex: '4',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codpos',
      name: '',
      labelCodapp: 'LOV_REQ3',
      labelIndex: '5',
      style: 'min-width: 200px;'
    }
  ]
  let codaplvlColumns = [
    {
      key: 'codaplvl',
      name: '',
      labelCodapp: 'LOV_APLV',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codaplvl',
      name: '',
      labelCodapp: 'LOV_APLV',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let grdscorColumns = [
    {
      key: 'grditem',
      name: '',
      labelCodapp: 'HRAP31EC6',
      labelIndex: '250',
      style: 'min-width: 100px;'
    }, {
      key: 'achieve',
      name: '',
      labelCodapp: 'HRAP31EC6',
      labelIndex: '260',
      style: 'min-width: 150px;'
    }, {
      key: 'qtyscor',
      name: '',
      labelCodapp: 'HRAP31EC6',
      labelIndex: '270',
      class: 'align-right',
      style: 'min-width: 100px;'
    }
  ]
  let codapprColumns = [
    {
      key: 'codappr',
      name: '',
      labelCodapp: 'LOV_CODAPPR',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codappr',
      name: '',
      labelCodapp: 'LOV_CODAPPR',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_staemp',
      name: '',
      labelCodapp: 'LOV_CODAPPR',
      labelIndex: '4',
      style: 'min-width: 150px;'
    }
  ]
  let codshiftColumns = [
    {
      key: 'codshift',
      name: '',
      labelCodapp: 'LOV_SHIF',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codshift',
      name: '',
      labelCodapp: 'LOV_SHIF',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }, {
      key: 'schedule',
      name: '',
      labelCodapp: 'LOV_SHIF',
      labelIndex: '4',
      class: 'align-center',
      style: 'min-width: 150px;'
    }
  ]
  let codroomColumns = [
    {
      key: 'codroom',
      name: '',
      labelCodapp: 'LOV_ROOM',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codroom',
      name: '',
      labelCodapp: 'LOV_ROOM',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codassetColumns = [
    {
      key: 'codasset',
      name: '',
      labelCodapp: 'LOV_ASSET',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codasset',
      name: '',
      labelCodapp: 'LOV_ASSET',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codcalenColumns = [
    {
      key: 'codcalen',
      name: '',
      labelCodapp: 'LOV_WORK',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codcalen',
      name: '',
      labelCodapp: 'LOV_WORK',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codcompyColumns = [
    {
      key: 'codcompy',
      name: '',
      labelCodapp: 'LOV_COMP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codcompy',
      name: '',
      labelCodapp: 'LOV_COMP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let coduserColumns = [
    {
      key: 'coduser',
      name: '',
      labelCodapp: 'LOV_CODUSER',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_coduser',
      name: '',
      labelCodapp: 'LOV_CODUSER',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codsecuColumns = [
    {
      key: 'codsecu',
      name: '',
      labelCodapp: 'LOV_CODSECU',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codsecu',
      name: '',
      labelCodapp: 'LOV_CODSECU',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codprocColumns = [
    {
      key: 'codproc',
      name: '',
      labelCodapp: 'LOV_CODPROC',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codproc',
      name: '',
      labelCodapp: 'LOV_CODPROC',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codincomColumns = [
    {
      key: 'codincom',
      name: '',
      labelCodapp: 'LOV_CODINCOM',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codincom',
      name: '',
      labelCodapp: 'LOV_CODINCOM',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codretroColumns = [
    {
      key: 'codretro',
      name: '',
      labelCodapp: 'LOV_CODRETRO',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codretro',
      name: '',
      labelCodapp: 'LOV_CODRETRO',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtableColumns = [
    {
      key: 'codtable',
      name: '',
      labelCodapp: 'LOV_CODTABLE',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtable',
      name: '',
      labelCodapp: 'LOV_CODTABLE',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codvalColumns = [
    {
      key: 'codval',
      name: '',
      labelCodapp: 'LOV_CODVAL',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codval',
      name: '',
      labelCodapp: 'LOV_CODVAL',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let costCenterColumns = [
    {
      key: 'costcent',
      name: '',
      labelCodapp: 'LOV_COSTCENT',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_costcent',
      name: '',
      labelCodapp: 'LOV_COSTCENT',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let compGroupColumns = [
    {
      key: 'compgrp',
      name: '',
      labelCodapp: 'LOV_COMPGRP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_compgrp',
      name: '',
      labelCodapp: 'LOV_COMPGRP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtencyColumns = [
    {
      key: 'codtency',
      name: '',
      labelCodapp: 'LOV_CODTENCY',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtency',
      name: '',
      labelCodapp: 'LOV_CODTENCY',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codoccupColumns = [
    {
      key: 'codoccup',
      name: '',
      labelCodapp: 'LOV_CODOCCUP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codoccup',
      name: '',
      labelCodapp: 'LOV_CODOCCUP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codhospColumns = [
    {
      key: 'codhosp',
      name: '',
      labelCodapp: 'LOV_CODHOSP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codhosp',
      name: '',
      labelCodapp: 'LOV_CODHOSP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codprovColumns = [
    {
      key: 'codprov',
      name: '',
      labelCodapp: 'LOV_CODPROV',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codprov',
      name: '',
      labelCodapp: 'LOV_CODPROV',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codgrpworkColumns = [
    {
      key: 'codgrpwork',
      name: '',
      labelCodapp: 'LOV_CODGRPWORK',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codgrpwork',
      name: '',
      labelCodapp: 'LOV_CODGRPWORK',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codcarcabColumns = [
    {
      key: 'codcarcab',
      name: '',
      labelCodapp: 'LOV_CODCARCAB',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codcarcab',
      name: '',
      labelCodapp: 'LOV_CODCARCAB',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtypincColumns = [
    {
      key: 'codtypinc',
      name: '',
      labelCodapp: 'LOV_CODTYPINC',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtypinc',
      name: '',
      labelCodapp: 'LOV_CODTYPINC',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtyppayrColumns = [
    {
      key: 'codtyppayr',
      name: '',
      labelCodapp: 'LOV_CODTYPPAYR',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtyppayr',
      name: '',
      labelCodapp: 'LOV_CODTYPPAYR',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtyppaytColumns = [
    {
      key: 'codtyppayt',
      name: '',
      labelCodapp: 'LOV_CODTYPPAYT',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtyppayt',
      name: '',
      labelCodapp: 'LOV_CODTYPPAYT',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtaxColumns = [
    {
      key: 'codtax',
      name: '',
      labelCodapp: 'LOV_CODTAX',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtax',
      name: '',
      labelCodapp: 'LOV_CODTAX',
      labelIndex: '3',
      class: 'align-center',
      style: 'min-width: 150px;'
    }
  ]
  let numotreqColumns = [
    {
      key: 'numotreq',
      name: '',
      labelCodapp: 'LOV_NUMOTREQ',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codempid',
      name: '',
      labelCodapp: 'LOV_NUMOTREQ',
      labelIndex: '3',
      class: 'align-center',
      style: 'min-width: 200px;'
    }, {
      key: 'namcent',
      name: '',
      labelCodapp: 'LOV_NUMOTREQ',
      labelIndex: '4',
      class: 'align-center',
      style: 'min-width: 250px;'
    }, {
      key: 'namcalen',
      name: '',
      labelCodapp: 'LOV_NUMOTREQ',
      labelIndex: '5',
      style: 'min-width: 250px;'
    }
  ]
  let codremColumns = [
    {
      key: 'codrem',
      name: '',
      labelCodapp: 'LOV_CODREM',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codrem',
      name: '',
      labelCodapp: 'LOV_CODREM',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let typeleaveColumns = [
    {
      key: 'typeleave',
      name: '',
      labelCodapp: 'LOV_TYPELEAVE',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_typeleave',
      name: '',
      labelCodapp: 'LOV_TYPELEAVE',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codpayColumns = [
    {
      key: 'codpay',
      name: '',
      labelCodapp: 'LOV_CODPAY',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codpay',
      name: '',
      labelCodapp: 'LOV_CODPAY',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codpayrolColumns = [
    {
      key: 'codpayrol',
      name: '',
      labelCodapp: 'LOV_CODPAYROL',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codpayrol',
      name: '',
      labelCodapp: 'LOV_CODPAYROL',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codfundColumns = [
    {
      key: 'codfund',
      name: '',
      labelCodapp: 'LOV_CODFUND',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codfund',
      name: '',
      labelCodapp: 'LOV_CODFUND',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codplanColumns = [
    {
      key: 'codplan',
      name: '',
      labelCodapp: 'LOV_CODPLAN',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codplan',
      name: '',
      labelCodapp: 'LOV_CODPLAN',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codcauseColumns = [
    {
      key: 'codcause',
      name: '',
      labelCodapp: 'LOV_CODCAUSE',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codcause',
      name: '',
      labelCodapp: 'LOV_CODCAUSE',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codedlvColumns = [
    {
      key: 'codedlv',
      name: '',
      labelCodapp: 'LOV_CODEDLV',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codedlv',
      name: '',
      labelCodapp: 'LOV_CODEDLV',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codmajsbColumns = [
    {
      key: 'codmajsb',
      name: '',
      labelCodapp: 'LOV_CODMAJSB',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codmajsb',
      name: '',
      labelCodapp: 'LOV_CODMAJSB',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codawardColumns = [
    {
      key: 'codaward',
      name: '',
      labelCodapp: 'LOV_CODAWARD',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codaward',
      name: '',
      labelCodapp: 'LOV_CODAWARD',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]

  return {
    en: {
      codempid: codempidColumns,
      codpos: codposColumns,
      codapp: codappColumns,
      nummemo: nummemoColumns,
      codcours: codcoursColumns,
      codform: codformColumns,
      numappl: numapplColumns,
      numreq: numreqColumns,
      codaplvl: codaplvlColumns,
      grdscor: grdscorColumns,
      codappr: codapprColumns,
      codshift: codshiftColumns,
      codroom: codroomColumns,
      codasset: codassetColumns,
      codcalen: codcalenColumns,
      codcompy: codcompyColumns,
      coduser: coduserColumns,
      codsecu: codsecuColumns,
      codproc: codprocColumns,
      codincom: codincomColumns,
      codretro: codretroColumns,
      codtable: codtableColumns,
      codval: codvalColumns,
      costcent: costCenterColumns,
      compgrp: compGroupColumns,
      codtency: codtencyColumns,
      codoccup: codoccupColumns,
      codhosp: codhospColumns,
      codprov: codprovColumns,
      codgrpwork: codgrpworkColumns,
      codcarcab: codcarcabColumns,
      codtypinc: codtypincColumns,
      codtyppayr: codtyppayrColumns,
      codtyppayt: codtyppaytColumns,
      codtax: codtaxColumns,
      numotreq: numotreqColumns,
      codrem: codremColumns,
      typeleave: typeleaveColumns,
      codpay: codpayColumns,
      codpayrol: codpayrolColumns,
      codfund: codfundColumns,
      codplan: codplanColumns,
      codcause: codcauseColumns,
      codedlv: codedlvColumns,
      codmajsb: codmajsbColumns,
      codaward: codawardColumns
    },
    th: {
      codempid: codempidColumns,
      codpos: codposColumns,
      codapp: codappColumns,
      nummemo: nummemoColumns,
      codcours: codcoursColumns,
      codform: codformColumns,
      numappl: numapplColumns,
      numreq: numreqColumns,
      codaplvl: codaplvlColumns,
      grdscor: grdscorColumns,
      codappr: codapprColumns,
      codshift: codshiftColumns,
      codroom: codroomColumns,
      codasset: codassetColumns,
      codcalen: codcalenColumns,
      codcompy: codcompyColumns,
      coduser: coduserColumns,
      codsecu: codsecuColumns,
      codproc: codprocColumns,
      codincom: codincomColumns,
      codretro: codretroColumns,
      codtable: codtableColumns,
      codval: codvalColumns,
      costcent: costCenterColumns,
      compgrp: compGroupColumns,
      codtency: codtencyColumns,
      codoccup: codoccupColumns,
      codhosp: codhospColumns,
      codprov: codprovColumns,
      codgrpwork: codgrpworkColumns,
      codcarcab: codcarcabColumns,
      codtypinc: codtypincColumns,
      codtyppayr: codtyppayrColumns,
      codtyppayt: codtyppaytColumns,
      codtax: codtaxColumns,
      numotreq: numotreqColumns,
      codrem: codremColumns,
      typeleave: typeleaveColumns,
      codpay: codpayColumns,
      codpayrol: codpayrolColumns,
      codfund: codfundColumns,
      codplan: codplanColumns,
      codcause: codcauseColumns,
      codedlv: codedlvColumns,
      codmajsb: codmajsbColumns,
      codaward: codawardColumns
    }
  }
}
