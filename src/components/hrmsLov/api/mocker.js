import faker from 'faker'
// import moment from 'moment'
import hrmsLovLabels from '../assets/labels'

export const mocker = {
  lovLabels () {
    return {
      labels: hrmsLovLabels
    }
  },
  lovCodempid () {
    let mockRows = []
    const mockTotal = 20
    mockRows = mockRows.concat([
      {
        codempid: '53100',
        namempe: 'นายกง กิต',
        namempt: 'นายกง กิต',
        namemp3: 'นายกง กิต',
        namemp4: 'นายกง กิต',
        namemp5: 'นายกง กิต',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '54003',
        namempe: 'น.ส.กชกร ผลถาวร',
        namempt: 'น.ส.กชกร ผลถาวร',
        namemp3: 'น.ส.กชกร ผลถาวร',
        namemp4: 'น.ส.กชกร ผลถาวร',
        namemp5: 'น.ส.กชกร ผลถาวร',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '57004',
        namempe: 'น.ส.กนกวรรณ พันธ์ประสิทธิ์',
        namempt: 'น.ส.กนกวรรณ พันธ์ประสิทธิ์',
        namemp3: 'น.ส.กนกวรรณ พันธ์ประสิทธิ์',
        namemp4: 'น.ส.กนกวรรณ พันธ์ประสิทธิ์',
        namemp5: 'น.ส.กนกวรรณ พันธ์ประสิทธิ์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '38055',
        namempe: 'นายขจรศักดิ์  บรรลือศักดิ์',
        namempt: 'นายขจรศักดิ์  บรรลือศักดิ์',
        namemp3: 'นายขจรศักดิ์  บรรลือศักดิ์',
        namemp4: 'นายขจรศักดิ์  บรรลือศักดิ์',
        namemp5: 'นายขจรศักดิ์  บรรลือศักดิ์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '54190',
        namempe: 'นายคง ชินนอก',
        namempt: 'นายคง ชินนอก',
        namemp3: 'นายคง ชินนอก',
        namemp4: 'นายคง ชินนอก',
        namemp5: 'นายคง ชินนอก',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '53074',
        namempe: 'นายคชภัค ฮวดศรีคณากร',
        namempt: 'นายคชภัค ฮวดศรีคณากร',
        namemp3: 'นายคชภัค ฮวดศรีคณากร',
        namemp4: 'นายคชภัค ฮวดศรีคณากร',
        namemp5: 'นายคชภัค ฮวดศรีคณากร',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '37010',
        namempe: 'นายจตุฤทธิ์ เสงี่ยม',
        namempt: 'นายจตุฤทธิ์ เสงี่ยม',
        namemp3: 'นายจตุฤทธิ์ เสงี่ยม',
        namemp4: 'นายจตุฤทธิ์ เสงี่ยม',
        namemp5: 'นายจตุฤทธิ์ เสงี่ยม',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '33172',
        namempe: 'นายจรัญ  คงวุฒิ',
        namempt: 'นายจรัญ  คงวุฒิ',
        namemp3: 'นายจรัญ  คงวุฒิ',
        namemp4: 'นายจรัญ  คงวุฒิ',
        namemp5: 'นายจรัญ  คงวุฒิ',
        desstaemp: 'ทดลองงาน',
        staemp: ''
      },
      {
        codempid: '37033',
        namempe: 'นายชรินทร์  ชัชวาล',
        namempt: 'นายชรินทร์  ชัชวาล',
        namemp3: 'นายชรินทร์  ชัชวาล',
        namemp4: 'นายชรินทร์  ชัชวาล',
        namemp5: 'นายชรินทร์  ชัชวาล',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '55154',
        namempe: 'นายชรินทร์ น้อยมา',
        namempt: 'นายชรินทร์ น้อยมา',
        namemp3: 'นายชรินทร์ น้อยมา',
        namemp4: 'นายชรินทร์ น้อยมา',
        namemp5: 'นายชรินทร์ น้อยมา',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '55155',
        namempe: 'นายชัยฤทธิ์ จุลศรี',
        namempt: 'นายชัยฤทธิ์ จุลศรี',
        namemp3: 'นายชัยฤทธิ์ จุลศรี',
        namemp4: 'นายชัยฤทธิ์ จุลศรี',
        namemp5: 'นายชัยฤทธิ์ จุลศรี',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '50059',
        namempe: 'นายชัยวัฒน์  คำภีร์',
        namempt: 'นายชัยวัฒน์  คำภีร์',
        namemp3: 'นายชัยวัฒน์  คำภีร์',
        namemp4: 'นายชัยวัฒน์  คำภีร์',
        namemp5: 'นายชัยวัฒน์  คำภีร์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '54114',
        namempe: 'นายชุติพงษ์ ทองทา',
        namempt: 'นายชุติพงษ์ ทองทา',
        namemp3: 'นายชุติพงษ์ ทองทา',
        namemp4: 'นายชุติพงษ์ ทองทา',
        namemp5: 'นายชุติพงษ์ ทองทา',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '52100',
        namempe: 'นางฐานิดา ครรชิต',
        namempt: 'นางฐานิดา ครรชิต',
        namemp3: 'นางฐานิดา ครรชิต',
        namemp4: 'นางฐานิดา ครรชิต',
        namemp5: 'นางฐานิดา ครรชิต',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '53185',
        namempe: 'นายวิโรจน์ สมรัตน์',
        namempt: 'นายวิโรจน์ สมรัตน์',
        namemp3: 'นายวิโรจน์ สมรัตน์',
        namemp4: 'นายวิโรจน์ สมรัตน์',
        namemp5: 'นายวิโรจน์ สมรัตน์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '55290',
        namempe: 'นายศราวุธ ทองเภา',
        namempt: 'นายศราวุธ ทองเภา',
        namemp3: 'นายศราวุธ ทองเภา',
        namemp4: 'นายศราวุธ ทองเภา',
        namemp5: 'นายศราวุธ ทองเภา',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '55382',
        namempe: 'นายสงวน บรรดาศักดิ์',
        namempt: 'นายสงวน บรรดาศักดิ์',
        namemp3: 'นายสงวน บรรดาศักดิ์',
        namemp4: 'นายสงวน บรรดาศักดิ์',
        namemp5: 'นายสงวน บรรดาศักดิ์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '16010',
        namempe: 'นายสมชาย เข็มกลัดทองแดง',
        namempt: 'นายสมชาย เข็มกลัดทองแดง',
        namemp3: 'นายสมชาย เข็มกลัดทองแดง',
        namemp4: 'นายสมชาย เข็มกลัดทองแดง',
        namemp5: 'นายสมชาย เข็มกลัดทองแดง',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '50165',
        namempe: 'นายสมศักดิ์ สำรวยศักดิ์',
        namempt: 'นายสมศักดิ์ สำรวยศักดิ์',
        namemp3: 'นายสมศักดิ์ สำรวยศักดิ์',
        namemp4: 'นายสมศักดิ์ สำรวยศักดิ์',
        namemp5: 'นายสมศักดิ์ สำรวยศักดิ์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '53144',
        namempe: 'นายประเสริฐ  สมศักดิ์',
        namempt: 'นายประเสริฐ  สมศักดิ์',
        namemp3: 'นายประเสริฐ  สมศักดิ์',
        namemp4: 'นายประเสริฐ  สมศักดิ์',
        namemp5: 'นายประเสริฐ  สมศักดิ์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodpos () {
    let mockRows = []
    const mockTotal = 30
    mockRows = mockRows.concat([
      {
        codpos: '0100',
        nampose: 'Managing director',
        nampost: 'Managing director',
        nampos3: 'Managing director',
        nampos4: 'Managing director',
        nampos5: 'Managing director'
      },
      {
        codpos: '0110',
        nampose: 'Advisor',
        nampost: 'Advisor',
        nampos3: 'Advisor',
        nampos4: 'Advisor',
        nampos5: 'Advisor'
      },
      {
        codpos: '0120',
        nampose: 'Secretary',
        nampost: 'Secretary',
        nampos3: 'Secretary',
        nampos4: 'Secretary',
        nampos5: 'Secretary'
      },
      {
        codpos: '0200',
        nampose: 'Marketing Manager',
        nampost: 'Marketing Manager',
        nampos3: 'Marketing Manager',
        nampos4: 'Marketing Manager',
        nampos5: 'Marketing Manager'
      },
      {
        codpos: '0300',
        nampose: 'Technical Manager',
        nampost: 'Technical Manager',
        nampos3: 'Technical Manager',
        nampos4: 'Technical Manager',
        nampos5: 'Technical Manager'
      },
      {
        codpos: '0310',
        nampose: 'Project Manager',
        nampost: 'Project Manager',
        nampos3: 'Project Manager',
        nampos4: 'Project Manager',
        nampos5: 'Project Manager'
      },
      {
        codpos: '0320',
        nampose: 'Support Manager',
        nampost: 'Support Manager',
        nampos3: 'Support Manager',
        nampos4: 'Support Manager',
        nampos5: 'Support Manager'
      },
      {
        codpos: '0330',
        nampose: 'Development Manager',
        nampost: 'Development Manager',
        nampos3: 'Development Manager',
        nampos4: 'Development Manager',
        nampos5: 'Development Manager'
      },
      {
        codpos: '0340',
        nampose: 'HR Manager',
        nampost: 'HR Manager',
        nampos3: 'HR Manager',
        nampos4: 'HR Manager',
        nampos5: 'HR Manager'
      },
      {
        codpos: '0510',
        nampose: 'Project Leader',
        nampost: 'Project Leader',
        nampos3: 'Project Leader',
        nampos4: 'Project Leader',
        nampos5: 'Project Leader'
      },
      {
        codpos: '0520',
        nampose: 'Sr. Sale',
        nampost: 'Sr. Sale',
        nampos3: 'Sr. Sale',
        nampos4: 'Sr. Sale',
        nampos5: 'Sr. Sale'
      },
      {
        codpos: '0530',
        nampose: 'Sr. Maketing',
        nampost: 'Sr. Maketing',
        nampos3: 'Sr. Maketing',
        nampos4: 'Sr. Maketing',
        nampos5: 'Sr. Maketing'
      },
      {
        codpos: '0600',
        nampose: 'Test Leader',
        nampost: 'Test Leader',
        nampos3: 'Test Leader',
        nampos4: 'Test Leader',
        nampos5: 'Test Leader'
      },
      {
        codpos: '0605',
        nampose: 'Sale',
        nampost: 'Sale',
        nampos3: 'Sale',
        nampos4: 'Sale',
        nampos5: 'Sale'
      },
      {
        codpos: '0610',
        nampose: 'Assistant Project Leader',
        nampost: 'Assistant Project Leader',
        nampos3: 'Assistant Project Leader',
        nampos4: 'Assistant Project Leader',
        nampos5: 'Assistant Project Leader'
      },
      {
        codpos: '0620',
        nampose: 'Senior Customer Support',
        nampost: 'Senior Customer Support',
        nampos3: 'Senior Customer Support',
        nampos4: 'Senior Customer Support',
        nampos5: 'Senior Customer Support'
      },
      {
        codpos: '0630',
        nampose: 'Senior System Analyst',
        nampost: 'Senior System Analyst',
        nampos3: 'Senior System Analyst',
        nampos4: 'Senior System Analyst',
        nampos5: 'Senior System Analyst'
      },
      {
        codpos: '0640',
        nampose: 'System Engineer',
        nampost: 'System Engineer',
        nampos3: 'System Engineer',
        nampos4: 'System Engineer',
        nampos5: 'System Engineer'
      },
      {
        codpos: '0650',
        nampose: 'System Analyst',
        nampost: 'System Analyst',
        nampos3: 'System Analyst',
        nampos4: 'System Analyst',
        nampos5: 'System Analyst'
      },
      {
        codpos: '0710',
        nampose: 'Software Consult',
        nampost: 'Software Consult',
        nampos3: 'Software Consult',
        nampos4: 'Software Consult',
        nampos5: 'Software Consult'
      },
      {
        codpos: '0715',
        nampose: 'Sr Programmer',
        nampost: 'Sr Programmer',
        nampos3: 'Sr Programmer',
        nampos4: 'Sr Programmer',
        nampos5: 'Sr Programmer'
      },
      {
        codpos: '0720',
        nampose: 'Technical Consult',
        nampost: 'Technical Consult',
        nampos3: 'Technical Consult',
        nampos4: 'Technical Consult',
        nampos5: 'Technical Consult'
      },
      {
        codpos: '0320',
        nampose: 'Support Manager',
        nampost: 'Support Manager',
        nampos3: 'Support Manager',
        nampos4: 'Support Manager',
        nampos5: 'Support Manager'
      },
      {
        codpos: '0760',
        nampose: 'Programmer',
        nampost: 'Programmer',
        nampos3: 'Programmer',
        nampos4: 'Programmer',
        nampos5: 'Programmer'
      },
      {
        codpos: '0810',
        nampose: 'Software Tester',
        nampost: 'Software Tester',
        nampos3: 'Software Tester',
        nampos4: 'Software Tester',
        nampos5: 'Software Tester'
      },
      {
        codpos: '0840',
        nampose: 'Technical',
        nampost: 'Technical',
        nampos3: 'Technical',
        nampos4: 'Technical',
        nampos5: 'Technical'
      },
      {
        codpos: '0900',
        nampose: 'Subcontact',
        nampost: 'Subcontact',
        nampos3: 'Subcontact',
        nampos4: 'Subcontact',
        nampos5: 'Subcontact'
      },
      {
        codpos: '0910',
        nampose: 'Staff Officer',
        nampost: 'Staff Officer',
        nampos3: 'Staff Officer',
        nampos4: 'Staff Officer',
        nampos5: 'Staff Officer'
      },
      {
        codpos: '0920',
        nampose: 'Parttime',
        nampost: 'Parttime',
        nampos3: 'Parttime',
        nampos4: 'Parttime',
        nampos5: 'Parttime'
      },
      {
        codpos: '4100',
        nampose: 'General Worker 1',
        nampost: 'General Worker 1',
        nampos3: 'General Worker 1',
        nampos4: 'General Worker 1',
        nampos5: 'General Worker 1'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodapp () {
    let mockRows = []
    const mockTotal = 15
    mockRows = mockRows.concat([
      {
        codapp: 'HRES32E',
        namappe: 'Personal Data Change',
        namappt: 'เปลี่ยนแปลงข้อมูลพนักงาน',
        namapp3: 'Personal Data Change',
        namapp4: 'Personal Data Change',
        namapp5: 'Personal Data Change'
      },
      {
        codapp: 'HRES34E',
        namappe: 'Transfer Request',
        namappt: 'ขอโอนย้าย',
        namapp3: 'Transfer Request',
        namapp4: 'Transfer Request',
        namapp5: 'Transfer Request'
      },
      {
        codapp: 'HRES36E',
        namappe: 'Working Ref. Letter Request',
        namappt: 'ขอหนังสือรับรอง',
        namapp3: 'Working Ref. Letter Request',
        namapp4: 'Working Ref. Letter Request',
        namapp5: 'Working Ref. Letter Request'
      },
      {
        codapp: 'HRES62E',
        namappe: 'Leave Application',
        namappt: 'ลาหยุดงาน',
        namapp3: 'Leave Application',
        namapp4: 'Leave Application',
        namapp5: 'Leave Application'
      },
      {
        codapp: 'HRES6AE',
        namappe: 'Change of Actual Time',
        namappt: 'แก้ไขเวลาเข้า-ออก',
        namapp3: 'Change of Actual Time',
        namapp4: 'Change of Actual Time',
        namapp5: 'Change of Actual Time'
      },
      {
        codapp: 'HRES6DE',
        namappe: 'Change of Shift Schedule',
        namappt: 'แก้ไขเวลาทำงาน',
        namapp3: 'Change of Shift Schedule',
        namapp4: 'Change of Shift Schedule',
        namapp5: 'Change of Shift Schedule'
      },
      {
        codapp: 'HRES6IE',
        namappe: 'Training Request',
        namappt: 'ขอฝึกอบรม',
        namapp3: 'Training Request',
        namapp4: 'Training Request',
        namapp5: 'Training Request'
      },
      {
        codapp: 'HRES6KE',
        namappe: 'O.T. Request',
        namappt: 'ทำงานล่วงเวลา',
        namapp3: 'O.T. Request',
        namapp4: 'O.T. Request',
        namapp5: 'O.T. Request'
      },
      {
        codapp: 'HRES6ME',
        namappe: 'Cancel Leave',
        namappt: 'ยกเลิกการลาหยุดงาน',
        namapp3: 'Cancel Leave',
        namapp4: 'Cancel Leave',
        namapp5: 'Cancel Leave'
      },
      {
        codapp: 'HRES71E',
        namappe: 'Medical Requisition',
        namappt: 'เบิกค่ารักษาพยาบาล',
        namapp3: 'Medical Requisition',
        namapp4: 'Medical Requisition',
        namapp5: 'Medical Requisition'
      },
      {
        codapp: 'HRES74E',
        namappe: 'Benefits Requisition',
        namappt: 'เบิกค่าสวัสดิการ',
        namapp3: 'Benefits Requisition',
        namapp4: 'Benefits Requisition',
        namapp5: 'Benefits Requisition'
      },
      {
        codapp: 'HRES77E',
        namappe: 'Loan Requisition',
        namappt: 'เบิกเงินกู้',
        namapp3: 'Loan Requisition',
        namapp4: 'Loan Requisition',
        namapp5: 'Loan Requisition'
      },
      {
        codapp: 'HRES81E',
        namappe: 'Travel',
        namappt: 'ค่าเดินทาง',
        namapp3: 'Travel',
        namapp4: 'Travel',
        namapp5: 'Travel'
      },
      {
        codapp: 'HRES86E',
        namappe: 'Resignation Requisition',
        namappt: 'ลาออก',
        namapp3: 'Resignation Requisition',
        namapp4: 'Resignation Requisition',
        namapp5: 'Resignation Requisition'
      },
      {
        codapp: 'HRES88E',
        namappe: 'Record Staff  Requisition',
        namappt: 'จ้างงาน',
        namapp3: 'Record Staff  Requisition',
        namapp4: 'Record Staff  Requisition',
        namapp5: 'Record Staff  Requisition'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovNummemo () {
    let mockRows = []
    const mockTotal = 15
    mockRows = mockRows.concat([
      {
        nummemo: '2017/009',
        dteyear: '2017',
        dtemonth: '9',
        numclseq: '7',
        namcourse: 'TAX Planing Strategy',
        namcourst: 'TAX Planing Strategy',
        namcours3: 'TAX Planing Strategy',
        namcours4: 'TAX Planing Strategy',
        namcours5: 'TAX Planing Strategy'
      },
      {
        nummemo: '2017/008',
        dteyear: '2017',
        dtemonth: '8',
        numclseq: '2',
        namcourse: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcourst: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours3: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours4: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours5: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง'
      },
      {
        nummemo: '2017/007',
        dteyear: '2017',
        dtemonth: '11',
        numclseq: '5',
        namcourse: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcourst: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours3: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours4: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours5: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง'
      },
      {
        nummemo: '2017/006',
        dteyear: '2017',
        dtemonth: '5',
        numclseq: '1',
        namcourse: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcourst: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours3: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours4: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours5: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ'
      },
      {
        nummemo: '2017/005',
        dteyear: '2017',
        dtemonth: '6',
        numclseq: '2',
        namcourse: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcourst: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours3: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours4: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours5: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ'
      },
      {
        nummemo: '2017/004',
        dteyear: '2017',
        dtemonth: '5',
        numclseq: '1',
        namcourse: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcourst: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours3: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours4: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours5: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน'
      },
      {
        nummemo: '2017/003',
        dteyear: '2017',
        dtemonth: '11',
        numclseq: '7',
        namcourse: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcourst: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours3: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours4: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours5: 'การใช้งานระบบบัญชีการซื้อขาย'
      },
      {
        nummemo: '2017/002',
        dteyear: '2017',
        dtemonth: '10',
        numclseq: '11',
        namcourse: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcourst: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours3: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours4: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours5: 'การใช้งาน Microsoft Windows เบื้องต้น'
      },
      {
        nummemo: '2017/001',
        dteyear: '2017',
        dtemonth: '6',
        numclseq: '3',
        namcourse: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcourst: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours3: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours4: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours5: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ'
      },
      {
        nummemo: '2016/001',
        dteyear: '2016',
        dtemonth: '7',
        numclseq: '1',
        namcourse: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcourst: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours3: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours4: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours5: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์'
      },
      {
        nummemo: '2558-006',
        dteyear: '2015',
        dtemonth: '8',
        numclseq: '4',
        namcourse: 'การสร้างแรงจูงใจ',
        namcourst: 'การสร้างแรงจูงใจ',
        namcours3: 'การสร้างแรงจูงใจ',
        namcours4: 'การสร้างแรงจูงใจ',
        namcours5: 'การสร้างแรงจูงใจ'
      },
      {
        nummemo: '2558-005',
        dteyear: '2015',
        dtemonth: '5',
        numclseq: '2',
        namcourse: 'การสร้างคนให้เป็นผู้นำ',
        namcourst: 'การสร้างคนให้เป็นผู้นำ',
        namcours3: 'การสร้างคนให้เป็นผู้นำ',
        namcours4: 'การสร้างคนให้เป็นผู้นำ',
        namcours5: 'การสร้างคนให้เป็นผู้นำ'
      },
      {
        nummemo: '2558-004',
        dteyear: '2015',
        dtemonth: '3',
        numclseq: '2',
        namcourse: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcourst: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours3: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours4: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours5: 'การวางแผนงานอย่างมีประสิทธิภาพ'
      },
      {
        nummemo: '2558-003',
        dteyear: '2015',
        dtemonth: '2',
        numclseq: '1',
        namcourse: 'การบริการจัดการทั่วไป',
        namcourst: 'การบริการจัดการทั่วไป',
        namcours3: 'การบริการจัดการทั่วไป',
        namcours4: 'การบริการจัดการทั่วไป',
        namcours5: 'การบริการจัดการทั่วไป'
      },
      {
        nummemo: '2558-002',
        dteyear: '2015',
        dtemonth: '3',
        numclseq: '2',
        namcourse: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcourst: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours3: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours4: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours5: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodcours () {
    let mockRows = []
    const mockTotal = 20
    mockRows = mockRows.concat([
      {
        codcours: 'AC0001',
        namcourse: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcourst: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours3: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours4: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours5: 'การใช้งานระบบบัญชีการซื้อขาย'
      },
      {
        codcours: 'AC0002',
        namcourse: 'TAX Planing Strategy',
        namcourst: 'TAX Planing Strategy',
        namcours3: 'TAX Planing Strategy',
        namcours4: 'TAX Planing Strategy',
        namcours5: 'TAX Planing Strategy'
      },
      {
        codcours: 'AC0003',
        namcourse: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcourst: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours3: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours4: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours5: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง'
      },
      {
        codcours: 'IT0001',
        namcourse: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcourst: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours3: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours4: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours5: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน'
      },
      {
        codcours: 'IT0002',
        namcourse: 'SQL Plus  for Information Technology',
        namcourst: 'SQL Plus  for Information Technology',
        namcours3: 'SQL Plus  for Information Technology',
        namcours4: 'SQL Plus  for Information Technology',
        namcours5: 'SQL Plus  for Information Technology'
      },
      {
        codcours: 'IT0003',
        namcourse: 'Oracle Developer',
        namcourst: 'Oracle Developer',
        namcours3: 'Oracle Developer',
        namcours4: 'Oracle Developer',
        namcours5: 'Oracle Developer'
      },
      {
        codcours: 'IT0004',
        namcourse: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcourst: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours3: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours4: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours5: 'การใช้งาน Microsoft Windows เบื้องต้น'
      },
      {
        codcours: 'IT0005',
        namcourse: 'Visual Studio .NET',
        namcourst: 'Visual Studio .NET',
        namcours3: 'Visual Studio .NET',
        namcours4: 'Visual Studio .NET',
        namcours5: 'Visual Studio .NET'
      },
      {
        codcours: 'M00001',
        namcourse: 'ผู้จัดการมืออาชีพเพื่อการทำงาน',
        namcourst: 'ผู้จัดการมืออาชีพเพื่อการทำงาน',
        namcours3: 'ผู้จัดการมืออาชีพเพื่อการทำงาน',
        namcours4: 'ผู้จัดการมืออาชีพเพื่อการทำงาน',
        namcours5: 'ผู้จัดการมืออาชีพเพื่อการทำงาน'
      },
      {
        codcours: 'M00002',
        namcourse: 'ผู้อำนวยการมืออาชีพเพื่อการทำงาน',
        namcourst: 'ผู้อำนวยการมืออาชีพเพื่อการทำงาน',
        namcours3: 'ผู้อำนวยการมืออาชีพเพื่อการทำงาน',
        namcours4: 'ผู้อำนวยการมืออาชีพเพื่อการทำงาน',
        namcours5: 'ผู้อำนวยการมืออาชีพเพื่อการทำงาน'
      },
      {
        codcours: 'M00003',
        namcourse: 'การบริการเชิงซ้อนเพื่อการสร้างทีม',
        namcourst: 'การบริการเชิงซ้อนเพื่อการสร้างทีม',
        namcours3: 'การบริการเชิงซ้อนเพื่อการสร้างทีม',
        namcours4: 'การบริการเชิงซ้อนเพื่อการสร้างทีม',
        namcours5: 'การบริการเชิงซ้อนเพื่อการสร้างทีม'
      },
      {
        codcours: 'M00004',
        namcourse: 'วิธีการบริหารงาน และการขอ ISO 9001',
        namcourst: 'วิธีการบริหารงาน และการขอ ISO 9001',
        namcours3: 'วิธีการบริหารงาน และการขอ ISO 9001',
        namcours4: 'วิธีการบริหารงาน และการขอ ISO 9001',
        namcours5: 'วิธีการบริหารงาน และการขอ ISO 9001'
      },
      {
        codcours: 'R00001',
        namcourse: 'ความรู้เบื้องต้นเกี่ยวกับการนำเสนองาน',
        namcourst: 'ความรู้เบื้องต้นเกี่ยวกับการนำเสนองาน',
        namcours3: 'ความรู้เบื้องต้นเกี่ยวกับการนำเสนองาน',
        namcours4: 'ความรู้เบื้องต้นเกี่ยวกับการนำเสนองาน',
        namcours5: 'ความรู้เบื้องต้นเกี่ยวกับการนำเสนองาน'
      },
      {
        codcours: 'R00002',
        namcourse: 'เทคนิคการประชุมที่มีประสิทธิภาพ',
        namcourst: 'เทคนิคการประชุมที่มีประสิทธิภาพ',
        namcours3: 'เทคนิคการประชุมที่มีประสิทธิภาพ',
        namcours4: 'เทคนิคการประชุมที่มีประสิทธิภาพ',
        namcours5: 'เทคนิคการประชุมที่มีประสิทธิภาพ'
      },
      {
        codcours: 'R00003',
        namcourse: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcourst: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours3: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours4: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours5: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์'
      },
      {
        codcours: 'R00004',
        namcourse: 'การสร้าง Customer Relations',
        namcourst: 'การสร้าง Customer Relations',
        namcours3: 'การสร้าง Customer Relations',
        namcours4: 'การสร้าง Customer Relations',
        namcours5: 'การสร้าง Customer Relations'
      },
      {
        codcours: 'R00005',
        namcourse: 'เลขามืออาชีพ',
        namcourst: 'เลขามืออาชีพ',
        namcours3: 'เลขามืออาชีพ',
        namcours4: 'เลขามืออาชีพ',
        namcours5: 'เลขามืออาชีพ'
      },
      {
        codcours: 'R00006',
        namcourse: 'การจัดการสิ่งแวดล้อม',
        namcourst: 'การจัดการสิ่งแวดล้อม',
        namcours3: 'การจัดการสิ่งแวดล้อม',
        namcours4: 'การจัดการสิ่งแวดล้อม',
        namcours5: 'การจัดการสิ่งแวดล้อม'
      },
      {
        codcours: 'R00007',
        namcourse: 'การบริการจัดการทั่วไป',
        namcourst: 'การบริการจัดการทั่วไป',
        namcours3: 'การบริการจัดการทั่วไป',
        namcours4: 'การบริการจัดการทั่วไป',
        namcours5: 'การบริการจัดการทั่วไป'
      },
      {
        codcours: 'R00008',
        namcourse: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcourst: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours3: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours4: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours5: 'การวางแผนงานอย่างมีประสิทธิภาพ'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodform () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        codform: '0001',
        desforme: 'Appraisal Form',
        desformt: 'แบบฟอร์มประเมินผลการปฎิบัติงาน',
        desform3: 'Appraisal Form',
        desform4: 'Appraisal Form',
        desform5: 'Appraisal Form'
      },
      {
        codform: '0002',
        desforme: 'General Appraisal Form',
        desformt: 'แบบประเมินผลหลังอบรม',
        desform3: 'General Appraisal Form',
        desform4: 'General Appraisal Form',
        desform5: 'General Appraisal Form'
      },
      {
        codform: '02RC',
        desforme: 'Interviews Form',
        desformt: 'แบบฟอร์มการสัมภาษณ์งาน - พนักงาน',
        desform3: 'Interviews Form',
        desform4: 'Interviews Form',
        desform5: 'Interviews Form'
      },
      {
        codform: '03RC',
        desforme: 'Interviews Form',
        desformt: 'แบบฟอร์มการสัมภาษณ์งาน - ผู้บริหาร',
        desform3: 'Interviews Form',
        desform4: 'Interviews Form',
        desform5: 'Interviews Form'
      },
      {
        codform: 'AD01',
        desforme: 'Appraisal Development',
        desformt: 'Appraisal Development',
        desform3: 'Appraisal Development',
        desform4: 'Appraisal Development',
        desform5: 'Appraisal Development'
      },
      {
        codform: 'AP01',
        desforme: 'Appraisal Form',
        desformt: 'แบบฟอร์มประเมินผลการปฎิบัติงาน',
        desform3: 'Appraisal Form',
        desform4: 'Appraisal Form',
        desform5: 'Appraisal Form'
      },
      {
        codform: 'AP02',
        desforme: 'Instructor Evaluate Form',
        desformt: 'แบบประเมินวิทยากร',
        desform3: 'Instructor Evaluate Form',
        desform4: 'Instructor Evaluate Form',
        desform5: 'Instructor Evaluate Form'
      },
      {
        codform: 'APY1',
        desforme: 'Performance Evaluation Form',
        desformt: 'แบบฟอร์มการประเมินผลประสิทธิภาพการทำงานประจำปี',
        desform3: 'Performance Evaluation Form',
        desform4: 'Performance Evaluation Form',
        desform5: 'Performance Evaluation Form'
      },
      {
        codform: 'APY2',
        desforme: 'General Appraisal Form',
        desformt: 'แบบฟอร์มทดสอบทั่วไป',
        desform3: 'General Appraisal Form',
        desform4: 'General Appraisal Form',
        desform5: 'General Appraisal Form'
      },
      {
        codform: 'C001',
        desforme: 'Beh Evaluate Form',
        desformt: 'ปัจจัยวัดความสามารถและพฤติกรรม',
        desform3: 'Beh Evaluate Form',
        desform4: 'Beh Evaluate Form',
        desform5: 'Beh Evaluate Form'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovNumappl () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        numappl: '2006504',
        desapple: 'Mr. Nophon  Komarachun',
        desapplt: 'นาย นพพล  โกมารชุน',
        desappl3: 'Mr. Nophon  Komarachun',
        desappl4: 'Mr. Nophon  Komarachun',
        desappl5: 'Mr. Nophon  Komarachun'
      },
      {
        numappl: '2007509',
        desapple: 'Miss. patharida  Aransri',
        desapplt: 'นางสาว ภัทริดา  อรัญศรี',
        desappl3: 'Miss. patharida  Aransri',
        desappl4: 'Miss. patharida  Aransri',
        desappl5: 'Miss. patharida  Aransri'
      },
      {
        numappl: '2007510',
        desapple: 'Mr.Kaew  Kagee',
        desapplt: 'นายแก้ว  ขจี',
        desappl3: 'Mr.Kaew  Kagee',
        desappl4: 'Mr.Kaew  Kagee',
        desappl5: 'Mr.Kaew  Kagee'
      },
      {
        numappl: '2007511',
        desapple: 'Mr. Akawoot  Bandon',
        desapplt: 'นาย เอกวุฒิ  บ้านดอน',
        desappl3: 'Mr. Akawoot  Bandon',
        desappl4: 'Mr. Akawoot  Bandon',
        desappl5: 'Mr. Akawoot  Bandon'
      },
      {
        numappl: '2007513',
        desapple: 'Miss. Meesuk  yusmer',
        desapplt: 'นางสาว มีสุข  อยู่เสมอ',
        desappl3: 'Miss. Meesuk  yusmer',
        desappl4: 'Miss. Meesuk  yusmer',
        desappl5: 'Miss. Meesuk  yusmer'
      },
      {
        numappl: '2007514',
        desapple: 'Mr. Pitool  Donsoongnern',
        desapplt: 'นาย ไพฑูร  ดอนสูงเนิน',
        desappl3: 'Mr. Pitool  Donsoongnern',
        desappl4: 'Mr. Pitool  Donsoongnern',
        desappl5: 'Mr. Pitool  Donsoongnern'
      },
      {
        numappl: '2007517',
        desapple: 'Mr.Paisarn  Sirivit',
        desapplt: 'นายไพศาล  สิริวิทย์',
        desappl3: 'Mr.Paisarn  Sirivit',
        desappl4: 'Mr.Paisarn  Sirivit',
        desappl5: 'Mr.Paisarn  Sirivit'
      },
      {
        numappl: '2007518',
        desapple: 'Mrs. Pimpa  Kana',
        desapplt: 'นาง พิมภา คณา',
        desappl3: 'Mrs. Pimpa  Kana',
        desappl4: 'Mrs. Pimpa  Kana',
        desappl5: 'Mrs. Pimpa  Kana'
      },
      {
        numappl: '2007519',
        desapple: 'Mrs. Sujitra  Khamtea',
        desapplt: 'นาง สุจิตรา  งามแท้',
        desappl3: 'Mrs. Sujitra  Khamtea',
        desappl4: 'Mrs. Sujitra  Khamtea',
        desappl5: 'Mrs. Sujitra  Khamtea'
      },
      {
        numappl: '2007520',
        desapple: 'Mr.Mongkol  Peamsook',
        desapplt: 'นายมงคล  เปี่ยมสุข',
        desappl3: 'Mr.Mongkol  Peamsook',
        desappl4: 'Mr.Mongkol  Peamsook',
        desappl5: 'Mr.Mongkol  Peamsook'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovNumreq () {
    let mockRows = []
    const mockTotal = 10
    const mockNumreq = [
      '2015/1',
      '2015/2',
      '2015/3',
      '2015/4',
      '2016/1',
      '2016/2',
      '2016/3',
      '2017/1',
      '2017/2',
      '2017/3'
    ]
    const mockCodcomp = [
      'TJS-100-000-000-000-000-000 ฝ่ายดำเนินการและพัฒนาระบบ',
      'TJS-200-000-000-000-000-000 ฝ่ายขายและการตลาด',
      'TJS-300-000-000-000-000-000 ฝ่ายบัญชีการเงินและทรัพยากรบุคคล',
      'TJS-100-100-000-000-000-000 ส่วนพัฒนาระบบและดำเนินการ',
      'TJS-300-000-000-000-000-000 ฝ่ายบัญชีการเงินและทรัพยากรบุคคล',
      'TJS-300-000-000-000-000-000 ฝ่ายบัญชีการเงินและทรัพยากรบุคคล',
      'TJS-000-000-000-000-000-000 บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด',
      'TJS-100-000-000-000-000-000 ฝ่ายดำเนินการและพัฒนาระบบ',
      'TJS-100-000-000-000-000-000 ฝ่ายดำเนินการและพัฒนาระบบ',
      'TJS-100-000-000-000-000-000 ฝ่ายดำเนินการและพัฒนาระบบ'
    ]
    const mockCodpos = [
      '0800',
      '0830',
      '0800',
      '0310',
      '0800',
      '0800',
      '0200',
      '0800',
      '0810',
      '0800'
    ]
    const mockNampos = [
      'Staff Officer',
      'Programmer',
      'Staff Officer',
      'Project Manager',
      'Staff Officer',
      'Staff Officer',
      'Marketing Manager',
      'Staff Officer',
      'Software Tester',
      'Staff Officer'
    ]
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        numreq: mockNumreq[i - 1],
        codcomp: mockCodcomp[i - 1],
        codpos: mockCodpos[i - 1],
        nampose: mockNampos[i - 1],
        nampost: mockNampos[i - 1],
        nampos3: mockNampos[i - 1],
        nampos4: mockNampos[i - 1],
        nampos5: mockNampos[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodaplvl () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        codaplvl: '0001',
        descode: 'Group Staff',
        descodt: 'กลุ่มพนักงาน',
        descod3: 'Group Staff',
        descod4: 'Group Staff',
        descod5: 'Group Staff'
      },
      {
        codaplvl: '0002',
        descode: 'Group Chief',
        descodt: 'กลุ่มหัวหน้างาน',
        descod3: 'Group Chief',
        descod4: 'Group Chief',
        descod5: 'Group Chief'
      },
      {
        codaplvl: '0003',
        descode: 'Management Group',
        descodt: 'กลุ่มผู้จัดการ',
        descod3: 'Management Group',
        descod4: 'Management Group',
        descod5: 'Management Group'
      },
      {
        codaplvl: '0004',
        descode: 'Advisor Group',
        descodt: 'กลุ่มที่ปรึกษา',
        descod3: 'Advisor Group',
        descod4: 'Advisor Group',
        descod5: 'Advisor Group'
      },
      {
        codaplvl: '0005',
        descode: 'Executive Group',
        descodt: 'กลุ่มผู้บริหาร',
        descod3: 'Executive Group',
        descod4: 'Executive Group',
        descod5: 'Executive Group'
      },
      {
        codaplvl: 'S001',
        descode: 'พนักงานระดับ 1-4',
        descodt: 'พนักงานระดับ 1-4',
        descod3: 'พนักงานระดับ 1-4',
        descod4: 'พนักงานระดับ 1-4',
        descod5: 'พนักงานระดับ 1-4'
      },
      {
        codaplvl: 'S002',
        descode: 'เจ้าหน้าที่ 5-8',
        descodt: 'เจ้าหน้าที่ 5-8',
        descod3: 'เจ้าหน้าที่ 5-8',
        descod4: 'เจ้าหน้าที่ 5-8',
        descod5: 'เจ้าหน้าที่ 5-8'
      },
      {
        codaplvl: 'S003',
        descode: 'ผู้ช่วยผู้จัดการ',
        descodt: 'ผู้ช่วยผู้จัดการ',
        descod3: 'ผู้ช่วยผู้จัดการ',
        descod4: 'ผู้ช่วยผู้จัดการ',
        descod5: 'ผู้ช่วยผู้จัดการ'
      },
      {
        codaplvl: 'S004',
        descode: 'ผู้จัดการ-ผู้จัดการอาวุโส',
        descodt: 'ผู้จัดการ-ผู้จัดการอาวุโส',
        descod3: 'ผู้จัดการ-ผู้จัดการอาวุโส',
        descod4: 'ผู้จัดการ-ผู้จัดการอาวุโส',
        descod5: 'ผู้จัดการ-ผู้จัดการอาวุโส'
      },
      {
        codaplvl: 'S005',
        descode: 'รองผู้อำนวยการ',
        descodt: 'รองผู้อำนวยการ',
        descod3: 'รองผู้อำนวยการ',
        descod4: 'รองผู้อำนวยการ',
        descod5: 'รองผู้อำนวยการ'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovGrdscor () {
    let mockRows = []
    let grdscor = ''
    const mockGrdscor = [ '5', '10', '15', '20' ]
    const mockQtyscor = {
      '5': '5.00',
      '10': '10.00',
      '15': '15.00',
      '20': '20.00'
    }
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      grdscor = mockGrdscor[Math.floor(Math.random() * 2)]
      mockRows.push({
        grdscor: '55',
        grditem: mockGrdscor[grdscor],
        achieve: 'TEST',
        qtyscor: mockQtyscor[grdscor]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodappr () {
    let mockRows = []
    let staemp = ''
    const mockStaemp = [ 'P', 'Y' ]
    const mockStatus = {
      'P': 'ทดลองงาน',
      'Y': 'ปัจจุบัน'
    }
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      staemp = mockStaemp[Math.floor(Math.random() * 2)]
      mockRows.push({
        codappr: faker.lorem.word(),
        namempe: faker.name.findName(),
        namempt: faker.name.findName(),
        namemp3: faker.name.findName(),
        namemp4: faker.name.findName(),
        namemp5: faker.name.findName(),
        desstaemp: mockStatus[staemp],
        staemp: ''
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodshift () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        codshift: 'SL',
        desshifte: 'Sales 08:00-17:00',
        desshiftt: 'Sales 08:00-17:00',
        desshift3: 'Sales 08:00-17:00',
        desshift4: 'Sales 08:00-17:00',
        desshift5: 'Sales 08:00-17:00',
        schedule: '08:00 - 17:00'
      },
      {
        codshift: 'P1',
        desshifte: 'ฝ่ายผลิต 08:00-17:00',
        desshiftt: 'ฝ่ายผลิต 08:00-17:00',
        desshift3: 'ฝ่ายผลิต 08:00-17:00',
        desshift4: 'ฝ่ายผลิต 08:00-17:00',
        desshift5: 'ฝ่ายผลิต 08:00-17:00',
        schedule: '08:00 - 17:00'
      },
      {
        codshift: 'P2',
        desshifte: 'ฝ่ายผลิต 08:00-20:00',
        desshiftt: 'ฝ่ายผลิต 08:00-20:00',
        desshift3: 'Production 08:00-20:00',
        desshift4: 'Production 08:00-20:00',
        desshift5: 'Production 08:00-20:00',
        schedule: '08:00 - 20:00'
      },
      {
        codshift: 'DS',
        desshifte: 'DS - 08:00-17:30',
        desshiftt: 'DS - 08:00-17:30',
        desshift3: 'DS - 08:00-17:30',
        desshift4: 'DS - 08:00-17:30',
        desshift5: 'DS - 08:00-17:30',
        schedule: '08:00 - 17:30'
      },
      {
        codshift: 'NS',
        desshifte: 'NS - 20:00-05:30',
        desshiftt: 'NS - 20:00-05:30',
        desshift3: 'NS - 20:00-05:30',
        desshift4: 'NS - 20:00-05:30',
        desshift5: 'NS - 20:00-05:30',
        schedule: '20:00 - 05:30'
      },
      {
        codshift: 'DA',
        desshifte: 'DA - 07:30-16:30',
        desshiftt: 'DA - 07:30-16:30',
        desshift3: 'DA - 07:30-16:30',
        desshift4: 'DA - 07:30-16:30',
        desshift5: 'DA - 07:30-16:30',
        schedule: '07:30 - 16:30'
      },
      {
        codshift: 'D1',
        desshifte: 'D1 - 08:00-17:00',
        desshiftt: 'D1 - 08:00-17:00',
        desshift3: 'D1 - 08:00-17:00',
        desshift4: 'D1 - 08:00-17:00',
        desshift5: 'D1 - 08:00-17:00',
        schedule: '08:00 - 17:00'
      },
      {
        codshift: 'D2',
        desshifte: 'D2 - 09:00-18:00',
        desshiftt: 'D2 - 09:00-18:00',
        desshift3: 'D2 - 09:00-18:00',
        desshift4: 'D2 - 09:00-18:00',
        desshift5: 'D2 - 09:00-18:00',
        schedule: '09:00 - 18:00'
      },
      {
        codshift: 'D3',
        desshifte: 'D3 - 10:00-19:00',
        desshiftt: 'D3 - 10:00-19:00',
        desshift3: 'D3 - 10:00-19:00',
        desshift4: 'D3 - 10:00-19:00',
        desshift5: 'D3 - 10:00-19:00',
        schedule: '10:00 - 19:00'
      },
      {
        codshift: 'S1',
        desshifte: 'Shift 08.00-16.00',
        desshiftt: 'Shift 08.00-16.00',
        desshift3: 'Shift 08.00-16.00',
        desshift4: 'Shift 08.00-16.00',
        desshift5: 'Shift 08.00-16.00',
        schedule: '08:00 - 16:00'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodroom () {
    let mockRows = []
    const mockTotal = 4
    mockRows = mockRows.concat([
      {
        codroom: 'BATH001',
        roomname: 'ห้องประชุม 4 - ห้องสันติภาพ',
        roomnamt: 'ห้องประชุม 4 - ห้องสันติภาพ',
        roomnam3: 'ห้องประชุม 4 - ห้องสันติภาพ',
        roomnam4: 'ห้องประชุม 4 - ห้องสันติภาพ',
        roomnam5: 'ห้องประชุม 4 - ห้องสันติภาพ'
      },
      {
        codroom: 'R0003',
        roomname: 'ห้องประชุม 3 - เกศรา',
        roomnamt: 'ห้องประชุม 3 - เกศรา',
        roomnam3: 'ห้องประชุม 3 - เกศรา',
        roomnam4: 'ห้องประชุม 3 - เกศรา',
        roomnam5: 'ห้องประชุม 3 - เกศรา'
      },
      {
        codroom: 'R0001',
        roomname: 'ห้องประชุม 1 - ห้องบัวบาน',
        roomnamt: 'ห้องประชุม 1 - ห้องบัวบาน',
        roomnam3: 'ห้องประชุม 1 - ห้องบัวบาน',
        roomnam4: 'ห้องประชุม 1 - ห้องบัวบาน',
        roomnam5: 'ห้องประชุม 1 - ห้องบัวบาน'
      },
      {
        codroom: 'R0002',
        roomname: 'ห้องประชุม 2 - พุทธรักษ',
        roomnamt: 'ห้องประชุม 2 - พุทธรักษ',
        roomnam3: 'ห้องประชุม 2 - พุทธรักษ',
        roomnam4: 'ห้องประชุม 2 - พุทธรักษ',
        roomnam5: 'ห้องประชุม 2 - พุทธรักษ'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodasset () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        codasset: '0001',
        desassee: 'Mobile Phone',
        desasset: 'โทรศัพท์',
        desasse3: 'Mobile Phone',
        desasse4: 'Mobile Phone',
        desasse5: 'Mobile Phone'
      },
      {
        codasset: '0002',
        desassee: 'Computer NoteBook',
        desasset: 'คอมพิวเตอร์ NoteBook',
        desasse3: 'Computer NoteBook',
        desasse4: 'Computer NoteBook',
        desasse5: 'Computer NoteBook'
      },
      {
        codasset: '0003',
        desassee: 'PDA',
        desasset: 'PDA',
        desasse3: 'PDA',
        desasse4: 'PDA',
        desasse5: 'PDA'
      },
      {
        codasset: '0004',
        desassee: 'Projector',
        desasset: 'เครื่องฉายจอภาพ',
        desasse3: 'Projector',
        desasse4: 'Projector',
        desasse5: 'Projector'
      },
      {
        codasset: '0005',
        desassee: 'Car',
        desasset: 'รถยนต์',
        desasse3: 'Car',
        desasse4: 'Car',
        desasse5: 'Car'
      },
      {
        codasset: '0006',
        desassee: 'CD',
        desasset: 'แผ่น CD',
        desasse3: 'CD',
        desasse4: 'CD',
        desasse5: 'CD'
      },
      {
        codasset: '0007',
        desassee: 'Technbical Tools',
        desasset: 'อุปกรณ์เครืองมือช่าง',
        desasse3: 'Technbical Tools',
        desasse4: 'Technbical Tools',
        desasse5: 'Technbical Tools'
      },
      {
        codasset: '0008',
        desassee: 'Uniform',
        desasset: 'ชุดพนักงาน',
        desasse3: 'Uniform',
        desasse4: 'Uniform',
        desasse5: 'Uniform'
      },
      {
        codasset: '0009',
        desassee: 'Locker Keys',
        desasset: 'กุญแจ Lockers',
        desasse3: 'Locker Keys',
        desasse4: 'Locker Keys',
        desasse5: 'Locker Keys'
      },
      {
        codasset: '9999',
        desassee: 'N.A',
        desasset: 'อื่นๆ',
        desasse3: 'N.A',
        desasse4: 'N.A',
        desasse5: 'N.A'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodcalen () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        codcalen: '0001',
        descalene: 'General staff of Head office',
        descalent: 'กลุ่มสำนักงาน',
        descalen3: 'General staff of Head office',
        descalen4: 'General staff of Head office',
        descalen5: 'General staff of Head office'
      },
      {
        codcalen: '0002',
        descalene: 'General staff of Branch',
        descalent: 'กลุ่มสาขา',
        descalen3: 'General staff of Branch',
        descalen4: 'General staff of Branch',
        descalen5: 'General staff of Branch'
      },
      {
        codcalen: '0003',
        descalene: 'Staff shift 1',
        descalent: 'พนักงานกะ เช้า',
        descalen3: 'Staff shift 1',
        descalen4: 'Staff shift 1',
        descalen5: 'Staff shift 1'
      },
      {
        codcalen: '0004',
        descalene: 'Staff shift 2',
        descalent: 'พนักงานกะ บ่าย',
        descalen3: 'Staff shift 2',
        descalen4: 'Staff shift 2',
        descalen5: 'Staff shift 2'
      },
      {
        codcalen: '0005',
        descalene: 'Shift 2',
        descalent: 'พนักงานกะ ดึก',
        descalen3: 'Shift 2',
        descalen4: 'Shift 2',
        descalen5: 'Shift 2'
      },
      {
        codcalen: 'S1',
        descalene: 'Group Shift 1',
        descalent: 'กลุ่มกะ1',
        descalen3: 'Group Shift 1',
        descalen4: 'Group Shift 1',
        descalen5: 'Group Shift 1'
      },
      {
        codcalen: 'S2',
        descalene: 'Group Shift 2',
        descalent: 'กลุ่มกะ2',
        descalen3: 'Group Shift 2',
        descalen4: 'Group Shift 2',
        descalen5: 'Group Shift 2'
      },
      {
        codcalen: 'S3',
        descalene: 'Group Shift 3',
        descalent: 'กลุ่มกะ3',
        descalen3: 'Group Shift 3',
        descalen4: 'Group Shift 3',
        descalen5: 'Group Shift 3'
      },
      {
        codcalen: 'S4',
        descalene: 'Group Shift 4',
        descalent: 'กลุ่มกะ4',
        descalen3: 'Group Shift 4',
        descalen4: 'Group Shift 4',
        descalen5: 'Group Shift 4'
      },
      {
        codcalen: 'S5',
        descalene: 'Group Shift 5',
        descalent: 'กลุ่มกะ5',
        descalen3: 'Group Shift 5',
        descalen4: 'Group Shift 5',
        descalen5: 'Group Shift 5'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodcompy () {
    let mockRows = []
    const mockTotal = 3
    mockRows = mockRows.concat([
      {
        codcompy: '001',
        descompye: 'GKN Driveline Thailand Ltd.',
        descompyt: 'บริษัท จีเคเอ็น ไดรฟไลน์ (ประเทศไทย) จำกัด',
        descompy3: 'GKN Driveline Thailand Ltd.',
        descompy4: 'GKN Driveline Thailand Ltd.',
        descompy5: 'GKN Driveline Thailand Ltd.'
      },
      {
        codcompy: 'NIK',
        descompye: 'Nikon Thailand Co., Ltd.',
        descompyt: 'บริษัท นิโคอิ เนกิ จำกัด',
        descompy3: 'Nikon Thailand Co., Ltd.',
        descompy4: 'Nikon Thailand Co., Ltd.',
        descompy5: 'Nikon Thailand Co., Ltd.'
      },
      {
        codcompy: 'PPS',
        descompye: 'People Plus Software Co.,Ltd.',
        descompyt: 'บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด',
        descompy3: 'People Plus Software Co.,Ltd.',
        descompy4: 'People Plus Software Co.,Ltd.',
        descompy5: 'People Plus Software Co.,Ltd.'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCoduser () {
    let mockRows = []
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        coduser: faker.lorem.word(),
        namempe: faker.name.findName(),
        namempt: faker.name.findName(),
        namemp3: faker.name.findName(),
        namemp4: faker.name.findName(),
        namemp5: faker.name.findName()
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodsecu () {
    let mockRows = []
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codsecu: faker.lorem.word(),
        descsecue: faker.lorem.words(),
        descsecut: faker.lorem.words(),
        descsecu3: faker.lorem.words(),
        descsecu4: faker.lorem.words(),
        descsecu5: faker.lorem.words()
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodproc () {
    let mockRows = []
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codproc: faker.lorem.word(),
        descproce: faker.lorem.words(),
        descproct: faker.lorem.words(),
        descproc3: faker.lorem.words(),
        descproc4: faker.lorem.words(),
        descproc5: faker.lorem.words()
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodincom () {
    let mockRows = []
    const mockTotal = 5
    const mockCodincom = [
      '01',
      '02',
      '03',
      '04',
      '05'
    ]
    const mockDesincom = [
      'เงินเดือน/ค่าแรง',
      'ค่าครองชีพ',
      'ค่าตำแหน่ง',
      'ค่าความสามารถ',
      'ค่าวิชาชีพ'
    ]
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codincom: mockCodincom[i - 1],
        descodincome: mockDesincom[i - 1],
        descodincomt: mockDesincom[i - 1],
        descodincom3: mockDesincom[i - 1],
        descodincom4: mockDesincom[i - 1],
        descodincom5: mockDesincom[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodretro () {
    let mockRows = []
    const mockTotal = 10
    const mockCodretro = [
      '10',
      '11',
      '12',
      '13',
      '14',
      '15',
      '16',
      '17',
      '18',
      '19'
    ]
    const mockDescretro = [
      'จ้างทำของ',
      'ค่าล่วงเวลา',
      'ค่าเบี้ยขยันรายเดือน',
      'ค่าเบี้ยขยันประจำปี',
      'ค่าอาหาร (ทำงานปกติ)',
      'ค่าอาหาร (ล่วงเวลา)',
      'ค่าเบี้ยเลี้ยงพิเศษ',
      'ค่าโทรศัพท์',
      'ค่าอยู่เวรพิเศษ',
      'ค่าความรู้พิเศษ'
    ]
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codretro: mockCodretro[i - 1],
        descodretroe: mockDescretro[i - 1],
        descodretrot: mockDescretro[i - 1],
        descodretro3: mockDescretro[i - 1],
        descodretro4: mockDescretro[i - 1],
        descodretro5: mockDescretro[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtable () {
    let mockRows = []
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtable: faker.lorem.word(),
        descodtablee: faker.lorem.words(),
        descodtablet: faker.lorem.words(),
        descodtable3: faker.lorem.words(),
        descodtable4: faker.lorem.words(),
        descodtable5: faker.lorem.words()
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodval () {
    let mockRows = []
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codval: faker.lorem.word(),
        descodvale: faker.lorem.words(),
        descodvalt: faker.lorem.words(),
        descodval3: faker.lorem.words(),
        descodval4: faker.lorem.words(),
        descodval5: faker.lorem.words()
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCostCenter () {
    let mockRows = []
    const mockTotal = 9
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        costcent: faker.lorem.word(),
        namcente: faker.lorem.words(),
        namcentt: faker.lorem.words(),
        namcent3: faker.lorem.words(),
        namcent4: faker.lorem.words(),
        namcent5: faker.lorem.words()
      })
    }
    mockRows = mockRows.concat([
      {
        costcent: 'PPS00001',
        namcente: 'สาขา1',
        namcentt: 'สาขา1',
        namcent3: 'สาขา1',
        namcent4: 'สาขา1',
        namcent5: 'สาขา1'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovComGroup () {
    let mockRows = []
    const mockTotal = 8
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        compgrp: faker.lorem.word(),
        descode: faker.lorem.words(),
        descodt: faker.lorem.words(),
        descod3: faker.lorem.words(),
        descod4: faker.lorem.words(),
        descod5: faker.lorem.words()
      })
    }
    mockRows = mockRows.concat([
      {
        compgrp: '0001',
        descode: 'กลุ่มบริษัทดูแลลูกค้า',
        descodt: 'กลุ่มบริษัทดูแลลูกค้า',
        descod3: 'กลุ่มบริษัทดูแลลูกค้า',
        descod4: 'กลุ่มบริษัทดูแลลูกค้า',
        descod5: 'กลุ่มบริษัทดูแลลูกค้า'
      },
      {
        compgrp: '0002',
        descode: 'กลุ่มบริษัทการผลิต',
        descodt: 'กลุ่มบริษัทการผลิต',
        descod3: 'กลุ่มบริษัทการผลิต',
        descod4: 'กลุ่มบริษัทการผลิต',
        descod5: 'กลุ่มบริษัทการผลิต'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtency () {
    let mockRows = []
    const mockCodtency = [
      'C001',
      'F001',
      'M001'
    ]
    const mockDescCodtency = [
      'Core Competency',
      'Functional Competency',
      'Management Competency'
    ]
    const mockTotal = 3
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtency: mockCodtency[i - 1],
        descodtencye: mockDescCodtency[i - 1],
        descodtencyt: mockDescCodtency[i - 1],
        descodtency3: mockDescCodtency[i - 1],
        descodtency4: mockDescCodtency[i - 1],
        descodtency5: mockDescCodtency[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodoccup () {
    let mockRows = []
    const mockCodoccup = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010'
    ]
    const mockOccup = [
      'รับราชการ',
      'ค้าขาย',
      'ประมง',
      'รับจ้าง',
      'ธุรกิจส่วนตัว',
      'พนักงานบริษัท',
      'เกษตรกร',
      'นักแสดง/พิธีกร',
      'นักกฎหมาย/การเมือง',
      'วิศวกร'
    ]
    const mockTotal = 7
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codoccup: mockCodoccup[i - 1],
        descodoccupe: mockOccup[i - 1],
        descodoccupt: mockOccup[i - 1],
        descodoccup3: mockOccup[i - 1],
        descodoccup4: mockOccup[i - 1],
        descodoccup5: mockOccup[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodhosp () {
    let mockRows = []
    const mockCodhosp = [
      'H0001',
      'H0002',
      'H0003',
      'H0004',
      'H0005',
      'H0006',
      'H0007',
      'H0008',
      'H0009',
      'H0010',
      'H0011',
      'H0012',
      'H0013'
    ]
    const mockdeschosp = [
      'คณะแพทยศาสตร์วชิรพยาบาล',
      'รพ.จุฬาลงกรณ์',
      'รพ.ตำรวจ',
      'รพ.พระมงกุฎเกล้า',
      'รพ.ภูมิพลอดุลยเดช',
      'รพ.ศิริราช',
      'รพ.สมเด็จพระปิ่นเกล้า',
      'รพ.กล้วยน้ำไท',
      'รพ.เกษมราษฎร์ ประชาชื่น',
      'รพ.เปาโล โชคชัย',
      'รพ.ราษฎร์บูรณะ',
      'รพ.ลาดพร้าว',
      'รพ.สายไหม'
    ]
    const mockTotal = 13
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codhosp: mockCodhosp[i - 1],
        descodhospe: mockdeschosp[i - 1],
        descodhospt: mockdeschosp[i - 1],
        descodhosp3: mockdeschosp[i - 1],
        descodhosp4: mockdeschosp[i - 1],
        descodhosp5: mockdeschosp[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodprov () {
    let mockRows = []
    const mockCodprov = [
      'P0001',
      'P0002',
      'P0003',
      'P0004',
      'P0005',
      'P0006',
      'P0007',
      'P0008',
      'P0009',
      'P0010',
      'P0011',
      'P0012',
      'P0013',
      'P0014',
      'P0015',
      'P0015',
      'P0016',
      'P0017',
      'P0018',
      'P0019',
      'P0020'
    ]
    const mockdescprov = [
      'เชียงใหม่',
      'เชียงราย',
      'แม่ฮ่องสอน',
      'นครราชสีมา',
      'ขอนแก่น',
      'เพชรบูรณ์',
      'ชลบุรี',
      'ระยอง',
      'จันทบุรี',
      'กรุงเทพมหานคร',
      'สมุทรปราการ',
      'ปทุมธานี',
      'กาญจนบุรี',
      'ประจวบคีรีขันธ์',
      'ชุมพร',
      'สุราษฎร์ธานี',
      'นครศรีธรรมราช',
      'กระบี่',
      'ภูเก็ต'
    ]
    const mockTotal = 15
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codprov: mockCodprov[i - 1],
        descodprove: mockdescprov[i - 1],
        descodprovt: mockdescprov[i - 1],
        descodprov3: mockdescprov[i - 1],
        descodprov4: mockdescprov[i - 1],
        descodprov5: mockdescprov[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodcarcab () {
    let mockRows = []
    const mockcodcarcab = [
      'CB0001',
      'CB0002',
      'CB0003',
      'CB0004',
      'CB0005',
      'CB0006',
      'CB0007',
      'CB0008',
      'CB0009',
      'CB0010'
    ]
    const mockdesccarcab = [
      'หมอชิตใหม่ - คลองสาน',
      'สถานีขนส่ง(จตุจักร) - จักรวรรดิ์',
      'คลองขวาง - หัวลำโพง',
      'คลองเตย - ห้วยขวาง',
      'หมอชิต2 - สุรวงศ์',
      'ท่าข้าม - อนุเสาวรีย์ชัยสมรภูมิ',
      'สวนสยาม - สาธุประดิษฐ์',
      'มีนบุรี - อนุสาวรีย์ชัยฯ',
      'มธ.ศูนย์รังสิต - หัวลำโพง',
      'รังสิต - ม.เกษตรศาสตร์'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codcarcab: mockcodcarcab[i - 1],
        descodcarcabe: mockdesccarcab[i - 1],
        descodcarcabt: mockdesccarcab[i - 1],
        descodcarcab3: mockdesccarcab[i - 1],
        descodcarcab4: mockdesccarcab[i - 1],
        descodcarcab5: mockdesccarcab[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodgrpwork () {
    let mockRows = []
    const mockcodgrpwork = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      'S1',
      'S2',
      'S3',
      'S4',
      'S5'
    ]
    const mockdescgrpwork = [
      'General staff of Head office',
      'General staff of Branch',
      'Staff shift 1',
      'Staff shift 2',
      'Shift 2',
      'Group Shift 1',
      'Group Shift 2',
      'Group Shift 3',
      'Group Shift 4',
      'Group Shift 5'
    ]
    const mockdescgrpworkt = [
      'กลุ่มสำนักงาน',
      'กลุ่มสาขา',
      'พนักงานกะ เช้า',
      'พนักงานกะ บ่าย',
      'พนักงานกะ ดึก',
      'กลุ่มกะ1',
      'กลุ่มกะ2',
      'กลุ่มกะ3',
      'กลุ่มกะ4',
      'กลุ่มกะ5'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codgrpwork: mockcodgrpwork[i - 1],
        descodgrpworke: mockdescgrpwork[i - 1],
        descodgrpworkt: mockdescgrpworkt[i - 1],
        descodgrpwork3: mockdescgrpwork[i - 1],
        descodgrpwork4: mockdescgrpwork[i - 1],
        descodgrpwork5: mockdescgrpwork[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtypinc () {
    let mockRows = []
    const mockcodtypinc = [
      '1101',
      '1201',
      '1301',
      '1401',
      '1501'
    ]
    const mockdesctypinc = [
      'เงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป',
      'เงินเดือน ค่าจ้าง ฯลฯ กรณีหักอัตรา ร้อยละ3',
      'เงินได้จ่ายครั้งเดียว',
      'เงินได้มาตรา 40(2) ผู้มีเงินได้อยู่ในประเทศไทย',
      'เงินได้มาตรา 40(2) ผู้มีเงินได้อยู่นอกประเทศ'
    ]
    const mockTotal = 5
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtypinc: mockcodtypinc[i - 1],
        descodtypince: mockdesctypinc[i - 1],
        descodtypinct: mockdesctypinc[i - 1],
        descodtypinc3: mockdesctypinc[i - 1],
        descodtypinc4: mockdesctypinc[i - 1],
        descodtypinc5: mockdesctypinc[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovNumotreq () {
    let mockRows = []
    const mockCodcomp = [
      '15-07-000001',
      '15-07-000002',
      '16-07-000001',
      '16-07-000002',
      '16-07-000003',
      '16-07-000004',
      '16-07-000005',
      '16-07-000006',
      '17-04-000001',
      '17-04-000002'
    ]
    const mockNamempt = [
      'น.ส.ศุภัคสร  เอนก',
      'นายกมล ก่อไผ่น้อย',
      'นายชุติพงษ์ ทองทา',
      'นางฐานิดา ครรชิต',
      'นางฐานิดา ครรชิต',
      'นายวิโรจน์ สมรัตน์',
      'น.ส.ศุภัคสร  เอนก',
      'นายกมล ก่อไผ่น้อย',
      'นายสมศักดิ์  ประกอบ',
      'นายวันชัย สมชาย'
    ]
    const mockNamemp = [
      'Miss.SUPUCSORN  ANEK',
      'MR.KAMON KOPHAINOI',
      'MR.CHUTIPONG THONGTA',
      'Mrs.THANIDA KANCHID',
      'Mrs.THANIDA KANCHID',
      'Mr.VIROJ SOMRAT',
      'Miss.SUPUCSORN  ANEK',
      'MR.KAMON KOPHAINOI',
      'Mr.SOMSAK  PRAKOB',
      'Mr.WANCHAI SOMCHAI'
    ]
    const mockCodcompt = [
      'ฝ่ายขายและการตลาด',
      'แผนกติดตั้ง/ออกแบบ/ตรวจสอบวางแผนพัฒนางาน',
      'แผนกติดตั้ง/ออกแบบ/ตรวจสอบวางแผนพัฒนางาน',
      'ฝ่ายดำเนินการและพัฒนาระบบ',
      'ฝ่ายดำเนินการและพัฒนาระบบ',
      'ฝ่ายการเงิน',
      'ฝ่ายขายและการตลาด',
      'แผนกติดตั้ง/ออกแบบ/ตรวจสอบวางแผนพัฒนางาน',
      'ฝ่ายการเงิน',
      'แผนกติดตั้ง/ออกแบบ/ตรวจสอบวางแผนพัฒนางาน'
    ]
    const mockNamcalen = [
      'พนักงานทั่วไป สำนักงานใหญ่',
      'พนักงานทั่วไป สาขารัชโยธิน',
      'พนักงานกะ 1',
      'พนักงานกะ 2',
      'Office'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        numotreq: mockCodcomp[i - 1],
        namempe: mockNamemp[i - 1],
        namempt: mockNamempt[i - 1],
        namemp3: mockNamemp[i - 1],
        namemp4: mockNamemp[i - 1],
        namemp5: mockNamemp[i - 1],
        namcent: mockCodcompt[i - 1],
        namcalen: mockNamcalen[Math.floor(Math.random() * 4) + 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtyppayr () {
    let mockRows = []
    const mockcodtyppayr = [
      '0101',
      '0201',
      '0210',
      '0230',
      '0301',
      '0401',
      '0501',
      '1101',
      '1201',
      '1301',
      '1401',
      '1501',
      '1601'
    ]
    const mockdesctyppayr = [
      'เงินเดือน/ค่าแรง',
      'ค่าตำแหน่ง',
      'ค่าครองชีพ',
      'ค่าวิชาชีพ',
      'Grade Allowance',
      'Supervisory Support Allow',
      'ค่าเบี้ยเลี้ยงนักศึกษา',
      'ค่าล่วงเวลา',
      'เบี้ยขยัน',
      'ค่ากะ',
      'ค่าอาหาร',
      'ค่าอยู่เวร',
      'ค่าโทรศัพท์'
    ]
    const mockTotal = 15
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtyppayr: mockcodtyppayr[i - 1],
        descodtyppayre: mockdesctyppayr[i - 1],
        descodtyppayrt: mockdesctyppayr[i - 1],
        descodtyppayr3: mockdesctyppayr[i - 1],
        descodtyppayr4: mockdesctyppayr[i - 1],
        descodtyppayr5: mockdesctyppayr[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtyppayt () {
    let mockRows = []
    const mockcodtyppayt = [
      '2101',
      '2201',
      '2301',
      '2401',
      '2501',
      '2601'
    ]
    const mockdesctyppayt = [
      'เงินเดือน ค่าจ้าง โบนัส ฯลฯ มาตรา 40(1)',
      'ค่าธรรมเนียม ค่านายหน้า ฯลฯ มาตรา 40(2)',
      'ค่าลิขสิทธิ์ ฯลฯ มาตรา 40(3)',
      'ดอกเบี้ย เงินปันผล ฯลฯ มาตรา 40(4)',
      'เงินได้ต้องหักภาษี ตาม มาตรา 3 เตรส',
      'เงินได้จ่ายครั้งเดียวออกจากงาน'
    ]
    const mockTotal = 6
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtyppayt: mockcodtyppayt[i - 1],
        descodtyppayte: mockdesctyppayt[i - 1],
        descodtyppaytt: mockdesctyppayt[i - 1],
        descodtyppayt3: mockdesctyppayt[i - 1],
        descodtyppayt4: mockdesctyppayt[i - 1],
        descodtyppayt5: mockdesctyppayt[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodrem () {
    let mockRows = []
    const mockCodrem = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '9999'
    ]
    const mockDescCodrem = [
      'งานนอกเวลา',
      'งานด่วน',
      'งานตามหัวหน้าสั่ง',
      'งานเพิ่ม',
      'งานพิเศษ',
      'N/A'
    ]
    const mockTotal = 6
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codrem: mockCodrem[i - 1],
        descodreme: mockDescCodrem[i - 1],
        descodremt: mockDescCodrem[i - 1],
        descodrem3: mockDescCodrem[i - 1],
        descodrem4: mockDescCodrem[i - 1],
        descodrem5: mockDescCodrem[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtax () {
    let mockRows = []
    const mockcodtax = [
      'Q2',
      'T1',
      'T2',
      'T3',
      'T4',
      'T5',
      'T6',
      'T7',
      'T8',
      'T9',
      'TC',
      'TD'
    ]
    const mockdesccodtax = [
      'Tax ภาษีค่าจ้างนอกระบบ',
      'ภาษีเงินได้',
      'ภาษีค่าล่วงเวลา',
      'ภาษีค่าเบี้ยขยันรายเดือน หัก ณ.ที่จ',
      'ภาษีค่ากะ',
      'ภาษีจ้างทำของ',
      'ภาษีค่าอยู่เวรพิเศษ (18)',
      'ภาษีโบนัส',
      'ภาษีค่า Commision',
      'ภาษีค่านายหน้า',
      'ภาษีเงินปีใหม่',
      'ภาษีเงินได้จ่ายครั้งเดียว'
    ]
    const mockTotal = 12
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtax: mockcodtax[i - 1],
        descodtaxe: mockdesccodtax[i - 1],
        descodtaxt: mockdesccodtax[i - 1],
        descodtax3: mockdesccodtax[i - 1],
        descodtax4: mockdesccodtax[i - 1],
        descodtax5: mockdesccodtax[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovTypeleave () {
    let mockRows = []
    const mockTypeleave = [ '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'H', 'O', 'S', 'T', 'U', 'W', 'X', 'Y', 'Z' ]
    const mockDescTypeleave = [
      'ลาพักผ่อน',
      'ประเภทลา 5 วันต่อครั้ง',
      'ลากิจ',
      'สมาชิกในครอบครัวถึงแก่กรรม',
      'ลาอุปสมบท',
      'ลาสมรส',
      'ลากิจภรรยาลาคลอดบุตร',
      'ลาเกี่ยวกับราชการทหาร',
      'กิจกรรมสหภาพ',
      'Line Stop',
      'ลาป่วย',
      'ลาคลอดบุตร',
      'ลาเพื่อทำหมันและเนื่องจากการทำหมัน',
      'พักร้อนใหม่',
      'ลาชดเชย OT',
      'ลาป่วย หรือ บาดเจ็บ ในการปฏิบัติงาน',
      'ลาพักร้อน'
    ]
    const mockTotal = 6
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        typeleave: mockTypeleave[i - 1],
        destypeleavee: mockDescTypeleave[i - 1],
        destypeleavet: mockDescTypeleave[i - 1],
        destypeleave3: mockDescTypeleave[i - 1],
        destypeleave4: mockDescTypeleave[i - 1],
        destypeleave5: mockDescTypeleave[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodpay () {
    let mockRows = []
    const mockCodpay = [
      '61',
      '62',
      '63',
      '64',
      '81',
      '82',
      '83',
      '84',
      '86',
      '91',
      '92',
      '93',
      '94',
      '95'
    ]
    const mockDescCodpay = [
      'หักเงินกู้',
      'หักกู้ที่อยู่อาศัย',
      'หักทำความผิด',
      'สมาชิกสหภาพ',
      'หักขาดงาน',
      'หักลาเกินกำหนด',
      'หักสาย',
      'หักกลับก่อน',
      'หักบังคับคดี15%',
      'รายการหักอื่นๆ ประจำ (คำนวณภาษี)',
      'รายการหักอื่นๆ ประจำ (ไม่คำนวณภาษี)',
      'รายการหักอื่นๆ ไม่ประจำ (คำนวณภาษี)',
      'รายการหักอื่นๆ ไม่ประจำ (ไม่คำนวณภาษี)',
      'Provident Fund (Company Dis)'
    ]
    const mockTotal = 14
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codpay: mockCodpay[i - 1],
        descodpaye: mockDescCodpay[i - 1],
        descodpayt: mockDescCodpay[i - 1],
        descodpay3: mockDescCodpay[i - 1],
        descodpay4: mockDescCodpay[i - 1],
        descodpay5: mockDescCodpay[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodpayrol () {
    let mockRows = []
    const mockcodpayrol = [
      '0001',
      '0002',
      '0003',
      '01',
      '02',
      '03',
      '04',
      'M1',
      'M2',
      'XXX1'
    ]
    const mockdesccodpayrol = [
      'รายเดือน',
      'รายวัน',
      'นักศึกษา',
      'รายเดือน ผู้บริหาร สำนักงานใหญ่',
      'รายเดือน สำนักงาน ใหญ่',
      'รายเดือน สาขา',
      'รายวัน สาขา',
      'รายได้ประจำเดือนละ 1 งวด',
      'รายได้ประจำเดือนละ 2 งวด',
      'XXXX-รายเดือน-XXXX'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codpayrol: mockcodpayrol[i - 1],
        descodpayrole: mockdesccodpayrol[i - 1],
        descodpayrolt: mockdesccodpayrol[i - 1],
        descodpayrol3: mockdesccodpayrol[i - 1],
        descodpayrol4: mockdesccodpayrol[i - 1],
        descodpayrol5: mockdesccodpayrol[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodfund () {
    let mockRows = []
    const mockcodfund = [
      '0001',
      '0002',
      '0003',
      '004',
      '01',
      '02',
      'KFUN',
      'KTAM',
      'SAS',
      'SCB',
      'TH01',
      'TIS',
      'TT1'
    ]
    const mockdesccodfund = [
      'กองทุนสำรองเลี้ยงชีพ #1',
      'กองทุนสำหรับพนักงานทั่วไป',
      'กองทุนทหารไทย',
      'ธนาคารกรุงเทพ',
      'กองทุน PF ลงทุนในหุ้น 10%',
      'กองทุน PF ลงทุนในหุ้น 25%',
      'ธ.กสิกรไทย',
      'KTAM',
      'กองทุนบริษัท SAS',
      'กองทุนไทยพาณิชย์',
      'กองทุนไทยมั่นคง',
      'Tisco bank',
      'กองทุน PF บ.TT1'
    ]
    const mockTotal = 13
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codfund: mockcodfund[i - 1],
        descodfunde: mockdesccodfund[i - 1],
        descodfundt: mockdesccodfund[i - 1],
        descodfund3: mockdesccodfund[i - 1],
        descodfund4: mockdesccodfund[i - 1],
        descodfund5: mockdesccodfund[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodplan () {
    let mockRows = []
    const mockcodplan = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006'
    ]
    const mockdesccodplan = [
      'แผนการลงทุนในหุ้น 10%',
      'แผนการลงทุนในหุ้น 25%',
      'แผนการลงทุนที่ 1 100%',
      'แผนการลงทุนที่ 2 100%',
      'แผนที่มีความเสี่ยงสูง',
      'แผนที่มีความเสี่ยงต่ำ'
    ]
    const mockTotal = 6
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codplan: mockcodplan[i - 1],
        descodplane: mockdesccodplan[i - 1],
        descodplant: mockdesccodplan[i - 1],
        descodplan3: mockdesccodplan[i - 1],
        descodplan4: mockdesccodplan[i - 1],
        descodplan5: mockdesccodplan[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodcause () {
    let mockRows = []
    const mockcodcause = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '9999'
    ]
    const mockdesccodcause = [
      'งานนอกเวลา',
      'งานด่วน',
      'งานตามหัวหน้าสั่ง',
      'งานเพิ่ม',
      'งานพิเศษ',
      'N/A'
    ]
    const mockTotal = 6
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codcause: mockcodcause[i - 1],
        descodcausee: mockdesccodcause[i - 1],
        descodcauset: mockdesccodcause[i - 1],
        descodcause3: mockdesccodcause[i - 1],
        descodcause4: mockdesccodcause[i - 1],
        descodcause5: mockdesccodcause[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodedlv () {
    let mockRows = []
    const mockcodedlv = [
      '0000',
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010'
    ]
    const mockdesccodedlv = [
      'N/A',
      'ปริญญาเอก',
      'ปริญญาโท',
      'ปริญญาตรี',
      'อนุปริญญา',
      'ประกาศนียบัตรวิชาชีพ (ปวท.)',
      'ประกาศนียบัตรวิชาชีพชั้นสูง (ปวส.)',
      'ประกาศนียบัตรวิชาชีพ (ปวช.)',
      'มัธยมศึกษาตอนปลาย',
      'มัธยมศึกษาตอนต้น',
      'ประถมศึกษา'
    ]
    const mockTotal = 11
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codedlv: mockcodedlv[i - 1],
        descodedlve: mockdesccodedlv[i - 1],
        descodedlvt: mockdesccodedlv[i - 1],
        descodedlv3: mockdesccodedlv[i - 1],
        descodedlv4: mockdesccodedlv[i - 1],
        descodedlv5: mockdesccodedlv[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodmajsb () {
    let mockRows = []
    const mockcodmajsb = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010',
      '0011',
      '0012',
      '0013',
      '0014',
      '0015',
      '0016',
      '0017',
      '0018',
      '0019',
      '0020',
      '0021',
      '0022',
      '0023',
      '0024',
      '0025',
      '0026',
      '0027',
      '0028',
      '0029',
      '0030',
      '0031',
      '0032',
      '0033',
      '0034',
      '0035',
      '0036',
      '0037',
      '0038',
      '0039',
      '0040',
      '0041',
      '0042',
      '0043',
      '0044',
      '0045',
      '0046',
      '0047',
      '0048',
      '0049',
      '0050',
      '0051',
      '9999'
    ]
    const mockdesccodmajsb = [
      'บัญชี',
      'การเงินการธนาคาร',
      'การตลาด',
      'การจัดการ',
      'ธุรกิจต่างประเทศ',
      'ประชาสัมพันธ์',
      'การจัดการการผลิต',
      'เศรษศาสตร์การเงิน',
      'เศรษศาสตร์ มหาภาค',
      'เศรษศาสตร์ระหว่างประเทศ',
      'เศรษศาสตร์การจัดการ',
      'เศรษฐศาสตร์แรงงานและทรัพยากรมนุษย์',
      'เศรษฐศาสตร์ปริมาณวิเคราะห์',
      'เศรษฐศาสตร์การเกษตรและธุรกิจการเกษตร',
      'เศรษฐศาสตร์สหกรณ์',
      'กฎหมาย',
      'กฏหมายธุรกิจ',
      'กฏหมายระหว่างประเทศ',
      'สถิติ',
      'คอมพิวเตอร์',
      'คณิตศาสตร์',
      'วิศวกรรมคอมพิวเตอร์',
      'วิศวกรรมไฟฟ้า',
      'วิศวกรรมอุตสาหกรรม',
      'วิศวกรรมโยธา',
      'วิศวกรรมเครื่องกล',
      'วิศวกรรมสิ่งแวดล้อม',
      'พยาบาล',
      'วิทยุโทรทัศน์',
      'วิทยุกระจายเสียง',
      'โฆษณา',
      'สารนิเทศ',
      'ประชาสัมพันธ์',
      'ภาพยนตร์และวีดีทัศน์',
      'ภาษาอังกฤษ',
      'ภาษาฝรั่งเศส',
      'ภาษาญี่ปุ่น',
      'ภาษาจีน',
      'วรรณคดี',
      'ประถมศึกษา',
      'มัธยมศึกษา',
      'การประเมินผล',
      'การศึกษาผู้ใหญ่',
      'จิตวิทยา',
      'สังคมวิทยาและมนุษยวิทยา',
      'ธุรกิจศึกษา',
      'พละศึกษา',
      'เลขานุการ',
      'รัฐประศาสนศาสตร์',
      'กฏหมาย',
      'ระหว่างประเทศ',
      'อื่นๆ'
    ]
    const mockTotal = 52
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codmajsb: mockcodmajsb[i - 1],
        descodmajsbe: mockdesccodmajsb[i - 1],
        descodmajsbt: mockdesccodmajsb[i - 1],
        descodmajsb3: mockdesccodmajsb[i - 1],
        descodmajsb4: mockdesccodmajsb[i - 1],
        descodmajsb5: mockdesccodmajsb[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodaward () {
    let mockRows = []
    const mockcodaward = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      'T0D1',
      'T0D2',
      'T0M1',
      'T0M2',
      'Z001',
      'Z002'
    ]
    const mockdesccodaward = [
      'เบี้ยขยัน',
      'เงินรางวัล',
      'ค่าตอบแทนพิเศษ',
      'ค่า OSA',
      'เบี้ยขยันประเภท 2',
      'เบี้ยขยันประเภท 3',
      'พนักงานรายวัน G0 หยุด/ลางาน1ครั้งภายในเดือน',
      'พนักงานรายวัน G0 ไม่หยุด/ไม่ลา',
      'พนักงานรายเดือน G1 ทุกสาขา',
      'พนักงานรายเดือน G1-G6 มาทำงานวันหยุด',
      '1.ชุดข้อสอบสำหรับตำแหน่งพนักงานทั่วไป',
      '2.ชุดข้อสอบสำหรับตำแหน่งพนักงานทั่วไปด้านภาษา'
    ]
    const mockTotal = 12
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codaward: mockcodaward[i - 1],
        descodawarde: mockdesccodaward[i - 1],
        descodawardt: mockdesccodaward[i - 1],
        descodaward3: mockdesccodaward[i - 1],
        descodaward4: mockdesccodaward[i - 1],
        descodaward5: mockdesccodaward[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  }
}
