import { instance, isMock, endpoint, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLovLabels () {
    if (isMock) return generator(mocker.lovLabels())
    return instance().get(endpoint + '/hrmsLov/getLabels')
  },
  getLovCodempid () {
    if (isMock) return generator(mocker.lovCodempid())
    return instance().get(endpoint + '/hrmsLov/getCodempid')
  },
  getLovCodpos () {
    if (isMock) return generator(mocker.lovCodpos())
    return instance().get(endpoint + '/hrmsLov/getCodpos')
  },
  getLovCodapp () {
    if (isMock) return generator(mocker.lovCodapp())
    return instance().get(endpoint + '/hrmsLov/getCodapp')
  },
  getLovNummemo () {
    if (isMock) return generator(mocker.lovNummemo())
    return instance().get(endpoint + '/hrmsLov/getNummemo')
  },
  getLovCodcours () {
    if (isMock) return generator(mocker.lovCodcours())
    return instance().get(endpoint + '/hrmsLov/getCodcours')
  },
  getLovNumappl () {
    if (isMock) return generator(mocker.lovNumappl())
    return instance().get(endpoint + '/hrmsLov/getNumappl')
  },
  getLovCodform () {
    if (isMock) return generator(mocker.lovCodform())
    return instance().get(endpoint + '/hrmsLov/getCodform')
  },
  getLovNumreq () {
    if (isMock) return generator(mocker.lovNumreq())
    return instance().get(endpoint + '/hrmsLov/getNumreq')
  },
  getLovCodaplvl () {
    if (isMock) return generator(mocker.lovCodaplvl())
    return instance().get(endpoint + '/hrmsLov/getCodaplvl')
  },
  getLovGrdscor (params) {
    if (isMock) return generator(mocker.lovGrdscor())
    let param = params.p_codempid + '/' + params.p_dteyreap + '/' + params.p_numtime + '/' + params.p_kpino
    return instance().get(endpoint + '/hrmsLov/getGrdscor/' + param)
  },
  getLovCodappr () {
    if (!isMock) return generator(mocker.lovCodappr())
    return instance().get(endpoint + '/hrmsLov/getLovCodappr')
  },
  getLovCodshift () {
    if (isMock) return generator(mocker.lovCodshift())
    return instance().get(endpoint + '/hrmsLov/getCodshift')
  },
  getLovCodroom () {
    if (isMock) return generator(mocker.lovCodroom())
    return instance().get(endpoint + '/hrmsLov/getCodroom')
  },
  getLovCodasset () {
    if (isMock) return generator(mocker.lovCodasset())
    return instance().get(endpoint + '/hrmsLov/getCodasset')
  },
  getLovCodcalen () {
    if (isMock) return generator(mocker.lovCodcalen())
    return instance().get(endpoint + '/hrmsLov/getCodcalen')
  },
  getLovCodcompy () {
    if (isMock) return generator(mocker.lovCodcompy())
    return instance().get(endpoint + '/hrmsLov/getCodcompy')
  },
  getLovCoduser () {
    if (isMock) return generator(mocker.lovCoduser())
    return instance().get(endpoint + '/hrmsLov/getCoduser')
  },
  getLovCodsecu () {
    if (isMock) return generator(mocker.lovCodsecu())
    return instance().get(endpoint + '/hrmsLov/getCodsecu')
  },
  getLovCodproc () {
    if (isMock) return generator(mocker.lovCodproc())
    return instance().get(endpoint + '/hrmsLov/getCodproc')
  },
  getLovCodincom () {
    if (isMock) return generator(mocker.lovCodincom())
    return instance().get(endpoint + '/hrmsLov/getCodincom')
  },
  getLovCodretro () {
    if (isMock) return generator(mocker.lovCodretro())
    return instance().get(endpoint + '/hrmsLov/getCodretro')
  },
  getLovCodtable () {
    if (isMock) return generator(mocker.lovCodtable())
    return instance().get(endpoint + '/hrmsLov/getCodtable')
  },
  getLovCodval () {
    if (isMock) return generator(mocker.lovCodval())
    return instance().get(endpoint + '/hrmsLov/getCodval')
  },
  getLovCostCenter () {
    if (isMock) return generator(mocker.lovCostCenter())
    return instance().get(endpoint + '/hrmsLov/getCostCenter')
  },
  getLovComGroup () {
    if (isMock) return generator(mocker.lovComGroup())
    return instance().get(endpoint + '/hrmsLov/getComGroup')
  },
  getLovCodtency () {
    if (isMock) return generator(mocker.lovCodtency())
    return instance().get(endpoint + '/hrmsLov/getCodtency')
  },
  getLovCodoccup () {
    if (isMock) return generator(mocker.lovCodoccup())
    return instance().get(endpoint + '/hrmsLov/getCodoccup')
  },
  getLovCodhosp () {
    if (isMock) return generator(mocker.lovCodhosp())
    return instance().get(endpoint + '/hrmsLov/getCodhosp')
  },
  getLovCodprov () {
    if (isMock) return generator(mocker.lovCodprov())
    return instance().get(endpoint + '/hrmsLov/getCodprov')
  },
  getLovCodgrpwork () {
    if (isMock) return generator(mocker.lovCodgrpwork())
    return instance().get(endpoint + '/hrmsLov/getCodgrpwork')
  },
  getLovCodcarcab () {
    if (isMock) return generator(mocker.lovCodcarcab())
    return instance().get(endpoint + '/hrmsLov/getCodcarcab')
  },
  getLovCodtypinc () {
    if (isMock) return generator(mocker.lovCodtypinc())
    return instance().get(endpoint + '/hrmsLov/getCodtypinc')
  },
  getLovCodtyppayr () {
    if (isMock) return generator(mocker.lovCodtyppayr())
    return instance().get(endpoint + '/hrmsLov/getCodtyppayr')
  },
  getLovCodtyppayt () {
    if (isMock) return generator(mocker.lovCodtyppayt())
    return instance().get(endpoint + '/hrmsLov/getCodtyppayt')
  },
  getLovCodtax () {
    if (isMock) return generator(mocker.lovCodtax())
    return instance().get(endpoint + '/hrmsLov/getCodtax')
  },
  getLovNumotreq () {
    if (isMock) return generator(mocker.lovNumotreq())
    return instance().get(endpoint + '/hrmsLov/getNumotreq')
  },
  getLovCodrem () {
    if (isMock) return generator(mocker.lovCodrem())
    return instance().get(endpoint + '/hrmsLov/getCodrem')
  },
  getLovTypeleave () {
    if (isMock) return generator(mocker.lovTypeleave())
    return instance().get(endpoint + '/hrmsLov/getTypeleave')
  },
  getLovCodpay () {
    if (isMock) return generator(mocker.lovCodpay())
    return instance().get(endpoint + '/hrmsLov/getCodpay')
  },
  getLovCodpayrol () {
    if (isMock) return generator(mocker.lovCodpayrol())
    return instance().get(endpoint + '/hrmsLov/getCodpayrol')
  },
  getLovCodfund () {
    if (isMock) return generator(mocker.lovCodfund())
    return instance().get(endpoint + '/hrmsLov/getCodfund')
  },
  getLovCodplan () {
    if (isMock) return generator(mocker.lovCodplan())
    return instance().get(endpoint + '/hrmsLov/getCodplan')
  },
  getLovCodcause () {
    if (isMock) return generator(mocker.lovCodcause())
    return instance().get(endpoint + '/hrmsLov/getCodcause')
  },
  getLovCodedlv () {
    if (isMock) return generator(mocker.lovCodedlv())
    return instance().get(endpoint + '/hrmsLov/getCodedlv')
  },
  getLovCodmajsb () {
    if (isMock) return generator(mocker.lovCodmajsb())
    return instance().get(endpoint + '/hrmsLov/getCodmajsb')
  },
  getLovCodaward () {
    if (isMock) return generator(mocker.lovCodaward())
    return instance().get(endpoint + '/hrmsLov/getCodaward')
  }
}
