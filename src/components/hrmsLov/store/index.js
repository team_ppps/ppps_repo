import * as types from './mutation-types'
import hrmsLov from '../api'
import swal from 'sweetalert'
import hrmsLovLabels from '../assets/labels'
import { hrmsLovColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'

export default {
  state: {
    popupLov: {
      codempid: { total: 0, rows: [], loading: false, finish: false },
      codpos: { total: 0, rows: [], loading: false, finish: false },
      codapp: { total: 0, rows: [], loading: false, finish: false },
      nummemo: { total: 0, rows: [], loading: false, finish: false },
      codcours: { total: 0, rows: [], loading: false, finish: false },
      codform: { total: 0, rows: [], loading: false, finish: false },
      numappl: { total: 0, rows: [], loading: false, finish: false },
      numreq: { total: 0, rows: [], loading: false, finish: false },
      codaplvl: { total: 0, rows: [], loading: false, finish: false },
      grdscor: { total: 0, rows: [], loading: false, finish: false },
      codappr: { total: 0, rows: [], loading: false, finish: false },
      codshift: { total: 0, rows: [], loading: false, finish: false },
      codroom: { total: 0, rows: [], loading: false, finish: false },
      codasset: { total: 0, rows: [], loading: false, finish: false },
      codcalen: { total: 0, rows: [], loading: false, finish: false },
      codcompy: { total: 0, rows: [], loading: false, finish: false },
      coduser: { total: 0, rows: [], loading: false, finish: false },
      codsecu: { total: 0, rows: [], loading: false, finish: false },
      codproc: { total: 0, rows: [], loading: false, finish: false },
      codincom: { total: 0, rows: [], loading: false, finish: false },
      codretro: { total: 0, rows: [], loading: false, finish: false },
      codtable: { total: 0, rows: [], loading: false, finish: false },
      codval: { total: 0, rows: [], loading: false, finish: false },
      costcent: { total: 0, rows: [], loading: false, finish: false },
      compgrp: { total: 0, rows: [], loading: false, finish: false },
      codtency: { total: 0, rows: [], loading: false, finish: false },
      codoccup: { total: 0, rows: [], loading: false, finish: false },
      codhosp: { total: 0, rows: [], loading: false, finish: false },
      codprov: { total: 0, rows: [], loading: false, finish: false },
      codgrpwork: { total: 0, rows: [], loading: false, finish: false },
      codcarcab: { total: 0, rows: [], loading: false, finish: false },
      codtypinc: { total: 0, rows: [], loading: false, finish: false },
      codtyppayr: { total: 0, rows: [], loading: false, finish: false },
      codtyppayt: { total: 0, rows: [], loading: false, finish: false },
      codtax: { total: 0, rows: [], loading: false, finish: false },
      numotreq: { total: 0, rows: [], loading: false, finish: false },
      codrem: { total: 0, rows: [], loading: false, finish: false },
      typeleave: { total: 0, rows: [], loading: false, finish: false },
      codpay: { total: 0, rows: [], loading: false, finish: false },
      codpayrol: { total: 0, rows: [], loading: false, finish: false },
      codfund: { total: 0, rows: [], loading: false, finish: false },
      codplan: { total: 0, rows: [], loading: false, finish: false },
      codcause: { total: 0, rows: [], loading: false, finish: false },
      codedlv: { total: 0, rows: [], loading: false, finish: false },
      codmajsb: { total: 0, rows: [], loading: false, finish: false },
      codaward: { total: 0, rows: [], loading: false, finish: false },
      lang: window.localStorage.getItem('lang')
    },
    popupLovTitle: {
      codempid: 'List of Employee',
      codpos: 'List of Position',
      codapp: 'List of Application',
      nummemo: 'List of Memo',
      codcours: 'List of Course',
      codform: 'List of Form',
      numappl: 'List of Application Number',
      numreq: 'List of Request',
      codaplvl: 'List of Appraisal Group',
      grdscor: 'List of Grade Score',
      codappr: 'List of Approval',
      codshift: 'List of Shift',
      codroom: 'List of Room',
      codasset: 'List of Asset',
      codcalen: 'List of Group',
      codcompy: 'List of Company',
      coduser: 'List of User Name',
      codsecu: 'List of Security Group',
      codproc: 'List of Work Process',
      codincom: 'List of Fixed Income Code',
      codretro: 'List Of Other Income Code',
      codtable: 'List Of Table Code',
      codval: 'List Of Value Code',
      costcent: 'List Of Cost Center',
      compgrp: 'List Of Company Group',
      codtency: 'Type of Special Skills/Exp.',
      codoccup: 'List Of Occupation',
      codhosp: 'List Of Hospital',
      codprov: 'List Of Province',
      codgrpwork: 'List Of Group Work',
      codcarcab: 'List Of Car Cable',
      codtypinc: 'List Of Type Inc',
      codtyppayr: 'List Of Pay Slip',
      codtyppayt: 'List Of Tax Certificate',
      codtax: 'List Of Tax Code',
      numotreq: 'No. of OT Request',
      codrem: 'Reasons for Overtime Request',
      typeleave: 'List of Type of Leave',
      codpay: 'List of Deduct Code ',
      codpayrol: 'List Of Payroll Code',
      codfund: 'List Of Fund Code',
      codplan: 'List Of Plan Code',
      codcause: 'List Of Cause Code',
      codedlv: 'List Of Education Code',
      codmajsb: 'List Of Major Code',
      codaward: 'List Of Award Code'
    },
    lovCurrentDesc: function (type, code) {
      let rowIndex = this.$store.getters[types.GET_LOV][type].rows.map(function (lov) {
        return lov[type]
      }).indexOf(code)
      if (rowIndex >= 0) {
        if (typeof this.$store.getters[types.GET_LOV][type].rows[rowIndex] !== 'undefined') {
          return this.$store.getters[types.GET_LOV][type].rows[rowIndex]['desc_' + type]
        }
      }
      return ''
    },
    columnsLabelsCalled: false,
    popupLovLabels: hrmsLovLabels,
    popupLovColumns: hrmsLovColumns()
  },
  getters: {
    [types.GET_LOV_COLUMNS] (state) {
      return state.popupLovColumns
    },
    [types.GET_LOV_LABELS] (state) {
      return state.popupLovLabels
    },
    [types.GET_LOV] (state) {
      return state.popupLov
    },
    [types.GET_LOV_TITLE] (state) {
      return state.popupLovTitle
    },
    [types.GET_LOV_CURRENT_DESC] (state) {
      return state.lovCurrentDesc
    }
  },
  mutations: {
    [types.SET_LOV_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.popupLovColumns[keyLang].codempid = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codempid, labels[keyLang])
        state.popupLovColumns[keyLang].codpos = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codpos, labels[keyLang])
        state.popupLovColumns[keyLang].codapp = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codapp, labels[keyLang])
        state.popupLovColumns[keyLang].nummemo = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].nummemo, labels[keyLang])
        state.popupLovColumns[keyLang].codcours = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codcours, labels[keyLang])
        state.popupLovColumns[keyLang].codform = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codform, labels[keyLang])
        state.popupLovColumns[keyLang].numappl = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].numappl, labels[keyLang])
        state.popupLovColumns[keyLang].numreq = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].numreq, labels[keyLang])
        state.popupLovColumns[keyLang].codaplvl = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codaplvl, labels[keyLang])
        state.popupLovColumns[keyLang].grdscor = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].grdscor, labels[keyLang])
        state.popupLovColumns[keyLang].codshift = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codshift, labels[keyLang])
        state.popupLovColumns[keyLang].codroom = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codroom, labels[keyLang])
        state.popupLovColumns[keyLang].codasset = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codasset, labels[keyLang])
        state.popupLovColumns[keyLang].codcalen = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codcalen, labels[keyLang])
        state.popupLovColumns[keyLang].coduser = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].coduser, labels[keyLang])
        state.popupLovColumns[keyLang].codsecu = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codsecu, labels[keyLang])
        state.popupLovColumns[keyLang].codproc = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codproc, labels[keyLang])
        state.popupLovColumns[keyLang].codincom = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codincom, labels[keyLang])
        state.popupLovColumns[keyLang].codretro = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codretro, labels[keyLang])
        state.popupLovColumns[keyLang].codtable = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codtable, labels[keyLang])
        state.popupLovColumns[keyLang].codval = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codval, labels[keyLang])
        state.popupLovColumns[keyLang].costcent = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].costcent, labels[keyLang])
        state.popupLovColumns[keyLang].compgrp = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].compgrp, labels[keyLang])
        state.popupLovColumns[keyLang].codtency = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codtency, labels[keyLang])
        state.popupLovColumns[keyLang].codoccup = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codoccup, labels[keyLang])
        state.popupLovColumns[keyLang].codhosp = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codhosp, labels[keyLang])
        state.popupLovColumns[keyLang].codprov = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codprov, labels[keyLang])
        state.popupLovColumns[keyLang].codgrpwork = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codgrpwork, labels[keyLang])
        state.popupLovColumns[keyLang].codcarcab = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codcarcab, labels[keyLang])
        state.popupLovColumns[keyLang].codtypinc = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codtypinc, labels[keyLang])
        state.popupLovColumns[keyLang].codtyppayr = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codtyppayr, labels[keyLang])
        state.popupLovColumns[keyLang].codtyppayt = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codtyppayt, labels[keyLang])
        state.popupLovColumns[keyLang].codtax = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codtax, labels[keyLang])
        state.popupLovColumns[keyLang].numotreq = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].numotreq, labels[keyLang])
        state.popupLovColumns[keyLang].codrem = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codrem, labels[keyLang])
        state.popupLovColumns[keyLang].typeleave = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].typeleave, labels[keyLang])
        state.popupLovColumns[keyLang].codpay = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codpay, labels[keyLang])
        state.popupLovColumns[keyLang].codpayrol = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codpayrol, labels[keyLang])
        state.popupLovColumns[keyLang].codfund = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codfund, labels[keyLang])
        state.popupLovColumns[keyLang].codplan = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codplan, labels[keyLang])
        state.popupLovColumns[keyLang].codcause = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codcause, labels[keyLang])
        state.popupLovColumns[keyLang].codedlv = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codedlv, labels[keyLang])
        state.popupLovColumns[keyLang].codmajsb = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codmajsb, labels[keyLang])
        state.popupLovColumns[keyLang].codaward = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codaward, labels[keyLang])
        // state.popupLovColumns[keyLang].codappr = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codappr, labels[keyLang])
        // state.popupLovColumns[keyLang].codcompy = assetsLabels.replaceLabelToColumns(state.popupLovColumns[keyLang].codcompy, labels[keyLang])
      }
    },
    [types.SET_LOV_LABELS] (state, labels) {
      state.popupLovLabels = labels
    },
    [types.SET_LOV_COLUMNS_LABELS_CALLED] (state, columnsLabelsCalled) {
      state.columnsLabelsCalled = columnsLabelsCalled
    },
    [types.SET_LOV_CODEMPID] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codempid = lov.namempe
            break
          case 'th':
            popupLov.rows[i].desc_codempid = lov.namempt
            break
          case '103':
            popupLov.rows[i].desc_codempid = lov.namemp3
            break
          case '104':
            popupLov.rows[i].desc_codempid = lov.namemp4
            break
          case '105':
            popupLov.rows[i].desc_codempid = lov.namemp5
            break
        }
      }
      state.popupLov.codempid = popupLov
    },
    [types.SET_LOV_CODPOS] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codpos = lov.nampose
            break
          case 'th':
            popupLov.rows[i].desc_codpos = lov.nampost
            break
          case '103':
            popupLov.rows[i].desc_codpos = lov.nampos3
            break
          case '104':
            popupLov.rows[i].desc_codpos = lov.nampos4
            break
          case '105':
            popupLov.rows[i].desc_codpos = lov.nampos5
            break
        }
      }
      state.popupLov.codpos = popupLov
    },
    [types.SET_LOV_CODAPP] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codapp = lov.namappe
            break
          case 'th':
            popupLov.rows[i].desc_codapp = lov.namappt
            break
          case '103':
            popupLov.rows[i].desc_codapp = lov.namapp3
            break
          case '104':
            popupLov.rows[i].desc_codapp = lov.namapp4
            break
          case '105':
            popupLov.rows[i].desc_codapp = lov.namapp5
            break
        }
      }
      state.popupLov.codapp = popupLov
    },
    [types.SET_LOV_NUMMEMO] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].dtemonth = assetsLabels.month['en'][parseInt(lov.dtemonth) - 1].full
            popupLov.rows[i].namcours = lov.namcourse
            break
          case 'th':
            popupLov.rows[i].dtemonth = assetsLabels.month['th'][parseInt(lov.dtemonth) - 1].full
            popupLov.rows[i].namcours = lov.namcourst
            break
          case '103':
            popupLov.rows[i].dtemonth = assetsLabels.month['103'][parseInt(lov.dtemonth) - 1].full
            popupLov.rows[i].namcours = lov.namcours3
            break
          case '104':
            popupLov.rows[i].dtemonth = assetsLabels.month['104'][parseInt(lov.dtemonth) - 1].full
            popupLov.rows[i].namcours = lov.namcours4
            break
          case '105':
            popupLov.rows[i].dtemonth = assetsLabels.month['105'][parseInt(lov.dtemonth) - 1].full
            popupLov.rows[i].namcours = lov.namcours5
            break
        }
      }
      state.popupLov.nummemo = popupLov
    },
    [types.SET_LOV_CODCOURS] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codcours = lov.namcourse
            break
          case 'th':
            popupLov.rows[i].desc_codcours = lov.namcourst
            break
          case '103':
            popupLov.rows[i].desc_codcours = lov.namcours3
            break
          case '104':
            popupLov.rows[i].desc_codcours = lov.namcours4
            break
          case '105':
            popupLov.rows[i].desc_codcours = lov.namcours5
            break
        }
      }
      state.popupLov.codcours = popupLov
    },
    [types.SET_LOV_CODFORM] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codform = lov.desforme
            break
          case 'th':
            popupLov.rows[i].desc_codform = lov.desformt
            break
          case '103':
            popupLov.rows[i].desc_codform = lov.desform3
            break
          case '104':
            popupLov.rows[i].desc_codform = lov.desform4
            break
          case '105':
            popupLov.rows[i].desc_codform = lov.desform5
            break
        }
      }
      state.popupLov.codform = popupLov
    },
    [types.SET_LOV_NUMAPPL] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_numappl = lov.desapple
            break
          case 'th':
            popupLov.rows[i].desc_numappl = lov.desapplt
            break
          case '103':
            popupLov.rows[i].desc_numappl = lov.desappl3
            break
          case '104':
            popupLov.rows[i].desc_numappl = lov.desappl4
            break
          case '105':
            popupLov.rows[i].desc_numappl = lov.desappl5
            break
        }
      }
      state.popupLov.numappl = popupLov
    },
    [types.SET_LOV_NUMREQ] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codpos = lov.nampose
            break
          case 'th':
            popupLov.rows[i].desc_codpos = lov.nampost
            break
          case '103':
            popupLov.rows[i].desc_codpos = lov.nampos3
            break
          case '104':
            popupLov.rows[i].desc_codpos = lov.nampos4
            break
          case '105':
            popupLov.rows[i].desc_codpos = lov.nampos5
            break
        }
        popupLov.rows[i].desc_numreq = lov.numreq
      }
      state.popupLov.numreq = popupLov
    },
    [types.SET_LOV_APLV] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codaplvl = lov.descode
            break
          case 'th':
            popupLov.rows[i].desc_codaplvl = lov.descodt
            break
          case '103':
            popupLov.rows[i].desc_codaplvl = lov.descod3
            break
          case '104':
            popupLov.rows[i].desc_codaplvl = lov.descod4
            break
          case '105':
            popupLov.rows[i].desc_codaplvl = lov.descod5
            break
        }
      }
      state.popupLov.codaplvl = popupLov
    },
    [types.SET_LOV_GRDSCOR] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        popupLov.rows[i].desc_grdscor = lov.qtyscor
      }
      state.popupLov.grdscor = popupLov
    },
    [types.SET_LOV_CODAPPR] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codempid = lov.namempe
            break
          case 'th':
            popupLov.rows[i].desc_codempid = lov.namempt
            break
          case '103':
            popupLov.rows[i].desc_codempid = lov.namemp3
            break
          case '104':
            popupLov.rows[i].desc_codempid = lov.namemp4
            break
          case '105':
            popupLov.rows[i].desc_codempid = lov.namemp5
            break
        }
      }
      state.popupLov.codappr = popupLov
    },
    [types.SET_LOV_CODCALEN] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codcalen = lov.descalene
            break
          case 'th':
            popupLov.rows[i].desc_codcalen = lov.descalent
            break
          case '103':
            popupLov.rows[i].desc_codcalen = lov.descalen3
            break
          case '104':
            popupLov.rows[i].desc_codcalen = lov.descalen4
            break
          case '105':
            popupLov.rows[i].desc_codcalen = lov.descalen5
            break
        }
      }
      state.popupLov.codcalen = popupLov
    },
    [types.SET_LOV_CODCOMPY] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codcompy = lov.descompye
            break
          case 'th':
            popupLov.rows[i].desc_codcompy = lov.descompyt
            break
          case '103':
            popupLov.rows[i].desc_codcompy = lov.descompy3
            break
          case '104':
            popupLov.rows[i].desc_codcompy = lov.descompy4
            break
          case '105':
            popupLov.rows[i].desc_codcompy = lov.descompy5
            break
        }
      }
      state.popupLov.codcompy = popupLov
    },
    [types.SET_LOV_CODSHIFT] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codshift = lov.desshifte
            break
          case 'th':
            popupLov.rows[i].desc_codshift = lov.desshiftt
            break
          case '103':
            popupLov.rows[i].desc_codshift = lov.desshift3
            break
          case '104':
            popupLov.rows[i].desc_codshift = lov.desshift4
            break
          case '105':
            popupLov.rows[i].desc_codshift = lov.desshift5
            break
        }
      }
      state.popupLov.codshift = popupLov
    },
    [types.SET_LOV_CODROOM] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codroom = lov.roomname
            break
          case 'th':
            popupLov.rows[i].desc_codroom = lov.roomnamt
            break
          case '103':
            popupLov.rows[i].desc_codroom = lov.roomnam3
            break
          case '104':
            popupLov.rows[i].desc_codroom = lov.roomnam4
            break
          case '105':
            popupLov.rows[i].desc_codroom = lov.roomnam5
            break
        }
      }
      state.popupLov.codroom = popupLov
    },
    [types.SET_LOV_CODASSET] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codasset = lov.desassee
            break
          case 'th':
            popupLov.rows[i].desc_codasset = lov.desasset
            break
          case '103':
            popupLov.rows[i].desc_codasset = lov.desasse3
            break
          case '104':
            popupLov.rows[i].desc_codasset = lov.desasse4
            break
          case '105':
            popupLov.rows[i].desc_codasset = lov.desasse5
            break
        }
      }
      state.popupLov.codasset = popupLov
    },
    [types.SET_LOV_CODUSER] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_coduser = lov.namempe
            break
          case 'th':
            popupLov.rows[i].desc_coduser = lov.namempt
            break
          case '103':
            popupLov.rows[i].desc_coduser = lov.namemp3
            break
          case '104':
            popupLov.rows[i].desc_coduser = lov.namemp4
            break
          case '105':
            popupLov.rows[i].desc_coduser = lov.namemp5
            break
        }
      }
      state.popupLov.coduser = popupLov
    },
    [types.SET_LOV_CODSECU] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codsecu = lov.descsecue
            break
          case 'th':
            popupLov.rows[i].desc_codsecu = lov.descsecut
            break
          case '103':
            popupLov.rows[i].desc_codsecu = lov.descsecu3
            break
          case '104':
            popupLov.rows[i].desc_codsecu = lov.descsecu4
            break
          case '105':
            popupLov.rows[i].desc_codsecu = lov.descsecu5
            break
        }
      }
      state.popupLov.codsecu = popupLov
    },
    [types.SET_LOV_CODPROC] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codproc = lov.descproce
            break
          case 'th':
            popupLov.rows[i].desc_codproc = lov.descproct
            break
          case '103':
            popupLov.rows[i].desc_codproc = lov.descproc3
            break
          case '104':
            popupLov.rows[i].desc_codproc = lov.descproc4
            break
          case '105':
            popupLov.rows[i].desc_codproc = lov.descproc5
            break
        }
      }
      state.popupLov.codproc = popupLov
    },
    [types.SET_LOV_CODINCOM] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codincom = lov.descodincome
            break
          case 'th':
            popupLov.rows[i].desc_codincom = lov.descodincomt
            break
          case '103':
            popupLov.rows[i].desc_codincom = lov.descodincom3
            break
          case '104':
            popupLov.rows[i].desc_codincom = lov.descodincom4
            break
          case '105':
            popupLov.rows[i].desc_codincom = lov.descodincom5
            break
        }
      }
      state.popupLov.codincom = popupLov
    },
    [types.SET_LOV_CODRETRO] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codretro = lov.descodretroe
            break
          case 'th':
            popupLov.rows[i].desc_codretro = lov.descodretrot
            break
          case '103':
            popupLov.rows[i].desc_codretro = lov.descodretro3
            break
          case '104':
            popupLov.rows[i].desc_codretro = lov.descodretro4
            break
          case '105':
            popupLov.rows[i].desc_codretro = lov.descodretro5
            break
        }
      }
      state.popupLov.codretro = popupLov
    },
    [types.SET_LOV_CODTABLE] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codtable = lov.descodtablee
            break
          case 'th':
            popupLov.rows[i].desc_codtable = lov.descodtablet
            break
          case '103':
            popupLov.rows[i].desc_codtable = lov.descodtable3
            break
          case '104':
            popupLov.rows[i].desc_codtable = lov.descodtable4
            break
          case '105':
            popupLov.rows[i].desc_codtable = lov.descodtable5
            break
        }
      }
      state.popupLov.codtable = popupLov
    },
    [types.SET_LOV_CODVAL] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codval = lov.descodvale
            break
          case 'th':
            popupLov.rows[i].desc_codval = lov.descodvalt
            break
          case '103':
            popupLov.rows[i].desc_codval = lov.descodval3
            break
          case '104':
            popupLov.rows[i].desc_codval = lov.descodval4
            break
          case '105':
            popupLov.rows[i].desc_codval = lov.descodval5
            break
        }
      }
      state.popupLov.codval = popupLov
    },
    [types.SET_LOV_COSTCENT] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_costcent = lov.namcente
            break
          case 'th':
            popupLov.rows[i].desc_costcent = lov.namcentt
            break
          case '103':
            popupLov.rows[i].desc_costcent = lov.namcent3
            break
          case '104':
            popupLov.rows[i].desc_costcent = lov.namcent4
            break
          case '105':
            popupLov.rows[i].desc_costcent = lov.namcent5
            break
        }
      }
      state.popupLov.costcent = popupLov
    },
    [types.SET_LOV_COMPGRP] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_compgrp = lov.descode
            break
          case 'th':
            popupLov.rows[i].desc_compgrp = lov.descodt
            break
          case '103':
            popupLov.rows[i].desc_compgrp = lov.descod3
            break
          case '104':
            popupLov.rows[i].desc_compgrp = lov.descod4
            break
          case '105':
            popupLov.rows[i].desc_compgrp = lov.descod5
            break
        }
      }
      state.popupLov.compgrp = popupLov
    },
    [types.SET_LOV_CODTENCY] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codtency = lov.descodtencye
            break
          case 'th':
            popupLov.rows[i].desc_codtency = lov.descodtencyt
            break
          case '103':
            popupLov.rows[i].desc_codtency = lov.descodtency3
            break
          case '104':
            popupLov.rows[i].desc_codtency = lov.descodtency4
            break
          case '105':
            popupLov.rows[i].desc_codtency = lov.descodtency5
            break
        }
      }
      state.popupLov.codtency = popupLov
    },
    [types.SET_LOV_CODOCCUP] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codoccup = lov.descodoccupe
            break
          case 'th':
            popupLov.rows[i].desc_codoccup = lov.descodoccupt
            break
          case '103':
            popupLov.rows[i].desc_codoccup = lov.descodoccup3
            break
          case '104':
            popupLov.rows[i].desc_codoccup = lov.descodoccup4
            break
          case '105':
            popupLov.rows[i].desc_codoccup = lov.descodoccup5
            break
        }
      }
      state.popupLov.codoccup = popupLov
    },
    [types.SET_LOV_CODHOSP] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codhosp = lov.descodhospe
            break
          case 'th':
            popupLov.rows[i].desc_codhosp = lov.descodhospt
            break
          case '103':
            popupLov.rows[i].desc_codhosp = lov.descodhosp3
            break
          case '104':
            popupLov.rows[i].desc_codhosp = lov.descodhosp4
            break
          case '105':
            popupLov.rows[i].desc_codhosp = lov.descodhosp5
            break
        }
      }
      state.popupLov.codhosp = popupLov
    },
    [types.SET_LOV_CODPROV] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codprov = lov.descodprove
            break
          case 'th':
            popupLov.rows[i].desc_codprov = lov.descodprovt
            break
          case '103':
            popupLov.rows[i].desc_codprov = lov.descodprov3
            break
          case '104':
            popupLov.rows[i].desc_codprov = lov.descodprov4
            break
          case '105':
            popupLov.rows[i].desc_codprov = lov.descodprov5
            break
        }
      }
      state.popupLov.codprov = popupLov
    },
    [types.SET_LOV_CODGRPWORK] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codgrpwork = lov.descodgrpworke
            break
          case 'th':
            popupLov.rows[i].desc_codgrpwork = lov.descodgrpworkt
            break
          case '103':
            popupLov.rows[i].desc_codgrpwork = lov.descodgrpwork3
            break
          case '104':
            popupLov.rows[i].desc_codgrpwork = lov.descodgrpwork4
            break
          case '105':
            popupLov.rows[i].desc_codgrpwork = lov.descodgrpwork5
            break
        }
      }
      state.popupLov.codgrpwork = popupLov
    },
    [types.SET_LOV_CODCARCAB] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codcarcab = lov.descodcarcabe
            break
          case 'th':
            popupLov.rows[i].desc_codcarcab = lov.descodcarcabt
            break
          case '103':
            popupLov.rows[i].desc_codcarcab = lov.descodcarcab3
            break
          case '104':
            popupLov.rows[i].desc_codcarcab = lov.descodcarcab4
            break
          case '105':
            popupLov.rows[i].desc_codcarcab = lov.descodcarcab5
            break
        }
      }
      state.popupLov.codcarcab = popupLov
    },
    [types.SET_LOV_CODTYPINC] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codtypinc = lov.descodtypince
            break
          case 'th':
            popupLov.rows[i].desc_codtypinc = lov.descodtypinct
            break
          case '103':
            popupLov.rows[i].desc_codtypinc = lov.descodtypinc3
            break
          case '104':
            popupLov.rows[i].desc_codtypinc = lov.descodtypinc4
            break
          case '105':
            popupLov.rows[i].desc_codtypinc = lov.descodtypinc5
            break
        }
      }
      state.popupLov.codtypinc = popupLov
    },
    [types.SET_LOV_CODTYPPAYR] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codtyppayr = lov.descodtyppayre
            break
          case 'th':
            popupLov.rows[i].desc_codtyppayr = lov.descodtyppayrt
            break
          case '103':
            popupLov.rows[i].desc_codtyppayr = lov.descodtyppayr3
            break
          case '104':
            popupLov.rows[i].desc_codtyppayr = lov.descodtyppayr4
            break
          case '105':
            popupLov.rows[i].desc_codtyppayr = lov.descodtyppayr5
            break
        }
      }
      state.popupLov.codtyppayr = popupLov
    },
    [types.SET_LOV_CODTYPPAYT] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codtyppayt = lov.descodtyppayte
            break
          case 'th':
            popupLov.rows[i].desc_codtyppayt = lov.descodtyppaytt
            break
          case '103':
            popupLov.rows[i].desc_codtyppayt = lov.descodtyppayt3
            break
          case '104':
            popupLov.rows[i].desc_codtyppayt = lov.descodtyppayt4
            break
          case '105':
            popupLov.rows[i].desc_codtyppayt = lov.descodtyppayt5
            break
        }
      }
      state.popupLov.codtyppayt = popupLov
    },
    [types.SET_LOV_CODTAX] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codtax = lov.descodtaxe
            break
          case 'th':
            popupLov.rows[i].desc_codtax = lov.descodtaxt
            break
          case '103':
            popupLov.rows[i].desc_codtax = lov.descodtax3
            break
          case '104':
            popupLov.rows[i].desc_codtax = lov.descodtax4
            break
          case '105':
            popupLov.rows[i].desc_codtax = lov.descodtax5
            break
        }
      }
      state.popupLov.codtax = popupLov
    },
    [types.SET_LOV_NUMOTREQ] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codempid = lov.namempe
            break
          case 'th':
            popupLov.rows[i].desc_codempid = lov.namempt
            break
          case '103':
            popupLov.rows[i].desc_codempid = lov.namemp3
            break
          case '104':
            popupLov.rows[i].desc_codempid = lov.namemp4
            break
          case '105':
            popupLov.rows[i].desc_codempid = lov.namemp5
            break
        }
      }
      state.popupLov.numotreq = popupLov
    },
    [types.SET_LOV_CODREM] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codrem = lov.descodreme
            break
          case 'th':
            popupLov.rows[i].desc_codrem = lov.descodremt
            break
          case '103':
            popupLov.rows[i].desc_codrem = lov.descodrem3
            break
          case '104':
            popupLov.rows[i].desc_codrem = lov.descodrem4
            break
          case '105':
            popupLov.rows[i].desc_codrem = lov.descodrem5
            break
        }
      }
      state.popupLov.codrem = popupLov
    },
    [types.SET_LOV_TYPELEAVE] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_typeleave = lov.destypeleavee
            break
          case 'th':
            popupLov.rows[i].desc_typeleave = lov.destypeleavet
            break
          case '103':
            popupLov.rows[i].desc_typeleave = lov.destypeleave3
            break
          case '104':
            popupLov.rows[i].desc_typeleave = lov.destypeleave4
            break
          case '105':
            popupLov.rows[i].desc_typeleave = lov.destypeleave5
            break
        }
      }
      state.popupLov.typeleave = popupLov
    },
    [types.SET_LOV_CODPAY] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codpay = lov.descodpaye
            break
          case 'th':
            popupLov.rows[i].desc_codpay = lov.descodpayt
            break
          case '103':
            popupLov.rows[i].desc_codpay = lov.descodpay3
            break
          case '104':
            popupLov.rows[i].desc_codpay = lov.descodpay4
            break
          case '105':
            popupLov.rows[i].desc_codpay = lov.descodpay5
            break
        }
      }
      state.popupLov.codpay = popupLov
    },
    [types.SET_LOV_CODPAYROL] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codpayrol = lov.descodpayrole
            break
          case 'th':
            popupLov.rows[i].desc_codpayrol = lov.descodpayrolt
            break
          case '103':
            popupLov.rows[i].desc_codpayrol = lov.descodpayrol3
            break
          case '104':
            popupLov.rows[i].desc_codpayrol = lov.descodpayrol4
            break
          case '105':
            popupLov.rows[i].desc_codpayrol = lov.descodpayrol5
            break
        }
      }
      state.popupLov.codpayrol = popupLov
    },
    [types.SET_LOV_CODFUND] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codfund = lov.descodfunde
            break
          case 'th':
            popupLov.rows[i].desc_codfund = lov.descodfundt
            break
          case '103':
            popupLov.rows[i].desc_codfund = lov.descodfund3
            break
          case '104':
            popupLov.rows[i].desc_codfund = lov.descodfund4
            break
          case '105':
            popupLov.rows[i].desc_codfund = lov.descodfund5
            break
        }
      }
      state.popupLov.codfund = popupLov
    },
    [types.SET_LOV_CODPLAN] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codplan = lov.descodplane
            break
          case 'th':
            popupLov.rows[i].desc_codplan = lov.descodplant
            break
          case '103':
            popupLov.rows[i].desc_codplan = lov.descodplan3
            break
          case '104':
            popupLov.rows[i].desc_codplan = lov.descodplan4
            break
          case '105':
            popupLov.rows[i].desc_codplan = lov.descodplan5
            break
        }
      }
      state.popupLov.codplan = popupLov
    },
    [types.SET_LOV_CODCAUSE] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codcause = lov.descodcausee
            break
          case 'th':
            popupLov.rows[i].desc_codcause = lov.descodcauset
            break
          case '103':
            popupLov.rows[i].desc_codcause = lov.descodcause3
            break
          case '104':
            popupLov.rows[i].desc_codcause = lov.descodcause4
            break
          case '105':
            popupLov.rows[i].desc_codcause = lov.descodcause5
            break
        }
      }
      state.popupLov.codcause = popupLov
    },
    [types.SET_LOV_CODEDLV] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codedlv = lov.descodedlve
            break
          case 'th':
            popupLov.rows[i].desc_codedlv = lov.descodedlvt
            break
          case '103':
            popupLov.rows[i].desc_codedlv = lov.descodedlv3
            break
          case '104':
            popupLov.rows[i].desc_codedlv = lov.descodedlv4
            break
          case '105':
            popupLov.rows[i].desc_codedlv = lov.descodedlv5
            break
        }
      }
      state.popupLov.codedlv = popupLov
    },
    [types.SET_LOV_CODMAJSB] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codmajsb = lov.descodmajsbe
            break
          case 'th':
            popupLov.rows[i].desc_codmajsb = lov.descodmajsbt
            break
          case '103':
            popupLov.rows[i].desc_codmajsb = lov.descodmajsb3
            break
          case '104':
            popupLov.rows[i].desc_codmajsb = lov.descodmajsb4
            break
          case '105':
            popupLov.rows[i].desc_codmajsb = lov.descodmajsb5
            break
        }
      }
      state.popupLov.codmajsb = popupLov
    },
    [types.SET_LOV_CODAWARD] (state, popupLov) {
      let nowLang = window.localStorage.getItem('lang')
      popupLov.lang = nowLang
      for (var i = 0; i < popupLov.rows.length; i++) {
        let lov = popupLov.rows[i]
        switch (nowLang) {
          case 'en':
            popupLov.rows[i].desc_codaward = lov.descodawarde
            break
          case 'th':
            popupLov.rows[i].desc_codaward = lov.descodawardt
            break
          case '103':
            popupLov.rows[i].desc_codaward = lov.descodaward3
            break
          case '104':
            popupLov.rows[i].desc_codaward = lov.descodaward4
            break
          case '105':
            popupLov.rows[i].desc_codaward = lov.descodaward5
            break
        }
      }
      state.popupLov.codaward = popupLov
    }
  },
  actions: {
    [types.RECEIVED_LOV_COLUMNS_LABELS] ({ state, commit }, lovType) {
      if (!state.columnsLabelsCalled) {
        commit(types.SET_LOV_COLUMNS_LABELS_CALLED, true)
        hrmsLov.getLovLabels()
          .then((response) => {
            const data = JSON.parse(response.request.response)
            commit(types.SET_LOV_COLUMNS, data.labels)
            commit(types.SET_LOV_LABELS, data.labels)
          })
          .catch((error) => {
            let message = error
            if (error.response) {
              const data = error.response.data
              message = data.response
            }
            swal({title: '', text: message, html: true, type: 'error'})
          })
      }
    },
    [types.RECEIVED_LOV] ({ state, commit }, { lovType, params }) {
      switch (lovType.toUpperCase()) {
        case 'CODEMPID':
          if (!state.popupLov.codempid.finish) {
            if (!state.popupLov.codempid.loading) {
              commit(types.SET_LOV_CODEMPID, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodempid()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODEMPID, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODEMPID, { total: state.popupLov.codempid.total, rows: state.popupLov.codempid.rows, loading: false, finish: true })
          }
          break
        case 'CODPOS':
          if (!state.popupLov.codpos.finish) {
            if (!state.popupLov.codpos.loading) {
              commit(types.SET_LOV_CODPOS, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodpos()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODPOS, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODPOS, { total: state.popupLov.codpos.total, rows: state.popupLov.codpos.rows, loading: false, finish: true })
          }
          break
        case 'CODAPP':
          if (!state.popupLov.codapp.finish) {
            if (!state.popupLov.codapp.loading) {
              commit(types.SET_LOV_CODAPP, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodapp()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODAPP, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODAPP, { total: state.popupLov.codapp.total, rows: state.popupLov.codapp.rows, loading: false, finish: true })
          }
          break
        case 'NUMMEMO':
          if (!state.popupLov.nummemo.finish) {
            if (!state.popupLov.nummemo.loading) {
              commit(types.SET_LOV_NUMMEMO, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovNummemo()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_NUMMEMO, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_NUMMEMO, { total: state.popupLov.nummemo.total, rows: state.popupLov.nummemo.rows, loading: false, finish: true })
          }
          break
        case 'CODCOURS':
          if (!state.popupLov.codcours.finish) {
            if (!state.popupLov.codcours.loading) {
              commit(types.SET_LOV_CODCOURS, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodcours()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODCOURS, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODCOURS, { total: state.popupLov.codcours.total, rows: state.popupLov.codcours.rows, loading: false, finish: true })
          }
          break
        case 'CODFORM':
          if (!state.popupLov.codform.finish) {
            if (!state.popupLov.codform.loading) {
              commit(types.SET_LOV_CODFORM, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodform()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODFORM, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODFORM, { total: state.popupLov.codform.total, rows: state.popupLov.codform.rows, loading: false, finish: true })
          }
          break
        case 'NUMAPPL':
          if (!state.popupLov.numappl.finish) {
            if (!state.popupLov.numappl.loading) {
              commit(types.SET_LOV_NUMAPPL, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovNumappl()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_NUMAPPL, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_NUMAPPL, { total: state.popupLov.numappl.total, rows: state.popupLov.numappl.rows, loading: false, finish: true })
          }
          break
        case 'NUMREQ':
          if (!state.popupLov.numreq.finish) {
            if (!state.popupLov.numreq.loading) {
              commit(types.SET_LOV_NUMREQ, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovNumreq()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_NUMREQ, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_NUMREQ, { total: state.popupLov.numreq.total, rows: state.popupLov.numreq.rows, loading: false, finish: true })
          }
          break
        case 'CODAPLVL':
          if (!state.popupLov.codaplvl.finish) {
            if (!state.popupLov.codaplvl.loading) {
              commit(types.SET_LOV_APLV, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodaplvl()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_APLV, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_APLV, { total: state.popupLov.codaplvl.total, rows: state.popupLov.codaplvl.rows, loading: false, finish: true })
          }
          break
        case 'GRDSCOR':
          if (!state.popupLov.grdscor.finish) {
            if (!state.popupLov.grdscor.loading) {
              commit(types.SET_LOV_GRDSCOR, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovGrdscor(params)
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_GRDSCOR, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_GRDSCOR, { total: state.popupLov.grdscor.total, rows: state.popupLov.grdscor.rows, loading: false, finish: true })
          }
          break
        case 'CODAPPR':
          if (!state.popupLov.codappr.finish) {
            if (!state.popupLov.codappr.loading) {
              commit(types.SET_LOV_CODAPPR, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodappr()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODAPPR, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODAPPR, { total: state.popupLov.codappr.total, rows: state.popupLov.codappr.rows, loading: false, finish: true })
          }
          break
        case 'CODCALEN':
          if (!state.popupLov.codcalen.finish) {
            if (!state.popupLov.codcalen.loading) {
              commit(types.SET_LOV_CODCALEN, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodcalen()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODCALEN, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODCALEN, { total: state.popupLov.codcalen.total, rows: state.popupLov.codcalen.rows, loading: false, finish: true })
          }
          break
        case 'CODSHIFT':
          if (!state.popupLov.codshift.finish) {
            if (!state.popupLov.codshift.loading) {
              commit(types.SET_LOV_CODSHIFT, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodshift()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODSHIFT, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODSHIFT, { total: state.popupLov.codshift.total, rows: state.popupLov.codshift.rows, loading: false, finish: true })
          }
          break
        case 'CODROOM':
          if (!state.popupLov.codroom.finish) {
            if (!state.popupLov.codroom.loading) {
              commit(types.SET_LOV_CODROOM, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodroom()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODROOM, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODROOM, { total: state.popupLov.codroom.total, rows: state.popupLov.codroom.rows, loading: false, finish: true })
          }
          break
        case 'CODASSET':
          if (!state.popupLov.codasset.finish) {
            if (!state.popupLov.codasset.loading) {
              commit(types.SET_LOV_CODASSET, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodasset()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODASSET, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODASSET, { total: state.popupLov.codasset.total, rows: state.popupLov.codasset.rows, loading: false, finish: true })
          }
          break
        case 'CODCOMPY':
          if (!state.popupLov.codcompy.finish) {
            if (!state.popupLov.codcompy.loading) {
              commit(types.SET_LOV_CODCOMPY, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodcompy()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODCOMPY, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODCOMPY, { total: state.popupLov.codcompy.total, rows: state.popupLov.codcompy.rows, loading: false, finish: true })
          }
          break
        case 'CODUSER':
          if (!state.popupLov.coduser.finish) {
            if (!state.popupLov.coduser.loading) {
              commit(types.SET_LOV_CODUSER, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCoduser()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODUSER, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODUSER, { total: state.popupLov.coduser.total, rows: state.popupLov.coduser.rows, loading: false, finish: true })
          }
          break
        case 'CODSECU':
          if (!state.popupLov.codsecu.finish) {
            if (!state.popupLov.codsecu.loading) {
              commit(types.SET_LOV_CODSECU, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodsecu()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODSECU, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODSECU, { total: state.popupLov.codsecu.total, rows: state.popupLov.codsecu.rows, loading: false, finish: true })
          }
          break
        case 'CODPROC':
          if (!state.popupLov.codproc.finish) {
            if (!state.popupLov.codproc.loading) {
              commit(types.SET_LOV_CODPROC, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodproc()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODPROC, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODPROC, { total: state.popupLov.codproc.total, rows: state.popupLov.codproc.rows, loading: false, finish: true })
          }
          break
        case 'CODINCOM':
          if (!state.popupLov.codincom.finish) {
            if (!state.popupLov.codincom.loading) {
              commit(types.SET_LOV_CODINCOM, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodincom()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODINCOM, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODINCOM, { total: state.popupLov.codincom.total, rows: state.popupLov.codincom.rows, loading: false, finish: true })
          }
          break
        case 'CODRETRO':
          if (!state.popupLov.codretro.finish) {
            if (!state.popupLov.codretro.loading) {
              commit(types.SET_LOV_CODRETRO, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodretro()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODRETRO, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODRETRO, { total: state.popupLov.codretro.total, rows: state.popupLov.codretro.rows, loading: false, finish: true })
          }
          break
        case 'CODTABLE':
          if (!state.popupLov.codtable.finish) {
            if (!state.popupLov.codtable.loading) {
              commit(types.SET_LOV_CODTABLE, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodtable()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODTABLE, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODTABLE, { total: state.popupLov.codtable.total, rows: state.popupLov.codtable.rows, loading: false, finish: true })
          }
          break
        case 'CODVAL':
          if (!state.popupLov.codval.finish) {
            if (!state.popupLov.codval.loading) {
              commit(types.SET_LOV_CODVAL, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodval()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODVAL, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODVAL, { total: state.popupLov.codval.total, rows: state.popupLov.codval.rows, loading: false, finish: true })
          }
          break
        case 'COSTCENT':
          if (!state.popupLov.costcent.finish) {
            if (!state.popupLov.costcent.loading) {
              commit(types.SET_LOV_COSTCENT, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCostCenter()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_COSTCENT, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_COSTCENT, { total: state.popupLov.costcent.total, rows: state.popupLov.costcent.rows, loading: false, finish: true })
          }
          break
        case 'COMPGRP':
          if (!state.popupLov.compgrp.finish) {
            if (!state.popupLov.compgrp.loading) {
              commit(types.SET_LOV_COMPGRP, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovComGroup()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_COMPGRP, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_COMPGRP, { total: state.popupLov.compgrp.total, rows: state.popupLov.compgrp.rows, loading: false, finish: true })
          }
          break
        case 'CODTENCY':
          if (!state.popupLov.codtency.finish) {
            if (!state.popupLov.codtency.loading) {
              commit(types.SET_LOV_CODTENCY, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodtency()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODTENCY, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODTENCY, { total: state.popupLov.codtency.total, rows: state.popupLov.codtency.rows, loading: false, finish: true })
          }
          break
        case 'CODOCCUP':
          if (!state.popupLov.codoccup.finish) {
            if (!state.popupLov.codoccup.loading) {
              commit(types.SET_LOV_CODOCCUP, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodoccup()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODOCCUP, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODOCCUP, { total: state.popupLov.codoccup.total, rows: state.popupLov.codoccup.rows, loading: false, finish: true })
          }
          break
        case 'CODHOSP':
          if (!state.popupLov.codhosp.finish) {
            if (!state.popupLov.codhosp.loading) {
              commit(types.SET_LOV_CODHOSP, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodhosp()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODHOSP, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODHOSP, { total: state.popupLov.codhosp.total, rows: state.popupLov.codhosp.rows, loading: false, finish: true })
          }
          break
        case 'CODPROV':
          if (!state.popupLov.codprov.finish) {
            if (!state.popupLov.codprov.loading) {
              commit(types.SET_LOV_CODPROV, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodprov()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODPROV, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODPROV, { total: state.popupLov.codprov.total, rows: state.popupLov.codprov.rows, loading: false, finish: true })
          }
          break
        case 'CODGRPWORK':
          if (!state.popupLov.codgrpwork.finish) {
            if (!state.popupLov.codgrpwork.loading) {
              commit(types.SET_LOV_CODGRPWORK, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodgrpwork()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODGRPWORK, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODGRPWORK, { total: state.popupLov.codgrpwork.total, rows: state.popupLov.codgrpwork.rows, loading: false, finish: true })
          }
          break
        case 'CODCARCAB':
          if (!state.popupLov.codcarcab.finish) {
            if (!state.popupLov.codcarcab.loading) {
              commit(types.SET_LOV_CODCARCAB, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodcarcab()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODCARCAB, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODCARCAB, { total: state.popupLov.codcarcab.total, rows: state.popupLov.codcarcab.rows, loading: false, finish: true })
          }
          break
        case 'CODTYPINC':
          if (!state.popupLov.codtypinc.finish) {
            if (!state.popupLov.codtypinc.loading) {
              commit(types.SET_LOV_CODTYPINC, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodtypinc()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODTYPINC, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODTYPINC, { total: state.popupLov.codtypinc.total, rows: state.popupLov.codtypinc.rows, loading: false, finish: true })
          }
          break
        case 'CODTYPPAYR':
          if (!state.popupLov.codtyppayr.finish) {
            if (!state.popupLov.codtyppayr.loading) {
              commit(types.SET_LOV_CODTYPPAYR, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodtyppayr()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODTYPPAYR, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODTYPPAYR, { total: state.popupLov.codtyppayr.total, rows: state.popupLov.codtyppayr.rows, loading: false, finish: true })
          }
          break
        case 'NUMOTREQ':
          if (!state.popupLov.numotreq.finish) {
            if (!state.popupLov.numotreq.loading) {
              commit(types.SET_LOV_NUMOTREQ, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovNumotreq()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_NUMOTREQ, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_NUMOTREQ, { total: state.popupLov.numotreq.total, rows: state.popupLov.numotreq.rows, loading: false, finish: true })
          }
          break
        case 'CODREM':
          if (!state.popupLov.codrem.finish) {
            if (!state.popupLov.codrem.loading) {
              commit(types.SET_LOV_CODREM, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodrem()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODREM, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODREM, { total: state.popupLov.codrem.total, rows: state.popupLov.codrem.rows, loading: false, finish: true })
          }
          break
        case 'CODTYPPAYT':
          if (!state.popupLov.codtyppayt.finish) {
            if (!state.popupLov.codtyppayt.loading) {
              commit(types.SET_LOV_CODTYPPAYT, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodtyppayt()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODTYPPAYT, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODTYPPAYT, { total: state.popupLov.codtyppayt.total, rows: state.popupLov.codtyppayt.rows, loading: false, finish: true })
          }
          break
        case 'CODTAX':
          if (!state.popupLov.codtax.finish) {
            if (!state.popupLov.codtax.loading) {
              commit(types.SET_LOV_CODTAX, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodtax()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODTAX, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODTAX, { total: state.popupLov.codtax.total, rows: state.popupLov.codtax.rows, loading: false, finish: true })
          }
          break
        case 'TYPELEAVE':
          if (!state.popupLov.typeleave.finish) {
            if (!state.popupLov.typeleave.loading) {
              commit(types.SET_LOV_TYPELEAVE, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovTypeleave()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_TYPELEAVE, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_TYPELEAVE, { total: state.popupLov.typeleave.total, rows: state.popupLov.typeleave.rows, loading: false, finish: true })
          }
          break
        case 'CODPAY':
          if (!state.popupLov.codpay.finish) {
            if (!state.popupLov.codpay.loading) {
              commit(types.SET_LOV_CODPAY, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodpay()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODPAY, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODPAY, { total: state.popupLov.codpay.total, rows: state.popupLov.codpay.rows, loading: false, finish: true })
          }
          break
        case 'CODPAYROL':
          if (!state.popupLov.codpayrol.finish) {
            if (!state.popupLov.codpayrol.loading) {
              commit(types.SET_LOV_CODPAYROL, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodpayrol()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODPAYROL, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODPAYROL, { total: state.popupLov.codpayrol.total, rows: state.popupLov.codpayrol.rows, loading: false, finish: true })
          }
          break
        case 'CODFUND':
          if (!state.popupLov.codfund.finish) {
            if (!state.popupLov.codfund.loading) {
              commit(types.SET_LOV_CODFUND, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodfund()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODFUND, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODFUND, { total: state.popupLov.codfund.total, rows: state.popupLov.codfund.rows, loading: false, finish: true })
          }
          break
        case 'CODPLAN':
          if (!state.popupLov.codplan.finish) {
            if (!state.popupLov.codplan.loading) {
              commit(types.SET_LOV_CODPLAN, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodplan()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODPLAN, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODPLAN, { total: state.popupLov.codplan.total, rows: state.popupLov.codplan.rows, loading: false, finish: true })
          }
          break
        case 'CODCAUSE':
          if (!state.popupLov.codcause.finish) {
            if (!state.popupLov.codcause.loading) {
              commit(types.SET_LOV_CODCAUSE, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodcause()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODCAUSE, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODCAUSE, { total: state.popupLov.codcause.total, rows: state.popupLov.codcause.rows, loading: false, finish: true })
          }
          break
        case 'CODEDLV':
          if (!state.popupLov.codedlv.finish) {
            if (!state.popupLov.codedlv.loading) {
              commit(types.SET_LOV_CODEDLV, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodedlv()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODEDLV, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODEDLV, { total: state.popupLov.codedlv.total, rows: state.popupLov.codedlv.rows, loading: false, finish: true })
          }
          break
        case 'CODMAJSB':
          if (!state.popupLov.codmajsb.finish) {
            if (!state.popupLov.codmajsb.loading) {
              commit(types.SET_LOV_CODMAJSB, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodmajsb()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODMAJSB, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODMAJSB, { total: state.popupLov.codmajsb.total, rows: state.popupLov.codmajsb.rows, loading: false, finish: true })
          }
          break
        case 'CODAWARD':
          if (!state.popupLov.codaward.finish) {
            if (!state.popupLov.codaward.loading) {
              commit(types.SET_LOV_CODAWARD, { total: 0, rows: [], loading: true, finish: false })
              hrmsLov.getLovCodaward()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_LOV_CODAWARD, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.popupLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_LOV_CODAWARD, { total: state.popupLov.codaward.total, rows: state.popupLov.codaward.rows, loading: false, finish: true })
          }
          break
      }
    }
  }
}
