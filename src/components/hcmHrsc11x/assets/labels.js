module.exports = {
  'en': {
    'HRSC11X': {
      '10': 'Use from date',
      '20': 'Use to date'
    }
  },
  'th': {
    'HRSC11X': {
      '10': 'วันที่ใช้งานตั้งแต่',
      '20': 'ถึงวันที่ใช้งาน'
    }
  }
}
