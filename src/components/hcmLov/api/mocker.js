import faker from 'faker'
// import moment from 'moment'
import hcmLovLabels from '../assets/labels'

export const mocker = {
  lovLabels () {
    return {
      labels: hcmLovLabels
    }
  },
  lovCodempid () {
    let mockRows = []
    const mockTotal = 20
    mockRows = mockRows.concat([
      {
        codempid: '53100',
        namempe: 'นายกง กิต',
        namempt: 'นายกง กิต',
        namemp3: 'นายกง กิต',
        namemp4: 'นายกง กิต',
        namemp5: 'นายกง กิต',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '54003',
        namempe: 'น.ส.กชกร ผลถาวร',
        namempt: 'น.ส.กชกร ผลถาวร',
        namemp3: 'น.ส.กชกร ผลถาวร',
        namemp4: 'น.ส.กชกร ผลถาวร',
        namemp5: 'น.ส.กชกร ผลถาวร',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '57004',
        namempe: 'น.ส.กนกวรรณ พันธ์ประสิทธิ์',
        namempt: 'น.ส.กนกวรรณ พันธ์ประสิทธิ์',
        namemp3: 'น.ส.กนกวรรณ พันธ์ประสิทธิ์',
        namemp4: 'น.ส.กนกวรรณ พันธ์ประสิทธิ์',
        namemp5: 'น.ส.กนกวรรณ พันธ์ประสิทธิ์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '38055',
        namempe: 'นายขจรศักดิ์  บรรลือศักดิ์',
        namempt: 'นายขจรศักดิ์  บรรลือศักดิ์',
        namemp3: 'นายขจรศักดิ์  บรรลือศักดิ์',
        namemp4: 'นายขจรศักดิ์  บรรลือศักดิ์',
        namemp5: 'นายขจรศักดิ์  บรรลือศักดิ์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '54190',
        namempe: 'นายคง ชินนอก',
        namempt: 'นายคง ชินนอก',
        namemp3: 'นายคง ชินนอก',
        namemp4: 'นายคง ชินนอก',
        namemp5: 'นายคง ชินนอก',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '53074',
        namempe: 'นายคชภัค ฮวดศรีคณากร',
        namempt: 'นายคชภัค ฮวดศรีคณากร',
        namemp3: 'นายคชภัค ฮวดศรีคณากร',
        namemp4: 'นายคชภัค ฮวดศรีคณากร',
        namemp5: 'นายคชภัค ฮวดศรีคณากร',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '37010',
        namempe: 'นายจตุฤทธิ์ เสงี่ยม',
        namempt: 'นายจตุฤทธิ์ เสงี่ยม',
        namemp3: 'นายจตุฤทธิ์ เสงี่ยม',
        namemp4: 'นายจตุฤทธิ์ เสงี่ยม',
        namemp5: 'นายจตุฤทธิ์ เสงี่ยม',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '33172',
        namempe: 'นายจรัญ  คงวุฒิ',
        namempt: 'นายจรัญ  คงวุฒิ',
        namemp3: 'นายจรัญ  คงวุฒิ',
        namemp4: 'นายจรัญ  คงวุฒิ',
        namemp5: 'นายจรัญ  คงวุฒิ',
        desstaemp: 'ทดลองงาน',
        staemp: ''
      },
      {
        codempid: '37033',
        namempe: 'นายชรินทร์  ชัชวาล',
        namempt: 'นายชรินทร์  ชัชวาล',
        namemp3: 'นายชรินทร์  ชัชวาล',
        namemp4: 'นายชรินทร์  ชัชวาล',
        namemp5: 'นายชรินทร์  ชัชวาล',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '55154',
        namempe: 'นายชรินทร์ น้อยมา',
        namempt: 'นายชรินทร์ น้อยมา',
        namemp3: 'นายชรินทร์ น้อยมา',
        namemp4: 'นายชรินทร์ น้อยมา',
        namemp5: 'นายชรินทร์ น้อยมา',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '55155',
        namempe: 'นายชัยฤทธิ์ จุลศรี',
        namempt: 'นายชัยฤทธิ์ จุลศรี',
        namemp3: 'นายชัยฤทธิ์ จุลศรี',
        namemp4: 'นายชัยฤทธิ์ จุลศรี',
        namemp5: 'นายชัยฤทธิ์ จุลศรี',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '50059',
        namempe: 'นายชัยวัฒน์  คำภีร์',
        namempt: 'นายชัยวัฒน์  คำภีร์',
        namemp3: 'นายชัยวัฒน์  คำภีร์',
        namemp4: 'นายชัยวัฒน์  คำภีร์',
        namemp5: 'นายชัยวัฒน์  คำภีร์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '54114',
        namempe: 'นายชุติพงษ์ ทองทา',
        namempt: 'นายชุติพงษ์ ทองทา',
        namemp3: 'นายชุติพงษ์ ทองทา',
        namemp4: 'นายชุติพงษ์ ทองทา',
        namemp5: 'นายชุติพงษ์ ทองทา',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '52100',
        namempe: 'นางฐานิดา ครรชิต',
        namempt: 'นางฐานิดา ครรชิต',
        namemp3: 'นางฐานิดา ครรชิต',
        namemp4: 'นางฐานิดา ครรชิต',
        namemp5: 'นางฐานิดา ครรชิต',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '53185',
        namempe: 'นายวิโรจน์ สมรัตน์',
        namempt: 'นายวิโรจน์ สมรัตน์',
        namemp3: 'นายวิโรจน์ สมรัตน์',
        namemp4: 'นายวิโรจน์ สมรัตน์',
        namemp5: 'นายวิโรจน์ สมรัตน์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '55290',
        namempe: 'นายศราวุธ ทองเภา',
        namempt: 'นายศราวุธ ทองเภา',
        namemp3: 'นายศราวุธ ทองเภา',
        namemp4: 'นายศราวุธ ทองเภา',
        namemp5: 'นายศราวุธ ทองเภา',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '55382',
        namempe: 'นายสงวน บรรดาศักดิ์',
        namempt: 'นายสงวน บรรดาศักดิ์',
        namemp3: 'นายสงวน บรรดาศักดิ์',
        namemp4: 'นายสงวน บรรดาศักดิ์',
        namemp5: 'นายสงวน บรรดาศักดิ์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '16010',
        namempe: 'นายสมชาย เข็มกลัดทองแดง',
        namempt: 'นายสมชาย เข็มกลัดทองแดง',
        namemp3: 'นายสมชาย เข็มกลัดทองแดง',
        namemp4: 'นายสมชาย เข็มกลัดทองแดง',
        namemp5: 'นายสมชาย เข็มกลัดทองแดง',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '50165',
        namempe: 'นายสมศักดิ์ สำรวยศักดิ์',
        namempt: 'นายสมศักดิ์ สำรวยศักดิ์',
        namemp3: 'นายสมศักดิ์ สำรวยศักดิ์',
        namemp4: 'นายสมศักดิ์ สำรวยศักดิ์',
        namemp5: 'นายสมศักดิ์ สำรวยศักดิ์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      },
      {
        codempid: '53144',
        namempe: 'นายประเสริฐ  สมศักดิ์',
        namempt: 'นายประเสริฐ  สมศักดิ์',
        namemp3: 'นายประเสริฐ  สมศักดิ์',
        namemp4: 'นายประเสริฐ  สมศักดิ์',
        namemp5: 'นายประเสริฐ  สมศักดิ์',
        desstaemp: 'ปัจจุบัน',
        staemp: ''
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodpos () {
    let mockRows = []
    const mockTotal = 30
    mockRows = mockRows.concat([
      {
        codpos: '0100',
        nampose: 'Managing director',
        nampost: 'Managing director',
        nampos3: 'Managing director',
        nampos4: 'Managing director',
        nampos5: 'Managing director'
      },
      {
        codpos: '0110',
        nampose: 'Advisor',
        nampost: 'Advisor',
        nampos3: 'Advisor',
        nampos4: 'Advisor',
        nampos5: 'Advisor'
      },
      {
        codpos: '0120',
        nampose: 'Secretary',
        nampost: 'Secretary',
        nampos3: 'Secretary',
        nampos4: 'Secretary',
        nampos5: 'Secretary'
      },
      {
        codpos: '0200',
        nampose: 'Marketing Manager',
        nampost: 'Marketing Manager',
        nampos3: 'Marketing Manager',
        nampos4: 'Marketing Manager',
        nampos5: 'Marketing Manager'
      },
      {
        codpos: '0300',
        nampose: 'Technical Manager',
        nampost: 'Technical Manager',
        nampos3: 'Technical Manager',
        nampos4: 'Technical Manager',
        nampos5: 'Technical Manager'
      },
      {
        codpos: '0310',
        nampose: 'Project Manager',
        nampost: 'Project Manager',
        nampos3: 'Project Manager',
        nampos4: 'Project Manager',
        nampos5: 'Project Manager'
      },
      {
        codpos: '0320',
        nampose: 'Support Manager',
        nampost: 'Support Manager',
        nampos3: 'Support Manager',
        nampos4: 'Support Manager',
        nampos5: 'Support Manager'
      },
      {
        codpos: '0330',
        nampose: 'Development Manager',
        nampost: 'Development Manager',
        nampos3: 'Development Manager',
        nampos4: 'Development Manager',
        nampos5: 'Development Manager'
      },
      {
        codpos: '0340',
        nampose: 'HR Manager',
        nampost: 'HR Manager',
        nampos3: 'HR Manager',
        nampos4: 'HR Manager',
        nampos5: 'HR Manager'
      },
      {
        codpos: '0510',
        nampose: 'Project Leader',
        nampost: 'Project Leader',
        nampos3: 'Project Leader',
        nampos4: 'Project Leader',
        nampos5: 'Project Leader'
      },
      {
        codpos: '0520',
        nampose: 'Sr. Sale',
        nampost: 'Sr. Sale',
        nampos3: 'Sr. Sale',
        nampos4: 'Sr. Sale',
        nampos5: 'Sr. Sale'
      },
      {
        codpos: '0530',
        nampose: 'Sr. Maketing',
        nampost: 'Sr. Maketing',
        nampos3: 'Sr. Maketing',
        nampos4: 'Sr. Maketing',
        nampos5: 'Sr. Maketing'
      },
      {
        codpos: '0600',
        nampose: 'Test Leader',
        nampost: 'Test Leader',
        nampos3: 'Test Leader',
        nampos4: 'Test Leader',
        nampos5: 'Test Leader'
      },
      {
        codpos: '0605',
        nampose: 'Sale',
        nampost: 'Sale',
        nampos3: 'Sale',
        nampos4: 'Sale',
        nampos5: 'Sale'
      },
      {
        codpos: '0610',
        nampose: 'Assistant Project Leader',
        nampost: 'Assistant Project Leader',
        nampos3: 'Assistant Project Leader',
        nampos4: 'Assistant Project Leader',
        nampos5: 'Assistant Project Leader'
      },
      {
        codpos: '0620',
        nampose: 'Senior Customer Support',
        nampost: 'Senior Customer Support',
        nampos3: 'Senior Customer Support',
        nampos4: 'Senior Customer Support',
        nampos5: 'Senior Customer Support'
      },
      {
        codpos: '0630',
        nampose: 'Senior System Analyst',
        nampost: 'Senior System Analyst',
        nampos3: 'Senior System Analyst',
        nampos4: 'Senior System Analyst',
        nampos5: 'Senior System Analyst'
      },
      {
        codpos: '0640',
        nampose: 'System Engineer',
        nampost: 'System Engineer',
        nampos3: 'System Engineer',
        nampos4: 'System Engineer',
        nampos5: 'System Engineer'
      },
      {
        codpos: '0650',
        nampose: 'System Analyst',
        nampost: 'System Analyst',
        nampos3: 'System Analyst',
        nampos4: 'System Analyst',
        nampos5: 'System Analyst'
      },
      {
        codpos: '0710',
        nampose: 'Software Consult',
        nampost: 'Software Consult',
        nampos3: 'Software Consult',
        nampos4: 'Software Consult',
        nampos5: 'Software Consult'
      },
      {
        codpos: '0715',
        nampose: 'Sr Programmer',
        nampost: 'Sr Programmer',
        nampos3: 'Sr Programmer',
        nampos4: 'Sr Programmer',
        nampos5: 'Sr Programmer'
      },
      {
        codpos: '0720',
        nampose: 'Technical Consult',
        nampost: 'Technical Consult',
        nampos3: 'Technical Consult',
        nampos4: 'Technical Consult',
        nampos5: 'Technical Consult'
      },
      {
        codpos: '0320',
        nampose: 'Support Manager',
        nampost: 'Support Manager',
        nampos3: 'Support Manager',
        nampos4: 'Support Manager',
        nampos5: 'Support Manager'
      },
      {
        codpos: '0760',
        nampose: 'Programmer',
        nampost: 'Programmer',
        nampos3: 'Programmer',
        nampos4: 'Programmer',
        nampos5: 'Programmer'
      },
      {
        codpos: '0810',
        nampose: 'Software Tester',
        nampost: 'Software Tester',
        nampos3: 'Software Tester',
        nampos4: 'Software Tester',
        nampos5: 'Software Tester'
      },
      {
        codpos: '0840',
        nampose: 'Technical',
        nampost: 'Technical',
        nampos3: 'Technical',
        nampos4: 'Technical',
        nampos5: 'Technical'
      },
      {
        codpos: '0900',
        nampose: 'Subcontact',
        nampost: 'Subcontact',
        nampos3: 'Subcontact',
        nampos4: 'Subcontact',
        nampos5: 'Subcontact'
      },
      {
        codpos: '0910',
        nampose: 'Staff Officer',
        nampost: 'Staff Officer',
        nampos3: 'Staff Officer',
        nampos4: 'Staff Officer',
        nampos5: 'Staff Officer'
      },
      {
        codpos: '0920',
        nampose: 'Parttime',
        nampost: 'Parttime',
        nampos3: 'Parttime',
        nampos4: 'Parttime',
        nampos5: 'Parttime'
      },
      {
        codpos: '4100',
        nampose: 'General Worker 1',
        nampost: 'General Worker 1',
        nampos3: 'General Worker 1',
        nampos4: 'General Worker 1',
        nampos5: 'General Worker 1'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodapp () {
    let mockRows = []
    const mockTotal = 15
    mockRows = mockRows.concat([
      {
        codapp: 'HRES32E',
        namappe: 'Personal Data Change',
        namappt: 'เปลี่ยนแปลงข้อมูลพนักงาน',
        namapp3: 'Personal Data Change',
        namapp4: 'Personal Data Change',
        namapp5: 'Personal Data Change'
      },
      {
        codapp: 'HRES34E',
        namappe: 'Transfer Request',
        namappt: 'ขอโอนย้าย',
        namapp3: 'Transfer Request',
        namapp4: 'Transfer Request',
        namapp5: 'Transfer Request'
      },
      {
        codapp: 'HRES36E',
        namappe: 'Working Ref. Letter Request',
        namappt: 'ขอหนังสือรับรอง',
        namapp3: 'Working Ref. Letter Request',
        namapp4: 'Working Ref. Letter Request',
        namapp5: 'Working Ref. Letter Request'
      },
      {
        codapp: 'HRES62E',
        namappe: 'Leave Application',
        namappt: 'ลาหยุดงาน',
        namapp3: 'Leave Application',
        namapp4: 'Leave Application',
        namapp5: 'Leave Application'
      },
      {
        codapp: 'HRES6AE',
        namappe: 'Change of Actual Time',
        namappt: 'แก้ไขเวลาเข้า-ออก',
        namapp3: 'Change of Actual Time',
        namapp4: 'Change of Actual Time',
        namapp5: 'Change of Actual Time'
      },
      {
        codapp: 'HRES6DE',
        namappe: 'Change of Shift Schedule',
        namappt: 'แก้ไขเวลาทำงาน',
        namapp3: 'Change of Shift Schedule',
        namapp4: 'Change of Shift Schedule',
        namapp5: 'Change of Shift Schedule'
      },
      {
        codapp: 'HRES6IE',
        namappe: 'Training Request',
        namappt: 'ขอฝึกอบรม',
        namapp3: 'Training Request',
        namapp4: 'Training Request',
        namapp5: 'Training Request'
      },
      {
        codapp: 'HRES6KE',
        namappe: 'O.T. Request',
        namappt: 'ทำงานล่วงเวลา',
        namapp3: 'O.T. Request',
        namapp4: 'O.T. Request',
        namapp5: 'O.T. Request'
      },
      {
        codapp: 'HRES6ME',
        namappe: 'Cancel Leave',
        namappt: 'ยกเลิกการลาหยุดงาน',
        namapp3: 'Cancel Leave',
        namapp4: 'Cancel Leave',
        namapp5: 'Cancel Leave'
      },
      {
        codapp: 'HRES71E',
        namappe: 'Medical Requisition',
        namappt: 'เบิกค่ารักษาพยาบาล',
        namapp3: 'Medical Requisition',
        namapp4: 'Medical Requisition',
        namapp5: 'Medical Requisition'
      },
      {
        codapp: 'HRES74E',
        namappe: 'Benefits Requisition',
        namappt: 'เบิกค่าสวัสดิการ',
        namapp3: 'Benefits Requisition',
        namapp4: 'Benefits Requisition',
        namapp5: 'Benefits Requisition'
      },
      {
        codapp: 'HRES77E',
        namappe: 'Loan Requisition',
        namappt: 'เบิกเงินกู้',
        namapp3: 'Loan Requisition',
        namapp4: 'Loan Requisition',
        namapp5: 'Loan Requisition'
      },
      {
        codapp: 'HRES81E',
        namappe: 'Travel',
        namappt: 'ค่าเดินทาง',
        namapp3: 'Travel',
        namapp4: 'Travel',
        namapp5: 'Travel'
      },
      {
        codapp: 'HRES86E',
        namappe: 'Resignation Requisition',
        namappt: 'ลาออก',
        namapp3: 'Resignation Requisition',
        namapp4: 'Resignation Requisition',
        namapp5: 'Resignation Requisition'
      },
      {
        codapp: 'HRES88E',
        namappe: 'Record Staff  Requisition',
        namappt: 'จ้างงาน',
        namapp3: 'Record Staff  Requisition',
        namapp4: 'Record Staff  Requisition',
        namapp5: 'Record Staff  Requisition'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovNummemo () {
    let mockRows = []
    const mockTotal = 15
    mockRows = mockRows.concat([
      {
        nummemo: '2017/009',
        dteyear: '2017',
        dtemonth: '9',
        numclseq: '7',
        namcourse: 'TAX Planing Strategy',
        namcourst: 'TAX Planing Strategy',
        namcours3: 'TAX Planing Strategy',
        namcours4: 'TAX Planing Strategy',
        namcours5: 'TAX Planing Strategy'
      },
      {
        nummemo: '2017/008',
        dteyear: '2017',
        dtemonth: '8',
        numclseq: '2',
        namcourse: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcourst: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours3: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours4: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours5: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง'
      },
      {
        nummemo: '2017/007',
        dteyear: '2017',
        dtemonth: '11',
        numclseq: '5',
        namcourse: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcourst: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours3: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours4: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours5: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง'
      },
      {
        nummemo: '2017/006',
        dteyear: '2017',
        dtemonth: '5',
        numclseq: '1',
        namcourse: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcourst: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours3: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours4: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours5: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ'
      },
      {
        nummemo: '2017/005',
        dteyear: '2017',
        dtemonth: '6',
        numclseq: '2',
        namcourse: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcourst: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours3: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours4: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours5: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ'
      },
      {
        nummemo: '2017/004',
        dteyear: '2017',
        dtemonth: '5',
        numclseq: '1',
        namcourse: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcourst: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours3: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours4: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours5: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน'
      },
      {
        nummemo: '2017/003',
        dteyear: '2017',
        dtemonth: '11',
        numclseq: '7',
        namcourse: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcourst: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours3: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours4: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours5: 'การใช้งานระบบบัญชีการซื้อขาย'
      },
      {
        nummemo: '2017/002',
        dteyear: '2017',
        dtemonth: '10',
        numclseq: '11',
        namcourse: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcourst: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours3: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours4: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours5: 'การใช้งาน Microsoft Windows เบื้องต้น'
      },
      {
        nummemo: '2017/001',
        dteyear: '2017',
        dtemonth: '6',
        numclseq: '3',
        namcourse: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcourst: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours3: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours4: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ',
        namcours5: 'การบริหารต้นทุนอย่างมีประสิทธิภาพ'
      },
      {
        nummemo: '2016/001',
        dteyear: '2016',
        dtemonth: '7',
        numclseq: '1',
        namcourse: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcourst: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours3: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours4: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours5: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์'
      },
      {
        nummemo: '2558-006',
        dteyear: '2015',
        dtemonth: '8',
        numclseq: '4',
        namcourse: 'การสร้างแรงจูงใจ',
        namcourst: 'การสร้างแรงจูงใจ',
        namcours3: 'การสร้างแรงจูงใจ',
        namcours4: 'การสร้างแรงจูงใจ',
        namcours5: 'การสร้างแรงจูงใจ'
      },
      {
        nummemo: '2558-005',
        dteyear: '2015',
        dtemonth: '5',
        numclseq: '2',
        namcourse: 'การสร้างคนให้เป็นผู้นำ',
        namcourst: 'การสร้างคนให้เป็นผู้นำ',
        namcours3: 'การสร้างคนให้เป็นผู้นำ',
        namcours4: 'การสร้างคนให้เป็นผู้นำ',
        namcours5: 'การสร้างคนให้เป็นผู้นำ'
      },
      {
        nummemo: '2558-004',
        dteyear: '2015',
        dtemonth: '3',
        numclseq: '2',
        namcourse: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcourst: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours3: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours4: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours5: 'การวางแผนงานอย่างมีประสิทธิภาพ'
      },
      {
        nummemo: '2558-003',
        dteyear: '2015',
        dtemonth: '2',
        numclseq: '1',
        namcourse: 'การบริการจัดการทั่วไป',
        namcourst: 'การบริการจัดการทั่วไป',
        namcours3: 'การบริการจัดการทั่วไป',
        namcours4: 'การบริการจัดการทั่วไป',
        namcours5: 'การบริการจัดการทั่วไป'
      },
      {
        nummemo: '2558-002',
        dteyear: '2015',
        dtemonth: '3',
        numclseq: '2',
        namcourse: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcourst: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours3: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours4: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours5: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodcours () {
    let mockRows = []
    const mockTotal = 20
    mockRows = mockRows.concat([
      {
        codcours: 'AC0001',
        namcourse: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcourst: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours3: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours4: 'การใช้งานระบบบัญชีการซื้อขาย',
        namcours5: 'การใช้งานระบบบัญชีการซื้อขาย'
      },
      {
        codcours: 'AC0002',
        namcourse: 'TAX Planing Strategy',
        namcourst: 'TAX Planing Strategy',
        namcours3: 'TAX Planing Strategy',
        namcours4: 'TAX Planing Strategy',
        namcours5: 'TAX Planing Strategy'
      },
      {
        codcours: 'AC0003',
        namcourse: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcourst: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours3: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours4: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง',
        namcours5: 'เทคนิคการจัดซื้อและการบริหารสินค้าคงคลัง'
      },
      {
        codcours: 'IT0001',
        namcourse: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcourst: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours3: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours4: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน',
        namcours5: 'การใช้งาน SQL Plusw ขั้นพื้นฐาน'
      },
      {
        codcours: 'IT0002',
        namcourse: 'SQL Plus  for Information Technology',
        namcourst: 'SQL Plus  for Information Technology',
        namcours3: 'SQL Plus  for Information Technology',
        namcours4: 'SQL Plus  for Information Technology',
        namcours5: 'SQL Plus  for Information Technology'
      },
      {
        codcours: 'IT0003',
        namcourse: 'Oracle Developer',
        namcourst: 'Oracle Developer',
        namcours3: 'Oracle Developer',
        namcours4: 'Oracle Developer',
        namcours5: 'Oracle Developer'
      },
      {
        codcours: 'IT0004',
        namcourse: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcourst: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours3: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours4: 'การใช้งาน Microsoft Windows เบื้องต้น',
        namcours5: 'การใช้งาน Microsoft Windows เบื้องต้น'
      },
      {
        codcours: 'IT0005',
        namcourse: 'Visual Studio .NET',
        namcourst: 'Visual Studio .NET',
        namcours3: 'Visual Studio .NET',
        namcours4: 'Visual Studio .NET',
        namcours5: 'Visual Studio .NET'
      },
      {
        codcours: 'M00001',
        namcourse: 'ผู้จัดการมืออาชีพเพื่อการทำงาน',
        namcourst: 'ผู้จัดการมืออาชีพเพื่อการทำงาน',
        namcours3: 'ผู้จัดการมืออาชีพเพื่อการทำงาน',
        namcours4: 'ผู้จัดการมืออาชีพเพื่อการทำงาน',
        namcours5: 'ผู้จัดการมืออาชีพเพื่อการทำงาน'
      },
      {
        codcours: 'M00002',
        namcourse: 'ผู้อำนวยการมืออาชีพเพื่อการทำงาน',
        namcourst: 'ผู้อำนวยการมืออาชีพเพื่อการทำงาน',
        namcours3: 'ผู้อำนวยการมืออาชีพเพื่อการทำงาน',
        namcours4: 'ผู้อำนวยการมืออาชีพเพื่อการทำงาน',
        namcours5: 'ผู้อำนวยการมืออาชีพเพื่อการทำงาน'
      },
      {
        codcours: 'M00003',
        namcourse: 'การบริการเชิงซ้อนเพื่อการสร้างทีม',
        namcourst: 'การบริการเชิงซ้อนเพื่อการสร้างทีม',
        namcours3: 'การบริการเชิงซ้อนเพื่อการสร้างทีม',
        namcours4: 'การบริการเชิงซ้อนเพื่อการสร้างทีม',
        namcours5: 'การบริการเชิงซ้อนเพื่อการสร้างทีม'
      },
      {
        codcours: 'M00004',
        namcourse: 'วิธีการบริหารงาน และการขอ ISO 9001',
        namcourst: 'วิธีการบริหารงาน และการขอ ISO 9001',
        namcours3: 'วิธีการบริหารงาน และการขอ ISO 9001',
        namcours4: 'วิธีการบริหารงาน และการขอ ISO 9001',
        namcours5: 'วิธีการบริหารงาน และการขอ ISO 9001'
      },
      {
        codcours: 'R00001',
        namcourse: 'ความรู้เบื้องต้นเกี่ยวกับการนำเสนองาน',
        namcourst: 'ความรู้เบื้องต้นเกี่ยวกับการนำเสนองาน',
        namcours3: 'ความรู้เบื้องต้นเกี่ยวกับการนำเสนองาน',
        namcours4: 'ความรู้เบื้องต้นเกี่ยวกับการนำเสนองาน',
        namcours5: 'ความรู้เบื้องต้นเกี่ยวกับการนำเสนองาน'
      },
      {
        codcours: 'R00002',
        namcourse: 'เทคนิคการประชุมที่มีประสิทธิภาพ',
        namcourst: 'เทคนิคการประชุมที่มีประสิทธิภาพ',
        namcours3: 'เทคนิคการประชุมที่มีประสิทธิภาพ',
        namcours4: 'เทคนิคการประชุมที่มีประสิทธิภาพ',
        namcours5: 'เทคนิคการประชุมที่มีประสิทธิภาพ'
      },
      {
        codcours: 'R00003',
        namcourse: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcourst: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours3: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours4: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์',
        namcours5: 'องค์กรที่เป็นเลิศกับการบริหารเชิงกลยุทธ์'
      },
      {
        codcours: 'R00004',
        namcourse: 'การสร้าง Customer Relations',
        namcourst: 'การสร้าง Customer Relations',
        namcours3: 'การสร้าง Customer Relations',
        namcours4: 'การสร้าง Customer Relations',
        namcours5: 'การสร้าง Customer Relations'
      },
      {
        codcours: 'R00005',
        namcourse: 'เลขามืออาชีพ',
        namcourst: 'เลขามืออาชีพ',
        namcours3: 'เลขามืออาชีพ',
        namcours4: 'เลขามืออาชีพ',
        namcours5: 'เลขามืออาชีพ'
      },
      {
        codcours: 'R00006',
        namcourse: 'การจัดการสิ่งแวดล้อม',
        namcourst: 'การจัดการสิ่งแวดล้อม',
        namcours3: 'การจัดการสิ่งแวดล้อม',
        namcours4: 'การจัดการสิ่งแวดล้อม',
        namcours5: 'การจัดการสิ่งแวดล้อม'
      },
      {
        codcours: 'R00007',
        namcourse: 'การบริการจัดการทั่วไป',
        namcourst: 'การบริการจัดการทั่วไป',
        namcours3: 'การบริการจัดการทั่วไป',
        namcours4: 'การบริการจัดการทั่วไป',
        namcours5: 'การบริการจัดการทั่วไป'
      },
      {
        codcours: 'R00008',
        namcourse: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcourst: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours3: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours4: 'การวางแผนงานอย่างมีประสิทธิภาพ',
        namcours5: 'การวางแผนงานอย่างมีประสิทธิภาพ'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodform () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        codform: '0001',
        desforme: 'Appraisal Form',
        desformt: 'แบบฟอร์มประเมินผลการปฎิบัติงาน',
        desform3: 'Appraisal Form',
        desform4: 'Appraisal Form',
        desform5: 'Appraisal Form'
      },
      {
        codform: '0002',
        desforme: 'General Appraisal Form',
        desformt: 'แบบประเมินผลหลังอบรม',
        desform3: 'General Appraisal Form',
        desform4: 'General Appraisal Form',
        desform5: 'General Appraisal Form'
      },
      {
        codform: '02RC',
        desforme: 'Interviews Form',
        desformt: 'แบบฟอร์มการสัมภาษณ์งาน - พนักงาน',
        desform3: 'Interviews Form',
        desform4: 'Interviews Form',
        desform5: 'Interviews Form'
      },
      {
        codform: '03RC',
        desforme: 'Interviews Form',
        desformt: 'แบบฟอร์มการสัมภาษณ์งาน - ผู้บริหาร',
        desform3: 'Interviews Form',
        desform4: 'Interviews Form',
        desform5: 'Interviews Form'
      },
      {
        codform: 'AD01',
        desforme: 'Appraisal Development',
        desformt: 'Appraisal Development',
        desform3: 'Appraisal Development',
        desform4: 'Appraisal Development',
        desform5: 'Appraisal Development'
      },
      {
        codform: 'AP01',
        desforme: 'Appraisal Form',
        desformt: 'แบบฟอร์มประเมินผลการปฎิบัติงาน',
        desform3: 'Appraisal Form',
        desform4: 'Appraisal Form',
        desform5: 'Appraisal Form'
      },
      {
        codform: 'AP02',
        desforme: 'Instructor Evaluate Form',
        desformt: 'แบบประเมินวิทยากร',
        desform3: 'Instructor Evaluate Form',
        desform4: 'Instructor Evaluate Form',
        desform5: 'Instructor Evaluate Form'
      },
      {
        codform: 'APY1',
        desforme: 'Performance Evaluation Form',
        desformt: 'แบบฟอร์มการประเมินผลประสิทธิภาพการทำงานประจำปี',
        desform3: 'Performance Evaluation Form',
        desform4: 'Performance Evaluation Form',
        desform5: 'Performance Evaluation Form'
      },
      {
        codform: 'APY2',
        desforme: 'General Appraisal Form',
        desformt: 'แบบฟอร์มทดสอบทั่วไป',
        desform3: 'General Appraisal Form',
        desform4: 'General Appraisal Form',
        desform5: 'General Appraisal Form'
      },
      {
        codform: 'C001',
        desforme: 'Beh Evaluate Form',
        desformt: 'ปัจจัยวัดความสามารถและพฤติกรรม',
        desform3: 'Beh Evaluate Form',
        desform4: 'Beh Evaluate Form',
        desform5: 'Beh Evaluate Form'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovNumappl () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        numappl: '2006504',
        desapple: 'Mr. Nophon  Komarachun',
        desapplt: 'นาย นพพล  โกมารชุน',
        desappl3: 'Mr. Nophon  Komarachun',
        desappl4: 'Mr. Nophon  Komarachun',
        desappl5: 'Mr. Nophon  Komarachun'
      },
      {
        numappl: '2007509',
        desapple: 'Miss. patharida  Aransri',
        desapplt: 'นางสาว ภัทริดา  อรัญศรี',
        desappl3: 'Miss. patharida  Aransri',
        desappl4: 'Miss. patharida  Aransri',
        desappl5: 'Miss. patharida  Aransri'
      },
      {
        numappl: '2007510',
        desapple: 'Mr.Kaew  Kagee',
        desapplt: 'นายแก้ว  ขจี',
        desappl3: 'Mr.Kaew  Kagee',
        desappl4: 'Mr.Kaew  Kagee',
        desappl5: 'Mr.Kaew  Kagee'
      },
      {
        numappl: '2007511',
        desapple: 'Mr. Akawoot  Bandon',
        desapplt: 'นาย เอกวุฒิ  บ้านดอน',
        desappl3: 'Mr. Akawoot  Bandon',
        desappl4: 'Mr. Akawoot  Bandon',
        desappl5: 'Mr. Akawoot  Bandon'
      },
      {
        numappl: '2007513',
        desapple: 'Miss. Meesuk  yusmer',
        desapplt: 'นางสาว มีสุข  อยู่เสมอ',
        desappl3: 'Miss. Meesuk  yusmer',
        desappl4: 'Miss. Meesuk  yusmer',
        desappl5: 'Miss. Meesuk  yusmer'
      },
      {
        numappl: '2007514',
        desapple: 'Mr. Pitool  Donsoongnern',
        desapplt: 'นาย ไพฑูร  ดอนสูงเนิน',
        desappl3: 'Mr. Pitool  Donsoongnern',
        desappl4: 'Mr. Pitool  Donsoongnern',
        desappl5: 'Mr. Pitool  Donsoongnern'
      },
      {
        numappl: '2007517',
        desapple: 'Mr.Paisarn  Sirivit',
        desapplt: 'นายไพศาล  สิริวิทย์',
        desappl3: 'Mr.Paisarn  Sirivit',
        desappl4: 'Mr.Paisarn  Sirivit',
        desappl5: 'Mr.Paisarn  Sirivit'
      },
      {
        numappl: '2007518',
        desapple: 'Mrs. Pimpa  Kana',
        desapplt: 'นาง พิมภา คณา',
        desappl3: 'Mrs. Pimpa  Kana',
        desappl4: 'Mrs. Pimpa  Kana',
        desappl5: 'Mrs. Pimpa  Kana'
      },
      {
        numappl: '2007519',
        desapple: 'Mrs. Sujitra  Khamtea',
        desapplt: 'นาง สุจิตรา  งามแท้',
        desappl3: 'Mrs. Sujitra  Khamtea',
        desappl4: 'Mrs. Sujitra  Khamtea',
        desappl5: 'Mrs. Sujitra  Khamtea'
      },
      {
        numappl: '2007520',
        desapple: 'Mr.Mongkol  Peamsook',
        desapplt: 'นายมงคล  เปี่ยมสุข',
        desappl3: 'Mr.Mongkol  Peamsook',
        desappl4: 'Mr.Mongkol  Peamsook',
        desappl5: 'Mr.Mongkol  Peamsook'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovNumreq () {
    let mockRows = []
    const mockTotal = 10
    const mockNumreq = [
      '2015/1',
      '2015/2',
      '2015/3',
      '2015/4',
      '2016/1',
      '2016/2',
      '2016/3',
      '2017/1',
      '2017/2',
      '2017/3'
    ]
    const mockCodcomp = [
      'TJS-100-000-000-000-000-000 ฝ่ายดำเนินการและพัฒนาระบบ',
      'TJS-200-000-000-000-000-000 ฝ่ายขายและการตลาด',
      'TJS-300-000-000-000-000-000 ฝ่ายบัญชีการเงินและทรัพยากรบุคคล',
      'TJS-100-100-000-000-000-000 ส่วนพัฒนาระบบและดำเนินการ',
      'TJS-300-000-000-000-000-000 ฝ่ายบัญชีการเงินและทรัพยากรบุคคล',
      'TJS-300-000-000-000-000-000 ฝ่ายบัญชีการเงินและทรัพยากรบุคคล',
      'TJS-000-000-000-000-000-000 บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด',
      'TJS-100-000-000-000-000-000 ฝ่ายดำเนินการและพัฒนาระบบ',
      'TJS-100-000-000-000-000-000 ฝ่ายดำเนินการและพัฒนาระบบ',
      'TJS-100-000-000-000-000-000 ฝ่ายดำเนินการและพัฒนาระบบ'
    ]
    const mockCodpos = [
      '0800',
      '0830',
      '0800',
      '0310',
      '0800',
      '0800',
      '0200',
      '0800',
      '0810',
      '0800'
    ]
    const mockNampos = [
      'Staff Officer',
      'Programmer',
      'Staff Officer',
      'Project Manager',
      'Staff Officer',
      'Staff Officer',
      'Marketing Manager',
      'Staff Officer',
      'Software Tester',
      'Staff Officer'
    ]
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        numreq: mockNumreq[i - 1],
        codcomp: mockCodcomp[i - 1],
        codpos: mockCodpos[i - 1],
        nampose: mockNampos[i - 1],
        nampost: mockNampos[i - 1],
        nampos3: mockNampos[i - 1],
        nampos4: mockNampos[i - 1],
        nampos5: mockNampos[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodaplvl () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        codaplvl: '0001',
        descode: 'Group Staff',
        descodt: 'กลุ่มพนักงาน',
        descod3: 'Group Staff',
        descod4: 'Group Staff',
        descod5: 'Group Staff'
      },
      {
        codaplvl: '0002',
        descode: 'Group Chief',
        descodt: 'กลุ่มหัวหน้างาน',
        descod3: 'Group Chief',
        descod4: 'Group Chief',
        descod5: 'Group Chief'
      },
      {
        codaplvl: '0003',
        descode: 'Management Group',
        descodt: 'กลุ่มผู้จัดการ',
        descod3: 'Management Group',
        descod4: 'Management Group',
        descod5: 'Management Group'
      },
      {
        codaplvl: '0004',
        descode: 'Advisor Group',
        descodt: 'กลุ่มที่ปรึกษา',
        descod3: 'Advisor Group',
        descod4: 'Advisor Group',
        descod5: 'Advisor Group'
      },
      {
        codaplvl: '0005',
        descode: 'Executive Group',
        descodt: 'กลุ่มผู้บริหาร',
        descod3: 'Executive Group',
        descod4: 'Executive Group',
        descod5: 'Executive Group'
      },
      {
        codaplvl: 'S001',
        descode: 'พนักงานระดับ 1-4',
        descodt: 'พนักงานระดับ 1-4',
        descod3: 'พนักงานระดับ 1-4',
        descod4: 'พนักงานระดับ 1-4',
        descod5: 'พนักงานระดับ 1-4'
      },
      {
        codaplvl: 'S002',
        descode: 'เจ้าหน้าที่ 5-8',
        descodt: 'เจ้าหน้าที่ 5-8',
        descod3: 'เจ้าหน้าที่ 5-8',
        descod4: 'เจ้าหน้าที่ 5-8',
        descod5: 'เจ้าหน้าที่ 5-8'
      },
      {
        codaplvl: 'S003',
        descode: 'ผู้ช่วยผู้จัดการ',
        descodt: 'ผู้ช่วยผู้จัดการ',
        descod3: 'ผู้ช่วยผู้จัดการ',
        descod4: 'ผู้ช่วยผู้จัดการ',
        descod5: 'ผู้ช่วยผู้จัดการ'
      },
      {
        codaplvl: 'S004',
        descode: 'ผู้จัดการ-ผู้จัดการอาวุโส',
        descodt: 'ผู้จัดการ-ผู้จัดการอาวุโส',
        descod3: 'ผู้จัดการ-ผู้จัดการอาวุโส',
        descod4: 'ผู้จัดการ-ผู้จัดการอาวุโส',
        descod5: 'ผู้จัดการ-ผู้จัดการอาวุโส'
      },
      {
        codaplvl: 'S005',
        descode: 'รองผู้อำนวยการ',
        descodt: 'รองผู้อำนวยการ',
        descod3: 'รองผู้อำนวยการ',
        descod4: 'รองผู้อำนวยการ',
        descod5: 'รองผู้อำนวยการ'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovGrdscor () {
    let mockRows = []
    let grdscor = ''
    const mockGrdscor = [ '5', '10', '15', '20' ]
    const mockQtyscor = {
      '5': '5.00',
      '10': '10.00',
      '15': '15.00',
      '20': '20.00'
    }
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      grdscor = mockGrdscor[Math.floor(Math.random() * 2)]
      mockRows.push({
        grdscor: '55',
        grditem: mockGrdscor[grdscor],
        achieve: 'TEST',
        qtyscor: mockQtyscor[grdscor]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodappr () {
    let mockRows = []
    let staemp = ''
    const mockStaemp = [ 'P', 'Y' ]
    const mockStatus = {
      'P': 'ทดลองงาน',
      'Y': 'ปัจจุบัน'
    }
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      staemp = mockStaemp[Math.floor(Math.random() * 2)]
      mockRows.push({
        codappr: faker.lorem.word(),
        namempe: faker.name.findName(),
        namempt: faker.name.findName(),
        namemp3: faker.name.findName(),
        namemp4: faker.name.findName(),
        namemp5: faker.name.findName(),
        desstaemp: mockStatus[staemp],
        staemp: ''
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodshift () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        codshift: 'SL',
        desshifte: 'Sales 08:00-17:00',
        desshiftt: 'Sales 08:00-17:00',
        desshift3: 'Sales 08:00-17:00',
        desshift4: 'Sales 08:00-17:00',
        desshift5: 'Sales 08:00-17:00',
        schedule: '08:00 - 17:00'
      },
      {
        codshift: 'P1',
        desshifte: 'ฝ่ายผลิต 08:00-17:00',
        desshiftt: 'ฝ่ายผลิต 08:00-17:00',
        desshift3: 'ฝ่ายผลิต 08:00-17:00',
        desshift4: 'ฝ่ายผลิต 08:00-17:00',
        desshift5: 'ฝ่ายผลิต 08:00-17:00',
        schedule: '08:00 - 17:00'
      },
      {
        codshift: 'P2',
        desshifte: 'ฝ่ายผลิต 08:00-20:00',
        desshiftt: 'ฝ่ายผลิต 08:00-20:00',
        desshift3: 'Production 08:00-20:00',
        desshift4: 'Production 08:00-20:00',
        desshift5: 'Production 08:00-20:00',
        schedule: '08:00 - 20:00'
      },
      {
        codshift: 'DS',
        desshifte: 'DS - 08:00-17:30',
        desshiftt: 'DS - 08:00-17:30',
        desshift3: 'DS - 08:00-17:30',
        desshift4: 'DS - 08:00-17:30',
        desshift5: 'DS - 08:00-17:30',
        schedule: '08:00 - 17:30'
      },
      {
        codshift: 'NS',
        desshifte: 'NS - 20:00-05:30',
        desshiftt: 'NS - 20:00-05:30',
        desshift3: 'NS - 20:00-05:30',
        desshift4: 'NS - 20:00-05:30',
        desshift5: 'NS - 20:00-05:30',
        schedule: '20:00 - 05:30'
      },
      {
        codshift: 'DA',
        desshifte: 'DA - 07:30-16:30',
        desshiftt: 'DA - 07:30-16:30',
        desshift3: 'DA - 07:30-16:30',
        desshift4: 'DA - 07:30-16:30',
        desshift5: 'DA - 07:30-16:30',
        schedule: '07:30 - 16:30'
      },
      {
        codshift: 'D1',
        desshifte: 'D1 - 08:00-17:00',
        desshiftt: 'D1 - 08:00-17:00',
        desshift3: 'D1 - 08:00-17:00',
        desshift4: 'D1 - 08:00-17:00',
        desshift5: 'D1 - 08:00-17:00',
        schedule: '08:00 - 17:00'
      },
      {
        codshift: 'D2',
        desshifte: 'D2 - 09:00-18:00',
        desshiftt: 'D2 - 09:00-18:00',
        desshift3: 'D2 - 09:00-18:00',
        desshift4: 'D2 - 09:00-18:00',
        desshift5: 'D2 - 09:00-18:00',
        schedule: '09:00 - 18:00'
      },
      {
        codshift: 'D3',
        desshifte: 'D3 - 10:00-19:00',
        desshiftt: 'D3 - 10:00-19:00',
        desshift3: 'D3 - 10:00-19:00',
        desshift4: 'D3 - 10:00-19:00',
        desshift5: 'D3 - 10:00-19:00',
        schedule: '10:00 - 19:00'
      },
      {
        codshift: 'S1',
        desshifte: 'Shift 08.00-16.00',
        desshiftt: 'Shift 08.00-16.00',
        desshift3: 'Shift 08.00-16.00',
        desshift4: 'Shift 08.00-16.00',
        desshift5: 'Shift 08.00-16.00',
        schedule: '08:00 - 16:00'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodroom () {
    let mockRows = []
    const mockTotal = 4
    mockRows = mockRows.concat([
      {
        codroom: 'BATH001',
        roomname: 'ห้องประชุม 4 - ห้องสันติภาพ',
        roomnamt: 'ห้องประชุม 4 - ห้องสันติภาพ',
        roomnam3: 'ห้องประชุม 4 - ห้องสันติภาพ',
        roomnam4: 'ห้องประชุม 4 - ห้องสันติภาพ',
        roomnam5: 'ห้องประชุม 4 - ห้องสันติภาพ'
      },
      {
        codroom: 'R0003',
        roomname: 'ห้องประชุม 3 - เกศรา',
        roomnamt: 'ห้องประชุม 3 - เกศรา',
        roomnam3: 'ห้องประชุม 3 - เกศรา',
        roomnam4: 'ห้องประชุม 3 - เกศรา',
        roomnam5: 'ห้องประชุม 3 - เกศรา'
      },
      {
        codroom: 'R0001',
        roomname: 'ห้องประชุม 1 - ห้องบัวบาน',
        roomnamt: 'ห้องประชุม 1 - ห้องบัวบาน',
        roomnam3: 'ห้องประชุม 1 - ห้องบัวบาน',
        roomnam4: 'ห้องประชุม 1 - ห้องบัวบาน',
        roomnam5: 'ห้องประชุม 1 - ห้องบัวบาน'
      },
      {
        codroom: 'R0002',
        roomname: 'ห้องประชุม 2 - พุทธรักษ',
        roomnamt: 'ห้องประชุม 2 - พุทธรักษ',
        roomnam3: 'ห้องประชุม 2 - พุทธรักษ',
        roomnam4: 'ห้องประชุม 2 - พุทธรักษ',
        roomnam5: 'ห้องประชุม 2 - พุทธรักษ'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodasset () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        codasset: '0001',
        desassee: 'Mobile Phone',
        desasset: 'โทรศัพท์',
        desasse3: 'Mobile Phone',
        desasse4: 'Mobile Phone',
        desasse5: 'Mobile Phone'
      },
      {
        codasset: '0002',
        desassee: 'Computer NoteBook',
        desasset: 'คอมพิวเตอร์ NoteBook',
        desasse3: 'Computer NoteBook',
        desasse4: 'Computer NoteBook',
        desasse5: 'Computer NoteBook'
      },
      {
        codasset: '0003',
        desassee: 'PDA',
        desasset: 'PDA',
        desasse3: 'PDA',
        desasse4: 'PDA',
        desasse5: 'PDA'
      },
      {
        codasset: '0004',
        desassee: 'Projector',
        desasset: 'เครื่องฉายจอภาพ',
        desasse3: 'Projector',
        desasse4: 'Projector',
        desasse5: 'Projector'
      },
      {
        codasset: '0005',
        desassee: 'Car',
        desasset: 'รถยนต์',
        desasse3: 'Car',
        desasse4: 'Car',
        desasse5: 'Car'
      },
      {
        codasset: '0006',
        desassee: 'CD',
        desasset: 'แผ่น CD',
        desasse3: 'CD',
        desasse4: 'CD',
        desasse5: 'CD'
      },
      {
        codasset: '0007',
        desassee: 'Technbical Tools',
        desasset: 'อุปกรณ์เครืองมือช่าง',
        desasse3: 'Technbical Tools',
        desasse4: 'Technbical Tools',
        desasse5: 'Technbical Tools'
      },
      {
        codasset: '0008',
        desassee: 'Uniform',
        desasset: 'ชุดพนักงาน',
        desasse3: 'Uniform',
        desasse4: 'Uniform',
        desasse5: 'Uniform'
      },
      {
        codasset: '0009',
        desassee: 'Locker Keys',
        desasset: 'กุญแจ Lockers',
        desasse3: 'Locker Keys',
        desasse4: 'Locker Keys',
        desasse5: 'Locker Keys'
      },
      {
        codasset: '9999',
        desassee: 'N.A',
        desasset: 'อื่นๆ',
        desasse3: 'N.A',
        desasse4: 'N.A',
        desasse5: 'N.A'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodcalen () {
    let mockRows = []
    const mockTotal = 10
    mockRows = mockRows.concat([
      {
        codcalen: '0001',
        descalene: 'General staff of Head office',
        descalent: 'กลุ่มสำนักงาน',
        descalen3: 'General staff of Head office',
        descalen4: 'General staff of Head office',
        descalen5: 'General staff of Head office'
      },
      {
        codcalen: '0002',
        descalene: 'General staff of Branch',
        descalent: 'กลุ่มสาขา',
        descalen3: 'General staff of Branch',
        descalen4: 'General staff of Branch',
        descalen5: 'General staff of Branch'
      },
      {
        codcalen: '0003',
        descalene: 'Staff shift 1',
        descalent: 'พนักงานกะ เช้า',
        descalen3: 'Staff shift 1',
        descalen4: 'Staff shift 1',
        descalen5: 'Staff shift 1'
      },
      {
        codcalen: '0004',
        descalene: 'Staff shift 2',
        descalent: 'พนักงานกะ บ่าย',
        descalen3: 'Staff shift 2',
        descalen4: 'Staff shift 2',
        descalen5: 'Staff shift 2'
      },
      {
        codcalen: '0005',
        descalene: 'Shift 2',
        descalent: 'พนักงานกะ ดึก',
        descalen3: 'Shift 2',
        descalen4: 'Shift 2',
        descalen5: 'Shift 2'
      },
      {
        codcalen: 'S1',
        descalene: 'Group Shift 1',
        descalent: 'กลุ่มกะ1',
        descalen3: 'Group Shift 1',
        descalen4: 'Group Shift 1',
        descalen5: 'Group Shift 1'
      },
      {
        codcalen: 'S2',
        descalene: 'Group Shift 2',
        descalent: 'กลุ่มกะ2',
        descalen3: 'Group Shift 2',
        descalen4: 'Group Shift 2',
        descalen5: 'Group Shift 2'
      },
      {
        codcalen: 'S3',
        descalene: 'Group Shift 3',
        descalent: 'กลุ่มกะ3',
        descalen3: 'Group Shift 3',
        descalen4: 'Group Shift 3',
        descalen5: 'Group Shift 3'
      },
      {
        codcalen: 'S4',
        descalene: 'Group Shift 4',
        descalent: 'กลุ่มกะ4',
        descalen3: 'Group Shift 4',
        descalen4: 'Group Shift 4',
        descalen5: 'Group Shift 4'
      },
      {
        codcalen: 'S5',
        descalene: 'Group Shift 5',
        descalent: 'กลุ่มกะ5',
        descalen3: 'Group Shift 5',
        descalen4: 'Group Shift 5',
        descalen5: 'Group Shift 5'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodcompy () {
    let mockRows = []
    const mockTotal = 3
    mockRows = mockRows.concat([
      {
        codcompy: '001',
        descompye: 'GKN Driveline Thailand Ltd.',
        descompyt: 'บริษัท จีเคเอ็น ไดรฟไลน์ (ประเทศไทย) จำกัด',
        descompy3: 'GKN Driveline Thailand Ltd.',
        descompy4: 'GKN Driveline Thailand Ltd.',
        descompy5: 'GKN Driveline Thailand Ltd.'
      },
      {
        codcompy: 'NIK',
        descompye: 'Nikon Thailand Co., Ltd.',
        descompyt: 'บริษัท นิโคอิ เนกิ จำกัด',
        descompy3: 'Nikon Thailand Co., Ltd.',
        descompy4: 'Nikon Thailand Co., Ltd.',
        descompy5: 'Nikon Thailand Co., Ltd.'
      },
      {
        codcompy: 'PPS',
        descompye: 'People Plus Software Co.,Ltd.',
        descompyt: 'บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด',
        descompy3: 'People Plus Software Co.,Ltd.',
        descompy4: 'People Plus Software Co.,Ltd.',
        descompy5: 'People Plus Software Co.,Ltd.'
      },
      {
        codcompy: 'SAN',
        descompye: 'Sann Sound Co.,Ltd.',
        descompyt: 'บริษัท แซนซาวด์ จำกัด',
        descompy3: 'Sann Sound Co.,Ltd.',
        descompy4: 'Sann Sound Co.,Ltd.',
        descompy5: 'Sann Sound Co.,Ltd.'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCoduser () {
    let mockRows = []
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        coduser: faker.lorem.word(),
        namempe: faker.name.findName(),
        namempt: faker.name.findName(),
        namemp3: faker.name.findName(),
        namemp4: faker.name.findName(),
        namemp5: faker.name.findName()
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodsecu () {
    let mockRows = []
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codsecu: faker.lorem.word(),
        descsecue: faker.lorem.words(),
        descsecut: faker.lorem.words(),
        descsecu3: faker.lorem.words(),
        descsecu4: faker.lorem.words(),
        descsecu5: faker.lorem.words()
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodproc () {
    let mockRows = []
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codproc: faker.lorem.word(),
        descproce: faker.lorem.words(),
        descproct: faker.lorem.words(),
        descproc3: faker.lorem.words(),
        descproc4: faker.lorem.words(),
        descproc5: faker.lorem.words()
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodincom () {
    let mockRows = []
    const mockTotal = 5
    const mockCodincom = [
      '01',
      '02',
      '03',
      '04',
      '05'
    ]
    const mockDesincom = [
      'เงินเดือน/ค่าแรง',
      'ค่าครองชีพ',
      'ค่าตำแหน่ง',
      'ค่าความสามารถ',
      'ค่าวิชาชีพ'
    ]
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codincom: mockCodincom[i - 1],
        descodincome: mockDesincom[i - 1],
        descodincomt: mockDesincom[i - 1],
        descodincom3: mockDesincom[i - 1],
        descodincom4: mockDesincom[i - 1],
        descodincom5: mockDesincom[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodretro () {
    let mockRows = []
    const mockTotal = 10
    const mockCodretro = [
      '10',
      '11',
      '12',
      '13',
      '14',
      '15',
      '16',
      '17',
      '18',
      '19'
    ]
    const mockDescretro = [
      'จ้างทำของ',
      'ค่าล่วงเวลา',
      'ค่าเบี้ยขยันรายเดือน',
      'ค่าเบี้ยขยันประจำปี',
      'ค่าอาหาร (ทำงานปกติ)',
      'ค่าอาหาร (ล่วงเวลา)',
      'ค่าเบี้ยเลี้ยงพิเศษ',
      'ค่าโทรศัพท์',
      'ค่าอยู่เวรพิเศษ',
      'ค่าความรู้พิเศษ'
    ]
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codretro: mockCodretro[i - 1],
        descodretroe: mockDescretro[i - 1],
        descodretrot: mockDescretro[i - 1],
        descodretro3: mockDescretro[i - 1],
        descodretro4: mockDescretro[i - 1],
        descodretro5: mockDescretro[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtable () {
    let mockRows = []
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtable: faker.lorem.word(),
        descodtablee: faker.lorem.words(),
        descodtablet: faker.lorem.words(),
        descodtable3: faker.lorem.words(),
        descodtable4: faker.lorem.words(),
        descodtable5: faker.lorem.words()
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodval () {
    let mockRows = []
    const mockTotal = 100
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codval: faker.lorem.word(),
        descodvale: faker.lorem.words(),
        descodvalt: faker.lorem.words(),
        descodval3: faker.lorem.words(),
        descodval4: faker.lorem.words(),
        descodval5: faker.lorem.words()
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCostCenter () {
    let mockRows = []
    const mockTotal = 9
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        costcent: faker.lorem.word(),
        namcente: faker.lorem.words(),
        namcentt: faker.lorem.words(),
        namcent3: faker.lorem.words(),
        namcent4: faker.lorem.words(),
        namcent5: faker.lorem.words()
      })
    }
    mockRows = mockRows.concat([
      {
        costcent: 'PPS00001',
        namcente: 'สาขา1',
        namcentt: 'สาขา1',
        namcent3: 'สาขา1',
        namcent4: 'สาขา1',
        namcent5: 'สาขา1'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovComGroup () {
    let mockRows = []
    const mockTotal = 8
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        compgrp: faker.lorem.word(),
        descode: faker.lorem.words(),
        descodt: faker.lorem.words(),
        descod3: faker.lorem.words(),
        descod4: faker.lorem.words(),
        descod5: faker.lorem.words()
      })
    }
    mockRows = mockRows.concat([
      {
        compgrp: '0001',
        descode: 'กลุ่มบริษัทดูแลลูกค้า',
        descodt: 'กลุ่มบริษัทดูแลลูกค้า',
        descod3: 'กลุ่มบริษัทดูแลลูกค้า',
        descod4: 'กลุ่มบริษัทดูแลลูกค้า',
        descod5: 'กลุ่มบริษัทดูแลลูกค้า'
      },
      {
        compgrp: '0002',
        descode: 'กลุ่มบริษัทการผลิต',
        descodt: 'กลุ่มบริษัทการผลิต',
        descod3: 'กลุ่มบริษัทการผลิต',
        descod4: 'กลุ่มบริษัทการผลิต',
        descod5: 'กลุ่มบริษัทการผลิต'
      }
    ])
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtency () {
    let mockRows = []
    const mockCodtency = [
      'C001',
      'F001',
      'M001'
    ]
    const mockDescCodtency = [
      'Core Competency',
      'Functional Competency',
      'Management Competency'
    ]
    const mockTotal = 3
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtency: mockCodtency[i - 1],
        descodtencye: mockDescCodtency[i - 1],
        descodtencyt: mockDescCodtency[i - 1],
        descodtency3: mockDescCodtency[i - 1],
        descodtency4: mockDescCodtency[i - 1],
        descodtency5: mockDescCodtency[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodoccup () {
    let mockRows = []
    const mockCodoccup = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010'
    ]
    const mockOccup = [
      'รับราชการ',
      'ค้าขาย',
      'ประมง',
      'รับจ้าง',
      'ธุรกิจส่วนตัว',
      'พนักงานบริษัท',
      'เกษตรกร',
      'นักแสดง/พิธีกร',
      'นักกฎหมาย/การเมือง',
      'วิศวกร'
    ]
    const mockTotal = 7
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codoccup: mockCodoccup[i - 1],
        descodoccupe: mockOccup[i - 1],
        descodoccupt: mockOccup[i - 1],
        descodoccup3: mockOccup[i - 1],
        descodoccup4: mockOccup[i - 1],
        descodoccup5: mockOccup[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodhosp () {
    let mockRows = []
    const mockCodhosp = [
      'H0001',
      'H0002',
      'H0003',
      'H0004',
      'H0005',
      'H0006',
      'H0007',
      'H0008',
      'H0009',
      'H0010',
      'H0011',
      'H0012',
      'H0013'
    ]
    const mockdeschosp = [
      'คณะแพทยศาสตร์วชิรพยาบาล',
      'รพ.จุฬาลงกรณ์',
      'รพ.ตำรวจ',
      'รพ.พระมงกุฎเกล้า',
      'รพ.ภูมิพลอดุลยเดช',
      'รพ.ศิริราช',
      'รพ.สมเด็จพระปิ่นเกล้า',
      'รพ.กล้วยน้ำไท',
      'รพ.เกษมราษฎร์ ประชาชื่น',
      'รพ.เปาโล โชคชัย',
      'รพ.ราษฎร์บูรณะ',
      'รพ.ลาดพร้าว',
      'รพ.สายไหม'
    ]
    const mockTotal = 13
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codhosp: mockCodhosp[i - 1],
        descodhospe: mockdeschosp[i - 1],
        descodhospt: mockdeschosp[i - 1],
        descodhosp3: mockdeschosp[i - 1],
        descodhosp4: mockdeschosp[i - 1],
        descodhosp5: mockdeschosp[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodprov () {
    let mockRows = []
    const mockCodprov = [
      'P0001',
      'P0002',
      'P0003',
      'P0004',
      'P0005',
      'P0006',
      'P0007',
      'P0008',
      'P0009',
      'P0010',
      'P0011',
      'P0012',
      'P0013',
      'P0014',
      'P0015',
      'P0015',
      'P0016',
      'P0017',
      'P0018',
      'P0019',
      'P0020'
    ]
    const mockdescprov = [
      'เชียงใหม่',
      'เชียงราย',
      'แม่ฮ่องสอน',
      'นครราชสีมา',
      'ขอนแก่น',
      'เพชรบูรณ์',
      'ชลบุรี',
      'ระยอง',
      'จันทบุรี',
      'กรุงเทพมหานคร',
      'สมุทรปราการ',
      'ปทุมธานี',
      'กาญจนบุรี',
      'ประจวบคีรีขันธ์',
      'ชุมพร',
      'สุราษฎร์ธานี',
      'นครศรีธรรมราช',
      'กระบี่',
      'ภูเก็ต'
    ]
    const mockTotal = 15
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codprov: mockCodprov[i - 1],
        descodprove: mockdescprov[i - 1],
        descodprovt: mockdescprov[i - 1],
        descodprov3: mockdescprov[i - 1],
        descodprov4: mockdescprov[i - 1],
        descodprov5: mockdescprov[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodcarcab () {
    let mockRows = []
    const mockcodcarcab = [
      'CB0001',
      'CB0002',
      'CB0003',
      'CB0004',
      'CB0005',
      'CB0006',
      'CB0007',
      'CB0008',
      'CB0009',
      'CB0010'
    ]
    const mockdesccarcab = [
      'หมอชิตใหม่ - คลองสาน',
      'สถานีขนส่ง(จตุจักร) - จักรวรรดิ์',
      'คลองขวาง - หัวลำโพง',
      'คลองเตย - ห้วยขวาง',
      'หมอชิต2 - สุรวงศ์',
      'ท่าข้าม - อนุเสาวรีย์ชัยสมรภูมิ',
      'สวนสยาม - สาธุประดิษฐ์',
      'มีนบุรี - อนุสาวรีย์ชัยฯ',
      'มธ.ศูนย์รังสิต - หัวลำโพง',
      'รังสิต - ม.เกษตรศาสตร์'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codcarcab: mockcodcarcab[i - 1],
        descodcarcabe: mockdesccarcab[i - 1],
        descodcarcabt: mockdesccarcab[i - 1],
        descodcarcab3: mockdesccarcab[i - 1],
        descodcarcab4: mockdesccarcab[i - 1],
        descodcarcab5: mockdesccarcab[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodgrpwork () {
    let mockRows = []
    const mockcodgrpwork = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      'S1',
      'S2',
      'S3',
      'S4',
      'S5'
    ]
    const mockdescgrpwork = [
      'General staff of Head office',
      'General staff of Branch',
      'Staff shift 1',
      'Staff shift 2',
      'Shift 2',
      'Group Shift 1',
      'Group Shift 2',
      'Group Shift 3',
      'Group Shift 4',
      'Group Shift 5'
    ]
    const mockdescgrpworkt = [
      'กลุ่มสำนักงาน',
      'กลุ่มสาขา',
      'พนักงานกะ เช้า',
      'พนักงานกะ บ่าย',
      'พนักงานกะ ดึก',
      'กลุ่มกะ1',
      'กลุ่มกะ2',
      'กลุ่มกะ3',
      'กลุ่มกะ4',
      'กลุ่มกะ5'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codgrpwork: mockcodgrpwork[i - 1],
        descodgrpworke: mockdescgrpwork[i - 1],
        descodgrpworkt: mockdescgrpworkt[i - 1],
        descodgrpwork3: mockdescgrpwork[i - 1],
        descodgrpwork4: mockdescgrpwork[i - 1],
        descodgrpwork5: mockdescgrpwork[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtypinc () {
    let mockRows = []
    const mockcodtypinc = [
      '1101',
      '1201',
      '1301',
      '1401',
      '1501'
    ]
    const mockdesctypinc = [
      'เงินเดือน ค่าจ้าง ฯลฯ กรณีทั่วไป',
      'เงินเดือน ค่าจ้าง ฯลฯ กรณีหักอัตรา ร้อยละ3',
      'เงินได้จ่ายครั้งเดียว',
      'เงินได้มาตรา 40(2) ผู้มีเงินได้อยู่ในประเทศไทย',
      'เงินได้มาตรา 40(2) ผู้มีเงินได้อยู่นอกประเทศ'
    ]
    const mockTotal = 5
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtypinc: mockcodtypinc[i - 1],
        descodtypince: mockdesctypinc[i - 1],
        descodtypinct: mockdesctypinc[i - 1],
        descodtypinc3: mockdesctypinc[i - 1],
        descodtypinc4: mockdesctypinc[i - 1],
        descodtypinc5: mockdesctypinc[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovNumotreq () {
    let mockRows = []
    const mockCodcomp = [
      '15-07-000001',
      '15-07-000002',
      '16-07-000001',
      '16-07-000002',
      '16-07-000003',
      '16-07-000004',
      '16-07-000005',
      '16-07-000006',
      '17-04-000001',
      '17-04-000002'
    ]
    const mockNamempt = [
      'น.ส.ศุภัคสร  เอนก',
      'นายกมล ก่อไผ่น้อย',
      'นายชุติพงษ์ ทองทา',
      'นางฐานิดา ครรชิต',
      'นางฐานิดา ครรชิต',
      'นายวิโรจน์ สมรัตน์',
      'น.ส.ศุภัคสร  เอนก',
      'นายกมล ก่อไผ่น้อย',
      'นายสมศักดิ์  ประกอบ',
      'นายวันชัย สมชาย'
    ]
    const mockNamemp = [
      'Miss.SUPUCSORN  ANEK',
      'MR.KAMON KOPHAINOI',
      'MR.CHUTIPONG THONGTA',
      'Mrs.THANIDA KANCHID',
      'Mrs.THANIDA KANCHID',
      'Mr.VIROJ SOMRAT',
      'Miss.SUPUCSORN  ANEK',
      'MR.KAMON KOPHAINOI',
      'Mr.SOMSAK  PRAKOB',
      'Mr.WANCHAI SOMCHAI'
    ]
    const mockCodcompt = [
      'ฝ่ายขายและการตลาด',
      'แผนกติดตั้ง/ออกแบบ/ตรวจสอบวางแผนพัฒนางาน',
      'แผนกติดตั้ง/ออกแบบ/ตรวจสอบวางแผนพัฒนางาน',
      'ฝ่ายดำเนินการและพัฒนาระบบ',
      'ฝ่ายดำเนินการและพัฒนาระบบ',
      'ฝ่ายการเงิน',
      'ฝ่ายขายและการตลาด',
      'แผนกติดตั้ง/ออกแบบ/ตรวจสอบวางแผนพัฒนางาน',
      'ฝ่ายการเงิน',
      'แผนกติดตั้ง/ออกแบบ/ตรวจสอบวางแผนพัฒนางาน'
    ]
    const mockNamcalen = [
      'พนักงานทั่วไป สำนักงานใหญ่',
      'พนักงานทั่วไป สาขารัชโยธิน',
      'พนักงานกะ 1',
      'พนักงานกะ 2',
      'Office'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        numotreq: mockCodcomp[i - 1],
        namempe: mockNamemp[i - 1],
        namempt: mockNamempt[i - 1],
        namemp3: mockNamemp[i - 1],
        namemp4: mockNamemp[i - 1],
        namemp5: mockNamemp[i - 1],
        namcent: mockCodcompt[i - 1],
        namcalen: mockNamcalen[Math.floor(Math.random() * 4) + 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtyppayr () {
    let mockRows = []
    const mockcodtyppayr = [
      '0101',
      '0201',
      '0210',
      '0230',
      '0301',
      '0401',
      '0501',
      '1101',
      '1201',
      '1301',
      '1401',
      '1501',
      '1601'
    ]
    const mockdesctyppayr = [
      'เงินเดือน/ค่าแรง',
      'ค่าตำแหน่ง',
      'ค่าครองชีพ',
      'ค่าวิชาชีพ',
      'Grade Allowance',
      'Supervisory Support Allow',
      'ค่าเบี้ยเลี้ยงนักศึกษา',
      'ค่าล่วงเวลา',
      'เบี้ยขยัน',
      'ค่ากะ',
      'ค่าอาหาร',
      'ค่าอยู่เวร',
      'ค่าโทรศัพท์'
    ]
    const mockTotal = 13
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtyppayr: mockcodtyppayr[i - 1],
        descodtyppayre: mockdesctyppayr[i - 1],
        descodtyppayrt: mockdesctyppayr[i - 1],
        descodtyppayr3: mockdesctyppayr[i - 1],
        descodtyppayr4: mockdesctyppayr[i - 1],
        descodtyppayr5: mockdesctyppayr[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtyppayt () {
    let mockRows = []
    const mockcodtyppayt = [
      '2101',
      '2201',
      '2301',
      '2401',
      '2501',
      '2601'
    ]
    const mockdesctyppayt = [
      'เงินเดือน ค่าจ้าง โบนัส ฯลฯ มาตรา 40(1)',
      'ค่าธรรมเนียม ค่านายหน้า ฯลฯ มาตรา 40(2)',
      'ค่าลิขสิทธิ์ ฯลฯ มาตรา 40(3)',
      'ดอกเบี้ย เงินปันผล ฯลฯ มาตรา 40(4)',
      'เงินได้ต้องหักภาษี ตาม มาตรา 3 เตรส',
      'เงินได้จ่ายครั้งเดียวออกจากงาน'
    ]
    const mockTotal = 6
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtyppayt: mockcodtyppayt[i - 1],
        descodtyppayte: mockdesctyppayt[i - 1],
        descodtyppaytt: mockdesctyppayt[i - 1],
        descodtyppayt3: mockdesctyppayt[i - 1],
        descodtyppayt4: mockdesctyppayt[i - 1],
        descodtyppayt5: mockdesctyppayt[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodrem () {
    let mockRows = []
    const mockCodrem = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '9999'
    ]
    const mockDescCodrem = [
      'งานนอกเวลา',
      'งานด่วน',
      'งานตามหัวหน้าสั่ง',
      'งานเพิ่ม',
      'งานพิเศษ',
      'N/A'
    ]
    const mockTotal = 6
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codrem: mockCodrem[i - 1],
        descodreme: mockDescCodrem[i - 1],
        descodremt: mockDescCodrem[i - 1],
        descodrem3: mockDescCodrem[i - 1],
        descodrem4: mockDescCodrem[i - 1],
        descodrem5: mockDescCodrem[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtax () {
    let mockRows = []
    const mockcodtax = [
      'Q2',
      'T1',
      'T2',
      'T3',
      'T4',
      'T5',
      'T6',
      'T7',
      'T8',
      'T9',
      'TC',
      'TD'
    ]
    const mockdesccodtax = [
      'Tax ภาษีค่าจ้างนอกระบบ',
      'ภาษีเงินได้',
      'ภาษีค่าล่วงเวลา',
      'ภาษีค่าเบี้ยขยันรายเดือน หัก ณ.ที่จ',
      'ภาษีค่ากะ',
      'ภาษีจ้างทำของ',
      'ภาษีค่าอยู่เวรพิเศษ (18)',
      'ภาษีโบนัส',
      'ภาษีค่า Commision',
      'ภาษีค่านายหน้า',
      'ภาษีเงินปีใหม่',
      'ภาษีเงินได้จ่ายครั้งเดียว'
    ]
    const mockTotal = 12
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtax: mockcodtax[i - 1],
        descodtaxe: mockdesccodtax[i - 1],
        descodtaxt: mockdesccodtax[i - 1],
        descodtax3: mockdesccodtax[i - 1],
        descodtax4: mockdesccodtax[i - 1],
        descodtax5: mockdesccodtax[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovTypeleave () {
    let mockRows = []
    const mockTypeleave = [ 'A', 'B', 'C', 'D', 'E', 'F', 'H', 'I', 'O', 'S', 'T', 'U', 'X', 'Y', 'Z' ]
    const mockDescTypeleave = [
      'ลากิจธุระ',
      'ลาครอบครัวถึงแก่กรรม',
      'ลาอุปสมบท',
      'ลาสมรส',
      'ลากิจภรรยาลาคลอดบุตร',
      'ลาเกี่ยวกับราชการทหาร',
      'ลากิจกรรมสหภาพแรงงาน',
      'ลาป่วยเนื่องจากรถบริษัท',
      'ลาหยุดการผลิต',
      'ลาป่วย',
      'ลาคลอดบุตร',
      'ลาเพื่อทำหมัน',
      'ลาชดเชย OT',
      'ลาป่วย หรือ บาดเจ็บ ในการปฏิบัติงาน',
      'ลาพักร้อน'
    ]
    const mockTotal = 15
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        typeleave: mockTypeleave[i - 1],
        destypeleavee: mockDescTypeleave[i - 1],
        destypeleavet: mockDescTypeleave[i - 1],
        destypeleave3: mockDescTypeleave[i - 1],
        destypeleave4: mockDescTypeleave[i - 1],
        destypeleave5: mockDescTypeleave[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodpay () {
    let mockRows = []
    const mockCodpay = [
      '01',
      '02',
      '03',
      '04',
      '05',
      '06',
      '07',
      '10',
      '11',
      '12',
      '13',
      '14',
      '15',
      '16',
      '20',
      '3C',
      '61',
      '62',
      '63',
      '64',
      '81',
      '82',
      '83',
      '84',
      '86',
      '91',
      '92',
      '93',
      '94',
      '95'
    ]
    const mockDescCodpay = [
      'Salary',
      'Position',
      'Grade Allow',
      'Supervisory Support Allow',
      'ค่าครองชีพ',
      'ค่าวิชาชีพ',
      'เงินเบิกล่วงหน้า',
      'จ้างทำของ',
      'ค่าล่วงเวลา',
      'ค่าเบี้ยขยันรายเดือน',
      'ค่าเบี้ยขยันรายสัปดาห์',
      'ค่าอาหาร',
      'ค่าอาหาร (ล่วงเวลา) บ.ออกให้',
      'ค่าเบี้ยเลี้ยงพิเศษออกให้ครั้งเดียว',
      'ค่ากะ',
      'ค่ารถ (Transportation)',
      'หักเงินกู้',
      'หักกู้ที่อยู่อาศัย',
      'หักทำความผิด',
      'สมาชิกสหภาพ',
      'หักขาดงาน',
      'หักลาเกินกำหนด',
      'หักสาย',
      'หักกลับก่อน',
      'หักบังคับคดี15%',
      'รายการหักอื่นๆ ประจำ (คำนวณภาษี)',
      'รายการหักอื่นๆ ประจำ (ไม่คำนวณภาษี)',
      'รายการหักอื่นๆ ไม่ประจำ (คำนวณภาษี)',
      'รายการหักอื่นๆ ไม่ประจำ (ไม่คำนวณภาษี)',
      'Provident Fund (Company Dis)'
    ]
    const mockTotal = 30
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codpay: mockCodpay[i - 1],
        descodpaye: mockDescCodpay[i - 1],
        descodpayt: mockDescCodpay[i - 1],
        descodpay3: mockDescCodpay[i - 1],
        descodpay4: mockDescCodpay[i - 1],
        descodpay5: mockDescCodpay[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodpayrol () {
    let mockRows = []
    const mockcodpayrol = [
      '0001',
      '0002',
      '0003',
      '01',
      '02',
      '03',
      '04',
      'M1',
      'M2',
      'XXX1'
    ]
    const mockdesccodpayrol = [
      'รายเดือน',
      'รายวัน',
      'นักศึกษา',
      'รายเดือน ผู้บริหาร สำนักงานใหญ่',
      'รายเดือน สำนักงาน ใหญ่',
      'รายเดือน สาขา',
      'รายวัน สาขา',
      'รายได้ประจำเดือนละ 1 งวด',
      'รายได้ประจำเดือนละ 2 งวด',
      'XXXX-รายเดือน-XXXX'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codpayrol: mockcodpayrol[i - 1],
        descodpayrole: mockdesccodpayrol[i - 1],
        descodpayrolt: mockdesccodpayrol[i - 1],
        descodpayrol3: mockdesccodpayrol[i - 1],
        descodpayrol4: mockdesccodpayrol[i - 1],
        descodpayrol5: mockdesccodpayrol[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodfund () {
    let mockRows = []
    const mockcodfund = [
      '0001',
      '0002',
      '0003',
      '004',
      '01',
      '02',
      'KFUN',
      'KTAM',
      'SAS',
      'SCB',
      'TH01',
      'TIS',
      'TT1'
    ]
    const mockdesccodfund = [
      'กองทุนสำรองเลี้ยงชีพ #1',
      'กองทุนสำหรับพนักงานทั่วไป',
      'กองทุนทหารไทย',
      'ธนาคารกรุงเทพ',
      'กองทุน PF ลงทุนในหุ้น 10%',
      'กองทุน PF ลงทุนในหุ้น 25%',
      'ธ.กสิกรไทย',
      'KTAM',
      'กองทุนบริษัท SAS',
      'กองทุนไทยพาณิชย์',
      'กองทุนไทยมั่นคง',
      'Tisco bank',
      'กองทุน PF บ.TT1'
    ]
    const mockTotal = 13
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codfund: mockcodfund[i - 1],
        descodfunde: mockdesccodfund[i - 1],
        descodfundt: mockdesccodfund[i - 1],
        descodfund3: mockdesccodfund[i - 1],
        descodfund4: mockdesccodfund[i - 1],
        descodfund5: mockdesccodfund[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodplan () {
    let mockRows = []
    const mockcodplan = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006'
    ]
    const mockdesccodplan = [
      'แผนการลงทุนในหุ้น 10%',
      'แผนการลงทุนในหุ้น 25%',
      'แผนการลงทุนที่ 1 100%',
      'แผนการลงทุนที่ 2 100%',
      'แผนที่มีความเสี่ยงสูง',
      'แผนที่มีความเสี่ยงต่ำ'
    ]
    const mockTotal = 6
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codplan: mockcodplan[i - 1],
        descodplane: mockdesccodplan[i - 1],
        descodplant: mockdesccodplan[i - 1],
        descodplan3: mockdesccodplan[i - 1],
        descodplan4: mockdesccodplan[i - 1],
        descodplan5: mockdesccodplan[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodcause () {
    let mockRows = []
    const mockcodcause = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '9999'
    ]
    const mockdesccodcause = [
      'งานนอกเวลา',
      'งานด่วน',
      'งานตามหัวหน้าสั่ง',
      'งานเพิ่ม',
      'งานพิเศษ',
      'N/A'
    ]
    const mockTotal = 6
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codcause: mockcodcause[i - 1],
        descodcausee: mockdesccodcause[i - 1],
        descodcauset: mockdesccodcause[i - 1],
        descodcause3: mockdesccodcause[i - 1],
        descodcause4: mockdesccodcause[i - 1],
        descodcause5: mockdesccodcause[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovTyprewd () {
    let mockRows = []
    const mocktyprewd = [
      '0001',
      '0002',
      '0003',
      '0004',
      'M1',
      'M2'
    ]
    const mockdesctyprewd = [
      'รางวัลพนักงานดีเด่น',
      'สหกรณ์ออมทรัพย์',
      'สหภาพแรงงาน',
      'สมาชิกฌาปนกิจสงเคราะห์',
      'เสื้อโปโลสีน้ำเงิน',
      'เสื้อแจ๊กเก็ต'
    ]
    const mockTotal = 6
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        typrewd: mocktyprewd[i - 1],
        destyprewde: mockdesctyprewd[i - 1],
        destyprewdt: mockdesctyprewd[i - 1],
        destyprewd3: mockdesctyprewd[i - 1],
        destyprewd4: mockdesctyprewd[i - 1],
        destyprewd5: mockdesctyprewd[i - 1]

      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCountry () {
    let mockRows = []
    const mockcountry = [
      'C001',
      'C002',
      'C003',
      'C004',
      'C005',
      'C006',
      'C007',
      'C008',
      'C009',
      'C010',
      'C020',
      'C021',
      'C022',
      'C023',
      'C024'
    ]
    const mockdesccountry = [
      'สิงคโปร์',
      'มาเลเซีย',
      'ลาว',
      'พม่า',
      'ญี่ปุ่น',
      'ไทย',
      'กัมพูชา',
      'อินโดนีเซีย',
      'เวียดนาม',
      'ฟิลิปปินส์',
      'กาหลีเหนือ',
      'จีน',
      'มัลดีฟส์',
      'มองโกเลีย',
      'อิรัก'
    ]
    const mockTotal = 15
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        country: mockcountry[i - 1],
        descountrye: mockdesccountry[i - 1],
        descountryt: mockdesccountry[i - 1],
        descountry3: mockdesccountry[i - 1],
        descountry4: mockdesccountry[i - 1],
        descountry5: mockdesccountry[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodedlv () {
    let mockRows = []
    const mockcodedlv = [
      '0000',
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010'
    ]
    const mockdesccodedlv = [
      'N/A',
      'ปริญญาเอก',
      'ปริญญาโท',
      'ปริญญาตรี',
      'อนุปริญญา',
      'ประกาศนียบัตรวิชาชีพ (ปวท.)',
      'ประกาศนียบัตรวิชาชีพชั้นสูง (ปวส.)',
      'ประกาศนียบัตรวิชาชีพ (ปวช.)',
      'มัธยมศึกษาตอนปลาย',
      'มัธยมศึกษาตอนต้น',
      'ประถมศึกษา'
    ]
    const mockTotal = 11
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codedlv: mockcodedlv[i - 1],
        descodedlve: mockdesccodedlv[i - 1],
        descodedlvt: mockdesccodedlv[i - 1],
        descodedlv3: mockdesccodedlv[i - 1],
        descodedlv4: mockdesccodedlv[i - 1],
        descodedlv5: mockdesccodedlv[i - 1]

      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodnatnl () {
    let mockRows = []
    const mockcodnatnl = [
      '0000',
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007'
    ]
    const mockdesccodnatnl = [
      'N/A',
      'ไทย',
      'จีน',
      'อเมริกา',
      'อังกฤษ',
      'เยอรมัน',
      'ญี่ปุ่น',
      'ลาว'
    ]
    const mockTotal = 8
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codnatnl: mockcodnatnl[i - 1],
        descodnatnle: mockdesccodnatnl[i - 1],
        descodnatnlt: mockdesccodnatnl[i - 1],
        descodnatnl3: mockdesccodnatnl[i - 1],
        descodnatnl4: mockdesccodnatnl[i - 1],
        descodnatnl5: mockdesccodnatnl[i - 1]

      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodrelgn () {
    let mockRows = []
    const mockcodrelgn = [
      '0001',
      '0002',
      '0003',
      '0004'
    ]
    const mockdesccodrelgn = [
      'พุทธ',
      'คริสต์',
      'อิสลาม',
      'ฮินดู'
    ]
    const mockTotal = 4
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codrelgn: mockcodrelgn[i - 1],
        descodrelgne: mockdesccodrelgn[i - 1],
        descodrelgnt: mockdesccodrelgn[i - 1],
        descodrelgn3: mockdesccodrelgn[i - 1],
        descodrelgn4: mockdesccodrelgn[i - 1],
        descodrelgn5: mockdesccodrelgn[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCoddist () {
    let mockRows = []
    const mockcoddist = [
      'D1',
      'D2',
      'D3',
      'D4',
      'D5',
      'D6',
      'D7',
      'D8',
      'D9',
      'D10'
    ]
    const mockdesccoddist = [
      'ทองผาภูมิ',
      'พุนพิน',
      'กาญจนดิษฐ์',
      'คลองเตย',
      'คลองสาน',
      'จตุจักร',
      'จอมทอง',
      'ดอนเมือง',
      'บางเขน',
      'บางรัก'
    ]
    const mockdesccodprov = [
      'กาญจนบุรี',
      'สุราษฎ์ธานี',
      'สุราษฎ์ธานี',
      'กรุงเทพมหานคร',
      'กรุงเทพมหานคร',
      'กรุงเทพมหานคร',
      'กรุงเทพมหานคร',
      'กรุงเทพมหานคร',
      'กรุงเทพมหานคร',
      'กรุงเทพมหานคร'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        coddist: mockcoddist[i - 1],
        descoddiste: mockdesccoddist[i - 1],
        descoddistt: mockdesccoddist[i - 1],
        descoddist3: mockdesccoddist[i - 1],
        descoddist4: mockdesccoddist[i - 1],
        descoddist5: mockdesccoddist[i - 1],
        descodprove: mockdesccodprov[i - 1],
        descodprovt: mockdesccodprov[i - 1],
        descodprov3: mockdesccodprov[i - 1],
        descodprov4: mockdesccodprov[i - 1],
        descodprov5: mockdesccodprov[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCoddglv () {
    let mockRows = []
    const mockcoddglv = [
      '0000',
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010',
      '0011',
      '0012',
      '0013',
      '0014',
      '0015',
      '0016',
      '0017',
      '0018',
      '0019',
      '0020'
    ]
    const mockdesccoddglv = [
      'ปริญญาเอก',
      'การศึกษาบัณฑิต',
      'การออกแบบอุตสาหกรรมบัณฑิต',
      'เกษตรศาสตรบัณฑิต',
      'ครุศาสตรบัณฑิต',
      'ครุศาสตรบัณฑิต (พยาบาลศึกษา)',
      'ครุศาสตร์อุตสาหกรรมบัณฑิต',
      'คหกรรมศาสตรบัณฑิต',
      'ทันตแพทยศาสตรบัณฑิต',
      'เทคโนโลยีการเกษตรบัณฑิต',
      'เทคโนโลยีบัณฑิต (เทคโนโลยีสารสนเทศธุรกิจ)',
      'นิติศาสตรบัณฑิต',
      'นิเทศศาสตรบัณฑิต',
      'บริหารธุรกิจบัณฑิต',
      'บัญชีบัณฑิต',
      'ผดุงครรภ์ชั้นสูง',
      'ประกาศนียบัตรวิชาพยาบาลศาสตร์',
      'ประกาศนียบัตรครูเทคนิคขั้นสูง',
      'พยาบาลศาสตรบัณฑิต',
      'แพทยศาสตรบัณฑิต',
      'พุทธศาสตรบัณฑิต'
    ]
    const mockTotal = 21
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        coddglv: mockcoddglv[i - 1],
        descoddglve: mockdesccoddglv[i - 1],
        descoddglvt: mockdesccoddglv[i - 1],
        descoddglv3: mockdesccoddglv[i - 1],
        descoddglv4: mockdesccoddglv[i - 1],
        descoddglv5: mockdesccoddglv[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodmajsb () {
    let mockRows = []
    const mockcodmajsb = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010',
      '0011',
      '0012',
      '0013',
      '0014',
      '0015',
      '0016',
      '0017',
      '0018',
      '0019',
      '0020'
    ]
    const mockdesccodmajsb = [
      'บัญชี',
      'การเงินการธนาคาร',
      'การตลาด',
      'การจัดการ',
      'ธุรกิจต่างประเทศ',
      'ประชาสัมพันธ์',
      'การจัดการการผลิต',
      'เศรษศาสตร์การเงิน',
      'เศรษศาสตร์ มหาภาค',
      'เศรษศาสตร์ระหว่าประเทศ',
      'เศรษศาสตร์การจัดการ',
      'เศรษฐศาสตร์แรงงานและทรัพยากรมนุษย์',
      'เศรษฐศาสตร์ปริมาณวิเคราะห์',
      'เศรษฐศาสตร์การเกษตรและธุรกิจการเกษตร',
      'เศรษฐศาสตร์สหกรณ์',
      'กฎหมาย',
      'กฏหมายธุรกิจ',
      'กฏหมายระหว่างประเทศ',
      'สถิติ',
      'คอมพิวเตอร์'
    ]
    const mockTotal = 20
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codmajsb: mockcodmajsb[i - 1],
        descodmajsbe: mockdesccodmajsb[i - 1],
        descodmajsbt: mockdesccodmajsb[i - 1],
        descodmajsb3: mockdesccodmajsb[i - 1],
        descodmajsb4: mockdesccodmajsb[i - 1],
        descodmajsb5: mockdesccodmajsb[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodminsb () {
    let mockRows = []
    const mockcodminsb = [
      '0000',
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010',
      '0011',
      '0012',
      '0013',
      '9999'
    ]
    const mockdesccodminsb = [
      'N/A',
      'การเงิน การธนาคาร',
      'คอมพิวเตอร์ธุรกิจ',
      'อิเล็กทรอนิกส์',
      'การไฟฟ้า',
      'โยธา',
      'บัญชี',
      'การจัดการ',
      'สถาปัตย์',
      'วิทยาศาสตร์',
      'วิทยาการคอม',
      'การสื่อสาร',
      'เทคโนโลยีสารสนเทศ',
      'นิเทศศาสตร์',
      'อื่นๆ'
    ]
    const mockTotal = 15
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codminsb: mockcodminsb[i - 1],
        descodminsbe: mockdesccodminsb[i - 1],
        descodminsbt: mockdesccodminsb[i - 1],
        descodminsb3: mockdesccodminsb[i - 1],
        descodminsb4: mockdesccodminsb[i - 1],
        descodminsb5: mockdesccodminsb[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodinst () {
    let mockRows = []
    const mockcodinst = [
      '0000',
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010',
      '0011',
      '0012',
      '0013',
      '0014'
    ]
    const mockdesccodinst = [
      'N/A',
      'จุฬาลงกรณ์มหาวิทยาลัย',
      'มหาวิทยาลัยเกษตรศาสตร์',
      'มหาวิทยาลัยขอนแก่น',
      'มหาวิทยาลัยเชียงใหม่',
      'มหาวิทยาลัยทักษิณ',
      'มหาวิทยาลัยเทคโนโลยีพระจอมเกล้าธนบุรี',
      'มหาวิทยาลัยเทคโนโลยีสุรนารี',
      'มหาวิทยาลัยธรรมศาสตร์',
      'มหาวิทยาลัยนเรศวร',
      'มหาวิทยาลัยบูรพา',
      'มหาวิทยาลัยมหาสารคาม',
      'มหาวิทยาลัยมหิดล',
      'มหาวิทยาลัยแม่โจ้',
      'มหาวิทยาลัยแม่ฟ้าหลวง'
    ]
    const mockTotal = 15
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codinst: mockcodinst[i - 1],
        descodinste: mockdesccodinst[i - 1],
        descodinstt: mockdesccodinst[i - 1],
        descodinst3: mockdesccodinst[i - 1],
        descodinst4: mockdesccodinst[i - 1],
        descodinst5: mockdesccodinst[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodsubdist () {
    let mockRows = []
    const mockcodsubdist = [
      '2601',
      '2602',
      '2603',
      '1201',
      '2301',
      '1541',
      '1041',
      '1042',
      '1043',
      '1045'
    ]
    const mockdesccodsubdist = [
      'ท่าเรือ',
      'บางปะอิน',
      'ลาดบัวหลวง',
      'วังน้อย',
      'ด่านขุนทด',
      'เฉลิมพระเกียรติ',
      'หลักสี่',
      'คลองเตย',
      'คลองสามวา',
      'บางกะปิ'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codsubdist: mockcodsubdist[i - 1],
        descodsubdiste: mockdesccodsubdist[i - 1],
        descodsubdistt: mockdesccodsubdist[i - 1],
        descodsubdist3: mockdesccodsubdist[i - 1],
        descodsubdist4: mockdesccodsubdist[i - 1],
        descodsubdist5: mockdesccodsubdist[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodbrlc () {
    let mockRows = []
    const mockcodbrlc = [
      '0001',
      '0002',
      '0003',
      '0004',
      '01',
      '02',
      '03',
      '04',
      '1000',
      'ALUM',
      'CAST',
      'ENGI',
      'OFFI'
    ]
    const mockdesccodbrlc = [
      'สำนักงานใหญ่',
      'สำนักงานสาขา รัชโยธิน',
      'สาขาย่อย 2',
      'สาขาย่อย 3',
      'สำนักงานใหญ่',
      'สาขา-สถานีบริการน้ำมัน',
      'สาขา-มินิมาร์ท',
      'สาขา-อินทนิล',
      'Amata',
      'Aluminium',
      'Casting',
      'Engine',
      'Office'
    ]
    const mockTotal = 13
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codbrlc: mockcodbrlc[i - 1],
        descodbrlce: mockdesccodbrlc[i - 1],
        descodbrlct: mockdesccodbrlc[i - 1],
        descodbrlc3: mockdesccodbrlc[i - 1],
        descodbrlc4: mockdesccodbrlc[i - 1],
        descodbrlc5: mockdesccodbrlc[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodempmt () {
    let mockRows = []
    const mockcodempmt = [
      '0001',
      '0002',
      '0003',
      '0004',
      '01',
      '02',
      '03',
      '1000',
      'D',
      'M',
      'PE',
      'SC',
      'TE'
    ]
    const mockdesccodempmt = [
      'รายเดือน',
      'รายวัน',
      'นักศึกษาฝึกงาน',
      'สัญญาจ้าง',
      'รายเดือน',
      'รายวัน',
      'Part Time',
      'รายเดือน สนญ.',
      'รายวัน',
      'รายเดือน',
      'Permanent',
      'Sub Contract',
      'Tempolary'
    ]
    const mockTotal = 13
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codempmt: mockcodempmt[i - 1],
        descodempmte: mockdesccodempmt[i - 1],
        descodempmtt: mockdesccodempmt[i - 1],
        descodempmt3: mockdesccodempmt[i - 1],
        descodempmt4: mockdesccodempmt[i - 1],
        descodempmt5: mockdesccodempmt[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtypemp () {
    let mockRows = []
    const mockcodtypemp = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '01',
      '02',
      'DI',
      'ENGI',
      'IN',
      'JS',
      'OFFI',
      'TS'
    ]
    const mockdesccodtypemp = [
      'รายเดือน (พ.ประจำ)',
      'ผู้บริหาร/ผู้จัดการญี่ปุ่น',
      'พนักงานสัญญาจ้าง',
      'นักศึกษาฝึกงาน',
      'ที่ปรึกษา',
      'รายเดือน',
      'รายวัน',
      'Direct',
      'Engineer',
      'Indirect',
      'Japanese Staff',
      'Office',
      'Thai Staff'
    ]
    const mockTotal = 13
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtypemp: mockcodtypemp[i - 1],
        descodtypempe: mockdesccodtypemp[i - 1],
        descodtypempt: mockdesccodtypemp[i - 1],
        descodtypemp3: mockdesccodtypemp[i - 1],
        descodtypemp4: mockdesccodtypemp[i - 1],
        descodtypemp5: mockdesccodtypemp[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodjob () {
    let mockRows = []
    const mockcodjob = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '01',
      '02',
      'DI',
      'ENGI',
      'IN',
      'JS',
      'OFFI',
      'TS'
    ]
    const mockdesccodjob = [
      'รายเดือน (พ.ประจำ)',
      'ผู้บริหาร/ผู้จัดการญี่ปุ่น',
      'พนักงานสัญญาจ้าง',
      'นักศึกษาฝึกงาน',
      'ที่ปรึกษา',
      'รายเดือน',
      'รายวัน',
      'Direct',
      'Engineer',
      'Indirect',
      'Japanese Staff',
      'Office',
      'Thai Staff'
    ]
    const mockTotal = 13
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codjob: mockcodjob[i - 1],
        descodjobe: mockdesccodjob[i - 1],
        descodjobt: mockdesccodjob[i - 1],
        descodjob3: mockdesccodjob[i - 1],
        descodjob4: mockdesccodjob[i - 1],
        descodjob5: mockdesccodjob[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodjobgrad () {
    let mockRows = []
    const mockcodjobgrad = [
      'BB',
      'JG',
      'P',
      'P1',
      'S'
    ]
    const mockdesccodjobgrad = [
      'Test',
      'Job Grade',
      'Primary',
      'Daily Worker',
      'Staff'
    ]
    const mockTotal = 5
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codjobgrad: mockcodjobgrad[i - 1],
        descodjobgrade: mockdesccodjobgrad[i - 1],
        descodjobgradt: mockdesccodjobgrad[i - 1],
        descodjobgrad3: mockdesccodjobgrad[i - 1],
        descodjobgrad4: mockdesccodjobgrad[i - 1],
        descodjobgrad5: mockdesccodjobgrad[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovTypdisp () {
    let mockRows = []
    const mocktypdisp = [
      '01',
      '02',
      '03',
      '04',
      '05',
      '06',
      '07',
      '08',
      '09'
    ]
    const mockdesctypdisp = [
      'บุคคลที่มีความบกพร่องทางการเห็น',
      'การได้ยิน',
      'ทางสติปัญญา',
      'ทางร่างกาย หรือการเคลื่อนไหว หรือสุขภาพ',
      'ทางการเรียนรู้',
      'ทางการพูด และภาษา',
      'ทางพฤติกรรม หรืออารมณ์',
      'ออทิสติก',
      'พิการซ้อน'
    ]
    const mockTotal = 9
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        typdisp: mocktypdisp[i - 1],
        destypdispe: mockdesctypdisp[i - 1],
        destypdispt: mockdesctypdisp[i - 1],
        destypdisp3: mockdesctypdisp[i - 1],
        destypdisp4: mockdesctypdisp[i - 1],
        destypdisp5: mockdesctypdisp[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodgl () {
    let mockRows = []
    const mockcodgl = [
      '01',
      '02',
      '99',
      'DIR'
    ]
    const mockdesccodgl = [
      '01xxx',
      '02xxx',
      'ไม่ระบุ',
      'Direct'
    ]
    const mockTotal = 4
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codgl: mockcodgl[i - 1],
        descodgle: mockdesccodgl[i - 1],
        descodglt: mockdesccodgl[i - 1],
        descodgl3: mockdesccodgl[i - 1],
        descodgl4: mockdesccodgl[i - 1],
        descodgl5: mockdesccodgl[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodpoint () {
    let mockRows = []
    const mockcodpoint = [
      '01',
      '02',
      '03',
      '04',
      '05'
    ]
    const mockdesccodpoint = [
      'สายใต้ใหม่',
      'บางใหญ่',
      'หมอชิต',
      'หัวลำโพง',
      'เตาปูน'
    ]
    const mockTotal = 5
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codpoint: mockcodpoint[i - 1],
        descodpointe: mockdesccodpoint[i - 1],
        descodpointt: mockdesccodpoint[i - 1],
        descodpoint3: mockdesccodpoint[i - 1],
        descodpoint4: mockdesccodpoint[i - 1],
        descodpoint5: mockdesccodpoint[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodbank () {
    let mockRows = []
    const mockcodbank = [
      '0002',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010'
    ]
    const mockdesccodbank = [
      'ธนาคารกรุงเทพจำกัด(มหาชน)',
      'ธนาคารกสิกรไทย',
      'ธนาคาร ABN AMRO',
      'ธนาคารกรุงไทย',
      'ธนาคารทหารไทย',
      'ธนาคารไทยพานิชย์',
      'ธนาคารซิตี้แบงค์',
      'ธนาคารกรุงศรีอยุธยา'
    ]
    const mockTotal = 8
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codbank: mockcodbank[i - 1],
        descodbanke: mockdesccodbank[i - 1],
        descodbankt: mockdesccodbank[i - 1],
        descodbank3: mockdesccodbank[i - 1],
        descodbank4: mockdesccodbank[i - 1],
        descodbank5: mockdesccodbank[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodcurr () {
    let mockRows = []
    const mockcodcurr = [
      '$',
      '0002',
      '0003',
      '0004',
      '01',
      'BAHT',
      'HK.$'
    ]
    const mockdesccodcurr = [
      'ดอลล่าร์ U.S.',
      'ดอลลาร์สหรัฐฯ',
      'ยูโร',
      'เยน',
      'บาท',
      'บาท',
      'ดอลล่าร์ H.K..'
    ]
    const mockTotal = 7
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codcurr: mockcodcurr[i - 1],
        descodcurre: mockdesccodcurr[i - 1],
        descodcurrt: mockdesccodcurr[i - 1],
        descodcurr3: mockdesccodcurr[i - 1],
        descodcurr4: mockdesccodcurr[i - 1],
        descodcurr5: mockdesccodcurr[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtypdoc () {
    let mockRows = []
    const mockcodtypdoc = [
      'PS',
      'RS',
      'TS'
    ]
    const mockdesccodtypdoc = [
      'testscript',
      'resume',
      'transript'
    ]
    const mockTotal = 3
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtypdoc: mockcodtypdoc[i - 1],
        descodtypdoce: mockdesccodtypdoc[i - 1],
        descodtypdoct: mockdesccodtypdoc[i - 1],
        descodtypdoc3: mockdesccodtypdoc[i - 1],
        descodtypdoc4: mockdesccodtypdoc[i - 1],
        descodtypdoc5: mockdesccodtypdoc[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtypcolla () {
    let mockRows = []
    const mockcodtypcolla = [
      '0001',
      '0002',
      '0003'
    ]
    const mockdesccodtypcolla = [
      'ที่ดิน-อสังหาริมทรัพย์',
      'เงินฝากประจำ',
      'บ้าน'
    ]
    const mockTotal = 3
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtypcolla: mockcodtypcolla[i - 1],
        descodtypcollae: mockdesccodtypcolla[i - 1],
        descodtypcollat: mockdesccodtypcolla[i - 1],
        descodtypcolla3: mockdesccodtypcolla[i - 1],
        descodtypcolla4: mockdesccodtypcolla[i - 1],
        descodtypcolla5: mockdesccodtypcolla[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodreqst () {
    let mockRows = []
    const mockcodreqst = [
      '150101',
      '150101',
      '150102',
      '150103'
    ]
    const mockdesccodreqst = [
      'TJS-000-000-000-000-000-000 บริษัท พีเพิล พลัส',
      'TJS-000-000-000-000-000-000 บริษัท พีเพิล พลัส',
      'TJS-100-000-000-000-000-000 ฝ่ายดำเนินการและพัฒนาระบบ'
    ]
    const mockTotal = 4
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codreqst: mockcodreqst[i - 1],
        descodreqste: mockdesccodreqst[i - 1],
        descodreqstt: mockdesccodreqst[i - 1],
        descodreqst3: mockdesccodreqst[i - 1],
        descodreqst4: mockdesccodreqst[i - 1],
        descodreqst5: mockdesccodreqst[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodmenu () {
    let mockRows = []
    const mockcodmenu = [
      '1.RP',
      '2.RC',
      '3.PM',
      '4.TR',
      '5-AP',
      '6.AL',
      '7.BF',
      '8.PY',
      '9.MS',
      'A.ES',
      'AL1',
      'ASDF',
      'B.WL',
      'CO',
      'CO1',
      'CO2',
      'CO3',
      'H_PM',
      'MAL',
      'MSRP',
      'PM-S',
      'PM01',
      'PM9',
      'PMA',
      'PMGG',
      'PMRE',
      'RAL',
      'SC',
      'SDF'
    ]
    const mockdesccodmenu = [
      'ระบบงานวางแผนกำลังคน',
      'ระบบงานรับสมัครและว่าจ้าง',
      'ระบบงานทะเบียนประวัติพนักงาน',
      'ระบบงานพัฒนาบุคลากร',
      'ระบบงานประเมินผลและขึ้นเงินเดือน',
      'ระบบงานการมาปฏิบัติงานและลาหยุดงาน',
      'ระบบงานสวัสดิการพนักงาน',
      'ระบบงานจ่ายเงินเดือนพนักงาน',
      'ระบบงานข้อมูลสำหรับผู้บริหาร',
      'ระบบงานบริการข้อมูลด้วยตนเอง',
      'ระบบงานการมาปฏิบัติงานและลาหยุดงาน',
      'ระบบงานการมาปฏิบัติงานและลาหยุดงาน',
      'ระบบงานภายใน Office  Commission',
      'ระบบข้อมูลสนับสนุนงาน',
      'ระบบข้อมูลสนับสนุนงาน',
      'ระบบข้อมูลสนับสนุนงาน',
      'ระบบข้อมูลสนับสนุนงาน',
      'ระบบงานทะเบียนประวัติพนักงาน',
      'ระบบงานการมาปฏิบัติงานและลาหยุดงาน(Manager)',
      'ระบบรายงานสำหรับผู้บริหาร',
      'ระบบงานทะเบียนประวัติพนักงาน',
      'ระบบงานทะเบียนประวัติพนักงาน',
      'ระบบงานทะเบียนประวัติพนักงาน',
      'ระบบงานทะเบียนประวัติพนักงาน-Test',
      'ระบบงานทะเบียนประวัติพนักงาน',
      'รายงานระบบทะเบียนประวัติพนักงาน',
      'รายงานระบบงานการมาปฏิบัติงานและลาหยุดงาน',
      'ระบบ SECURITY',
      'ระบบงานทะเบียนประวัติพนักงาน'
    ]
    const mockTotal = 29
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codmenu: mockcodmenu[i - 1],
        descodmenue: mockdesccodmenu[i - 1],
        descodmenut: mockdesccodmenu[i - 1],
        descodmenu3: mockdesccodmenu[i - 1],
        descodmenu4: mockdesccodmenu[i - 1],
        descodmenu5: mockdesccodmenu[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodfunction () {
    let mockRows = []
    const mockcodfunction = [
      'FL',
      'F1',
      'F2',
      'F3',
      'F4',
      'F5'
    ]
    const mockdesccodfunction = [
      'LOGIN',
      'HRES71E',
      'HRES8AX',
      'HRES8AX',
      'HRES8AX'
    ]
    const mockTotal = 4
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codfunction: mockcodfunction[i - 1],
        descodfunctione: mockdesccodfunction[i - 1],
        descodfunctiont: mockdesccodfunction[i - 1],
        descodfunction3: mockdesccodfunction[i - 1],
        descodfunction4: mockdesccodfunction[i - 1],
        descodfunction5: mockdesccodfunction[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodskill () {
    let mockRows = []
    const mockcodskill = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010',
      '01',
      '1',
      'ACH',
      'ACH',
      'B200'
    ]
    const mockdesccodskill = [
      'ขับรถยนต์ส่วนตัว',
      'เล่นกอล์ฟ',
      'ยิงปืน',
      'ขี่ม้า',
      'Computer Office',
      'ภาษาอังกฤษ',
      'ภาษาต่างประเทศ',
      'ความรู้ด้าน HRMS',
      'fdsafasd',
      'ภาษาญี่ปุ่น',
      'ความสามารถในการเรียนรู้',
      'zzz',
      'Achievement Motivation',
      'Analytical Thinking',
      'สำหรับขยาย length tskilscor 200 คร้าบ'
    ]
    const mockTotal = 15
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codskill: mockcodskill[i - 1],
        descodskille: mockdesccodskill[i - 1],
        descodskillt: mockdesccodskill[i - 1],
        descodskill3: mockdesccodskill[i - 1],
        descodskill4: mockdesccodskill[i - 1],
        descodskill5: mockdesccodskill[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodaward () {
    let mockRows = []
    const mockcodaward = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      'T0D1',
      'T0D2',
      'T0M1',
      'T0M2',
      'Z001',
      'Z002'
    ]
    const mockdesccodaward = [
      'เบี้ยขยัน',
      'เงินรางวัล',
      'ค่าตอบแทนพิเศษ',
      'ค่า OSA',
      'เบี้ยขยันประเภท 2',
      'เบี้ยขยันประเภท 3',
      'พนักงานรายวัน G0 หยุด/ลางาน1ครั้งภายในเดือน',
      'พนักงานรายวัน G0 ไม่หยุด/ไม่ลา',
      'พนักงานรายเดือน G1 ทุกสาขา',
      'พนักงานรายเดือน G1-G6 มาทำงานวันหยุด',
      '1.ชุดข้อสอบสำหรับตำแหน่งพนักงานทั่วไป',
      '2.ชุดข้อสอบสำหรับตำแหน่งพนักงานทั่วไปด้านภาษา'
    ]
    const mockTotal = 12
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codaward: mockcodaward[i - 1],
        descodawarde: mockdesccodaward[i - 1],
        descodawardt: mockdesccodaward[i - 1],
        descodaward3: mockdesccodaward[i - 1],
        descodaward4: mockdesccodaward[i - 1],
        descodaward5: mockdesccodaward[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodfrgtcard () {
    let mockRows = []
    const mockcodfrgtcard = [
      '0001',
      '0002',
      '0003',
      '0004'
    ]
    const mockdesccodfrgtcard = [
      'บัตรหาย',
      'ลืมพกบัตร',
      'บัตรชำรุด',
      'อื่นๆ'
    ]
    const mockTotal = 4
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codfrgtcard: mockcodfrgtcard[i - 1],
        descodfrgtcarde: mockdesccodfrgtcard[i - 1],
        descodfrgtcardt: mockdesccodfrgtcard[i - 1],
        descodfrgtcard3: mockdesccodfrgtcard[i - 1],
        descodfrgtcard4: mockdesccodfrgtcard[i - 1],
        descodfrgtcard5: mockdesccodfrgtcard[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodtrn () {
    let mockRows = []
    const mockcodtrn = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '0008',
      '0009',
      '0010',
      '0011',
      '0012',
      '0013',
      '0014',
      '0015',
      '0018',
      '0019',
      '0020',
      '0021',
      '0022',
      '0023',
      '0034',
      '005x',
      '005y',
      '0099',
      '1',
      '10',
      '11',
      'ASDF',
      'TG'
    ]
    const mockdesccodtrn = [
      'พนักงานใหม่',
      'กลับเข้าทำงานใหม่',
      'ทดลองงาน',
      'ทดลองตำแหน่ง',
      'พ้นสภาพ',
      'ยกเลิกตำแหน่ง',
      'ปรับรายได้',
      'ปรับตำแหน่ง',
      'ปรับระดับ',
      'เลื่อนระดับ/ปรับตำแหน่ง',
      'แต่งตั้ง/เลื่อนระดับ',
      'โอนย้าย',
      'รักษาการณ์/ควบตำแหน่ง',
      'ปรับรายได้',
      'เลื่อนระดับ/ปรับเงิน',
      'ลดตำแหน่ง',
      'ปรับเงินประจำปี',
      'ทดลองรักษาการณ์',
      'czvcv',
      'xxx',
      'Transfer Group',
      'ย้ายที่ทำงาน',
      'เพิ่มเงิน',
      'อื่นๆ',
      '1',
      'ปรับระดับ',
      'เลื่อนระดับ/ปรับตำแหน่ง',
      'ทดองเคลื่อนไหว',
      'test01'
    ]
    const mockTotal = 30
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codtrn: mockcodtrn[i - 1],
        descodtrne: mockdesccodtrn[i - 1],
        descodtrnt: mockdesccodtrn[i - 1],
        descodtrn3: mockdesccodtrn[i - 1],
        descodtrn4: mockdesccodtrn[i - 1],
        descodtrn5: mockdesccodtrn[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovTyppayroll () {
    let mockRows = []
    const mocktyppayroll = [
      '0001',
      '0002',
      '0003',
      '01',
      '02',
      '03',
      '04',
      'M1',
      'M2',
      'XXX1'
    ]
    const mockdesctyppayroll = [
      'รายเดือน',
      'รายวัน',
      'นักศึกษา',
      'รายเดือนผู้บริหารสำนักงานใหญ่',
      'รายเดือนสำนักงานใหญ่',
      'รายเดือนสาขา',
      'รายวันสาขา',
      'รายได้ประจำเดือนละ 1 งวด',
      'รายได้ประจำเดือนละ 2 งวด',
      'XXXX-รายเดือน-XXXX'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        typpayroll: mocktyppayroll[i - 1],
        destyppayrolle: mockdesctyppayroll[i - 1],
        destyppayrollt: mockdesctyppayroll[i - 1],
        destyppayroll3: mockdesctyppayroll[i - 1],
        destyppayroll4: mockdesctyppayroll[i - 1],
        destyppayroll5: mockdesctyppayroll[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodfrm () {
    let mockRows = []
    const mockcodfrm = [
      'TEST1',
      'TEST2',
      'TEST3',
      'TEST55',
      'TEST56'
    ]
    const mockdesccodfrm = [
      'สำหรับเทส',
      'testtest2',
      'เทส3',
      'test55  0300',
      'test66 3020'
    ]
    const mockTotal = 5
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codfrm: mockcodfrm[i - 1],
        descodfrme: mockdesccodfrm[i - 1],
        descodfrmt: mockdesccodfrm[i - 1],
        descodfrm3: mockdesccodfrm[i - 1],
        descodfrm4: mockdesccodfrm[i - 1],
        descodfrm5: mockdesccodfrm[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovTempfile () {
    let mockRows = []
    const mocktempfile = [
      'HRPM55INCE',
      'HRPM55INCT',
      'HRPM55NONE',
      'HRPM55NONT',
      'XXX'
    ]
    const mockdesctempfile = [
      'หนังสือแสดงรายได้(อ.)',
      'หนังสือแสดงรายได้(ท.)',
      'หนังสือไม่แสดงรายได้(อ.)',
      'หนังสือไม่แสดงรายได้(ท.)',
      'xxxx'
    ]
    const mockTotal = 5
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        tempfile: mocktempfile[i - 1],
        destempfilee: mockdesctempfile[i - 1],
        destempfilet: mockdesctempfile[i - 1],
        destempfile3: mockdesctempfile[i - 1],
        destempfile4: mockdesctempfile[i - 1],
        destempfile5: mockdesctempfile[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodchng () {
    let mockRows = []
    const mockcodchng = [
      '0001',
      '0002',
      '0003',
      '0004',
      '0005',
      '0006',
      '0007',
      '9999'
    ]
    const mockdesccodchng = [
      'ลืมบัตร',
      'ลืมรูดบัตรเข้า',
      'ลืมรูดบัตรออก',
      'ไปทำงานข้างนอก',
      'เครื่องรูดบัตรเสีย',
      'บัตรเสีย',
      'รถบริษัทมาสาย',
      'N/A'
    ]
    const mockTotal = 5
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codchng: mockcodchng[i - 1],
        descodchnge: mockdesccodchng[i - 1],
        descodchngt: mockdesccodchng[i - 1],
        descodchng3: mockdesccodchng[i - 1],
        descodchng4: mockdesccodchng[i - 1],
        descodchng5: mockdesccodchng[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovNumlereq () {
    let mockRows = []
    const mocknumlereq = [
      '10-05-000001',
      '10-07-000001',
      '10-07-000002',
      '10-10-000002',
      '10-10-000003',
      '10-11-000001',
      '10-11-000002',
      '11-01-000001',
      '11-02-000002',
      '11-02-000003'
    ]
    const mockdesccodempid = [
      'นายจตุฤทธิ์ เสงี่ยม',
      'นายกง กิต',
      'นายกง กิต',
      'นายกง กิต',
      'นายสมชาย เข็มกลัดทองแดง',
      'นายสมชาย วิชัย',
      'น.ส.มะลิ ศุภชัย',
      'นายสมศักดิ์ สำรวยศักดิ์',
      'นายณรงค์ พิมชัยศรี',
      'นายณรงค์ พิมชัยศรี'
    ]
    const mockTotal = 10
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        numlereq: mocknumlereq[i - 1],
        desccodempide: mockdesccodempid[i - 1],
        desccodempidt: mockdesccodempid[i - 1],
        desccodempid3: mockdesccodempid[i - 1],
        desccodempid4: mockdesccodempid[i - 1],
        desccodempid5: mockdesccodempid[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovCodleave () {
    let mockRows = []
    const mockcodleave = [
      '91',
      'O1',
      'O2',
      'O3',
      'P1',
      'P2',
      'P3',
      'P4',
      'P5',
      'S1',
      'S2',
      'S3',
      'S4',
      'S5',
      'V1',
      'X1',
      'Z1',
      'Z2',
      'Z3'
    ]
    const mockdesccodleave = [
      'ลา 5 วันต่อครั้ง',
      'Line Stop',
      'ลากิจราชการทหาร',
      'กิจสหภาพ',
      'ลากิจ',
      'ลาสมรส',
      'ลาอุปสมบท',
      'ลากิจภรรยาคลอดบุตร',
      'สมาชิกในครอบครัวถึงแก่กรรม',
      'ลาป่วย ไม่เกิน 30 วัน',
      'ลาป่วยต่อเนื่อง',
      'ป่วยในงาน',
      'ลาคลอดบุตร',
      'ลาอุบัติเหตุไม่จ่ายเบี้ยขยัน',
      'ลาพักร้อน',
      'ลาชดเชย',
      'z1 ขั้นต่ำ เต็มวัน',
      'z1 ขั้นต่ำ ครึ่งวัน',
      'z1 ขั้นต่ำ กำหนดเอง'
    ]
    const mockTotal = 19
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codleave: mockcodleave[i - 1],
        desccodleavee: mockdesccodleave[i - 1],
        desccodleavet: mockdesccodleave[i - 1],
        desccodleave3: mockdesccodleave[i - 1],
        desccodleave4: mockdesccodleave[i - 1],
        desccodleave5: mockdesccodleave[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  lovNumlereqg () {
    let mockRows = []
    const mockNumlereqg = [
      '10-05-000001',
      '10-07-000001',
      '10-07-000002',
      '10-10-000002',
      '10-10-000003'
    ]
    const mockNumcalen = [
      'กลุ่มสำนักงาน',
      'กลุ่มสำนักงาน',
      'กลุ่มสำนักงาน',
      'กลุ่มสำนักงาน',
      'พนักงานกะ เช้า'
    ]
    const mockCodcomp = [
      'ฝ่ายดำเนินการและพัฒนาระบบ',
      'ฝ่ายขายและการตลาด',
      'ฝ่ายบัญชีการเงินและทรัพยากรบุคคล',
      'บริษัท นิโคอิ เนกิ จำกัด',
      'บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด'
    ]
    const mockTotal = 5
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        numlereqg: mockNumlereqg[i - 1],
        numcalene: mockNumcalen[i - 1],
        numcalent: mockNumcalen[i - 1],
        numcalen3: mockNumcalen[i - 1],
        numcalen4: mockNumcalen[i - 1],
        numcalen5: mockNumcalen[i - 1],
        codcomp: mockCodcomp[i - 1]
      })
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  }
}
