import { instance, isMock, endpoint, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLovLabels () {
    if (isMock) return generator(mocker.lovLabels())
    return instance().get(endpoint + '/hcmLov/getLabels')
  },
  getLovCodempid () {
    if (isMock) return generator(mocker.lovCodempid())
    return instance().get(endpoint + '/hcmLov/getCodempid')
  },
  getLovCodpos () {
    if (isMock) return generator(mocker.lovCodpos())
    return instance().get(endpoint + '/hcmLov/getCodpos')
  },
  getLovCodapp () {
    if (isMock) return generator(mocker.lovCodapp())
    return instance().get(endpoint + '/hcmLov/getCodapp')
  },
  getLovNummemo () {
    if (isMock) return generator(mocker.lovNummemo())
    return instance().get(endpoint + '/hcmLov/getNummemo')
  },
  getLovCodcours () {
    if (isMock) return generator(mocker.lovCodcours())
    return instance().get(endpoint + '/hcmLov/getCodcours')
  },
  getLovNumappl () {
    if (isMock) return generator(mocker.lovNumappl())
    return instance().get(endpoint + '/hcmLov/getNumappl')
  },
  getLovCodform () {
    if (isMock) return generator(mocker.lovCodform())
    return instance().get(endpoint + '/hcmLov/getCodform')
  },
  getLovNumreq () {
    if (isMock) return generator(mocker.lovNumreq())
    return instance().get(endpoint + '/hcmLov/getNumreq')
  },
  getLovCodaplvl () {
    if (isMock) return generator(mocker.lovCodaplvl())
    return instance().get(endpoint + '/hcmLov/getCodaplvl')
  },
  getLovGrdscor (params) {
    if (isMock) return generator(mocker.lovGrdscor())
    let param = params.p_codempid + '/' + params.p_dteyreap + '/' + params.p_numtime + '/' + params.p_kpino
    return instance().get(endpoint + '/hcmLov/getGrdscor/' + param)
  },
  getLovCodappr () {
    if (!isMock) return generator(mocker.lovCodappr())
    return instance().get(endpoint + '/hcmLov/getLovCodappr')
  },
  getLovCodshift () {
    if (isMock) return generator(mocker.lovCodshift())
    return instance().get(endpoint + '/hcmLov/getCodshift')
  },
  getLovCodroom () {
    if (isMock) return generator(mocker.lovCodroom())
    return instance().get(endpoint + '/hcmLov/getCodroom')
  },
  getLovCodasset () {
    if (isMock) return generator(mocker.lovCodasset())
    return instance().get(endpoint + '/hcmLov/getCodasset')
  },
  getLovCodcalen () {
    if (isMock) return generator(mocker.lovCodcalen())
    return instance().get(endpoint + '/hcmLov/getCodcalen')
  },
  getLovCodcompy () {
    if (isMock) return generator(mocker.lovCodcompy())
    return instance().get(endpoint + '/hcmLov/getCodcompy')
  },
  getLovCoduser () {
    if (isMock) return generator(mocker.lovCoduser())
    return instance().get(endpoint + '/hcmLov/getCoduser')
  },
  getLovCodsecu () {
    if (isMock) return generator(mocker.lovCodsecu())
    return instance().get(endpoint + '/hcmLov/getCodsecu')
  },
  getLovCodproc () {
    if (isMock) return generator(mocker.lovCodproc())
    return instance().get(endpoint + '/hcmLov/getCodproc')
  },
  getLovCodincom () {
    if (isMock) return generator(mocker.lovCodincom())
    return instance().get(endpoint + '/hcmLov/getCodincom')
  },
  getLovCodretro () {
    if (isMock) return generator(mocker.lovCodretro())
    return instance().get(endpoint + '/hcmLov/getCodretro')
  },
  getLovCodtable () {
    if (isMock) return generator(mocker.lovCodtable())
    return instance().get(endpoint + '/hcmLov/getCodtable')
  },
  getLovCodval () {
    if (isMock) return generator(mocker.lovCodval())
    return instance().get(endpoint + '/hcmLov/getCodval')
  },
  getLovCostCenter () {
    if (isMock) return generator(mocker.lovCostCenter())
    return instance().get(endpoint + '/hcmLov/getCostCenter')
  },
  getLovComGroup () {
    if (isMock) return generator(mocker.lovComGroup())
    return instance().get(endpoint + '/hcmLov/getComGroup')
  },
  getLovCodtency () {
    if (isMock) return generator(mocker.lovCodtency())
    return instance().get(endpoint + '/hcmLov/getCodtency')
  },
  getLovCodoccup () {
    if (isMock) return generator(mocker.lovCodoccup())
    return instance().get(endpoint + '/hcmLov/getCodoccup')
  },
  getLovCodhosp () {
    if (isMock) return generator(mocker.lovCodhosp())
    return instance().get(endpoint + '/hcmLov/getCodhosp')
  },
  getLovCodprov () {
    if (isMock) return generator(mocker.lovCodprov())
    return instance().get(endpoint + '/hcmLov/getCodprov')
  },
  getLovCodgrpwork () {
    if (isMock) return generator(mocker.lovCodgrpwork())
    return instance().get(endpoint + '/hcmLov/getCodgrpwork')
  },
  getLovCodcarcab () {
    if (isMock) return generator(mocker.lovCodcarcab())
    return instance().get(endpoint + '/hcmLov/getCodcarcab')
  },
  getLovCodtypinc () {
    if (isMock) return generator(mocker.lovCodtypinc())
    return instance().get(endpoint + '/hcmLov/getCodtypinc')
  },
  getLovCodtyppayr () {
    if (isMock) return generator(mocker.lovCodtyppayr())
    return instance().get(endpoint + '/hcmLov/getCodtyppayr')
  },
  getLovCodtyppayt () {
    if (isMock) return generator(mocker.lovCodtyppayt())
    return instance().get(endpoint + '/hcmLov/getCodtyppayt')
  },
  getLovCodtax () {
    if (isMock) return generator(mocker.lovCodtax())
    return instance().get(endpoint + '/hcmLov/getCodtax')
  },
  getLovNumotreq () {
    if (isMock) return generator(mocker.lovNumotreq())
    return instance().get(endpoint + '/hcmLov/getNumotreq')
  },
  getLovCodrem () {
    if (isMock) return generator(mocker.lovCodrem())
    return instance().get(endpoint + '/hcmLov/getCodrem')
  },
  getLovTypeleave () {
    if (isMock) return generator(mocker.lovTypeleave())
    return instance().get(endpoint + '/hcmLov/getTypeleave')
  },
  getLovCodpay () {
    if (isMock) return generator(mocker.lovCodpay())
    return instance().get(endpoint + '/hcmLov/getCodpay')
  },
  getLovCodpayrol () {
    if (isMock) return generator(mocker.lovCodpayrol())
    return instance().get(endpoint + '/hcmLov/getCodpayrol')
  },
  getLovCodfund () {
    if (isMock) return generator(mocker.lovCodfund())
    return instance().get(endpoint + '/hcmLov/getCodfund')
  },
  getLovCodplan () {
    if (isMock) return generator(mocker.lovCodplan())
    return instance().get(endpoint + '/hcmLov/getCodplan')
  },
  getLovCodcause () {
    if (isMock) return generator(mocker.lovCodcause())
    return instance().get(endpoint + '/hrmsLov/getCodcause')
  },
  getLovTyprewd () {
    if (isMock) return generator(mocker.lovTyprewd())
    return instance().get(endpoint + '/hcmLov/getTyprewd')
  },
  getLovCountry () {
    if (isMock) return generator(mocker.lovCountry())
    return instance().get(endpoint + '/hrmsLov/getCountry')
  },
  getLovCodedlv () {
    if (isMock) return generator(mocker.lovCodedlv())
    return instance().get(endpoint + '/hcmLov/getCodedlv')
  },
  getLovCodnatnl () {
    if (isMock) return generator(mocker.lovCodnatnl())
    return instance().get(endpoint + '/hcmLov/getCodnatnl')
  },
  getLovCodrelgn () {
    if (isMock) return generator(mocker.lovCodrelgn())
    return instance().get(endpoint + '/hcmLov/getCodrelgn')
  },
  getLovCoddist () {
    if (isMock) return generator(mocker.lovCoddist())
    return instance().get(endpoint + '/hcmLov/getCoddist')
  },
  getLovCoddglv () {
    if (isMock) return generator(mocker.lovCoddglv())
    return instance().get(endpoint + '/hrmsLov/getCoddglv')
  },
  getLovCodmajsb () {
    if (isMock) return generator(mocker.lovCodmajsb())
    return instance().get(endpoint + '/hrmsLov/getCodmajsb')
  },
  getLovCodminsb () {
    if (isMock) return generator(mocker.lovCodminsb())
    return instance().get(endpoint + '/hrmsLov/getCodminsb')
  },
  getLovCodinst () {
    if (isMock) return generator(mocker.lovCodinst())
    return instance().get(endpoint + '/hrmsLov/getCodinst')
  },
  getLovCodsubdist () {
    if (isMock) return generator(mocker.lovCodsubdist())
    return instance().get(endpoint + '/hcmLov/getCodsubdist')
  },
  getLovCodbrlc () {
    if (isMock) return generator(mocker.lovCodbrlc())
    return instance().get(endpoint + '/hcmLov/getCodbrlc')
  },
  getLovCodempmt () {
    if (isMock) return generator(mocker.lovCodempmt())
    return instance().get(endpoint + '/hcmLov/getCodempmt')
  },
  getLovCodtypemp () {
    if (isMock) return generator(mocker.lovCodtypemp())
    return instance().get(endpoint + '/hcmLov/getCodtypemp')
  },
  getLovCodjob () {
    if (isMock) return generator(mocker.lovCodjob())
    return instance().get(endpoint + '/hcmLov/getCodjob')
  },
  getLovCodjobgrad () {
    if (isMock) return generator(mocker.lovCodjobgrad())
    return instance().get(endpoint + '/hcmLov/getCodjobgrad')
  },
  getLovTypdisp () {
    if (isMock) return generator(mocker.lovTypdisp())
    return instance().get(endpoint + '/hcmLov/getTypdisp')
  },
  getLovCodgl () {
    if (isMock) return generator(mocker.lovCodgl())
    return instance().get(endpoint + '/hcmLov/getCodgl')
  },
  getLovCodpoint () {
    if (isMock) return generator(mocker.lovCodpoint())
    return instance().get(endpoint + '/hcmLov/getCodpoint')
  },
  getLovCodbank () {
    if (isMock) return generator(mocker.lovCodbank())
    return instance().get(endpoint + '/hcmLov/getCodbank')
  },
  getLovCodcurr () {
    if (isMock) return generator(mocker.lovCodcurr())
    return instance().get(endpoint + '/hcmLov/getCodcurr')
  },
  getLovCodtypdoc () {
    if (isMock) return generator(mocker.lovCodtypdoc())
    return instance().get(endpoint + '/hcmLov/getCodtypdoc')
  },
  getLovCodtypcolla () {
    if (isMock) return generator(mocker.lovCodtypcolla())
    return instance().get(endpoint + '/hcmLov/getCodtypcolla')
  },
  getLovCodreqst () {
    if (isMock) return generator(mocker.lovCodreqst())
    return instance().get(endpoint + '/hcmLov/getCodreqst')
  },
  getLovCodmenu () {
    if (isMock) return generator(mocker.lovCodmenu())
    return instance().get(endpoint + '/hcmLov/getCodmenu')
  },
  getLovCodfunction () {
    if (isMock) return generator(mocker.lovCodfunction())
    return instance().get(endpoint + '/hcmLov/getCodfunction')
  },
  getLovCodskill () {
    if (isMock) return generator(mocker.lovCodskill())
    return instance().get(endpoint + '/hcmLov/getCodskill')
  },
  getLovCodaward () {
    if (isMock) return generator(mocker.lovCodaward())
    return instance().get(endpoint + '/hcmLov/getCodaward')
  },
  getLovCodfrgtcard () {
    if (isMock) return generator(mocker.lovCodfrgtcard())
    return instance().get(endpoint + '/hcmLov/getCodfrgtcard')
  },
  getLovCodtrn () {
    if (isMock) return generator(mocker.lovCodtrn())
    return instance().get(endpoint + '/hcmLov/getCodtrn')
  },
  getLovTyppayroll () {
    if (isMock) return generator(mocker.lovTyppayroll())
    return instance().get(endpoint + '/hcmLov/getTyppayroll')
  },
  getLovCodfrm () {
    if (isMock) return generator(mocker.lovCodfrm())
    return instance().get(endpoint + '/hcmLov/getCodfrm')
  },
  getLovTempfile () {
    if (isMock) return generator(mocker.lovTempfile())
    return instance().get(endpoint + '/hcmLov/getTempfile')
  },
  getLovCodchng () {
    if (isMock) return generator(mocker.lovCodchng())
    return instance().get(endpoint + '/hcmLov/getCodchng')
  },
  getLovNumlereq () {
    if (isMock) return generator(mocker.lovNumlereq())
    return instance().get(endpoint + '/hcmLov/getNumlereq')
  },
  getLovCodleave () {
    if (isMock) return generator(mocker.lovCodleave())
    return instance().get(endpoint + '/hcmLov/getCodleave')
  },
  getLovNumlereqg () {
    if (isMock) return generator(mocker.lovNumlereqg())
    return instance().get(endpoint + '/hcmLov/getNumlereqg')
  }
}
