module.exports = {
  'en': {
    'HCM_LOV': {
      '10': 'Search Name or Code'
    },
    'LOV_COMP': {
      '0': 'List Of Company Name',
      '1': 'List Of Company Name',
      '2': 'Code of Company',
      '3': 'Name of Company'
    },
    'LOV_EMP': {
      '0': 'List of All Employee',
      '1': 'List of All Employee',
      '2': 'Employee Code',
      '3': 'Employee Name',
      '4': 'Status of Emp.',
      '5': 'Security'
    },
    'LOV_POS': {
      '0': 'List of Position Code',
      '1': 'List of Position Code',
      '2': 'Position Code',
      '3': 'Description'
    },
    'LOV_CODAPP': {
      '0': 'List of Appl. Name',
      '1': 'List of Appl. Name',
      '2': 'Code Appl. Name',
      '3': 'Description'
    },
    'LOV_MEMO': {
      '0': 'List Of Memo No.',
      '1': 'Memo No.',
      '2': 'Year',
      '3': 'Month',
      '4': 'Class',
      '5': 'Course',
      '6': 'Description'
    },
    'LOV_CORS': {
      '1': 'List of Course Training Code',
      '2': 'Course Code',
      '3': 'Course Name'
    },
    'LOV_FORM': {
      '0': 'List of Appraisal Form',
      '1': 'List of Appraisal Form',
      '2': 'App. Form No.',
      '3': 'Description'
    },
    'LOV_APP3': {
      '0': 'List of All Application No.',
      '1': 'List of All Application No.',
      '2': 'Applicant No.',
      '3': 'Appl Name.',
      '4': 'Status'
    },
    'LOV_APLV': {
      '0': 'Type of Appraisal group',
      '1': 'Type of Appraisal group',
      '2': 'Type of Appraisal group',
      '3': 'Description'
    },
    'LOV_REQ3': {
      '0': 'List of Personnal Request',
      '1': 'List of Personnal Request',
      '2': 'Request No',
      '3': 'Department Req.',
      '4': 'Position Req.',
      '5': 'Description'
    },
    'LOV_CODAPPR': {
      '0': 'List of All Employee',
      '1': 'List of All Employee',
      '2': 'Employee Code',
      '3': 'Employee Name',
      '4': 'Status of Emp.',
      '5': 'Security'
    },
    'LOV_SHIF': {
      '0': 'List of Shift Code',
      '1': 'List of Shift Code',
      '2': 'Shift Code',
      '3': 'Description',
      '4': 'Schedule Time'
    },
    'LOV_ROOM': {
      '0': 'List of Room Number',
      '1': 'List of Room Number',
      '2': 'Room Number',
      '3': 'Room Name'
    },
    'LOV_ASSET': {
      '0': 'List of Company Asset Code',
      '1': 'List of Company Asset Code',
      '2': 'Company Asset Code',
      '3': 'Description'
    },
    'LOV_WORK': {
      '0': 'List of Work Group Code',
      '1': 'List of Work Group Code',
      '2': 'Work Group Code',
      '3': 'Description'
    },
    'HRAP31EC6': {
      '250': '',
      '260': '',
      '270': ''
    },
    'LOV_CODUSER': {
      '0': 'List of User Name',
      '1': 'List of User Name',
      '2': 'Code of User',
      '3': 'Name of User'
    },
    'LOV_CODSECU': {
      '0': 'List of Security Group',
      '1': 'List of Security Group',
      '2': 'Security Group Code',
      '3': 'Security Group Name'
    },
    'LOV_CODPROC': {
      '0': 'List of Work Process',
      '1': 'List of Work Process',
      '2': 'Code of Process',
      '3': 'Description'
    },
    'LOV_CODINCOM': {
      '0': 'List of Fixed Income Code',
      '1': 'List of Fixed Income Code',
      '2': 'Income Code',
      '3': 'Description'
    },
    'LOV_CODRETRO': {
      '0': 'List Of Other Income Code',
      '1': 'List Of Other Income Code',
      '2': 'Income Code',
      '3': 'Description'
    },
    'LOV_CODTABLE': {
      '0': 'List Of Table Code',
      '1': 'List Of Table Code',
      '2': 'Table Code',
      '3': 'Description'
    },
    'LOV_CODVAL': {
      '0': 'List Of Value Code',
      '1': 'List Of Value Code',
      '2': 'Value Code',
      '3': 'Description'
    },
    'LOV_COSTCENT': {
      '0': 'รายการรหัส Cost Center',
      '1': 'รายการรหัส Cost Center',
      '2': 'รหัสค่าข้อมูล',
      '3': 'รายละเอียด'
    },
    'LOV_COMPGRP': {
      '0': 'รายการรหัสกลุ่มบริษัท',
      '1': 'รายการรหัสกลุ่มบริษัท',
      '2': 'รหัสค่าข้อมูล',
      '3': 'รายละเอียด'
    },
    'LOV_CODTENCY': {
      '0': 'Type of Special Skills/Exp.',
      '1': 'Type of Special Skills/Exp.',
      '2': 'Code of Special Skills/Exp.',
      '3': 'Description'
    },
    'LOV_CODOCCUP': {
      '0': 'List Of Occupation Code',
      '1': 'List Of Occupation Code',
      '2': 'Occupation Code',
      '3': 'Description'
    },
    'LOV_CODHOSP': {
      '0': 'List Of Hospital Code',
      '1': 'List Of Hospital Code',
      '2': 'Hospital Code',
      '3': 'Description'
    },
    'LOV_CODPROV': {
      '0': 'List Of Province',
      '1': 'List Of Province',
      '2': 'Province Code',
      '3': 'Description'
    },
    'LOV_CODGRPWORK': {
      '0': 'List Of Group Work',
      '1': 'List Of Group Work',
      '2': 'Group Work Code',
      '3': 'Description'
    },
    'LOV_CODCARCAB': {
      '0': 'List Of Car Cable',
      '1': 'List Of Car Cable',
      '2': 'Car Cable Code',
      '3': 'Description'
    },
    'LOV_CODTYPINC': {
      '0': 'List Of Type Inc',
      '1': 'List Of Type Inc',
      '2': 'Type Inc Code',
      '3': 'Description'
    },
    'LOV_CODTYPPAYR': {
      '0': 'List Of Pay Slip',
      '1': 'List Of Pay Slip',
      '2': 'Pay Slip Code',
      '3': 'Description'
    },
    'LOV_CODTYPPAYT': {
      '0': 'List Of Tax Certificate',
      '1': 'List Of Tax Certificate',
      '2': 'Tax Certificate Code',
      '3': 'Description'
    },
    'LOV_CODTAX': {
      '0': 'List Of Tax Code',
      '1': 'List Of Tax Code',
      '2': 'Tax Code',
      '3': 'Description'
    },
    'LOV_NUMOTREQ': {
      '0': 'No. of OT Request',
      '1': 'No. of OT Request',
      '2': 'Request No.',
      '3': 'Employee Name',
      '4': 'Department',
      '5': 'Employee Group'
    },
    'LOV_CODREM': {
      '0': 'Reasons for Overtime Request',
      '1': 'Reasons for Overtime Request',
      '2': 'Code of Reasons',
      '3': 'Description'
    },
    'LOV_TYPELEAVE': {
      '0': 'List of Type of Leave',
      '1': 'List of Type of Leave',
      '2': 'Type of Leave',
      '3': 'Description'
    },
    'LOV_CODPAY': {
      '0': 'List of Deduct Code',
      '1': 'List of Deduct Code',
      '2': 'Deduct Code',
      '3': 'Description'
    },
    'LOV_CODPAYROL': {
      '0': 'List of Payroll Group',
      '1': 'List of Payroll Group',
      '2': 'Payroll Group Code',
      '3': 'Description'
    },
    'LOV_CODFUND': {
      '0': 'List of Fund Code',
      '1': 'List of Fund Code',
      '2': 'Fund Code',
      '3': 'Description'
    },
    'LOV_CODPLAN': {
      '0': 'List of Plan Code',
      '1': 'List of Plan Code',
      '2': 'Plan Code',
      '3': 'Description'
    },
    'LOV_CODCAUSE': {
      '0': 'List of Cause Code',
      '1': 'List of Cause Code',
      '2': 'Cause Code',
      '3': 'Description'
    },
    'LOV_TYPREWD': {
      '0': 'Type of Reward Code',
      '1': 'Type of Reward Code',
      '2': 'Type of Reward Code',
      '3': 'Description'
    },
    'LOV_COUNTRY': {
      '0': 'List of Country Code',
      '1': 'List of Country Code',
      '2': 'Country Code',
      '3': 'Description'
    },
    'LOV_CODEDLV': {
      '0': 'Educating Level',
      '1': 'Educating Level',
      '2': 'Code',
      '3': 'Description'
    },
    'LOV_CODNATNL': {
      '0': 'Nationality Code',
      '1': 'Nationality Code',
      '2': 'Nationality Code',
      '3': 'Description'
    },
    'LOV_CODRELGN': {
      '0': 'Religion Code',
      '1': 'Religion Code',
      '2': 'Religion Code',
      '3': 'Description'
    },
    'LOV_CODDIST': {
      '0': 'List of District Code',
      '1': 'District Code',
      '2': 'Description',
      '3': 'Province'
    },
    'LOV_CODDGLV': {
      '0': 'Academic Qualification',
      '1': 'Academic Qualification',
      '2': 'Code of Degree',
      '3': 'Description'
    },
    'LOV_CODMAJSB': {
      '0': 'List of major subject',
      '1': 'List of major subject',
      '2': 'Code of major subject',
      '3': 'Description'
    },
    'LOV_CODMINSB': {
      '0': 'Academic Field',
      '1': 'Academic Field',
      '2': 'Code of Academic Field',
      '3': 'Description'
    },
    'LOV_CODINST': {
      '0': 'Institute Code',
      '1': 'Institute Code',
      '2': 'Institute Code',
      '3': 'Description'
    },
    'LOV_CODSUBDIST': {
      '0': 'List of Sub District Code',
      '1': 'Sub District Code',
      '2': 'Description',
      '3': 'District'
    },
    'LOV_CODBRLC': {
      '0': 'Work Place Code',
      '1': 'Work Place Code',
      '2': 'Work Place Code',
      '3': 'Description'
    },
    'LOV_CODEMPMT': {
      '0': 'List Of Employment Type',
      '1': 'List Of Employment Type',
      '2': 'Employment Type Code',
      '3': 'Description'
    },
    'LOV_CODTYPEMP': {
      '0': 'List Of Employee Type',
      '1': 'List Of Employee Type',
      '2': 'Employee Type Code',
      '3': 'Description'
    },
    'LOV_CODJOB': {
      '0': 'List Of job description',
      '1': 'List Of job description',
      '2': 'job description Code',
      '3': 'Description'
    },
    'LOV_CODJOBGRAD': {
      '0': 'List Of job Grade',
      '1': 'List Of job Grade',
      '2': 'job Grade Code',
      '3': 'Description'
    },
    'LOV_TYPDISP': {
      '0': 'List Of Disability Type',
      '1': 'List Of Disability Type',
      '2': 'Disability Type Code',
      '3': 'Description'
    },
    'LOV_CODGL': {
      '0': 'List Of GL Code',
      '1': 'List Of GL Code',
      '2': 'GL Code',
      '3': 'Description'
    },
    'LOV_CODBANK': {
      '0': 'List Of Bank Code',
      '1': 'List Of Bank Code',
      '2': 'Bank Code',
      '3': 'Description'
    },
    'LOV_CODCURR': {
      '0': 'List Of Monetary unit',
      '1': 'List Of Monetary unit',
      '2': 'Monetary unit',
      '3': 'Description'
    },
    'LOV_CODTYPDOC': {
      '0': 'List Of Document Type',
      '1': 'List Of Document Type',
      '2': 'Document Type',
      '3': 'Description'
    },
    'LOV_CODTYPCOLLA': {
      '0': 'List Of  Collateral Type',
      '1': 'List Of  Collateral Type',
      '2': ' Collateral Type',
      '3': 'Description'
    },
    'LOV_CODREQST': {
      '0': 'List Of Number Request',
      '1': 'List Of Number Request',
      '2': 'Number Request',
      '3': 'Company Request'
    },
    'LOV_CODMENU': {
      '0': 'List Of Menu Code',
      '1': 'List Of Menu Code',
      '2': 'Menu Code',
      '3': 'Description'
    },
    'LOV_CODFUNCTION': {
      '0': 'List Of Function Code',
      '1': 'List Of Function Code',
      '2': 'Function Code',
      '3': 'Description'
    },
    'LOV_CODSKILL': {
      '0': 'List Of Skill Code',
      '1': 'List Of Skill Code',
      '2': 'Skill Code',
      '3': 'Description'
    },
    'LOV_CODAWARD': {
      '0': 'List Of Award Code',
      '1': 'List Of Award Code',
      '2': 'Award Code',
      '3': 'Description'
    },
    'LOV_CODFRGTCARD': {
      '0': 'List Of Forgive card Code',
      '1': 'List Of Forgive card Code',
      '2': 'Forgive card Code',
      '3': 'Description'
    },
    'LOV_CODTRN': {
      '0': 'List Of Movement type Code',
      '1': 'List Of Movement type Code',
      '2': 'Movement type Code',
      '3': 'Description'
    },
    'LOV_TYPPAYROLL': {
      '0': 'List of Payroll Group',
      '1': 'List of Payroll Group',
      '2': 'Code',
      '3': 'Description'
    },
    'LOV_CODFRM': {
      '0': 'List of Condition Code',
      '1': 'List of Condition Code',
      '2': 'Condition Code',
      '3': 'Description'
    },
    'LOV_TEMPFILE': {
      '0': 'List of Tempfile Code',
      '1': 'List of Tempfile Code',
      '2': 'Tempfile Code',
      '3': 'Description'
    },
    'LOV_CODCHNG': {
      '0': 'Reas. for Corr. of Time In/Out',
      '1': 'Reas. for Corr. of Time In/Out',
      '2': 'Code',
      '3': 'Description'
    },
    'LOV_NUMLEREQ': {
      '0': 'List of Leave Request',
      '1': 'List of Leave Request',
      '2': 'Number of Leave',
      '3': 'Name'
    },
    'LOV_CODLEAVE': {
      '0': 'List of Leave Code',
      '1': 'List of Leave Code',
      '2': 'Leave Code',
      '3': 'Description'
    },
    'LOV_NUMLEREQG': {
      '0': 'List of Group Leave Request',
      '1': 'List of Group Leave Request',
      '2': 'Number of Leave',
      '3': 'Employee Group',
      '4': 'Department'
    }
  },
  'th': {
    'HCM_LOV': {
      '10': 'ค้นหาชื่อหรือรหัส'
    },
    'LOV_COMP': {
      '0': 'รายชื่อบริษัท',
      '1': 'รายชื่อบริษัท',
      '2': 'รหัสบริษัท',
      '3': 'ชือบริษัท'
    },
    'LOV_EMP': {
      '0': 'รายชื่อพนักงานทั้งหมด',
      '1': 'รายชื่อพนักงานทั้งหมด',
      '2': 'รหัสพนักงาน',
      '3': 'ชื่อ - นามสกุล',
      '4': 'สถานะพนักงาน',
      '5': 'Security'
    },
    'LOV_POS': {
      '0': 'รายชื่อตำแหน่งงาน',
      '1': 'รายชื่อตำแหน่งงาน',
      '2': 'รหัสตำแหน่ง',
      '3': 'รายละเอียด'
    },
    'LOV_CODAPP': {
      '0': 'รายชื่อของโปรแกรม',
      '1': 'รายชื่อของโปรแกรม',
      '2': 'รหัสโปรแกรม',
      '3': 'รายละเอียด'
    },
    'LOV_MEMO': {
      '0': 'รายการรหัส Memo',
      '1': 'รหัส Memo',
      '2': 'ปี',
      '3': 'เดือน',
      '4': 'รุ่น',
      '5': 'หลักสูตร',
      '6': 'รายละเอียด'
    },
    'LOV_CORS': {
      '1': 'รายชื่อหลักสูตรการอบรมพนักงาน',
      '2': 'รหัสหลักสูตร',
      '3': 'ชื่อหลักสูตร'
    },
    'LOV_FORM': {
      '0': 'รายการเลชที่แบบฟอร์มประมเินผล',
      '1': 'รายการเลชที่แบบฟอร์มประมเินผล',
      '2': 'เลขที่แบบฟอร์ม',
      '3': 'รายละเอียด'
    },
    'LOV_APP3': {
      '0': 'รายชื่อเลขที่ใบสมัคร',
      '1': 'รายการเลขที่ใบสมัคร',
      '2': 'เลขที่ใบสมัคร',
      '3': 'ชื่อผู้สมัคร',
      '4': 'สถานะ'
    },
    'LOV_APLV': {
      '0': 'รายชื่อประเภทกลุ่มการประเมิน',
      '1': 'รายชื่อประเภทกลุ่มการประเมิน',
      '2': 'ประเภทกลุ่มการประเมิน',
      '3': 'รายละเอียด'
    },
    'LOV_NUMREQ': {
      '0': 'รายการเลขที่ใบขออนุมัติจ้างงาน',
      '1': 'รายการเลขที่ใบขออนุมัติจ้างงาน',
      '2': 'เลขที่ใบขออนุมัติ',
      '3': 'หน่วยงานที่ขอ',
      '4': 'ตำแหน่งที่ขอ',
      '5': 'รายละเอียด'
    },
    'LOV_CODAPPR': {
      '0': 'รายชื่อพนักงานทั้งหมด',
      '1': 'รายชื่อพนักงานทั้งหมด',
      '2': 'รหัสพนักงาน',
      '3': 'ชื่อ - นามสกุล',
      '4': 'สถานะพนักงาน',
      '5': 'Security'
    },
    'LOV_SHIF': {
      '0': 'รายการรหัสกะ',
      '1': 'รายการรหัสกะ',
      '2': 'รหัสกะ',
      '3': 'รายละเอียด',
      '4': 'ตารางเวลา'
    },
    'LOV_ROOM': {
      '0': 'รายการห้องประชุม',
      '1': 'รายการห้องประชุม',
      '2': 'เลขที่ห้อง',
      '3': 'ชื่อห้อง'
    },
    'LOV_ASSET': {
      '0': 'รายการรหัสทรัพย์สินของบริษัท',
      '1': 'รายการรหัสทรัพย์สินของบริษัท',
      '2': 'รหัสทรัพย์สินของบริษัท',
      '3': 'รายละเอียด'
    },
    'LOV_WORK': {
      '0': 'รายการรหัสกลุ่มการทำงาน',
      '1': 'รายการรหัสกลุ่มการทำงาน',
      '2': 'รหัสกลุ่มการทำงาน',
      '3': 'รายละเอียด'
    },
    'HRAP31EC6': {
      '250': '',
      '260': '',
      '270': ''
    },
    'LOV_CODUSER': {
      '0': 'รายชื่อผู้ใช้ระบบ',
      '1': 'รายชื่อผู้ใช้ระบบ',
      '2': 'รหัสผู้ใช้ระบบ',
      '3': 'ชื่อของผู้ใช้ระบบ'
    },
    'LOV_CODSECU': {
      '0': 'รายชื่อรหัสกลุ่ม Security',
      '1': 'รายชื่อรหัสกลุ่ม Security',
      '2': 'รหัสกลุ่ม Security',
      '3': 'ชื่อกลุ่ม Security'
    },
    'LOV_CODPROC': {
      '0': 'รายชื่อ Work Process',
      '1': 'รายชื่อ Work Process',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_CODINCOM': {
      '0': 'รหัสประเภทการจ้างงาน',
      '1': 'รหัสประเภทการจ้างงาน',
      '2': 'รหัสประเภทการจ้างงาน',
      '3': 'รายละเอียด'
    },
    'LOV_CODRETRO': {
      '0': 'รายการรหัสเงินได้อื่นๆ',
      '1': 'รายการรหัสเงินได้อื่นๆ',
      '2': 'รหัสตกเบิก',
      '3': 'รายละเอียด'
    },
    'LOV_CODTABLE': {
      '0': 'รายการรหัสตาราง',
      '1': 'รายการรหัสตาราง',
      '2': 'รหัสตาราง',
      '3': 'รายละเอียด'
    },
    'LOV_CODVAL': {
      '0': 'รายการรหัสค่าข้อมูล',
      '1': 'รายการรหัสค่าข้อมูล',
      '2': 'รหัสค่าข้อมูล',
      '3': 'รายละเอียด'
    },
    'LOV_COSTCENT': {
      '0': 'รายการรหัส Cost Center',
      '1': 'รายการรหัส Cost Center',
      '2': 'รหัส Cost Center',
      '3': 'รายละเอียด'
    },
    'LOV_COMPGRP': {
      '0': 'รายการรหัสกลุ่มบริษัท',
      '1': 'รายการรหัสกลุ่มบริษัท',
      '2': 'รหัสกลุ่มบริษัท',
      '3': 'รายละเอียด'
    },
    'LOV_CODTENCY': {
      '0': 'รหัสความสามารถ',
      '1': 'รหัสความสามารถ',
      '2': 'รหัสความสามารถ',
      '3': 'รายละเอียด'
    },
    'LOV_CODOCCUP': {
      '0': 'รายการรหัสอาชีพ',
      '1': 'รายการรหัสอาชีพ',
      '2': 'รหัสอาชีพ',
      '3': 'รายละเอียด'
    },
    'LOV_CODHOSP': {
      '0': 'รายการรหัสโรงพยาบาล',
      '1': 'รายการรหัสโรงพยาบาล',
      '2': 'รหัสโรงพยาบาล',
      '3': 'รายละเอียด'
    },
    'LOV_CODPROV': {
      '0': 'รายการรหัสจังหวัด',
      '1': 'รายการรหัสจังหวัด',
      '2': 'รหัสจังหวัด',
      '3': 'รายละเอียด'
    },
    'LOV_CODGRPWORK': {
      '0': 'รายการรหัสกลุ่มทำงาน',
      '1': 'รายการรหัสกลุ่มทำงาน',
      '2': 'รหัสกลุ่มทำงาน',
      '3': 'รายละเอียด'
    },
    'LOV_CODCARCAB': {
      '0': 'รายการรหัสสายรถ',
      '1': 'รายการรหัสสายรถ',
      '2': 'รหัสสายรถ',
      '3': 'รายละเอียด'
    },
    'LOV_CODTYPINC': {
      '0': 'รายการรหัสประเภทเงินได้',
      '1': 'รายการรหัสประเภทเงินได้',
      '2': 'รหัสประเภทเงินได้',
      '3': 'รายละเอียด'
    },
    'LOV_CODTYPPAYR': {
      '0': 'รายการรหัสใบจ่ายเงินเดือน',
      '1': 'รายการรหัสใบจ่ายเงินเดือน',
      '2': 'รหัสใบจ่ายเงินเดือน',
      '3': 'รายละเอียด'
    },
    'LOV_CODTYPPAYT': {
      '0': 'รายการรหัสหนังสือรับรองภาษี',
      '1': 'รายการรหัสหนังสือรับรองภาษี',
      '2': 'รหัสหนังสือรับรองภาษี',
      '3': 'รายละเอียด'
    },
    'LOV_CODTAX': {
      '0': 'รายการรหัสภาษี',
      '1': 'รายการรหัสภาษี',
      '2': 'รหัสภาษี',
      '3': 'รายละเอียด'
    },
    'LOV_NUMOTREQ': {
      '0': 'เลขที่ใบขออนุมัติการทำงานล่วงเวลา',
      '1': 'เลขที่ใบขออนุมัติการทำงานล่วงเวลา',
      '2': 'เลขที่ใบขออนุมัติ',
      '3': 'ชื่อพนักงาน',
      '4': 'หน่วยงาน',
      '5': 'กลุ่มพนักงาน'
    },
    'LOV_CODREM': {
      '0': 'สาเหตุการขออนุมัติทำล่วงเวลา',
      '1': 'สาเหตุการขออนุมัติทำล่วงเวลา',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_TYPELEAVE': {
      '0': 'รหัสประเภทการลา',
      '1': 'รหัสประเภทการลา',
      '2': 'ประเภทการลา',
      '3': 'รายละเอียด'
    },
    'LOV_CODPAY': {
      '0': 'รายการรหัสส่วนหัก',
      '1': 'รายการรหัสส่วนหัก',
      '2': 'รหัสส่วนหัก',
      '3': 'รายละเอียด'
    },
    'LOV_CODPAYROL': {
      '0': 'รายการกลุ่มการจ่ายเงินเดือน',
      '1': 'รายการกลุ่มการจ่ายเงินเดือน',
      '2': 'รหัสกลุ่มการจ่ายเงินเดือน',
      '3': 'รายละเอียด'
    },
    'LOV_CODFUND': {
      '0': 'รายการรหัสกองทุน',
      '1': 'รายการรหัสกองทุน',
      '2': 'รหัสกองทุน',
      '3': 'รายละเอียด'
    },
    'LOV_CODPLAN': {
      '0': 'รายการนำส่งกองทุน',
      '1': 'รายการนำส่งกองทุน',
      '2': 'รหัสนำส่งกองทุน',
      '3': 'รายละเอียด'
    },
    'LOV_CODCAUSE': {
      '0': 'รายการรหัสสาเหตุ',
      '1': 'รายการรหัสสาเหตุ',
      '2': 'รหัสสาเหตุ',
      '3': 'รายละเอียด'
    },
    'LOV_TYPREWD': {
      '0': 'รหัสประเภทคุณความดี',
      '1': 'รหัสประเภทคุณความดี',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_COUNTRY': {
      '0': 'รายการประเทศ',
      '1': 'รายการประเทศ',
      '2': 'รหัสประเทศ',
      '3': 'รายละเอียด'
    },
    'LOV_CODEDLV': {
      '0': 'ระดับการศึกษา',
      '1': 'ระดับการศึกษา',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_CODNATNL': {
      '0': 'รายการรหัสสัญชาติ',
      '1': 'รายการรหัสสัญชาติ',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_CODRELGN': {
      '0': 'รหัสศาสนา',
      '1': 'รหัสศาสนา',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_CODDIST': {
      '0': 'รายการรหัสอำเภอ',
      '1': 'รหัสอำเภอ',
      '2': 'รายละเอียด',
      '3': 'จังหวัด'
    },
    'LOV_CODDGLV': {
      '0': 'ชื่อวุฒิ',
      '1': 'ชื่อวุฒิ',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_CODMAJSB': {
      '0': 'รายชื่อรหัสวิชาหลัก',
      '1': 'รายชื่อรหัสวิชาหลัก',
      '2': 'รหัสวิชาหลัก',
      '3': 'รายละเอียด'
    },
    'LOV_CODMINSB': {
      '0': 'สาขาการศึกษา',
      '1': 'สาขาการศึกษา',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_CODINST': {
      '0': 'รหัสสถาบัน',
      '1': 'รหัสสถาบัน',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_CODSUBDIST': {
      '0': 'รายการรหัสตำบล',
      '1': 'รหัสตำบล',
      '2': 'รายละเอียด',
      '3': 'อำเภอ'
    },
    'LOV_CODBRLC': {
      '0': 'รายการรหัสสถานที่ทำงาน',
      '1': 'รายการรหัสสถานที่ทำงาน',
      '2': 'รหัสสถานที่ทำงาน',
      '3': 'รายละเอียด'
    },
    'LOV_CODEMPMT': {
      '0': 'รายการประเภทการจ้างงาน',
      '1': 'รายการประเภทการจ้างงาน',
      '2': 'รหัสประเภทการจ้างงาน',
      '3': 'รายละเอียด'
    },
    'LOV_CODTYPEMP': {
      '0': 'รายการประเภทพนักงาน',
      '1': 'รายการประเภทพนักงาน',
      '2': 'รหัสประเภทพนักงาน',
      '3': 'รายละเอียด'
    },
    'LOV_CODJOB': {
      '0': 'รายการรหัสลักษณะงาน',
      '1': 'รายการรหัสลักษณะงาน',
      '2': 'รหัสลักษณะงาน',
      '3': 'รายละเอียด'
    },
    'LOV_CODJOBGRAD': {
      '0': 'รายการ Job Grade',
      '1': 'รายการ Job Grade',
      '2': 'รหัส Job Grade',
      '3': 'รายละเอียด'
    },
    'LOV_TYPDISP': {
      '0': 'ประเภทความพิการ',
      '1': 'ประเภทความพิการ',
      '2': 'รหัสประเภทความพิการ',
      '3': 'รายละเอียด'
    },
    'LOV_CODGL': {
      '0': 'GL Code',
      '1': 'GL Code',
      '2': 'GL Code',
      '3': 'รายละเอียด'
    },
    'LOV_CODPOINT': {
      '0': 'จุดออกรถ',
      '1': 'จุดออกรถ',
      '2': 'รหัสจุดออกรถ',
      '3': 'รายละเอียด'
    },
    'LOV_CODBANK': {
      '0': 'รายการธนาคาร',
      '1': 'รายการธนาคาร',
      '2': 'รหัสธนาคาร',
      '3': 'รายละเอียด'
    },
    'LOV_CODCURR': {
      '0': 'รายการหน่วยเงิน',
      '1': 'รายการหน่วยเงิน',
      '2': 'รหัสหน่วยเงิน',
      '3': 'รายละเอียด'
    },
    'LOV_CODTYPDOC': {
      '0': 'รายการประกอบเอกสาร',
      '1': 'รายการประกอบเอกสาร',
      '2': 'รหัสประกอบเอกสาร',
      '3': 'รายละเอียด'
    },
    'LOV_CODTYPCOLLA': {
      '0': 'รายการประเภทหลักทรัพย์',
      '1': 'รายการประเภทหลักทรัพย์',
      '2': 'รหัสประเภทหลักทรัพย์',
      '3': 'รายละเอียด'
    },
    'LOV_CODREQST': {
      '0': 'รายการเลขที่ใบอนุมัติ',
      '1': 'รายการเลขที่ใบอนุมัติ',
      '2': 'รหัสเลขที่ใบอนุมัติ',
      '3': 'หน่วยงานที่ขอ'
    },
    'LOV_CODMENU': {
      '0': 'รายการเมนู',
      '1': 'รายการเมนู',
      '2': 'รหัสเมนู',
      '3': 'รายละเอียด'
    },
    'LOV_CODFUNCTION': {
      '0': 'รายการฟังก์ชั่น',
      '1': 'รายการฟังก์ชั่น',
      '2': 'รหัสรังก์ชั่น',
      '3': 'รายละเอียด'
    },
    'LOV_CODSKILL': {
      '0': 'รายการรหัสความสามารถ',
      '1': 'รายการรหัสความสามารถ',
      '2': 'รหัสความสามารถ',
      '3': 'รายละเอียด'
    },
    'LOV_CODAWARD': {
      '0': 'รายการรหัสประเภทเบี้ยขยัน',
      '1': 'รายการรหัสประเภทเบี้ยขยัน',
      '2': 'รหัสประเภทเบี้ยขยัน',
      '3': 'รายละเอียด'
    },
    'LOV_CODFRGTCARD': {
      '0': 'รายการรหัสลืมรูดบัตร',
      '1': 'รายการรหัสลืมรูดบัตร',
      '2': 'รหัสลืมรูดบัตร',
      '3': 'รายละเอียด'
    },
    'LOV_CODTRN': {
      '0': 'รายการรหัสข้อมูลการเคลื่อนไหว',
      '1': 'รายการรหัสข้อมูลการเคลื่อนไหว',
      '2': 'รหัสข้อมูลการเคลื่อนไหว',
      '3': 'รายละเอียด'
    },
    'LOV_TYPPAYROLL': {
      '0': 'รายการกลุ่มการจ่ายเงินเดือน',
      '1': 'รายการกลุ่มการจ่ายเงินเดือน',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_CODFRM': {
      '0': 'รายการรหัสเงื่อนไข',
      '1': 'รายการรหัสเงื่อนไข',
      '2': 'รหัสเงื่อนไข',
      '3': 'รายละเอียด'
    },
    'LOV_TEMPFILE': {
      '0': 'รายการรหัสแแบฟอร์ม',
      '1': 'รายการรหัสแแบฟอร์ม',
      '2': 'รหัสแบบฟอร์ม',
      '3': 'รายละเอียด'
    },
    'LOV_CODCHNG': {
      '0': 'รหัสเหตุผลการแก้ไขเวลาเข้า-ออก',
      '1': 'รหัสเหตุผลการแก้ไขเวลาเข้า-ออก',
      '2': 'รหัส',
      '3': 'รายละเอียด'
    },
    'LOV_NUMLEREQ': {
      '0': 'รายละเอียดเลขที่ใบขอลา',
      '1': 'รายละเอียดเลขที่ใบขอลา',
      '2': 'เลขที่ใบขอลา',
      '3': 'ชื่อ'
    },
    'LOV_CODLEAVE': {
      '0': 'ตารางรหัสการลาหยุดงาน',
      '1': 'ตารางรหัสการลาหยุดงาน',
      '2': 'รหัสการลา',
      '3': 'รายละเอียด'
    },
    'LOV_NUMLEREQG': {
      '0': 'รายละเอียดเลขที่ใบขอลาแบบกลุ่ม',
      '1': 'รายละเอียดเลขที่ใบขอลาแบบกลุ่ม',
      '2': 'เลขที่ใบขอลา',
      '3': 'กลุ่มพนักงาน',
      '4': 'หน่วยงาน'
    }
  }
}
