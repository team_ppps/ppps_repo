export let hcmLovColumns = () => {
  let codempidColumns = [
    {
      key: 'codempid',
      name: '',
      labelCodapp: 'LOV_EMP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codempid',
      name: '',
      labelCodapp: 'LOV_EMP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_staemp',
      name: '',
      labelCodapp: 'LOV_EMP',
      labelIndex: '4',
      style: 'min-width: 150px;'
    }
  ]

  let codposColumns = [
    {
      key: 'codpos',
      name: '',
      labelCodapp: 'LOV_POS',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codpos',
      name: '',
      labelCodapp: 'LOV_POS',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]

  let codappColumns = [
    {
      key: 'codapp',
      name: '',
      labelCodapp: 'LOV_CODAPP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codapp',
      name: '',
      labelCodapp: 'LOV_CODAPP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]

  let nummemoColumns = [
    {
      key: 'nummemo',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '1',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'dteyear',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'dtemonth',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '3',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'numclseq',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '4',
      class: 'align-center',
      style: 'min-width: 80px;'
    }, {
      key: 'codcours',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '5',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'namcours',
      name: '',
      labelCodapp: 'LOV_MEMO',
      labelIndex: '6',
      style: 'min-width: 200px;'
    }
  ]

  let codcoursColumns = [
    {
      key: 'codcours',
      name: '',
      labelCodapp: 'LOV_CORS',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codcours',
      name: '',
      labelCodapp: 'LOV_CORS',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]

  let codformColumns = [
    {
      key: 'codform',
      name: '',
      labelCodapp: 'LOV_FORM',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codform',
      name: '',
      labelCodapp: 'LOV_FORM',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]

  let numapplColumns = [
    {
      key: 'numappl',
      name: '',
      labelCodapp: 'LOV_APP3',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_numappl',
      name: '',
      labelCodapp: 'LOV_APP3',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_statappl',
      name: '',
      labelCodapp: 'LOV_APP3',
      labelIndex: '4',
      style: 'min-width: 200px;'
    }
  ]

  let numreqColumns = [
    {
      key: 'numreq',
      name: '',
      labelCodapp: 'LOV_REQ3',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'codcomp',
      name: '',
      labelCodapp: 'LOV_REQ3',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }, {
      key: 'codpos',
      name: '',
      labelCodapp: 'LOV_REQ3',
      labelIndex: '4',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codpos',
      name: '',
      labelCodapp: 'LOV_REQ3',
      labelIndex: '5',
      style: 'min-width: 200px;'
    }
  ]
  let codaplvlColumns = [
    {
      key: 'codaplvl',
      name: '',
      labelCodapp: 'LOV_APLV',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codaplvl',
      name: '',
      labelCodapp: 'LOV_APLV',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let grdscorColumns = [
    {
      key: 'grditem',
      name: '',
      labelCodapp: 'HRAP31EC6',
      labelIndex: '250',
      style: 'min-width: 100px;'
    }, {
      key: 'achieve',
      name: '',
      labelCodapp: 'HRAP31EC6',
      labelIndex: '260',
      style: 'min-width: 150px;'
    }, {
      key: 'qtyscor',
      name: '',
      labelCodapp: 'HRAP31EC6',
      labelIndex: '270',
      class: 'align-right',
      style: 'min-width: 100px;'
    }
  ]
  let codapprColumns = [
    {
      key: 'codappr',
      name: '',
      labelCodapp: 'LOV_CODAPPR',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codappr',
      name: '',
      labelCodapp: 'LOV_CODAPPR',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_staemp',
      name: '',
      labelCodapp: 'LOV_CODAPPR',
      labelIndex: '4',
      style: 'min-width: 150px;'
    }
  ]
  let codshiftColumns = [
    {
      key: 'codshift',
      name: '',
      labelCodapp: 'LOV_SHIF',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codshift',
      name: '',
      labelCodapp: 'LOV_SHIF',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }, {
      key: 'schedule',
      name: '',
      labelCodapp: 'LOV_SHIF',
      labelIndex: '4',
      class: 'align-center',
      style: 'min-width: 150px;'
    }
  ]
  let codroomColumns = [
    {
      key: 'codroom',
      name: '',
      labelCodapp: 'LOV_ROOM',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codroom',
      name: '',
      labelCodapp: 'LOV_ROOM',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codassetColumns = [
    {
      key: 'codasset',
      name: '',
      labelCodapp: 'LOV_ASSET',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codasset',
      name: '',
      labelCodapp: 'LOV_ASSET',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codcalenColumns = [
    {
      key: 'codcalen',
      name: '',
      labelCodapp: 'LOV_WORK',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codcalen',
      name: '',
      labelCodapp: 'LOV_WORK',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codcompyColumns = [
    {
      key: 'codcompy',
      name: '',
      labelCodapp: 'LOV_COMP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codcompy',
      name: '',
      labelCodapp: 'LOV_COMP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let coduserColumns = [
    {
      key: 'coduser',
      name: '',
      labelCodapp: 'LOV_CODUSER',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_coduser',
      name: '',
      labelCodapp: 'LOV_CODUSER',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codsecuColumns = [
    {
      key: 'codsecu',
      name: '',
      labelCodapp: 'LOV_CODSECU',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codsecu',
      name: '',
      labelCodapp: 'LOV_CODSECU',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codprocColumns = [
    {
      key: 'codproc',
      name: '',
      labelCodapp: 'LOV_CODPROC',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codproc',
      name: '',
      labelCodapp: 'LOV_CODPROC',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codincomColumns = [
    {
      key: 'codincom',
      name: '',
      labelCodapp: 'LOV_CODINCOM',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codincom',
      name: '',
      labelCodapp: 'LOV_CODINCOM',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codretroColumns = [
    {
      key: 'codretro',
      name: '',
      labelCodapp: 'LOV_CODRETRO',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codretro',
      name: '',
      labelCodapp: 'LOV_CODRETRO',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtableColumns = [
    {
      key: 'codtable',
      name: '',
      labelCodapp: 'LOV_CODTABLE',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtable',
      name: '',
      labelCodapp: 'LOV_CODTABLE',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codvalColumns = [
    {
      key: 'codval',
      name: '',
      labelCodapp: 'LOV_CODVAL',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codval',
      name: '',
      labelCodapp: 'LOV_CODVAL',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let costCenterColumns = [
    {
      key: 'costcent',
      name: '',
      labelCodapp: 'LOV_COSTCENT',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_costcent',
      name: '',
      labelCodapp: 'LOV_COSTCENT',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let compGroupColumns = [
    {
      key: 'compgrp',
      name: '',
      labelCodapp: 'LOV_COMPGRP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_compgrp',
      name: '',
      labelCodapp: 'LOV_COMPGRP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtencyColumns = [
    {
      key: 'codtency',
      name: '',
      labelCodapp: 'LOV_CODTENCY',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtency',
      name: '',
      labelCodapp: 'LOV_CODTENCY',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codoccupColumns = [
    {
      key: 'codoccup',
      name: '',
      labelCodapp: 'LOV_CODOCCUP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codoccup',
      name: '',
      labelCodapp: 'LOV_CODOCCUP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codhospColumns = [
    {
      key: 'codhosp',
      name: '',
      labelCodapp: 'LOV_CODHOSP',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codhosp',
      name: '',
      labelCodapp: 'LOV_CODHOSP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codprovColumns = [
    {
      key: 'codprov',
      name: '',
      labelCodapp: 'LOV_CODPROV',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codprov',
      name: '',
      labelCodapp: 'LOV_CODPROV',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codgrpworkColumns = [
    {
      key: 'codgrpwork',
      name: '',
      labelCodapp: 'LOV_CODGRPWORK',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codgrpwork',
      name: '',
      labelCodapp: 'LOV_CODGRPWORK',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codcarcabColumns = [
    {
      key: 'codcarcab',
      name: '',
      labelCodapp: 'LOV_CODCARCAB',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codcarcab',
      name: '',
      labelCodapp: 'LOV_CODCARCAB',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtypincColumns = [
    {
      key: 'codtypinc',
      name: '',
      labelCodapp: 'LOV_CODTYPINC',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtypinc',
      name: '',
      labelCodapp: 'LOV_CODTYPINC',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtyppayrColumns = [
    {
      key: 'codtyppayr',
      name: '',
      labelCodapp: 'LOV_CODTYPPAYR',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtyppayr',
      name: '',
      labelCodapp: 'LOV_CODTYPPAYR',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtyppaytColumns = [
    {
      key: 'codtyppayt',
      name: '',
      labelCodapp: 'LOV_CODTYPPAYT',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtyppayt',
      name: '',
      labelCodapp: 'LOV_CODTYPPAYT',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtaxColumns = [
    {
      key: 'codtax',
      name: '',
      labelCodapp: 'LOV_CODTAX',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codtax',
      name: '',
      labelCodapp: 'LOV_CODTAX',
      labelIndex: '3',
      class: 'align-center',
      style: 'min-width: 150px;'
    }
  ]
  let numotreqColumns = [
    {
      key: 'numotreq',
      name: '',
      labelCodapp: 'LOV_NUMOTREQ',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codempid',
      name: '',
      labelCodapp: 'LOV_NUMOTREQ',
      labelIndex: '3',
      class: 'align-center',
      style: 'min-width: 200px;'
    }, {
      key: 'namcent',
      name: '',
      labelCodapp: 'LOV_NUMOTREQ',
      labelIndex: '4',
      class: 'align-center',
      style: 'min-width: 250px;'
    }, {
      key: 'namcalen',
      name: '',
      labelCodapp: 'LOV_NUMOTREQ',
      labelIndex: '5',
      style: 'min-width: 250px;'
    }
  ]
  let codremColumns = [
    {
      key: 'codrem',
      name: '',
      labelCodapp: 'LOV_CODREM',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codrem',
      name: '',
      labelCodapp: 'LOV_CODREM',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let typeleaveColumns = [
    {
      key: 'typeleave',
      name: '',
      labelCodapp: 'LOV_TYPELEAVE',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_typeleave',
      name: '',
      labelCodapp: 'LOV_TYPELEAVE',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codpayColumns = [
    {
      key: 'codpay',
      name: '',
      labelCodapp: 'LOV_CODPAY',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codpay',
      name: '',
      labelCodapp: 'LOV_CODPAY',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codpayrolColumns = [
    {
      key: 'codpayrol',
      name: '',
      labelCodapp: 'LOV_CODPAYROL',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codpayrol',
      name: '',
      labelCodapp: 'LOV_CODPAYROL',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codfundColumns = [
    {
      key: 'codfund',
      name: '',
      labelCodapp: 'LOV_CODFUND',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codfund',
      name: '',
      labelCodapp: 'LOV_CODFUND',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codplanColumns = [
    {
      key: 'codplan',
      name: '',
      labelCodapp: 'LOV_CODPLAN',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codplan',
      name: '',
      labelCodapp: 'LOV_CODPLAN',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codcauseColumns = [
    {
      key: 'codcause',
      name: '',
      labelCodapp: 'LOV_CODCAUSE',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 150px;'
    }, {
      key: 'desc_codcause',
      name: '',
      labelCodapp: 'LOV_CODCAUSE',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let typrewdColumns = [
    {
      key: 'typrewd',
      name: '',
      labelCodapp: 'LOV_TYPREWD',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_typrewd',
      name: '',
      labelCodapp: 'LOV_TYPREWD',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let countryColumns = [
    {
      key: 'country',
      name: '',
      labelCodapp: 'LOV_COUNTRY',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_country',
      name: '',
      labelCodapp: 'LOV_COUNTRY',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codedlvColumns = [
    {
      key: 'codedlv',
      name: '',
      labelCodapp: 'LOV_CODEDLV',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codedlv',
      name: '',
      labelCodapp: 'LOV_CODEDLV',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codnatnlColumns = [
    {
      key: 'codnatnl',
      name: '',
      labelCodapp: 'LOV_CODEDLV',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codnatnl',
      name: '',
      labelCodapp: 'LOV_CODEDLV',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codrelgnColumns = [
    {
      key: 'codrelgn',
      name: '',
      labelCodapp: 'LOV_CODEDLV',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codrelgn',
      name: '',
      labelCodapp: 'LOV_CODEDLV',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let coddistColumns = [
    {
      key: 'coddist',
      name: '',
      labelCodapp: 'LOV_CODDIST',
      labelIndex: '1',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_coddist',
      name: '',
      labelCodapp: 'LOV_CODDIST',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }]
  let coddglvColumns = [
    {
      key: 'coddglv',
      name: '',
      labelCodapp: 'LOV_CODDGLV',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_coddglv',
      name: '',
      labelCodapp: 'LOV_CODDGLV',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codmajsbColumns = [
    {
      key: 'codmajsb',
      name: '',
      labelCodapp: 'LOV_CODMAJSB',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codmajsb',
      name: '',
      labelCodapp: 'LOV_CODMAJSB',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codminsbColumns = [
    {
      key: 'codminsb',
      name: '',
      labelCodapp: 'LOV_CODMINSB',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codminsb',
      name: '',
      labelCodapp: 'LOV_CODMINSB',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codinstColumns = [
    {
      key: 'codinst',
      name: '',
      labelCodapp: 'LOV_CODINST',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codinst',
      name: '',
      labelCodapp: 'LOV_CODINST',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codsubdistColumns = [
    {
      key: 'codsubdist',
      name: '',
      labelCodapp: 'LOV_CODSUBDIST',
      labelIndex: '1',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codsubdist',
      name: '',
      labelCodapp: 'LOV_CODSUBDIST',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }
  ]
  let codbrlcColumns = [
    {
      key: 'codbrlc',
      name: '',
      labelCodapp: 'LOV_CODBRLC',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codbrlc',
      name: '',
      labelCodapp: 'LOV_CODBRLC',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codempmtColumns = [
    {
      key: 'codempmt',
      name: '',
      labelCodapp: 'LOV_CODEMPMT',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codempmt',
      name: '',
      labelCodapp: 'LOV_CODEMPMT',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtypempColumns = [
    {
      key: 'codtypemp',
      name: '',
      labelCodapp: 'LOV_CODTYPEMP',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codtypemp',
      name: '',
      labelCodapp: 'LOV_CODTYPEMP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codjobColumns = [
    {
      key: 'codjob',
      name: '',
      labelCodapp: 'LOV_CODJOB',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codjob',
      name: '',
      labelCodapp: 'LOV_CODJOB',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codjobgradColumns = [
    {
      key: 'codjobgrad',
      name: '',
      labelCodapp: 'LOV_CODJOBGRAD',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codjobgrad',
      name: '',
      labelCodapp: 'LOV_CODJOBGRAD',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let typdispColumns = [
    {
      key: 'typdisp',
      name: '',
      labelCodapp: 'LOV_TYPDISP',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_typdisp',
      name: '',
      labelCodapp: 'LOV_TYPDISP',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codglColumns = [
    {
      key: 'codgl',
      name: '',
      labelCodapp: 'LOV_CODGL',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codgl',
      name: '',
      labelCodapp: 'LOV_CODGL',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codpointColumns = [
    {
      key: 'codpoint',
      name: '',
      labelCodapp: 'LOV_CODPOINT',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codpoint',
      name: '',
      labelCodapp: 'LOV_CODPOINT',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codbankColumns = [
    {
      key: 'codbank',
      name: '',
      labelCodapp: 'LOV_CODBANK',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codbank',
      name: '',
      labelCodapp: 'LOV_CODBANK',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codcurrColumns = [
    {
      key: 'codcurr',
      name: '',
      labelCodapp: 'LOV_CODCURR',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codcurr',
      name: '',
      labelCodapp: 'LOV_CODCURR',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtypdocColumns = [
    {
      key: 'codtypdoc',
      name: '',
      labelCodapp: 'LOV_CODTYPDOC',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codtypdoc',
      name: '',
      labelCodapp: 'LOV_CODTYPDOC',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtypcollaColumns = [
    {
      key: 'codtypcolla',
      name: '',
      labelCodapp: 'LOV_CODTYPCOLLA',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codtypcolla',
      name: '',
      labelCodapp: 'LOV_CODTYPCOLLA',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codreqstColumns = [
    {
      key: 'codreqst',
      name: '',
      labelCodapp: 'LOV_CODREQST',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codreqst',
      name: '',
      labelCodapp: 'LOV_CODREQST',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codmenuColumns = [
    {
      key: 'codmenu',
      name: '',
      labelCodapp: 'LOV_CODMENU',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codmenu',
      name: '',
      labelCodapp: 'LOV_CODMENU',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codfunctionColumns = [
    {
      key: 'codfunction',
      name: '',
      labelCodapp: 'LOV_CODFUNCTION',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codfunction',
      name: '',
      labelCodapp: 'LOV_CODFUNCTION',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codskillColumns = [
    {
      key: 'codskill',
      name: '',
      labelCodapp: 'LOV_CODSKILL',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codskill',
      name: '',
      labelCodapp: 'LOV_CODSKILL',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codawardColumns = [
    {
      key: 'codaward',
      name: '',
      labelCodapp: 'LOV_CODAWARD',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codaward',
      name: '',
      labelCodapp: 'LOV_CODAWARD',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codfrgtcardColumns = [
    {
      key: 'codfrgtcard',
      name: '',
      labelCodapp: 'LOV_CODFRGTCARD',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codfrgtcard',
      name: '',
      labelCodapp: 'LOV_CODFRGTCARD',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codtrnColumns = [
    {
      key: 'codtrn',
      name: '',
      labelCodapp: 'LOV_CODTRN',
      labelIndex: '2',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codtrn',
      name: '',
      labelCodapp: 'LOV_CODTRN',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let typpayrollColumns = [
    {
      key: 'typpayroll',
      name: '',
      labelCodapp: 'LOV_TYPPAYROLL',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_typpayroll',
      name: '',
      labelCodapp: 'LOV_TYPPAYROLL',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codfrmColumns = [
    {
      key: 'codfrm',
      name: '',
      labelCodapp: 'LOV_CODFRM',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codfrm',
      name: '',
      labelCodapp: 'LOV_CODFRM',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let tempfileColumns = [
    {
      key: 'tempfile',
      name: '',
      labelCodapp: 'LOV_TEMPFILE',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_tempfile',
      name: '',
      labelCodapp: 'LOV_TEMPFILE',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codchngColumns = [
    {
      key: 'codchng',
      name: '',
      labelCodapp: 'LOV_CODCHNG',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codchng',
      name: '',
      labelCodapp: 'LOV_CODCHNG',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let numlereqColumns = [
    {
      key: 'numlereq',
      name: '',
      labelCodapp: 'LOV_NUMLEREQ',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codempid',
      name: '',
      labelCodapp: 'LOV_NUMLEREQ',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let codleaveColumns = [
    {
      key: 'codleave',
      name: '',
      labelCodapp: 'LOV_CODLEAVE',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codleave',
      name: '',
      labelCodapp: 'LOV_CODLEAVE',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }
  ]
  let numlereqgColumns = [
    {
      key: 'numlereqg',
      name: '',
      labelCodapp: 'LOV_NUMLEREQG',
      labelIndex: '2',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'numcalen',
      name: '',
      labelCodapp: 'LOV_NUMLEREQG',
      labelIndex: '3',
      style: 'min-width: 200px;'
    }, {
      key: 'codcomp',
      name: '',
      labelCodapp: 'LOV_NUMLEREQG',
      labelIndex: '4',
      style: 'min-width: 200px;'
    }
  ]

  return {
    en: {
      codempid: codempidColumns,
      codpos: codposColumns,
      codapp: codappColumns,
      nummemo: nummemoColumns,
      codcours: codcoursColumns,
      codform: codformColumns,
      numappl: numapplColumns,
      numreq: numreqColumns,
      codaplvl: codaplvlColumns,
      grdscor: grdscorColumns,
      codappr: codapprColumns,
      codshift: codshiftColumns,
      codroom: codroomColumns,
      codasset: codassetColumns,
      codcalen: codcalenColumns,
      codcompy: codcompyColumns,
      coduser: coduserColumns,
      codsecu: codsecuColumns,
      codproc: codprocColumns,
      codincom: codincomColumns,
      codretro: codretroColumns,
      codtable: codtableColumns,
      codval: codvalColumns,
      costcent: costCenterColumns,
      compgrp: compGroupColumns,
      codtency: codtencyColumns,
      codoccup: codoccupColumns,
      codhosp: codhospColumns,
      codprov: codprovColumns,
      codgrpwork: codgrpworkColumns,
      codcarcab: codcarcabColumns,
      codtypinc: codtypincColumns,
      codtyppayr: codtyppayrColumns,
      codtyppayt: codtyppaytColumns,
      codtax: codtaxColumns,
      numotreq: numotreqColumns,
      codrem: codremColumns,
      typeleave: typeleaveColumns,
      codpay: codpayColumns,
      codpayrol: codpayrolColumns,
      codfund: codfundColumns,
      codplan: codplanColumns,
      codcause: codcauseColumns,
      typrewd: typrewdColumns,
      country: countryColumns,
      codedlv: codedlvColumns,
      codnatnl: codnatnlColumns,
      codrelgn: codrelgnColumns,
      coddist: coddistColumns,
      coddglv: coddglvColumns,
      codmajsb: codmajsbColumns,
      codminsb: codminsbColumns,
      codinst: codinstColumns,
      codsubdist: codsubdistColumns,
      codbrlc: codbrlcColumns,
      codempmt: codempmtColumns,
      codtypemp: codtypempColumns,
      codjob: codjobColumns,
      codjobgrad: codjobgradColumns,
      typdisp: typdispColumns,
      codgl: codglColumns,
      codpoint: codpointColumns,
      codbank: codbankColumns,
      codcurr: codcurrColumns,
      codtypdoc: codtypdocColumns,
      codtypcolla: codtypcollaColumns,
      codreqst: codreqstColumns,
      codmenu: codmenuColumns,
      codfunction: codfunctionColumns,
      codskill: codskillColumns,
      codaward: codawardColumns,
      codfrgtcard: codfrgtcardColumns,
      codtrn: codtrnColumns,
      typpayroll: typpayrollColumns,
      codfrm: codfrmColumns,
      tempfile: tempfileColumns,
      codchng: codchngColumns,
      numlereq: numlereqColumns,
      codleave: codleaveColumns,
      numlereqg: numlereqgColumns
    },
    th: {
      codempid: codempidColumns,
      codpos: codposColumns,
      codapp: codappColumns,
      nummemo: nummemoColumns,
      codcours: codcoursColumns,
      codform: codformColumns,
      numappl: numapplColumns,
      numreq: numreqColumns,
      codaplvl: codaplvlColumns,
      grdscor: grdscorColumns,
      codappr: codapprColumns,
      codshift: codshiftColumns,
      codroom: codroomColumns,
      codasset: codassetColumns,
      codcalen: codcalenColumns,
      codcompy: codcompyColumns,
      coduser: coduserColumns,
      codsecu: codsecuColumns,
      codproc: codprocColumns,
      codincom: codincomColumns,
      codretro: codretroColumns,
      codtable: codtableColumns,
      codval: codvalColumns,
      costcent: costCenterColumns,
      compgrp: compGroupColumns,
      codtency: codtencyColumns,
      codoccup: codoccupColumns,
      codhosp: codhospColumns,
      codprov: codprovColumns,
      codgrpwork: codgrpworkColumns,
      codcarcab: codcarcabColumns,
      codtypinc: codtypincColumns,
      codtyppayr: codtyppayrColumns,
      codtyppayt: codtyppaytColumns,
      codtax: codtaxColumns,
      numotreq: numotreqColumns,
      codrem: codremColumns,
      typeleave: typeleaveColumns,
      codpay: codpayColumns,
      codpayrol: codpayrolColumns,
      codfund: codfundColumns,
      codplan: codplanColumns,
      codcause: codcauseColumns,
      typrewd: typrewdColumns,
      country: countryColumns,
      codedlv: codedlvColumns,
      codnatnl: codnatnlColumns,
      codrelgn: codrelgnColumns,
      coddist: coddistColumns,
      coddglv: coddglvColumns,
      codmajsb: codmajsbColumns,
      codminsb: codminsbColumns,
      codinst: codinstColumns,
      codsubdist: codsubdistColumns,
      codbrlc: codbrlcColumns,
      codempmt: codempmtColumns,
      codtypemp: codtypempColumns,
      codjob: codjobColumns,
      codjobgrad: codjobgradColumns,
      typdisp: typdispColumns,
      codgl: codglColumns,
      codpoint: codpointColumns,
      codbank: codbankColumns,
      codcurr: codcurrColumns,
      codtypdoc: codtypdocColumns,
      codtypcolla: codtypcollaColumns,
      codreqst: codreqstColumns,
      codmenu: codmenuColumns,
      codfunction: codfunctionColumns,
      codskill: codskillColumns,
      codaward: codawardColumns,
      codfrgtcard: codfrgtcardColumns,
      codtrn: codtrnColumns,
      typpayroll: typpayrollColumns,
      codfrm: codfrmColumns,
      tempfile: tempfileColumns,
      codchng: codchngColumns,
      numlereq: numlereqColumns,
      codleave: codleaveColumns,
      numlereqg: numlereqgColumns
    }
  }
}
