import * as types from './mutation-types'
import hcmLov from '../api'
import swal from 'sweetalert'
import hcmLovLabels from '../assets/labels'
import { hcmLovColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'

export default {
  state: {
    hcmLov: {
      codempid: { total: 0, rows: [], loading: false, finish: false },
      codpos: { total: 0, rows: [], loading: false, finish: false },
      codapp: { total: 0, rows: [], loading: false, finish: false },
      nummemo: { total: 0, rows: [], loading: false, finish: false },
      codcours: { total: 0, rows: [], loading: false, finish: false },
      codform: { total: 0, rows: [], loading: false, finish: false },
      numappl: { total: 0, rows: [], loading: false, finish: false },
      numreq: { total: 0, rows: [], loading: false, finish: false },
      codaplvl: { total: 0, rows: [], loading: false, finish: false },
      grdscor: { total: 0, rows: [], loading: false, finish: false },
      codappr: { total: 0, rows: [], loading: false, finish: false },
      codshift: { total: 0, rows: [], loading: false, finish: false },
      codroom: { total: 0, rows: [], loading: false, finish: false },
      codasset: { total: 0, rows: [], loading: false, finish: false },
      codcalen: { total: 0, rows: [], loading: false, finish: false },
      codcompy: { total: 0, rows: [], loading: false, finish: false },
      coduser: { total: 0, rows: [], loading: false, finish: false },
      codsecu: { total: 0, rows: [], loading: false, finish: false },
      codproc: { total: 0, rows: [], loading: false, finish: false },
      codincom: { total: 0, rows: [], loading: false, finish: false },
      codretro: { total: 0, rows: [], loading: false, finish: false },
      codtable: { total: 0, rows: [], loading: false, finish: false },
      codval: { total: 0, rows: [], loading: false, finish: false },
      costcent: { total: 0, rows: [], loading: false, finish: false },
      compgrp: { total: 0, rows: [], loading: false, finish: false },
      codtency: { total: 0, rows: [], loading: false, finish: false },
      codoccup: { total: 0, rows: [], loading: false, finish: false },
      codhosp: { total: 0, rows: [], loading: false, finish: false },
      codprov: { total: 0, rows: [], loading: false, finish: false },
      codgrpwork: { total: 0, rows: [], loading: false, finish: false },
      codcarcab: { total: 0, rows: [], loading: false, finish: false },
      codtypinc: { total: 0, rows: [], loading: false, finish: false },
      codtyppayr: { total: 0, rows: [], loading: false, finish: false },
      codtyppayt: { total: 0, rows: [], loading: false, finish: false },
      codtax: { total: 0, rows: [], loading: false, finish: false },
      numotreq: { total: 0, rows: [], loading: false, finish: false },
      codrem: { total: 0, rows: [], loading: false, finish: false },
      typeleave: { total: 0, rows: [], loading: false, finish: false },
      codpay: { total: 0, rows: [], loading: false, finish: false },
      codpayrol: { total: 0, rows: [], loading: false, finish: false },
      codfund: { total: 0, rows: [], loading: false, finish: false },
      codplan: { total: 0, rows: [], loading: false, finish: false },
      codcause: { total: 0, rows: [], loading: false, finish: false },
      typrewd: { total: 0, rows: [], loading: false, finish: false },
      country: { total: 0, rows: [], loading: false, finish: false },
      codedlv: { total: 0, rows: [], loading: false, finish: false },
      codnatnl: { total: 0, rows: [], loading: false, finish: false },
      codrelgn: { total: 0, rows: [], loading: false, finish: false },
      coddist: { total: 0, rows: [], loading: false, finish: false },
      coddglv: { total: 0, rows: [], loading: false, finish: false },
      codmajsb: { total: 0, rows: [], loading: false, finish: false },
      codminsb: { total: 0, rows: [], loading: false, finish: false },
      codinst: { total: 0, rows: [], loading: false, finish: false },
      codsubdist: { total: 0, rows: [], loading: false, finish: false },
      codbrlc: { total: 0, rows: [], loading: false, finish: false },
      codempmt: { total: 0, rows: [], loading: false, finish: false },
      codtypemp: { total: 0, rows: [], loading: false, finish: false },
      codjob: { total: 0, rows: [], loading: false, finish: false },
      codjobgrad: { total: 0, rows: [], loading: false, finish: false },
      typdisp: { total: 0, rows: [], loading: false, finish: false },
      codgl: { total: 0, rows: [], loading: false, finish: false },
      codpoint: { total: 0, rows: [], loading: false, finish: false },
      codbank: { total: 0, rows: [], loading: false, finish: false },
      codcurr: { total: 0, rows: [], loading: false, finish: false },
      codtypdoc: { total: 0, rows: [], loading: false, finish: false },
      codtypcolla: { total: 0, rows: [], loading: false, finish: false },
      codreqst: { total: 0, rows: [], loading: false, finish: false },
      codmenu: { total: 0, rows: [], loading: false, finish: false },
      codfunction: { total: 0, rows: [], loading: false, finish: false },
      codskill: { total: 0, rows: [], loading: false, finish: false },
      codaward: { total: 0, rows: [], loading: false, finish: false },
      codfrgtcard: { total: 0, rows: [], loading: false, finish: false },
      codtrn: { total: 0, rows: [], loading: false, finish: false },
      typpayroll: { total: 0, rows: [], loading: false, finish: false },
      codfrm: { total: 0, rows: [], loading: false, finish: false },
      tempfile: { total: 0, rows: [], loading: false, finish: false },
      codchng: { total: 0, rows: [], loading: false, finish: false },
      numlereq: { total: 0, rows: [], loading: false, finish: false },
      codleave: { total: 0, rows: [], loading: false, finish: false },
      numlereqg: { total: 0, rows: [], loading: false, finish: false }
    },
    hcmLovSearch: [],
    hcmLovTitle: {
      codempid: 'List of Employee',
      codpos: 'List of Position',
      codapp: 'List of Application',
      nummemo: 'List of Memo',
      codcours: 'List of Course',
      codform: 'List of Form',
      numappl: 'List of Application Number',
      numreq: 'List of Request',
      codaplvl: 'List of Appraisal Group',
      grdscor: 'List of Grade Score',
      codappr: 'List of Approval',
      codshift: 'List of Shift',
      codroom: 'List of Room',
      codasset: 'List of Asset',
      codcalen: 'List of Group',
      codcompy: 'List of Company',
      coduser: 'List of User Name',
      codsecu: 'List of Security Group',
      codproc: 'List of Work Process',
      codincom: 'List of Fixed Income Code',
      codretro: 'List Of Other Income Code',
      codtable: 'List Of Table Code',
      codval: 'List Of Value Code',
      costcent: 'List Of Cost Center',
      compgrp: 'List Of Company Group',
      codtency: 'Type of Special Skills/Exp.',
      codoccup: 'List Of Occupation',
      codhosp: 'List Of Hospital',
      codprov: 'List Of Province',
      codgrpwork: 'List Of Group Work',
      codcarcab: 'List Of Car Cable',
      codtypinc: 'List Of Type Inc',
      codtyppayr: 'List Of Pay Slip',
      codtyppayt: 'List Of Tax Certificate',
      codtax: 'List Of Tax Code',
      numotreq: 'No. of OT Request',
      codrem: 'Reasons for Overtime Request',
      typeleave: 'List of Type of Leave',
      codpay: 'List of Deduct Code ',
      codpayrol: 'List Of Payroll Code',
      codfund: 'List Of Fund Code',
      codplan: 'List Of Plan Code',
      codcause: 'List Of Cause Code',
      typrewd: 'Details of Award',
      country: 'List Of Country Code',
      codedlv: 'Educating Level',
      codnatnl: 'List Of Nationality Code',
      codrelgn: 'List Of Religion Code',
      coddist: 'List Of District Code',
      coddglv: 'Academic Qualification',
      codmajsb: 'List of major subject',
      codminsb: 'Academic Field',
      codinst: 'Institute Code',
      codsubdist: 'List Of Sub District Code',
      codbrlc: 'List Of Work Place',
      codempmt: 'List Of Employment Type',
      codtypemp: 'List Of Employee Type',
      codjob: 'List Of Job',
      codjobgrad: 'List Of Job Grade',
      typdisp: 'List Of Disability Type',
      codgl: 'List Of GL Code',
      codpoint: 'List Of Point Code',
      codbank: 'List Of Bank Code',
      codcurr: 'List Of Monetary unit',
      codtypdoc: 'List Of Document Type',
      codtypcolla: 'List Of Type Collateral',
      codreqst: 'List Of Request',
      codmenu: 'List Of Menu',
      codfunction: 'List Of Function',
      codskill: 'List Of Skill',
      codaward: 'List Of Award',
      codfrgtcard: 'List Of Forgive card',
      codtrn: 'List Of Movement type',
      typpayroll: 'List Of Payroll Code',
      codfrm: 'List Of Condition Code',
      tempfile: 'List Of Tempfile Code',
      codchng: 'Reas. for Corr. of Time In/Out',
      numlereq: 'List of Leave Request',
      codleave: 'List of Leave Code',
      numlereqg: 'List of Group Leave Request'
    },
    lovCurrentCode: function (type, descCode) {
      let rowIndex = this.$store.getters[types.GET_HCM_LOV][type].rows.map(function (lov) {
        return lov['desc_' + type]
      }).indexOf(descCode)
      if (rowIndex >= 0) {
        if (typeof this.$store.getters[types.GET_HCM_LOV][type].rows[rowIndex] !== 'undefined') {
          return this.$store.getters[types.GET_HCM_LOV][type].rows[rowIndex][type]
        }
      }
      return ''
    },
    lovCurrentDesc: function (type, code) {
      let rowIndex = this.$store.getters[types.GET_HCM_LOV][type].rows.map(function (lov) {
        return lov[type]
      }).indexOf(code)
      if (rowIndex >= 0) {
        if (typeof this.$store.getters[types.GET_HCM_LOV][type].rows[rowIndex] !== 'undefined') {
          return this.$store.getters[types.GET_HCM_LOV][type].rows[rowIndex]['desc_' + type]
        }
      }
      return ''
    },
    columnsLabelsCalled: false,
    hcmLovLabels: hcmLovLabels,
    hcmLovColumns: hcmLovColumns()
  },
  getters: {
    [types.GET_HCM_LOV_COLUMNS] (state) {
      return state.hcmLovColumns
    },
    [types.GET_HCM_LOV_LABELS] (state) {
      return state.hcmLovLabels
    },
    [types.GET_HCM_LOV] (state) {
      return state.hcmLov
    },
    [types.GET_HCM_LOV_SEARCH] (state) {
      return state.hcmLovSearch
    },
    [types.GET_HCM_LOV_TITLE] (state) {
      return state.hcmLovTitle
    },
    [types.GET_HCM_LOV_CURRENT_CODE] (state) {
      return state.lovCurrentCode
    },
    [types.GET_HCM_LOV_CURRENT_DESC] (state) {
      return state.lovCurrentDesc
    }
  },
  mutations: {
    [types.SET_HCM_LOV_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.hcmLovColumns[keyLang].codempid = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codempid, labels[keyLang])
        state.hcmLovColumns[keyLang].codpos = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codpos, labels[keyLang])
        state.hcmLovColumns[keyLang].codapp = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codapp, labels[keyLang])
        state.hcmLovColumns[keyLang].nummemo = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].nummemo, labels[keyLang])
        state.hcmLovColumns[keyLang].codcours = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codcours, labels[keyLang])
        state.hcmLovColumns[keyLang].codform = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codform, labels[keyLang])
        state.hcmLovColumns[keyLang].numappl = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].numappl, labels[keyLang])
        state.hcmLovColumns[keyLang].numreq = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].numreq, labels[keyLang])
        state.hcmLovColumns[keyLang].codaplvl = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codaplvl, labels[keyLang])
        state.hcmLovColumns[keyLang].grdscor = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].grdscor, labels[keyLang])
        state.hcmLovColumns[keyLang].codshift = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codshift, labels[keyLang])
        state.hcmLovColumns[keyLang].codroom = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codroom, labels[keyLang])
        state.hcmLovColumns[keyLang].codasset = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codasset, labels[keyLang])
        state.hcmLovColumns[keyLang].codcalen = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codcalen, labels[keyLang])
        state.hcmLovColumns[keyLang].coduser = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].coduser, labels[keyLang])
        state.hcmLovColumns[keyLang].codsecu = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codsecu, labels[keyLang])
        state.hcmLovColumns[keyLang].codproc = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codproc, labels[keyLang])
        state.hcmLovColumns[keyLang].codincom = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codincom, labels[keyLang])
        state.hcmLovColumns[keyLang].codretro = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codretro, labels[keyLang])
        state.hcmLovColumns[keyLang].codtable = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codtable, labels[keyLang])
        state.hcmLovColumns[keyLang].codval = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codval, labels[keyLang])
        state.hcmLovColumns[keyLang].costcent = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].costcent, labels[keyLang])
        state.hcmLovColumns[keyLang].compgrp = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].compgrp, labels[keyLang])
        state.hcmLovColumns[keyLang].codtency = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codtency, labels[keyLang])
        state.hcmLovColumns[keyLang].codoccup = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codoccup, labels[keyLang])
        state.hcmLovColumns[keyLang].codhosp = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codhosp, labels[keyLang])
        state.hcmLovColumns[keyLang].codprov = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codprov, labels[keyLang])
        state.hcmLovColumns[keyLang].codgrpwork = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codgrpwork, labels[keyLang])
        state.hcmLovColumns[keyLang].codcarcab = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codcarcab, labels[keyLang])
        state.hcmLovColumns[keyLang].codtypinc = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codtypinc, labels[keyLang])
        state.hcmLovColumns[keyLang].codtyppayr = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codtyppayr, labels[keyLang])
        state.hcmLovColumns[keyLang].codtyppayt = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codtyppayt, labels[keyLang])
        state.hcmLovColumns[keyLang].codtax = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codtax, labels[keyLang])
        state.hcmLovColumns[keyLang].numotreq = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].numotreq, labels[keyLang])
        state.hcmLovColumns[keyLang].codrem = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codrem, labels[keyLang])
        state.hcmLovColumns[keyLang].typeleave = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].typeleave, labels[keyLang])
        state.hcmLovColumns[keyLang].codpay = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codpay, labels[keyLang])
        state.hcmLovColumns[keyLang].codpayrol = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codpayrol, labels[keyLang])
        state.hcmLovColumns[keyLang].codfund = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codfund, labels[keyLang])
        state.hcmLovColumns[keyLang].codplan = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codplan, labels[keyLang])
        state.hcmLovColumns[keyLang].codcause = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codcause, labels[keyLang])
        state.hcmLovColumns[keyLang].typrewd = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].typrewd, labels[keyLang])
        state.hcmLovColumns[keyLang].country = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].country, labels[keyLang])
        state.hcmLovColumns[keyLang].codedlv = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codedlv, labels[keyLang])
        state.hcmLovColumns[keyLang].codnatnl = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codnatnl, labels[keyLang])
        state.hcmLovColumns[keyLang].codrelgn = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codrelgn, labels[keyLang])
        state.hcmLovColumns[keyLang].coddist = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].coddist, labels[keyLang])
        state.hcmLovColumns[keyLang].coddglv = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].coddglv, labels[keyLang])
        state.hcmLovColumns[keyLang].codmajsb = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codmajsb, labels[keyLang])
        state.hcmLovColumns[keyLang].codminsb = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codminsb, labels[keyLang])
        state.hcmLovColumns[keyLang].codinst = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codinst, labels[keyLang])
        state.hcmLovColumns[keyLang].codsubdist = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codsubdist, labels[keyLang])
        state.hcmLovColumns[keyLang].codbrlc = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codbrlc, labels[keyLang])
        state.hcmLovColumns[keyLang].codempmt = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codempmt, labels[keyLang])
        state.hcmLovColumns[keyLang].codtypemp = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codtypemp, labels[keyLang])
        state.hcmLovColumns[keyLang].codjob = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codjob, labels[keyLang])
        state.hcmLovColumns[keyLang].codcompy = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codcompy, labels[keyLang])
        state.hcmLovColumns[keyLang].codjobgrad = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codjobgrad, labels[keyLang])
        state.hcmLovColumns[keyLang].typdisp = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].typdisp, labels[keyLang])
        state.hcmLovColumns[keyLang].codgl = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codgl, labels[keyLang])
        state.hcmLovColumns[keyLang].codpoint = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codpoint, labels[keyLang])
        state.hcmLovColumns[keyLang].codbank = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codbank, labels[keyLang])
        state.hcmLovColumns[keyLang].codcurr = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codcurr, labels[keyLang])
        state.hcmLovColumns[keyLang].codtypdoc = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codtypdoc, labels[keyLang])
        state.hcmLovColumns[keyLang].codtypcolla = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codtypcolla, labels[keyLang])
        state.hcmLovColumns[keyLang].codreqst = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codreqst, labels[keyLang])
        state.hcmLovColumns[keyLang].codmenu = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codmenu, labels[keyLang])
        state.hcmLovColumns[keyLang].codfunction = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codfunction, labels[keyLang])
        state.hcmLovColumns[keyLang].codskill = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codskill, labels[keyLang])
        state.hcmLovColumns[keyLang].codaward = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codaward, labels[keyLang])
        state.hcmLovColumns[keyLang].codfrgtcard = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codfrgtcard, labels[keyLang])
        state.hcmLovColumns[keyLang].codtrn = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codtrn, labels[keyLang])
        state.hcmLovColumns[keyLang].typpayroll = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].typpayroll, labels[keyLang])
        state.hcmLovColumns[keyLang].codfrm = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codfrm, labels[keyLang])
        state.hcmLovColumns[keyLang].tempfile = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].tempfile, labels[keyLang])
        state.hcmLovColumns[keyLang].codchng = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codchng, labels[keyLang])
        state.hcmLovColumns[keyLang].numlereq = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].numlereq, labels[keyLang])
        state.hcmLovColumns[keyLang].codleave = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codleave, labels[keyLang])
        state.hcmLovColumns[keyLang].numlereqg = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].numlereqg, labels[keyLang])
        // state.hcmLovColumns[keyLang].codappr = assetsLabels.replaceLabelToColumns(state.hcmLovColumns[keyLang].codappr, labels[keyLang])
      }
    },
    [types.SET_HCM_LOV_LABELS] (state, labels) {
      state.hcmLovLabels = labels
    },
    [types.SET_HCM_LOV_SEARCH] (state, { inputSearch, lovType }) {
      let arrInputSearch = inputSearch.split(' - ')
      let condition = true
      let codeSearch = ''
      let nameSearch = ''
      let hcmLovSearch = state.hcmLov[lovType].rows.filter((list) => {
        if (arrInputSearch.length === 1) {
          codeSearch = arrInputSearch[0]
          nameSearch = arrInputSearch[0]
          if (list[lovType]) {
            condition = (list[lovType].toLowerCase().includes(codeSearch.toLowerCase()))
          }
          if (list['desc_' + lovType]) {
            condition = condition || list['desc_' + lovType].toLowerCase().includes(nameSearch.toLowerCase())
          }
        } else {
          codeSearch = arrInputSearch[0]
          arrInputSearch.splice(0, 1)
          nameSearch = arrInputSearch.join(' - ')
          if (list[lovType]) {
            condition = (list[lovType].toLowerCase().includes(codeSearch.toLowerCase()))
          }
          if (list['desc_' + lovType]) {
            condition = condition && list['desc_' + lovType].toLowerCase().includes(nameSearch.toLowerCase())
          }
        }
        return condition
      })
      state.hcmLovSearch = hcmLovSearch
    },
    [types.SET_HCM_LOV_COLUMNS_LABELS_CALLED] (state, columnsLabelsCalled) {
      state.columnsLabelsCalled = columnsLabelsCalled
    },
    [types.SET_HCM_LOV_CODEMPID] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codempid = lov.namempe
            break
          case 'th':
            hcmLov.rows[i].desc_codempid = lov.namempt
            break
          case '103':
            hcmLov.rows[i].desc_codempid = lov.namemp3
            break
          case '104':
            hcmLov.rows[i].desc_codempid = lov.namemp4
            break
          case '105':
            hcmLov.rows[i].desc_codempid = lov.namemp5
            break
        }
      }
      state.hcmLov.codempid = hcmLov
    },
    [types.SET_HCM_LOV_CODPOS] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codpos = lov.nampose
            break
          case 'th':
            hcmLov.rows[i].desc_codpos = lov.nampost
            break
          case '103':
            hcmLov.rows[i].desc_codpos = lov.nampos3
            break
          case '104':
            hcmLov.rows[i].desc_codpos = lov.nampos4
            break
          case '105':
            hcmLov.rows[i].desc_codpos = lov.nampos5
            break
        }
      }
      state.hcmLov.codpos = hcmLov
    },
    [types.SET_HCM_LOV_CODAPP] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codapp = lov.namappe
            break
          case 'th':
            hcmLov.rows[i].desc_codapp = lov.namappt
            break
          case '103':
            hcmLov.rows[i].desc_codapp = lov.namapp3
            break
          case '104':
            hcmLov.rows[i].desc_codapp = lov.namapp4
            break
          case '105':
            hcmLov.rows[i].desc_codapp = lov.namapp5
            break
        }
      }
      state.hcmLov.codapp = hcmLov
    },
    [types.SET_HCM_LOV_NUMMEMO] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].dtemonth = assetsLabels.month['en'][parseInt(lov.dtemonth) - 1].full
            hcmLov.rows[i].namcours = lov.namcourse
            break
          case 'th':
            hcmLov.rows[i].dtemonth = assetsLabels.month['th'][parseInt(lov.dtemonth) - 1].full
            hcmLov.rows[i].namcours = lov.namcourst
            break
          case '103':
            hcmLov.rows[i].dtemonth = assetsLabels.month['103'][parseInt(lov.dtemonth) - 1].full
            hcmLov.rows[i].namcours = lov.namcours3
            break
          case '104':
            hcmLov.rows[i].dtemonth = assetsLabels.month['104'][parseInt(lov.dtemonth) - 1].full
            hcmLov.rows[i].namcours = lov.namcours4
            break
          case '105':
            hcmLov.rows[i].dtemonth = assetsLabels.month['105'][parseInt(lov.dtemonth) - 1].full
            hcmLov.rows[i].namcours = lov.namcours5
            break
        }
      }
      state.hcmLov.nummemo = hcmLov
    },
    [types.SET_HCM_LOV_CODCOURS] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codcours = lov.namcourse
            break
          case 'th':
            hcmLov.rows[i].desc_codcours = lov.namcourst
            break
          case '103':
            hcmLov.rows[i].desc_codcours = lov.namcours3
            break
          case '104':
            hcmLov.rows[i].desc_codcours = lov.namcours4
            break
          case '105':
            hcmLov.rows[i].desc_codcours = lov.namcours5
            break
        }
      }
      state.hcmLov.codcours = hcmLov
    },
    [types.SET_HCM_LOV_CODFORM] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codform = lov.desforme
            break
          case 'th':
            hcmLov.rows[i].desc_codform = lov.desformt
            break
          case '103':
            hcmLov.rows[i].desc_codform = lov.desform3
            break
          case '104':
            hcmLov.rows[i].desc_codform = lov.desform4
            break
          case '105':
            hcmLov.rows[i].desc_codform = lov.desform5
            break
        }
      }
      state.hcmLov.codform = hcmLov
    },
    [types.SET_HCM_LOV_NUMAPPL] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_numappl = lov.desapple
            break
          case 'th':
            hcmLov.rows[i].desc_numappl = lov.desapplt
            break
          case '103':
            hcmLov.rows[i].desc_numappl = lov.desappl3
            break
          case '104':
            hcmLov.rows[i].desc_numappl = lov.desappl4
            break
          case '105':
            hcmLov.rows[i].desc_numappl = lov.desappl5
            break
        }
      }
      state.hcmLov.numappl = hcmLov
    },
    [types.SET_HCM_LOV_NUMREQ] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codpos = lov.nampose
            break
          case 'th':
            hcmLov.rows[i].desc_codpos = lov.nampost
            break
          case '103':
            hcmLov.rows[i].desc_codpos = lov.nampos3
            break
          case '104':
            hcmLov.rows[i].desc_codpos = lov.nampos4
            break
          case '105':
            hcmLov.rows[i].desc_codpos = lov.nampos5
            break
        }
        hcmLov.rows[i].desc_numreq = lov.numreq
      }
      state.hcmLov.numreq = hcmLov
    },
    [types.SET_HCM_LOV_APLV] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codaplvl = lov.descode
            break
          case 'th':
            hcmLov.rows[i].desc_codaplvl = lov.descodt
            break
          case '103':
            hcmLov.rows[i].desc_codaplvl = lov.descod3
            break
          case '104':
            hcmLov.rows[i].desc_codaplvl = lov.descod4
            break
          case '105':
            hcmLov.rows[i].desc_codaplvl = lov.descod5
            break
        }
      }
      state.hcmLov.codaplvl = hcmLov
    },
    [types.SET_HCM_LOV_GRDSCOR] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        hcmLov.rows[i].desc_grdscor = lov.qtyscor
      }
      state.hcmLov.grdscor = hcmLov
    },
    [types.SET_HCM_LOV_CODAPPR] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codempid = lov.namempe
            break
          case 'th':
            hcmLov.rows[i].desc_codempid = lov.namempt
            break
          case '103':
            hcmLov.rows[i].desc_codempid = lov.namemp3
            break
          case '104':
            hcmLov.rows[i].desc_codempid = lov.namemp4
            break
          case '105':
            hcmLov.rows[i].desc_codempid = lov.namemp5
            break
        }
      }
      state.hcmLov.codappr = hcmLov
    },
    [types.SET_HCM_LOV_CODCALEN] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codcalen = lov.descalene
            break
          case 'th':
            hcmLov.rows[i].desc_codcalen = lov.descalent
            break
          case '103':
            hcmLov.rows[i].desc_codcalen = lov.descalen3
            break
          case '104':
            hcmLov.rows[i].desc_codcalen = lov.descalen4
            break
          case '105':
            hcmLov.rows[i].desc_codcalen = lov.descalen5
            break
        }
      }
      state.hcmLov.codcalen = hcmLov
    },
    [types.SET_HCM_LOV_CODCOMPY] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codcompy = lov.descompye
            break
          case 'th':
            hcmLov.rows[i].desc_codcompy = lov.descompyt
            break
          case '103':
            hcmLov.rows[i].desc_codcompy = lov.descompy3
            break
          case '104':
            hcmLov.rows[i].desc_codcompy = lov.descompy4
            break
          case '105':
            hcmLov.rows[i].desc_codcompy = lov.descompy5
            break
        }
      }
      state.hcmLov.codcompy = hcmLov
    },
    [types.SET_HCM_LOV_CODSHIFT] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codshift = lov.desshifte
            break
          case 'th':
            hcmLov.rows[i].desc_codshift = lov.desshiftt
            break
          case '103':
            hcmLov.rows[i].desc_codshift = lov.desshift3
            break
          case '104':
            hcmLov.rows[i].desc_codshift = lov.desshift4
            break
          case '105':
            hcmLov.rows[i].desc_codshift = lov.desshift5
            break
        }
      }
      state.hcmLov.codshift = hcmLov
    },
    [types.SET_HCM_LOV_CODROOM] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codroom = lov.roomname
            break
          case 'th':
            hcmLov.rows[i].desc_codroom = lov.roomnamt
            break
          case '103':
            hcmLov.rows[i].desc_codroom = lov.roomnam3
            break
          case '104':
            hcmLov.rows[i].desc_codroom = lov.roomnam4
            break
          case '105':
            hcmLov.rows[i].desc_codroom = lov.roomnam5
            break
        }
      }
      state.hcmLov.codroom = hcmLov
    },
    [types.SET_HCM_LOV_CODASSET] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codasset = lov.desassee
            break
          case 'th':
            hcmLov.rows[i].desc_codasset = lov.desasset
            break
          case '103':
            hcmLov.rows[i].desc_codasset = lov.desasse3
            break
          case '104':
            hcmLov.rows[i].desc_codasset = lov.desasse4
            break
          case '105':
            hcmLov.rows[i].desc_codasset = lov.desasse5
            break
        }
      }
      state.hcmLov.codasset = hcmLov
    },
    [types.SET_HCM_LOV_CODUSER] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_coduser = lov.namempe
            break
          case 'th':
            hcmLov.rows[i].desc_coduser = lov.namempt
            break
          case '103':
            hcmLov.rows[i].desc_coduser = lov.namemp3
            break
          case '104':
            hcmLov.rows[i].desc_coduser = lov.namemp4
            break
          case '105':
            hcmLov.rows[i].desc_coduser = lov.namemp5
            break
        }
      }
      state.hcmLov.coduser = hcmLov
    },
    [types.SET_HCM_LOV_CODSECU] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codsecu = lov.descsecue
            break
          case 'th':
            hcmLov.rows[i].desc_codsecu = lov.descsecut
            break
          case '103':
            hcmLov.rows[i].desc_codsecu = lov.descsecu3
            break
          case '104':
            hcmLov.rows[i].desc_codsecu = lov.descsecu4
            break
          case '105':
            hcmLov.rows[i].desc_codsecu = lov.descsecu5
            break
        }
      }
      state.hcmLov.codsecu = hcmLov
    },
    [types.SET_HCM_LOV_CODPROC] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codproc = lov.descproce
            break
          case 'th':
            hcmLov.rows[i].desc_codproc = lov.descproct
            break
          case '103':
            hcmLov.rows[i].desc_codproc = lov.descproc3
            break
          case '104':
            hcmLov.rows[i].desc_codproc = lov.descproc4
            break
          case '105':
            hcmLov.rows[i].desc_codproc = lov.descproc5
            break
        }
      }
      state.hcmLov.codproc = hcmLov
    },
    [types.SET_HCM_LOV_CODINCOM] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codincom = lov.descodincome
            break
          case 'th':
            hcmLov.rows[i].desc_codincom = lov.descodincomt
            break
          case '103':
            hcmLov.rows[i].desc_codincom = lov.descodincom3
            break
          case '104':
            hcmLov.rows[i].desc_codincom = lov.descodincom4
            break
          case '105':
            hcmLov.rows[i].desc_codincom = lov.descodincom5
            break
        }
      }
      state.hcmLov.codincom = hcmLov
    },
    [types.SET_HCM_LOV_CODRETRO] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codretro = lov.descodretroe
            break
          case 'th':
            hcmLov.rows[i].desc_codretro = lov.descodretrot
            break
          case '103':
            hcmLov.rows[i].desc_codretro = lov.descodretro3
            break
          case '104':
            hcmLov.rows[i].desc_codretro = lov.descodretro4
            break
          case '105':
            hcmLov.rows[i].desc_codretro = lov.descodretro5
            break
        }
      }
      state.hcmLov.codretro = hcmLov
    },
    [types.SET_HCM_LOV_CODTABLE] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codtable = lov.descodtablee
            break
          case 'th':
            hcmLov.rows[i].desc_codtable = lov.descodtablet
            break
          case '103':
            hcmLov.rows[i].desc_codtable = lov.descodtable3
            break
          case '104':
            hcmLov.rows[i].desc_codtable = lov.descodtable4
            break
          case '105':
            hcmLov.rows[i].desc_codtable = lov.descodtable5
            break
        }
      }
      state.hcmLov.codtable = hcmLov
    },
    [types.SET_HCM_LOV_CODVAL] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codval = lov.descodvale
            break
          case 'th':
            hcmLov.rows[i].desc_codval = lov.descodvalt
            break
          case '103':
            hcmLov.rows[i].desc_codval = lov.descodval3
            break
          case '104':
            hcmLov.rows[i].desc_codval = lov.descodval4
            break
          case '105':
            hcmLov.rows[i].desc_codval = lov.descodval5
            break
        }
      }
      state.hcmLov.codval = hcmLov
    },
    [types.SET_HCM_LOV_COSTCENT] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_costcent = lov.namcente
            break
          case 'th':
            hcmLov.rows[i].desc_costcent = lov.namcentt
            break
          case '103':
            hcmLov.rows[i].desc_costcent = lov.namcent3
            break
          case '104':
            hcmLov.rows[i].desc_costcent = lov.namcent4
            break
          case '105':
            hcmLov.rows[i].desc_costcent = lov.namcent5
            break
        }
      }
      state.hcmLov.costcent = hcmLov
    },
    [types.SET_HCM_LOV_COMPGRP] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_compgrp = lov.descode
            break
          case 'th':
            hcmLov.rows[i].desc_compgrp = lov.descodt
            break
          case '103':
            hcmLov.rows[i].desc_compgrp = lov.descod3
            break
          case '104':
            hcmLov.rows[i].desc_compgrp = lov.descod4
            break
          case '105':
            hcmLov.rows[i].desc_compgrp = lov.descod5
            break
        }
      }
      state.hcmLov.compgrp = hcmLov
    },
    [types.SET_HCM_LOV_CODTENCY] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codtency = lov.descodtencye
            break
          case 'th':
            hcmLov.rows[i].desc_codtency = lov.descodtencyt
            break
          case '103':
            hcmLov.rows[i].desc_codtency = lov.descodtency3
            break
          case '104':
            hcmLov.rows[i].desc_codtency = lov.descodtency4
            break
          case '105':
            hcmLov.rows[i].desc_codtency = lov.descodtency5
            break
        }
      }
      state.hcmLov.codtency = hcmLov
    },
    [types.SET_HCM_LOV_CODOCCUP] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codoccup = lov.descodoccupe
            break
          case 'th':
            hcmLov.rows[i].desc_codoccup = lov.descodoccupt
            break
          case '103':
            hcmLov.rows[i].desc_codoccup = lov.descodoccup3
            break
          case '104':
            hcmLov.rows[i].desc_codoccup = lov.descodoccup4
            break
          case '105':
            hcmLov.rows[i].desc_codoccup = lov.descodoccup5
            break
        }
      }
      state.hcmLov.codoccup = hcmLov
    },
    [types.SET_HCM_LOV_CODHOSP] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codhosp = lov.descodhospe
            break
          case 'th':
            hcmLov.rows[i].desc_codhosp = lov.descodhospt
            break
          case '103':
            hcmLov.rows[i].desc_codhosp = lov.descodhosp3
            break
          case '104':
            hcmLov.rows[i].desc_codhosp = lov.descodhosp4
            break
          case '105':
            hcmLov.rows[i].desc_codhosp = lov.descodhosp5
            break
        }
      }
      state.hcmLov.codhosp = hcmLov
    },
    [types.SET_HCM_LOV_CODPROV] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codprov = lov.descodprove
            break
          case 'th':
            hcmLov.rows[i].desc_codprov = lov.descodprovt
            break
          case '103':
            hcmLov.rows[i].desc_codprov = lov.descodprov3
            break
          case '104':
            hcmLov.rows[i].desc_codprov = lov.descodprov4
            break
          case '105':
            hcmLov.rows[i].desc_codprov = lov.descodprov5
            break
        }
      }
      state.hcmLov.codprov = hcmLov
    },
    [types.SET_HCM_LOV_CODGRPWORK] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codgrpwork = lov.descodgrpworke
            break
          case 'th':
            hcmLov.rows[i].desc_codgrpwork = lov.descodgrpworkt
            break
          case '103':
            hcmLov.rows[i].desc_codgrpwork = lov.descodgrpwork3
            break
          case '104':
            hcmLov.rows[i].desc_codgrpwork = lov.descodgrpwork4
            break
          case '105':
            hcmLov.rows[i].desc_codgrpwork = lov.descodgrpwork5
            break
        }
      }
      state.hcmLov.codgrpwork = hcmLov
    },
    [types.SET_HCM_LOV_CODCARCAB] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codcarcab = lov.descodcarcabe
            break
          case 'th':
            hcmLov.rows[i].desc_codcarcab = lov.descodcarcabt
            break
          case '103':
            hcmLov.rows[i].desc_codcarcab = lov.descodcarcab3
            break
          case '104':
            hcmLov.rows[i].desc_codcarcab = lov.descodcarcab4
            break
          case '105':
            hcmLov.rows[i].desc_codcarcab = lov.descodcarcab5
            break
        }
      }
      state.hcmLov.codcarcab = hcmLov
    },
    [types.SET_HCM_LOV_CODTYPINC] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codtypinc = lov.descodtypince
            break
          case 'th':
            hcmLov.rows[i].desc_codtypinc = lov.descodtypinct
            break
          case '103':
            hcmLov.rows[i].desc_codtypinc = lov.descodtypinc3
            break
          case '104':
            hcmLov.rows[i].desc_codtypinc = lov.descodtypinc4
            break
          case '105':
            hcmLov.rows[i].desc_codtypinc = lov.descodtypinc5
            break
        }
      }
      state.hcmLov.codtypinc = hcmLov
    },
    [types.SET_HCM_LOV_CODTYPPAYR] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codtyppayr = lov.descodtyppayre
            break
          case 'th':
            hcmLov.rows[i].desc_codtyppayr = lov.descodtyppayrt
            break
          case '103':
            hcmLov.rows[i].desc_codtyppayr = lov.descodtyppayr3
            break
          case '104':
            hcmLov.rows[i].desc_codtyppayr = lov.descodtyppayr4
            break
          case '105':
            hcmLov.rows[i].desc_codtyppayr = lov.descodtyppayr5
            break
        }
      }
      state.hcmLov.codtyppayr = hcmLov
    },
    [types.SET_HCM_LOV_CODTYPPAYT] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codtyppayt = lov.descodtyppayte
            break
          case 'th':
            hcmLov.rows[i].desc_codtyppayt = lov.descodtyppaytt
            break
          case '103':
            hcmLov.rows[i].desc_codtyppayt = lov.descodtyppayt3
            break
          case '104':
            hcmLov.rows[i].desc_codtyppayt = lov.descodtyppayt4
            break
          case '105':
            hcmLov.rows[i].desc_codtyppayt = lov.descodtyppayt5
            break
        }
      }
      state.hcmLov.codtyppayt = hcmLov
    },
    [types.SET_HCM_LOV_CODTAX] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codtax = lov.descodtaxe
            break
          case 'th':
            hcmLov.rows[i].desc_codtax = lov.descodtaxt
            break
          case '103':
            hcmLov.rows[i].desc_codtax = lov.descodtax3
            break
          case '104':
            hcmLov.rows[i].desc_codtax = lov.descodtax4
            break
          case '105':
            hcmLov.rows[i].desc_codtax = lov.descodtax5
            break
        }
      }
      state.hcmLov.codtax = hcmLov
    },
    [types.SET_HCM_LOV_NUMOTREQ] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codempid = lov.namempe
            break
          case 'th':
            hcmLov.rows[i].desc_codempid = lov.namempt
            break
          case '103':
            hcmLov.rows[i].desc_codempid = lov.namemp3
            break
          case '104':
            hcmLov.rows[i].desc_codempid = lov.namemp4
            break
          case '105':
            hcmLov.rows[i].desc_codempid = lov.namemp5
            break
        }
      }
      state.hcmLov.numotreq = hcmLov
    },
    [types.SET_HCM_LOV_CODREM] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codrem = lov.descodreme
            break
          case 'th':
            hcmLov.rows[i].desc_codrem = lov.descodremt
            break
          case '103':
            hcmLov.rows[i].desc_codrem = lov.descodrem3
            break
          case '104':
            hcmLov.rows[i].desc_codrem = lov.descodrem4
            break
          case '105':
            hcmLov.rows[i].desc_codrem = lov.descodrem5
            break
        }
      }
      state.hcmLov.codrem = hcmLov
    },
    [types.SET_HCM_LOV_TYPELEAVE] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_typeleave = lov.destypeleavee
            break
          case 'th':
            hcmLov.rows[i].desc_typeleave = lov.destypeleavet
            break
          case '103':
            hcmLov.rows[i].desc_typeleave = lov.destypeleave3
            break
          case '104':
            hcmLov.rows[i].desc_typeleave = lov.destypeleave4
            break
          case '105':
            hcmLov.rows[i].desc_typeleave = lov.destypeleave5
            break
        }
      }
      state.hcmLov.typeleave = hcmLov
    },
    [types.SET_HCM_LOV_CODPAY] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codpay = lov.descodpaye
            break
          case 'th':
            hcmLov.rows[i].desc_codpay = lov.descodpayt
            break
          case '103':
            hcmLov.rows[i].desc_codpay = lov.descodpay3
            break
          case '104':
            hcmLov.rows[i].desc_codpay = lov.descodpay4
            break
          case '105':
            hcmLov.rows[i].desc_codpay = lov.descodpay5
            break
        }
      }
      state.hcmLov.codpay = hcmLov
    },
    [types.SET_HCM_LOV_CODPAYROL] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codpayrol = lov.descodpayrole
            break
          case 'th':
            hcmLov.rows[i].desc_codpayrol = lov.descodpayrolt
            break
          case '103':
            hcmLov.rows[i].desc_codpayrol = lov.descodpayrol3
            break
          case '104':
            hcmLov.rows[i].desc_codpayrol = lov.descodpayrol4
            break
          case '105':
            hcmLov.rows[i].desc_codpayrol = lov.descodpayrol5
            break
        }
      }
      state.hcmLov.codpayrol = hcmLov
    },
    [types.SET_HCM_LOV_CODFUND] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codfund = lov.descodfunde
            break
          case 'th':
            hcmLov.rows[i].desc_codfund = lov.descodfundt
            break
          case '103':
            hcmLov.rows[i].desc_codfund = lov.descodfund3
            break
          case '104':
            hcmLov.rows[i].desc_codfund = lov.descodfund4
            break
          case '105':
            hcmLov.rows[i].desc_codfund = lov.descodfund5
            break
        }
      }
      state.hcmLov.codfund = hcmLov
    },
    [types.SET_HCM_LOV_CODPLAN] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codplan = lov.descodplane
            break
          case 'th':
            hcmLov.rows[i].desc_codplan = lov.descodplant
            break
          case '103':
            hcmLov.rows[i].desc_codplan = lov.descodplan3
            break
          case '104':
            hcmLov.rows[i].desc_codplan = lov.descodplan4
            break
          case '105':
            hcmLov.rows[i].desc_codplan = lov.descodplan5
            break
        }
      }
      state.hcmLov.codplan = hcmLov
    },
    [types.SET_HCM_LOV_CODCAUSE] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codcause = lov.descodcausee
            break
          case 'th':
            hcmLov.rows[i].desc_codcause = lov.descodcauset
            break
          case '103':
            hcmLov.rows[i].desc_codcause = lov.descodcause3
            break
          case '104':
            hcmLov.rows[i].desc_codcause = lov.descodcause4
            break
          case '105':
            hcmLov.rows[i].desc_codcause = lov.descodcause5
            break
        }
      }
      state.hcmLov.codcause = hcmLov
    },
    [types.SET_HCM_LOV_TYPREWD] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_typrewd = lov.destyprewde
            break
          case 'th':
            hcmLov.rows[i].desc_typrewd = lov.destyprewdt
            break
          case '103':
            hcmLov.rows[i].desc_typrewd = lov.destyprewd3
            break
          case '104':
            hcmLov.rows[i].desc_typrewd = lov.destyprewd4
            break
          case '105':
            hcmLov.rows[i].desc_typrewd = lov.destyprewd5
            break
        }
      }
      state.hcmLov.typrewd = hcmLov
    },
    [types.SET_HCM_LOV_COUNTRY] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_country = lov.descountrye
            break
          case 'th':
            hcmLov.rows[i].desc_country = lov.descountryt
            break
          case '103':
            hcmLov.rows[i].desc_country = lov.descountry3
            break
          case '104':
            hcmLov.rows[i].desc_country = lov.descountry4
            break
          case '105':
            hcmLov.rows[i].desc_country = lov.descountry5
            break
        }
      }
      state.hcmLov.country = hcmLov
    },
    [types.SET_HCM_LOV_CODEDLV] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codedlv = lov.descodedlve
            break
          case 'th':
            hcmLov.rows[i].desc_codedlv = lov.descodedlvt
            break
          case '103':
            hcmLov.rows[i].desc_codedlv = lov.descodedlv3
            break
          case '104':
            hcmLov.rows[i].desc_codedlv = lov.descodedlv4
            break
          case '105':
            hcmLov.rows[i].desc_codedlv = lov.descodedlv5
            break
        }
      }
      state.hcmLov.codedlv = hcmLov
    },
    [types.SET_HCM_LOV_CODNATNL] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codnatnl = lov.descodnatnle
            break
          case 'th':
            hcmLov.rows[i].desc_codnatnl = lov.descodnatnlt
            break
          case '103':
            hcmLov.rows[i].desc_codnatnl = lov.descodnatnl3
            break
          case '104':
            hcmLov.rows[i].desc_codnatnl = lov.descodnatnl4
            break
          case '105':
            hcmLov.rows[i].desc_codnatnl = lov.descodnatnl5
            break
        }
      }
      state.hcmLov.codnatnl = hcmLov
    },
    [types.SET_HCM_LOV_CODRELGN] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codrelgn = lov.descodrelgne
            break
          case 'th':
            hcmLov.rows[i].desc_codrelgn = lov.descodrelgnt
            break
          case '103':
            hcmLov.rows[i].desc_codrelgn = lov.descodrelgn3
            break
          case '104':
            hcmLov.rows[i].desc_codrelgn = lov.descodrelgn4
            break
          case '105':
            hcmLov.rows[i].desc_codrelgn = lov.descodrelgn5
            break
        }
      }
      state.hcmLov.codrelgn = hcmLov
    },
    [types.SET_HCM_LOV_CODDIST] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_coddist = lov.descoddiste
            break
          case 'th':
            hcmLov.rows[i].desc_coddist = lov.descoddistt
            break
          case '103':
            hcmLov.rows[i].desc_coddist = lov.descoddist3
            break
          case '104':
            hcmLov.rows[i].desc_coddist = lov.descoddist4
            break
          case '105':
            hcmLov.rows[i].desc_coddist = lov.descoddist5
            break
        }
      }
      state.hcmLov.coddist = hcmLov
    },
    [types.SET_HCM_LOV_CODDGLV] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_coddglv = lov.descoddglve
            break
          case 'th':
            hcmLov.rows[i].desc_coddglv = lov.descoddglvt
            break
          case '103':
            hcmLov.rows[i].desc_coddglv = lov.descoddglv3
            break
          case '104':
            hcmLov.rows[i].desc_coddglv = lov.descoddglv4
            break
          case '105':
            hcmLov.rows[i].desc_coddglv = lov.descoddglv5
            break
        }
      }
      state.hcmLov.coddglv = hcmLov
    },
    [types.SET_HCM_LOV_CODMAJSB] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codmajsb = lov.descodmajsbe
            break
          case 'th':
            hcmLov.rows[i].desc_codmajsb = lov.descodmajsbt
            break
          case '103':
            hcmLov.rows[i].desc_codmajsb = lov.descodmajsb3
            break
          case '104':
            hcmLov.rows[i].desc_codmajsb = lov.descodmajsb4
            break
          case '105':
            hcmLov.rows[i].desc_codmajsb = lov.descodmajsb5
            break
        }
      }
      state.hcmLov.codmajsb = hcmLov
    },
    [types.SET_HCM_LOV_CODMINSB] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codminsb = lov.descodminsbe
            break
          case 'th':
            hcmLov.rows[i].desc_codminsb = lov.descodminsbt
            break
          case '103':
            hcmLov.rows[i].desc_codminsb = lov.descodminsb3
            break
          case '104':
            hcmLov.rows[i].desc_codminsb = lov.descodminsb4
            break
          case '105':
            hcmLov.rows[i].desc_codminsb = lov.descodminsb5
            break
        }
      }
      state.hcmLov.codminsb = hcmLov
    },
    [types.SET_HCM_LOV_CODINST] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codinst = lov.descodinste
            break
          case 'th':
            hcmLov.rows[i].desc_codinst = lov.descodinstt
            break
          case '103':
            hcmLov.rows[i].desc_codinst = lov.descodinst3
            break
          case '104':
            hcmLov.rows[i].desc_codinst = lov.descodinst4
            break
          case '105':
            hcmLov.rows[i].desc_codinst = lov.descodinst5
            break
        }
      }
      state.hcmLov.codinst = hcmLov
    },
    [types.SET_HCM_LOV_CODSUBDIST] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codsubdist = lov.descodsubdiste
            break
          case 'th':
            hcmLov.rows[i].desc_codsubdist = lov.descodsubdistt
            break
          case '103':
            hcmLov.rows[i].desc_codsubdist = lov.descodsubdist3
            break
          case '104':
            hcmLov.rows[i].desc_codsubdist = lov.descodsubdist4
            break
          case '105':
            hcmLov.rows[i].desc_codsubdist = lov.descodsubdist5
            break
        }
      }
      state.hcmLov.codsubdist = hcmLov
    },
    [types.SET_HCM_LOV_CODBRLC] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codbrlc = lov.descodbrlce
            break
          case 'th':
            hcmLov.rows[i].desc_codbrlc = lov.descodbrlct
            break
          case '103':
            hcmLov.rows[i].desc_codbrlc = lov.descodbrlc3
            break
          case '104':
            hcmLov.rows[i].desc_codbrlc = lov.descodbrlc4
            break
          case '105':
            hcmLov.rows[i].desc_codbrlc = lov.descodbrlc5
            break
        }
      }
      state.hcmLov.codbrlc = hcmLov
    },
    [types.SET_HCM_LOV_CODEMPMT] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codempmt = lov.descodempmte
            break
          case 'th':
            hcmLov.rows[i].desc_codempmt = lov.descodempmtt
            break
          case '103':
            hcmLov.rows[i].desc_codempmt = lov.descodempmt3
            break
          case '104':
            hcmLov.rows[i].desc_codempmt = lov.descodempmt4
            break
          case '105':
            hcmLov.rows[i].desc_codempmt = lov.descodempmt5
            break
        }
      }
      state.hcmLov.codempmt = hcmLov
    },
    [types.SET_HCM_LOV_CODTYPEMP] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codtypemp = lov.descodtypempe
            break
          case 'th':
            hcmLov.rows[i].desc_codtypemp = lov.descodtypempt
            break
          case '103':
            hcmLov.rows[i].desc_codtypemp = lov.descodtypemp3
            break
          case '104':
            hcmLov.rows[i].desc_codtypemp = lov.descodtypemp4
            break
          case '105':
            hcmLov.rows[i].desc_codtypemp = lov.descodtypemp5
            break
        }
      }
      state.hcmLov.codtypemp = hcmLov
    },
    [types.SET_HCM_LOV_CODJOB] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codjob = lov.descodjobe
            break
          case 'th':
            hcmLov.rows[i].desc_codjob = lov.descodjobt
            break
          case '103':
            hcmLov.rows[i].desc_codjob = lov.descodjob3
            break
          case '104':
            hcmLov.rows[i].desc_codjob = lov.descodjob4
            break
          case '105':
            hcmLov.rows[i].desc_codjob = lov.descodjob5
            break
        }
      }
      state.hcmLov.codjob = hcmLov
    },
    [types.SET_HCM_LOV_CODJOBGRAD] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codjobgrad = lov.descodjobgrade
            break
          case 'th':
            hcmLov.rows[i].desc_codjobgrad = lov.descodjobgradt
            break
          case '103':
            hcmLov.rows[i].desc_codjobgrad = lov.descodjobgrad3
            break
          case '104':
            hcmLov.rows[i].desc_codjobgrad = lov.descodjobgrad4
            break
          case '105':
            hcmLov.rows[i].desc_codjobgrad = lov.descodjobgrad5
            break
        }
      }
      state.hcmLov.codjobgrad = hcmLov
    },
    [types.SET_HCM_LOV_TYPDISP] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_typdisp = lov.destypdispe
            break
          case 'th':
            hcmLov.rows[i].desc_typdisp = lov.destypdispt
            break
          case '103':
            hcmLov.rows[i].desc_typdisp = lov.destypdisp3
            break
          case '104':
            hcmLov.rows[i].desc_typdisp = lov.destypdisp4
            break
          case '105':
            hcmLov.rows[i].desc_typdisp = lov.destypdisp5
            break
        }
      }
      state.hcmLov.typdisp = hcmLov
    },
    [types.SET_HCM_LOV_CODGL] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codgl = lov.descodgle
            break
          case 'th':
            hcmLov.rows[i].desc_codgl = lov.descodglt
            break
          case '103':
            hcmLov.rows[i].desc_codgl = lov.descodgl3
            break
          case '104':
            hcmLov.rows[i].desc_codgl = lov.descodgl4
            break
          case '105':
            hcmLov.rows[i].desc_codgl = lov.descodgl5
            break
        }
      }
      state.hcmLov.codgl = hcmLov
    },
    [types.SET_HCM_LOV_CODPOINT] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codpoint = lov.descodpointe
            break
          case 'th':
            hcmLov.rows[i].desc_codpoint = lov.descodpointt
            break
          case '103':
            hcmLov.rows[i].desc_codpoint = lov.descodpoint3
            break
          case '104':
            hcmLov.rows[i].desc_codpoint = lov.descodpoint4
            break
          case '105':
            hcmLov.rows[i].desc_codpoint = lov.descodpoint5
            break
        }
      }
      state.hcmLov.codpoint = hcmLov
    },
    [types.SET_HCM_LOV_CODBANK] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codbank = lov.descodbanke
            break
          case 'th':
            hcmLov.rows[i].desc_codbank = lov.descodbankt
            break
          case '103':
            hcmLov.rows[i].desc_codbank = lov.descodbank3
            break
          case '104':
            hcmLov.rows[i].desc_codbank = lov.descodbank4
            break
          case '105':
            hcmLov.rows[i].desc_codbank = lov.descodbank5
            break
        }
      }
      state.hcmLov.codbank = hcmLov
    },
    [types.SET_HCM_LOV_CODCURR] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codcurr = lov.descodcurre
            break
          case 'th':
            hcmLov.rows[i].desc_codcurr = lov.descodcurrt
            break
          case '103':
            hcmLov.rows[i].desc_codcurr = lov.descodcurr3
            break
          case '104':
            hcmLov.rows[i].desc_codcurr = lov.descodcurr4
            break
          case '105':
            hcmLov.rows[i].desc_codcurr = lov.descodcurr5
            break
        }
      }
      state.hcmLov.codcurr = hcmLov
    },
    [types.SET_HCM_LOV_CODTYPDOC] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codtypdoc = lov.descodtypdoce
            break
          case 'th':
            hcmLov.rows[i].desc_codtypdoc = lov.descodtypdoct
            break
          case '103':
            hcmLov.rows[i].desc_codtypdoc = lov.descodtypdoc3
            break
          case '104':
            hcmLov.rows[i].desc_codtypdoc = lov.descodtypdoc4
            break
          case '105':
            hcmLov.rows[i].desc_codtypdoc = lov.descodtypdoc5
            break
        }
      }
      state.hcmLov.codtypdoc = hcmLov
    },
    [types.SET_HCM_LOV_CODTYPCOLLA] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codtypcolla = lov.descodtypcollae
            break
          case 'th':
            hcmLov.rows[i].desc_codtypcolla = lov.descodtypcollat
            break
          case '103':
            hcmLov.rows[i].desc_codtypcolla = lov.descodtypcolla3
            break
          case '104':
            hcmLov.rows[i].desc_codtypcolla = lov.descodtypcolla4
            break
          case '105':
            hcmLov.rows[i].desc_codtypcolla = lov.descodtypcolla5
            break
        }
      }
      state.hcmLov.codtypcolla = hcmLov
    },
    [types.SET_HCM_LOV_CODREQST] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codreqst = lov.descodreqste
            break
          case 'th':
            hcmLov.rows[i].desc_codreqst = lov.descodreqstt
            break
          case '103':
            hcmLov.rows[i].desc_codreqst = lov.descodreqst3
            break
          case '104':
            hcmLov.rows[i].desc_codreqst = lov.descodreqst4
            break
          case '105':
            hcmLov.rows[i].desc_codreqst = lov.descodreqst5
            break
        }
      }
      state.hcmLov.codreqst = hcmLov
    },
    [types.SET_HCM_LOV_CODMENU] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codmenu = lov.descodmenue
            break
          case 'th':
            hcmLov.rows[i].desc_codmenu = lov.descodmenut
            break
          case '103':
            hcmLov.rows[i].desc_codmenu = lov.descodmenu3
            break
          case '104':
            hcmLov.rows[i].desc_codmenu = lov.descodmenu4
            break
          case '105':
            hcmLov.rows[i].desc_codmenu = lov.descodmenu5
            break
        }
      }
      state.hcmLov.codmenu = hcmLov
    },
    [types.SET_HCM_LOV_CODFUNCTION] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codfunction = lov.descodfunctione
            break
          case 'th':
            hcmLov.rows[i].desc_codfunction = lov.descodfunctiont
            break
          case '103':
            hcmLov.rows[i].desc_codfunction = lov.descodfunction3
            break
          case '104':
            hcmLov.rows[i].desc_codfunction = lov.descodfunction4
            break
          case '105':
            hcmLov.rows[i].desc_codfunction = lov.descodfunction5
            break
        }
      }
      state.hcmLov.codfunction = hcmLov
    },
    [types.SET_HCM_LOV_CODSKILL] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codskill = lov.descodskille
            break
          case 'th':
            hcmLov.rows[i].desc_codskill = lov.descodskillt
            break
          case '103':
            hcmLov.rows[i].desc_codskill = lov.descodskill3
            break
          case '104':
            hcmLov.rows[i].desc_codskill = lov.descodskill4
            break
          case '105':
            hcmLov.rows[i].desc_codskill = lov.descodskill5
            break
        }
      }
      state.hcmLov.codskill = hcmLov
    },
    [types.SET_HCM_LOV_CODAWARD] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codaward = lov.descodawarde
            break
          case 'th':
            hcmLov.rows[i].desc_codaward = lov.descodawardt
            break
          case '103':
            hcmLov.rows[i].desc_codaward = lov.descodaward3
            break
          case '104':
            hcmLov.rows[i].desc_codaward = lov.descodaward4
            break
          case '105':
            hcmLov.rows[i].desc_codaward = lov.descodaward5
            break
        }
      }
      state.hcmLov.codaward = hcmLov
    },
    [types.SET_HCM_LOV_CODFRGTCARD] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codfrgtcard = lov.descodfrgtcarde
            break
          case 'th':
            hcmLov.rows[i].desc_codfrgtcard = lov.descodfrgtcardt
            break
          case '103':
            hcmLov.rows[i].desc_codfrgtcard = lov.descodfrgtcard3
            break
          case '104':
            hcmLov.rows[i].desc_codfrgtcard = lov.descodfrgtcard4
            break
          case '105':
            hcmLov.rows[i].desc_codfrgtcard = lov.descodfrgtcard5
            break
        }
      }
      state.hcmLov.codfrgtcard = hcmLov
    },
    [types.SET_HCM_LOV_CODTRN] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codtrn = lov.descodtrne
            break
          case 'th':
            hcmLov.rows[i].desc_codtrn = lov.descodtrnt
            break
          case '103':
            hcmLov.rows[i].desc_codtrn = lov.descodtrn3
            break
          case '104':
            hcmLov.rows[i].desc_codtrn = lov.descodtrn4
            break
          case '105':
            hcmLov.rows[i].desc_codtrn = lov.descodtrn5
            break
        }
      }
      state.hcmLov.codtrn = hcmLov
    },
    [types.SET_HCM_LOV_TYPPAYROLL] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_typpayroll = lov.destyppayrolle
            break
          case 'th':
            hcmLov.rows[i].desc_typpayroll = lov.destyppayrollt
            break
          case '103':
            hcmLov.rows[i].desc_typpayroll = lov.destyppayroll3
            break
          case '104':
            hcmLov.rows[i].desc_typpayroll = lov.destyppayroll4
            break
          case '105':
            hcmLov.rows[i].desc_typpayroll = lov.destyppayroll5
            break
        }
      }
      state.hcmLov.typpayroll = hcmLov
    },
    [types.SET_HCM_LOV_CODFRM] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codfrm = lov.descodfrme
            break
          case 'th':
            hcmLov.rows[i].desc_codfrm = lov.descodfrmt
            break
          case '103':
            hcmLov.rows[i].desc_codfrm = lov.descodfrm3
            break
          case '104':
            hcmLov.rows[i].desc_codfrm = lov.descodfrm4
            break
          case '105':
            hcmLov.rows[i].desc_codfrm = lov.descodfrm5
            break
        }
      }
      state.hcmLov.codfrm = hcmLov
    },
    [types.SET_HCM_LOV_TEMPFILE] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_tempfile = lov.destempfilee
            break
          case 'th':
            hcmLov.rows[i].desc_tempfile = lov.destempfilet
            break
          case '103':
            hcmLov.rows[i].desc_tempfile = lov.destempfile3
            break
          case '104':
            hcmLov.rows[i].desc_tempfile = lov.destempfile4
            break
          case '105':
            hcmLov.rows[i].desc_tempfile = lov.destempfile5
            break
        }
      }
      state.hcmLov.tempfile = hcmLov
    },
    [types.SET_HCM_LOV_CODCHNG] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codchng = lov.descodchnge
            break
          case 'th':
            hcmLov.rows[i].desc_codchng = lov.descodchngt
            break
          case '103':
            hcmLov.rows[i].desc_codchng = lov.descodchng3
            break
          case '104':
            hcmLov.rows[i].desc_codchng = lov.descodchng4
            break
          case '105':
            hcmLov.rows[i].desc_codchng = lov.descodchng5
            break
        }
      }
      state.hcmLov.codchng = hcmLov
    },
    [types.SET_HCM_LOV_NUMLEREQ] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codempid = lov.desccodempide
            break
          case 'th':
            hcmLov.rows[i].desc_codempid = lov.desccodempidt
            break
          case '103':
            hcmLov.rows[i].desc_codempid = lov.desccodempid3
            break
          case '104':
            hcmLov.rows[i].desc_codempid = lov.desccodempid4
            break
          case '105':
            hcmLov.rows[i].desc_codempid = lov.desccodempid5
            break
        }
      }
      state.hcmLov.numlereq = hcmLov
    },
    [types.SET_HCM_LOV_CODLEAVE] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].desc_codleave = lov.desccodleavee
            break
          case 'th':
            hcmLov.rows[i].desc_codleave = lov.desccodleavet
            break
          case '103':
            hcmLov.rows[i].desc_codleave = lov.desccodleave3
            break
          case '104':
            hcmLov.rows[i].desc_codleave = lov.desccodleave4
            break
          case '105':
            hcmLov.rows[i].desc_codleave = lov.desccodleave5
            break
        }
      }
      state.hcmLov.codleave = hcmLov
    },
    [types.SET_HCM_LOV_NUMLEREQG] (state, hcmLov) {
      let nowLang = window.localStorage.getItem('lang')
      hcmLov.lang = nowLang
      for (var i = 0; i < hcmLov.rows.length; i++) {
        let lov = hcmLov.rows[i]
        switch (nowLang) {
          case 'en':
            hcmLov.rows[i].numcalen = lov.numcalene
            break
          case 'th':
            hcmLov.rows[i].numcalen = lov.numcalent
            break
          case '103':
            hcmLov.rows[i].numcalen = lov.numcalen3
            break
          case '104':
            hcmLov.rows[i].numcalen = lov.numcalen4
            break
          case '105':
            hcmLov.rows[i].numcalen = lov.numcalen5
            break
        }
      }
      state.hcmLov.numlereqg = hcmLov
    }
  },
  actions: {
    [types.RECEIVED_HCM_LOV_COLUMNS_LABELS] ({ state, commit }, lovType) {
      if (!state.columnsLabelsCalled) {
        commit(types.SET_HCM_LOV_COLUMNS_LABELS_CALLED, true)
        hcmLov.getLovLabels()
          .then((response) => {
            const data = JSON.parse(response.request.response)
            commit(types.SET_HCM_LOV_COLUMNS, data.labels)
            commit(types.SET_HCM_LOV_LABELS, data.labels)
          })
          .catch((error) => {
            let message = error
            if (error.response) {
              const data = error.response.data
              message = data.response
            }
            swal({title: '', text: message, html: true, type: 'error'})
          })
      }
    },
    [types.RECEIVED_HCM_LOV] ({ state, commit }, { lovType, params }) {
      switch (lovType.toUpperCase()) {
        case 'CODEMPID':
          if (!state.hcmLov.codempid.finish) {
            if (!state.hcmLov.codempid.loading) {
              commit(types.SET_HCM_LOV_CODEMPID, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodempid()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODEMPID, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODEMPID, { total: state.hcmLov.codempid.total, rows: state.hcmLov.codempid.rows, loading: false, finish: true })
          }
          break
        case 'CODPOS':
          if (!state.hcmLov.codpos.finish) {
            if (!state.hcmLov.codpos.loading) {
              commit(types.SET_HCM_LOV_CODPOS, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodpos()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODPOS, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODPOS, { total: state.hcmLov.codpos.total, rows: state.hcmLov.codpos.rows, loading: false, finish: true })
          }
          break
        case 'CODAPP':
          if (!state.hcmLov.codapp.finish) {
            if (!state.hcmLov.codapp.loading) {
              commit(types.SET_HCM_LOV_CODAPP, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodapp()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODAPP, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODAPP, { total: state.hcmLov.codapp.total, rows: state.hcmLov.codapp.rows, loading: false, finish: true })
          }
          break
        case 'NUMMEMO':
          if (!state.hcmLov.nummemo.finish) {
            if (!state.hcmLov.nummemo.loading) {
              commit(types.SET_HCM_LOV_NUMMEMO, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovNummemo()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_NUMMEMO, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_NUMMEMO, { total: state.hcmLov.nummemo.total, rows: state.hcmLov.nummemo.rows, loading: false, finish: true })
          }
          break
        case 'CODCOURS':
          if (!state.hcmLov.codcours.finish) {
            if (!state.hcmLov.codcours.loading) {
              commit(types.SET_HCM_LOV_CODCOURS, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodcours()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODCOURS, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODCOURS, { total: state.hcmLov.codcours.total, rows: state.hcmLov.codcours.rows, loading: false, finish: true })
          }
          break
        case 'CODFORM':
          if (!state.hcmLov.codform.finish) {
            if (!state.hcmLov.codform.loading) {
              commit(types.SET_HCM_LOV_CODFORM, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodform()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODFORM, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODFORM, { total: state.hcmLov.codform.total, rows: state.hcmLov.codform.rows, loading: false, finish: true })
          }
          break
        case 'NUMAPPL':
          if (!state.hcmLov.numappl.finish) {
            if (!state.hcmLov.numappl.loading) {
              commit(types.SET_HCM_LOV_NUMAPPL, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovNumappl()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_NUMAPPL, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_NUMAPPL, { total: state.hcmLov.numappl.total, rows: state.hcmLov.numappl.rows, loading: false, finish: true })
          }
          break
        case 'NUMREQ':
          if (!state.hcmLov.numreq.finish) {
            if (!state.hcmLov.numreq.loading) {
              commit(types.SET_HCM_LOV_NUMREQ, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovNumreq()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_NUMREQ, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_NUMREQ, { total: state.hcmLov.numreq.total, rows: state.hcmLov.numreq.rows, loading: false, finish: true })
          }
          break
        case 'CODAPLVL':
          if (!state.hcmLov.codaplvl.finish) {
            if (!state.hcmLov.codaplvl.loading) {
              commit(types.SET_HCM_LOV_APLV, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodaplvl()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_APLV, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_APLV, { total: state.hcmLov.codaplvl.total, rows: state.hcmLov.codaplvl.rows, loading: false, finish: true })
          }
          break
        case 'GRDSCOR':
          if (!state.hcmLov.grdscor.finish) {
            if (!state.hcmLov.grdscor.loading) {
              commit(types.SET_HCM_LOV_GRDSCOR, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovGrdscor(params)
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_GRDSCOR, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_GRDSCOR, { total: state.hcmLov.grdscor.total, rows: state.hcmLov.grdscor.rows, loading: false, finish: true })
          }
          break
        case 'CODAPPR':
          if (!state.hcmLov.codappr.finish) {
            if (!state.hcmLov.codappr.loading) {
              commit(types.SET_HCM_LOV_CODAPPR, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodappr()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODAPPR, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODAPPR, { total: state.hcmLov.codappr.total, rows: state.hcmLov.codappr.rows, loading: false, finish: true })
          }
          break
        case 'CODCALEN':
          if (!state.hcmLov.codcalen.finish) {
            if (!state.hcmLov.codcalen.loading) {
              commit(types.SET_HCM_LOV_CODCALEN, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodcalen()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODCALEN, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODCALEN, { total: state.hcmLov.codcalen.total, rows: state.hcmLov.codcalen.rows, loading: false, finish: true })
          }
          break
        case 'CODSHIFT':
          if (!state.hcmLov.codshift.finish) {
            if (!state.hcmLov.codshift.loading) {
              commit(types.SET_HCM_LOV_CODSHIFT, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodshift()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODSHIFT, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODSHIFT, { total: state.hcmLov.codshift.total, rows: state.hcmLov.codshift.rows, loading: false, finish: true })
          }
          break
        case 'CODROOM':
          if (!state.hcmLov.codroom.finish) {
            if (!state.hcmLov.codroom.loading) {
              commit(types.SET_HCM_LOV_CODROOM, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodroom()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODROOM, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODROOM, { total: state.hcmLov.codroom.total, rows: state.hcmLov.codroom.rows, loading: false, finish: true })
          }
          break
        case 'CODASSET':
          if (!state.hcmLov.codasset.finish) {
            if (!state.hcmLov.codasset.loading) {
              commit(types.SET_HCM_LOV_CODASSET, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodasset()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODASSET, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODASSET, { total: state.hcmLov.codasset.total, rows: state.hcmLov.codasset.rows, loading: false, finish: true })
          }
          break
        case 'CODCOMPY':
          if (!state.hcmLov.codcompy.finish) {
            if (!state.hcmLov.codcompy.loading) {
              commit(types.SET_HCM_LOV_CODCOMPY, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodcompy()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODCOMPY, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODCOMPY, { total: state.hcmLov.codcompy.total, rows: state.hcmLov.codcompy.rows, loading: false, finish: true })
          }
          break
        case 'CODUSER':
          if (!state.hcmLov.coduser.finish) {
            if (!state.hcmLov.coduser.loading) {
              commit(types.SET_HCM_LOV_CODUSER, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCoduser()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODUSER, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODUSER, { total: state.hcmLov.coduser.total, rows: state.hcmLov.coduser.rows, loading: false, finish: true })
          }
          break
        case 'CODSECU':
          if (!state.hcmLov.codsecu.finish) {
            if (!state.hcmLov.codsecu.loading) {
              commit(types.SET_HCM_LOV_CODSECU, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodsecu()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODSECU, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODSECU, { total: state.hcmLov.codsecu.total, rows: state.hcmLov.codsecu.rows, loading: false, finish: true })
          }
          break
        case 'CODPROC':
          if (!state.hcmLov.codproc.finish) {
            if (!state.hcmLov.codproc.loading) {
              commit(types.SET_HCM_LOV_CODPROC, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodproc()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODPROC, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODPROC, { total: state.hcmLov.codproc.total, rows: state.hcmLov.codproc.rows, loading: false, finish: true })
          }
          break
        case 'CODINCOM':
          if (!state.hcmLov.codincom.finish) {
            if (!state.hcmLov.codincom.loading) {
              commit(types.SET_HCM_LOV_CODINCOM, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodincom()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODINCOM, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODINCOM, { total: state.hcmLov.codincom.total, rows: state.hcmLov.codincom.rows, loading: false, finish: true })
          }
          break
        case 'CODRETRO':
          if (!state.hcmLov.codretro.finish) {
            if (!state.hcmLov.codretro.loading) {
              commit(types.SET_HCM_LOV_CODRETRO, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodretro()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODRETRO, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODRETRO, { total: state.hcmLov.codretro.total, rows: state.hcmLov.codretro.rows, loading: false, finish: true })
          }
          break
        case 'CODTABLE':
          if (!state.hcmLov.codtable.finish) {
            if (!state.hcmLov.codtable.loading) {
              commit(types.SET_HCM_LOV_CODTABLE, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodtable()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODTABLE, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODTABLE, { total: state.hcmLov.codtable.total, rows: state.hcmLov.codtable.rows, loading: false, finish: true })
          }
          break
        case 'CODVAL':
          if (!state.hcmLov.codval.finish) {
            if (!state.hcmLov.codval.loading) {
              commit(types.SET_HCM_LOV_CODVAL, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodval()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODVAL, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODVAL, { total: state.hcmLov.codval.total, rows: state.hcmLov.codval.rows, loading: false, finish: true })
          }
          break
        case 'COSTCENT':
          if (!state.hcmLov.costcent.finish) {
            if (!state.hcmLov.costcent.loading) {
              commit(types.SET_HCM_LOV_COSTCENT, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCostCenter()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_COSTCENT, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_COSTCENT, { total: state.hcmLov.costcent.total, rows: state.hcmLov.costcent.rows, loading: false, finish: true })
          }
          break
        case 'COMPGRP':
          if (!state.hcmLov.compgrp.finish) {
            if (!state.hcmLov.compgrp.loading) {
              commit(types.SET_HCM_LOV_COMPGRP, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovComGroup()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_COMPGRP, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_COMPGRP, { total: state.hcmLov.compgrp.total, rows: state.hcmLov.compgrp.rows, loading: false, finish: true })
          }
          break
        case 'CODTENCY':
          if (!state.hcmLov.codtency.finish) {
            if (!state.hcmLov.codtency.loading) {
              commit(types.SET_HCM_LOV_CODTENCY, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodtency()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODTENCY, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODTENCY, { total: state.hcmLov.codtency.total, rows: state.hcmLov.codtency.rows, loading: false, finish: true })
          }
          break
        case 'CODOCCUP':
          if (!state.hcmLov.codoccup.finish) {
            if (!state.hcmLov.codoccup.loading) {
              commit(types.SET_HCM_LOV_CODOCCUP, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodoccup()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODOCCUP, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODOCCUP, { total: state.hcmLov.codoccup.total, rows: state.hcmLov.codoccup.rows, loading: false, finish: true })
          }
          break
        case 'CODHOSP':
          if (!state.hcmLov.codhosp.finish) {
            if (!state.hcmLov.codhosp.loading) {
              commit(types.SET_HCM_LOV_CODHOSP, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodhosp()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODHOSP, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODHOSP, { total: state.hcmLov.codhosp.total, rows: state.hcmLov.codhosp.rows, loading: false, finish: true })
          }
          break
        case 'CODPROV':
          if (!state.hcmLov.codprov.finish) {
            if (!state.hcmLov.codprov.loading) {
              commit(types.SET_HCM_LOV_CODPROV, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodprov()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODPROV, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODPROV, { total: state.hcmLov.codprov.total, rows: state.hcmLov.codprov.rows, loading: false, finish: true })
          }
          break
        case 'CODGRPWORK':
          if (!state.hcmLov.codgrpwork.finish) {
            if (!state.hcmLov.codgrpwork.loading) {
              commit(types.SET_HCM_LOV_CODGRPWORK, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodgrpwork()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODGRPWORK, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODGRPWORK, { total: state.hcmLov.codgrpwork.total, rows: state.hcmLov.codgrpwork.rows, loading: false, finish: true })
          }
          break
        case 'CODCARCAB':
          if (!state.hcmLov.codcarcab.finish) {
            if (!state.hcmLov.codcarcab.loading) {
              commit(types.SET_HCM_LOV_CODCARCAB, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodcarcab()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODCARCAB, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODCARCAB, { total: state.hcmLov.codcarcab.total, rows: state.hcmLov.codcarcab.rows, loading: false, finish: true })
          }
          break
        case 'CODTYPINC':
          if (!state.hcmLov.codtypinc.finish) {
            if (!state.hcmLov.codtypinc.loading) {
              commit(types.SET_HCM_LOV_CODTYPINC, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodtypinc()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODTYPINC, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODTYPINC, { total: state.hcmLov.codtypinc.total, rows: state.hcmLov.codtypinc.rows, loading: false, finish: true })
          }
          break
        case 'CODTYPPAYR':
          if (!state.hcmLov.codtyppayr.finish) {
            if (!state.hcmLov.codtyppayr.loading) {
              commit(types.SET_HCM_LOV_CODTYPPAYR, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodtyppayr()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODTYPPAYR, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODTYPPAYR, { total: state.hcmLov.codtyppayr.total, rows: state.hcmLov.codtyppayr.rows, loading: false, finish: true })
          }
          break
        case 'NUMOTREQ':
          if (!state.hcmLov.numotreq.finish) {
            if (!state.hcmLov.numotreq.loading) {
              commit(types.SET_HCM_LOV_NUMOTREQ, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovNumotreq()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_NUMOTREQ, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_NUMOTREQ, { total: state.hcmLov.numotreq.total, rows: state.hcmLov.numotreq.rows, loading: false, finish: true })
          }
          break
        case 'CODREM':
          if (!state.hcmLov.codrem.finish) {
            if (!state.hcmLov.codrem.loading) {
              commit(types.SET_HCM_LOV_CODREM, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodrem()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODREM, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODREM, { total: state.hcmLov.codrem.total, rows: state.hcmLov.codrem.rows, loading: false, finish: true })
          }
          break
        case 'CODTYPPAYT':
          if (!state.hcmLov.codtyppayt.finish) {
            if (!state.hcmLov.codtyppayt.loading) {
              commit(types.SET_HCM_LOV_CODTYPPAYT, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodtyppayt()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODTYPPAYT, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODTYPPAYT, { total: state.hcmLov.codtyppayt.total, rows: state.hcmLov.codtyppayt.rows, loading: false, finish: true })
          }
          break
        case 'CODTAX':
          if (!state.hcmLov.codtax.finish) {
            if (!state.hcmLov.codtax.loading) {
              commit(types.SET_HCM_LOV_CODTAX, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodtax()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODTAX, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODTAX, { total: state.hcmLov.codtax.total, rows: state.hcmLov.codtax.rows, loading: false, finish: true })
          }
          break
        case 'TYPELEAVE':
          if (!state.hcmLov.typeleave.finish) {
            if (!state.hcmLov.typeleave.loading) {
              commit(types.SET_HCM_LOV_TYPELEAVE, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovTypeleave()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_TYPELEAVE, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_TYPELEAVE, { total: state.hcmLov.typeleave.total, rows: state.hcmLov.typeleave.rows, loading: false, finish: true })
          }
          break
        case 'CODPAY':
          if (!state.hcmLov.codpay.finish) {
            if (!state.hcmLov.codpay.loading) {
              commit(types.SET_HCM_LOV_CODPAY, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodpay()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODPAY, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODPAY, { total: state.hcmLov.codpay.total, rows: state.hcmLov.codpay.rows, loading: false, finish: true })
          }
          break
        case 'CODPAYROL':
          if (!state.hcmLov.codpayrol.finish) {
            if (!state.hcmLov.codpayrol.loading) {
              commit(types.SET_HCM_LOV_CODPAYROL, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodpayrol()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODPAYROL, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODPAYROL, { total: state.hcmLov.codpayrol.total, rows: state.hcmLov.codpayrol.rows, loading: false, finish: true })
          }
          break
        case 'CODFUND':
          if (!state.hcmLov.codfund.finish) {
            if (!state.hcmLov.codfund.loading) {
              commit(types.SET_HCM_LOV_CODFUND, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodfund()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODFUND, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODFUND, { total: state.hcmLov.codfund.total, rows: state.hcmLov.codfund.rows, loading: false, finish: true })
          }
          break
        case 'CODPLAN':
          if (!state.hcmLov.codplan.finish) {
            if (!state.hcmLov.codplan.loading) {
              commit(types.SET_HCM_LOV_CODPLAN, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodplan()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODPLAN, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODPLAN, { total: state.hcmLov.codplan.total, rows: state.hcmLov.codplan.rows, loading: false, finish: true })
          }
          break
        case 'CODCAUSE':
          if (!state.hcmLov.codcause.finish) {
            if (!state.hcmLov.codcause.loading) {
              commit(types.SET_HCM_LOV_CODCAUSE, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodcause()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODCAUSE, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODCAUSE, { total: state.hcmLov.codcause.total, rows: state.hcmLov.codcause.rows, loading: false, finish: true })
          }
          break
        case 'TYPREWD':
          if (!state.hcmLov.typrewd.finish) {
            if (!state.hcmLov.typrewd.loading) {
              commit(types.SET_HCM_LOV_TYPREWD, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovTyprewd()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_TYPREWD, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_TYPREWD, { total: state.hcmLov.typrewd.total, rows: state.hcmLov.typrewd.rows, loading: false, finish: true })
          }
          break
        case 'COUNTRY':
          if (!state.hcmLov.country.finish) {
            if (!state.hcmLov.country.loading) {
              commit(types.SET_HCM_LOV_COUNTRY, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCountry()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_COUNTRY, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_COUNTRY, { total: state.hcmLov.country.total, rows: state.hcmLov.country.rows, loading: false, finish: true })
          }
          break
        case 'CODEDLV':
          if (!state.hcmLov.codedlv.finish) {
            if (!state.hcmLov.codedlv.loading) {
              commit(types.SET_HCM_LOV_CODEDLV, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodedlv()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODEDLV, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODEDLV, { total: state.hcmLov.codedlv.total, rows: state.hcmLov.codedlv.rows, loading: false, finish: true })
          }
          break
        case 'CODNATNL':
          if (!state.hcmLov.codnatnl.finish) {
            if (!state.hcmLov.codnatnl.loading) {
              commit(types.SET_HCM_LOV_CODNATNL, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodnatnl()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODNATNL, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODNATNL, { total: state.hcmLov.codnatnl.total, rows: state.hcmLov.codnatnl.rows, loading: false, finish: true })
          }
          break
        case 'CODRELGN':
          if (!state.hcmLov.codrelgn.finish) {
            if (!state.hcmLov.codrelgn.loading) {
              commit(types.SET_HCM_LOV_CODRELGN, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodrelgn()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODRELGN, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODRELGN, { total: state.hcmLov.codrelgn.total, rows: state.hcmLov.codrelgn.rows, loading: false, finish: true })
          }
          break
        case 'CODDIST':
          if (!state.hcmLov.coddist.finish) {
            if (!state.hcmLov.coddist.loading) {
              commit(types.SET_HCM_LOV_CODDIST, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCoddist()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODDIST, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODDIST, { total: state.hcmLov.coddist.total, rows: state.hcmLov.coddist.rows, loading: false, finish: true })
          }
          break
        case 'CODDGLV':
          if (!state.hcmLov.coddglv.finish) {
            if (!state.hcmLov.coddglv.loading) {
              commit(types.SET_HCM_LOV_CODDGLV, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCoddglv()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODDGLV, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODDGLV, { total: state.hcmLov.coddglv.total, rows: state.hcmLov.coddglv.rows, loading: false, finish: true })
          }
          break
        case 'CODMAJSB':
          if (!state.hcmLov.codmajsb.finish) {
            if (!state.hcmLov.codmajsb.loading) {
              commit(types.SET_HCM_LOV_CODMAJSB, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodmajsb()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODMAJSB, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODMAJSB, { total: state.hcmLov.codmajsb.total, rows: state.hcmLov.codmajsb.rows, loading: false, finish: true })
          }
          break
        case 'CODMINSB':
          if (!state.hcmLov.codminsb.finish) {
            if (!state.hcmLov.codminsb.loading) {
              commit(types.SET_HCM_LOV_CODMINSB, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodminsb()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODMINSB, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODMINSB, { total: state.hcmLov.codminsb.total, rows: state.hcmLov.codminsb.rows, loading: false, finish: true })
          }
          break
        case 'CODINST':
          if (!state.hcmLov.codinst.finish) {
            if (!state.hcmLov.codinst.loading) {
              commit(types.SET_HCM_LOV_CODINST, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodinst()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODINST, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODINST, { total: state.hcmLov.codinst.total, rows: state.hcmLov.codinst.rows, loading: false, finish: true })
          }
          break
        case 'CODSUBDIST':
          if (!state.hcmLov.codsubdist.finish) {
            if (!state.hcmLov.codsubdist.loading) {
              commit(types.SET_HCM_LOV_CODSUBDIST, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodsubdist()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODSUBDIST, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODSUBDIST, { total: state.hcmLov.codsubdist.total, rows: state.hcmLov.codsubdist.rows, loading: false, finish: true })
          }
          break
        case 'CODBRLC':
          if (!state.hcmLov.codbrlc.finish) {
            if (!state.hcmLov.codbrlc.loading) {
              commit(types.SET_HCM_LOV_CODBRLC, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodbrlc()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODBRLC, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODBRLC, { total: state.hcmLov.codbrlc.total, rows: state.hcmLov.codbrlc.rows, loading: false, finish: true })
          }
          break
        case 'CODEMPMT':
          if (!state.hcmLov.codempmt.finish) {
            if (!state.hcmLov.codempmt.loading) {
              commit(types.SET_HCM_LOV_CODEMPMT, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodempmt()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODEMPMT, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODEMPMT, { total: state.hcmLov.codempmt.total, rows: state.hcmLov.codempmt.rows, loading: false, finish: true })
          }
          break
        case 'CODTYPEMP':
          if (!state.hcmLov.codtypemp.finish) {
            if (!state.hcmLov.codtypemp.loading) {
              commit(types.SET_HCM_LOV_CODTYPEMP, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodtypemp()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODTYPEMP, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODTYPEMP, { total: state.hcmLov.codtypemp.total, rows: state.hcmLov.codtypemp.rows, loading: false, finish: true })
          }
          break
        case 'CODJOB':
          if (!state.hcmLov.codjob.finish) {
            if (!state.hcmLov.codjob.loading) {
              commit(types.SET_HCM_LOV_CODJOB, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodjob()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODJOB, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODJOB, { total: state.hcmLov.codjob.total, rows: state.hcmLov.codjob.rows, loading: false, finish: true })
          }
          break
        case 'CODJOBGRAD':
          if (!state.hcmLov.codjobgrad.finish) {
            if (!state.hcmLov.codjobgrad.loading) {
              commit(types.SET_HCM_LOV_CODJOBGRAD, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodjobgrad()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODJOBGRAD, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODJOBGRAD, { total: state.hcmLov.codjobgrad.total, rows: state.hcmLov.codjobgrad.rows, loading: false, finish: true })
          }
          break
        case 'TYPDISP':
          if (!state.hcmLov.typdisp.finish) {
            if (!state.hcmLov.typdisp.loading) {
              commit(types.SET_HCM_LOV_TYPDISP, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovTypdisp()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_TYPDISP, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_TYPDISP, { total: state.hcmLov.typdisp.total, rows: state.hcmLov.typdisp.rows, loading: false, finish: true })
          }
          break
        case 'CODGL':
          if (!state.hcmLov.codgl.finish) {
            if (!state.hcmLov.codgl.loading) {
              commit(types.SET_HCM_LOV_CODGL, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodgl()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODGL, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODGL, { total: state.hcmLov.codgl.total, rows: state.hcmLov.codgl.rows, loading: false, finish: true })
          }
          break
        case 'CODPOINT':
          if (!state.hcmLov.codpoint.finish) {
            if (!state.hcmLov.codpoint.loading) {
              commit(types.SET_HCM_LOV_CODPOINT, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodpoint()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODPOINT, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODPOINT, { total: state.hcmLov.codpoint.total, rows: state.hcmLov.codpoint.rows, loading: false, finish: true })
          }
          break
        case 'CODBANK':
          if (!state.hcmLov.codbank.finish) {
            if (!state.hcmLov.codbank.loading) {
              commit(types.SET_HCM_LOV_CODBANK, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodbank()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODBANK, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODBANK, { total: state.hcmLov.codbank.total, rows: state.hcmLov.codbank.rows, loading: false, finish: true })
          }
          break
        case 'CODCURR':
          if (!state.hcmLov.codcurr.finish) {
            if (!state.hcmLov.codcurr.loading) {
              commit(types.SET_HCM_LOV_CODCURR, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodcurr()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODCURR, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODCURR, { total: state.hcmLov.codcurr.total, rows: state.hcmLov.codcurr.rows, loading: false, finish: true })
          }
          break
        case 'CODTYPDOC':
          if (!state.hcmLov.codtypdoc.finish) {
            if (!state.hcmLov.codtypdoc.loading) {
              commit(types.SET_HCM_LOV_CODTYPDOC, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodtypdoc()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODTYPDOC, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODTYPDOC, { total: state.hcmLov.codtypdoc.total, rows: state.hcmLov.codtypdoc.rows, loading: false, finish: true })
          }
          break
        case 'CODTYPCOLLA':
          if (!state.hcmLov.codtypcolla.finish) {
            if (!state.hcmLov.codtypcolla.loading) {
              commit(types.SET_HCM_LOV_CODTYPCOLLA, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodtypcolla()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODTYPCOLLA, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODTYPCOLLA, { total: state.hcmLov.codtypcolla.total, rows: state.hcmLov.codtypcolla.rows, loading: false, finish: true })
          }
          break
        case 'CODREQST':
          if (!state.hcmLov.codreqst.finish) {
            if (!state.hcmLov.codreqst.loading) {
              commit(types.SET_HCM_LOV_CODREQST, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodreqst()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODREQST, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODREQST, { total: state.hcmLov.codreqst.total, rows: state.hcmLov.codreqst.rows, loading: false, finish: true })
          }
          break
        case 'CODMENU':
          if (!state.hcmLov.codmenu.finish) {
            if (!state.hcmLov.codmenu.loading) {
              commit(types.SET_HCM_LOV_CODMENU, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodmenu()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODMENU, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODMENU, { total: state.hcmLov.codmenu.total, rows: state.hcmLov.codmenu.rows, loading: false, finish: true })
          }
          break
        case 'CODFUNCTION':
          if (!state.hcmLov.codfunction.finish) {
            if (!state.hcmLov.codfunction.loading) {
              commit(types.SET_HCM_LOV_CODFUNCTION, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodfunction()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODFUNCTION, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODFUNCTION, { total: state.hcmLov.codfunction.total, rows: state.hcmLov.codfunction.rows, loading: false, finish: true })
          }
          break
        case 'CODSKILL':
          if (!state.hcmLov.codskill.finish) {
            if (!state.hcmLov.codskill.loading) {
              commit(types.SET_HCM_LOV_CODSKILL, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodskill()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODSKILL, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODSKILL, { total: state.hcmLov.codskill.total, rows: state.hcmLov.codskill.rows, loading: false, finish: true })
          }
          break
        case 'CODAWARD':
          if (!state.hcmLov.codaward.finish) {
            if (!state.hcmLov.codaward.loading) {
              commit(types.SET_HCM_LOV_CODAWARD, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodaward()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODAWARD, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODAWARD, { total: state.hcmLov.codaward.total, rows: state.hcmLov.codaward.rows, loading: false, finish: true })
          }
          break
        case 'CODFRGTCARD':
          if (!state.hcmLov.codfrgtcard.finish) {
            if (!state.hcmLov.codfrgtcard.loading) {
              commit(types.SET_HCM_LOV_CODFRGTCARD, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodfrgtcard()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODFRGTCARD, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODFRGTCARD, { total: state.hcmLov.codfrgtcard.total, rows: state.hcmLov.codfrgtcard.rows, loading: false, finish: true })
          }
          break
        case 'TYPPAYROLL':
          if (!state.hcmLov.typpayroll.finish) {
            if (!state.hcmLov.typpayroll.loading) {
              commit(types.SET_HCM_LOV_TYPPAYROLL, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovTyppayroll()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_TYPPAYROLL, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_TYPPAYROLL, { total: state.hcmLov.typpayroll.total, rows: state.hcmLov.typpayroll.rows, loading: false, finish: true })
          }
          break
        case 'CODTRN':
          if (!state.hcmLov.codtrn.finish) {
            if (!state.hcmLov.codtrn.loading) {
              commit(types.SET_HCM_LOV_CODTRN, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodtrn()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODTRN, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODTRN, { total: state.hcmLov.codtrn.total, rows: state.hcmLov.codtrn.rows, loading: false, finish: true })
          }
          break
        case 'CODFRM':
          if (!state.hcmLov.codfrm.finish) {
            if (!state.hcmLov.codfrm.loading) {
              commit(types.SET_HCM_LOV_CODFRM, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodfrm()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODFRM, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODFRM, { total: state.hcmLov.codfrm.total, rows: state.hcmLov.codfrm.rows, loading: false, finish: true })
          }
          break
        case 'TEMPFILE':
          if (!state.hcmLov.tempfile.finish) {
            if (!state.hcmLov.tempfile.loading) {
              commit(types.SET_HCM_LOV_TEMPFILE, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovTempfile()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_TEMPFILE, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_TEMPFILE, { total: state.hcmLov.tempfile.total, rows: state.hcmLov.tempfile.rows, loading: false, finish: true })
          }
          break
        case 'CODCHNG':
          if (!state.hcmLov.codchng.finish) {
            if (!state.hcmLov.codchng.loading) {
              commit(types.SET_HCM_LOV_CODCHNG, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodchng()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODCHNG, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODCHNG, { total: state.hcmLov.codchng.total, rows: state.hcmLov.codchng.rows, loading: false, finish: true })
          }
          break
        case 'NUMLEREQ':
          if (!state.hcmLov.numlereq.finish) {
            if (!state.hcmLov.numlereq.loading) {
              commit(types.SET_HCM_LOV_NUMLEREQ, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovNumlereq()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_NUMLEREQ, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_NUMLEREQ, { total: state.hcmLov.numlereq.total, rows: state.hcmLov.numlereq.rows, loading: false, finish: true })
          }
          break
        case 'CODLEAVE':
          if (!state.hcmLov.codleave.finish) {
            if (!state.hcmLov.codleave.loading) {
              commit(types.SET_HCM_LOV_CODLEAVE, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovCodleave()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_CODLEAVE, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_CODLEAVE, { total: state.hcmLov.codleave.total, rows: state.hcmLov.codleave.rows, loading: false, finish: true })
          }
          break
        case 'NUMLEREQG':
          if (!state.hcmLov.numlereqg.finish) {
            if (!state.hcmLov.numlereqg.loading) {
              commit(types.SET_HCM_LOV_NUMLEREQG, { total: 0, rows: [], loading: true, finish: false })
              hcmLov.getLovNumlereqg()
                .then((response) => {
                  const data = JSON.parse(response.request.response)
                  commit(types.SET_HCM_LOV_NUMLEREQG, { total: data.total, rows: data.rows, loading: false, finish: true })
                })
                .catch((error) => {
                  let message = error
                  if (error.response) {
                    const data = error.response.data
                    message = data.response
                  }
                  swal({title: '', text: message, html: true, type: 'error'})
                })
            }
          } else if (state.hcmLov.lang !== window.localStorage.getItem('lang')) {
            commit(types.SET_HCM_LOV_NUMLEREQG, { total: state.hcmLov.numlereqg.total, rows: state.hcmLov.numlereqg.rows, loading: false, finish: true })
          }
          break
      }
    }
  }
}
