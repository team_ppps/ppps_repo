import * as types from './mutation-types'
import hrmsChangeLabel from '../api'
import swal from 'sweetalert'
import hrmsChangeLabelLabels from '../assets/labels'
import { constLabels } from 'assets/js/constLabels'

export default {
  state: {
    data: {},
    labels: hrmsChangeLabelLabels
  },
  getters: {
    [types.GET_HRMS_CHANGE_LABEL_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRMS_CHANGE_LABEL_DATA] (state) {
      return state.data
    }
  },
  mutations: {
    [types.SET_HRMS_CHANGE_LABEL_LABELS] (state, labels) {
      state.labels = labels
    },
    [types.SET_HRMS_CHANGE_LABEL_DATA] (state, data) {
      state.data = data
    }
  },
  actions: {
    [types.RECEIVED_HRMS_CHANGE_LABEL_LABELS] ({ commit }) {
      hrmsChangeLabel.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRMS_CHANGE_LABEL_LABELS, data.labels)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRMS_CHANGE_LABEL_DATA] ({ commit }, inputValue) {
      hrmsChangeLabel.getData(inputValue)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRMS_CHANGE_LABEL_DATA, data)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.SAVE_HRMS_CHANGE_LABEL_DATA] ({ commit }, data) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrmsChangeLabel.saveData(data)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((response) => {
              const data = response.response.data
              swal({title: '', text: data.response, html: true, type: 'error'})
            })
        }
      })
    }
  }
}
