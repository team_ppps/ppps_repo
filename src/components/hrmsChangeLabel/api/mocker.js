import hrmsChangeLabels from '../assets/labels'
import assetsLabels from 'assets/js/labels'

export const mocker = {
  labels () {
    return {
      labels: hrmsChangeLabels
    }
  },
  data (inputValue) {
    let mockData = {
      ti_descode: inputValue,
      ti_descodt: inputValue,
      ti_descod3: inputValue,
      ti_descod4: inputValue,
      ti_descod5: inputValue
    }
    return mockData
  },
  save () {
    return assetsLabels.mockPostMessage
  }
}
