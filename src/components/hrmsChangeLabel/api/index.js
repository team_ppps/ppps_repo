import { instance, isMock, endpoint, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLabels () {
    if (isMock) return generator(mocker.labels())
    return instance().get(endpoint + '/hrmsChangeLabel/getLabels')
  },
  getData (inputValue) {
    if (isMock) return generator(mocker.data(inputValue))
    return instance().get(endpoint + '/hrmsChangeLabel/getData')
  },
  saveData (data) {
    let sendParams = {
      param_json: JSON.stringify({
        data: data
      })
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrmsChangeLabel/dataSave', sendParams)
  }
}
