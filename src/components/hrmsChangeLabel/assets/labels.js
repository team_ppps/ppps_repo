module.exports = {
  'en': {
    'CHANGE_LABEL': {
      '0': 'English',
      '1': 'Thai',
      '2': 'Chinese',
      '3': 'German',
      '4': 'Japanese',
      '5': 'Data Change Language'
    }
  },
  'th': {
    'CHANGE_LABEL': {
      '0': 'อังกฤษ',
      '1': 'ไทย',
      '2': 'จีน',
      '3': 'เยอรมัน',
      '4': 'ญี่ปุ่น',
      '5': 'ข้อมูลการเปลี่ยนภาษา'
    }
  }
}
