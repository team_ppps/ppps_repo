module.exports = {
  'en': {
    'HRAL4KE': {
      '10': 'Recording Format',
      '11': 'By date',
      '12': 'By Emp.Code'
    }
  },
  'th': {
    'HRAL4KE': {
      '10': 'รูปแบบการบันทึก',
      '11': 'ตามวันที่',
      '12': 'ตามรหัสพนักงาน'
    }
  }
}
