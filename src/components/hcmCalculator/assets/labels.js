module.exports = {
  en: {
    'HCMCAL': {
      '10': 'Income Items',
      '20': 'Income Code',
      '30': 'Detail'
    }
  },
  th: {
    'HCMCAL': {
      '10': 'รายการรหัสเงินได้/ส่วนหัก',
      '20': 'รหัสเงินได้/ส่วนหัก',
      '30': 'รายละเอียด'
    }
  }
}
