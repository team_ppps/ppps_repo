export let hcmLovCodcompColumns = () => {
  let historyColumns = [
    {
      key: 'descCodcomp',
      name: '',
      labelCodapp: 'HCM_COMP',
      labelIndex: '30',
      style: 'min-width: 150px;'
    }
  ]

  return {
    en: {
      history: historyColumns
    },
    th: {
      history: historyColumns
    }
  }
}
