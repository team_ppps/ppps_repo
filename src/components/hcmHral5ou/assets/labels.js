module.exports = {
  'en': {
    'HCM_COMP': {
      '10': 'List Of Company Name',
      '20': 'List Of Company Name',
      '30': 'History Of Company Name',
      '40': 'Search Department Name, Department Code, Department Level',
      '50': 'Clear History'
    }
  },
  'th': {
    'HCM_COMP': {
      '10': 'รายชื่อบริษัท',
      '20': 'รายชื่อบริษัท',
      '30': 'ประวัติรายชื่อบริษัท',
      '40': 'ค้นหาชื่อหน่วยงาน, รหัสหน่วยงาน, ระดับหน่วยงาน',
      '50': 'ล้างประวัติ'
    }
  }
}
