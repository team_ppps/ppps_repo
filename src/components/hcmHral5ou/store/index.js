import * as types from './mutation-types'
import hcmLovCodcomp from '../api'
import hcmLovCodcompLabels from '../assets/labels'
import { hcmLovCodcompColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'

let getDepartments = (inputSearch, departments) => {
  let recursiveDepartments = []
  recursiveDepartmentsFunction(inputSearch, departments, recursiveDepartments)
  let resultDepartments = []
  for (let index = 0; index < recursiveDepartments.length; index++) {
    if (recursiveDepartments[index]) {
      if (recursiveDepartments[index].details) {
        if (recursiveDepartments[index].details.length > 0) {
          resultDepartments.push(recursiveDepartments[index])
        }
      }
    }
  }
  return resultDepartments
}

let recursiveDepartmentsFunction = (inputSearch, departments, recursiveDepartments, index = 0) => {
  for (let department of departments) {
    let arrInputSearch = inputSearch.split(' - ')
    let condition = true
    let codeSearch = ''
    let nameSearch = ''
    if (arrInputSearch.length === 1) {
      codeSearch = arrInputSearch[0]
      nameSearch = arrInputSearch[0]
      condition = (department.code.toLowerCase().includes(codeSearch.toLowerCase()) || department.name.toLowerCase().includes(nameSearch.toLowerCase()))
    } else {
      codeSearch = arrInputSearch[0]
      arrInputSearch.splice(0, 1)
      nameSearch = arrInputSearch.join(' - ')
      condition = (department.code.toLowerCase().includes(codeSearch.toLowerCase()) && department.name.toLowerCase().includes(nameSearch.toLowerCase()))
    }
    if (condition) {
      if (!recursiveDepartments[index]) {
        recursiveDepartments[index] = {}
      }
      recursiveDepartments[index].title = department.title
      recursiveDepartments[index].level = department.level
      if (!recursiveDepartments[index].details) {
        recursiveDepartments[index].details = []
      }
      recursiveDepartments[index].details.push({
        code: department.code,
        name: department.name,
        codcomp: department.codcomp
      })
    }
    if (department.children) {
      recursiveDepartmentsFunction(inputSearch, department.children, recursiveDepartments, department.level)
    }
  }
}

let getDepartmentSubDetailLeftParent = (baseDepartments, department, detail) => {
  let resultDepartment = {}
  recursiveDepartmentsSubDetailLeftParentFunction(baseDepartments, department, detail, {}, resultDepartment)
  let departmentSubDetailLeftParent = { details: [] }
  if (resultDepartment.parent) {
    departmentSubDetailLeftParent.title = resultDepartment.parent.title
    departmentSubDetailLeftParent.level = resultDepartment.parent.level
    departmentSubDetailLeftParent.details.push({
      code: resultDepartment.parent.code,
      name: resultDepartment.parent.name,
      codcomp: resultDepartment.parent.codcomp
    })
  }
  return departmentSubDetailLeftParent
}

let recursiveDepartmentsSubDetailLeftParentFunction = (baseDepartments, department, detail, departmentParent, resultDepartment) => {
  for (let baseDepartment of baseDepartments) {
    if (baseDepartment.title === department.title &&
      baseDepartment.level === department.level &&
      baseDepartment.code === detail.code &&
      baseDepartment.name === detail.name &&
      baseDepartment.codcomp === detail.codcomp) {
      // pass by reference object
      resultDepartment.parent = departmentParent
      break
    } else {
      if (baseDepartment.children) {
        recursiveDepartmentsSubDetailLeftParentFunction(baseDepartment.children, department, detail, baseDepartment, resultDepartment)
      }
    }
  }
}

let getDepartmentSubDetailRight = (baseDepartments, department, detail) => {
  let resultDepartment = {}
  recursiveDepartmentsSubDetailRightFunction(baseDepartments, department, detail, resultDepartment)
  let departmentSubDetailRight = { details: [] }
  if (resultDepartment.children) {
    let children = resultDepartment.children
    for (let child of children) {
      departmentSubDetailRight.title = child.title
      departmentSubDetailRight.level = child.level
      departmentSubDetailRight.details.push({
        code: child.code,
        name: child.name,
        codcomp: child.codcomp
      })
    }
  }
  return departmentSubDetailRight
}

let recursiveDepartmentsSubDetailRightFunction = (baseDepartments, department, detail, resultDepartment) => {
  for (let baseDepartment of baseDepartments) {
    if (baseDepartment.title === department.title &&
      baseDepartment.level === department.level &&
      baseDepartment.code === detail.code &&
      baseDepartment.name === detail.name &&
      baseDepartment.codcomp === detail.codcomp) {
      // pass by reference object
      resultDepartment.children = baseDepartment.children
      break
    } else {
      if (baseDepartment.children) {
        recursiveDepartmentsSubDetailRightFunction(baseDepartment.children, department, detail, resultDepartment)
      }
    }
  }
}

export default {
  state: {
    hcmLovCodcompColumns: hcmLovCodcompColumns(),
    hcmLovCodcompLabels: hcmLovCodcompLabels,
    hcmLovCodcompHistory: {
      total: 0,
      rows: []
    },
    hcmLovCodcompBaseDepartments: [],
    hcmLovCodcompDepartments: [],
    hcmLovCodcompDepartmentDisp: {
      history: true,
      main: false,
      sub: false,
      selected: false
    },
    hcmLovCodcompDepartmentSelected: {
      details: []
    },
    hcmLovCodcompDepartmentSubDetailLeftParent: {
      details: []
    },
    hcmLovCodcompDepartmentSubDetailLeft: {
      details: []
    },
    hcmLovCodcompDepartmentSubDetailRight: {
      details: []
    }
  },
  getters: {
    [types.GET_HCM_LOV_CODCOMP_COLUMNS] (state) {
      return state.hcmLovCodcompColumns
    },
    [types.GET_HCM_LOV_CODCOMP_LABELS] (state) {
      return state.hcmLovCodcompLabels
    },
    [types.GET_HCM_LOV_CODCOMP_HISTORY] (state) {
      return state.hcmLovCodcompHistory
    },
    [types.GET_HCM_LOV_CODCOMP_DEPARTMENTS] (state) {
      return state.hcmLovCodcompDepartments
    },
    [types.GET_HCM_LOV_CODCOMP_BASE_DEPARTMENTS] (state) {
      return state.hcmLovCodcompBaseDepartments
    },
    [types.GET_HCM_LOV_CODCOMP_DEPARTMENT_DISP] (state) {
      return state.hcmLovCodcompDepartmentDisp
    },
    [types.GET_HCM_LOV_CODCOMP_DEPARTMENT_SELECTED] (state) {
      return state.hcmLovCodcompDepartmentSelected
    },
    [types.GET_HCM_LOV_CODCOMP_DEPARTMENT_SUB_DETAIL_LEFT_PARENT] (state) {
      return state.hcmLovCodcompDepartmentSubDetailLeftParent
    },
    [types.GET_HCM_LOV_CODCOMP_DEPARTMENT_SUB_DETAIL_LEFT] (state) {
      return state.hcmLovCodcompDepartmentSubDetailLeft
    },
    [types.GET_HCM_LOV_CODCOMP_DEPARTMENT_SUB_DETAIL_RIGHT] (state) {
      return state.hcmLovCodcompDepartmentSubDetailRight
    }
  },
  mutations: {
    [types.SET_HCM_LOV_CODCOMP_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.hcmLovCodcompColumns[keyLang].history = assetsLabels.replaceLabelToColumns(state.hcmLovCodcompColumns[keyLang].history, labels[keyLang])
      }
    },
    [types.SET_HCM_LOV_CODCOMP_LABELS] (state, labels) {
      state.hcmLovCodcompLabels = labels
    },
    [types.SET_HCM_LOV_CODCOMP_HISTORY] (state, history) {
      state.hcmLovCodcompHistory = history
    },
    [types.SET_HCM_LOV_CODCOMP_BASE_DEPARTMENTS] (state, data) {
      state.hcmLovCodcompBaseDepartments = data
    },
    [types.SET_HCM_LOV_CODCOMP_DEPARTMENTS] (state, inputSearch) {
      let resultDepartments = getDepartments(inputSearch, state.hcmLovCodcompBaseDepartments)
      state.hcmLovCodcompDepartments = resultDepartments
    },
    [types.SET_HCM_LOV_CODCOMP_DEPARTMENT_DISP] (state, departmentPage) {
      state.hcmLovCodcompDepartmentDisp.history = (departmentPage === 'history')
      state.hcmLovCodcompDepartmentDisp.main = (departmentPage === 'main')
      state.hcmLovCodcompDepartmentDisp.sub = (departmentPage === 'sub')
      state.hcmLovCodcompDepartmentDisp.selected = (departmentPage === 'selected')
    },
    [types.SET_HCM_LOV_CODCOMP_DEPARTMENT_SELECTED] (state, departmentSelected) {
      state.hcmLovCodcompDepartmentSelected = departmentSelected
    },
    [types.SET_HCM_LOV_CODCOMP_DEPARTMENT_SUB_DETAIL_LEFT_PARENT] (state, departmentSubDetailLeftParent) {
      state.hcmLovCodcompDepartmentSubDetailLeftParent = departmentSubDetailLeftParent
    },
    [types.SET_HCM_LOV_CODCOMP_DEPARTMENT_SUB_DETAIL_LEFT] (state, departmentSubDetailLeft) {
      state.hcmLovCodcompDepartmentSubDetailLeft = departmentSubDetailLeft
    },
    [types.SET_HCM_LOV_CODCOMP_DEPARTMENT_SUB_DETAIL_RIGHT] (state, departmentSubDetailRight) {
      state.hcmLovCodcompDepartmentSubDetailRight = departmentSubDetailRight
    }
  },
  actions: {
    [types.RECEIVED_HCM_LOV_CODCOMP_COLUMNS_LABELS] ({ commit }, { simplert }) {
      hcmLovCodcomp.getLovCodcompLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HCM_LOV_CODCOMP_COLUMNS, data.labels)
          commit(types.SET_HCM_LOV_CODCOMP_LABELS, data.labels)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          simplert.openSimplert({
            title: 'Error',
            message: message
          })
        })
    },
    [types.RECEIVED_HCM_LOV_CODCOMP_HISTORY] ({ commit }) {
      if (JSON.parse(window.localStorage.getItem('codcompHistory')) === null) {
        window.localStorage.setItem('codcompHistory', JSON.stringify([]))
      }
      let historyLists = JSON.parse(window.localStorage.getItem('codcompHistory'))
      let data = {
        total: historyLists.length,
        rows: historyLists
      }
      commit(types.SET_HCM_LOV_CODCOMP_HISTORY, data)
    },
    [types.UPDATE_HCM_LOV_CODCOMP_HISTORY] ({ commit }, detail) {
      let historyLists = JSON.parse(window.localStorage.getItem('codcompHistory'))
      let duplicateHistory = historyLists.filter((history) => {
        return history.descCodcomp === detail.code + ' - ' + detail.name
      })
      if (duplicateHistory.length === 0) {
        historyLists.splice(0, 0, {
          descCodcomp: detail.code + ' - ' + detail.name
        })
      }
      if (historyLists.length > 5) {
        historyLists.splice(-1, 1)
      }
      window.localStorage.setItem('codcompHistory', JSON.stringify(historyLists))
      let data = {
        total: historyLists.length,
        rows: historyLists
      }
      commit(types.SET_HCM_LOV_CODCOMP_HISTORY, data)
    },
    [types.RECEIVED_HCM_LOV_CODCOMP_BASE_DEPARTMENTS] ({ commit }, { simplert }) {
      hcmLovCodcomp.getBaseDepartments()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HCM_LOV_CODCOMP_BASE_DEPARTMENTS, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          simplert.openSimplert({
            title: 'Error',
            message: message
          })
        })
    },
    [types.RECEIVED_HCM_LOV_CODCOMP_DEPARTMENT_SUB_DETAIL] ({ state, commit }, { department, detail }) {
      let departmentSubDetailLeftParent = getDepartmentSubDetailLeftParent(state.hcmLovCodcompBaseDepartments, department, detail)
      commit(types.SET_HCM_LOV_CODCOMP_DEPARTMENT_SUB_DETAIL_LEFT_PARENT, departmentSubDetailLeftParent)

      let departmentSubDetailLeft = {
        title: department.title,
        level: department.level,
        details: [ detail ]
      }
      commit(types.SET_HCM_LOV_CODCOMP_DEPARTMENT_SUB_DETAIL_LEFT, departmentSubDetailLeft)

      let departmentSubDetailRight = getDepartmentSubDetailRight(state.hcmLovCodcompBaseDepartments, department, detail)
      commit(types.SET_HCM_LOV_CODCOMP_DEPARTMENT_SUB_DETAIL_RIGHT, departmentSubDetailRight)
    }
  }
}
