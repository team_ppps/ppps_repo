import { instance, isMock, endpoint, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLovCodcompLabels () {
    if (isMock) return generator(mocker.lovCodcompLabels())
    return instance().get(endpoint + '/hcmLovCodcomp/getLabels')
  },
  getBaseDepartments () {
    if (isMock) return generator(mocker.baseDepartments())
    return instance().get(endpoint + '/hcmLovCodcomp/getBaseDepartments')
  }
}
