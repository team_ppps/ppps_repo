/*
 * COLUMN METHODS
 * - enableHeaderColumn
 * - headerColumnsFn
 * - bodyColumnsFn
 * - resetOthersColumnSortKey
 * COLUMN SETTING METHODS
 * - toggleColumnSettingDisp
 * - filterSettingColumns
 * DATAROWS METHODS
 * - initialLocalKeyOnDataTableRows
 * - sortData
 * - displayRowsFilter
 * PAGE METHODS
 * - filterPage
 * - setPageClicked
 * - setCurrentPage
 * SELECT, USERSTATUS, STAAPPR AND DELETE BUTTON METHODS
 * - countSelect
 * - countUserStatus
 * - countStaappr
 * - selectAllSelectInPage
 * - selectAllUserStatusInPage
 * - selectAllStatusInPage
 * - selectAllDeleteInPage
 * - toggleDelete
 * - findDataSelected
 * TICK, ADD, EDIT and CANCEL METHODS
 * - eventTrigger
 * INLINE METHODS
 * - addInlineEventBuiltIn
 * - editInlineEventBuiltIn
 * - deleteInlineEventBuiltIn
 * - saveInlineEventBuiltIn
 * - detailEventBuiltIn
 * - selectEventBuiltIn
 * - findInlineDataHasFlg
 * - onSelectChangeEditInlineAndColumnEvent
 * - clearInlineDataRows
 * VALIDATE METHODS
 * - findValidateRules
 * ROW CONDITION METHODS
 * - rowDisableConditionBuiltIn
 * - rowDisableBaseCondition
 * SUMMARY COLUMN METHODS
 * - summaryDisp
 * - summaryColumn
 * RESIZE METHODS
 * - handleResize
*/

import _ from 'lodash'
import moment from 'moment'
import swal from 'sweetalert'
import { constLabels } from 'assets/js/constLabels'

module.exports = {
  // Columns Filter Header and Body Methods
  enableHeaderColumn (column) {
    return column.enabled || typeof column.enabled === 'undefined'
  },
  headerColumnsFn (headerRow) {
    return this.localColumns.filter((column) => {
      column.headerRow = (typeof column.headerRow === 'undefined') ? 1 : parseInt(column.headerRow)
      column.colspan = (typeof column.colspan === 'undefined') ? 1 : parseInt(column.colspan)
      column.rowspan = (typeof column.rowspan === 'undefined') ? this.maxRowspan : parseInt(column.rowspan)
      return column.headerRow === headerRow
    })
  },
  bodyColumnsFn () {
    let columnHasNotOneSpan = this.localColumns.filter((column) => {
      column.colspan = (typeof column.colspan === 'undefined') ? 1 : parseInt(column.colspan)
      return column.colspan > 1 || column.headerRow > 1 || column.rowspan > 1
    })

    return this.localColumns.filter((column) => {
      column.colspan = (typeof column.colspan === 'undefined') ? 1 : parseInt(column.colspan)
      if (columnHasNotOneSpan.length > 0) {
        return column.colspan === 1 && (column.headerRow !== 1 || column.rowspan !== 1)
      } else {
        return column.colspan === 1
      }
    })
  },
  resetOthersColumnSortKey (sortKey) {
    this.sortKey = sortKey
    for (let row = 1; row <= this.filteredBodyColumns.length; row++) {
      if (this.filteredBodyColumns[row - 1].key !== this.sortKey) {
        this.$set(this.filteredBodyColumns[row - 1], 'sortNum', 0)
      }
    }
  },
  // Columns Setting Methods
  toggleColumnSettingDisp () {
    this.columnSettingDisp = !this.columnSettingDisp
  },
  // DataRows Methods
  initialLocalKeyOnDataTableRows () {
    let row = 0
    for (let dataRow of this.dataTableRows) {
      row++
      this.$set(dataRow, 'rowID', row)
      if (this.addInlineEventBool) {
        this.$set(dataRow, 'flgAdd', dataRow.flgAdd || false)
      }
      if (this.editInlineEventBool) {
        this.$set(dataRow, 'flgEdit', dataRow.flgEdit || false)
      }
      if (this.deleteInlineEventBool) {
        this.$set(dataRow, 'flgDelete', dataRow.flgDelete || false)
        this.$set(dataRow, 'flgDeleteHide', dataRow.flgDeleteHide || false)
      }
      if (typeof this.approveEvent !== 'undefined') {
        this.$set(dataRow, 'staappr', (typeof dataRow.staappr === 'undefined') ? '' : dataRow.staappr)
        this.$set(dataRow, 'flgStaappr', '')
      }
      if (typeof this.selectEvent !== 'undefined') {
        this.$set(dataRow, 'select', (typeof dataRow.select === 'undefined') ? '' : dataRow.select)
        this.$set(dataRow, 'flgSelect', '')
      }
      if (typeof this.userStatusEvent !== 'undefined') {
        this.$set(dataRow, 'userStatus', (typeof dataRow.userStatus === 'undefined') ? '' : dataRow.userStatus)
        this.$set(dataRow, 'flgact', '')
      }
    }
  },
  sortData (dataRows) {
    for (let column of this.filteredBodyColumns) {
      if (column.key === this.sortKey) {
        if (column.sortNum === 0) {
          return dataRows
        } else {
          let dataRowsSorted = dataRows.sort(function (dataRow1, dataRow2) {
            let dateRow1 = moment(dataRow1[column.key], 'DD-MM-YYYY HH:mm')
            let dateRow2 = moment(dataRow2[column.key], 'DD-MM-YYYY HH:mm')
            if (moment(dateRow1).isValid() && moment(dateRow2).isValid()) {
              return new Date(dateRow1) - new Date(dateRow2)
            } else {
              /*   return dataRow1[column.key] - dataRow2[column.key] //for number case */
              if (dataRow2[column.key] > dataRow1[column.key]) {
                return -1
              }
              if (dataRow2[column.key] < dataRow1[column.key]) {
                return 1
              }
              return 0
            }
          })

          if (column.sortNum === 1) {
            return dataRowsSorted
          } else if (column.sortNum === 2) {
            return dataRowsSorted.reverse()
          }
        }
      }
    }
    return dataRows
  },
  displayRowsFilter () {
    let boolPass = true
    let newDisplayRows = []
    for (var displayRow of this.displayRows) {
      if (displayRow < this.dataTableTotal) {
        newDisplayRows.push(displayRow)
      } else {
        if (boolPass) {
          newDisplayRows.push(displayRow)
          boolPass = false
        }
      }
    }
    return newDisplayRows
  },
  // Page Methods
  filterPage (page) {
    let pageIndex = Math.ceil(this.currentPage / this.showPage)
    return (page >= (pageIndex - 1) * this.showPage + 1 && page <= pageIndex * this.showPage)
  },
  setPageClicked (page) {
    this.$validator.validateAll().then(result => {
      if (result) {
        if (this.addInlineEventBool || this.editInlineEventBool || this.deleteInlineEventBool) {
          this.searchBool = false
          this.pageBool = false
        }
        this.setCurrentPage(page)
      }
    })
  },
  setCurrentPage (page) {
    this.staapprBtn = 0 // reset Approve button
    this.currentPage = (page <= 0) ? 1 : (page > this.totalPage) ? this.totalPage : page
  },
  // Select UserStatus and Staappr Button Methods
  countSelect () {
    this.selectBtn++
    if (this.selectBtn > 1) {
      this.selectBtn = 0
    }
    switch (this.selectBtn) {
      case 0:
        this.selectDesc = 'None'
        this.selectAllSelectInPage('N')
        break
      case 1:
        this.selectDesc = 'Chekced'
        this.selectAllSelectInPage('Y')
        break
    }
  },
  countUserStatus () {
    this.userStatusBtn++
    if (this.userStatusBtn > 2) {
      this.userStatusBtn = 1
    }
    switch (this.userStatusBtn) {
      case 0:
        this.userStatusDesc = 'None'
        this.selectAllUserStatusInPage('')
        break
      case 1:
        this.userStatusDesc = 'ปัจจุบัน'
        this.selectAllUserStatusInPage('1')
        break
      case 2:
        this.userStatusDesc = 'ระงับใช้'
        this.selectAllUserStatusInPage('3')
        break
      default:
        this.userStatusDesc = 'None'
        this.selectAllUserStatusInPage('')
        break
    }
  },
  countStaappr () {
    this.staapprBtn++
    if (this.staapprBtn > 2) {
      this.staapprBtn = 0
    }
    switch (this.staapprBtn) {
      case 0:
        this.statusBtn = 'None'
        this.selectAllStatusInPage('')
        break
      case 1:
        this.statusBtn = 'Approve'
        this.selectAllStatusInPage('A')
        break
      case 2:
        this.statusBtn = 'Reject'
        this.selectAllStatusInPage('N')
        break
    }
  },
  selectAllSelectInPage (flgSelect) {
    for (var i = this.startRow - 1; i < this.endRow; i++) {
      this.$set(this.dataTableRows[i], 'flgSelect', flgSelect)
    }
  },
  selectAllUserStatusInPage (flgact) {
    for (var i = this.startRow - 1; i < this.endRow; i++) {
      this.$set(this.dataTableRows[i], 'flgact', flgact)
    }
  },
  selectAllStatusInPage (flgStaappr) {
    for (var i = this.startRow - 1; i < this.endRow; i++) {
      if (!this.rowDisableConditionBuiltIn(this.dataTableRows[i])) {
        this.$set(this.dataTableRows[i], 'flgStaappr', flgStaappr)
      }
    }
  },
  selectAllDeleteInPage (deleteIconBool) {
    for (var i = this.startRow - 1; i < this.endRow; i++) {
      if (!this.rowDisableConditionBuiltIn(this.dataTableRows[i])) {
        this.dataTableRows[i].flgDelete = deleteIconBool
      }
    }
  },
  toggleDelete (deleteIconBool) {
    this.deleteIconBool = deleteIconBool
    this.deleteIconDesc = (deleteIconBool) ? 'Delete' : 'None'
    this.selectAllDeleteInPage(deleteIconBool)
  },
  findDataSelected (dataRows) {
    return dataRows.filter(function (dataRow) {
      return dataRow.flgSelect === 'Y'
    })
  },
  // Tick Add Edit Cancel Methods
  eventTrigger (event, dataRow) {
    this.inputSearch = ''
    switch (event) {
      case 'tick':
        this.tickEvent(dataRow)
        break
      case 'add':
        this.addEvent()
        break
      case 'edit':
        if (this.rowDisableConditionBuiltIn(dataRow)) {
          let editEvent = this.editEvent
          swal({
            title: constLabels('confirmTitle'),
            text: constLabels('HR1490'),
            confirmButtonText: constLabels('ok'),
            closeOnConfirm: true,
            closeOnCancel: true,
            html: true
          },
          function (isConfirm) {
            if (isConfirm) editEvent(dataRow)
          })
        } else {
          this.editEvent(dataRow)
        }
        break
      case 'cancel':
        if (this.rowDisableConditionBuiltIn(dataRow)) {
          let cancelEvent = this.cancelEvent
          swal({
            title: constLabels('confirmTitle'),
            text: constLabels('HR1490'),
            confirmButtonText: constLabels('ok'),
            closeOnConfirm: true,
            closeOnCancel: true,
            html: true
          },
          function (isConfirm) {
            if (isConfirm) cancelEvent(dataRow)
          })
        } else {
          this.cancelEvent(dataRow)
        }
    }
  },
  // Inline Methods
  addInlineEventBuiltIn () {
    this.searchBool = true
    this.pageBool = true
    this.inputSearch = ''
    let maxRowID = this.data.rows.map(function (dataRow) { return dataRow.rowID })
                                  .reduce(function (maxRowID, rowID) { return (maxRowID > rowID) ? maxRowID : rowID }, 0)
    let lastObj = this.data.rows[this.data.rows.length - 1]
    let newObj = {}
    if (this.dataTableRows.length === 0 || !_.isEmpty(lastObj, true) || (!lastObj.flgAdd && !lastObj.flgEdit && !lastObj.flgDelete)) {
      for (var filterColumn of this.filteredBodyColumns) {
        if (filterColumn.inlineType === 'logical' || filterColumn.inlineType === 'calculator') {
          newObj[filterColumn.key] = {
            code: '',
            description: ''
          }
        } else {
          newObj[filterColumn.key] = ''
        }
      }
      newObj.rowID = maxRowID + 1
      if (this.addInlineEventBool) {
        newObj.flgAdd = true
      }
      if (this.editInlineEventBool) {
        newObj.flgEdit = false
      }
      if (this.deleteInlineEventBool) {
        newObj.flgDelete = false
        newObj.flgDeleteHide = false
      }
      if (typeof this.approveEvent !== 'undefined') {
        newObj.staappr = (typeof newObj.staappr === 'undefined') ? '' : newObj.staappr
        newObj.flgstaappr = newObj.staappr
      }
      this.data.rows.push(newObj)
      this.data.total = this.data.rows.length + 1
    }
    var page = (this.displayRowSelectedComputed % this.displayRowSelected === 0) ? this.totalPage + 1 : this.totalPage
    this.setCurrentPage(page)
    if (typeof this.afterAddInlineEvent !== 'undefined') {
      this.afterAddInlineEvent(newObj)
    }
  },
  editInlineEventBuiltIn (eventName, dataRow) {
    if (!dataRow[eventName + '-flgEdit'] && !dataRow.flgAdd) {
      this.$set(dataRow, eventName + '-flgEdit', true)
      if (!dataRow.flgEdit) {
        this.searchBool = true
        this.pageBool = true
        this.inputSearch = ''
        this.$set(dataRow, 'flgEdit', true)
      }
    }
  },
  deleteInlineEventBuiltIn (flgDeleteHide = false) {
    for (var rowIndex = this.endRow - 1; rowIndex >= this.startRow - 1; rowIndex--) {
      let paramRow = this.dataTableRows[rowIndex]
      if (paramRow.flgDelete && flgDeleteHide) {
        let index = this.data.rows.map(function (dataRow) {
          return isNaN(dataRow.rowID) ? 0 : dataRow.rowID
        }).indexOf(paramRow.rowID)
        this.$set(this.data.rows[index], 'flgDeleteHide', true)
      }
    }
    let total = this.data.rows.filter(function (dataRow) {
      return !dataRow.flgDelete || !dataRow.flgDeleteHide
    }).length
    this.data.total = total

    this.setCurrentPage(1)
    this.deleteIconBool = false
    if (typeof this.afterDeleteInlineEvent !== 'undefined') {
      let dataRowsInline = this.findInlineDataHasFlg(this.data.rows)
      this.afterDeleteInlineEvent(dataRowsInline)
      // this.clearInlineDataRows('delete')
    }
  },
  saveInlineEventBuiltIn () {
    this.$validator.validateAll().then(result => {
      if (result) {
        this.searchBool = false
        this.pageBool = false
        this.deleteInlineEventBuiltIn()
        if (typeof this.saveInlineEvent !== 'undefined') {
          let dataRowsInline = this.findInlineDataHasFlg(this.data.rows)
          this.saveInlineEvent(dataRowsInline)
          // this.clearInlineDataRows('save')
        }
      }
    })
  },
  detailEventBuiltIn (dataRow) {
    window.scrollTo(0, 0)
    this.detailEvent(dataRow)
  },
  selectEventBuiltIn () {
    if (typeof this.selectEvent !== 'undefined') {
      let selectedDataRows = this.findDataSelected(this.data.rows)
      this.selectEvent(selectedDataRows)
    }
  },
  findInlineDataHasFlg (dataRows) {
    let dataRowsFilter = dataRows.filter(function (dataRow) {
      return dataRow.flgAdd || dataRow.flgEdit || dataRow.flgDelete
    })
    let dataRowsNew = dataRowsFilter.map(function (dataRowFilter) {
      let dataRowNew = {}
      dataRowNew['flg'] = (dataRowFilter.flgAdd && !dataRowFilter.flgDelete) ? 'add'
                        : (dataRowFilter.flgEdit && !dataRowFilter.flgAdd && !dataRowFilter.flgDelete) ? 'edit'
                        : (dataRowFilter.flgDelete && !dataRowFilter.flgAdd) ? 'delete' : ''
      for (var key of Object.keys(dataRowFilter)) {
        if (key !== 'rowID' && key !== 'flgAdd' && key !== 'flgEdit' && key !== 'flgDelete' && key !== 'flgDeleteHide' && key !== 'flg') {
          dataRowNew[key] = dataRowFilter[key]
        }
      }
      return dataRowNew
    })
    return dataRowsNew
  },
  onSelectChangeEditInlineAndColumnEvent (eventName, dataRow, event) {
    this.editInlineEventBuiltIn(eventName, dataRow)
    let eventObj = event || { type: '', key: '' }
    this.columnEvent(eventName, dataRow, eventObj)
    this.$nextTick(() => {
      this.errors.clear()
    })
  },
  clearInlineDataRows (flg) {
    for (var i = this.data.rows.length - 1; i >= 0; i--) {
      let dataRow = this.data.rows[i]
      if (flg === 'save') {
        dataRow.flgAdd = false
        dataRow.flgEdit = false
        dataRow.flg = ''
      }
      if (dataRow.flgDelete) {
        let index = this.data.rows.map(function (dataRow) {
          return dataRow.rowID
        }).indexOf(dataRow.rowID)
        this.$set(this.data.rows[i], 'flgDelete', true)
        this.data.rows.splice(index, 1)
      }
    }
    this.data.total = this.data.rows.length
  },
  // Validate Methods
  findValidateRules (rules, rowIndex) {
    let targetRules = [
      'after',
      'before',
      'requiredWhenExist',
      'requiredWhenNotExist',
      'requiredAny',
      'requiredAllWhenExistAll',
      'requiredAllWhenExistAny',
      'requiredAllWhenNotExistAll',
      'requiredAllWhenNotExistAny',
      'requiredAnyWhenExistAll',
      'requiredAnyWhenExistAny',
      'requiredAnyWhenNotExistAll',
      'requiredAnyWhenNotExistAny'
    ]
    let multiParamRules = [
      'requiredAllWhenExistAll',
      'requiredAllWhenExistAny',
      'requiredAllWhenNotExistAll',
      'requiredAllWhenNotExistAny',
      'requiredAnyWhenExistAll',
      'requiredAnyWhenExistAny',
      'requiredAnyWhenNotExistAll',
      'requiredAnyWhenNotExistAny'
    ]
    let arrRules = []
    if (typeof rules !== 'undefined') {
      arrRules = rules.split('|')
      for (let targetRule of targetRules) {
        let ruleIndex = arrRules.map(function (rule) {
          let mainRule = rule.split(':')
          return mainRule[0]
        }).indexOf(targetRule)
        if (ruleIndex > -1) {
          if (multiParamRules.indexOf(targetRule) > -1) {
            let arrRulesFromRuleIndex = arrRules[ruleIndex].split(':')
            let arrRuleParams = arrRulesFromRuleIndex[1].split(',')
            let arrRequireParams = arrRuleParams[0].split('-')
            let requireParams = ''
            let requireConcat = ''
            for (let requireParam of arrRequireParams) {
              requireParams += requireConcat + requireParam + rowIndex
              requireConcat = '-'
            }
            let arrWhenParams = arrRuleParams[1].split('-')
            let whenParams = ''
            let whenConcat = ''
            for (let whenParam of arrWhenParams) {
              whenParams += whenConcat + whenParam + rowIndex
              whenConcat = '-'
            }
            let ruleParams = requireParams + ',' + whenParams
            arrRules[ruleIndex] = arrRulesFromRuleIndex[0] + ':' + ruleParams
          } else {
            arrRules[ruleIndex] = arrRules[ruleIndex] + rowIndex
          }
        }
      }
    }
    return arrRules.join('|')
  },
  // Row Condition Methods
  rowDisableConditionBuiltIn (dataRow) {
    return (typeof this.rowDisableCondition !== 'undefined') ? this.rowDisableCondition(dataRow) : this.rowDisableBaseCondition(dataRow)
  },
  rowDisableBaseCondition (dataRow) {
    if (typeof dataRow.staappr !== 'undefined') {
      if (typeof this.approveEvent !== 'undefined') { // approve page
        return (!(dataRow.staappr === '' || dataRow.staappr === 'P' || dataRow.staappr === 'A') &&
                (dataRow.staappr === 'C' || dataRow.staappr === 'Y' || dataRow.staappr === 'N'))
      } else { // request page
        return (!(dataRow.staappr === '' || dataRow.staappr === 'P' || dataRow.staappr === 'C') &&
                (dataRow.staappr === 'A' || dataRow.staappr === 'Y' || dataRow.staappr === 'N'))
      }
    } else {
      return false
    }
  },
  // Summary Column Methods
  summaryDisp () {
    let summaryColumnCounted = this.filteredBodyColumns.filter(function (column) {
      return (column.summary)
    }).length
    return summaryColumnCounted > 0
  },
  summaryColumn (column) {
    let summary = this.data.rows.map(function (dataRow) {
      return isNaN(parseFloat(dataRow[column.key].replace(/,/g, ''), 2)) ? 0 : parseFloat(dataRow[column.key].replace(/,/g, ''), 2)
    }).reduce(function (a, b) {
      return a + b
    })
    return summary.toFixed(2)
  },
  // Cursor Position
  getCalculatorCursorPosition () {
    let oField = this.$refs.textareaCalculator
    let cursorPos = 0
    if (document.selection) {
      oField.focus()
      let oSel = document.selection.createRange()
      oSel.moveStart('character', -oField.value.length)
      cursorPos = oSel.text.length
    } else if (oField.selectionStart || oField.selectionStart === '0') {
      cursorPos = oField.selectionStart
    }
    return cursorPos
  },
  setCalculatorCursorPosition (setCursorPosition) {
    let oField = this.$refs.textareaCalculator
    oField.focus()
    oField.selectionEnd = setCursorPosition
  },
  // Resize Methods
  handleResize () {
    this.windowWidth = document.documentElement.clientWidth
    this.windowHeight = document.documentElement.clientHeight
  }
}
