import assetsLabels from 'assets/js/labels'

module.exports = {
  pcDisp () {
    return (this.windowWidth >= assetsLabels.pcWidth)
  },
  showPage () {
    return (this.pcDisp) ? 5 : 3
  },
  checkedAll: {
    get: function () {
      return this.dataTableRows ? this.selectedRows.length === this.dataTableRows.length : false
    },
    set: function (value) {
      let selectedRows = []
      if (value) {
        this.dataTableRows.forEach(function (dataRow) {
          selectedRows.push(dataRow)
        })
      }
      this.selectedRows = selectedRows
    }
  },
  displayRowSelectedComputed () {
    let displayRowSelected = (parseInt(this.displayRowSelected) <= this.dataTableTotal) ? parseInt(this.displayRowSelected) : this.dataTableTotal
    return displayRowSelected
  },
  startRow () {
    return (((this.currentPage - 1) * this.displayRowSelectedComputed) + 1)
  },
  endRow () {
    let endRow = (((this.currentPage - 1) * this.displayRowSelectedComputed) + this.displayRowSelectedComputed)
    return (endRow <= this.dataTableTotal) ? endRow : this.dataTableTotal
  },
  totalPage () {
    return Math.ceil(this.dataTableTotal / this.displayRowSelectedComputed)
  },
  dataTableTotal () {
    let deleteInlineEventBool = this.deleteInlineEventBool
    return this.dataTableRows.filter(function (dataRow) {
      return !dataRow.flgDeleteHide || !deleteInlineEventBool
    }).length
  },
  dataTableRows () {
    let _this = this
    let dataRows = []
    dataRows = this.data.rows.filter(function (dataRow, index) {
      if (!dataRow.flgDeleteHide || !_this.deleteInlineEventBool) {
        if (_this.inputSearch !== '') {
          for (let column of _this.filteredBodyColumns) {
            let value = ''
            if (typeof column.inlineType !== 'undefined' && column.inlineType === 'select') {
              value = column.inlineDropdown[dataRow[column.key]]
            } else {
              value = dataRow[column.key]
            }
            if (typeof value === 'string') {
              if (value.includes(_this.inputSearch)) {
                return true
              }
            } else if (typeof value === 'number') {
              if (value.toString().includes(_this.inputSearch)) {
                return true
              }
            }
          }
        } else {
          return true
        }
      }
    })
    return this.sortData(dataRows)
  },
  noDataColumn () {
    let numColumn = this.filteredBodyColumns.length
    numColumn = (this.indexDisp) ? ++numColumn : numColumn
    numColumn = (typeof this.multiSelect !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.approveEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.selectEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.userStatusEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.tickEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.detailEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.approveEvent !== 'undefined' && !this.approveBtnDisp) ? --numColumn : numColumn
    numColumn = (typeof this.addEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.editEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.cancelEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (this.deleteInlineEventBool) ? ++numColumn : numColumn
    return numColumn
  },
  preSummaryColumn () {
    let numColumn = this.notPreSummaryColumn
    numColumn = (this.indexDisp) ? ++numColumn : numColumn
    numColumn = (typeof this.multiSelect !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.approveEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.selectEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.userStatusEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.tickEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.detailEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.approveEvent !== 'undefined' && !this.approveBtnDisp) ? --numColumn : numColumn
    numColumn = (typeof this.addEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.editEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (typeof this.cancelEvent !== 'undefined') ? ++numColumn : numColumn
    numColumn = (this.deleteInlineEventBool) ? ++numColumn : numColumn
    return numColumn
  },
  notPreSummaryColumn () {
    let numColumn = 0
    for (let column of this.filteredBodyColumns) {
      if (typeof column.summary === 'undefined') {
        numColumn = ++numColumn
      } else {
        if (column.summary) {
          break
        }
      }
    }
    return numColumn
  },
  filterSettingColumns: {
    get () {
      let columnsRowspanMoreThanOneRow = this.filteredBodyColumns.filter((column) => {
        return column.rowspan > 1
      })
      return this.filteredBodyColumns.filter((column) => {
        if (columnsRowspanMoreThanOneRow.length > 0) {
          return column.colspan === 1 && column.rowspan > 1
        } else {
          return column.colspan === 1 && column.rowspan === 1
        }
      })
    },
    set (newValue) {
      let localIndex = 0
      let settingIndex = 0
      for (let column of this.localColumns) {
        let colspan = column.colspan || 1
        let headerRow = column.headerRow || 1
        if (this.maxRowspan === 1) {
          if (colspan === 1 && headerRow === 1) {
            if (column.key !== newValue[settingIndex].key) {
              this.$set(this.localColumns, localIndex, newValue[settingIndex])
            }
            settingIndex++
          }
        } else {
          if (colspan === 1 && headerRow === 1 && column.rowspan === this.maxRowspan - column.headerRow + 1) {
            if (column.key !== newValue[settingIndex].key) {
              this.$set(this.localColumns, localIndex, newValue[settingIndex])
            }
            settingIndex++
          }
        }
        localIndex++
      }
    }
  }
}
