import assetsLabels from 'assets/js/labels'

module.exports = {
  value: {},
  columns: {
    type: Array,
    required: true,
    default () {
      return {
        key: '',
        name: ''
      }
    }
  },
  data: {
    type: Object,
    required: true,
    default: function () {
      return {
        total: 0,
        rows: []
      }
    }
  },
  loadingParentBool: {
    type: Boolean,
    default: false
  },
  preLoadParentBool: {
    type: Boolean,
    default: false
  },
  page: {
    type: Number,
    default: 1,
    validator: function (value) {
      return value > 0
    }
  },
  limit: {
    type: Number,
    default: 10,
    validator: function (value) {
      return value > 0
    }
  },
  maxRowspan: {
    type: Number,
    default: 1
  },
  searchDisp: {
    type: Boolean,
    default: true
  },
  tablePageDisp: {
    type: Boolean,
    default: true
  },
  sortDisp: {
    type: Boolean,
    default: true
  },
  indexDisp: {
    type: Boolean,
    default: true
  },
  multiSelect: {},
  approveRowBool: {
    type: Boolean,
    default: false
  },
  approveBtnDisp: {
    type: Boolean,
    default: false
  },
  approveEvent: {
    type: Function
  },
  detailEvent: {
    type: Function
  },
  tickEvent: {
    type: Function
  },
  selectEvent: {
    type: Function
  },
  selectRowBool: {
    type: Boolean,
    default: false
  },
  selectBtnDisp: {
    type: Boolean,
    default: false
  },
  userStatusEvent: {
    type: Function
  },
  userStatusRowBool: {
    type: Boolean,
    default: false
  },
  userStatusBtnDisp: {
    type: Boolean,
    default: false
  },
  addBtnDisp: {
    type: Boolean,
    default: true
  },
  addEvent: {
    type: Function
  },
  editEvent: {
    type: Function
  },
  cancelEvent: {
    type: Function
  },
  backEvent: {
    type: Function
  },
  afterAddInlineEvent: {
    type: Function
  },
  afterEditInlineEvent: {
    type: Function
  },
  afterDeleteInlineEvent: {
    type: Function
  },
  columnEvent: {
    type: Function,
    default () {}
  },
  saveInlineEvent: {
    type: Function
  },
  addInlineEventBool: {
    type: Boolean,
    default: false
  },
  editInlineEventBool: {
    type: Boolean,
    default: false
  },
  deleteInlineEventBool: {
    type: Boolean,
    default: false
  },
  nowLang: {
    type: String,
    default: assetsLabels.defaultLang
  },
  rowDisableCondition: {
    type: Function
  },
  deleteInlineBtnDisp: {
    type: Boolean,
    default: true
  },
  saveInlineBtnDisp: {
    type: Boolean,
    default: true
  },
  summaryLabel: {
    type: String,
    default: ''
  },
  pdfExportDisp: {
    type: Boolean,
    default: false
  },
  csvExportDisp: {
    type: Boolean,
    default: true
  },
  columnSettingDisp: {
    type: Boolean,
    default: true
  },
  footerDisp: {
    type: Boolean,
    default: false
  },
  footerData: {
    type: Object
  },
  footerComponent: {
    type: Object
  },
  headerLabels: {
    type: Object
  },
  headerEvent: {
    type: Object
  },
  headerData: {
    type: Object
  },
  headerDisp: {
    type: Boolean,
    default: false
  },
  headerComponent: {
    type: Object
  }
}
