module.exports = {
  selectedRows () {
    this.$emit('input', this.selectedRows)
  },
  displayRowSelected () {
    let currentPage = Math.ceil(this.dataTableTotal / this.displayRowSelected)
    this.currentPage = (currentPage <= 0) ? 1 : currentPage
  },
  loadingParentBool () {
    this.loadingBool = this.loadingParentBool
  },
  preLoadParentBool () {
    this.preLoadBool = this.preLoadParentBool
  },
  columns () {
    this.localColumns = Object.assign([], this.columns)
  },
  localColumns () {
    this.$nextTick(() => {
      this.filteredBodyColumns = this.bodyColumnsFn()
      for (let column of this.filteredBodyColumns) {
        this.$set(column, 'sortNum', column.sortNum || 0)
        this.$set(column, 'enabled', column.enabled || true)
      }
    })
  },
  data () {
    this.initialLocalKeyOnDataTableRows()
    var page = 1 // (this.displayRowSelectedComputed % this.displayRowSelected === 0) ? this.totalPage + 1 : this.totalPage
    this.setCurrentPage(page)
    this.loadingBool = false
    this.$emit('setLoadingParentBool', false)
    this.$nextTick(() => {
      this.errors.clear()
    })
  }
}
