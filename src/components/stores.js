let hrmsCalendarWorkTime = require('./hrmsCalendarWorkTime/store').default
let hrmsChangeLabel = require('./hrmsChangeLabel/store').default
let hrmsDropzone = require('./hrmsDropzone/store').default
let hrmsLov = require('./hrmsLov/store').default
let hrmsLovCodcomp = require('./hrmsLovCodcomp/store').default
let hcmLovCodcomp = require('./hcmLovCodcomp/store').default
let hcmLov = require('./hcmLov/store').default

module.exports = {
  hrmsCalendarWorkTime,
  hrmsChangeLabel,
  hrmsDropzone,
  hrmsLov,
  hrmsLovCodcomp,
  hcmLovCodcomp,
  hcmLov
}
