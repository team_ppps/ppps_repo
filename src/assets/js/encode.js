var EncodeUtils = window.Utils || {}

EncodeUtils.stdenc = function (data) {
  var data01 = data.substr(0, 1)
  var data02 = data.substr(1, 1)
  var data03 = data.substr(2, 1)
  var data04 = data.substr(3, 1)
  var data05 = data.substr(4, 1)
  var data06 = data.substr(5, 1)
  var data07 = data.substr(6, 1)
  var data08 = data.substr(7, 1)
  var data09 = data.substr(8, 1)
  var data10 = data.substr(9, 1)
  var data11 = data.substr(10, 1)
  var data12 = data.substr(11, 1)
  var data13 = data.substr(12, 1)
  var data14 = data.substr(13, 1)
  var data15 = data.substr(14, 1)
  var chk01 = Math.floor((Math.random() * 9) + 1)
  var chk02 = Math.floor((Math.random() * 9) + 1)
  var chk03 = Math.floor((Math.random() * 9) + 1)
  var chk04 = Math.floor((Math.random() * 9) + 1)
  var chk05 = Math.floor((Math.random() * 9) + 1)
  var chk06 = Math.floor((Math.random() * 9) + 1)
  var chk07 = Math.floor((Math.random() * 9) + 1)
  var chk08 = Math.floor((Math.random() * 9) + 1)
  var chk09 = Math.floor((Math.random() * 9) + 1)
  var chk10 = Math.floor((Math.random() * 9) + 1)
  var chk11 = Math.floor((Math.random() * 9) + 1)
  var chk12 = Math.floor((Math.random() * 9) + 1)
  var chk13 = Math.floor((Math.random() * 9) + 1)
  var chk14 = Math.floor((Math.random() * 9) + 1)
  var chk15 = Math.floor((Math.random() * 9) + 1)
  var dataEnc = chk14 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data12) + (chk12 * 12), 3, '0') +
                 chk03 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data06) + (chk06 * 6), 3, '0') +
                 chk13 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data02) + (chk02 * 2), 3, '0') +
                 chk05 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data01) + (chk01 * 1), 3, '0') +
                 chk06 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data13) + (chk13 * 13), 3, '0') +
                 chk15 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data05) + (chk05 * 5), 3, '0') +
                 chk04 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data03) + (chk03 * 3), 3, '0') +
                 chk08 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data07) + (chk07 * 7), 3, '0') +
                 chk07 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data14) + (chk14 * 14), 3, '0') +
                 chk09 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data11) + (chk11 * 11), 3, '0') +
                 chk12 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data08) + (chk08 * 8), 3, '0') +
                 chk01 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data04) + (chk04 * 4), 3, '0') +
                 chk10 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data15) + (chk15 * 15), 3, '0') +
                 chk02 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data09) + (chk09 * 9), 3, '0') +
                 chk11 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data10) + (chk10 * 10), 3, '0')
  return dataEnc
}

EncodeUtils.stddec = function (data) {
  var chk01 = data.substr(44, 1)
  var chk02 = data.substr(52, 1)
  var chk03 = data.substr(4, 1)
  var chk04 = data.substr(24, 1)
  var chk05 = data.substr(12, 1)
  var chk06 = data.substr(16, 1)
  var chk07 = data.substr(32, 1)
  var chk08 = data.substr(28, 1)
  var chk09 = data.substr(36, 1)
  var chk10 = data.substr(48, 1)
  var chk11 = data.substr(56, 1)
  var chk12 = data.substr(40, 1)
  var chk13 = data.substr(8, 1)
  var chk14 = data.substr(0, 1)
  var chk15 = data.substr(20, 1)
  var data01 = String.fromCharCode(data.substr(13, 3) - (chk01 * 1))
  var data02 = String.fromCharCode(data.substr(9, 3) - (chk02 * 2))
  var data03 = String.fromCharCode(data.substr(25, 3) - (chk03 * 3))
  var data04 = String.fromCharCode(data.substr(45, 3) - (chk04 * 4))
  var data05 = String.fromCharCode(data.substr(21, 3) - (chk05 * 5))
  var data06 = String.fromCharCode(data.substr(5, 3) - (chk06 * 6))
  var data07 = String.fromCharCode(data.substr(29, 3) - (chk07 * 7))
  var data08 = String.fromCharCode(data.substr(41, 3) - (chk08 * 8))
  var data09 = String.fromCharCode(data.substr(53, 3) - (chk09 * 9))
  var data10 = String.fromCharCode(data.substr(57, 3) - (chk10 * 10))
  var data11 = String.fromCharCode(data.substr(37, 3) - (chk11 * 11))
  var data12 = String.fromCharCode(data.substr(1, 3) - (chk12 * 12))
  var data13 = String.fromCharCode(data.substr(17, 3) - (chk13 * 13))
  var data14 = String.fromCharCode(data.substr(33, 3) - (chk14 * 14))
  var data15 = String.fromCharCode(data.substr(49, 3) - (chk15 * 15))
  var dataDec = data01 + data02 + data03 + data04 + data05 + data06 + data07 + data08 + data09 + data10 + data11 + data12 + data13 + data14 + data15

  return dataDec
}

EncodeUtils.stdencWithKey = function (data, key) {
  var data01 = data.substr(0, 1)
  var data02 = data.substr(1, 1)
  var data03 = data.substr(2, 1)
  var data04 = data.substr(3, 1)
  var data05 = data.substr(4, 1)
  var data06 = data.substr(5, 1)
  var data07 = data.substr(6, 1)
  var data08 = data.substr(7, 1)
  var data09 = data.substr(8, 1)
  var data10 = data.substr(9, 1)
  var data11 = data.substr(10, 1)
  var data12 = data.substr(11, 1)
  var data13 = data.substr(12, 1)
  var data14 = data.substr(13, 1)
  var data15 = data.substr(14, 1)
  var chk01 = Math.floor((Math.random() * 9) + 1)
  var chk02 = Math.floor((Math.random() * 9) + 1)
  var chk03 = Math.floor((Math.random() * 9) + 1)
  var chk04 = Math.floor((Math.random() * 9) + 1)
  var chk05 = Math.floor((Math.random() * 9) + 1)
  var chk06 = Math.floor((Math.random() * 9) + 1)
  var chk07 = Math.floor((Math.random() * 9) + 1)
  var chk08 = Math.floor((Math.random() * 9) + 1)
  var chk09 = Math.floor((Math.random() * 9) + 1)
  var chk10 = Math.floor((Math.random() * 9) + 1)
  var chk11 = Math.floor((Math.random() * 9) + 1)
  var chk12 = Math.floor((Math.random() * 9) + 1)
  var chk13 = Math.floor((Math.random() * 9) + 1)
  var chk14 = Math.floor((Math.random() * 9) + 1)
  var chk15 = Math.floor((Math.random() * 9) + 1)
  var dataEnc = key +
                 chk14 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data12) + (chk12 * 12), 3, '0') +
                 chk03 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data06) + (chk06 * 6), 3, '0') +
                 chk13 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data02) + (chk02 * 2), 3, '0') +
                 chk05 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data01) + (chk01 * 1), 3, '0') +
                 chk06 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data13) + (chk13 * 13), 3, '0') +
                 chk15 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data05) + (chk05 * 5), 3, '0') +
                 chk04 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data03) + (chk03 * 3), 3, '0') +
                 chk08 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data07) + (chk07 * 7), 3, '0') +
                 chk07 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data14) + (chk14 * 14), 3, '0') +
                 chk09 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data11) + (chk11 * 11), 3, '0') +
                 chk12 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data08) + (chk08 * 8), 3, '0') +
                 chk01 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data04) + (chk04 * 4), 3, '0') +
                 chk10 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data15) + (chk15 * 15), 3, '0') +
                 chk02 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data09) + (chk09 * 9), 3, '0') +
                 chk11 + EncodeUtils.lpad(EncodeUtils.charCodeOf(data10) + (chk10 * 10), 3, '0')
  return dataEnc
}

EncodeUtils.stddecWithKey = function (datakey, key) {
  var data = datakey.replace(key, '')
  var chk01 = data.substr(44, 1)
  var chk02 = data.substr(52, 1)
  var chk03 = data.substr(4, 1)
  var chk04 = data.substr(24, 1)
  var chk05 = data.substr(12, 1)
  var chk06 = data.substr(16, 1)
  var chk07 = data.substr(32, 1)
  var chk08 = data.substr(28, 1)
  var chk09 = data.substr(36, 1)
  var chk10 = data.substr(48, 1)
  var chk11 = data.substr(56, 1)
  var chk12 = data.substr(40, 1)
  var chk13 = data.substr(8, 1)
  var chk14 = data.substr(0, 1)
  var chk15 = data.substr(20, 1)
  var data01 = String.fromCharCode(data.substr(13, 3) - (chk01 * 1))
  var data02 = String.fromCharCode(data.substr(9, 3) - (chk02 * 2))
  var data03 = String.fromCharCode(data.substr(25, 3) - (chk03 * 3))
  var data04 = String.fromCharCode(data.substr(45, 3) - (chk04 * 4))
  var data05 = String.fromCharCode(data.substr(21, 3) - (chk05 * 5))
  var data06 = String.fromCharCode(data.substr(5, 3) - (chk06 * 6))
  var data07 = String.fromCharCode(data.substr(29, 3) - (chk07 * 7))
  var data08 = String.fromCharCode(data.substr(41, 3) - (chk08 * 8))
  var data09 = String.fromCharCode(data.substr(53, 3) - (chk09 * 9))
  var data10 = String.fromCharCode(data.substr(57, 3) - (chk10 * 10))
  var data11 = String.fromCharCode(data.substr(37, 3) - (chk11 * 11))
  var data12 = String.fromCharCode(data.substr(1, 3) - (chk12 * 12))
  var data13 = String.fromCharCode(data.substr(17, 3) - (chk13 * 13))
  var data14 = String.fromCharCode(data.substr(33, 3) - (chk14 * 14))
  var data15 = String.fromCharCode(data.substr(49, 3) - (chk15 * 15))
  var dataDec = data01 + data02 + data03 + data04 + data05 + data06 + data07 + data08 + data09 + data10 + data11 + data12 + data13 + data14 + data15

  return dataDec
}

EncodeUtils.charCodeOf = function (char) {
  if (isNaN(char.charCodeAt(0))) {
    return 0
  }
  return char.charCodeAt(0)
}

EncodeUtils.lpad = function (n, width, z) {
  z = z || '0'
  n = n + ''
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n
}

export { EncodeUtils }
