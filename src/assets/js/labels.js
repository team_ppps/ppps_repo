module.exports = {
  version: '2.2.2',
  softwareName: 'PeoplePlus HCM V.11.0',
  loginLabel: 'Login',
  defaultLang: 'th',
  allLang: ['en', 'th'],
  showConn: true,
  windowWidth: 0,
  windowHeight: 0,
  pcWidth: 769,
  baseConn: {
    user: 'ESS_STA3',
    db: 'HR12C'
  },
  loginMessageError: {
    en: {
      ERROR: 'System Error',
      TIMEOUT: 'Login time was expired',
      LICENSE: 'User entrance is limited',
      PERMIT: 'User permission failed'
    },
    th: {
      ERROR: 'ระบบเกิดข้อผิดพลาด',
      TIMEOUT: 'ช่วงเวลาเชื่อมต่อระบบสิ้นสุดลง',
      LICENSE: 'ผู้เข้าใช้งานเกินกำหนด',
      PERMIT: 'ผู้เข้าใช้งานไม่มีสิทธิใช้งานฟังก์ชันนี้'
    }
  },
  fontIcon: {
    'request': 'fa-pencil',
    'report': 'fa-file-text',
    'approve': 'fa-pencil-square-o',
    'dashboard': 'fa-dashboard'
  },
  mainMenu: {
    'en': 'Main Menu',
    'th': 'เมนูหลัก'
  },
  menuRequest: {
    'en': 'Employee Self-Service',
    'th': 'บริการข้อมูลด้วยตนเอง'
  },
  menuApprove: {
    'en': 'Manager Self-Service',
    'th': 'ข้อมูลสำหรับผู้บริหาร'
  },
  menu: {
    'request': {
      'en': 'Request Menu',
      'th': 'เมนูบันทึก'
    },
    'report': {
      'en': 'Report Menu',
      'th': 'เมนูรายงาน'
    },
    'approve': {
      'en': 'Approve Menu',
      'th': 'เมนูอนุมัติ'
    },
    'dashboard': {
      'en': 'Dashboard',
      'th': 'Dashboard'
    }
  },
  hres81x: {
    en: {
      'currency': 'Currency',
      'net_pay': 'Net pay',
      'net_pay_by_currency': 'Net pay by currency',
      'no_data_found': 'No data found',
      'btn_search': 'Search'
    },
    th: {
      'currency': 'สกุลเงิน',
      'net_pay': 'ยอดสุทธิสำหรับสกุลเงิน',
      'net_pay_by_currency': 'ยอดสุทธิสำหรับงวดนี้',
      'no_data_found': 'ไม่พบข้อมูล',
      'btn_search': 'ค้นหา'
    }
  },
  home: {
    en: {
      'warningmsg': 'Warning Message',
      'activity': 'Self Planner',
      'contact': 'Employee Directory',
      'knowledge': 'General Knowledge',
      'poll': 'Survey Form'
    },
    th: {
      'warningmsg': 'ข้อความแจ้งเตือน',
      'activity': 'กิจกรรม',
      'contact': 'รายชื่อผู้ติดต่อ',
      'knowledge': 'ความรู้ทั่วไป',
      'poll': 'แบบสอบถาม'
    }
  },
  button: {
    en: {
      save: 'Save',
      approve: 'Confirm',
      back: 'Back',
      search: 'Search',
      select: 'Select',
      edit: 'Edit',
      cancel: 'Cancel',
      delete: 'Delete',
      add: 'Add',
      detail: 'Detail',
      clear: 'Clear',
      sendmail: 'Sendmail',
      view: 'View',
      process: 'Process',
      ok: 'Ok',
      okAndContinue: 'Ok and Continue',
      book: 'Book',
      generate: 'Generate',
      load: 'Load',
      submit: 'Submit',
      saveAndContinue: 'Save and Continue',
      copy: 'Copy',
      usageStatus: 'Select',
      reset: 'Reset'
    },
    th: {
      save: 'บันทึก',
      approve: 'ยืนยัน',
      back: 'ย้อนกลับ',
      search: 'ค้นหา',
      select: 'เลือก',
      edit: 'แก้ไข',
      cancel: 'ยกเลิก',
      delete: 'ลบ',
      add: 'เพิ่ม',
      detail: 'รายละเอียด',
      clear: 'คืนค่า',
      sendmail: 'ส่งอีเมล์',
      view: 'ดูรายการ',
      process: 'Process',
      ok: 'ตกลง',
      okAndContinue: 'ตกลงและทำรายการใหม่',
      book: 'จอง',
      generate: 'Generate',
      load: 'โหลด',
      submit: 'ยืนยัน',
      saveAndContinue: 'บันทึกและทำรายการใหม่',
      copy: 'คัดลอก',
      usageStatus: 'เลือก',
      reset: 'คืนค่า'
    }
  },
  msgError: {
    en: {
      noDataApproved: '',
      noDataSelected: '',
      noDataHasFlg: 'no data was changed'
    },
    th: {
      noDataApproved: '',
      noDataSelected: '',
      noDataHasFlg: 'ข้อมูลไม่มีการเปลี่ยนแปลง'
    }
  },
  days: {
    en: [ 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa' ],
    th: [ 'อา', 'จ', 'อ', 'พ', 'พฤ', 'ศ', 'ส' ]
  },
  month: {
    en: [{
      full: 'January',
      short: 'Jan'
    }, {
      full: 'February',
      short: 'Feb'
    }, {
      full: 'March',
      short: 'Mar'
    }, {
      full: 'April',
      short: 'Apr'
    }, {
      full: 'May',
      short: 'May'
    }, {
      full: 'June',
      short: 'Jun'
    }, {
      full: 'July',
      short: 'Jul'
    }, {
      full: 'August',
      short: 'Aug'
    }, {
      full: 'September',
      short: 'Sep'
    }, {
      full: 'October',
      short: 'Oct'
    }, {
      full: 'November',
      short: 'Nov'
    }, {
      full: 'December',
      short: 'Dec'
    }],
    th: [{
      full: 'มกราคม',
      short: 'ม.ค.'
    }, {
      full: 'กุมภาพันธ์',
      short: 'ก.พ.'
    }, {
      full: 'มีนาคม',
      short: 'มี.ค.'
    }, {
      full: 'เมษายน',
      short: 'เม.ย.'
    }, {
      full: 'พฤษภาคม',
      short: 'พ.ค.'
    }, {
      full: 'มิถุนายน',
      short: 'มิ.ย.'
    }, {
      full: 'กรกฏาคม',
      short: 'ก.ค.'
    }, {
      full: 'สิงหาคม',
      short: 'ส.ค.'
    }, {
      full: 'กันยายน',
      short: 'ก.ย.'
    }, {
      full: 'ตุลาคม',
      short: 'ต.ค.'
    }, {
      full: 'พฤศจิกายน',
      short: 'พ.ย.'
    }, {
      full: 'ธันวาคม',
      short: 'ธ.ค.'
    }]
  },
  statusApprove: {
    en: {
      P: 'Pending',
      A: 'Approved',
      N: 'Rejected'
    },
    th: {
      P: 'รอพิจารณา',
      A: 'อนุมัติ',
      N: 'ไม่อนุมัติ'
    }
  },
  statusApproveHres3cu: {
    en: {
      D: 'During Process',
      N: 'New',
      S: 'Settled'
    },
    th: {
      D: 'ระหว่างดำเนินการ',
      N: 'คำร้องใหม่',
      S: 'จบ'
    }
  },
  period: {
    en: {
      1: '1',
      2: '2',
      3: '3',
      4: '4',
      5: '5',
      6: '6',
      7: '7',
      8: '8',
      9: '9',
      10: '10'
    },
    th: {
      1: '1',
      2: '2',
      3: '3',
      4: '4',
      5: '5',
      6: '6',
      7: '7',
      8: '8',
      9: '9',
      10: '10'
    }
  },
  typeuser: {
    en: {
      1: 'HR',
      2: 'Employee',
      3: 'Supervisor',
      4: 'All'
    },
    th: {
      1: 'HR',
      2: 'พนักงาน',
      3: 'หัวหน้างาน',
      4: 'ทั้งหมด'
    }
  },
  typeauth: {
    en: {
      1: 'Create user and set the main menu code.',
      2: 'Set permissions and Browsing',
      3: 'General User',
      4: 'Administrator',
      5: 'All'
    },
    th: {
      1: 'สร้าง Userและกำหนดรหัสเมนูหลัก',
      2: 'กำหนดสิทธิ์และการเรียกดูข้อมูล',
      3: 'ผู้ใช้งานทั่วไป',
      4: 'ผู้ดูแลระบบ',
      5: 'ทั้งหมด'
    }
  },
  flgact: {
    en: {
      1: 'Current',
      2: 'Resign',
      3: 'Suspend',
      4: 'All'
    },
    th: {
      1: 'ปัจจุบัน',
      2: 'ลาออก',
      3: 'ระงับใช้',
      4: 'ทั้งหมด'
    }
  },
  typincom: {
    en: {
      1: 'Normal Income',
      2: 'One-time Payment',
      3: 'Fix Rate 3%',
      4: 'Fix Rate 5%'
    },
    th: {
      1: 'เงินได้ปกติ',
      2: 'เงินได้จ่ายครั้งเดียว',
      3: 'Fix Rate 3%',
      4: 'Fix Rate 5%'
    }
  },
  typproba: {
    en: {
      1: 'Probation',
      2: 'Assay Position'
    },
    th: {
      1: 'ทดลองงาน',
      2: 'ทดลองตำแหน่ง'
    }
  },
  evastat: {
    en: {
      1: 'Not worth',
      2: 'Not confirmed',
      3: 'Confirm',
      4: 'Confirm and print the letter.',
      5: 'Processed',
      6: 'All'
    },
    th: {
      1: 'ยังไม่ประเมิณ',
      2: 'ยังไม่ยืนยัน',
      3: 'ยืนยัน',
      4: 'ยืนยันและพิมพ์จดหมาย',
      5: 'ประมวลผลแล้ว',
      6: 'ทั้งหมด'
    }
  },
  typabs: {
    en: {
      1: 'Late',
      2: 'Early Out',
      3: 'Absence',
      4: 'Not Stamp',
      5: 'All'
    },
    th: {
      1: 'สาย',
      2: 'กลับก่อน',
      3: 'ขาดงาน',
      4: 'ลืมรูดบัตร',
      5: 'ทั้งหมด'
    }
  },
  flgcanc: {
    en: {
      1: 'Cancel Success',
      2: 'Cancel Fail',
      3: 'All'
    },
    th: {
      1: 'ยกเลิกสำเร็จ',
      2: 'ยกเลิกไม่สำเร็จ',
      3: 'ทั้งหมด'
    }
  },
  mockPostMessage: {
    response: 'success'
  },
  mockErrorMessage: {
    status: 500,
    response: 'Data not found'
  },
  replaceLabelToColumns (columns, labels, dropdownSets, listFieldSets) {
    let columnArrs = []
    for (let columnIndex = 0; columnIndex < columns.length; columnIndex++) {
      let column = Object.assign({}, columns[columnIndex])
      if (typeof dropdownSets === 'object' && typeof column['inlineDropdown'] !== 'undefined') {
        for (let dropdownIndex = 0; dropdownIndex < dropdownSets.length; dropdownIndex++) {
          let key = (typeof dropdownSets[dropdownIndex].key !== 'undefined') ? dropdownSets[dropdownIndex].key : ''
          if (column.key === key) {
            column['inlineDropdown'] = dropdownSets[dropdownIndex]['value']
          }
        }
      }
      if (typeof listFieldSets === 'object' && typeof column['inlineLogicalListFields'] !== 'undefined') {
        for (let listFieldIndex = 0; listFieldIndex < listFieldSets.length; listFieldIndex++) {
          let key = (typeof listFieldSets[listFieldIndex].key !== 'undefined') ? listFieldSets[listFieldIndex].key : ''
          if (column.key === key) {
            column['inlineLogicalListFields'] = listFieldSets[listFieldIndex]['value']
          }
        }
      }
      let codapp = (typeof column['labelCodapp'] !== 'undefined') ? column['labelCodapp'] : ''
      let numseqStr = (typeof column['labelIndex'] !== 'undefined') ? column['labelIndex'] : ''
      if (typeof labels[codapp] !== 'undefined') {
        let arrNumseq = numseqStr.split('+')
        let concat = ''
        column['name'] = ''
        for (let numseq of arrNumseq) {
          if (typeof labels[codapp][numseq] !== 'undefined') {
            column['name'] += concat + labels[codapp][numseq]
            concat = ''
          } else {
            column['name'] = 'L[' + codapp + '][' + numseq + ']'
          }
        }
      } else {
        column['name'] = 'L[' + codapp + '][' + numseqStr + ']'
      }
      // delete column['labelCodapp']
      // delete column['labelIndex']

      // add status color to column['status']
      if (column['key'] === 'status' || column['key'] === 'desc_stareq') {
        column.columnClassCondition = function (dataRow, key, index) {
          return 'status-' + dataRow['staappr']
        }
      }
      if (column['key'] === 'statuscc') {
        column.columnClassCondition = function (dataRow, key, index) {
          return 'status-' + dataRow['staapprcc']
        }
      }
      columnArrs.push(column)
    }
    return columnArrs
  },
  getCurrentUserHeader () {
    const user = JSON.parse(window.localStorage.getItem('currentUser'))
    const lang = window.localStorage.getItem('lang')
    let currentUser = {}

    if (user !== null && lang !== null) {
      let pLang = '102'
      switch (lang) {
        case 'en':
          pLang = '101'
          break
        case 'th':
          pLang = '102'
          break
        default:
          pLang = '102'
          break
      }
      currentUser = {
        p_lrunning: user.p_lrunning,
        p_coduser: user.p_coduser,
        p_codpswd: user.p_codpswd,
        p_codempid: user.p_codempid,
        p_lang: pLang,
        login_status: user.status
      }
    }

    return JSON.stringify(currentUser)
  },
  getUsrCom () {
    const user = JSON.parse(window.localStorage.getItem('currentUser'))
    let usrcom = []

    if (user !== null) {
      usrcom = user.usrcom
    }

    return usrcom
  }
}
