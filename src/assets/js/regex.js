module.exports = {
  currency: /^[+-]?[0-9]{1,3}(?:,?[0-9]{3})*(?:\.[0-9]{2})?$/,
  colorCode: /^[#a-fA-F0-9]{6}$/
}
