let constLabels = {}

constLabels = function (index) {
  let arrLabels = {}
  let lang = window.localStorage.getItem('lang')

  arrLabels.en = {
    logout: 'Logout',
    confirmTitle: 'Confirmation',
    warningTitle: 'Warning Message',
    msgConfirmSave: 'Do you want to save data ?',
    msgConfirmDelete: 'Do you want to delete data ?',
    msgConfirmDeleteFile: 'Do you want to delete file ?',
    msgConfirmCancel: 'Do you want to cancel this record ?',
    msgConfirmCancelGeneral: 'Do you want to cancel ?',
    msgConfirmEmail: 'Do you want to send mail ?',
    msgConfirmResetSetting: 'Do you want to reset all settings ?',
    msgResetSettingSuccess: 'Reset all settings successfully.',
    msgWaiting: 'Please wait while processing ...',
    msgNoRecordSelect: 'No record selected',
    remark: 'Remark',
    HR1490: 'HR1490 Data had been committed.It can not be insert/delete.',
    HR1506: 'HR1506 This item has already been cancelled.',
    HR2045: 'HR2045 Please enter all required data.',
    ok: 'OK',
    cancel: 'Cancel',
    save: 'Save',
    back: 'Back',
    importData: 'Import Data',
    totalRows: 'Total',
    completeRows: 'Complete',
    errorRows: 'Error',
    record: 'Record',
    errorImportColm: 'Column number is invalid'
  }

  arrLabels.th = {
    logout: 'ออกจากระบบ',
    confirmTitle: 'ยืนยันการทำรายการ',
    warningTitle: 'ข้อความแจ้งเตือน',
    msgConfirmSave: 'ต้องการบันทึกข้อมูล ?',
    msgConfirmDelete: 'ต้องการลบข้อมูล ?',
    msgConfirmDeleteFile: 'ต้องการลบไฟล์ ?',
    msgConfirmCancel: 'ต้องการยกเลิกรายการหรือไม่ ?',
    msgConfirmCancelGeneral: 'ต้องการยกเลิกหรือไม่ ?',
    msgConfirmEmail: 'ต้องการยืนยันการส่งเมล์หรือไม่ ?',
    msgConfirmResetSetting: 'ต้องการรีเซ็ตการตั้งค่าทั้งหมด ?',
    msgResetSettingSuccess: 'รีเซ็ตการตั้งค่าทั้งหมดสำเร็จ',
    msgWaiting: 'กรุณารอสักครู่ ระบบกำลังประมวลผล ...',
    msgNoRecordSelect: 'รายการยังไม่ถูกเลือก',
    remark: 'หมายเหตุ',
    HR1490: 'HR1490 ข้อมูลมีการยืนยันแล้วไม่สามารถ เพิ่ม/แก้ไข/ลบ',
    HR1506: 'HR1506 ไม่สามารถแก้ไขรายการนี้ เพราะได้ยกเลิกรายการแล้ว',
    HR2045: 'HR2045 โปรดบันทึกข้อมูลที่จำเป็นให้ครบถ้วน',
    ok: 'ตกลง',
    cancel: 'ยกเลิก',
    save: 'บันทึก',
    back: 'ย้อนกลับ',
    importData: 'นำเข้าข้อมูล',
    totalRows: 'ทั้งหมด',
    completeRows: 'สำเร็จ',
    errorRows: 'ผิดพลาด',
    record: 'รายการ',
    errorImportColm: 'จำนวนคอลัมน์ไม่ถูกต้อง'
  }

  return (typeof arrLabels[lang][index] !== 'undefined') ? arrLabels[lang][index] : ''
}

export { constLabels }
