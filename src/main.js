// import 'babel-polyfill'
import Vue from 'vue'
import VueRouter from 'vue-router'
import VeeValidate from 'register/veeValidate'
import VueTouch from 'vue-touch'
import App from './App'

import { router } from 'register/router'
import store from 'register/stores'
import * as types from 'modules/main/login/store/mutation-types'
import { sync } from 'vuex-router-sync'
sync(store, router)

import 'font-awesome/css/font-awesome.min.css'
import 'sweetalert/dist/sweetalert.css'
import 'animate.css'

import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'prismjs/themes/prism.css'

import simplert from 'vue2-simplert'
import hrmsTable from 'components/hrmsTableTemplate'
import hrmsSearchBox from 'components/hrmsSearchBox'
import hrmsSearchBoxTable from 'components/hrmsSearchBoxTable'
import hrmsDatepicker from 'components/hrmsDatepicker'
import hrmsTimepicker from 'components/hrmsTimepicker'
import hrmsLovCodcomp from 'components/hrmsLovCodcomp'
import hrmsLov from 'components/hrmsLov'
import hrmsStaappr from 'components/hrmsStaappr'
import hrmsDropdown from 'components/hrmsDropdown'
import hrmsDropdownMonth from 'components/hrmsDropdownMonth'
import hrmsDropdownYear from 'components/hrmsDropdownYear'
import hrmsInfiniteLoading from 'vue-infinite-loading'
import hrmsDropzone from 'components/hrmsDropzone'
import hrmsLogicalStatement from 'components/hrmsLogicalStatement'
import hrmsChangeLabel from 'components/hrmsChangeLabel'
import hcmLovCodcomp from 'components/hcmLovCodcomp'
import hcmSelect from 'components/hcmSelect'
import hcmHral5ou from 'components/hcmHral5ou'
import hcmHral4ke from 'components/hcmHral4ke'
import hcmHrsc11x from 'components/hcmHrsc11x'
import hcmCalculator from 'components/hcmCalculator'
import hcmLov from 'components/hcmLov'
import hcmSettingPage from 'components/hcmSettingPage'
import hcmBanner from 'components/hcmBanner'
import higherOrderComponent from 'components/higherOrderComponent'
import * as VueGoogleMaps from 'vue2-google-maps'

const assetsLabels = require('assets/js/labels')
let _ = require('lodash')

// overrided _.isEmpty for check isEmpty of object with command _.isEmpty(obj, true)
_.mixin(function () {
  var _isEmpty = _.isEmpty
  return {
    isEmpty: function (value, defined) {
      if (defined && _.isObject(value)) {
        return !_.some(value, function (value, key) {
          return !_isEmpty(value)
        })
      }
      return _isEmpty(value)
    }
  }
}())

VueTouch.config.swipe = {
  threshold: 200
}
VueTouch.registerCustomEvent('doubletap', {
  type: 'tap',
  taps: 2
})

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(VeeValidate)
Vue.use(VueTouch, { name: 'v-touch' })
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDnlb02M1OwgsGNoNM2e0c53vkSxk-Q7ro',
    libraries: 'places'
  }
})
Vue.component('simplert', simplert)
Vue.component('hrms-table', hrmsTable)
Vue.component('hrms-search-box', hrmsSearchBox)
Vue.component('hrms-search-box-table', hrmsSearchBoxTable)
Vue.component('hrms-datepicker', hrmsDatepicker)
Vue.component('hrms-timepicker', hrmsTimepicker)
Vue.component('hrms-lov-codcomp', hrmsLovCodcomp)
Vue.component('hrms-lov', hrmsLov)
Vue.component('hrms-staappr', hrmsStaappr)
Vue.component('hrms-dropdown', hrmsDropdown)
Vue.component('hrms-dropdown-month', hrmsDropdownMonth)
Vue.component('hrms-dropdown-year', hrmsDropdownYear)
Vue.component('hrms-infinite-loading', hrmsInfiniteLoading)
Vue.component('hrms-dropzone', hrmsDropzone)
Vue.component('hrms-logical-statement', hrmsLogicalStatement)
Vue.component('hrms-change-label', hrmsChangeLabel)
Vue.component('hcm-lov-codcomp', hcmLovCodcomp)
Vue.component('hcm-select', hcmSelect)
Vue.component('hcm-hral5ou', hcmHral5ou)
Vue.component('hcm-hral4ke', hcmHral4ke)
Vue.component('hcm-hrsc11x', hcmHrsc11x)
Vue.component('hcm-calculator', hcmCalculator)
Vue.component('hcm-lov', hcmLov)
Vue.component('hcm-setting-page', hcmSettingPage)
Vue.component('hcm-banner', hcmBanner)
Vue.component('higher-order-component', higherOrderComponent)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})

if (store.getters[types.GET_LANG] !== window.localStorage.getItem('lang')) {
  if (window.localStorage.getItem('lang')) {
    store.commit(types.SET_LANG, window.localStorage.getItem('lang'))
  } else {
    store.commit(types.SET_LANG, assetsLabels.defaultLang)
  }
}

if (JSON.parse(window.localStorage.getItem('currentUser')) === null) {
  window.localStorage.setItem('currentUser', JSON.stringify({}))
}

if (store.getters[types.GET_CURRENT_USER] !== JSON.parse(window.localStorage.getItem('currentUser'))) {
  store.commit(types.SET_CURRENT_USER, JSON.parse(window.localStorage.getItem('currentUser')))
}
