import VeeValidate from 'assets/js/vee-validate'
import moment from 'moment'

function requiredWhenExist (textAnyFields, flgRequired, textWhenExistFields, flgWhenExist, flgExist = true) {
  if (validateWhenExist(textWhenExistFields.split('-'), flgWhenExist, flgExist)) {
    return validateRequired(textAnyFields.split('-'), flgRequired)
  } else {
    return true
  }
}

function validateWhenExist (targetFields, flg, flgExist) {
  let valid
  if (flg === 'all') {
    valid = true
  } else if (flg === 'any') {
    valid = false
  }
  for (let targetField of targetFields) {
    let field = targetField ? document.querySelector(`input[name='${targetField}']`) : null
    if (field === null) {
      field = document.querySelector(`textarea[name='${targetField}']`)
    }
    if (field === null) {
      field = document.querySelector(`select[name='${targetField}']`)
    }
    if (field !== null) {
      let validValue = (field.value !== '' && field.value !== '000000000000000000000')
      if (flg === 'all') {
        valid &= (flgExist) ? validValue : !validValue
      } else if (flg === 'any') {
        valid |= (flgExist) ? validValue : !validValue
      }
    }
  }
  return valid
}

function validateRequired (targetFields, flg) {
  let valid = true
  for (let targetField of targetFields) {
    let field = targetField ? document.querySelector(`input[name='${targetField}']`) : null
    if (field === null) {
      field = document.querySelector(`textarea[name='${targetField}']`)
    }
    if (field === null) {
      field = document.querySelector(`select[name='${targetField}']`)
    }
    if (field !== null) {
      if (flg === 'all') {
        valid &= (field.value !== '')
      } else if (flg === 'any') {
        valid = false
        if (field.value !== '') {
          return true
        }
      }
    }
  }
  return valid
}

VeeValidate.Validator.extend('requiredWhenExist', {
  getMessage: field => 'This field is required.',
  validate: (value, [targetField]) => {
    let field = targetField ? document.querySelector(`input[name='${targetField}']`) : null
    if (field === null) {
      field = document.querySelector(`textarea[name='${targetField}']`)
    }
    if (field === null) {
      field = document.querySelector(`select[name='${targetField}']`)
    }
    if (field !== null) {
      return !(value === '' && (field.value !== '' && field.value !== '000000000000000000000'))
    }
    return true
  }
})
VeeValidate.Validator.extend('requiredWhenNotExist', {
  getMessage: field => 'This field is required.',
  validate: (value, targetFields) => {
    let valid = false
    for (var targetField of targetFields) {
      let field = targetField ? document.querySelector(`input[name='${targetField}']`) : null
      if (field === null) {
        field = document.querySelector(`textarea[name='${targetField}']`)
      }
      if (field === null) {
        field = document.querySelector(`select[name='${targetField}']`)
      }
      if (field !== null) {
        if (!(value === '' && (field.value === '' || field.value === '000000000000000000000'))) {
          valid = true
        }
      } else {
        valid = true
      }
    }
    return valid
  }
})
VeeValidate.Validator.extend('requiredAny', {
  getMessage: (field, req) => 'This field is required any.',
  validate: (value, targetFields) => {
    for (var targetField of targetFields) {
      let field = targetField ? document.querySelector(`input[name='${targetField}']`) : null
      if (field === null) {
        field = document.querySelector(`textarea[name='${targetField}']`)
      }
      if (field === null) {
        field = document.querySelector(`select[name='${targetField}']`)
      }
      if (field !== null) {
        if (field.value !== '') {
          return true
        }
      }
    }
    return false
  }
})
VeeValidate.Validator.extend('requiredAllWhenExistAll', {
  getMessage: (field, req) => 'This field is required any.',
  validate: (value, [ textAnyFields, textWhenExistFields ]) => {
    return requiredWhenExist(textAnyFields, 'all', textWhenExistFields, 'all')
  }
})
VeeValidate.Validator.extend('requiredAllWhenExistAny', {
  getMessage: (field, req) => 'This field is required any.',
  validate: (value, [ textAnyFields, textWhenExistFields ]) => {
    return requiredWhenExist(textAnyFields, 'all', textWhenExistFields, 'any')
  }
})
VeeValidate.Validator.extend('requiredAllWhenNotExistAll', {
  getMessage: (field, req) => 'This field is required any.',
  validate: (value, [ textAnyFields, textWhenExistFields ]) => {
    return requiredWhenExist(textAnyFields, 'all', textWhenExistFields, 'all', false)
  }
})
VeeValidate.Validator.extend('requiredAllWhenNotExistAny', {
  getMessage: (field, req) => 'This field is required any.',
  validate: (value, [ textAnyFields, textWhenExistFields ]) => {
    return requiredWhenExist(textAnyFields, 'all', textWhenExistFields, 'any', false)
  }
})
VeeValidate.Validator.extend('requiredAnyWhenExistAll', {
  getMessage: (field, req) => 'This field is required any.',
  validate: (value, [ textAnyFields, textWhenExistFields ]) => {
    return requiredWhenExist(textAnyFields, 'any', textWhenExistFields, 'all')
  }
})
VeeValidate.Validator.extend('requiredAnyWhenExistAny', {
  getMessage: (field, req) => 'This field is required any.',
  validate: (value, [ textAnyFields, textWhenExistFields ]) => {
    return requiredWhenExist(textAnyFields, 'any', textWhenExistFields, 'any')
  }
})
VeeValidate.Validator.extend('requiredAnyWhenNotExistAll', {
  getMessage: (field, req) => 'This field is required any.',
  validate: (value, [ textAnyFields, textWhenExistFields ]) => {
    return requiredWhenExist(textAnyFields, 'any', textWhenExistFields, 'all', false)
  }
})
VeeValidate.Validator.extend('requiredAnyWhenNotExistAny', {
  getMessage: (field, req) => 'This field is required any.',
  validate: (value, [ textAnyFields, textWhenExistFields ]) => {
    return requiredWhenExist(textAnyFields, 'any', textWhenExistFields, 'any', false)
  }
})
VeeValidate.Validator.installDateTimeValidators(moment)

export default VeeValidate
