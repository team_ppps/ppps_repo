import Vue from 'vue'
import Vuex from 'vuex'

let loadStoreModule = (moduleName) => {
  return require('modules/' + moduleName + '/stores')
}

let storeModules = require('modules/stores')

let modules = Object.assign({}, require('components/stores'))
for (let storeModule of storeModules) {
  modules = Object.assign(modules, loadStoreModule(storeModule))
}
Vue.use(Vuex)

export default new Vuex.Store({
  modules: modules
})
