import VueRouter from 'vue-router'
import * as types from 'modules/main/login/store/mutation-types'

const config = require('config/index')

let lazyLoad = (moduleLoad) => { return resolve => require([ '@/modules/' + moduleLoad.moduleName + '/' + moduleLoad.functionName + '/index.vue' ], resolve) }
let load = (moduleLoad) => { return require('@/modules/' + moduleLoad.moduleName + '/' + moduleLoad.functionName + '/index.vue') }
let moduleConfig = (moduleName, functionName, options = { requiresAuth: false, scrollToTop: true, routeKnowledge: false }) => {
  let moduleLoad = { moduleName: moduleName, functionName: functionName }
  return { path: functionName, name: functionName, component: lazyLoad(moduleLoad), meta: options }
}

let routeModules = require('modules/routes')

let routeConfigs = []
for (let routeModule of routeModules) {
  for (let route of require('modules/' + routeModule + '/routes')) {
    routeConfigs.push({ module: routeModule, function: route.function, options: route.options || undefined })
  }
}

let childrenRoutes = []
for (let routeConfig of routeConfigs) {
  if (typeof routeConfig.options !== 'undefined') {
    if (typeof routeConfig.options.routeKnowledge !== 'undefined') {
      if (process.env.NODE_ENV !== 'production' && routeConfig.options.routeKnowledge) {
        childrenRoutes.push(moduleConfig(routeConfig.module, routeConfig.function, routeConfig.options))
      }
    } else {
      childrenRoutes.push(moduleConfig(routeConfig.module, routeConfig.function, routeConfig.options))
    }
  } else {
    childrenRoutes.push(moduleConfig(routeConfig.module, routeConfig.function, routeConfig.options))
  }
}

const routes = [
  {
    path: '/',
    redirect: { name: 'home' }
  }, {
    path: '/autologin/:runno?',
    name: 'autologin',
    component: lazyLoad({ moduleName: 'main', functionName: 'autologin' })
  }, {
    path: '/login/:codapp?',
    name: 'login',
    component: lazyLoad({ moduleName: 'main', functionName: 'login' })
  }, {
    path: '/',
    name: 'template',
    component: load({ moduleName: 'main', functionName: 'template' }),
    meta: { requiresAuth: true },
    children: childrenRoutes
  }, {
    path: '/uiKit',
    name: 'uiKit',
    component: lazyLoad({ moduleName: 'knowledge', functionName: 'uiKit' })
  }, {
    path: '/demo',
    name: 'demo',
    component: lazyLoad({ moduleName: 'knowledge', functionName: 'demo' })
  }, {
    // not found handler
    path: '*',
    name: '404',
    component: lazyLoad({ moduleName: 'main', functionName: '404' })
  }
]

const router = new VueRouter({
  base: config.build.assetsPublicPath,
  routes: routes,
  mode: 'history',
  scrollBehavior: function (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      const position = {}
      if (to.hash) {
        position.selector = to.hash
      }
      if (to.matched.some(record => record.meta.scrollToTop)) {
        position.x = 0
        position.y = 0
      }
      return position
    }
  }
})

router.beforeEach((to, from, next) => {
  let codapp = to.name
  if (codapp === 'demo' || codapp === 'uiKit') {
    next()
  }
  let store = require('register/stores').default
  if (store.getters[types.GET_CURRENT_USER] !== JSON.parse(window.localStorage.getItem('currentUser'))) {
    store.commit(types.SET_CURRENT_USER, JSON.parse(window.localStorage.getItem('currentUser')))
  }
  var baseURL = (process.env.NODE_ENV !== 'development') ? window.location.origin + '/' : config.build.defaultBaseURL
  if (window.localStorage.getItem('baseURL') !== baseURL) {
    window.localStorage.setItem('baseURL', baseURL)
  }

  const token = JSON.parse(window.localStorage.getItem('token')) || {}
  const refreshToken = token.refresh_token
  if (refreshToken) {
    let storeToken = store.getters[types.GET_TOKEN]
    if (storeToken.access_token === '') { // has not access token
      store.dispatch(types.RECEIVED_TOKEN, { refreshToken, codapp, next })
    } else { // has access token
      if (codapp === 'login') {
        next('home')
      } else {
        if (router.match(codapp).name === '404') {
          next()
        } else {
          if (to.matched.some(record => record.meta.routeKnowledge)) {
            next()
          } else {
            next()
            // store.dispatch(types.POST_AUTH_VUE_MIDDLEWARE, { codapp, next })
          }
        }
      }
    }
  } else {
    if (codapp === 'login') {
      next()
    } else {
      next('login')
    }
  }
})

export { router }
