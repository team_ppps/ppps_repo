import axios from 'axios'
import * as types from 'modules/main/login/store/mutation-types'

const config = require('config/index')
if (window.localStorage.getItem('baseURL') === null) {
  var location = window.location
  var baseURL = (process.env.NODE_ENV !== 'development') ? location.origin + '/' : config.build.defaultBaseURL
  window.localStorage.setItem('baseURL', baseURL)
}

export let instance = function () {
  let store = require('register/stores').default
  let currentUser = {
    accessToken: '',
    lang: ''
  }
  if (store) {
    let storeToken = store.getters[types.GET_TOKEN]
    const lang = window.localStorage.getItem('lang')

    let pLang = '102'
    if (lang !== null) {
      switch (lang) {
        case 'en':
          pLang = 'en-EN'
          break
        case 'th':
          pLang = 'th-TH'
          break
        default:
          pLang = 'en-EN'
          break
      }
    }
    currentUser = {
      accessToken: storeToken.access_token,
      lang: pLang
    }
  }

  return axios.create({
    baseURL: window.localStorage.getItem('baseURL'),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json;charset=utf-8',
      'Authorization': 'Bearer ' + currentUser.accessToken,
      'Accept-Language': currentUser.lang
    },
    validateStatus: function (status) {
      return status >= 200 && status < 300
    }
  })
}

export const endpoint = config.build.apiVersion
export const isMock = config.build.mocker
export const isMockLogin = config.build.mockerLogin
// export const isMock = (process.env.NODE_ENV !== 'production' && config.build.mocker)
// export const isMockLogin = (process.env.NODE_ENV !== 'production' && config.build.mockerLogin)

export const generator = (mocker) => {
  if (typeof mocker.status === 'undefined' || typeof mocker.status === 'string' || mocker.status === 1 || (mocker.status >= 200 && mocker.status < 300)) {
    return Promise.resolve({
      request: {
        response: JSON.stringify(mocker)
      }
    })
  } else {
    return Promise.reject({
      response: {
        data: mocker
      }
    })
  }
}
