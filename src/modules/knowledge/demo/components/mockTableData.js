import faker from 'faker'
import moment from 'moment'
import _ from 'lodash'

module.exports = {
  requestLabels: {
    'HRES62EC1': {
      '5': 'วันที่ขอตั้งแต่',
      '7': 'ถึง',
      '10': 'วันที่ขอ',
      '20': 'ลำดับที่',
      '30': 'รหัสการลา',
      '40': 'ช่วงวันที่ขอลา',
      '50': 'สถานะ',
      '60': 'หมายเหตุ',
      '70': 'ผู้อนุมัติล่าสุด',
      '80': 'ผู้อนุมัติลำดับถัดไป',
      '90': 'เลขที่ใบลา',
      '99': 'คำร้องขอลาหยุดงาน'
    },
    'HRES62EC2': {
      '10': 'บันทึกใบลาหยุดงาน',
      '15': 'สิทธิการลาคงเหลือ',
      '20': 'วันที่ขอ',
      '25': 'ลำดับที่',
      '30': 'การลาหยุดงาน',
      '40': 'รายละเอียดการลา',
      '50': 'ตั้งแต่วันที่',
      '60': 'ถึงวันที่',
      '70': 'ตั้งแต่เวลา',
      '80': 'ถึงเวลา',
      '90': 'รายละเอียดเพิ่มเติม',
      '100': 'รายละเอียดการลา',
      '110': 'สิทธการลาคงเหลือ',
      '120': 'แนบไฟล์',
      '130': 'อนุมัติโดยข้ามขั้นตอน',
      '140': 'ผู้บันทึก',
      '150': 'รายละเอียดเวลาปฏิบัติงาน',
      '160': 'รหัสกะ',
      '170': 'เวลาปฎิบัติงาน',
      '180': 'วันที่ลา',
      '200': 'เต็มวัน',
      '210': 'ครึ่งแรก',
      '220': 'ครึ่งหลัง',
      '230': 'กำหนดเอง',
      '250': 'ประเภทการลา',
      '260': 'การนับสิทธิ์การลา',
      '261': 'ต่อครั้ง',
      '262': 'ต่อปี',
      '270': 'วัน',
      '280': 'ชม.',
      '290': 'นาที',
      '300': 'จำนวนวันลาที่มีสิทธิ์',
      '310': 'จำนวนวันลาสะสม',
      '320': 'คงเหลือ',
      '330': 'จำนวนวันที่ขอลาสะสม',
      '340': 'ข้อมูลเพิ่มเติม'
    }
  },
  approveLabels: {
    'HRES63UC1': {
      '0': 'อนุมัติการลาหยุดงาน',
      '10': '[HRES63U] อนุมัติการลาหยุดงาน',
      '20': 'ลำดับที่',
      '30': 'รหัสพนักงาน',
      '40': 'ชื่อ',
      '50': 'หน่วยงาน',
      '60': 'ตำแหน่ง',
      '70': 'วันที่ขอ',
      '80': 'ลำดับที่',
      '90': 'รหัสการลา',
      '100': 'ช่วงวันที่ขอลา',
      '110': 'สถานะ',
      '120': 'ผู้อนุมัติ',
      '130': 'วันที่อนุมัติ',
      '140': 'หมายเหตุการอนุมัติ',
      '150': 'ผู้อนุมัติลำดับถัดไป'
    },
    'HRES63UC2': {
      '0': 'รายละเอียดการอนุมัติ',
      '10': 'รายละเอียดการอนุมัติ',
      '20': 'สำหรับพนักงาน',
      '30': 'ลำดับที่อนุมัติ',
      '40': 'อนุมัติโดย',
      '50': 'วันที่อนุมัติ',
      '60': 'สถานะปัจจุบัน',
      '70': 'สถานะ',
      '80': 'อนุมัติ',
      '90': 'ไม่อนุมัติ',
      '100': 'หมายเหตุ',
      '110': 'รายการที่เลือกทั้งหมด',
      '120': 'รายการ'
    },
    'HRES63UC3': {
      '0': 'รายละเอียดใบลา',
      '10': 'รายละเอียดใบลา',
      '20': 'รหัสการลา',
      '30': 'รายละเอียด',
      '40': 'ตั้งแต่วันที่',
      '50': 'เริ่มเวลา',
      '60': 'ถึงวันที่',
      '70': 'ถึงเวลา',
      '80': 'สิทธิการลาหยุดงาน',
      '90': 'ประวัติการลา',
      '100': 'รหัสพนักงาน',
      '110': 'วันที่ขอ',
      '120': 'ไฟล์เอกสาร',
      '130': 'ผู้บันทึก'
    },
    'HRES63UC4': {
      '0': 'สิทธิการลาหยุดงาน',
      '10': 'สิทธิการลาหยุดงาน',
      '20': 'รหัสพนักงาน',
      '30': 'ประเภทการลา',
      '40': 'การนับสิทธิ์การลา',
      '50': 'ต่อครั้ง',
      '60': 'ต่อปี',
      '70': 'วัน',
      '80': 'ชม.',
      '90': 'นาที',
      '100': 'จำนวนวันลาที่มีสิทธิ์',
      '110': 'จำนวนวันที่ลา',
      '120': 'จำนวนวันคงเหลือ',
      '130': 'จำนวนวันขอลาสะสม'
    },
    'HRES63UC5': {
      '0': 'ประวัติการลา',
      '10': 'ประวัติการลา',
      '20': 'รหัสพนักงาน',
      '30': 'ตั้งแต่วันที่',
      '40': 'ถึงวันที่',
      '50': 'วันที่',
      '60': 'รหัสกะ',
      '70': 'ช่วงเวลาที่ลา',
      '80': 'รหัสการลา',
      '90': 'รายละเอียด',
      '100': 'ชม.การลา',
      '110': '(ชม.:นาที)',
      '120': 'เลขที่ใบลา'
    },
    'HRES63UC6': {
      '10': 'รายชื่อพนักงานที่ขอลาหยุดงาน',
      '20': 'ตั้งแต่วันที่',
      '30': 'ถึงวันที่',
      '40': 'สำหรับหน่วยงาน',
      '50': 'รหัสกะ',
      '60': 'วันที่',
      '70': 'รหัสกะ',
      '80': 'เวลาทำงาน',
      '90': 'ลำดับที่',
      '100': 'รหัสพนักงาน',
      '110': 'ชื่อพนักงาน',
      '120': 'รหัสการลา',
      '130': 'เวลาขอลาหยุด',
      '140': 'ตั้งแต่',
      '150': 'ถึง'
    }
  },
  inlineLabels: {
    'HRES6AEC1': {
      '30': 'วันที่ขอตั้งแต่',
      '40': 'ถึง',
      '70': 'วันที่ขอ',
      '80': 'ลำดับที่',
      '90': 'วันที่ทำงาน',
      '100': 'รหัสกะ',
      '110': 'เวลาเข้า-ออกงานจริง',
      '120': 'รหัสกะ',
      '130': 'เวลาเข้า-ออกงานจริง',
      '140': 'สถานะ',
      '150': 'หมายเหตุ',
      '160': 'ผู้อนุมัติล่าสุด',
      '170': 'ผู้อนุมัติลำดับถัดไป'
    },
    'HRES6AEC2': {
      '10': 'สำหรับพนักงาน',
      '20': 'ตั้งแต่วันที่',
      '30': 'ถึง',
      '40': 'วันที่',
      '50': 'วัน',
      '60': 'กะ',
      '70': 'เวลารูดบัตรเข้า',
      '80': 'วันที่',
      '90': 'เวลา',
      '99': 'คำร้องเรื่อง การขอแก้ไขเวลาเข้า-ออก',
      '100': 'เวลารูดบัตรออก',
      '110': 'วันที่',
      '120': 'เวลา',
      '130': 'รหัสสาเหตุ',
      '140': 'สาเหตุการขอแก้ไข',
      '150': 'รายละเอียด-สถานที่/จังหวัด',
      '160': 'ความผิดปกติ(ชม:นาที)',
      '170': 'สาย',
      '180': 'กลับก่อน',
      '190': 'ขาดงาน',
      '195': 'สถานะ',
      '200': 'รายละเอียดการรูดบัตร'
    },
    'STDATTEN': {
      '0': 'รายละเอียดเวลาปฏิบัติงาน',
      '10': 'รายละเอียดเวลาปฏิบัติงาน',
      '20': 'ชื่อพนักงาน',
      '30': 'วันที่',
      '40': 'วัน',
      '50': 'รหัสกะ',
      '60': 'เวลาปฏิบัติงาน',
      '70': 'เวลารูดบัตรเข้า',
      '80': 'เวลารูดบัตรออก',
      '90': 'เวลารูดบัตร',
      '100': 'ตั้งแต่',
      '110': 'ถึง',
      '120': 'รหัสลูกค้า',
      '130': 'Job Code',
      '140': 'รหัสสาเหตุ',
      '150': 'สาเหตุที่ไปทำงาน',
      '160': 'รายละเอียดงานที่ทำ'
    }
  },
  sortLabels: {
    'HRES62EC1': {
      '5': 'วันที่ขอตั้งแต่',
      '7': 'ถึง',
      '10': 'วันที่ขอ',
      '20': 'ลำดับที่',
      '30': 'รหัสการลา',
      '40': 'ช่วงวันที่ขอลา',
      '50': 'สถานะ',
      '60': 'หมายเหตุ',
      '70': 'ผู้อนุมัติล่าสุด',
      '80': 'ผู้อนุมัติลำดับถัดไป',
      '90': 'เลขที่ใบลา',
      '99': 'คำร้องขอลาหยุดงาน'
    },
    'HRES62EC2': {
      '10': 'บันทึกใบลาหยุดงาน',
      '15': 'สิทธิการลาคงเหลือ',
      '20': 'วันที่ขอ',
      '25': 'ลำดับที่',
      '30': 'การลาหยุดงาน',
      '40': 'รายละเอียดการลา',
      '50': 'ตั้งแต่วันที่',
      '60': 'ถึงวันที่',
      '70': 'ตั้งแต่เวลา',
      '80': 'ถึงเวลา',
      '90': 'รายละเอียดเพิ่มเติม',
      '100': 'รายละเอียดการลา',
      '110': 'สิทธการลาคงเหลือ',
      '120': 'แนบไฟล์',
      '130': 'อนุมัติโดยข้ามขั้นตอน',
      '140': 'ผู้บันทึก',
      '150': 'รายละเอียดเวลาปฏิบัติงาน',
      '160': 'รหัสกะ',
      '170': 'เวลาปฎิบัติงาน',
      '180': 'วันที่ลา',
      '200': 'เต็มวัน',
      '210': 'ครึ่งแรก',
      '220': 'ครึ่งหลัง',
      '230': 'กำหนดเอง',
      '250': 'ประเภทการลา',
      '260': 'การนับสิทธิ์การลา',
      '261': 'ต่อครั้ง',
      '262': 'ต่อปี',
      '270': 'วัน',
      '280': 'ชม.',
      '290': 'นาที',
      '300': 'จำนวนวันลาที่มีสิทธิ์',
      '310': 'จำนวนวันลาสะสม',
      '320': 'คงเหลือ',
      '330': 'จำนวนวันที่ขอลาสะสม',
      '340': 'ข้อมูลเพิ่มเติม'
    }
  },
  dataInline (flgRowCondition) {
    const mockTotal = 10
    let mockRows = []
    const mockStaappr = [ 'P', 'Y', 'N', 'A', 'C' ]
    for (var j = 1; j <= mockTotal; j++) {
      let mockRow = {
        dtework: moment().daysInMonth() + '/' + _.padStart(moment().month() + 1, 2, '0') + '/' + moment().year(),
        typwork: faker.address.stateAbbr(),
        codshift: faker.address.stateAbbr(),
        dtein: moment().daysInMonth() + '/' + _.padStart(moment().month() + 1, 2, '0') + '/' + moment().year(),
        timin: faker.date.past().toString().substr(16, 5),
        dteout: moment().daysInMonth() + '/' + _.padStart(moment().month() + 1, 2, '0') + '/' + moment().year(),
        timout: faker.date.past().toString().substr(16, 5),
        codreqst: faker.random.number().toString().substr(0, 4),
        remark: faker.lorem.word(),
        qtylate: faker.date.past().toString().substr(16, 5),
        qtyearly: faker.date.past().toString().substr(16, 5),
        qtyabsent: faker.date.past().toString().substr(16, 5),
        desc_staappr: faker.lorem.word()
      }
      if (flgRowCondition) {
        mockRow.staappr = mockStaappr[Math.floor(Math.random() * 5)]
      }
      mockRows.push(mockRow)
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  dataRequest (flgRowCondition) {
    const mockTotal = 10
    let mockRows = []
    const mockStaappr = [ 'P', 'Y', 'N', 'A', 'C' ]
    for (var i = 1; i <= mockTotal; i++) {
      let mockRow = {
        dtereq: moment().format('DD/MM/YYYY'),
        seqno: faker.random.number().toString().substr(0, 1),
        numlereq: faker.random.number(),
        desc_codleave: faker.lorem.word(),
        dtestrt: moment().format('DD/MM/YYYY'),
        dteend: moment().format('DD/MM/YYYY'),
        timstrt: faker.date.past().toString().substr(16, 5),
        timend: faker.date.past().toString().substr(16, 5),
        status: faker.lorem.word(),
        remarkap: faker.lorem.sentence(),
        desc_codappr: faker.lorem.word(),
        desc_codempap: faker.lorem.word()
      }
      if (flgRowCondition) {
        mockRow.staappr = mockStaappr[Math.floor(Math.random() * 5)]
      }
      mockRows.push(mockRow)
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  dataSort (flgRowCondition) {
    const mockTotal = 10
    let mockRows = []
    const mockStaappr = [ 'P', 'Y', 'N', 'A', 'C' ]
    for (var i = 1; i <= mockTotal; i++) {
      let mockRow = {
        dtereq: moment(faker.date.past()).format('DD/MM/YYYY'),
        dtetim: moment(faker.date.past()).format('DD/MM/YYYY HH:mm'),
        timstrt: moment(faker.date.past()).format('HH:mm'),
        seqno: faker.random.number().toString().substr(0, 1),
        numlereq: faker.random.number(),
        desc_codleave: faker.lorem.word()
      }
      if (flgRowCondition) {
        mockRow.staappr = mockStaappr[Math.floor(Math.random() * 5)]
      }
      mockRows.push(mockRow)
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  dataApprove (flgRowCondition) {
    const mockTotal = 20
    let mockRows = []
    const mockStaappr = [ 'P', 'Y', 'N', 'A', 'C' ]
    for (var j = 1; j <= mockTotal; j++) {
      let mockRow = {
        codempid: faker.lorem.word(),
        desc_codempid: faker.name.findName(),
        desc_codcomp: faker.lorem.word(),
        desc_codpos: faker.lorem.word(),
        dtereq: moment().format('DD/MM/YYYY'),
        numseq: faker.random.number(),
        desc_codleave: faker.lorem.word(),
        dteperiod: moment().format('DD/MM/YYYY') + ' - ' + moment().format('DD/MM/YYYY'),
        status: faker.lorem.word(),
        desc_codappr: faker.name.findName(),
        deslereq: faker.lorem.word(),
        dteappr: moment().format('DD/MM/YYYY'),
        remark: faker.lorem.words(),
        desc_codempap: faker.name.findName()
      }
      if (flgRowCondition) {
        mockRow.staappr = mockStaappr[Math.floor(Math.random() * 5)]
      }
      mockRows.push(mockRow)
    }
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  columns (labels, type, flgRowCondition) {
    let mockCodreqstDropdown = {}
    let mockKey = ''
    let mockColumns = []
    const totalCodreqstDropdown = 5 + Math.floor(Math.random() * 6)
    for (var i = 1; i < totalCodreqstDropdown; i++) {
      mockKey = _.padStart(faker.lorem.word(), 4, '0')
      mockCodreqstDropdown[mockKey] = faker.lorem.word()
    }
    if (flgRowCondition) {
      mockColumns.push({
        key: 'staappr',
        name: 'Status Code',
        style: 'min-width: 50px;'
      })
    }
    switch (type) {
      case 'display':
        mockColumns.push({
          key: 'codempid',
          name: labels['HRES63UC1']['30'],
          class: 'align-center',
          style: 'min-width: 100px;'
        }, {
          key: 'desc_codempid',
          name: labels['HRES63UC1']['40'],
          style: 'min-width: 200px;'
        }, {
          key: 'dtereq',
          name: labels['HRES63UC1']['70'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'desc_codleave',
          name: labels['HRES63UC1']['90'],
          style: 'min-width: 150px;'
        }, {
          key: 'dteperiod',
          name: labels['HRES63UC1']['100'],
          class: 'align-center',
          style: 'min-width: 500px;'
        }, {
          key: 'deslereq',
          name: labels['HRES63UC3']['30'],
          style: 'min-width: 200px;'
        }, {
          key: 'status',
          name: labels['HRES63UC1']['110'],
          style: 'min-width: 200px;'
        }, {
          key: 'desc_codappr',
          name: labels['HRES63UC1']['120'],
          style: 'min-width: 200px;'
        }, {
          key: 'dteappr',
          name: labels['HRES63UC1']['130'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'remark',
          name: labels['HRES63UC1']['140'],
          style: 'min-width: 200px;'
        }, {
          key: 'desc_codempap',
          name: labels['HRES63UC1']['150'],
          style: 'min-width: 200px;'
        })
        break
      case 'request':
        mockColumns.push({
          key: 'dtereq',
          name: labels['HRES62EC1']['10'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'seqno',
          name: labels['HRES62EC1']['20'],
          class: 'align-center',
          style: 'min-width: 80px;'
        }, {
          key: 'numlereq',
          name: labels['HRES62EC1']['90'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'desc_codleave',
          name: labels['HRES62EC1']['30'],
          style: 'min-width: 150px;'
        }, {
          key: 'dtetimperiod',
          name: labels['HRES62EC1']['40'],
          style: 'min-width: 400px;',
          headerRow: 1,
          colspan: 4,
          rowspan: 1
        }, {
          key: 'dtestrt',
          name: labels['HRES62EC2']['50'],
          class: 'align-center',
          style: 'min-width: 100px;',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'dteend',
          name: labels['HRES62EC2']['60'],
          class: 'align-center',
          style: 'min-width: 100px;',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timstrt',
          name: labels['HRES62EC2']['70'],
          class: 'align-center',
          style: 'min-width: 100px;',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timend',
          name: labels['HRES62EC2']['80'],
          class: 'align-center',
          style: 'min-width: 100px;',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'status',
          name: labels['HRES62EC1']['50'],
          style: 'min-width: 200px;'
        }, {
          key: 'remarkap',
          name: labels['HRES62EC1']['60'],
          style: 'min-width: 200px;'
        }, {
          key: 'desc_codappr',
          name: labels['HRES62EC1']['70'],
          style: 'min-width: 200px;'
        }, {
          key: 'desc_codempap',
          name: labels['HRES62EC1']['80'],
          style: 'min-width: 200px;'
        })
        break
      case 'approve':
        mockColumns.push({
          key: 'codempid',
          name: labels['HRES63UC1']['30'],
          class: 'align-center',
          style: 'min-width: 100px;'
        }, {
          key: 'desc_codempid',
          name: labels['HRES63UC1']['40'],
          style: 'min-width: 200px;'
        }, {
          key: 'dtereq',
          name: labels['HRES63UC1']['70'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'desc_codleave',
          name: labels['HRES63UC1']['90'],
          style: 'min-width: 150px;'
        }, {
          key: 'dteperiod',
          name: labels['HRES63UC1']['100'],
          class: 'align-center',
          style: 'min-width: 500px;'
        }, {
          key: 'deslereq',
          name: labels['HRES63UC3']['30'],
          style: 'min-width: 200px;'
        }, {
          key: 'status',
          name: labels['HRES63UC1']['110'],
          style: 'min-width: 200px;'
        }, {
          key: 'desc_codappr',
          name: labels['HRES63UC1']['120'],
          style: 'min-width: 200px;'
        }, {
          key: 'dteappr',
          name: labels['HRES63UC1']['130'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'remark',
          name: labels['HRES63UC1']['140'],
          style: 'min-width: 200px;'
        }, {
          key: 'desc_codempap',
          name: labels['HRES63UC1']['150'],
          style: 'min-width: 200px;'
        })
        break
      case 'columnClassCondition':
        mockColumns.push({
          key: 'codempid',
          name: labels['HRES63UC1']['30'],
          class: 'align-center',
          style: 'min-width: 100px;'
        }, {
          key: 'desc_codempid',
          name: labels['HRES63UC1']['40'],
          style: 'min-width: 200px;',
          columnClassCondition (dataRow, key, index) {
            if (index % 3 === 0) {
              return 'bg-color-skin'
            } else if (index % 4 === 0) {
              return 'bg-red'
            } else if (index % 5 === 0) {
              return 'bg-green'
            } else {
              return ''
            }
          }
        }, {
          key: 'dtereq',
          name: labels['HRES63UC1']['70'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'desc_codleave',
          name: labels['HRES63UC1']['90'],
          style: 'min-width: 150px;'
        }, {
          key: 'dteperiod',
          name: labels['HRES63UC1']['100'],
          class: 'align-center',
          style: 'min-width: 500px;'
        }, {
          key: 'deslereq',
          name: labels['HRES63UC3']['30'],
          style: 'min-width: 200px;'
        }, {
          key: 'status',
          name: labels['HRES63UC1']['110'],
          style: 'min-width: 200px;'
        }, {
          key: 'desc_codappr',
          name: labels['HRES63UC1']['120'],
          style: 'min-width: 200px;'
        }, {
          key: 'dteappr',
          name: labels['HRES63UC1']['130'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'remark',
          name: labels['HRES63UC1']['140'],
          style: 'min-width: 200px;'
        }, {
          key: 'desc_codempap',
          name: labels['HRES63UC1']['150'],
          style: 'min-width: 200px;'
        })
        break
      case 'add':
        mockColumns.push({
          key: 'dtework',
          name: labels['HRES6AEC2']['40'],
          class: 'align-center',
          style: 'min-width: 120px;',
          inlineType: 'text'
        }, {
          key: 'typwork',
          name: labels['HRES6AEC2']['50'],
          class: 'align-center',
          style: 'min-width: 50px;',
          inlineType: 'text'
        }, {
          key: 'codshift',
          name: labels['HRES6AEC2']['60'],
          class: 'align-center',
          style: 'min-width: 50px;'
        }, {
          key: 'hTimeIn',
          name: labels['HRES6AEC2']['70'],
          class: 'align-center',
          style: 'min-width: 250px;',
          headerRow: 1,
          colspan: 2,
          rowspan: 1
        }, {
          key: 'dtein',
          name: labels['HRES6AEC2']['80'],
          class: 'align-center',
          inlineType: 'date',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timin',
          name: labels['HRES6AEC2']['90'],
          class: 'align-center',
          inlineType: 'time',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'hTimeOut',
          name: labels['HRES6AEC2']['100'],
          class: 'align-center',
          style: 'min-width: 250px;',
          headerRow: 1,
          colspan: 2,
          rowspan: 1
        }, {
          key: 'dteout',
          name: labels['HRES6AEC2']['110'],
          class: 'align-center',
          inlineType: 'date',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timout',
          name: labels['HRES6AEC2']['120'],
          class: 'align-center',
          inlineType: 'time',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'codreqst',
          name: labels['HRES6AEC2']['130'],
          inlineType: 'select',
          inlineDropdown: mockCodreqstDropdown,
          style: 'min-width: 100px;'
        }, {
          key: 'remark',
          name: labels['HRES6AEC2']['150'],
          class: 'align-center',
          inlineType: 'textarea',
          style: 'min-width: 200px;'
        }, {
          key: 'hQty',
          name: labels['HRES6AEC2']['160'],
          class: 'align-center text-red',
          style: 'min-width: 300px;',
          headerRow: 1,
          colspan: 3,
          rowspan: 1
        }, {
          key: 'qtylate',
          name: labels['HRES6AEC2']['170'],
          class: 'align-center text-red',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'qtyearly',
          name: labels['HRES6AEC2']['180'],
          class: 'align-center text-red',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'qtyabsent',
          name: labels['HRES6AEC2']['190'],
          class: 'align-center',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'desc_staappr',
          name: labels['HRES6AEC2']['195'],
          style: 'min-width: 200px;'
        })
        break
      case 'edit':
        mockColumns.push({
          key: 'dtework',
          name: labels['HRES6AEC2']['40'],
          class: 'align-center',
          style: 'min-width: 120px;',
          inlineType: 'text'
        }, {
          key: 'typwork',
          name: labels['HRES6AEC2']['50'],
          class: 'align-center',
          style: 'min-width: 50px;',
          inlineType: 'text'
        }, {
          key: 'codshift',
          name: labels['HRES6AEC2']['60'],
          class: 'align-center',
          style: 'min-width: 50px;'
        }, {
          key: 'hTimeIn',
          name: labels['HRES6AEC2']['70'],
          class: 'align-center',
          style: 'min-width: 250px;',
          headerRow: 1,
          colspan: 2,
          rowspan: 1
        }, {
          key: 'dtein',
          name: labels['HRES6AEC2']['80'],
          class: 'align-center',
          inlineType: 'date',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timin',
          name: labels['HRES6AEC2']['90'],
          class: 'align-center',
          inlineType: 'time',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'hTimeOut',
          name: labels['HRES6AEC2']['100'],
          class: 'align-center',
          style: 'min-width: 250px;',
          headerRow: 1,
          colspan: 2,
          rowspan: 1
        }, {
          key: 'dteout',
          name: labels['HRES6AEC2']['110'],
          class: 'align-center',
          inlineType: 'date',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timout',
          name: labels['HRES6AEC2']['120'],
          class: 'align-center',
          inlineType: 'time',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'codreqst',
          name: labels['HRES6AEC2']['130'],
          inlineType: 'select',
          inlineDropdown: mockCodreqstDropdown,
          style: 'min-width: 100px;'
        }, {
          key: 'remark',
          name: labels['HRES6AEC2']['150'],
          class: 'align-center',
          inlineType: 'textarea',
          style: 'min-width: 200px;'
        }, {
          key: 'hQty',
          name: labels['HRES6AEC2']['160'],
          class: 'align-center text-red',
          style: 'min-width: 300px;',
          headerRow: 1,
          colspan: 3,
          rowspan: 1
        }, {
          key: 'qtylate',
          name: labels['HRES6AEC2']['170'],
          class: 'align-center text-red',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'qtyearly',
          name: labels['HRES6AEC2']['180'],
          class: 'align-center text-red',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'qtyabsent',
          name: labels['HRES6AEC2']['190'],
          class: 'align-center',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'desc_staappr',
          name: labels['HRES6AEC2']['195'],
          style: 'min-width: 200px;'
        })
        break
      case 'delete':
        mockColumns.push({
          key: 'dtework',
          name: labels['HRES6AEC2']['40'],
          class: 'align-center',
          style: 'min-width: 120px;',
          inlineType: 'text'
        }, {
          key: 'typwork',
          name: labels['HRES6AEC2']['50'],
          class: 'align-center',
          style: 'min-width: 50px;',
          inlineType: 'text'
        }, {
          key: 'codshift',
          name: labels['HRES6AEC2']['60'],
          class: 'align-center',
          style: 'min-width: 50px;'
        }, {
          key: 'hTimeIn',
          name: labels['HRES6AEC2']['70'],
          class: 'align-center',
          style: 'min-width: 250px;',
          headerRow: 1,
          colspan: 2,
          rowspan: 1
        }, {
          key: 'dtein',
          name: labels['HRES6AEC2']['80'],
          class: 'align-center',
          inlineType: 'date',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timin',
          name: labels['HRES6AEC2']['90'],
          class: 'align-center',
          inlineType: 'time',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'hTimeOut',
          name: labels['HRES6AEC2']['100'],
          class: 'align-center',
          style: 'min-width: 250px;',
          headerRow: 1,
          colspan: 2,
          rowspan: 1
        }, {
          key: 'dteout',
          name: labels['HRES6AEC2']['110'],
          class: 'align-center',
          inlineType: 'date',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timout',
          name: labels['HRES6AEC2']['120'],
          class: 'align-center',
          inlineType: 'time',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'codreqst',
          name: labels['HRES6AEC2']['130'],
          inlineType: 'select',
          inlineDropdown: mockCodreqstDropdown,
          style: 'min-width: 100px;'
        }, {
          key: 'remark',
          name: labels['HRES6AEC2']['150'],
          class: 'align-center',
          inlineType: 'textarea',
          style: 'min-width: 200px;'
        }, {
          key: 'hQty',
          name: labels['HRES6AEC2']['160'],
          class: 'align-center text-red',
          style: 'min-width: 300px;',
          headerRow: 1,
          colspan: 3,
          rowspan: 1
        }, {
          key: 'qtylate',
          name: labels['HRES6AEC2']['170'],
          class: 'align-center text-red',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'qtyearly',
          name: labels['HRES6AEC2']['180'],
          class: 'align-center text-red',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'qtyabsent',
          name: labels['HRES6AEC2']['190'],
          class: 'align-center',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'desc_staappr',
          name: labels['HRES6AEC2']['195'],
          style: 'min-width: 200px;'
        })
        break
      case 'validate':
        mockColumns.push({
          key: 'dtework',
          name: labels['HRES6AEC2']['40'],
          class: 'align-center',
          style: 'min-width: 120px;',
          inlineType: 'text',
          validateRules: 'required|date_format:DD/MM/YYYY'
        }, {
          key: 'typwork',
          name: labels['HRES6AEC2']['50'],
          class: 'align-center',
          style: 'min-width: 50px;',
          inlineType: 'text',
          validateRules: 'requiredWhenExist:dtework'
        }, {
          key: 'codshift',
          name: labels['HRES6AEC2']['60'],
          class: 'align-center',
          style: 'min-width: 50px;'
        }, {
          key: 'hTimeIn',
          name: labels['HRES6AEC2']['70'],
          class: 'align-center',
          style: 'min-width: 250px;',
          headerRow: 1,
          colspan: 2,
          rowspan: 1
        }, {
          key: 'dtein',
          name: labels['HRES6AEC2']['80'],
          class: 'align-center',
          inlineType: 'date',
          validateRules: 'required|date_format:DD/MM/YYYY',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timin',
          name: labels['HRES6AEC2']['90'],
          class: 'align-center',
          inlineType: 'time',
          validateRules: 'required|date_format:HH:mm',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'hTimeOut',
          name: labels['HRES6AEC2']['100'],
          class: 'align-center',
          style: 'min-width: 250px;',
          headerRow: 1,
          colspan: 2,
          rowspan: 1
        }, {
          key: 'dteout',
          name: labels['HRES6AEC2']['110'],
          class: 'align-center',
          inlineType: 'date',
          validateRules: 'required|date_format:DD/MM/YYYY|after:dtein',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timout',
          name: labels['HRES6AEC2']['120'],
          class: 'align-center',
          inlineType: 'time',
          validateRules: 'required|date_format:HH:mm',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'codreqst',
          name: labels['HRES6AEC2']['130'],
          inlineType: 'select',
          inlineDropdown: mockCodreqstDropdown,
          style: 'min-width: 100px;'
        }, {
          key: 'remark',
          name: labels['HRES6AEC2']['150'],
          class: 'align-center',
          inlineType: 'textarea',
          style: 'min-width: 200px;'
        }, {
          key: 'hQty',
          name: labels['HRES6AEC2']['160'],
          class: 'align-center text-red',
          style: 'min-width: 300px;',
          headerRow: 1,
          colspan: 3,
          rowspan: 1
        }, {
          key: 'qtylate',
          name: labels['HRES6AEC2']['170'],
          class: 'align-center text-red',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'qtyearly',
          name: labels['HRES6AEC2']['180'],
          class: 'align-center text-red',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'qtyabsent',
          name: labels['HRES6AEC2']['190'],
          class: 'align-center',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'desc_staappr',
          name: labels['HRES6AEC2']['195'],
          style: 'min-width: 200px;'
        })
        break
      case 'all':
        mockColumns.push({
          key: 'dtework',
          name: labels['HRES6AEC2']['40'],
          class: 'align-center',
          style: 'min-width: 120px;',
          inlineType: 'text',
          validateRules: 'required|date_format:DD/MM/YYYY',
          eventName: 'dtework'
        }, {
          key: 'typwork',
          name: labels['HRES6AEC2']['50'],
          class: 'align-center',
          style: 'min-width: 50px;',
          inlineType: 'text',
          validateRules: 'requiredWhenExist:dtework'
        }, {
          key: 'codshift',
          name: labels['HRES6AEC2']['60'],
          class: 'align-center',
          style: 'min-width: 50px;'
        }, {
          key: 'hTimeIn',
          name: labels['HRES6AEC2']['70'],
          class: 'align-center',
          style: 'min-width: 250px;',
          headerRow: 1,
          colspan: 2,
          rowspan: 1
        }, {
          key: 'dtein',
          name: labels['HRES6AEC2']['80'],
          class: 'align-center',
          inlineType: 'date',
          validateRules: 'required|date_format:DD/MM/YYYY',
          eventName: 'dtein',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timin',
          name: labels['HRES6AEC2']['90'],
          class: 'align-center',
          inlineType: 'time',
          validateRules: 'required|date_format:HH:mm',
          eventName: 'timin',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'hTimeOut',
          name: labels['HRES6AEC2']['100'],
          class: 'align-center',
          style: 'min-width: 250px;',
          headerRow: 1,
          colspan: 2,
          rowspan: 1
        }, {
          key: 'dteout',
          name: labels['HRES6AEC2']['110'],
          class: 'align-center',
          inlineType: 'date',
          validateRules: 'required|date_format:DD/MM/YYYY|after:dtein',
          eventName: 'dteout',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'timout',
          name: labels['HRES6AEC2']['120'],
          class: 'align-center',
          inlineType: 'time',
          validateRules: 'required|date_format:HH:mm',
          eventName: 'timout',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'codreqst',
          name: labels['HRES6AEC2']['130'],
          inlineType: 'select',
          inlineDropdown: mockCodreqstDropdown,
          eventName: 'codreqst',
          style: 'min-width: 100px;'
        }, {
          key: 'remark',
          name: labels['HRES6AEC2']['150'],
          class: 'align-center',
          inlineType: 'textarea',
          style: 'min-width: 200px;'
        }, {
          key: 'hQty',
          name: labels['HRES6AEC2']['160'],
          class: 'align-center text-red',
          style: 'min-width: 300px;',
          headerRow: 1,
          colspan: 3,
          rowspan: 1
        }, {
          key: 'qtylate',
          name: labels['HRES6AEC2']['170'],
          class: 'align-center text-red',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'qtyearly',
          name: labels['HRES6AEC2']['180'],
          class: 'align-center text-red',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'qtyabsent',
          name: labels['HRES6AEC2']['190'],
          class: 'align-center',
          headerRow: 2,
          colspan: 1,
          rowspan: 1
        }, {
          key: 'desc_staappr',
          name: labels['HRES6AEC2']['195'],
          style: 'min-width: 200px;'
        })
        break
      case 'sort':
        mockColumns.push({
          key: 'dtereq',
          name: labels['HRES62EC1']['10'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'dtetim',
          name: labels['HRES62EC1']['10'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'timstrt',
          name: labels['HRES62EC2']['70'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'seqno',
          name: labels['HRES62EC1']['20'],
          class: 'align-center',
          style: 'min-width: 80px;'
        }, {
          key: 'numlereq',
          name: labels['HRES62EC1']['90'],
          class: 'align-center',
          style: 'min-width: 120px;'
        }, {
          key: 'desc_codleave',
          name: labels['HRES62EC1']['30'],
          style: 'min-width: 150px;'
        })
        break
    }
    return mockColumns
  }
}
