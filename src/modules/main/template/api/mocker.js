import faker from 'faker'
import assetslabels from 'assets/js/labels'

export const mocker = {
  setting () {
    return {
      mainColor: '#3887C9',
      advanceColor: '',
      switchUser: [{
        name: 'สมชาย เข็มกลัดทองแดง',
        email: 'somchai.k@peopleplus.co.th',
        flgActive: 'Y'
      }, {
        name: 'Administrator',
        email: 'admin@peopleplus.co.th',
        flgActive: 'N'
      }, {
        name: 'C.S.R. People Plus',
        email: 'csr@peopleplus.co.th',
        flgActive: 'N'
      }]
    }
  },
  saveThemeColor () {
    return assetslabels.mockPostMessage
  },
  tinitial () {
    return require('static/tinitial.json')
  },
  tfolder () {
    return {
      total: 0,
      rows: [
        {
          main: '',
          temp: '',
          codapp: '',
          folder: ''
        }
      ]
    }
  },
  modules () {
    return require('static/module.json')
  },
  headerNotiTotal () {
    // const mockArrCodapp = [ 'hres33u', 'hres35u', 'hres37u', 'hres3cu', 'hres63u', 'hres6nu', 'hres6bu', 'hres6eu', 'hres6lu', 'hres96u', 'hres72u', 'hres75u', 'hres78u', 'hres85u', 'hres87u', 'hress3u', 'hres89u', 'hress5u', 'hres6ju', 'hres92u', 'hres94u' ]
    const mockArrCodapp = []
    const mockArrOption1 = [ 'user', 'sitemap', 'file-text-o', 'bullhorn', 'envelope', 'envelope', 'clock-o', 'clock-o', 'clock-o', 'calendar', 'ambulance', 'briefcase', 'btc', 'car', 'envelope', 'btc', 'user-plus', 'user-plus', 'book', 'book', 'book' ]
    const mockArrOption2 = [ 'red', 'purple', 'maroon', 'olive', 'orange', 'blue' ]
    let mockRequest = []
    let totalRequest = 0
    const requestAmount = Math.floor((Math.random() * 20))
    for (var j = 0; j < requestAmount; j++) {
      mockRequest.push({
        key: mockArrCodapp[Math.floor(Math.random() * mockArrCodapp.length)],
        option1: 'fa-' + mockArrOption1[Math.floor(Math.random() * mockArrOption1.length)],
        option2: 'bg-' + mockArrOption2[Math.floor(Math.random() * mockArrOption2.length)],
        values: faker.lorem.sentence().substring(1, 20),
        summary: Math.floor((Math.random() * 100))
      })
      totalRequest += mockRequest[j]['summary']
    }
    return {
      announce: Math.floor(Math.random() * 101) + 10,
      request: Math.floor(Math.random() * 101) + 10,
      approve: {
        total: totalRequest,
        rows: mockRequest
      }
    }
  },
  headerNotiAnnounceTotal () {
    return Math.floor(Math.random() * 101) + 10
  },
  headerNotiAnnounce () {
    let mockAnnounces = []
    const announceAmount = 10
    for (var i = 0; i < announceAmount; i++) {
      mockAnnounces.push({
        codempid: faker.lorem.word(),
        descCodempid: faker.lorem.sentence()
      })
    }
    return {
      total: announceAmount,
      rows: mockAnnounces
    }
  },
  headerNotiRequestTotal () {
    return Math.floor(Math.random() * 101) + 10
  },
  headerNotiRequest () {
    let mockNotis = []
    const notiAmount = 10
    const mockStaappr = [ 'P', 'Y', 'N', 'A' ]
    // const mockArrCodapp = [ 'hres34e', 'hres62e', 'hres36e', 'hres6ae' ]
    const mockArrCodapp = [ '' ]
    for (var j = 0; j < notiAmount; j++) {
      mockNotis.push({
        codapp: mockArrCodapp[Math.floor(Math.random() * mockArrCodapp.length)],
        desccodapp: faker.lorem.sentence(),
        descapp: faker.lorem.sentence(),
        staappr: mockStaappr[Math.floor((Math.random() * 3))],
        detail1: faker.lorem.sentence(),
        detail2: faker.lorem.sentence(),
        dtereq: faker.date.past().toString().substr(4, 12)
      })
    }
    return {
      total: notiAmount,
      rows: mockNotis
    }
  },
  headerNotiApprove () {
    // const mockArrCodapp = [ 'hres33u', 'hres35u', 'hres37u', 'hres3cu', 'hres63u', 'hres6nu', 'hres6bu', 'hres6eu', 'hres6lu', 'hres96u', 'hres72u', 'hres75u', 'hres78u', 'hres85u', 'hres87u', 'hress3u', 'hres89u', 'hress5u', 'hres6ju', 'hres92u', 'hres94u' ]
    const mockArrCodapp = []
    const mockArrOption1 = [ 'user', 'sitemap', 'file-text-o', 'bullhorn', 'envelope', 'envelope', 'clock-o', 'clock-o', 'clock-o', 'calendar', 'ambulance', 'briefcase', 'btc', 'car', 'envelope', 'btc', 'user-plus', 'user-plus', 'book', 'book', 'book' ]
    const mockArrOption2 = [ 'red', 'purple', 'maroon', 'olive', 'orange', 'blue' ]
    let mockRequest = []
    let totalRequest = 0
    const requestAmount = Math.floor((Math.random() * 20))
    for (var j = 0; j < requestAmount; j++) {
      mockRequest.push({
        key: mockArrCodapp[Math.floor(Math.random() * mockArrCodapp.length)],
        option1: 'fa-' + mockArrOption1[Math.floor(Math.random() * mockArrOption1.length)],
        option2: 'bg-' + mockArrOption2[Math.floor(Math.random() * mockArrOption2.length)],
        values: faker.lorem.sentence().substring(1, 20),
        summary: Math.floor((Math.random() * 100))
      })
      totalRequest += mockRequest[j]['summary']
    }
    return {
      total: totalRequest,
      rows: mockRequest
    }
  },
  headerFavourite () {
    return ['HRSC07E', 'HRCO04E', 'HRPM15E', 'HRPMB1E', 'HRPMC2E', 'HRAL92E', 'HRAL9DE', 'HRAL19E', 'HRAL22E', 'HRAL27X', 'HRAL2IX', 'HRAL41E', 'HRAL44X', 'HRAL4KE', 'HRAL5OU', 'HRAL11E', 'HRPY21E', 'HRPY29E', 'HRPY35B', 'HRPY70B', 'HRPY90B', 'HRES31X', 'HRES8AX', 'HRES62E', 'HRES6OE', 'HRAL13E']
  }
}
