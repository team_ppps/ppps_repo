import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  receivedSetting () {
    if (isMock) return generator(mocker.setting())
    return instance().get(endpoint + '/dash/setting')
  },
  receivedTinitial () {
    if (isMock) return generator(mocker.tinitial())
    return instance().get(endpoint + '/dash/tinitial')
  },
  receivedTfolder () {
    if (isMock) return generator(mocker.tfolder())
    return instance().get(endpoint + '/dash/tfolder')
  },
  receivedModules () {
    if (isMock) return generator(mocker.modules())
    return instance().get(endpoint + '/template/menu')
  },
  receivedHeaderNotiTotal ($staappr) {
    if (isMock) return generator(mocker.headerNotiTotal())
    return instance().get(endpoint + '/dash/notiTotal/' + $staappr)
  },
  receivedHeaderNotiAnnounceInfiniteLoading (searchParams) {
    if (isMock) return generator(mocker.headerNotiAnnounce())
    return instance().get(endpoint + '/dash/notiAnnounce', { params: searchParams })
  },
  receivedHeaderNotiRequestInfiniteLoading (staappr, searchParams) {
    if (isMock) return generator(mocker.headerNotiRequest())
    return instance().get(endpoint + '/dash/notiRequest/' + staappr, { params: searchParams })
  },
  receivedHeaderFavourite (searchParams) {
    if (isMock) return generator(mocker.headerFavourite())
    return instance().get(endpoint + '/dash/favourite', { params: searchParams })
  },
  postThemeColor ({paramSave}) {
    if (isMock) return generator(mocker.saveThemeColor())
    return instance().post(endpoint + '/dash/saveThemeColor', paramSave)
  }
}
