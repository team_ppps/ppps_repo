import * as types from './mutation-types'
import template from '../api'
import swal from 'sweetalert'
import _ from 'lodash'

let getAllModuleCodapp = (modules) => {
  let allModules = []
  let allCodapps = []
  Object.keys(modules).map(function (module, moduleIndex) {
    allModules[moduleIndex] = {
      description: modules[module].description
    }
    recursiveCodapp(allCodapps, modules[module].children)
  })
  return {
    allModules: allModules,
    allCodapps: allCodapps
  }
}

let recursiveCodapp = (allCodapps, codapps) => {
  Object.keys(codapps).map(function (codapp, codappIndex) {
    if (codapps[codapp].type === 'function') {
      allCodapps[codapps[codapp].code] = {
        description: codapps[codapp].description
      }
    }
    if (codapps[codapp].children) {
      recursiveCodapp(allCodapps, codapps[codapp].children)
    }
  })
}

export default {
  state: {
    themeColor: '',
    setting: {
      mainColor: '',
      advanceColor: ''
    },
    flgMainColor: false,
    flgAdvanceColor: false,
    sidebarModuleDisp: false,
    controlsidebarDisp: false,
    modules: require('static/module.json'),
    tinitial: require('static/tinitial.json'),
    tfolder: {
      total: 0,
      rows: [
        {
          main: '',
          temp: '',
          codapp: '',
          folder: ''
        }
      ]
    },
    headerNotiAnnounceTotal: 0,
    headerNotiAnnounceInfiniteLoading: {
      rows: [{
        dteeffec: '',
        numseq: '',
        descapp: '',
        subject: '',
        remark: '',
        codappr: '',
        dteappr: ''
      }]
    },
    headerNotiRequestTotal: 0,
    headerNotiRequestInfiniteLoading: {
      rows: [{
        codapp: '',
        desccodapp: '',
        descapp: '',
        staappr: '',
        detail1: '',
        detail2: '',
        dtereq: ''
      }]
    },
    headerNotiApprove: {
      total: '',
      rows: {}
    },
    favouriteList: {},
    headerFavourite: {},
    keyUp: '',
    currModule: '',
    currCodapp: '',
    descCodapp: {},
    currPath: '',
    breadcrumbs: {
      en: [],
      th: []
    },
    allModules: [],
    allCodapps: [],
    modalDisp: false,
    settingThemeColorDisp: false,
    switchUserDisp: false,
    settingLanguageDisp: false
  },
  getters: {
    [types.GET_SIDEBAR_MODULE_DISP] (state) {
      return state.sidebarModuleDisp
    },
    [types.GET_SETTING] (state) {
      return state.setting
    },
    [types.GET_THEME_COLOR] (state) {
      return state.themeColor
    },
    [types.GET_FLG_MAIN_COLOR] (state) {
      return state.flgMainColor
    },
    [types.GET_FLG_ADVANCE_COLOR] (state) {
      return state.flgAdvanceColor
    },
    [types.GET_TINITIAL] (state) {
      return state.tinitial
    },
    [types.GET_TFOLDER] (state) {
      return state.tfolder
    },
    [types.GET_MODULES] (state) {
      return state.modules
    },
    [types.GET_ALL_MODULES] (state) {
      return state.allModules
    },
    [types.GET_ALL_CODAPPS] (state) {
      return state.allCodapps
    },
    [types.GET_HEADER_ANNOUNCE_TOTAL] (state) {
      return state.headerNotiAnnounceTotal
    },
    [types.GET_HEADER_ANNOUNCE_INFINITE_LOADING] (state) {
      return state.headerNotiAnnounceInfiniteLoading
    },
    [types.GET_HEADER_REQUEST_TOTAL] (state) {
      return state.headerNotiRequestTotal
    },
    [types.GET_HEADER_REQUEST_INFINITE_LOADING] (state) {
      return state.headerNotiRequestInfiniteLoading
    },
    [types.GET_HEADER_APPROVE] (state) {
      return state.headerNotiApprove
    },
    [types.GET_HEADER_FAVOURITE] (state) {
      return state.headerFavourite
    },
    [types.GET_FAVOURITE_LIST] (state) {
      return state.favouriteList
    },
    [types.GET_CURR_MODULE] (state) {
      return state.currModule
    },
    [types.GET_CURR_CODAPP] (state) {
      return state.currCodapp
    },
    [types.GET_DESC_CODAPP] (state) {
      return state.descCodapp
    },
    [types.GET_CURR_PATH] (state) {
      return state.currPath
    },
    [types.GET_BREADCRUMBS] (state) {
      return state.breadcrumbs
    },
    [types.GET_MODAL_DISP] (state) {
      return state.modalDisp
    },
    [types.GET_SETTING_THEME_COLOR_DISP] (state) {
      return state.settingThemeColorDisp
    },
    [types.GET_SWITCH_USER_DISP] (state) {
      return state.switchUserDisp
    },
    [types.GET_SETTING_LANGUAGE_DISP] (state) {
      return state.settingLanguageDisp
    }
  },
  mutations: {
    [types.SET_SIDEBAR_MODULE_DISP_TRUE] (state) {
      state.sidebarModuleDisp = true
    },
    [types.SET_SIDEBAR_MODULE_DISP_FALSE] (state) {
      state.sidebarModuleDisp = false
    },
    [types.SET_SETTING] (state, setting) {
      state.setting = setting
    },
    [types.TOGGLE_FLG_COLOR] (state, flg) {
      let flgMainColor = false
      let flgAdvanceColor = false
      switch (flg) {
        case 'main':
          flgMainColor = true
          break
        case 'advance':
          flgAdvanceColor = true
          break
      }
      state.flgMainColor = flgMainColor
      state.flgAdvanceColor = flgAdvanceColor
    },
    [types.SET_FLG_MAIN_COLOR] (state, flgMainColor) {
      state.flgMainColor = flgMainColor
    },
    [types.SET_FLG_ADVANCE_COLOR] (state, flgAdvanceColor) {
      state.flgAdvanceColor = flgAdvanceColor
    },
    [types.SET_TINITIAL] (state, tinitial) {
      state.tinitial = tinitial
    },
    [types.SET_TFOLDER] (state, tfolder) {
      state.tfolder = tfolder
    },
    [types.SET_MODULES] (state, modules) {
      state.modules = modules
    },
    [types.SET_ALL_MODULES_CODAPP] (state) {
      let obj = getAllModuleCodapp(state.modules)
      state.allModules = obj.allModules
      state.allCodapps = obj.allCodapps
    },
    [types.SET_HEADER_ANNOUNCE_TOTAL] (state, headerAnnounceTotal) {
      state.headerNotiAnnounceTotal = headerAnnounceTotal
    },
    [types.SET_HEADER_ANNOUNCE_INFINITE_LOADING] (state, headerAnnounceInfiniteLoading) {
      state.headerNotiAnnounceInfiniteLoading = headerAnnounceInfiniteLoading
    },
    [types.SET_HEADER_REQUEST_TOTAL] (state, headerRequestTotal) {
      state.headerNotiRequestTotal = headerRequestTotal
    },
    [types.SET_HEADER_REQUEST_INFINITE_LOADING] (state, headerRequestInfiniteLoading) {
      state.headerNotiRequestInfiniteLoading = headerRequestInfiniteLoading
    },
    [types.SET_HEADER_APPROVE] (state, headerApprove) {
      state.headerNotiApprove = headerApprove
    },
    [types.SET_HEADER_FAVOURITE] (state) {
      let codapp = []
      state.headerFavourite = {}
      _.each(state.modules, function (module, index) {
        codapp = state.favouriteList.filter((codapp, codappIndex) => {
          return codapp.substring(2, 4).toUpperCase() === index.toUpperCase()
        }).map((codapp, codappIndex) => {
          let description = typeof state.allCodapps[codapp] !== 'undefined' ? state.allCodapps[codapp].description : {}
          return {
            codapp: codapp,
            descCodapp: description
          }
        })
        if (codapp.length > 0) {
          codapp.sort(function (a, b) {
            if (a.codapp < b.codapp) return -1
            return 1
          })

          state.headerFavourite[index] = {
            description: module.description,
            codapp: codapp
          }
        }
      })
    },
    [types.SET_FAVOURITE_LIST] (state, favouriteList) {
      // state.favouriteList = favouriteList

      // <<-- set localStorage
      if (window.localStorage.getItem('favouriteList') === null) {
        state.favouriteList = favouriteList
        window.localStorage.setItem('favouriteList', JSON.stringify(favouriteList))
      } else {
        state.favouriteList = JSON.parse(window.localStorage.getItem('favouriteList'))
      }
      // -->> set localStorage
    },
    [types.SET_TOGGLE_FAVOURITE_LIST_ITEM] (state, codapp) {
      codapp = typeof codapp !== 'undefined' ? codapp.toUpperCase() : ''
      let indexCodapp = state.favouriteList.indexOf(codapp)
      if (indexCodapp > -1) {
        state.favouriteList.splice(indexCodapp, 1)
      } else {
        state.favouriteList.push(codapp)
      }
      window.localStorage.setItem('favouriteList', JSON.stringify(state.favouriteList)) // set localStorage
    },
    [types.SET_CURR_MODULE] (state, module) {
      state.currModule = module
    },
    [types.SET_CURR_PATH] (state, currPath) {
      currPath = currPath.toLowerCase()
      state.currPath = currPath
      let foundCodapp = false
      state.breadcrumbs = {
        en: [],
        th: []
      }
      if (currPath.substr(0, 2) === 'hr') {
        _.each(state.modules, function (module, moduleName) {
          if (foundCodapp === true) return
          if (typeof module.children !== 'undefined') {
            _.each(module.children, function (menuLv1, keyLv1) {
              if (menuLv1.code.toLowerCase() === currPath) {
                foundCodapp = true
                state.currModule = moduleName
                state.currCodapp = currPath
                state.descCodapp.en = menuLv1.description.en
                state.descCodapp.th = menuLv1.description.th
                state.breadcrumbs.en[0] = menuLv1.description.en
                state.breadcrumbs.th[0] = menuLv1.description.th
                return
              }
              if (foundCodapp === true) return
              if (typeof menuLv1.children !== 'undefined') {
                _.each(menuLv1.children, function (menuLv2, keyLv2) {
                  if (menuLv2.code.toLowerCase() === currPath) {
                    foundCodapp = true
                    state.currModule = moduleName
                    state.currCodapp = currPath
                    state.descCodapp.en = menuLv2.description.en
                    state.descCodapp.th = menuLv2.description.th

                    state.breadcrumbs.en[0] = menuLv1.description.en
                    state.breadcrumbs.th[0] = menuLv1.description.th
                    state.breadcrumbs.en[1] = menuLv2.description.en
                    state.breadcrumbs.th[1] = menuLv2.description.th
                    return
                  }
                  if (foundCodapp === true) return
                  if (typeof menuLv2.children !== 'undefined') {
                    _.each(menuLv2.children, function (menuLv3, keyLv3) {
                      if (menuLv3.code.toLowerCase() === currPath) {
                        foundCodapp = true
                        state.currModule = moduleName
                        state.currCodapp = currPath
                        state.descCodapp.en = menuLv3.description.en
                        state.descCodapp.th = menuLv3.description.th

                        state.breadcrumbs.en[0] = menuLv1.description.en
                        state.breadcrumbs.th[0] = menuLv1.description.th
                        state.breadcrumbs.en[1] = menuLv2.description.en
                        state.breadcrumbs.th[1] = menuLv2.description.th
                        state.breadcrumbs.en[2] = menuLv3.description.en
                        state.breadcrumbs.th[2] = menuLv3.description.th
                        return
                      }
                      if (foundCodapp === true) return
                      if (typeof menuLv3.children !== 'undefined') {
                        _.each(menuLv3.children, function (menuLv4, keyLv4) {
                          if (menuLv4.code.toLowerCase() === currPath) {
                            foundCodapp = true
                            state.currModule = moduleName
                            state.currCodapp = currPath
                            state.descCodapp.en = menuLv4.description.en
                            state.descCodapp.th = menuLv4.description.th

                            state.breadcrumbs.en[0] = menuLv1.description.en
                            state.breadcrumbs.th[0] = menuLv1.description.th
                            state.breadcrumbs.en[1] = menuLv2.description.en
                            state.breadcrumbs.th[1] = menuLv2.description.th
                            state.breadcrumbs.en[2] = menuLv3.description.en
                            state.breadcrumbs.th[2] = menuLv3.description.th
                            state.breadcrumbs.en[3] = menuLv4.description.en
                            state.breadcrumbs.th[3] = menuLv4.description.th
                            return
                          }
                        })
                      }
                    })
                  }
                })
              }
            })
          }
        })
      } else {
        state.currModule = ''
        state.currCodapp = ''
        state.descCodapp = {}
      }
    },
    [types.SET_THEME_COLOR] (state, themeColor) {
      state.themeColor = themeColor
    },
    [types.SET_MODAL_DISP] (state, modalDisp) {
      state.modalDisp = modalDisp
    },
    [types.TOGGLE_SETTING_DISP] (state, settingPage) {
      state.settingThemeColorDisp = false
      state.switchUserDisp = false
      state.settingLanguageDisp = false

      let settingThemeColorDisp = false
      let switchUserDisp = false
      let settingLanguageDisp = false
      switch (settingPage) {
        case 'themeColor':
          settingThemeColorDisp = true
          break
        case 'switchUser':
          switchUserDisp = true
          break
        case 'language':
          settingLanguageDisp = true
          break
      }
      state.settingThemeColorDisp = settingThemeColorDisp
      state.switchUserDisp = switchUserDisp
      state.settingLanguageDisp = settingLanguageDisp
    }
  },
  actions: {
    [types.SET_THEME_DEFAULT] ({ commit }, data) {
      let flgToggle
      if (data.mainColor !== '') {
        flgToggle = 'main'
        commit(types.SET_THEME_COLOR, data.mainColor)
      } else if (data.advanceColor !== '') {
        flgToggle = 'advance'
        commit(types.SET_THEME_COLOR, data.advanceColor)
      }
      commit(types.TOGGLE_FLG_COLOR, flgToggle)
    },
    [types.CLEAR_SETTING_THEME] ({ commit, state, dispatch }) {
      dispatch(types.SET_THEME_DEFAULT, state.setting)
    },
    [types.RECEIVED_SETTING] ({ commit, dispatch }) {
      // <<-- for get from database
      // template.receivedSetting()
      //   .then((response) => {
      //     let data = JSON.parse(response.request.response)
      //     commit(types.SET_SETTING, data)
      //     dispatch(types.SET_THEME_DEFAULT, data)
      //   })
      //   .catch((error) => {
      //     const data = error.response.data
      //     swal({title: '', text: data.response, html: true, type: 'error'})
      //   })
      // -->> for get from database

      // <<-- set localStorage
      if (window.localStorage.getItem('settingTheme') === null) {
        template.receivedSetting()
        .then((response) => {
          let data = JSON.parse(response.request.response)
          window.localStorage.setItem('settingTheme', JSON.stringify(data))
          commit(types.SET_SETTING, data)
          dispatch(types.SET_THEME_DEFAULT, data)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
      } else {
        let data = JSON.parse(window.localStorage.getItem('settingTheme'))
        commit(types.SET_SETTING, data)
        dispatch(types.SET_THEME_DEFAULT, data)
      }
      // -->> set localStorage
    },
    [types.RECEIVED_TINITIAL] ({ commit }) {
      template.receivedTinitial()
        .then((response) => {
          commit(types.SET_TINITIAL, JSON.parse(response.request.response))
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_TFOLDER] ({ commit }) {
      template.receivedTfolder()
        .then((response) => {
          commit(types.SET_TFOLDER, JSON.parse(response.request.response))
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_MODULES] ({ commit }) {
      template.receivedModules()
        .then((response) => {
          let modules = response.request.response
          commit(types.SET_MODULES, JSON.parse(modules))

          // set module and codapp
          commit(types.SET_ALL_MODULES_CODAPP)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HEADER_NOTI_TOTAL] ({ commit }, $staappr) {
      template.receivedHeaderNotiTotal($staappr)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HEADER_ANNOUNCE_TOTAL, data.announce)
          commit(types.SET_HEADER_REQUEST_TOTAL, data.request)
          commit(types.SET_HEADER_APPROVE, data.approve)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HEADER_ANNOUNCE_INFINITE_LOADING] ({ commit }, params) {
      let triggerInfinite = params.triggerInfinite
      template.receivedHeaderNotiAnnounceInfiniteLoading(params.searchParams)
        .then((response) => {
          triggerInfinite({ data: JSON.parse(response.request.response), $state: params.$state })
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HEADER_REQUEST_INFINITE_LOADING] ({ commit }, params) {
      let triggerInfinite = params.triggerInfinite
      template.receivedHeaderNotiRequestInfiniteLoading(params.staappr, params.searchParams)
        .then((response) => {
          triggerInfinite({ data: JSON.parse(response.request.response), $state: params.$state })
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HEADER_FAVOURITE] ({ commit, dispatch }, params) {
      template.receivedHeaderFavourite(params)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_FAVOURITE_LIST, data)
          commit(types.SET_HEADER_FAVOURITE)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.POST_THEME_COLOR] ({ commit, dispatch }, { saveParam }) {
      template.postThemeColor({ saveParam })
        .then((response) => {
          const data = JSON.parse(response.request.response)
          window.localStorage.setItem('settingTheme', JSON.stringify(saveParam)) // set localStorage
          dispatch(types.RECEIVED_SETTING)
          commit(types.TOGGLE_SETTING_DISP, 'none')
          swal({title: '', text: data.response, html: true, type: 'success'})
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    }
  }
}
