import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getDashLayoutDefault () {
    if (isMock) return generator(mocker.dashLayoutDefault())
    return instance().get(endpoint + '/dash/dashLayoutDefault')
  },
  getDashLayout () {
    if (isMock) return generator(mocker.dashLayoutDefault())
    // if (isMock) return generator(mocker.dashLayout())
    return instance().get(endpoint + '/dash/dashLayout')
  },
  getCalendar () {
    if (isMock) return generator(mocker.calendar())
    return instance().get(endpoint + '/dash/calendar')
  },
  getAnnoucement () {
    if (isMock) return generator(mocker.annoucement())
    return instance().get(endpoint + '/dash/annoucement')
  },
  getAllContact () {
    if (isMock) return generator(mocker.allContact())
    return instance().get(endpoint + '/dash/allContact')
  },
  getContact () {
    if (isMock) return generator(mocker.contact())
    return instance().get(endpoint + '/dash/contact')
  },
  getMeetingRoom () {
    if (isMock) return generator(mocker.meetingRoom())
    return instance().get(endpoint + '/dash/meetingRoom')
  },
  getWeeklyReport () {
    if (isMock) return generator(mocker.weeklyReport())
    return instance().get(endpoint + '/dash/weeklyReport')
  },
  getNews () {
    if (isMock) return generator(mocker.news())
    return instance().get(endpoint + '/dash/news')
  },
  getPoll () {
    if (isMock) return generator(mocker.poll())
    return instance().get(endpoint + '/dash/poll')
  },
  postDashLayout ({dashLayout}) {
    let sendParams = {
      param_json: JSON.stringify(dashLayout)
    }
    if (isMock) return generator(mocker.saveDashLayout())
    return instance().post(endpoint + '/dash/saveDashLayout', sendParams)
  },
  postTodoList ({paramSave}) {
    let sendParams = {
      param_json: JSON.stringify(paramSave)
    }
    if (isMock) return generator(mocker.saveTodoList())
    return instance().post(endpoint + '/dash/saveTodoList', sendParams)
  },
  postDeleteTodoList ({paramDelete}) {
    let sendParams = {
      param_json: JSON.stringify(paramDelete)
    }
    if (isMock) return generator(mocker.deleteTodoList())
    return instance().post(endpoint + '/dash/deleteTodoList', sendParams)
  }
}
