import faker from 'faker'
import assetslabels from 'assets/js/labels'

export const mocker = {
  dashLayoutDefault () {
    return {
      left: [{
        component: 'todayTask',
        name: 'Today Task',
        enabled: true
      }, {
        component: 'annoucement',
        name: 'Annoucement',
        enabled: true
      }],
      center: [{
        component: 'overview',
        name: 'Overview',
        enabled: true
      }],
      right: [{
        component: 'meetingRoom',
        name: 'Meeting Room',
        enabled: true
      }, {
        component: 'weeklyReport',
        name: 'Weekly Reports',
        enabled: true
      }, {
        component: 'news',
        name: 'News',
        enabled: true
      }, {
        component: 'poll',
        name: 'Poll',
        enabled: true
      }]
    }
  },
  dashLayout () {
    return {
      left: [{
        component: 'meetingRoom',
        name: 'Meeting Room',
        enabled: true
      }, {
        component: 'annoucement',
        name: 'Annoucement',
        enabled: false
      }],
      center: [{
        component: 'weeklyReport',
        name: 'Weekly Reports',
        enabled: true
      }, {
        component: 'overview',
        name: 'Overview',
        enabled: true
      }],
      right: [{
        component: 'todayTask',
        name: 'Today Task',
        enabled: true
      }, {
        component: 'news',
        name: 'News',
        enabled: false
      }, {
        component: 'poll',
        name: 'Poll',
        enabled: true
      }]
    }
  },
  calendar () {
    let mockTotal = 21
    let mockRows = []
    let mockDate
    let mockType = {
      holiday: {
        type: 'holiday',
        numType: '1'
      },
      leave: {
        type: 'leave',
        numType: '2'
      },
      appointment: {
        type: 'appointment',
        numType: '3'
      },
      training: {
        type: 'training',
        numType: '4'
      },
      todoList: {
        type: 'todoList',
        numType: '5'
      }
    }

    /* mockDate = '23/11/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['holiday']['type'],
        numType: mockType['holiday']['numType'],
        title: 'วันอยากหยุด 1',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '00:00',
        timend: '00:00'
      }, {
        type: mockType['leave']['type'],
        numType: mockType['leave']['numType'],
        codleave: 'P1',
        desc_codleave: 'ลา 1',
        staappr: 'Y',
        desc_staappr: 'อนุมัติ',
        dtestrt: '08/11/2017',
        dteend: '10/11/2017',
        timstrt: '09:00',
        timend: '10:00',
        remark: 'ลาพักร้อน'
      }, {
        type: mockType['leave']['type'],
        numType: mockType['leave']['numType'],
        codleave: 'P1',
        desc_codleave: 'ลา 2',
        staappr: 'Y',
        desc_staappr: 'อนุมัติ',
        dtestrt: '08/11/2017',
        dteend: '10/11/2017',
        timstrt: '17:30',
        timend: '18:00',
        remark: 'ลาพักร้อน'
      }, {
        type: mockType['training']['type'],
        numType: mockType['training']['numType'],
        title: 'อบรมหลักสูตร 1',
        place: 'ห้องอบรม 3 ชั้น 6',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '17:00',
        timend: '17:30',
        detail: 'อบรมหลักสูตร 1'
      }, {
        type: mockType['training']['type'],
        numType: mockType['training']['numType'],
        title: 'อบรมหลักสูตร 2',
        place: 'ห้องอบรม 3 ชั้น 6',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '13:00',
        timend: '13:30',
        detail: 'อบรมหลักสูตร 1'
      }, {
        type: mockType['appointment']['type'],
        numType: mockType['appointment']['numType'],
        title: 'appointment 1',
        place: 'ห้องประชุมใหญ่ ชั้น 3',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '11:00',
        timend: '17:10',
        detail: 'appointment 1'
      }, {
        type: mockType['appointment']['type'],
        numType: mockType['appointment']['numType'],
        title: 'appointment 2',
        place: 'ห้องประชุมใหญ่ ชั้น 3',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '16:00',
        timend: '17:00',
        detail: 'appointment 2'
      }, {
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'todoList 1',
        place: 'ห้องประชุมใหญ่ ชั้น 3',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '08:00',
        timend: '09:00',
        detail: 'todoList 1',
        flgAllDay: 'N'
      }, {
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'todoList 2',
        place: 'ห้องประชุมใหญ่ ชั้น 3',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '14:00',
        timend: '17:30',
        detail: 'todoList 2',
        flgAllDay: 'N'
      }, {
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'todoList 3',
        place: 'ห้องประชุมใหญ่ ชั้น 3',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '15:00',
        timend: '16:00',
        detail: 'todoList 3',
        flgAllDay: 'N'
      }, {
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'todoList 4',
        place: 'ห้องประชุมใหญ่ ชั้น 3',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '17:30',
        timend: '18:00',
        detail: 'todoList 4',
        flgAllDay: 'N'
      }, {
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'todoList 5',
        place: 'ห้องประชุมใหญ่ ชั้น 3',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '17:45',
        timend: '18:00',
        detail: 'todoList 5',
        flgAllDay: 'N'
      }]
    }) */

    mockDate = '13/10/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['holiday']['type'],
        numType: mockType['holiday']['numType'],
        title: 'วันคล้ายวันสวรรคต พระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช (รัชกาลที่ 9)',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '00:00',
        timend: '00:00'
      }]
    })

    mockDate = '23/10/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['holiday']['type'],
        numType: mockType['holiday']['numType'],
        title: 'วันปิยมหาราช',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '00:00',
        timend: '00:00'
      }]
    })

    mockDate = '26/10/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['holiday']['type'],
        numType: mockType['holiday']['numType'],
        title: 'วันพระราชพิธีถวายพระเพลิงพระบรมศพ พระบาทสมเด็จพระปรมินทรมหาภูมิพลอดุลยเดช',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '00:00',
        timend: '00:00'
      }]
    })

    mockDate = '08/11/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['leave']['type'],
        numType: mockType['leave']['numType'],
        codleave: 'P1',
        desc_codleave: 'ลาพักร้อน',
        staappr: 'Y',
        desc_staappr: 'อนุมัติ',
        dtestrt: '08/11/2017',
        dteend: '10/11/2017',
        timstrt: '09:00',
        timend: '18:00',
        remark: 'ลาพักร้อน'
      }]
    })

    mockDate = '09/11/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['leave']['type'],
        numType: mockType['leave']['numType'],
        codleave: 'P1',
        desc_codleave: 'ลาพักร้อน',
        staappr: 'Y',
        desc_staappr: 'อนุมัติ',
        dtestrt: '08/11/2017',
        dteend: '10/11/2017',
        timstrt: '09:00',
        timend: '18:00',
        remark: 'ลาพักร้อน'
      }]
    })

    mockDate = '10/11/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['leave']['type'],
        numType: mockType['leave']['numType'],
        codleave: 'P1',
        desc_codleave: 'ลาพักร้อน',
        staappr: 'Y',
        desc_staappr: 'อนุมัติ',
        dtestrt: '08/11/2017',
        dteend: '10/11/2017',
        timstrt: '09:00',
        timend: '18:00',
        remark: 'ลาพักร้อน'
      }]
    })

    mockDate = '13/11/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['training']['type'],
        numType: mockType['training']['numType'],
        title: 'อบรมหลักสูตร กลยุทธ์การทำงานอย่างมืออาชีพ',
        place: 'ห้องอบรม 3 ชั้น 6',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '12:00',
        timend: '18:00',
        detail: 'อบรมหลักสูตร กลยุทธ์การทำงานอย่างมืออาชีพ'
      }]
    })

    mockDate = '16/11/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['appointment']['type'],
        numType: mockType['appointment']['numType'],
        title: 'สัมภาษณ์งาน ตำแหน่ง Pregrammer',
        place: 'ห้องประชุมเล็ก ชั้น 2',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '10:00',
        timend: '12:00',
        detail: 'สัมภาษณ์งาน ตำแหน่ง Pregrammer'
      }, {
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'ประชุมความคืบหน้าโครงการ',
        place: 'ห้องประชุมใหญ่ ชั้น 3',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '13:00',
        timend: '14:00',
        detail: 'ประชุมความคืบหน้าโครงการ',
        flgAllDay: 'N'
      }]
    })

    mockDate = '22/11/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['training']['type'],
        numType: mockType['training']['numType'],
        title: 'อบรมหลักสูตร การบริหารโครงการมืออาชีพ',
        place: 'ห้องประชุมใหญ่ ชั้น 8',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '08:30',
        timend: '12:30',
        detail: 'อบรมหลักสูตร การบริหารโครงการมืออาชีพ'
      }]
    })

    mockDate = '30/11/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'เตรียมการนำเสนองานซอฟต์แวร์',
        place: 'ABC Technologu Co., Ltd.',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '13:30',
        timend: '17:30',
        detail: 'เตรียมการนำเสนองานซอฟต์แวร์',
        flgAllDay: 'N'
      }]
    })

    mockDate = '01/12/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['leave']['type'],
        numType: mockType['leave']['numType'],
        codleave: 'P1',
        desc_codleave: 'ลากิจ',
        staappr: 'Y',
        desc_staappr: 'อนุมัติ',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '09:00',
        timend: '18:00',
        remark: 'ไปติดต่อราชการ'
      }]
    })

    mockDate = '05/12/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['holiday']['type'],
        numType: mockType['holiday']['numType'],
        title: 'วันเฉลิมพระชนมพรรษา พระบาทสมเด็จพระเจ้าอยู่หัว รัชกาลที่ 9',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '00:00',
        timend: '00:00'
      }]
    })

    mockDate = '06/12/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['appointment']['type'],
        numType: mockType['appointment']['numType'],
        title: 'การประชุมบริษัทประจำปี',
        place: 'ห้องประชุมใหญ่ ชั้น 3',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '09:30',
        timend: '12:30',
        detail: 'ประชุมบริษัทประจำปี 2017'
      }, {
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'เตรียมเอกสารประชุม',
        place: 'ห้องประชุมใหญ่ ชั้น 3',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '08:00',
        timend: '09:30',
        detail: 'เตรียมเอกสารประชุม',
        flgAllDay: 'N'
      }]
    })

    mockDate = '10/12/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['holiday']['type'],
        numType: mockType['holiday']['numType'],
        title: 'วันพระราชทานรัฐธรรมนูญ',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '00:00',
        timend: '00:00'
      }]
    })

    mockDate = '11/12/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['holiday']['type'],
        numType: mockType['holiday']['numType'],
        title: 'ชดเชยวันพระราชทานรัฐธรรมนูญ',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '00:00',
        timend: '00:00'
      }]
    })

    mockDate = '14/12/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['appointment']['type'],
        numType: mockType['appointment']['numType'],
        title: 'การประชุมหัวหน้าแผนก',
        place: 'ห้องประชุมเล็ก ชั้น 2',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '13:30',
        timend: '15:30',
        detail: 'การประชุมหัวหน้าแผนก ทุกแผนก'
      }, {
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'งานสัมมนาเปิดตัวซอฟต์แวร์',
        place: 'ห้องประชุมใหญ่ ชั้น 3',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '09:00',
        timend: '18:00',
        detail: 'งานสัมมนาเปิดตัวซอฟต์แวร์',
        flgAllDay: 'Y'
      }]
    })

    mockDate = '19/12/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['training']['type'],
        numType: mockType['training']['numType'],
        title: 'อบรมหลักสูตร กลยุทธการตลาดและการบริการเชิงรุก',
        place: 'ห้องอบรม 2 ชั้น 5',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '10:30',
        timend: '15:30',
        detail: 'อบรมหลักสูตร กลยุทธการตลาดและการบริการเชิงรุก'
      }, {
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'ออกบูธนอกสถานที่',
        place: 'ศูนย์ประชุมแห่งชาติสิริกิต อาคารนิทรรศการโซน ซี ชั้น 2',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '09:00',
        timend: '18:00',
        detail: 'ออกบูธนอกสถานที่',
        flgAllDay: 'Y'
      }]
    })

    mockDate = '21/12/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['appointment']['type'],
        numType: mockType['appointment']['numType'],
        title: 'สัมภาษณ์งาน ตำแหน่ง Sale Manager',
        place: 'ห้องประชุมเล็ก ชั้น 4',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '10:00',
        timend: '12:00',
        detail: 'สัมภาษณ์งาน ตำแหน่ง Pregrammer'
      }]
    })

    mockDate = '25/12/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['leave']['type'],
        numType: mockType['leave']['numType'],
        codleave: 'P1',
        desc_codleave: 'ลากิจ',
        staappr: 'Y',
        desc_staappr: 'อนุมัติ',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '13:00',
        timend: '15:00',
        remark: 'ไปติดต่อราชการ'
      }, {
        type: mockType['appointment']['type'],
        numType: mockType['appointment']['numType'],
        title: 'สัมภาษณ์งาน ตำแหน่ง Sale Manager',
        place: 'ห้องประชุมเล็ก ชั้น 4',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '10:00',
        timend: '12:00',
        detail: 'สัมภาษณ์งาน ตำแหน่ง Pregrammer'
      }, {
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'เตรียมเอกสารประชุม',
        place: 'People Plus Software Co., Ltd.',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '08:00',
        timend: '09:00',
        detail: 'เตรียมเอกสารประชุม',
        flgAllDay: 'N'
      }, {
        type: mockType['todoList']['type'],
        numType: mockType['todoList']['numType'],
        title: 'ออกบูธนอกสถานที่',
        place: 'ศูนย์ประชุมแห่งชาติสิริกิต อาคารนิทรรศการโซน ซี ชั้น 2',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '16:00',
        timend: '18:00',
        detail: 'ออกบูธนอกสถานที่',
        flgAllDay: 'N'
      }]
    })

    mockDate = '29/12/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['leave']['type'],
        numType: mockType['leave']['numType'],
        codleave: 'L3',
        desc_codleave: 'ลาพักร้อน',
        staappr: 'A',
        desc_staappr: 'อยู่ระหว่างการอนุมัติ',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '09:00',
        timend: '18:00',
        remark: '-'
      }]
    })

    mockDate = '31/12/2017'
    mockRows.push({
      date: mockDate.substring(0, 2),
      month: mockDate.substring(3, 5),
      year: mockDate.substring(6, 10),
      dtedate: mockDate,
      events: [{
        type: mockType['holiday']['type'],
        numType: mockType['holiday']['numType'],
        title: 'วันสิ้นปี',
        dtestrt: mockDate,
        dteend: mockDate,
        timstrt: '00:00',
        timend: '00:00'
      }]
    })

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  annoucement () {
    let mockRows = []
    const mockTotal = 5
    mockRows.push({
      id: 1,
      company: 'People Plus Software',
      title: 'การทำความสะอาดพื้น',
      description: 'ในวันที่ 20/12/2017 บริษัทจะทำความสะอาดพื้นพรมให้พนักงานทุกคนยกของไว้บนโต๊ะในวันดังกล่าวด้วย',
      time: '1:30 PM',
      attachfile: 'file'
    })
    mockRows.push({
      id: 2,
      company: 'People Plus Software',
      title: 'การตัดรอบเงินเดือนใหม่',
      description: 'บริษัทให้มีประกาศเปลี่ยนรอบเงินเดือนใหม่ตั้งแต่เดือนมกราคม 2018 เป็นต้นไป โดยจะเปลี่ยนเป็นรอบ 25 - 24 ของทุกเดือน',
      time: '11:00 AM',
      attachfile: 'file'
    })
    mockRows.push({
      id: 3,
      company: 'People Plus Software',
      title: 'การตรวจสอบ ISO',
      description: 'ให้พนักงานทุกคนเตรียมเอกสารให้พร้อมสำหรับการตรวจสอบ ISO ในวันที่ 12/12/2017',
      time: '30 Nov',
      attachfile: 'file'
    })
    mockRows.push({
      id: 4,
      company: 'People Plus Software',
      title: 'Notify Internet usage',
      description: 'Notify Internet usage for October 2017 in attach file.',
      time: '31 Oct',
      attachfile: 'file'
    })
    mockRows.push({
      id: 5,
      company: 'People Plus Software',
      title: 'Notify Internet usage',
      description: 'Notify Internet usage for September 2017 in attach file.',
      time: '28 Sep',
      attachfile: 'file'
    })

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  allContact () {
    let mockRows = []
    const mockTotal = 20
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        id: i,
        name: faker.name.findName(),
        position: faker.name.jobTitle(),
        department: faker.name.jobType(),
        email: faker.internet.exampleEmail(),
        phone: faker.phone.phoneNumberFormat(),
        mobile: faker.phone.phoneNumberFormat(),
        image: i + '.jpg'
      })
    }

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  contact () {
    let mockGroupRows1 = []
    let mockGroupRows2 = []
    let mockRows
    let mockTotal

    mockTotal = 10
    mockRows = []
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        id: i,
        name: faker.name.findName(),
        position: faker.name.jobTitle(),
        department: faker.name.jobType(),
        email: faker.internet.exampleEmail(),
        phone: faker.phone.phoneNumberFormat(),
        mobile: faker.phone.phoneNumberFormat(),
        image: i + '.jpg',
        flgPresent: i % 3 ? 'Y' : 'N'
      })
    }
    mockGroupRows1 = {
      name: 'People',
      total: mockTotal,
      rows: mockRows
    }

    mockTotal = 5
    mockRows = []
    for (i = 1; i <= 5; i++) {
      mockRows.push({
        id: i,
        name: faker.name.findName(),
        position: faker.name.jobTitle(),
        department: faker.name.jobType(),
        email: faker.internet.exampleEmail(),
        phone: faker.phone.phoneNumberFormat(),
        mobile: faker.phone.phoneNumberFormat(),
        image: i + '.jpg',
        flgPresent: i % 3 ? 'Y' : 'N'
      })
    }
    mockGroupRows2 = {
      name: 'Subordinate',
      total: mockTotal,
      rows: mockRows
    }

    return {
      total: 2,
      rows: [
        mockGroupRows1,
        mockGroupRows2
      ]
    }
  },
  meetingRoom () {
    return {
      name: faker.name.findName(),
      date: 'Thursday, Dec 14',
      time: '3.00-4.00 PM',
      location: 'Geneva Meeting Room'
    }
  },
  weeklyReport () {
    return {
      title: 'Record Weekly Reports',
      detail: 'Record Weekly Reports'
    }
  },
  news () {
    let mockRows = []
    const mockTotal = 3

    mockRows.push({
      id: 1,
      title: 'Creating a better everyday life for people',
      detail: 'Creating a better everyday life for people',
      image: 'news_image1.png'
    })
    mockRows.push({
      id: 2,
      title: 'Digital Citizenship and Safety Course',
      detail: 'Digital Citizenship and Safety Course',
      image: 'news_image2.png'
    })
    mockRows.push({
      id: 3,
      title: 'Board Meetings and Agendas',
      detail: 'Board Meetings and Agendas',
      image: 'news_image3.png'
    })

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  poll () {
    let mockHasImage = []
    let mockRows = []
    let mockChoice = []
    const mockTotal = 2

    mockHasImage = (Math.random() >= 0.5) ? 'Y' : 'N'
    mockChoice = [{
      image: (mockHasImage === 'Y') ? faker.image.avatar() : '',
      text: 'Sports',
      number: 4,
      selected: 'N'
    }, {
      image: (mockHasImage === 'Y') ? faker.image.avatar() : '',
      text: 'Music',
      number: 2,
      selected: 'Y'
    }, {
      image: (mockHasImage === 'Y') ? faker.image.avatar() : '',
      text: 'Traveling',
      number: 5,
      selected: 'N'
    }, {
      image: (mockHasImage === 'Y') ? faker.image.avatar() : '',
      text: 'Socializing',
      number: 8,
      selected: 'N'
    }, {
      image: (mockHasImage === 'Y') ? faker.image.avatar() : '',
      text: 'Computer',
      number: 4,
      selected: 'N'
    }, {
      image: (mockHasImage === 'Y') ? faker.image.avatar() : '',
      text: 'Others',
      number: 6,
      selected: 'N'
    }]
    mockRows.push({
      codpoll: '1',
      postBy: 'People Plus Software',
      topic: 'What do you interest about most ?',
      totalVote: '29',
      permission: 'Public',
      dateRange: '',
      timeRange: 'Just now',
      choices: mockChoice,
      hasImage: 'N',
      addChoice: 'Y'
    })

    mockHasImage = (Math.random() >= 0.5) ? 'Y' : 'N'
    mockChoice = [{
      image: (mockHasImage === 'Y') ? faker.image.avatar() : '',
      text: 'Travel',
      number: 10,
      selected: 'Y'
    }, {
      image: (mockHasImage === 'Y') ? faker.image.avatar() : '',
      text: 'Training',
      number: 8,
      selected: 'N'
    }]
    mockRows.push({
      codpoll: '2',
      postBy: 'People Plus Software',
      topic: 'What activities do you want to organize ?',
      totalVote: '9',
      permission: 'Public',
      dateRange: '1 Aug',
      timeRange: '11:00 AM',
      choices: mockChoice,
      hasImage: 'N',
      addChoice: 'N'
    })

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  saveDashLayout () {
    return assetslabels.mockPostMessage
  },
  saveTodoList () {
    return assetslabels.mockPostMessage
  },
  deleteTodoList () {
    return assetslabels.mockPostMessage
  }
}
