import * as types from './mutation-types'
import home from '../api'
import swal from 'sweetalert'
import moment from 'moment'
import _ from 'lodash'

export default {
  state: {
    dashLayoutDefault: {},
    dashLayout: {},
    calendar: {},
    todayTask: {},
    annoucement: {},
    allContact: {},
    contact: {},
    meetingRoom: {},
    weeklyReport: {},
    news: {},
    poll: {},
    calendarDetailGroup: {
      leave: {
        name: 'Leave',
        enabled: true
      },
      appointment: {
        name: 'Appointment',
        enabled: true
      },
      training: {
        name: 'Training',
        enabled: true
      },
      todoList: {
        name: 'To Do list',
        enabled: true
      },
      holiday: {
        name: 'Holiday',
        enabled: true
      }
    },
    todoListdetails: {
      topic: '',
      place: '',
      dtestrt: '',
      dteend: '',
      timstrt: '',
      timend: '',
      detail: '',
      flgAllDay: 'Y'
    }
  },
  getters: {
    [types.GET_DASH_LAYOUT_DEFAULT] (state) {
      return state.dashLayoutDefault
    },
    [types.GET_DASH_LAYOUT] (state) {
      return state.dashLayout
    },
    [types.GET_CALENDAR] (state) {
      return state.calendar
    },
    [types.GET_TODAY_TASK] (state) {
      return state.todayTask
    },
    [types.GET_ANNOUCEMENT] (state) {
      return state.annoucement
    },
    [types.GET_ALL_CONTACT] (state) {
      return state.allContact
    },
    [types.GET_CONTACT] (state) {
      return state.contact
    },
    [types.GET_MEETING_ROOM] (state) {
      return state.meetingRoom
    },
    [types.GET_WEEKLY_REPORT] (state) {
      return state.weeklyReport
    },
    [types.GET_NEWS] (state) {
      return state.news
    },
    [types.GET_POLL] (state) {
      return state.poll
    },
    [types.GET_CALENDAR_DETAIL_GROUP] (state) {
      return state.calendarDetailGroup
    },
    [types.GET_TODO_LIST_DETAILS] (state) {
      return state.todoListdetails
    }
  },
  mutations: {
    [types.SET_DASH_LAYOUT_DEFAULT] (state, dashLayout) {
      state.dashLayoutDefault = dashLayout
    },
    [types.SET_DASH_LAYOUT] (state, dashLayout) {
      state.dashLayout = dashLayout
    },
    [types.SET_CALENDAR] (state, calendar) {
      state.calendar = calendar
    },
    [types.SET_TODAY_TASK] (state, todayTask) {
      let date = moment().format('DD')
      let month = moment().format('MM')
      let year = moment().format('YYYY')
      state.todayTask = state.calendar.rows.filter((val, index) => {
        return val.date === _.padStart(date, 2, '0') && val.month === _.padStart(month, 2, '0') && val.year === year.toString()
      }).map((val, index) => {
        return val.events
      })
      if (typeof state.todayTask[0] !== 'undefined') {
        state.todayTask = state.todayTask[0]
      }
    },
    [types.SET_ANNOUCEMENT] (state, annoucement) {
      state.annoucement = annoucement
    },
    [types.SET_ALL_CONTACT] (state, allContact) {
      state.allContact = allContact
    },
    [types.SET_CONTACT] (state, contact) {
      state.contact = contact
    },
    [types.SET_MEETING_ROOM] (state, meetingRoom) {
      state.meetingRoom = meetingRoom
    },
    [types.SET_WEEKLY_REPORT] (state, weeklyReport) {
      state.weeklyReport = weeklyReport
    },
    [types.SET_NEWS] (state, news) {
      state.news = news
    },
    [types.SET_POLL] (state, poll) {
      state.poll = poll
    },
    [types.SET_TODO_LIST_DETAILS] (state, todoListdetails) {
      state.todoListdetails = todoListdetails
    }
  },
  actions: {
    [types.RECEIVED_DASH_LAYOUT_DEFAULT] ({ commit }) {
      home.getDashLayoutDefault()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_DASH_LAYOUT_DEFAULT, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_DASH_LAYOUT] ({ commit }) {
      home.getDashLayout()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_DASH_LAYOUT, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_CALENDAR] ({ commit }) {
      home.getCalendar()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_CALENDAR, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_TODAY_TASK] ({ commit }) {
      commit(types.SET_TODAY_TASK)
    },
    [types.RECEIVED_ANNOUCEMENT] ({ commit }) {
      home.getAnnoucement()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_ANNOUCEMENT, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_ALL_CONTACT] ({ commit }) {
      home.getAllContact()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_ALL_CONTACT, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_CONTACT] ({ commit }) {
      home.getContact()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_CONTACT, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_MEETING_ROOM] ({ commit }) {
      home.getMeetingRoom()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_MEETING_ROOM, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_WEEKLY_REPORT] ({ commit }) {
      home.getWeeklyReport()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_WEEKLY_REPORT, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_NEWS] ({ commit }) {
      home.getNews()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_NEWS, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_POLL] ({ commit }) {
      home.getPoll()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_POLL, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.POST_DASH_LAYOUT] ({ commit, dispatch }, { saveParam }) {
      home.postDashLayout({ saveParam })
        .then((response) => {
          const data = JSON.parse(response.request.response)
          dispatch(types.RECEIVED_DASH_LAYOUT)
          swal({title: '', text: data.response, html: true, type: 'success'})
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.POST_TODO_LIST] ({ commit, dispatch }, { saveParam }) {
      home.postTodoList({ saveParam })
        .then((response) => {
          const data = JSON.parse(response.request.response)
          dispatch(types.RECEIVED_CALENDAR)
          swal({title: '', text: data.response, html: true, type: 'success'})
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.POST_DELETE_TODO_LIST] ({ commit, dispatch }, { deleteParam }) {
      home.postDeleteTodoList({ deleteParam })
        .then((response) => {
          const data = JSON.parse(response.request.response)
          dispatch(types.RECEIVED_CALENDAR)
          swal({title: '', text: data.response, html: true, type: 'success'})
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    }
  }
}
