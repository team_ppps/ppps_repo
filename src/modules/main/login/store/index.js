import * as types from './mutation-types'
import login from '../api'
import { router } from 'register/router'
import swal from 'sweetalert'
import assetsLabels from 'assets/js/labels'

export default {
  state: {
    loginDisp: true,
    forgotPasswordDisp: false,
    forgotPasswordVerifyDisp: false,
    changePasswordDisp: false,
    loadingDisp: false,
    response: '',
    lang: assetsLabels.defaultLang,
    flgAction: '',
    currentUser: {
      p_coduser: '',
      p_codempid: '',
      username: {
        en: '',
        th: ''
      },
      p_codpos: '',
      position: {
        en: '',
        th: ''
      },
      p_codcomp: '',
      company: {
        en: '',
        th: ''
      },
      descCodcomp: '',
      descCodpos: '',
      usrcom: []
    },
    currentUserPermission: {
      edit: []
    },
    baseConn: {},
    token: {
      refresh_token: '',
      access_token: ''
    }
  },
  getters: {
    [types.GET_LOGIN_LOADING_DISP] (state) {
      return state.loadingDisp
    },
    [types.GET_LOGIN_LOGIN_DISP] (state) {
      return state.loginDisp
    },
    [types.GET_LOGIN_FORGOT_PASSWORD_DISP] (state) {
      return state.forgotPasswordDisp
    },
    [types.GET_LOGIN_FORGOT_PASSWORD_VERIFY_DISP] (state) {
      return state.forgotPasswordVerifyDisp
    },
    [types.GET_LOGIN_CHANGE_PASSWORD_DISP] (state) {
      return state.changePasswordDisp
    },
    [types.GET_BASE_CONNECTION] (state) {
      return state.baseConn
    },
    [types.GET_RESPONSE] (state) {
      return state.response
    },
    [types.GET_LANG] (state) {
      return state.lang
    },
    [types.GET_LOGIN_FLG_ACTION] (state) {
      return state.flgAction
    },
    [types.GET_CURRENT_USER] (state) {
      return state.currentUser
    },
    [types.GET_CURRENT_USER_PERMISSION] (state) {
      return state.currentUserPermission
    },
    [types.GET_TOKEN] (state) {
      return state.token
    }
  },
  mutations: {
    [types.SET_LOGIN_LOADING_DISP] (state, loadingDisp) {
      state.loadingDisp = loadingDisp
    },
    [types.SET_LOGIN_LOGIN_DISP] (state, loginDisp) {
      state.loginDisp = loginDisp
    },
    [types.SET_LOGIN_FORGOT_PASSWORD_DISP] (state, forgotPasswordDisp) {
      state.forgotPasswordDisp = forgotPasswordDisp
    },
    [types.SET_LOGIN_FORGOT_PASSWORD_VERIFY_DISP] (state, forgotPasswordVerifyDisp) {
      state.forgotPasswordVerifyDisp = forgotPasswordVerifyDisp
    },
    [types.SET_LOGIN_CHANGE_PASSWORD_DISP] (state, changePasswordDisp) {
      state.changePasswordDisp = changePasswordDisp
    },
    [types.SET_BASE_CONNECTION] (state, baseConn) {
      state.baseConn = baseConn
    },
    [types.SET_RESPONSE] (state, response) {
      state.response = response
    },
    [types.SET_LANG] (state, lang) {
      state.lang = lang
      window.localStorage.setItem('lang', lang)
    },
    [types.SET_LOGIN_FLG_ACTION] (state, flgAction) {
      state.flgAction = flgAction
      window.localStorage.setItem('flgAction', flgAction)
    },
    [types.SET_CURRENT_USER] (state, currentUser) {
      state.currentUser = currentUser
      window.localStorage.setItem('currentUser', JSON.stringify(currentUser))
    },
    [types.SET_CURRENT_USER_PERMISSION] (state, currentUserPermission) {
      state.currentUserPermission = currentUserPermission
      window.localStorage.setItem('currentUserPermission', JSON.stringify(currentUserPermission))
    },
    [types.SET_LOGOUT] (state, currentUser) {
      window.localStorage.setItem('currentUser', JSON.stringify(currentUser.user_profile))
      window.localStorage.setItem('currentUserPermission', JSON.stringify(currentUser.user_permission))
      window.localStorage.setItem('token', JSON.stringify({}))
      state.currentUser = currentUser
      state.response = ''
      state.token = {}
      router.push({ name: 'login' })
      location.reload(true)
    },
    [types.TOGGLE_LOGIN_PAGE] (state, page) {
      let loginDisp = false
      let forgotPasswordDisp = false
      let forgotPasswordVerifyDisp = false
      let changePasswordDisp = false
      switch (page) {
        case 'login':
          loginDisp = true
          break
        case 'forgotPassword':
          forgotPasswordDisp = true
          break
        case 'forgotPasswordVerify':
          forgotPasswordVerifyDisp = true
          break
        case 'changePassword':
          changePasswordDisp = true
          break
      }
      state.loginDisp = loginDisp
      state.forgotPasswordDisp = forgotPasswordDisp
      state.forgotPasswordVerifyDisp = forgotPasswordVerifyDisp
      state.changePasswordDisp = changePasswordDisp
      state.loadingDisp = false
    },
    [types.SET_TOKEN] (state, token) {
      window.localStorage.setItem('token', JSON.stringify({ refresh_token: token.refresh_token }))
      state.token.refresh_token = token.refresh_token
      state.token.access_token = token.access_token
    }
  },
  actions: {
    [types.RECEIVED_BASE_CONNECTION] ({ commit }) {
      login.getBaseConnection()
        .then((response) => {
          const baseConn = JSON.parse(response.request.response)
          commit(types.SET_BASE_CONNECTION, baseConn)
        })
        .catch((response) => {
          commit(types.SET_RESPONSE, 'Cannot Access a Webservice.')
        })
    },
    [types.POST_CURRENT_USER] ({ commit }, formUser) {
      const username = formUser.coduser
      const password = formUser.codpswd
      const codapp = (typeof formUser.codapp === 'undefined' || formUser.codapp === '' ? 'home' : formUser.codapp)
      const autologin = (typeof formUser.autologin === 'undefined' ? 'N' : formUser.autologin)

      commit(types.SET_LOGIN_LOADING_DISP, true)
      if (username !== '' && password !== '') {
        login.postAuthentication(username, password)
          .then((response) => {
            const token = JSON.parse(response.request.response)
            commit(types.SET_TOKEN, token)
            login.postCurrentUser()
              .then((response) => {
                const currentUser = JSON.parse(response.request.response)
                commit(types.SET_CURRENT_USER, currentUser.user_profile)
                commit(types.SET_CURRENT_USER_PERMISSION, currentUser.user_permission)
                commit(types.SET_RESPONSE, '')
                console.log(codapp)
                router.push({ name: codapp })
                commit(types.SET_LOGIN_LOADING_DISP, false)
              })
              .catch((error) => {
                let message = error
                if (error.response) {
                  const data = error.response.data
                  message = data.response
                }
                if (autologin === 'Y') router.push({ name: 'login' })
                commit(types.SET_LOGIN_LOADING_DISP, false)
                commit(types.SET_RESPONSE, message)
              })
          })
          .catch((error) => {
            let message = error
            if (error.response) {
              const data = error.response.data
              message = data.message
            }
            if (autologin === 'Y') router.push({ name: 'login' })
            commit(types.SET_LOGIN_LOADING_DISP, false)
            commit(types.SET_RESPONSE, message)
          })
      } else {
        if (autologin === 'Y') router.push({ name: 'login' })
        commit(types.SET_LOGIN_LOADING_DISP, false)
        commit(types.SET_RESPONSE, 'Please enter Username or Password')
      }
    },
    [types.POST_LOGOUT] ({ commit }) {
      login.postLogout()
        .then((response) => {
          const currentUser = {
            user_profile: {
              p_codempid: '',
              p_coduser: '',
              username: {}
            },
            user_permission: {
              edit: []
            }
          }
          commit(types.SET_LOGOUT, currentUser)
        })
        .catch((response) => {
          console.log(response)
        })
    },
    [types.POST_FORGOT_PASSWORD] ({ commit }, params) {
      commit(types.SET_LOGIN_LOADING_DISP, true)
      login.postForgotPassword(params)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.TOGGLE_LOGIN_PAGE, 'forgotPasswordVerify')
          commit(types.SET_RESPONSE, data.message)
          commit(types.SET_LOGIN_LOADING_DISP, false)
        })
        .catch((response) => {
          commit(types.SET_LOGIN_LOADING_DISP, false)
          commit(types.SET_RESPONSE, 'ERROR While Process, Please Refresh page then process again.')
        })
    },
    [types.POST_FORGOT_PASSWORD_VERIFY] ({ commit }, params) {
      commit(types.SET_LOGIN_LOADING_DISP, true)
      login.postForgotPasswordVerify(params)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.TOGGLE_LOGIN_PAGE, 'changePassword')
          commit(types.SET_RESPONSE, data.message)
          commit(types.SET_LOGIN_LOADING_DISP, false)
        })
        .catch((response) => {
          commit(types.SET_LOGIN_LOADING_DISP, false)
          commit(types.SET_RESPONSE, 'ERROR While Process, Please Refresh page then process again.')
        })
    },
    [types.POST_CHANGE_PASSWORD] ({ commit }, params) {
      commit(types.SET_LOGIN_LOADING_DISP, true)
      login.postChangePassword(params)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_RESPONSE, data.message)
          commit(types.SET_LOGIN_LOADING_DISP, false)
        })
        .catch((response) => {
          commit(types.SET_LOGIN_LOADING_DISP, false)
          commit(types.SET_RESPONSE, 'ERROR While Process, Please Refresh page then process again.')
        })
    },
    [types.POST_AUTH_VUE_MIDDLEWARE] ({ commit, dispatch }, params) {
      let lang = (window.localStorage.getItem('lang')) ? window.localStorage.getItem('lang') : assetsLabels.defaultLang
      login.postAuthVueMiddleware(params.codapp)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          if (data.status === 'SUCCESS') {
            params.next()
          } else {
            swal({
              title: 'Error Warning',
              text: assetsLabels.loginMessageError[lang].ERROR
            },
            function () {
              dispatch(types.POST_LOGOUT)
            })
          }
        })
        .catch((error) => {
          const data = error.response.data
          if (data.status === 'LICENSE') {
            swal({
              title: 'License Warning',
              text: assetsLabels.loginMessageError[lang].LICENSE
            },
            function () {
              router.push({ name: data.redirect })
            })
          } else if (data.status === 'PERMIT') {
            swal({
              title: 'Permit Warning',
              text: assetsLabels.loginMessageError[lang].PERMIT
            },
            function () {
              router.push({ name: data.redirect })
            })
          } else if (data.status === 'TIMEOUT') {
            swal({
              title: 'Timeout Warning',
              text: assetsLabels.loginMessageError[lang].TIMEOUT
            },
            function () {
              dispatch(types.POST_LOGOUT)
            })
          } else if (data.status === 'ERROR') {
            swal({
              title: 'Error Warning',
              text: assetsLabels.loginMessageError[lang].ERROR
            },
            function () {
              dispatch(types.POST_LOGOUT)
            })
          }
        })
    },
    [types.RECEIVED_TOKEN] ({ commit, dispatch }, { refreshToken, codapp, next }) {
      login.receivedToken(refreshToken)
        .then(response => {
          const token = JSON.parse(response.request.response)
          commit(types.SET_TOKEN, token)
          next()
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.message
          }
          swal({title: '', text: message, html: true, type: 'error'})
          const currentUser = {
            user_profile: {
              p_codempid: '',
              p_coduser: '',
              username: {}
            },
            user_permission: {
              edit: []
            }
          }
          commit(types.SET_LOGOUT, currentUser)
          router.push({ name: 'login' })
        })
    }
  }
}
