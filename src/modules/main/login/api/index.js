import { instance, endpoint, isMockLogin, generator } from 'register/api'
import { mocker } from './mocker'
import env from '@/../env.js'

export default {
  postAuthentication (username, password) {
    if (isMockLogin) return generator(mocker.authentication())
    let params = {
      username: username,
      password: password,
      client_id: env.client_id,
      client_secret: env.client_secret,
      grant_type: 'password',
      scope: '*'
    }
    return instance().post('v1/oauth/token', params)
  },
  receivedToken (refreshToken) {
    let sendParams = {
      client_id: env.client_id,
      client_secret: env.client_secret,
      grant_type: 'refresh_token',
      refresh_token: refreshToken
    }
    if (isMockLogin) return generator(mocker.token())
    return instance().post('/v1/oauth/token', sendParams)
  },
  postCurrentUser () {
    if (isMockLogin) return generator(mocker.currentUser())
    return instance().get(endpoint + '/login/user')
  },
  postLogout () {
    if (isMockLogin) return generator(mocker.signout())
    return instance().delete(endpoint + '/logout')
  },
  getBaseConnection () {
    if (isMockLogin) return generator(mocker.baseConn())
    return instance().get(endpoint + '/login/baseCon')
  },
  postForgotPassword (params) {
    if (isMockLogin) return generator(mocker.forgotPassword())
    return instance().post(endpoint + '/login/forgotpassword', params)
  },
  postForgotPasswordVerify (params) {
    if (isMockLogin) return generator(mocker.forgotPasswordVerify())
    return instance().post(endpoint + '/login/forgotPasswordVerify', params)
  },
  postChangePassword (params) {
    if (isMockLogin) return generator(mocker.changePassword())
    return instance().post(endpoint + '/login/changepassword', params)
  },
  postAuthVueMiddleware (codapp) {
    if (isMockLogin) return generator(mocker.authVueMiddleware())
    return instance().get(endpoint + '/login/authVueMiddleware/' + codapp)
  }
}
