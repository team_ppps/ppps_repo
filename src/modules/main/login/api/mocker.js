import assetsLabels from 'assets/js/labels'

export const mocker = {
  authentication () {
    return {
      token_type: 'Bearer',
      expires_in: 2592000,
      access_token: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjZkZTNmNDhmZjM1MTFiYjVjNWM5Njc4YmMwZTNjZjQ4MzFhYmE0M2NhYTgwYjhjNTAyYmZmMTMzOGQ2YzRhMDNmYTMwOWY1OWNkNjQxZGZhIn0.eyJhdWQiOiI4MiIsImp0aSI6IjZkZTNmNDhmZjM1MTFiYjVjNWM5Njc4YmMwZTNjZjQ4MzFhYmE0M2NhYTgwYjhjNTAyYmZmMTMzOGQ2YzRhMDNmYTMwOWY1OWNkNjQxZGZhIiwiaWF0IjoxNTE2NjkxMTY1LCJuYmYiOjE1MTY2OTExNjUsImV4cCI6MTUxOTI4MzE2NSwic3ViIjoiNjkiLCJzY29wZXMiOlsiKiJdfQ.cKq2PoK9or7VXq8QVWOfYR8mVIw-ViESQjDYYBde15-3IjWBUpMYu8--jg5mejKid_CkSYNYmrjVOUMHmrFA3saUzDVp_2EA61FP2OTFZQaDyfM5fthbFYxfUTEYZnGpG2mTDY9DUIBdMSGMZ6qLN9mtQ2un_Net5yxCs5qFOKPLryCaUxCp-WoMHmqQdslA6s5qoAKAq1k0fXOKMfT1BzNRsa3yunNpu6M_HXt838t5nkxN9BQcq6E1NG1KhMa3LPCA1HlBiPa2ZAidghcZwty-ytwuBkwfbFTwaO6FnGdj9e26Ge9oREd0CylgCZjof0zawiAm7xl1VwxhBG_5ZMKAdtvpKhx-yJeBh4yXwsZGAX3-YsASt1nJaX8v5AqqfCnZq0qoVxaPfLbygiGaS6mjfYLGQOeScRb9vxCWA1L52R1LZlx8rt2cilrPG69TWY7A0nAFzf2fnKu4iVsn6wtAcH0EqYkVMFPtUmpLOeH7dWIGixieD5Qi3xF5CrwEBzA7Q4mC2tWq4sRSYlNk5DqpC4sGzN4tu7gGnBK55mjnpD_Cvl7EmXjOEMH50vo4OFQ6Ej5Ax-F6LCw9iV0NtWM4-CttjKUIuQaobd16BLyTcyRPRRndgoEBQ9PR4-wjbmNJijVUGjIgg6_ayNyewHULTfoBr7V3MNkt6dUCPeA',
      refresh_token: 'def5020094aeeb348896b43c71448c51f8448cf86b4b6b82450a5eb7c2eb3b453c161ca1fa3f9ebb5f51a798f3bce8e06d7132e788e7ec9d887479588f163d72a320ea84ba137107d575aee4dd16436b91e92a84f619de6fac0c21689c2f8a91c5da48c51452602a4926bb4e3510fd096e2998401ba81f52c13bf0e27feb0a63a95c6c7dfd96aace838ea465449f9ab3f4b7c370ba5fbf5815c291c46bf868877ec6f02020e54f1eea3e0cda472f216255f52645dc8056f480a49d617ae4b3dbe9e18296c9b2f676d4c5ad339974bc88e07d15c733b3c3e686ed1951b9367cbd5c98daccf4b6e1984d12645f0c371478ec9f5afd25a3f004d14e2766c5bc5692c2b0173914f8110174201499e6b7b4f775d262bfed490b147113cf3788aff670620b0dd77646b2b137e9fdfbe7d046fce48b55a11a28a39f6be4282a8d8ab21b5fb69aa673005c26b59a955e71bd7a833904d1b5219d9c763a7a492e4e7d8b40c8b15d4c'
    }
  },
  currentUser () {
    return {
      user_profile: {
        p_coduser: 'TJS00001',
        p_codempid: '16010',
        username: {
          en: 'Mr. Somchai  Kemgladthongdaeng',
          th: 'นายสมชาย เข็มกลัดทองแดง'
        },
        p_codpos: '0310',
        position: {
          en: 'Project Manager',
          th: 'Project Manager'
        },
        p_codcomp: 'PPS-IMP',
        company: {
          en: 'Implement',
          th: 'Implement'
        },
        usrcom: {
          0: 'PPS'
        },
        p_lrunning: Math.floor((Math.random() * 2) + 1)
      },
      user_permission: {
        edit: []
      }
    }
  },
  signout () {
    return {
      message: 'logout success'
    }
  },
  baseConn () {
    return assetsLabels.baseConn
  },
  forgotPassword () {
    return {
      message: ''
      // message: 'Send OTP Code to your email completely, please type OTP Code to continue process.'
      // message: 'ระบบได้ส่ง รหัส OTP Code ไปที่อีเมลของคุณ กรุณากรอก รหัส OTP Code เพื่อยืนยันการทำรายการ'
    }
  },
  forgotPasswordVerify () {
    return {
      message: ''
      // message: 'Verify your OTP Success, please create your new password.'
    }
  },
  changePassword () {
    return {
      message: 'HR2410 Data has been completely Corrected.'
    }
  },
  authVueMiddleware () {
    return {
      status: 'SUCCESS',
      redirect: ''
    }
  },
  token () {
    return {
      refresh_token: '1',
      access_token: '1'
    }
  }
}
