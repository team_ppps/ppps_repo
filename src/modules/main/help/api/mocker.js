import assetsLabels from 'assets/js/labels'

export const mocker = {
  currentUser () {
    return {
      status: 1,
      p_coduser: 'TJS00001',
      p_codpswd: '11111',
      p_codempid: '16010',
      username: {
        en: 'Mr. Somchai  Kemgladthongdaeng',
        th: 'นายสมชาย เข็มกลัดทองแดง'
      },
      p_codpos: '0310',
      position: {
        en: 'Project Manager',
        th: 'Project Manager'
      },
      p_codcomp: 'PPS-IMP',
      company: {
        en: 'Implement',
        th: 'Implement'
      },
      usrcom: {
        0: 'PPS'
      },
      p_lrunning: Math.floor((Math.random() * 2) + 1)
    }
  },
  signout () {
    return {
      message: 'logout success'
    }
  },
  baseConn () {
    return assetsLabels.baseConn
  },
  forgotPassword () {
    return {
      message: ''
      // message: 'Send OTP Code to your email completely, please type OTP Code to continue process.'
      // message: 'ระบบได้ส่ง รหัส OTP Code ไปที่อีเมลของคุณ กรุณากรอก รหัส OTP Code เพื่อยืนยันการทำรายการ'
    }
  },
  forgotPasswordVerify () {
    return {
      message: ''
      // message: 'Verify your OTP Success, please create your new password.'
    }
  },
  changePassword () {
    return {
      message: 'HR2410 Data has been completely Corrected.'
    }
  },
  authVueMiddleware () {
    return {
      status: 'SUCCESS',
      redirect: ''
    }
  }
}
