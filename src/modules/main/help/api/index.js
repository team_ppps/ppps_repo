import { instance, endpoint, isMockLogin, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  postCurrentUser (username, password) {
    if (isMockLogin) return generator(mocker.currentUser())
    return instance().post(endpoint + '/login/signin', { 'coduser': username, 'codpswd': password })
  },
  postLogout () {
    if (isMockLogin) return generator(mocker.signout())
    return instance().post(endpoint + '/login/signout')
  },
  getBaseConnection () {
    if (isMockLogin) return generator(mocker.baseConn())
    return instance().get(endpoint + '/login/baseConn')
  },
  postForgotPassword (params) {
    if (isMockLogin) return generator(mocker.forgotPassword())
    return instance().post(endpoint + '/login/forgotpassword', params)
  },
  postForgotPasswordVerify (params) {
    if (isMockLogin) return generator(mocker.forgotPasswordVerify())
    return instance().post(endpoint + '/login/forgotPasswordVerify', params)
  },
  postChangePassword (params) {
    if (isMockLogin) return generator(mocker.changePassword())
    return instance().post(endpoint + '/login/changepassword', params)
  },
  postAuthVueMiddleware (codapp) {
    if (isMockLogin) return generator(mocker.authVueMiddleware())
    return instance().get(endpoint + '/login/authVueMiddleware/' + codapp)
  }
}
