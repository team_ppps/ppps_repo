import { instance, endpoint, isMockLogin, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getAutoLoginParams (runno) {
    if (isMockLogin) return generator(mocker.autoLoginParams())
    return instance().get(endpoint + '/autologin/' + runno)
  }
}
