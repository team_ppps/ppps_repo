import * as types from './mutation-types'
import * as loginTypes from 'modules/main/login/store/mutation-types'
import autologin from '../api'
import { router } from 'register/router'

export default {
  state: {
    loginParams: {
      coduser: '',
      codpswd: '',
      codapp: ''
    }
  },
  getters: {
    [types.GET_AUTOLOGIN] (state) {
      return state.loginParams
    }
  },
  mutations: {
    [types.SET_AUTOLOGIN] (state, loginParams) {
      state.loginParams = loginParams
    }
  },
  actions: {
    [types.RECEIVED_AUTOLOGIN] ({ commit, dispatch }, runno) {
      autologin.getAutoLoginParams(runno)
        .then((response) => {
          const loginParams = JSON.parse(response.request.response)
          let codpswd = loginParams.codpswd
          let encRound = codpswd.substr(0, 1)
          let codpswdEnc = codpswd.substr(1)
          for (let i = 1; i <= encRound; i++) {
            codpswdEnc = window.atob(codpswdEnc)
          }

          let formUser = {
            coduser: loginParams.coduser,
            codpswd: codpswdEnc,
            codapp: (typeof loginParams.codapp !== 'undefined') ? (loginParams.codapp).toLowerCase() : '',
            autologin: 'Y'
          }
          dispatch(loginTypes.POST_CURRENT_USER, formUser)
        })
        .catch((response) => {
          router.push({ name: 'login' })
        })
    }
  }
}
