let login = require('./login/store').default
let template = require('./template/store').default
let home = require('./home/store').default

module.exports = {
  login,
  template,
  home
}
