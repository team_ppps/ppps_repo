let hrpm1ee = require('./hrpm1ee/store').default
let hrpm4ce = require('./hrpm4ce/store').default
let hrpm15e = require('./hrpm15e/store').default
let hrpm38e = require('./hrpm38e/store').default
let hrpmb2e = require('./hrpmb2e/store').default
let hrpmb3e = require('./hrpmb3e/store').default
let hrpmb5e = require('./hrpmb5e/store').default
let hrpmb8e = require('./hrpmb8e/store').default
let hrpmc2e = require('./hrpmc2e/store').default
let hrpmb1e = require('./hrpmb1e/store').default
let hrpm36x = require('./hrpm36x/store').default
let hrpm33r = require('./hrpm33r/store').default
let hrpm55r = require('./hrpm55r/store').default
let hrpm4ge = require('./hrpm4ge/store').default
let hrpm4ie = require('./hrpm4ie/store').default
let hrpm4de = require('./hrpm4de/store').default
let hrpmc1b = require('./hrpmc1b/store').default
let hrpm32u = require('./hrpm32u/store').default
let hrpm31e = require('./hrpm31e/store').default

module.exports = {
  hrpm1ee,
  hrpm4ce,
  hrpm15e,
  hrpm38e,
  hrpmb2e,
  hrpmb3e,
  hrpmb5e,
  hrpmb8e,
  hrpmc2e,
  hrpmb1e,
  hrpm36x,
  hrpm33r,
  hrpm55r,
  hrpm4ge,
  hrpm4ie,
  hrpm4de,
  hrpmc1b,
  hrpm32u,
  hrpm31e
}
