export let hrpmb2eColumns = () => {
  let indexColumns = [{
    key: 'dteeffec',
    name: '',
    labelCodapp: 'HRPMB2E1',
    labelIndex: '40',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'numseq',
    name: '',
    labelCodapp: 'HRPMB2E1',
    labelIndex: '50',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codtrn',
    name: '',
    labelCodapp: 'HRPMB2E1',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codcomp',
    name: '',
    labelCodapp: 'HRPMB2E1',
    labelIndex: '70',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codpos',
    name: '',
    labelCodapp: 'HRPMB2E1',
    labelIndex: '80',
    style: 'min-width: 100px;'
  }, {
    key: 'numannou',
    name: '',
    labelCodapp: 'HRPMB2E1',
    labelIndex: '90',
    style: 'min-width: 100px;'
  }]

  let detailTab2Columns = [{
    key: 'code',
    name: '',
    labelCodapp: 'HRPMB2E3',
    labelIndex: '30',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_code',
    name: '',
    labelCodapp: 'HRPMB2E3',
    labelIndex: '40',
    style: 'min-width: 190px;'
  }, {
    key: 'unit',
    name: '',
    labelCodapp: 'HRPMB2E3',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: 'v_amt1',
    name: '',
    labelCodapp: 'HRPMB2E3',
    labelIndex: '60',
    style: 'min-width: 100px;',
    inlineType: 'text'
  }, {
    key: 'v_adjamt1',
    name: '',
    labelCodapp: 'HRPMB2E3',
    labelIndex: '70',
    style: 'min-width: 100px;',
    inlineType: 'text'
  }, {
    key: 'v_netamt1',
    name: '',
    labelCodapp: 'HRPMB2E3',
    labelIndex: '80',
    style: 'min-width: 100px;',
    inlineType: 'text'
  }]

  return {
    en: {
      index: indexColumns,
      detailTab2: detailTab2Columns
    },
    th: {
      index: indexColumns,
      detailTab2: detailTab2Columns
    }
  }
}

