import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLabels () {
    if (isMock) return generator(mocker.labels())
    return instance().get(endpoint + '/hrpmb2e/labels')
  },
  getIndex (searchParams) {
    if (isMock) return generator(mocker.index())
    return instance().get(endpoint + '/hrpmb2e/index')
  },
  getIndexHead (searchParams) {
    if (isMock) return generator(mocker.indexHead())
    const params = searchParams.p_codempid + '/' + searchParams.p_yearstrt + '/' + searchParams.p_yearend
    return instance().get(endpoint + '/hrpmb2e/indexHead/' + params)
  },
  getDetailCreate () {
    return generator(mocker.detailCreate())
  },
  getDetail (searchParams) {
    if (isMock) return generator(mocker.detail(searchParams.p_codsecu))
    const params = searchParams.p_codsecu + '/' + searchParams.p_desc_codsecu + '/' + searchParams.p_v_syncond
    return instance().get(endpoint + '/hrpmb2e/detail/' + params)
  },
  getDropdowns () {
    if (isMock) return generator(mocker.dropdowns())
    return instance().get(endpoint + '/hrpmb2e/dropdowns')
  },
  getPopupOtherData (popupParams) {
    if (isMock) return generator(mocker.popupOtherData())
    return instance().get(endpoint + '/hrpmb2e/detailPopupOtherData')
  },
  getPopupCreateTable (popupParams) {
    if (isMock) return generator(mocker.popupCreateTable())
    return instance().get(endpoint + '/hrpmb2e/detailPopupCreateTable')
  },
  saveDetail ({ indexSelected, detail }) {
    let saveParams = {
      param_json: JSON.stringify({
        p_codsecu: indexSelected.codsecu
      })
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmb2e/detailSave', saveParams)
  },
  saveCreateTablePopup (dataRowsHasFlg) {
    var saveParams = {
      'param_json': JSON.stringify(dataRowsHasFlg)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmb2e/saveCreateTable', saveParams)
  },
  deleteIndex (dataRowsHasFlg) {
    let sendParams = {
      param_json: JSON.stringify(dataRowsHasFlg)
    }
    if (isMock) return generator(mocker.delete())
    return instance().post(endpoint + '/hrpmb2e/delete', sendParams)
  }
}
