import hrpmb2eLabels from '../assets/labels'
// import faker from 'faker'
import moment from 'moment'
import assetsLabels from 'assets/js/labels'

export const mocker = {
  labels () {
    let datatypeDropdown = {
      '1': 'VARCHAR2',
      '2': 'DATE',
      '3': 'NUMBER',
      '4': 'DECIMAL'
    }
    return {
      labels: hrpmb2eLabels,
      dropdowns: {
        datatype: {
          en: datatypeDropdown,
          th: datatypeDropdown
        }
      }
    }
  },
  index () {
    let mockRows = []
    const mockTotal = 2
    mockRows.push({
      dteeffec: '13/07/2017',
      numseq: '1',
      desc_codtrn: 'ปรับรายได้',
      desc_codcomp: 'ทดสอบบริษัท',
      desc_codpos: 'Managing Director',
      numannou: 'SAL00002'
    })
    mockRows.push({
      dteeffec: '01/03/2017',
      numseq: '1',
      desc_codtrn: 'ปรับตำแหน่ง',
      desc_codcomp: 'บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด',
      desc_codpos: 'Managing Director',
      numannou: 'TRA00003'
    })
    mockRows.push({
      dteeffec: '01/03/2013',
      numseq: '2',
      desc_codtrn: 'ปรับตำแหน่ง',
      desc_codcomp: 'บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด',
      desc_codpos: 'Managing Director',
      numannou: 'TRA00001'
    })

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  indexHead () {
    let mockIndexHead = {
      codempid: '001002',
      desc_codempid: 'Test Test',
      yearstrt: '2013',
      yearend: '2017'
    }

    return mockIndexHead
  },
  detailCreate () {
    let mockDetail = {
      codempid: '',
      dteeffec: '',
      numseq: '',
      codtrn: ''
    }
    let mockDetailTab1 = {
      numannou: '',
      codcomp: '',
      codpos: '',
      codjob: '',
      numlvl: '',
      codbrlc: '',
      codempmt: '',
      codcalen: '',
      codtypemp: '',
      codpayrol: '',
      codjobgrad: '',
      codgl: '',
      tistaemp: '',
      tistapost2: '',
      desnote: '',
      qtydue: '',
      qtyexpand: '',
      qtymthrq: '',
      dteduepr: '',
      scoreget: '',
      ticodrespr: '',
      flgadjin: '',
      codcause: ''
    }
    let mockDetailTab2 = {
      codcurr: ''
    }
    let mockRowsTab2 = []
    const mockTotalTab2 = 2
    mockRowsTab2.push({
      code: '',
      desc_code: '',
      unit: '',
      v_amt1: '',
      v_adjamt1: '',
      v_netamt1: ''
    })

    return {
      detail: mockDetail,
      tab1: mockDetailTab1,
      tab2: {
        detail: mockDetailTab2,
        table: {
          total: mockTotalTab2,
          rows: mockRowsTab2
        }
      }
    }
  },
  detail () {
    let mockDetail = {
      codempid: '001002',
      dteeffec: moment().format('DD/MM/YYYY'),
      numseq: '1',
      codtrn: ''
    }
    let mockDetailTab1 = {
      numannou: '021/212',
      codcomp: '',
      codpos: '',
      codjob: '',
      numlvl: '90',
      codbrlc: '',
      codempmt: '',
      codcalen: '',
      codtypemp: '',
      codpayrol: '',
      codjobgrad: '',
      codgl: '',
      tistaemp: '1',
      tistapost2: '3',
      desnote: '',
      qtydue: '90',
      qtyexpand: '1',
      qtymthrq: '3',
      dteduepr: '',
      scoreget: '90',
      codrespr: '1',
      flgadjin: '',
      codcause: 'ไม่ผ่านการทดลองงาน เนื่องจากไม่สามารถปฏิบัติหน้าที่ที่ได้รับมอบหมายจากหัวหน้างานได้ และขาดความรับผิดชอบต่อหน้าที่'
    }
    let mockDetailTab2 = {
      codcurr: 'THB-BATH',
      incomm_amt1: '30,000',
      incomm_adjamt1: '3,500',
      incomm_netamt1: '33,500',
      incomd_amt1: '1,000',
      incomd_adjamt1: '116.67',
      incomd_netamt1: '1,116.67',
      wage_amt1: '125',
      wage_adjamt1: '14.58',
      wage_netamt1: '139.58'
    }
    let mockRowsTab2 = []
    const mockTotalTab2 = 3
    mockRowsTab2.push({
      code: '01',
      desc_code: 'เงินเดือน',
      unit: 'ต่อเดือน',
      v_amt1: '30,000',
      v_adjamt1: '2,000',
      v_netamt1: '32,000'
    })
    mockRowsTab2.push({
      code: '02',
      desc_code: 'ค่าครองชีพ',
      unit: 'ต่อเดือน',
      v_amt1: '0.00',
      v_adjamt1: '0.00',
      v_netamt1: '0.00'
    })
    mockRowsTab2.push({
      code: '03',
      desc_code: 'ค่าตำแหน่ง',
      unit: 'ต่อเดือน',
      v_amt1: '0.00',
      v_adjamt1: '1,500',
      v_netamt1: '1,500'
    })

    return {
      detail: mockDetail,
      tab1: mockDetailTab1,
      tab2: {
        detail: mockDetailTab2,
        table: {
          total: mockTotalTab2,
          rows: mockRowsTab2
        }
      }
    }
  },
  dropdowns () {
    const mockTiStaemp = {
      '1': 'พนักงานใหม่',
      '2': 'ทดลองงาน',
      '3': 'ปัจจุบัน',
      '4': 'พ้นสภาพ'
    }
    const mockTiStapost2 = {
      '1': 'แต่งตั้งปกติ',
      '2': 'รักษาการณ์',
      '3': 'ตำแหน่งควบ'
    }
    const mockCodrespr = {
      '1': 'ผ่าน',
      '2': 'ไม่ผ่าน',
      '3': 'ขยายเวลา'
    }

    return {
      tistaemp: {
        en: mockTiStaemp,
        th: mockTiStaemp
      },
      tistapost2: {
        en: mockTiStapost2,
        th: mockTiStapost2
      },
      codrespr: {
        en: mockCodrespr,
        th: mockCodrespr
      }
    }
  },
  delete () {
    return assetsLabels.mockPostMessage
  },
  save () {
    return assetsLabels.mockPostMessage
  }
}
