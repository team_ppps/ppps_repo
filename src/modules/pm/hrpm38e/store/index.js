import * as types from './mutation-types'
import hrpm38e from '../api'
import swal from 'sweetalert'
import { hrpm38eColumns } from '../assets/columns'
import hrpm38eLabels from '../assets/labels'
import assetsLabels from 'assets/js/labels'
import { constLabels } from 'assets/js/constLabels'
// import axios from 'axios'

export default {
  state: {
    indexDisp: true,
    detailDisp: true,
    detailDisabled: true,
    labels: hrpm38eLabels,
    columns: hrpm38eColumns(),
    index: {
      codcompy: '',
      codaward: ''
    },
    detail: {
      tab1: {
        detail: {
          codform: ''
        },
        table: {
          total: 0,
          rows: []
        }
      },
      tab2: {
        detail: {},
        table: {
          total: 0,
          rows: []
        }
      }
    },
    dropdowns: {
      appvby: {
        en: {},
        th: {}
      }
    }
  },
  getters: {
    [types.GET_HRPM38E_INDEX_DISP] (state) {
      return state.indexDisp
    },
    [types.GET_HRPM38E_DETAIL_DISP] (state) {
      return state.detailDisp
    },
    [types.GET_HRPM38E_COLUMNS] (state) {
      return state.columns
    },
    [types.GET_HRPM38E_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPM38E_INDEX] (state) {
      return state.index
    },
    [types.GET_HRPM38E_DETAIL] (state) {
      return state.detail
    },
    [types.GET_HRPM38E_DROPDOWNS] (state) {
      return state.dropdowns
    },
    [types.GET_HRPM38E_DETAIL_DISABLED] (state) {
      return state.detailDisabled
    }
  },
  mutations: {
    [types.SET_HRPM38E_INDEX_DISP] (state, indexDisp) {
      state.indexDisp = indexDisp
    },
    [types.SET_HRPM38E_DETAIL_DISP] (state, detailDisp) {
      state.detailDisp = detailDisp
    },
    [types.SET_HRPM38E_COLUMNS] (state, { labels, dropdowns }) {
      for (var keyLang of assetsLabels.allLang) {
        let dropdownSets = [
          { key: 'appvby', value: dropdowns.appvby[keyLang] }
        ]
        state.columns[keyLang].detail1 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].detail1, labels[keyLang], dropdownSets)
        state.columns[keyLang].detail2 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].detail2, labels[keyLang])
      }
    },
    [types.SET_HRPM38E_LABELS] (state, labels) {
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPM38E_INDEX] (state, index) {
      state.index = index
    },
    [types.SET_HRPM38E_DETAIL] (state, detail) {
      state.detail = detail
    },
    [types.SET_HRPM38E_DROPDOWNS] (state, dropdowns) {
      state.dropdowns = dropdowns
    },
    [types.SET_HRPM38E_DETAIL_DISABLED] (state, detailDisabled) {
      state.detailDisabled = detailDisabled
    }
  },
  actions: {
    [types.TOGGLE_HRPM38E_PAGE] ({ commit }, page) {
      let indexDisp = false
      let detailDisp = false
      switch (page) {
        case 'index':
          indexDisp = true
          break
        case 'detail':
          detailDisp = true
          break
      }
      commit(types.SET_HRPM38E_INDEX_DISP, indexDisp)
      commit(types.SET_HRPM38E_DETAIL_DISP, detailDisp)
    },
    [types.RECEIVED_HRPM38E_COLUMNS_LABELS] ({ commit }) {
      hrpm38e.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM38E_COLUMNS, { labels: data.labels, dropdowns: data.dropdowns })
          commit(types.SET_HRPM38E_LABELS, data.labels)
          commit(types.SET_HRPM38E_DROPDOWNS, data.dropdowns)
        })
        .catch((response) => {
          const data = response.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM38E_INDEX] ({ commit }, searchParams) {
      hrpm38e.getIndex(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM38E_INDEX, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM38E_DETAIL] ({ commit }, searchParams) {
      hrpm38e.getDetail(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM38E_DETAIL, data)
          // commit(types.SET_HRPM38E_DETAIL_DISABLED, false)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.SAVE_HRPM38E_DETAIL] ({ commit, dispatch }, { detail, searchParams }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpm38e.saveDetail({detail, searchParams})
            .then((response) => {
              const data = JSON.parse(response.request.response)
              // dispatch(types.RECEIVED_HRPM38E_DETAIL, searchParams)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((response) => {
              const data = response.response.data
              swal({title: '', text: data.response, html: true, type: 'error'})
            })
        }
      })
    }
  }
}
