import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLabels () {
    if (isMock) return generator(mocker.labels())
    return instance().get(endpoint + '/hrpm38e/labels')
  },
  getIndex (searchParams) {
    if (isMock) return generator(mocker.index())
    let dteeffec = ''
    if (typeof searchParams !== 'undefined') {
      dteeffec = (typeof searchParams.dteeffec !== 'undefined' ? searchParams.dteeffec : dteeffec)
    }
    return instance().get(endpoint + '/hrpm38e/index/' + dteeffec)
  },
  getDetailCreate (searchParams) {
    let param = searchParams.p_codcompy
    if (isMock) return generator(mocker.create())
    return instance().get(endpoint + '/hrpm38e/detail/' + param)
  },
  getDetail (searchParams) {
    let param = searchParams.p_codcompy
    if (isMock) return generator(mocker.detail())
    return instance().get(endpoint + '/hrpm38e/detail/' + param)
  },
  // getDropdowns () {
  //   if (isMock) return generator(mocker.dropdowns())
  //   return instance().get(endpoint + '/hrpm38e/dropdowns')
  // },
  saveDetail (detail) {
    let saveParams = {
      param_json: JSON.stringify({
        p_codcomp: detail.codcomp
      })
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpm38e/detailSave', saveParams)
  }
}
