import hrpm38eLabels from '../assets/labels'
// import faker from 'faker'
import assetslabels from 'assets/js/labels'
// import moment from 'moment'

export const mocker = {
  labels () {
    let mockAppvby = {
      '1': 'หัวหน้างาน',
      '2': 'หัวหน้าตามโครงสร้างสายงาน',
      '3': 'หน่วยงานและตำแหน่ง',
      '4': 'พนักงาน',
      '5': 'ประเมิณตัวเอง'
    }
    return {
      labels: hrpm38eLabels,
      dropdowns: {
        appvby: {
          en: mockAppvby,
          th: mockAppvby
        }
      }
    }
  },
  index () {
    let mockIndex = {
      codcomp: '001',
      codpos: '0001',
      codempid: '28/03/2017'
    }

    return mockIndex
  },
  detail () {
    let detailTab1 = {
      codform: '0001-แบบฟอร์มประเมินผลการปฎิบัติงาน',
      qtyapp: '2',
      qtyday: '30',
      calscore: '1',
      lowscore: '50'
    }
    let mockTab1Rows = []
    const mockTab1Total = 2
    mockTab1Rows.push({
      flgappr: '1',
      codcomp: '',
      codpos: '0',
      codempid: '0',
      desc_codempid: 'test test'
    })
    let detailTab2 = {
      codform: '0001-แบบฟอร์มประเมินผลการปฎิบัติงาน',
      qtyapp: '2',
      qtyday: '30',
      calscore: '1',
      lowscore: '50'
    }
    let mockTab2Rows = []
    const mockTab2Total = 2
    mockTab2Rows.push({
      flgappr: '2',
      codcomp: '',
      codpos: '',
      codempid: '',
      desc_codempid: 'test test'
    })

    return {
      tab1: {
        detail: detailTab1,
        table: {
          total: mockTab1Total,
          rows: mockTab1Rows
        }
      },
      tab2: {
        detail: detailTab2,
        table: {
          total: mockTab2Total,
          rows: mockTab2Rows
        }
      }
    }
  },
  save () {
    return assetslabels.mockPostMessage
  },
  delete () {
    return assetslabels.mockPostMessage
  }
}
