export let hrpm38eColumns = () => {
  let detailTab1Columns = [{
    key: 'appvby',
    name: '',
    labelCodapp: 'HRPM38EC1',
    labelIndex: '100',
    style: 'min-width: 100px;',
    inlineType: 'select',
    inlineDropdown: {}
  }, {
    key: 'codcomp',
    name: '',
    labelCodapp: 'HRPM38EC1',
    labelIndex: '110',
    style: 'min-width: 200px;',
    inlineType: 'lov',
    lovType: 'codcomp'
  }, {
    key: 'codpos',
    name: '',
    labelCodapp: 'HRPM38EC1',
    labelIndex: '120',
    style: 'min-width: 100px;',
    inlineType: 'lov',
    lovType: 'codpos'
  }, {
    key: 'codempid',
    name: '',
    labelCodapp: 'HRPM38EC1',
    labelIndex: '130',
    style: 'min-width: 100px;',
    inlineType: 'lov',
    lovType: 'codempid'
  }, {
    key: 'desc_codempid',
    name: '',
    labelCodapp: 'HRPM38EC1',
    labelIndex: '140',
    style: 'min-width: 100px;'
  }]

  let detailTab2Columns = [{
    key: 'flgappr',
    name: '',
    labelCodapp: 'HRPM38EC2',
    labelIndex: '70',
    style: 'min-width: 100px;',
    inlineType: 'select',
    inlineDropdown: {}
  }, {
    key: 'codcomp',
    name: '',
    labelCodapp: 'HRPM38EC2',
    labelIndex: '80',
    style: 'min-width: 200px;',
    inlineType: 'lov',
    lovType: 'codcomp'
  }, {
    key: 'codpos',
    name: '',
    labelCodapp: 'HRPM38EC2',
    labelIndex: '90',
    style: 'min-width: 100px;',
    inlineType: 'lov',
    lovType: 'codpos'
  }, {
    key: 'codempid',
    name: '',
    labelCodapp: 'HRPM38EC2',
    labelIndex: '100',
    style: 'min-width: 100px;',
    inlineType: 'lov',
    lovType: 'codempid'
  }, {
    key: 'desc_codempid',
    name: '',
    labelCodapp: 'HRPM38EC2',
    labelIndex: '110',
    style: 'min-width: 100px;'
  }]

  return {
    en: {
      detail1: detailTab1Columns,
      detail2: detailTab2Columns
    },
    th: {
      detail1: detailTab1Columns,
      detail2: detailTab2Columns
    }
  }
}
