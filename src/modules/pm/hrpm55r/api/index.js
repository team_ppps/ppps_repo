import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLabels () {
    if (isMock) return generator(mocker.labels())
    return instance().get(endpoint + '/hrpm55r/labels')
  },
  getIndex (searchParams) {
    if (isMock) return generator(mocker.index())
    let dteeffec = ''
    if (typeof searchParams !== 'undefined') {
      dteeffec = (typeof searchParams.dteeffec !== 'undefined' ? searchParams.dteeffec : dteeffec)
    }
    return instance().get(endpoint + '/hrpm55r/index/' + dteeffec)
  },
  getDetailCreate (searchParams) {
    let param = searchParams.p_codcompy
    if (isMock) return generator(mocker.create())
    return instance().get(endpoint + '/hrpm55r/detail/' + param)
  },
  getDetail (searchParams) {
    let param = searchParams.p_codcompy
    if (isMock) return generator(mocker.detail())
    return instance().get(endpoint + '/hrpm55r/detail/' + param)
  },
  getPopupConditionCurrent (popupParams) {
    if (isMock) return generator(mocker.popupConditionCurrent())
    return instance().get(endpoint + '/hrpm55r/detailPopupConditionCurrent')
  },
  saveDetail (detail) {
    let sendParams = {
      param_json: JSON.stringify({
        // codcomp: detail.codcomp
      })
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpm55r/detailSave', sendParams)
  }
}
