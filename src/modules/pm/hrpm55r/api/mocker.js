import hrpm55rLabels from '../assets/labels'
// import faker from 'faker'
import assetslabels from 'assets/js/labels'
// import moment from 'moment'

export const mocker = {
  labels () {
    return {
      labels: hrpm55rLabels
    }
  },
  index () {
    let mockIndex = {
      codcomp: '',
      codempid: '16010',
      stdate: '',
      endate: ''
    }

    return mockIndex
  },
  detail () {
    let mockTab1Rows = []
    const mockTab1Total = 2
    mockTab1Rows.push({
      image: 'static/image/employee/emp_img16.jpg',
      dteapprov: '06/02/2017',
      codempid: '5800001',
      desc_codempid: 'น.ส.ทดสอบ สำหรับทดสอบ',
      codcomp: 'บริษัทพิเพิลพลัสซอฟต์แวร์ จำกัด',
      codincome: 'ไม่แสดง',
      object: 'test',
      numdoc: '',
      desc_typcertif: 'หนังสือแสดงรายได้'
    })
    mockTab1Rows.push({
      image: 'static/image/employee/emp_img28.jpg',
      dteapprov: '04/02/2017',
      codempid: '5800002',
      desc_codempid: 'นายกัปตัน พิเพิลพลัส',
      codcomp: 'บริษัทพิเพิลพลัสซอฟต์แวร์ จำกัด',
      codincome: 'ไม่แสดง',
      object: 'test',
      numdoc: '',
      desc_typcertif: 'หนังสือแสดงรายได้'
    })
    let detailTab2 = {
      tempfile: 'HRPM55INCT',
      dtehmref: '12/01/2018',
      numdoc: '1234'
    }
    let mockTab2Rows = []
    const mockTab2Total = 3
    mockTab2Rows.push({
      descript: 'ชื่อผู้ลงนาม',
      value: ''
    })
    mockTab2Rows.push({
      descript: 'หมายเหตุ',
      value: ''
    })
    mockTab2Rows.push({
      descript: 'ตำแหน่ง',
      value: ''
    })

    return {
      tab1: {
        total: mockTab1Total,
        rows: mockTab1Rows
      },
      tab2: {
        detail: detailTab2,
        table: {
          total: mockTab2Total,
          rows: mockTab2Rows
        }
      }
    }
  },
  popupConditionCurrent () {
    let mockConditionCurrentRows = []
    const mockConditionCurrentTotal = 2
    mockConditionCurrentRows.push({
      codcompy: '001',
      codaward: '0001',
      dteeffec: '29/06/2007'
    })
    mockConditionCurrentRows.push({
      codcompy: 'PJW',
      codaward: '0005',
      dteeffec: '14/02/2015'
    })

    return {
      total: mockConditionCurrentTotal,
      rows: mockConditionCurrentRows
    }
  },
  save () {
    return assetslabels.mockPostMessage
  },
  delete () {
    return assetslabels.mockPostMessage
  }
}
