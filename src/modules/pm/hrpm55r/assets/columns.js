export let hrpm55rColumns = () => {
  let detailTab1Columns = [{
    key: 'dteapprov',
    name: '',
    labelCodapp: 'HRPM55RP1',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }, {
    key: 'image',
    name: '',
    labelCodapp: 'HRPM55RP1',
    labelIndex: '5',
    style: 'min-width: 100px;',
    tagType: 'image'
  }, {
    key: 'codempid',
    name: '',
    labelCodapp: 'HRPM55RP1',
    labelIndex: '70',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codempid',
    name: '',
    labelCodapp: 'HRPM55RP1',
    labelIndex: '80',
    style: 'min-width: 150px;'
  }, {
    key: 'codcomp',
    name: '',
    labelCodapp: 'HRPM55RP1',
    labelIndex: '90',
    style: 'min-width: 100px;'
  }, {
    key: 'object',
    name: '',
    labelCodapp: 'HRPM55RP1',
    labelIndex: '100',
    style: 'min-width: 100px;'
  }, {
    key: 'numdoc',
    name: '',
    labelCodapp: 'HRPM55RP1',
    labelIndex: '110',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_typcertif',
    name: '',
    labelCodapp: 'HRPM55RP1',
    labelIndex: '120',
    style: 'min-width: 100px;'
  }]

  let detailTab2Columns = [{
    key: 'descript',
    name: '',
    labelCodapp: 'HRPM55RP2',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: 'value',
    name: '',
    labelCodapp: 'HRPM55RP2',
    labelIndex: '60',
    style: 'min-width: 200px;',
    inlineType: 'text'
  }]

  return {
    en: {
      detailTab1: detailTab1Columns,
      detailTab2: detailTab2Columns
    },
    th: {
      detailTab1: detailTab1Columns,
      detailTab2: detailTab2Columns
    }
  }
}
