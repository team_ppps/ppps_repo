import * as types from './mutation-types'
import hrpm55r from '../api'
import swal from 'sweetalert'
import { hrpm55rColumns } from '../assets/columns'
import hrpm55rLabels from '../assets/labels'
import assetsLabels from 'assets/js/labels'
import { constLabels } from 'assets/js/constLabels'
// import axios from 'axios'

export default {
  state: {
    indexDisp: true,
    detailDisp: true,
    detailDisabled: true,
    labels: hrpm55rLabels,
    columns: hrpm55rColumns(),
    index: {
      codcomp: '',
      codempid: ''
    },
    detail: {
      tab1: {
        total: 0,
        rows: []
      },
      tab2: {
        detail: {
          tempfile: ''
        },
        table: {
          total: 0,
          rows: []
        }
      }
    }
  },
  getters: {
    [types.GET_HRPM55R_INDEX_DISP] (state) {
      return state.indexDisp
    },
    [types.GET_HRPM55R_DETAIL_DISP] (state) {
      return state.detailDisp
    },
    [types.GET_HRPM55R_COLUMNS] (state) {
      return state.columns
    },
    [types.GET_HRPM55R_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPM55R_INDEX] (state) {
      return state.index
    },
    [types.GET_HRPM55R_DETAIL] (state) {
      return state.detail
    },
    [types.GET_HRPM55R_DETAIL_DISABLED] (state) {
      return state.detailDisabled
    }
  },
  mutations: {
    [types.SET_HRPM55R_INDEX_DISP] (state, indexDisp) {
      state.indexDisp = indexDisp
    },
    [types.SET_HRPM55R_DETAIL_DISP] (state, detailDisp) {
      state.detailDisp = detailDisp
    },
    [types.SET_HRPM55R_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.columns[keyLang].detailTab1 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].detailTab1, labels[keyLang])
        state.columns[keyLang].detailTab2 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].detailTab2, labels[keyLang])
      }
    },
    [types.SET_HRPM55R_LABELS] (state, labels) {
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPM55R_INDEX] (state, index) {
      state.index = index
    },
    [types.SET_HRPM55R_DETAIL] (state, detail) {
      state.detail = detail
    },
    [types.SET_HRPM55R_DETAIL_DISABLED] (state, detailDisabled) {
      state.detailDisabled = detailDisabled
    }
  },
  actions: {
    [types.TOGGLE_HRPM55R_PAGE] ({ commit }, page) {
      let indexDisp = false
      let detailDisp = false
      switch (page) {
        case 'index':
          indexDisp = true
          break
        case 'detail':
          detailDisp = true
          break
      }
      commit(types.SET_HRPM55R_INDEX_DISP, indexDisp)
      commit(types.SET_HRPM55R_DETAIL_DISP, detailDisp)
    },
    [types.RECEIVED_HRPM55R_COLUMNS_LABELS] ({ commit }) {
      hrpm55r.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM55R_COLUMNS, data.labels)
          commit(types.SET_HRPM55R_LABELS, data.labels)
        })
        .catch((response) => {
          const data = response.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM55R_INDEX] ({ commit }, searchParams) {
      hrpm55r.getIndex(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM55R_INDEX, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM55R_DETAIL] ({ commit }, searchParams) {
      hrpm55r.getDetail(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM55R_DETAIL, data)
          // commit(types.SET_HRPM55R_DETAIL_DISABLED, false)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.SAVE_HRPM55R_DETAIL] ({ commit, dispatch }, detail) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpm55r.saveDetail(detail)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              // dispatch(types.RECEIVED_HRCO01E_INDEX, {})
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((response) => {
              const data = response.response.data
              swal({title: '', text: data.response, html: true, type: 'error'})
            })
        }
      })
    }
  }
}
