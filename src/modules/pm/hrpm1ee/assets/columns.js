export let hrpm1eeColumns = () => {
  let indexColumns = [{
    key: 'codasset',
    name: '',
    labelCodapp: 'HRPM1EEC1',
    labelIndex: '20',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'descasseet',
    name: '',
    labelCodapp: 'HRPM1EEC1',
    labelIndex: '30',
    style: 'min-width: 200px;'
  }, {
    key: 'desctypass',
    name: '',
    labelCodapp: 'HRPM1EEC1',
    labelIndex: '40',
    style: 'min-width: 150px;'
  }, {
    key: 'dterec',
    name: '',
    labelCodapp: 'HRPM1EEC1',
    labelIndex: '50',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'srcasset',
    name: '',
    labelCodapp: 'HRPM1EEC1',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }, {
    key: 'status',
    name: '',
    labelCodapp: 'HRPM1EEC1',
    labelIndex: '70',
    style: 'min-width: 100px;'
  }, {
    key: 'dtetake',
    name: '',
    labelCodapp: 'HRPM1EEC1',
    labelIndex: '80',
    style: 'min-width: 100px;'
  }, {
    key: 'takeby',
    name: '',
    labelCodapp: 'HRPM1EEC1',
    labelIndex: '90',
    style: 'min-width: 100px;'
  }]

  return {
    en: {
      index: indexColumns
    },
    th: {
      index: indexColumns
    }
  }
}
