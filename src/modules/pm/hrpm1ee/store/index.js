import * as types from './mutation-types'
import hrpm1ee from '../api'
import swal from 'sweetalert'
import hrpm1eeLabels from '../assets/labels'
import { hrpm1eeColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'
import { constLabels } from 'assets/js/constLabels'

export default {
  state: {
    indexDisp: true,
    indexHeadDisp: false,
    detailDisp: false,
    labels: hrpm1eeLabels,
    columns: hrpm1eeColumns(),
    index: {
      total: 0,
      rows: []
    },
    indexHead: {},
    indexSelected: {},
    detail: {
      codasset: '',
      imgfile: '',
      desasse: '',
      dterec: '',
      desnote: '',
      srcasset: '',
      typasset: '',
      status: '',
      dtetake: '',
      takeby: ''
    }
  },
  getters: {
    [types.GET_HRPM1EE_INDEX_DISP] (state) {
      return state.indexDisp
    },
    [types.GET_HRPM1EE_INDEX_HEAD_DISP] (state) {
      return state.indexHeadDisp
    },
    [types.GET_HRPM1EE_DETAIL_DISP] (state) {
      return state.detailDisp
    },
    [types.GET_HRPM1EE_COLUMNS] (state) {
      return state.columns
    },
    [types.GET_HRPM1EE_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPM1EE_INDEX] (state) {
      return state.index
    },
    [types.GET_HRPM1EE_INDEX_HEAD] (state) {
      return state.indexHead
    },
    [types.GET_HRPM1EE_INDEX_SELECTED] (state) {
      return state.indexSelected
    },
    [types.GET_HRPM1EE_DETAIL] (state) {
      return state.detail
    }
  },
  mutations: {
    [types.SET_HRPM1EE_INDEX_DISP] (state, indexDisp) {
      state.indexDisp = indexDisp
    },
    [types.SET_HRPM1EE_INDEX_HEAD_DISP] (state, indexHeadDisp) {
      state.indexHeadDisp = indexHeadDisp
    },
    [types.SET_HRPM1EE_DETAIL_DISP] (state, detailDisp) {
      state.detailDisp = detailDisp
    },
    [types.SET_HRPM1EE_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.columns[keyLang].index = assetsLabels.replaceLabelToColumns(state.columns[keyLang].index, labels[keyLang])
      }
    },
    [types.SET_HRPM1EE_LABELS] (state, labels) {
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPM1EE_INDEX] (state, index) {
      state.index = index
    },
    [types.SET_HRPM1EE_INDEX_HEAD] (state, indexHead) {
      state.indexHead = indexHead
    },
    [types.SET_HRPM1EE_INDEX_SELECTED] (state, indexSelected) {
      state.indexSelected = indexSelected
    },
    [types.SET_HRPM1EE_DETAIL] (state, detail) {
      state.detail = detail
    }
  },
  actions: {
    [types.TOGGLE_HRPM1EE_PAGE] ({ commit }, page) {
      let indexDisp = false
      let detailDisp = false
      let indexHeadDisp = false
      switch (page) {
        case 'index':
          indexDisp = true
          break
        case 'detail':
          detailDisp = true
          break
        case 'indexHead':
          indexHeadDisp = true
          break
      }
      commit(types.SET_HRPM1EE_INDEX_DISP, indexDisp)
      commit(types.SET_HRPM1EE_DETAIL_DISP, detailDisp)
      commit(types.SET_HRPM1EE_INDEX_HEAD_DISP, indexHeadDisp)
    },
    [types.RECEIVED_HRPM1EE_COLUMNS_LABELS] ({ commit }) {
      hrpm1ee.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM1EE_COLUMNS, data.labels)
          commit(types.SET_HRPM1EE_LABELS, data.labels)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM1EE_INDEX] ({ commit }, searchParams) {
      hrpm1ee.getIndex(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM1EE_INDEX, data)
          commit(types.SET_HRPM1EE_INDEX_HEAD_DISP, true)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM1EE_DETAIL_CREATE] ({ commit }) {
      hrpm1ee.getDetailCreate()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM1EE_DETAIL, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM1EE_DETAIL] ({ commit }, detailParams) {
      hrpm1ee.getDetail(detailParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM1EE_DETAIL, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM1EE_INDEX_HEAD] ({ commit }, searchParams) {
      hrpm1ee.getIndexHead(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM1EE_INDEX_HEAD, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.SAVE_HRPM1EE_DETAIL] ({ commit, dispatch }, { searchIndex, indexSelected, detail }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        html: true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel')
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpm1ee.saveDetail({ indexSelected, detail })
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPM1EE_INDEX, searchIndex)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              let message = error
              if (error.response) {
                const data = error.response.data
                message = data.response
              }
              swal({title: '', text: message, html: true, type: 'error'})
            })
        }
      })
    },
    [types.DELETE_HRPM1EE_INDEX] ({ commit, dispatch }, { searchIndex, dataRowsHasFlg }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmCancel'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel'),
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpm1ee.deleteIndex(dataRowsHasFlg)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPM1EE_INDEX, searchIndex)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              let message = error
              if (error.response) {
                const data = error.response.data
                message = data.response
              }
              swal({title: '', text: message, html: true, type: 'error'})
            })
        }
      })
    }
  }
}
