import hrpm1eeLabels from '../assets/labels'
// import faker from 'faker'
// import moment from 'moment'
import assetsLabels from 'assets/js/labels'

export const mocker = {
  labels () {
    return {
      labels: hrpm1eeLabels
    }
  },
  index () {
    let mockRows = []
    const mockTotal = 3
    mockRows.push({
      codasset: 'BB01',
      descasseet: 'bb',
      desctypass: 'โทรศัพท์',
      dterec: '01/01/2016',
      srcasset: 'คลองถม',
      status: '1',
      dtetake: '20/10/2016',
      takeby: 'TJS00001'
    })
    mockRows.push({
      codasset: 'DEL001',
      descasseet: 'Notebook Dell',
      desctypass: 'คอมพิวเตอร์ NoteBook',
      dterec: '04/01/2017',
      srcasset: 'ซื้อ',
      status: '1',
      dtetake: '',
      takeby: 'TJS00001'
    })
    mockRows.push({
      codasset: 'NB',
      descasseet: 'Notebook',
      desctypass: 'เครื่องใช้สำนักงาน',
      dterec: '01/01/2015',
      srcasset: 'พันธ์ทิพย์',
      status: '1',
      dtetake: '20/10/2016',
      takeby: 'TJS00001'
    })

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  indexHead () {
    let mockIndexHead = {
      codasset: '0001-โทรศัพท์'
    }

    return mockIndexHead
  },
  detailCreate () {
    return {
      codasset: '',
      imgfile: '',
      desasse: '',
      dterec: '',
      desnote: '',
      srcasset: '',
      typasset: '',
      status: '',
      dtetake: '',
      takeby: ''
    }
  },
  detail (detailParams) {
    let mockDetail
    if (detailParams.p_codasset === 'BB01') {
      mockDetail = {
        codasset: 'BB01-bb',
        imgfile: '',
        desasse: 'bb',
        dterec: '01/01/2016',
        desnote: 'bb',
        srcasset: 'คลองถม',
        typasset: '0001-โทรศัพท์',
        status: '1',
        dtetake: '20/10/2016',
        takeby: 'TJS00001'
      }
    } else if (detailParams.p_codasset === 'DEL001') {
      mockDetail = {
        codasset: 'DEL001-Notebook Dell',
        imgfile: '',
        desasse: 'Notebook Dell',
        dterec: '04/01/2017',
        desnote: 'Dell รุ่น AX420G  Ram 8 CPU 2.2 HDD 1 TB',
        srcasset: 'ซื้อ',
        typasset: '0002-คอมพิวเตอร์ NoteBook',
        status: '1',
        dtetake: '20/10/2016',
        takeby: 'TJS00001'
      }
    } else if (detailParams.p_codasset === 'NB') {
      mockDetail = {
        codasset: 'NB-Notebook',
        imgfile: '',
        desasse: 'Notebook',
        dterec: '01/01/2015',
        desnote: 'Lenovo',
        srcasset: 'พันธ์ทิพย์',
        typasset: 'A002-เครื่องใช้สำนักงาน',
        status: '1',
        dtetake: '20/10/2016',
        takeby: 'TJS00001'
      }
    } else {
      return assetsLabels.mockErrorMessage
    }
    return mockDetail
  },
  delete () {
    return assetsLabels.mockPostMessage
  },
  save () {
    return assetsLabels.mockPostMessage
  }
}
