import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLabels () {
    if (isMock) return generator(mocker.labels())
    return instance().get(endpoint + '/hrpm1ee/labels')
  },
  getIndex (searchParams) {
    if (isMock) return generator(mocker.index())
    return instance().get(endpoint + '/hrpm1ee/index')
  },
  getIndexHead (searchParams) {
    if (isMock) return generator(mocker.indexHead())
    const params = searchParams.p_codempid + '/' + searchParams.p_dtestrt + '/' + searchParams.p_dteend
    return instance().get(endpoint + '/hrpm1ee/indexHead/' + params)
  },
  getDetailCreate () {
    return generator(mocker.detailCreate())
  },
  getDetail (detailParams) {
    if (isMock) return generator(mocker.detail(detailParams))
    const params = detailParams.p_codasset
    return instance().get(endpoint + '/hrpm1ee/detail/' + params)
  },
  saveDetail ({ indexSelected, detail }) {
    let saveParams = {
      param_json: JSON.stringify({
        p_codempid: indexSelected.codempid
      })
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpm1ee/detailSave', saveParams)
  },
  deleteIndex (dataRowsHasFlg) {
    let sendParams = {
      param_json: JSON.stringify(dataRowsHasFlg)
    }
    if (isMock) return generator(mocker.delete())
    return instance().post(endpoint + '/hrpm1ee/delete', sendParams)
  }
}
