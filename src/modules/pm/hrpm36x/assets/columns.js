export let hrpm36xColumns = () => {
  let indexColumns = [{
    key: 'image',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '5',
    class: 'align-center',
    style: 'min-width: 100px;',
    tagType: 'image'
  }, {
    key: 'codempid',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '60',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codempid',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '70',
    style: 'min-width: 200px;'
  }, {
    key: 'dtetrial',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '80',
    style: 'min-width: 150px;'
  }, {
    key: 'qtyasses',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '90',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'codcomp',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '100',
    style: 'min-width: 100px;'
  }, {
    key: 'codpos',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '110',
    style: 'min-width: 100px;'
  }, {
    key: 'dteatten',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '120',
    style: 'min-width: 100px;'
  }, {
    key: 'dtenext',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '130',
    style: 'min-width: 100px;'
  }, {
    key: '',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '140',
    style: 'min-width: 100px;',
    headerRow: 1,
    colspan: 2,
    rowspan: 1
  }, {
    key: 'codassor',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '150',
    style: 'min-width: 100px;',
    headerRow: 2,
    colspan: 1,
    rowspan: 1
  }, {
    key: 'desc_codassor',
    name: '',
    labelCodapp: 'HRPM36XC1',
    labelIndex: '160',
    style: 'min-width: 100px;',
    headerRow: 2,
    colspan: 1,
    rowspan: 1
  }]

  return {
    en: {
      index: indexColumns
    },
    th: {
      index: indexColumns
    }
  }
}
export let hrpm36xMaxRowspan = {
  index: 2
}

