import hrpm36xLabels from '../assets/labels'
// import faker from 'faker'
import moment from 'moment'
import assetsLabels from 'assets/js/labels'

export const mocker = {
  labels () {
    return {
      labels: hrpm36xLabels
    }
  },
  index () {
    let mockRows = []
    const mockTotal = 3
    mockRows.push({
      codempid: 'T010001',
      image: 'static/image/employee/emp_img28.jpg',
      desc_codempid: 'ทดสอบ ระบบพนักงาน',
      dtetrial: '03/06/2016',
      qtyasses: '1',
      codcomp: 'ฝ่ายดำเนินการและพั้ฒนาระบบ',
      codpos: 'Tester',
      dteatten: '05/03/2017',
      dtenext: '05/04/2017',
      codassor: 'TJS00001',
      desc_codassor: 'นาย กัปตัน ทดสอบ'
    })
    mockRows.push({
      codempid: '53100',
      image: 'static/image/employee/emp_img16.jpg',
      desc_codempid: 'มารีญา ชอบธรรม',
      dtetrial: '01/05/2016',
      qtyasses: '2',
      codcomp: 'ฝ่ายดำเนินการและพั้ฒนาระบบ',
      codpos: 'Tester',
      dteatten: '05/02/2017',
      dtenext: '05/04/2017',
      codassor: 'TJS00001',
      desc_codassor: 'นาย กัปตัน ทดสอบ'
    })

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  indexHead () {
    let mockIndexHead = {
      codcomp: 'ฝ่ายดำเนินการและพั้ฒนาระบบ',
      datest: '20/10/2010',
      dateend: moment().format('DD/MM/YYYY'),
      typproba: 'ทดลองงาน',
      evastat: 'ยังไม่ประเมิณ'
    }

    return mockIndexHead
  },
  delete () {
    return assetsLabels.mockPostMessage
  },
  save () {
    return assetsLabels.mockPostMessage
  }
}
