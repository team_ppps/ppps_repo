import * as types from './mutation-types'
import hrpm36x from '../api'
import swal from 'sweetalert'
import hrpm36xLabels from '../assets/labels'
import { hrpm36xMaxRowspan, hrpm36xColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'
import { constLabels } from 'assets/js/constLabels'

export default {
  state: {
    indexDisp: true,
    indexHeadDisp: false,
    labels: hrpm36xLabels,
    maxRowspan: hrpm36xMaxRowspan,
    columns: hrpm36xColumns(),
    index: {
      total: 0,
      rows: []
    },
    indexHead: {},
    indexSelected: {}
  },
  getters: {
    [types.GET_HRPM36X_INDEX_DISP] (state) {
      return state.indexDisp
    },
    [types.GET_HRPM36X_INDEX_HEAD_DISP] (state) {
      return state.indexHeadDisp
    },
    [types.GET_HRPM36X_COLUMNS] (state) {
      return state.columns
    },
    [types.GET_HRPM36X_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPM36X_MAX_ROWSPAN] (state) {
      return state.maxRowspan
    },
    [types.GET_HRPM36X_INDEX] (state) {
      return state.index
    },
    [types.GET_HRPM36X_INDEX_HEAD] (state) {
      return state.indexHead
    },
    [types.GET_HRPM36X_INDEX_SELECTED] (state) {
      return state.indexSelected
    }
  },
  mutations: {
    [types.SET_HRPM36X_INDEX_DISP] (state, indexDisp) {
      state.indexDisp = indexDisp
    },
    [types.SET_HRPM36X_INDEX_HEAD_DISP] (state, indexHeadDisp) {
      state.indexHeadDisp = indexHeadDisp
    },
    [types.SET_HRPM36X_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.columns[keyLang].index = assetsLabels.replaceLabelToColumns(state.columns[keyLang].index, labels[keyLang])
      }
    },
    [types.SET_HRPM36X_LABELS] (state, labels) {
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPM36X_INDEX] (state, index) {
      state.index = index
    },
    [types.SET_HRPM36X_INDEX_HEAD] (state, indexHead) {
      state.indexHead = indexHead
    },
    [types.SET_HRPM36X_INDEX_SELECTED] (state, indexSelected) {
      state.indexSelected = indexSelected
    }
  },
  actions: {
    [types.TOGGLE_HRPM36X_PAGE] ({ commit }, page) {
      let indexDisp = false
      let indexHeadDisp = false
      switch (page) {
        case 'index':
          indexDisp = true
          break
        case 'indexHead':
          indexHeadDisp = true
          break
      }
      commit(types.SET_HRPM36X_INDEX_DISP, indexDisp)
      commit(types.SET_HRPM36X_INDEX_HEAD_DISP, indexHeadDisp)
    },
    [types.RECEIVED_HRPM36X_COLUMNS_LABELS] ({ commit }) {
      hrpm36x.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM36X_COLUMNS, data.labels)
          commit(types.SET_HRPM36X_LABELS, data.labels)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM36X_INDEX] ({ commit }, searchParams) {
      hrpm36x.getIndex(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM36X_INDEX, data)
          commit(types.SET_HRPM36X_INDEX_HEAD_DISP, true)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM36X_INDEX_HEAD] ({ commit }, searchParams) {
      hrpm36x.getIndexHead(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM36X_INDEX_HEAD, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.DELETE_HRPM36X_INDEX] ({ commit, dispatch }, { searchIndex, dataRowsHasFlg }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmCancel'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel'),
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpm36x.deleteIndex(dataRowsHasFlg)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPM36X_INDEX, searchIndex)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              let message = error
              if (error.response) {
                const data = error.response.data
                message = data.response
              }
              swal({title: '', text: message, html: true, type: 'error'})
            })
        }
      })
    }
  }
}
