import * as types from './mutation-types'
import hrpm15e from '../api'
import swal from 'sweetalert'
import hrpm15eLabels from '../assets/labels'
import { hrpm15eColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'
import { constLabels } from 'assets/js/constLabels'
import moment from 'moment'

export default {
  state: {
    indexDisp: true,
    detailDisp: false,
    labels: hrpm15eLabels,
    columns: hrpm15eColumns(),
    index: {
      total: 0,
      rows: []
    },
    indexSelected: {
      groupid: '',
      namempid: '',
      syncond: {
        code: '',
        description: ''
      },
      typgrpids: [],
      lastFullId: ''
    },
    listFields: {
      en: [
        { code: 'codcompy', description: 'บริษัท', type: 'varchar2' },
        { code: 'typused', description: 'ประเภทการจ้าง', type: 'varchar2' },
        { code: 'worklocat', description: 'สถานที่ทำงาน', type: 'varchar2' }
      ],
      th: [
        { code: 'codcompy', description: 'บริษัท', type: 'varchar2' },
        { code: 'typused', description: 'ประเภทการจ้าง', type: 'varchar2' },
        { code: 'worklocat', description: 'สถานที่ทำงาน', type: 'varchar2' }
      ]
    }
  },
  getters: {
    [types.GET_HRPM15E_INDEX_DISP] (state) {
      return state.indexDisp
    },
    [types.GET_HRPM15E_DETAIL_DISP] (state) {
      return state.detailDisp
    },
    [types.GET_HRPM15E_COLUMNS] (state) {
      return state.columns
    },
    [types.GET_HRPM15E_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPM15E_INDEX] (state) {
      return state.index
    },
    [types.GET_HRPM15E_INDEX_SELECTED] (state) {
      return state.indexSelected
    },
    [types.GET_HRPM15E_LOGICAL_LISTFIELDS] (state) {
      return state.listFields
    }
  },
  mutations: {
    [types.SET_HRPM15E_INDEX_DISP] (state, indexDisp) {
      state.indexDisp = indexDisp
    },
    [types.SET_HRPM15E_DETAIL_DISP] (state, detailDisp) {
      state.detailDisp = detailDisp
    },
    [types.SET_HRPM15E_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.columns[keyLang].index = assetsLabels.replaceLabelToColumns(state.columns[keyLang].index, labels[keyLang])
      }
    },
    [types.SET_HRPM15E_LABELS] (state, labels) {
      let typgrpidsDefaultEn = [
        {
          numseq: 1,
          name: labels['en']['HRPM15EC3']['20'],
          desc: labels['en']['HRPM15EC3']['60'],
          disabled: false,
          typgrpid: 'ST',
          type: 'text',
          typeval: 'ST',
          value: 'ST'
        },
        {
          numseq: 2,
          name: labels['en']['HRPM15EC3']['30'],
          desc: labels['en']['HRPM15EC3']['60'],
          disabled: false,
          typgrpid: 'CE',
          type: 'number',
          typeval: moment().format('YY'),
          value: moment().format('YY')
        },
        {
          numseq: 3,
          name: labels['en']['HRPM15EC3']['40'],
          desc: labels['en']['HRPM15EC3']['60'],
          disabled: false,
          typgrpid: 'MT',
          type: 'number',
          typeval: moment().format('MM'),
          value: moment().format('MM')
        },
        {
          numseq: 4,
          name: labels['en']['HRPM15EC3']['50'],
          desc: labels['en']['HRPM15EC3']['60'],
          disabled: false,
          typgrpid: 'RN',
          type: 'text',
          typeval: '1',
          value: '0'
        }
      ]
      let typgrpidsDefaultTh = [
        {
          numseq: 1,
          name: labels['th']['HRPM15EC3']['20'],
          desc: labels['th']['HRPM15EC3']['60'],
          disabled: false,
          typgrpid: 'ST',
          type: 'text',
          typeval: 'ST',
          value: 'ST'
        },
        {
          numseq: 2,
          name: labels['th']['HRPM15EC3']['30'],
          desc: labels['th']['HRPM15EC3']['60'],
          disabled: false,
          typgrpid: 'CE',
          type: 'number',
          typeval: moment().format('YY'),
          value: moment().format('YY')
        },
        {
          numseq: 3,
          name: labels['th']['HRPM15EC3']['40'],
          desc: labels['th']['HRPM15EC3']['60'],
          disabled: false,
          typgrpid: 'MT',
          type: 'number',
          typeval: moment().format('MM'),
          value: moment().format('MM')
        },
        {
          numseq: 4,
          name: labels['th']['HRPM15EC3']['50'],
          desc: labels['th']['HRPM15EC3']['60'],
          disabled: false,
          typgrpid: 'RN',
          type: 'text',
          typeval: '1',
          value: '0'
        }
      ]
      labels.en.typgrpidsDefault = typgrpidsDefaultEn
      labels.th.typgrpidsDefault = typgrpidsDefaultTh
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPM15E_INDEX] (state, index) {
      state.index = index
    },
    [types.SET_HRPM15E_INDEX_SELECTED] (state, indexSelected) {
      state.indexSelected = indexSelected
    }
  },
  actions: {
    [types.TOGGLE_HRPM15E_PAGE] ({ commit }, page) {
      let indexDisp = false
      let detailDisp = false
      switch (page) {
        case 'index':
          indexDisp = true
          break
        case 'detail':
          detailDisp = true
          break
      }
      commit(types.SET_HRPM15E_INDEX_DISP, indexDisp)
      commit(types.SET_HRPM15E_DETAIL_DISP, detailDisp)
    },
    [types.RECEIVED_HRPM15E_COLUMNS_LABELS] ({ commit }) {
      hrpm15e.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM15E_COLUMNS, data.labels)
          commit(types.SET_HRPM15E_LABELS, data.labels)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM15E_INDEX] ({ commit }) {
      hrpm15e.getIndex()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM15E_INDEX, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.POST_HRPM15E_SAVE] ({ commit, dispatch }, { searchIndex, detail }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmCancel'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel'),
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpm15e.postSave(detail)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPM15E_INDEX, searchIndex)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              let message = error
              if (error.response) {
                const data = error.response.data
                message = data.response
              }
              swal({title: '', text: message, html: true, type: 'error'})
            })
        }
      })
    }
  }
}
