export let hrpm15eColumns = () => {
  let indexColumns = [{
    key: 'groupid',
    name: '',
    labelCodapp: 'HRPM15EC1',
    labelIndex: '20',
    class: 'align-center',
    style: 'min-width: 200px;'
  }, {
    key: 'namempid',
    name: '',
    labelCodapp: 'HRPM15EC1',
    labelIndex: '30',
    style: 'min-width: 200px;'
  }, {
    key: 'lastFullId',
    name: '',
    labelCodapp: 'HRPM15EC1',
    labelIndex: '40',
    style: 'min-width: 200px;'
  }]

  return {
    en: {
      index: indexColumns
    },
    th: {
      index: indexColumns
    }
  }
}
