import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLabels () {
    if (isMock) return generator(mocker.labels())
    return instance().get(endpoint + '/hrpm15e/labels')
  },
  getIndex () {
    if (isMock) return generator(mocker.index())
    return instance().get(endpoint + '/hrpm15e/index')
  },
  postSave (detail) {
    let sendParams = {
      param_json: JSON.stringify(detail)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpm15e/update', sendParams)
  }
}
