import hrpm15eLabels from '../assets/labels'
// import faker from 'faker'
import assetsLabels from 'assets/js/labels'

export const mocker = {
  labels () {
    return {
      labels: hrpm15eLabels
    }
  },
  index () {
    let mockRows = []
    const mockTotal = 2
    const mockTypegrpid = ['ST', 'CE', 'MT', 'RN']
    const mockTypegrpidData = [
      [1, 0, 3],
      [1, 0, 3]
    ]
    const mockTpeval = [
      [17, 'M', 4],
      [17, 'D', 4]
    ]
    const mockLastId = [951, 1801]
    const mockGroupId = ['0001', '0002']
    const mockGroupNam = ['พนักงานรายเดือน', 'พนักงานรายวัน']
    const mockStatement = [
      {
        code: '',
        description: ''
      },
      {
        code: '',
        description: ''
      }
    ]
    for (var i = 0; i < mockTotal; i++) {
      let typgrpid = []
      for (var j = 0; j < mockTypegrpidData[i].length; j++) {
        let value = (mockTypegrpid[mockTypegrpidData[i][j]] === 'RN') ? mockLastId[i] : mockTpeval[i][j]
        typgrpid.push({
          typgrpid: mockTypegrpid[mockTypegrpidData[i][j]],
          typeval: mockTpeval[i][j],
          value: value,
          numseq: j + 1
        })
      }
      mockRows.push({
        groupid: mockGroupId[i],
        namempid: mockGroupNam[i],
        syncond: mockStatement[i],
        typgrpids: typgrpid,
        lastFullId: ''
      })
    }

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  save () {
    return assetsLabels.mockPostMessage
  }
}
