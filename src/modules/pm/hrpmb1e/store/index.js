import * as types from './mutation-types'
import hrpmb1e from '../api'
import hrpmb1eLabels from '../assets/labels'
import swal from 'sweetalert'
import { constLabels } from 'assets/js/constLabels'

export default {
  state: {
    indexDisp: true,
    detailDisp: false,
    detailTreeDisp: false,
    labels: hrpmb1eLabels,
    // columns: hrpmb1eColumns(),
    index: {
      total: 0,
      rows: []
    },
    personalData: {
      personalInfo: {
        total: 0,
        rows: [{tabno: ''}]
      },
      personalAddr: {
        total: 0,
        rows: [{tabno: ''}]
      },
      personalEmp: {
        total: 0,
        rows: [{tabno: ''}]
      },
      personalTravel: {
        total: 0,
        rows: [{tabno: ''}]
      },
      personalIncom: {
        total: 0,
        rows: [{tabno: ''}]
      },
      personalTax: {
        total: 0,
        rows: [{tabno: ''}]
      },
      personalSpouseTax: {
        total: 0,
        rows: [{tabno: ''}]
      },
      personalChangName: {
        total: 0,
        rows: [{tabno: ''}]
      },
      personalDoc: {
        total: 0,
        rows: [{tabno: ''}]
      }
    },
    educatWorkData: {
      educat: {
        total: 0,
        rows: [{tabno: ''}]
      },
      workHis: {
        total: 0,
        rows: [{tabno: ''}]
      },
      trainHis: {
        total: 0,
        rows: [{tabno: ''}]
      },
      internalTrainHis: {
        total: 0,
        rows: [{tabno: ''}]
      }
    },
    familyData: {
      spouse: {
        total: 0,
        rows: [{tabno: ''}]
      },
      children: {
        total: 0,
        rows: [{tabno: ''}]
      },
      parent: {
        total: 0,
        rows: [{tabno: ''}]
      },
      relative: {
        total: 0,
        rows: [{tabno: ''}]
      }
    },
    guaranteeData: {
      guarantor: {
        total: 0,
        rows: [{tabno: ''}]
      },
      securities: {
        total: 0,
        rows: [{tabno: ''}]
      },
      reference: {
        total: 0,
        rows: [{tabno: ''}]
      }
    },
    talentData: {
      talent: {
        total: 0,
        rows: [{tabno: ''}]
      },
      langSkill: {
        total: 0,
        rows: [{tabno: ''}]
      },
      goodness: {
        total: 0,
        rows: [{tabno: ''}]
      }
    },
    othersData: {
      total: 0,
      rows: [{tabno: ''}]
    },
    indexSelected: {},
    detailSelected: {},
    dropdowns: {
      en: {},
      th: {}
    }
  },
  getters: {
    [types.GET_HRPMB1E_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPMB1E_PERSONAL_DETAIL] (state) {
      return state.personalData
    },
    [types.GET_HRPMB1E_EDUCAT_WORK_DETAIL] (state) {
      return state.educatWorkData
    },
    [types.GET_HRPMB1E_FAMILY_DETAIL] (state) {
      return state.familyData
    },
    [types.GET_HRPMB1E_GUARANTEE_DETAIL] (state) {
      return state.guaranteeData
    },
    [types.GET_HRPMB1E_TALENT_DETAIL] (state) {
      return state.talentData
    },
    [types.GET_HRPMB1E_OTHERS_DETAIL] (state) {
      return state.othersData
    }
  },
  mutations: {
    [types.SET_HRPMB1E_LABELS] (state, labels) {
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPMB1E_PERSONAL_DETAIL] (state, data) {
      state.personalData.personalInfo = typeof data.personalInfo !== 'undefined' ? data.personalInfo : state.personalData.personalInfo
      state.personalData.personalAddr = typeof data.personalAddr !== 'undefined' ? data.personalAddr : state.personalData.personalAddr
      state.personalData.personalEmp = typeof data.personalEmp !== 'undefined' ? data.personalEmp : state.personalData.personalEmp
      state.personalData.personalTravel = typeof data.personalTravel !== 'undefined' ? data.personalTravel : state.personalData.personalTravel
      state.personalData.personalIncom = typeof data.personalIncom !== 'undefined' ? data.personalIncom : state.personalData.personalIncom
      state.personalData.personalTax = typeof data.personalTax !== 'undefined' ? data.personalTax : state.personalData.personalTax
      state.personalData.personalSpouseTax = typeof data.personalSpouseTax !== 'undefined' ? data.personalSpouseTax : state.personalData.personalSpouseTax
      state.personalData.personalChangName = typeof data.personalChangName !== 'undefined' ? data.personalChangName : state.personalData.personalChangName
      state.personalData.personalDoc = typeof data.personalDoc !== 'undefined' ? data.personalDoc : state.personalData.personalDoc
    },
    [types.SET_HRPMB1E_EDUCAT_WORK_DETAIL] (state, data) {
      state.educatWorkData.educat = typeof data.educat !== 'undefined' ? data.educat : state.educatWorkData.educat
      state.educatWorkData.workHis = typeof data.workHis !== 'undefined' ? data.workHis : state.educatWorkData.workHis
      state.educatWorkData.trainHis = typeof data.trainHis !== 'undefined' ? data.trainHis : state.educatWorkData.trainHis
      state.educatWorkData.internalTrainHis = typeof data.internalTrainHis !== 'undefined' ? data.internalTrainHis : state.educatWorkData.internalTrainHis
    },
    [types.SET_HRPMB1E_FAMILY_DETAIL] (state, data) {
      state.familyData.spouse = typeof data.spouse !== 'undefined' ? data.spouse : state.familyData.spouse
      state.familyData.children = typeof data.children !== 'undefined' ? data.children : state.familyData.children
      state.familyData.parent = typeof data.parent !== 'undefined' ? data.parent : state.familyData.parent
      state.familyData.relative = typeof data.relative !== 'undefined' ? data.relative : state.familyData.relative
    },
    [types.SET_HRPMB1E_GUARANTEE_DETAIL] (state, data) {
      state.guaranteeData.guarantor = typeof data.guarantor !== 'undefined' ? data.guarantor : state.guaranteeData.guarantor
      state.guaranteeData.securities = typeof data.securities !== 'undefined' ? data.securities : state.guaranteeData.securities
      state.guaranteeData.reference = typeof data.reference !== 'undefined' ? data.reference : state.guaranteeData.reference
    },
    [types.SET_HRPMB1E_TALENT_DETAIL] (state, data) {
      state.talentData.talent = typeof data.talent !== 'undefined' ? data.talent : state.talentData.talent
      state.talentData.langSkill = typeof data.langSkill !== 'undefined' ? data.langSkill : state.talentData.langSkill
      state.talentData.goodness = typeof data.goodness !== 'undefined' ? data.goodness : state.talentData.goodness
    },
    [types.SET_HRPMB1E_OTHERS_DETAIL] (state, data) {
      state.othersData = typeof data !== 'undefined' ? data : state.othersData
    }
  },
  actions: {
    [types.RECEIVED_HRPMB1E_COLUMNS_LABELS] ({ commit }) {
      hrpmb1e.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB1E_LABELS, data.labels)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB1E_PERSONAL_DETAIL] ({ commit }) {
      hrpmb1e.getPersonalDetail()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB1E_PERSONAL_DETAIL, data)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB1E_EDUCAT_WORK_DETAIL] ({ commit }) {
      hrpmb1e.getEducatWorkDetail()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB1E_EDUCAT_WORK_DETAIL, data)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB1E_FAMILY_DETAIL] ({ commit }) {
      hrpmb1e.getFamilyDetail()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB1E_FAMILY_DETAIL, data)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB1E_GUARANTEE_DETAIL] ({ commit }) {
      hrpmb1e.getGuaranteeDetail()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB1E_GUARANTEE_DETAIL, data)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB1E_TALENT_DETAIL] ({ commit }) {
      hrpmb1e.getTalentDetail()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB1E_TALENT_DETAIL, data)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB1E_OTHERS_DETAIL] ({ commit }) {
      hrpmb1e.getOthersDetail()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB1E_OTHERS_DETAIL, data)
        })
        .catch((error) => {
          const data = error.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.POST_HRPMB1E_DATA] ({ commit }, saveParams) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        html: true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel')
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmb1e.postData(saveParams)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              const data = error.response.data
              swal({title: '', text: data.response, html: true, type: 'error'})
            })
        }
      })
    }
  }
}
