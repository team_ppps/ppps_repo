// import faker from 'faker'
// import moment from 'moment'
import hrpmb1eLabels from '../assets/labels'
import assetsLabels from 'assets/js/labels'
// import labels from 'src/../config/label.js'

export const mocker = {
  labels () {
    return {
      labels: hrpmb1eLabels
    }
  },
  personalDetail () {
    const personalInfoTotal = 35
    let personalInfo = [
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'CODTITLE', flgdisp: '', stdrequire: '', usrrequire: 'Y', flgdefault: '', defaultval: '', desc_column: 'คำนำหน้า'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'NAMFIRST', flgdisp: '', stdrequire: 'Y', usrrequire: 'Y', flgdefault: '', defaultval: '', desc_column: 'ชื่อ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'NAMLAST', flgdisp: '', stdrequire: 'Y', usrrequire: 'Y', flgdefault: '', defaultval: '', desc_column: 'นามสกุล'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'NAMNICK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อเล่น'}, // s
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'NUMTEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เบอร์โทรศัพท์'}, // s
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'NUMTELMO', flgdisp: '', stdrequire: 'Y', usrrequire: 'Y', flgdefault: '', defaultval: '', desc_column: 'เบอร์โทรศัพท์มือถือ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '7', tablename: 'TEMPLOY2', fieldname: 'LINEID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'Line ID'}, // s
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '8', tablename: 'TEMPLOY2', fieldname: 'NUMOFFID', flgdisp: '', stdrequire: 'Y', usrrequire: 'Y', flgdefault: '', defaultval: '', desc_column: 'บัตรประชาชน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '9', tablename: 'TEMPLOY2', fieldname: 'DTEOFFID', flgdisp: '', stdrequire: 'Y', usrrequire: 'Y', flgdefault: '', defaultval: '', desc_column: 'วันที่หมดอายุ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '10', tablename: 'TEMPLOY2', fieldname: 'ADRISSUE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ออกบัตรที่'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '11', tablename: 'TEMPLOY2', fieldname: 'CODPROVI', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'จังหวัด'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '12', tablename: 'TEMPLOY2', fieldname: 'HOSPSS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ร.พ.ประกันสังคม'}, // s
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '13', tablename: 'TEMPLOY2', fieldname: 'NUMPASID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่พาสปอร์ต'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '14', tablename: 'TEMPLOY2', fieldname: 'DTEPASID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่หมดอายุ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '15', tablename: 'TEMPLOY2', fieldname: 'NUMVISA', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'VISA ID'}, // s
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '16', tablename: 'TEMPLOY2', fieldname: 'DTEVISA', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่หมดอายุ'}, // s
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '17', tablename: 'TEMPLOY2', fieldname: 'NUMLICID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่ใบขับขี่'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '18', tablename: 'TEMPLOY2', fieldname: 'DTELICID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่หมดอายุ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '19', tablename: 'TEMPLOY1', fieldname: 'DTEEMPDB', flgdisp: '', stdrequire: '', usrrequire: 'Y', flgdefault: '', defaultval: '', desc_column: 'วันเกิด'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '20', tablename: 'TEMPLOY2', fieldname: 'CODDOMCL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานที่เกิด'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '21', tablename: 'TEMPLOY2', fieldname: 'AGE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อายุ'}, // s
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '22', tablename: 'TEMPLOY1', fieldname: 'CODSEX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เพศ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '23', tablename: 'TEMPLOY2', fieldname: 'WEIGHT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'น้ำหนัก'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '24', tablename: 'TEMPLOY2', fieldname: 'HIGH', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ส่วนสูง'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '25', tablename: 'TEMPLOY2', fieldname: 'CODBLOOD', flgdisp: '', stdrequire: '', usrrequire: 'Y', flgdefault: '', defaultval: '', desc_column: 'กรุ๊ปเลือด'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '26', tablename: 'TEMPLOY2', fieldname: 'CODORGIN', flgdisp: '', stdrequire: '', usrrequire: 'Y', flgdefault: 'Y', defaultval: '', desc_column: 'เชื้อชาติ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '27', tablename: 'TEMPLOY2', fieldname: 'CODNATNL', flgdisp: '', stdrequire: '', usrrequire: 'Y', flgdefault: 'Y', defaultval: '', desc_column: 'สัญชาติ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '28', tablename: 'TEMPLOY2', fieldname: 'CODRELGN', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: 'Y', defaultval: '', desc_column: 'ศาสนา'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '29', tablename: 'TEMPLOY1', fieldname: 'STAMARRY', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานภาพสมรส'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '30', tablename: 'TEMPLOY2', fieldname: 'STAMILIT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานะทหาร'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '31', tablename: 'TEMPLOY2', fieldname: 'NUMPRMID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่ใบอนุญาตทำงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '32', tablename: 'TEMPLOY2', fieldname: 'DTEPRMST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่ออกใบอนุญาต'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '33', tablename: 'TEMPLOY2', fieldname: 'DTEPRMEN', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่หมดอายุ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '34', tablename: 'TEMPLOY1', fieldname: 'NUMAPPL', flgdisp: '', stdrequire: '', usrrequire: 'Y', flgdefault: '', defaultval: '', desc_column: 'เลขที่ใบสมัคร'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_1', seqno: '35', tablename: 'TEMPLOY1', fieldname: 'DTERETIRE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่เกษียณ'} // s
    ]
    const personalAddrTotal = 12
    let personalAddr = [
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '1', tablename: 'TEMPLOY2', fieldname: 'ADRREG', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ที่อยู่'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '2', tablename: 'TEMPLOY2', fieldname: 'CODPROVR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'จังหวัด'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '3', tablename: 'TEMPLOY2', fieldname: 'CODDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อำเภอ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'CODSUBDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ตำบล'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'CODCNTYR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเทศ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'CODPOSTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสไปรษณีย์'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '7', tablename: 'TEMPLOY2', fieldname: 'ADRCONT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ที่อยู่'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '8', tablename: 'TEMPLOY2', fieldname: 'CODPROVC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'จังหวัด'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '9', tablename: 'TEMPLOY2', fieldname: 'CODDISTC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อำเภอ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '10', tablename: 'TEMPLOY2', fieldname: 'CODSUBDISTC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ตำบล'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '11', tablename: 'TEMPLOY2', fieldname: 'CODCNTYC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเทศ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_2', seqno: '12', tablename: 'TEMPLOY2', fieldname: 'CODPOSTC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสไปรษณีย์'}
    ]
    const personalEmpTotal = 35
    let personalEmp = [
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'DTEEMPT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่เข้างาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'STAEMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานภาพพนักงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'DTEEFFEX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่พ้นสภาพ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '4', tablename: 'TEMPLOY1', fieldname: 'CODDOMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'หน่วยงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '5', tablename: 'TEMPLOY1', fieldname: 'CODPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ตำแหน่ง'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '6', tablename: 'TEMPLOY1', fieldname: 'DTEEFPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่เริ่มตำแหน่ง'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '7', tablename: 'TEMPLOY1', fieldname: 'NUMLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ระดับ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '8', tablename: 'TEMPLOY1', fieldname: 'DTEEFLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่เริ่มระดับ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '9', tablename: 'TEMPLOY1', fieldname: 'CODBRLC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานที่ทำงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '10', tablename: 'TEMPLOY1', fieldname: 'CODEMPMT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเภทการจ้างงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '11', tablename: 'TEMPLOY1', fieldname: 'TYPPAYROLL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'กลุ่มการจ่ายเงินเดือน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '12', tablename: 'TEMPLOY1', fieldname: 'TYPEMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเภทพนักงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '13', tablename: 'TEMPLOY1', fieldname: 'CODCALEN', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'กลุ่มพนักงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '14', tablename: 'TEMPLOY1', fieldname: 'FLGATTEN', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'บันทึกเวลา'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '15', tablename: 'TEMPLOY1', fieldname: 'CODJOB', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ลักษณะงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '16', tablename: 'TEMPLOY1', fieldname: 'JOBGRADE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'Job Grade'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '17', tablename: 'TEMPLOY1', fieldname: 'DTEEFSTEP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่เริ่ม Job Grade'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '18', tablename: 'TEMPLOY1', fieldname: 'CODGRPGL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัส GL Code'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '19', tablename: 'TEMPLOY1', fieldname: 'STADISB', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'พนักงานพิการ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '20', tablename: 'TEMPLOY1', fieldname: 'NUMDISAB', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขทะเบียนพิการ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '21', tablename: 'TEMPLOY1', fieldname: 'DTEDISB', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่ออกสมุดคนพิการ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '22', tablename: 'TEMPLOY1', fieldname: 'DTEDISEN', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่หมดอายุ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '23', tablename: 'TEMPLOY1', fieldname: 'TYPDISP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเภทความพิการ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '24', tablename: 'TEMPLOY1', fieldname: 'DESDISP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ลักษณะความพิการ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '25', tablename: 'TEMPLOY1', fieldname: 'QTYDATRQ', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'กำหนดเวลาทดลองงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '26', tablename: 'TEMPLOY1', fieldname: 'NUMTELOF', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เบอร์ติดต่อภายใน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '27', tablename: 'TEMPLOY1', fieldname: 'DTEDUEPR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันครบกำหนดทดลองงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '28', tablename: 'TEMPLOY1', fieldname: 'EMAIL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อีเมล์'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '29', tablename: 'TEMPLOY1', fieldname: 'QTYDATRQ', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ระยะเวลาการจ้างงาน (เดือน)'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '30', tablename: 'TEMPLOY1', fieldname: 'NUMREQST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่ใบขออนุมัติ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '31', tablename: 'TEMPLOY1', fieldname: 'DTEOCCUP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่บรรจุ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '32', tablename: 'TEMPLOY1', fieldname: 'OCODEMPID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสพนักงานเดิม'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '33', tablename: 'TEMPLOY1', fieldname: 'QTYREDUE1', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'กำหนดเวลาทดลองงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '34', tablename: 'TEMPLOY1', fieldname: 'DTEREEMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่กลับเข้าทำงานใหม่'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_3', seqno: '35', tablename: 'TEMPLOY1', fieldname: 'DTEREDUE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ครบกำหนดทดลองงาน'}
    ]
    const personalTravelTotal = 6
    let personalTravel = [
      {codapp: 'HRPMC2E', tabno: 'TAB1_4', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'CODTRAVEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: 'Y', defaultval: '', desc_column: 'วิธีการเดินทาง'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_4', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'NUMTRAVEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ระยะทางจากบ้านถึงที่ทำงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_4', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'FUELTYP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'หมายเลขทะเบียน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_4', seqno: '4', tablename: 'TEMPLOY1', fieldname: 'DISTANCE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สายรถ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_4', seqno: '5', tablename: 'TEMPLOY1', fieldname: 'CODCABLE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: 'Y', defaultval: '', desc_column: 'ประเภทเชื้อเพลิง'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_4', seqno: '6', tablename: 'TEMPLOY1', fieldname: 'CODPOINT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'จุดออกรถ'}
    ]
    const personalIncomTotal = 11
    let personalIncom = [
      {codapp: 'HRPMC2E', tabno: 'TAB1_5', seqno: '1', tablename: 'TEMPLOY2', fieldname: 'NUMTAXID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'หน่วยเงิน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_5', seqno: '2', tablename: 'TEMPLOY2', fieldname: 'NUMSAID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสรายได้'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_5', seqno: '3', tablename: 'TEMPLOY2', fieldname: 'FRSMEMB', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'คำอธิบาย'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_5', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'FAXTAX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'หน่วยคำนวณ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_5', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'TYPTAX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'จำนวนเงิน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_5', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'DTEYRRELF', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เงินเดือนปรับหลังพ้นทดลองงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_5', seqno: '7', tablename: 'TEMPLOY2', fieldname: 'DTEYRRELT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ค่าจ้าง/ชม.'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_5', seqno: '8', tablename: 'TEMPLOY2', fieldname: 'AMTRELAS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ค่าจ้าง/วัน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_5', seqno: '9', tablename: 'TEMPLOY2', fieldname: 'AMTTAXREL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เงินได้รวม'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_5', seqno: '10', tablename: 'TEMPLOY2', fieldname: 'CODBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขลาสุด'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_5', seqno: '11', tablename: 'TEMPLOY2', fieldname: 'NUMBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขโดย'}
    ]
    const personalTaxTotal = 17
    let personalTax = [
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '1', tablename: 'TEMPLOY2', fieldname: 'NUMTAXID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดภาษี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '2', tablename: 'TEMPLOY2', fieldname: 'NUMSAID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'หมายเลขผู้เสียภาษี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '3', tablename: 'TEMPLOY2', fieldname: 'FRSMEMB', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่ประกันสังคม'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'FAXTAX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่สมทบ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'TYPTAX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วิธีการหักภาษี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'DTEYRRELF', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'การยื่นแบบชำระภาษี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '7', tablename: 'TEMPLOY2', fieldname: 'DTEYRRELT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'มูลค่าอสังหาริมทรัพย์'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '8', tablename: 'TEMPLOY2', fieldname: 'AMTRELAS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ปีที่เริ่มยกเว้น'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '9', tablename: 'TEMPLOY2', fieldname: 'AMTTAXREL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ถึง'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '10', tablename: 'TEMPLOY2', fieldname: 'CODBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ภาษีที่ได้รับยกเว้น'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '11', tablename: 'TEMPLOY2', fieldname: 'NUMBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดธนาคาร'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '12', tablename: 'TEMPLOY2', fieldname: 'NUMBRNCH', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ธนาคาร'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '13', tablename: 'TEMPLOY2', fieldname: 'AMTBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่บัญชี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '14', tablename: 'TEMPLOY2', fieldname: 'QTYAMTBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสสาขา'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '15', tablename: 'TEMPLOY2', fieldname: 'CODBANK2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: '% เข้าบัญชี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '16', tablename: 'TEMPLOY2', fieldname: 'NUMBANK2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสสาขา'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_6', seqno: '17', tablename: 'TEMPLOY2', fieldname: 'NUMBRNCH2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'หรือจำนวนเงิน'}
    ]
    const personalSpouseTaxTotal = 17
    let personalSpouseTax = [
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '1', tablename: 'TEMPLOY2', fieldname: 'NUMTAXID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดภาษี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '2', tablename: 'TEMPLOY2', fieldname: 'NUMSAID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'หมายเลขผู้เสียภาษี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '3', tablename: 'TEMPLOY2', fieldname: 'FRSMEMB', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่ประกันสังคม'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'FAXTAX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่สมทบ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'TYPTAX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วิธีการหักภาษี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'DTEYRRELF', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'การยื่นแบบชำระภาษี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '7', tablename: 'TEMPLOY2', fieldname: 'DTEYRRELT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'มูลค่าอสังหาริมทรัพย์'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '8', tablename: 'TEMPLOY2', fieldname: 'AMTRELAS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ปีที่เริ่มยกเว้น'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '9', tablename: 'TEMPLOY2', fieldname: 'AMTTAXREL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ถึง'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '10', tablename: 'TEMPLOY2', fieldname: 'CODBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ภาษีที่ได้รับยกเว้น'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '11', tablename: 'TEMPLOY2', fieldname: 'NUMBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดธนาคาร'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '12', tablename: 'TEMPLOY2', fieldname: 'NUMBRNCH', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ธนาคาร'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '13', tablename: 'TEMPLOY2', fieldname: 'AMTBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่บัญชี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '14', tablename: 'TEMPLOY2', fieldname: 'QTYAMTBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสสาขา'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '15', tablename: 'TEMPLOY2', fieldname: 'CODBANK2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: '% เข้าบัญชี'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '16', tablename: 'TEMPLOY2', fieldname: 'NUMBANK2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสสาขา'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_7', seqno: '17', tablename: 'TEMPLOY2', fieldname: 'NUMBRNCH2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'หรือจำนวนเงิน'}
    ]
    const personalChangNameTotal = 8
    let personalChangName = [
      {codapp: 'HRPMC2E', tabno: 'TAB1_8', seqno: '1', tablename: 'TEMPLOY2', fieldname: 'CODBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_8', seqno: '2', tablename: 'TEMPLOY2', fieldname: 'NUMBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'คำนำหน้า'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_8', seqno: '3', tablename: 'TEMPLOY2', fieldname: 'NUMBRNCH', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_8', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'AMTBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'นามสกุล'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_8', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'QTYAMTBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เหตุผลที่เปลี่ยน'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_8', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'CODBANK2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขลาสุด'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_8', seqno: '7', tablename: 'TEMPLOY2', fieldname: 'NUMBANK2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขโดย'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_8', seqno: '8', tablename: 'TEMPLOY2', fieldname: 'NUMBRNCH2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดการเปลี่ยนชื่อ-สกุล'}
    ]
    const personalDocTotal = 10
    let personalDoc = [
      {codapp: 'HRPMC2E', tabno: 'TAB1_9', seqno: '1', tablename: 'TEMPLOY2', fieldname: 'AMTRELAS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ลำดับที่'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_9', seqno: '2', tablename: 'TEMPLOY2', fieldname: 'AMTTAXREL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประกอบเอกสาร'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_9', seqno: '3', tablename: 'TEMPLOY2', fieldname: 'CODBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อเอกสาร'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_9', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'NUMBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่รับเอกสาร'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_9', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'NUMBRNCH', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่หมดอายุ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_9', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'AMTBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่เอกสาร'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_9', seqno: '7', tablename: 'TEMPLOY2', fieldname: 'QTYAMTBANK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อไฟล์'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_9', seqno: '8', tablename: 'TEMPLOY2', fieldname: 'CODBANK2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดอื่นๆ'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_9', seqno: '9', tablename: 'TEMPLOY2', fieldname: 'NUMBANK2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขลาสุด'},
      {codapp: 'HRPMC2E', tabno: 'TAB1_9', seqno: '10', tablename: 'TEMPLOY2', fieldname: 'NUMBRNCH2', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขโดย'}
    ]
    return {
      personalInfo: {
        total: personalInfoTotal,
        rows: personalInfo
      },
      personalAddr: {
        total: personalAddrTotal,
        rows: personalAddr
      },
      personalEmp: {
        total: personalEmpTotal,
        rows: personalEmp
      },
      personalTravel: {
        total: personalTravelTotal,
        rows: personalTravel
      },
      personalIncom: {
        total: personalIncomTotal,
        rows: personalIncom
      },
      personalTax: {
        total: personalTaxTotal,
        rows: personalTax
      },
      personalSpouseTax: {
        total: personalSpouseTaxTotal,
        rows: personalSpouseTax
      },
      personalChangName: {
        total: personalChangNameTotal,
        rows: personalChangName
      },
      personalDoc: {
        total: personalDocTotal,
        rows: personalDoc
      }
    }
  },
  educatWorkDetail () {
    const educatTotal = 12
    let educat = [
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'CODTITLE', flgdisp: '', stdrequire: '', usrrequire: 'Y', flgdefault: '', defaultval: '', desc_column: 'ระดับการศึกษา'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'NAMFIRST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วุฒิการศึกษา'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'NAMLAST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'คณะ/สาขา'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'NAMNICK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถาบันการศึกษา'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'NUMTEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเทศ'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'NUMTELEC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดการแก้ไข'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '7', tablename: 'TEMPLOY2', fieldname: 'LINEID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ลำดับที่'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '8', tablename: 'TEMPLOY2', fieldname: 'NUMOFFID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สาขาวิชาเอก'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '9', tablename: 'TEMPLOY2', fieldname: 'DTEOFFID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เกรดเฉลี่ย'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '10', tablename: 'TEMPLOY2', fieldname: 'ADRISSUE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ปีที่เริ่มศึกษา'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '11', tablename: 'TEMPLOY2', fieldname: 'CODPROVI', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ปีที่จบการศึก'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_1', seqno: '12', tablename: 'TEMPLOY2', fieldname: 'HOSPSS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'Upload File'}
    ]
    const workHisTotal = 20
    let workHis = [
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '1', tablename: 'TEMPLOY2', fieldname: 'ADRREG', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อบริษัท'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '2', tablename: 'TEMPLOY2', fieldname: 'CODPROVR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานที่'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '3', tablename: 'TEMPLOY2', fieldname: 'CODDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ตำแหน่ง'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'CODSUBDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่เริ่มทำงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'CODCNTYR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่ลาออก'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'CODPOSTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขล่าสุด'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '7', tablename: 'TEMPLOY2', fieldname: 'ADRCONT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขโดย'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '8', tablename: 'TEMPLOY2', fieldname: 'CODPROVC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'บริษัท'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '9', tablename: 'TEMPLOY2', fieldname: 'CODDISTC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเภทธุรกิจ'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '10', tablename: 'TEMPLOY2', fieldname: 'CODSUBDISTC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ลักษณะงานที่ทำ'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '11', tablename: 'TEMPLOY2', fieldname: 'CODCNTYC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ปัจจัยเสี่ยงต่อสุขภาพ'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '12', tablename: 'TEMPLOY2', fieldname: 'CODPOSTC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อุปกรณ์ป้องกันอันตราย(ชนิด)'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '13', tablename: 'TEMPLOY2', fieldname: 'CODDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานที่ตั้ง'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '14', tablename: 'TEMPLOY2', fieldname: 'CODSUBDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เบอร์โทรศัพท์'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '15', tablename: 'TEMPLOY2', fieldname: 'CODCNTYR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อหัวหน้างาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '16', tablename: 'TEMPLOY2', fieldname: 'CODPOSTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เหตุผลการออก'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '17', tablename: 'TEMPLOY2', fieldname: 'ADRCONT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เงินเดือนล่าสุด'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '18', tablename: 'TEMPLOY2', fieldname: 'CODPROVC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'หมายเหตุ'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '19', tablename: 'TEMPLOY2', fieldname: 'CODDISTC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'Upload File'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_2', seqno: '20', tablename: 'TEMPLOY2', fieldname: 'CODSUBDISTC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ลำดับที่'}
    ]
    const trainHisTotal = 9
    let trainHis = [
      {codapp: 'HRPMC2E', tabno: 'TAB2_3', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'DTEEMPT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'หลักสูตรอบรม'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_3', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'STAEMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่ฝึกอบรม'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_3', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'DTEEFFEX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานที่ฝึกอบรม'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_3', seqno: '4', tablename: 'TEMPLOY1', fieldname: 'CODDOMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'จัดโดย'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_3', seqno: '5', tablename: 'TEMPLOY1', fieldname: 'CODPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดการแก้ไข'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_3', seqno: '6', tablename: 'TEMPLOY1', fieldname: 'DTEEFPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ลำดับที่'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_3', seqno: '7', tablename: 'TEMPLOY1', fieldname: 'NUMLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ถึง'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_3', seqno: '8', tablename: 'TEMPLOY1', fieldname: 'DTEEFLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'จัดโดย'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_3', seqno: '9', tablename: 'TEMPLOY1', fieldname: 'CODBRLC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'Upload File'}
    ]
    const internalTrainHisTotal = 8
    let internalTrainHis = [
      {codapp: 'HRPMC2E', tabno: 'TAB2_4', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'CODTRAVEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสหลักสูตร'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_4', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'NUMTRAVEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อหลักสูตร'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_4', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'FUELTYP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเภทการอบรม'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_4', seqno: '4', tablename: 'TEMPLOY1', fieldname: 'DISTANCE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่เริ่มต้น'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_4', seqno: '5', tablename: 'TEMPLOY1', fieldname: 'CODCABLE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่สิ้นสุด'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_4', seqno: '6', tablename: 'TEMPLOY1', fieldname: 'CODPOINT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ค่าใช้จ่ายอบรม (บาท/คน)'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_4', seqno: '7', tablename: 'TEMPLOY1', fieldname: 'CODPOINT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'จำนวน ชม. อบรม'},
      {codapp: 'HRPMC2E', tabno: 'TAB2_4', seqno: '8', tablename: 'TEMPLOY1', fieldname: 'CODPOINT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดการแก้ไข'}
    ]
    return {
      educat: {
        total: educatTotal,
        rows: educat
      },
      workHis: {
        total: workHisTotal,
        rows: workHis
      },
      trainHis: {
        total: trainHisTotal,
        rows: trainHis
      },
      internalTrainHis: {
        total: internalTrainHisTotal,
        rows: internalTrainHis
      }
    }
  },
  familyDetail () {
    const spouseTotal = 24
    let spouse = [
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'CODTITLE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดคู่สมรส'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'NAMFIRST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสพนักงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'NAMLAST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ในกรณีที่เป็นพนักงานบริษันี้'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'NAMNICK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'Upload รูปภาพ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'NUMTEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'คำนำหน้า'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'NUMTELEC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '7', tablename: 'TEMPLOY2', fieldname: 'LINEID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'นามสกุล'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '8', tablename: 'TEMPLOY2', fieldname: 'NUMOFFID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่บัตรประชาชน'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '9', tablename: 'TEMPLOY2', fieldname: 'DTEOFFID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันเกิด'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '10', tablename: 'TEMPLOY2', fieldname: 'ADRISSUE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานะ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '11', tablename: 'TEMPLOY2', fieldname: 'CODPROVI', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'มีชีวิต'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '12', tablename: 'TEMPLOY2', fieldname: 'HOSPSS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เสียชีวิต'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '13', tablename: 'TEMPLOY1', fieldname: 'CODTITLE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานะรายได้'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '14', tablename: 'TEMPLOY1', fieldname: 'NAMFIRST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'มี'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '15', tablename: 'TEMPLOY1', fieldname: 'NAMLAST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ไม่มี'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '16', tablename: 'TEMPLOY2', fieldname: 'NAMNICK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'บริษัท'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '17', tablename: 'TEMPLOY2', fieldname: 'NUMTEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อาชีพ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '18', tablename: 'TEMPLOY2', fieldname: 'NUMTELEC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่บัตรประชาชนบิดา'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '19', tablename: 'TEMPLOY2', fieldname: 'LINEID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่บัตรประชาชนมารดา'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '20', tablename: 'TEMPLOY2', fieldname: 'NUMOFFID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ข้อมูลการจดทะเบียน'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '21', tablename: 'TEMPLOY2', fieldname: 'DTEOFFID', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'จังหวัด'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '22', tablename: 'TEMPLOY2', fieldname: 'ADRISSUE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเทศ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '23', tablename: 'TEMPLOY2', fieldname: 'CODPROVI', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานที่จดทะเบียน'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_1', seqno: '24', tablename: 'TEMPLOY2', fieldname: 'HOSPSS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อื่นๆ'}
    ]
    const childrenTotal = 6
    let children = [
      {codapp: 'HRPMC2E', tabno: 'TAB3_2', seqno: '1', tablename: 'TEMPLOY2', fieldname: 'ADRREG', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อบุตร'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_2', seqno: '2', tablename: 'TEMPLOY2', fieldname: 'CODPROVR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่บัตรประชาชน'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_2', seqno: '3', tablename: 'TEMPLOY2', fieldname: 'CODDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันเกิด'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_2', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'CODSUBDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เพศ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_2', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'CODCNTYR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานะการศึกษา'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_2', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'CODPOSTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'การลดหย่อนภาษี'}
    ]
    const parentTotal = 35
    let parent = [
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'DTEEMPT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ข้อมูลบิดา'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'STAEMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ข้อมูลมารดา'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'DTEEFFEX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสพนักงาน'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '4', tablename: 'TEMPLOY1', fieldname: 'CODDOMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ในกรณีที่เป็นพนักงานบริษันี้'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '5', tablename: 'TEMPLOY1', fieldname: 'CODPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'Upload รูปภาพ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '6', tablename: 'TEMPLOY1', fieldname: 'DTEEFPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'คำนำหน้า'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '7', tablename: 'TEMPLOY1', fieldname: 'NUMLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '8', tablename: 'TEMPLOY1', fieldname: 'DTEEFLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'นามสกุล'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '9', tablename: 'TEMPLOY1', fieldname: 'CODBRLC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่บัตรประชาชน'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '10', tablename: 'TEMPLOY1', fieldname: 'DTEEMPT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันเกิด'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '11', tablename: 'TEMPLOY1', fieldname: 'STAEMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สัญชาติ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '12', tablename: 'TEMPLOY1', fieldname: 'DTEEFFEX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ศาสนา'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '13', tablename: 'TEMPLOY1', fieldname: 'CODDOMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อาชีพ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '14', tablename: 'TEMPLOY1', fieldname: 'CODPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'สถานะ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '15', tablename: 'TEMPLOY1', fieldname: 'DTEEFPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'มีชีวิต'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '16', tablename: 'TEMPLOY1', fieldname: 'NUMLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เสียชีวิต'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '17', tablename: 'TEMPLOY1', fieldname: 'DTEEFLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'Upload File'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '18', tablename: 'TEMPLOY1', fieldname: 'CODBRLC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'บุคคลที่สามารถติดต่อได้กรณีฉุกเฉิน'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '19', tablename: 'TEMPLOY1', fieldname: 'CODPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ที่อยู่'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '20', tablename: 'TEMPLOY1', fieldname: 'DTEEFPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสไปรษณีย์'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '21', tablename: 'TEMPLOY1', fieldname: 'NUMLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'โทรศัพท์'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '22', tablename: 'TEMPLOY1', fieldname: 'DTEEFLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แฟกซ์'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '23', tablename: 'TEMPLOY1', fieldname: 'CODBRLC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อีเมล์'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_3', seqno: '24', tablename: 'TEMPLOY1', fieldname: 'CODBRLC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ความสัมพันธ์'}
    ]
    const relativeTotal = 5
    let relative = [
      {codapp: 'HRPMC2E', tabno: 'TAB3_4', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'CODTRAVEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อ-นามสกุล'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_4', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'NUMTRAVEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่บัตรประชาชน'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_4', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'FUELTYP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันเกิด'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_4', seqno: '4', tablename: 'TEMPLOY1', fieldname: 'DISTANCE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เพศ'},
      {codapp: 'HRPMC2E', tabno: 'TAB3_4', seqno: '5', tablename: 'TEMPLOY1', fieldname: 'CODCABLE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ความสัมพันธ์'}
    ]
    return {
      spouse: {
        total: spouseTotal,
        rows: spouse
      },
      children: {
        total: childrenTotal,
        rows: children
      },
      parent: {
        total: parentTotal,
        rows: parent
      },
      relative: {
        total: relativeTotal,
        rows: relative
      }
    }
  },
  guaranteeDetail () {
    const guarantorTotal = 5
    let guarantor = [
      {codapp: 'HRPMC2E', tabno: 'TAB4_1', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'CODTITLE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อผู้ค้ำประกัน'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_1', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'NAMFIRST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่ค้ำประกัน'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_1', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'NAMLAST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่สิ้นสุด'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_1', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'NAMNICK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ความสัมพันธ์'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_1', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'NUMTEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อาชีพ'}
    ]
    const securitiesTotal = 4
    let securities = [
      {codapp: 'HRPMC2E', tabno: 'TAB4_2', seqno: '1', tablename: 'TEMPLOY2', fieldname: 'ADRREG', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัสหลักทรัพย์'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_2', seqno: '2', tablename: 'TEMPLOY2', fieldname: 'CODPROVR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเภทหลักทรัพย์'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_2', seqno: '3', tablename: 'TEMPLOY2', fieldname: 'CODDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'มูลค่า'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_2', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'CODSUBDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่เอกสาร'}
    ]
    const referenceTotal = 6
    let reference = [
      {codapp: 'HRPMC2E', tabno: 'TAB4_3', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'DTEEMPT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ชื่อ'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_3', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'STAEMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ความสัมพันธ์'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_3', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'DTEEFFEX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ตำแหน่ง'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_3', seqno: '4', tablename: 'TEMPLOY1', fieldname: 'CODDOMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'บริษัท'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_3', seqno: '5', tablename: 'TEMPLOY1', fieldname: 'CODPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขล่าสุด'},
      {codapp: 'HRPMC2E', tabno: 'TAB4_3', seqno: '6', tablename: 'TEMPLOY1', fieldname: 'DTEEFPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขโดย'}
    ]
    return {
      guarantor: {
        total: guarantorTotal,
        rows: guarantor
      },
      securities: {
        total: securitiesTotal,
        rows: securities
      },
      reference: {
        total: referenceTotal,
        rows: reference
      }
    }
  },
  talentDetail () {
    const talentTotal = 6
    let talent = [
      {codapp: 'HRPMC2E', tabno: 'TAB5_1', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'CODTITLE', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเภทความสามารถ'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_1', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'NAMFIRST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รหัส'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_1', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'NAMLAST', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียด'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_1', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'NAMNICK', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'คะแนที่ได้'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_1', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'NUMTEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ค่าคาดหวัง'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_1', seqno: '6', tablename: 'TEMPLOY2', fieldname: 'NUMTELEC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'Gap'}
    ]
    const langSkillTotal = 5
    let langSkill = [
      {codapp: 'HRPMC2E', tabno: 'TAB5_2', seqno: '1', tablename: 'TEMPLOY2', fieldname: 'ADRREG', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ฟัง'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_2', seqno: '2', tablename: 'TEMPLOY2', fieldname: 'CODPROVR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'พูด'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_2', seqno: '3', tablename: 'TEMPLOY2', fieldname: 'CODDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อ่าน'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_2', seqno: '4', tablename: 'TEMPLOY2', fieldname: 'CODSUBDISTR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เขียน'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_2', seqno: '5', tablename: 'TEMPLOY2', fieldname: 'CODCNTYR', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดการแก้ไข'}
    ]
    const goodnessTotal = 14
    let goodness = [
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'DTEEMPT', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่บันทึก'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'STAEMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'ประเภทรางวัล'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'DTEEFFEX', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดคุณความี'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '4', tablename: 'TEMPLOY1', fieldname: 'CODDOMP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'เลขที่เอกสารอ้างอิง'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '5', tablename: 'TEMPLOY1', fieldname: 'CODPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขล่าสุด'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '6', tablename: 'TEMPLOY1', fieldname: 'DTEEFPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'แก้ไขโดย'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '7', tablename: 'TEMPLOY1', fieldname: 'NUMLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดการแก้ไข'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '8', tablename: 'TEMPLOY1', fieldname: 'DTEEFLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียดคุณความดี'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '9', tablename: 'TEMPLOY1', fieldname: 'CODBRLC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่ประกาศ'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '10', tablename: 'TEMPLOY1', fieldname: 'CODPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รางวัล'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '11', tablename: 'TEMPLOY1', fieldname: 'DTEEFPOS', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'อ้างอิงเอกสารเลขที่'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '12', tablename: 'TEMPLOY1', fieldname: 'NUMLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'Upload File'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '13', tablename: 'TEMPLOY1', fieldname: 'DTEEFLVL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'รายละเอียด'},
      {codapp: 'HRPMC2E', tabno: 'TAB5_3', seqno: '14', tablename: 'TEMPLOY1', fieldname: 'CODBRLC', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'Upload รูปภาพ'}
    ]
    return {
      talent: {
        total: talentTotal,
        rows: talent
      },
      langSkill: {
        total: langSkillTotal,
        rows: langSkill
      },
      goodness: {
        total: goodnessTotal,
        rows: goodness
      }
    }
  },
  othersDetail () {
    const othersTotal = 3
    let others = [
      {codapp: 'HRPMC2E', tabno: 'TAB6', seqno: '1', tablename: 'TEMPLOY1', fieldname: 'CODTRAVEL', flgdisp: '', stdrequire: '', usrrequire: 'Y', flgdefault: '', defaultval: '', desc_column: 'สถานะการปรับลด'},
      {codapp: 'HRPMC2E', tabno: 'TAB6', seqno: '2', tablename: 'TEMPLOY1', fieldname: 'NUMTRAVEL', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่ปรับลด'},
      {codapp: 'HRPMC2E', tabno: 'TAB6', seqno: '3', tablename: 'TEMPLOY1', fieldname: 'FUELTYP', flgdisp: '', stdrequire: '', usrrequire: '', flgdefault: '', defaultval: '', desc_column: 'วันที่สิ้นสุด'}
    ]
    return {
      total: othersTotal,
      rows: others
    }
  },
  save () {
    return assetsLabels.mockPostMessage
  }
}
