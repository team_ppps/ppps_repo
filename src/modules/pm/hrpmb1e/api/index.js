import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLabels () {
    if (isMock) return generator(mocker.labels())
    return instance().get(endpoint + '/hrpmb1e/getLabels')
  },
  getPersonalDetail () {
    if (isMock) return generator(mocker.personalDetail())
    return instance().get(endpoint + '/hrpmb1e/personal')
  },
  getEducatWorkDetail () {
    if (isMock) return generator(mocker.educatWorkDetail())
    return instance().get(endpoint + '/hrpmb1e/educatWork')
  },
  getGuaranteeDetail () {
    if (isMock) return generator(mocker.guaranteeDetail())
    return instance().get(endpoint + '/hrpmb1e/guarantee')
  },
  getFamilyDetail () {
    if (isMock) return generator(mocker.familyDetail())
    return instance().get(endpoint + '/hrpmb1e/family')
  },
  getTalentDetail () {
    if (isMock) return generator(mocker.talentDetail())
    return instance().get(endpoint + '/hrpmb1e/talent')
  },
  getOthersDetail () {
    if (isMock) return generator(mocker.othersDetail())
    return instance().get(endpoint + '/hrpmb1e/others')
  },
  postData (saveParams) {
    if (isMock) return generator(mocker.save())
    let mapUsrrequire = saveParams.map((dataMap, index) => {
      saveParams[index].usrrequire = dataMap.usrrequire ? 'Y' : ''
      return saveParams[index]
    })
    let sendParams = {
      param_json: JSON.stringify(mapUsrrequire)
    }
    return instance().post(endpoint + '/hrpmb1e/saveRequire', sendParams)
  }
}
