export let hrpmb1eColumns = () => {
  let indexColumns = [
    {
      key: 'compy_image',
      name: '',
      labelCodapp: 'HRCO04EC1',
      labelIndex: '30',
      style: 'min-width: 100px;'
    }, {
      key: 'codcompy',
      name: '',
      labelCodapp: 'HRCO04EC1',
      labelIndex: '10',
      class: 'align-center',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_codcompy',
      name: '',
      labelCodapp: 'HRCO04EC1',
      labelIndex: '20',
      style: 'min-width: 200px;'
    }
  ]

  let detailColumn = [
    {
      key: 'codcomp',
      name: '',
      labelCodapp: 'HRCO04EC2',
      labelIndex: '20',
      style: 'min-width: 200px;'
    }, {
      key: 'desc_codcomp',
      name: '',
      labelCodapp: 'HRCO04EC2',
      labelIndex: '30',
      style: 'min-width: 200px;'
    }, {
      key: 'naminit',
      name: '',
      labelCodapp: 'HRCO04EC2',
      labelIndex: '50',
      style: 'min-width: 100px;'
    }, {
      key: 'desc_status',
      name: '',
      labelCodapp: 'HRCO04EC2',
      labelIndex: '40',
      style: 'min-width: 100px;'
    }
  ]
  return {
    en: {
      index: indexColumns,
      detail: detailColumn
    },
    th: {
      index: indexColumns,
      detail: detailColumn
    }
  }
}
