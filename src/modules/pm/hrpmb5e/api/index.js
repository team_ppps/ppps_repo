import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLabels () {
    if (isMock) return generator(mocker.labels())
    return instance().get(endpoint + '/hrpmb5e/labels')
  },
  getIndex (searchParams) {
    if (isMock) return generator(mocker.index())
    return instance().get(endpoint + '/hrpmb5e/index')
  },
  getIndexHead (searchParams) {
    if (isMock) return generator(mocker.indexHead())
    const params = searchParams.p_codcompy + '/' + searchParams.p_dteeffect
    return instance().get(endpoint + '/hrpmb5e/indexHead/' + params)
  },
  getDetailCreate () {
    return generator(mocker.detailCreate())
  },
  getDetail (searchParams) {
    if (isMock) return generator(mocker.detail(searchParams))
    const params = searchParams.codfrm
    return instance().get(endpoint + '/hrpmb5e/detail/' + params)
  },
  saveDetail ({ indexSelected, detail }) {
    let saveParams = {
      param_json: JSON.stringify({
        p_codsecu: indexSelected.codsecu
      })
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmb5e/detailSave', saveParams)
  },
  saveCreateTablePopup (dataRowsHasFlg) {
    var saveParams = {
      'param_json': JSON.stringify(dataRowsHasFlg)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmb5e/saveCreateTable', saveParams)
  },
  deleteIndex (dataRowsHasFlg) {
    let sendParams = {
      param_json: JSON.stringify(dataRowsHasFlg)
    }
    if (isMock) return generator(mocker.delete())
    return instance().post(endpoint + '/hrpmb5e/delete', sendParams)
  }
}
