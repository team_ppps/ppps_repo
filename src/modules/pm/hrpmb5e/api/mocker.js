import hrpmb5eLabels from '../assets/labels'
// import faker from 'faker'
// import moment from 'moment'
import assetsLabels from 'assets/js/labels'

export const mocker = {
  labels () {
    return {
      labels: hrpmb5eLabels
    }
  },
  index () {
    let mockRows = []
    const mockTotal = 3
    mockRows.push({
      codfrm: 'TEST1',
      namfrm: 'สำหรับเทส'
    })
    mockRows.push({
      codfrm: 'TEST2',
      namfrm: 'testtest2'
    })
    mockRows.push({
      codfrm: 'TEST55',
      namfrm: 'test55  0300'
    })

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  indexHead () {
    let mockIndexHead = {
      codcompy: 'TJS-บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด',
      desc_codcompy: 'บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด',
      dteeffect: '12/01/2018'
    }

    return mockIndexHead
  },
  detailCreate () {
    let mockDetail = {
      codcompy: '',
      dteeffect: '',
      codfrm: '',
      namfrm: '',
      syncond: ''
    }
    let mockRowsDetail = []
    const mockTotalDetail = 2
    mockRowsDetail.push({
      codincome: '01',
      desc_codincome: 'Salary',
      apb: '',
      apc: ''
    })
    mockRowsDetail.push({
      codincome: '02',
      desc_codincome: 'Postion',
      apb: '',
      apc: ''
    })
    mockRowsDetail.push({
      codincome: '05',
      desc_codincome: 'ค่าครองชีพ',
      apb: '',
      apc: ''
    })
    mockRowsDetail.push({
      codincome: '06',
      desc_codincome: 'ค่าวิชาชีพ',
      apb: '',
      apc: ''
    })

    return {
      detail: mockDetail,
      table: {
        total: mockTotalDetail,
        rows: mockRowsDetail
      }
    }
  },
  detail (searchParams) {
    let mockDetail = {}
    let mockRowsDetail = []
    const mockTotalDetail = 4

    if (searchParams.codfrm === 'TEST1') {
      mockDetail = {
        codcompy: 'TJS-บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด',
        dteeffect: '12/01/2018',
        codfrm: 'TEST1',
        namfrm: 'สำหรับเทส',
        syncond: 'รหัสหน่วยงาน LIKE แผนกติดตั้ง/ออกแบบ/ตรวจสอบวางแผนพัฒนางาน AND รหัสตำแหน่ง = Advisor'
      }
      mockRowsDetail.push({
        codincome: '01',
        desc_codincome: 'Salary',
        apb: '',
        apc: ''
      })
      mockRowsDetail.push({
        codincome: '02',
        desc_codincome: 'Postion',
        apb: '1000',
        apc: '2000'
      })
      mockRowsDetail.push({
        codincome: '05',
        desc_codincome: 'ค่าครองชีพ',
        apb: '1000',
        apc: '1500'
      })
      mockRowsDetail.push({
        codincome: '06',
        desc_codincome: 'ค่าวิชาชีพ',
        apb: '',
        apc: ''
      })
    } else if (searchParams.codfrm === 'TEST2') {
      mockDetail = {
        codcompy: 'TJS-บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด',
        dteeffect: '12/01/2018',
        codfrm: 'TEST2',
        namfrm: 'testtest2',
        syncond: 'รหัสหน่วยงาน LIKE ฝ่ายขายและการตลาด AND รหัสตำแหน่ง = Staff Officer'
      }
      mockRowsDetail.push({
        codincome: '01',
        desc_codincome: 'Salary',
        apb: '',
        apc: ''
      })
      mockRowsDetail.push({
        codincome: '02',
        desc_codincome: 'Postion',
        apb: '1000',
        apc: '2000'
      })
      mockRowsDetail.push({
        codincome: '05',
        desc_codincome: 'ค่าครองชีพ',
        apb: '1000',
        apc: '1500'
      })
      mockRowsDetail.push({
        codincome: '06',
        desc_codincome: 'ค่าวิชาชีพ',
        apb: '',
        apc: ''
      })
    }

    return {
      detail: mockDetail,
      table: {
        total: mockTotalDetail,
        rows: mockRowsDetail
      }
    }
  },
  delete () {
    return assetsLabels.mockPostMessage
  },
  save () {
    return assetsLabels.mockPostMessage
  }
}
