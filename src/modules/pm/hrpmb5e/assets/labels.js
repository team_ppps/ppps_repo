module.exports = {
  en: {
    'HRPMB5E1': {
      '10': 'Company Code',
      '20': 'Effective Date ',
      '30': 'Condition code',
      '40': 'Condition name'
    },
    'HRPMB5E2': {
      '10': 'Company Code',
      '20': 'Effective Date',
      '30': 'Condition Code',
      '40': 'Detail',
      '50': 'Condition Name',
      '60': 'Conditional details',
      '70': 'Income Code',
      '80': 'Description',
      '90': 'Probation',
      '100': 'Placement'
    }
  },
  th: {
    'HRPMB5E1': {
      '10': 'รหัสบริษัท',
      '20': 'วันที่มีผลบังคับใช้',
      '30': 'รหัสเงื่อนไข',
      '40': 'ชื่อเงื่อนไข'
    },
    'HRPMB5E2': {
      '10': 'รหัสบริษัท',
      '20': 'วันที่มีผลบังคับใช้',
      '30': 'รหัสเงื่อนไข',
      '40': 'รายละเอียด',
      '50': 'ชื่อเงื่อนไข',
      '60': 'รายละเอียดเงื่อนไข',
      '70': 'รหัสรายได้',
      '80': 'คำอธิบาย',
      '90': 'ทดลองงาน',
      '100': 'บรรจุ'
    }
  }
}
