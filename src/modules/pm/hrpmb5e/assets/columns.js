export let hrpmb5eColumns = () => {
  let indexColumns = [{
    key: 'codfrm',
    name: '',
    labelCodapp: 'HRPMB5E1',
    labelIndex: '30',
    style: 'min-width: 100px;'
  }, {
    key: 'namfrm',
    name: '',
    labelCodapp: 'HRPMB5E1',
    labelIndex: '40',
    style: 'min-width: 200px;'
  }]

  let detailColumns = [{
    key: 'codincome',
    name: '',
    labelCodapp: 'HRPMB5E2',
    labelIndex: '70',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codincome',
    name: '',
    labelCodapp: 'HRPMB5E2',
    labelIndex: '80',
    style: 'min-width: 190px;'
  }, {
    key: 'apb',
    name: '',
    labelCodapp: 'HRPMB5E2',
    labelIndex: '90',
    style: 'min-width: 100px;',
    inlineType: 'text'
  }, {
    key: 'apc',
    name: '',
    labelCodapp: 'HRPMB5E2',
    labelIndex: '100',
    style: 'min-width: 100px;',
    inlineType: 'text'
  }]

  return {
    en: {
      index: indexColumns,
      detail: detailColumns
    },
    th: {
      index: indexColumns,
      detail: detailColumns
    }
  }
}

