import * as types from './mutation-types'
import hrpmb5e from '../api'
import swal from 'sweetalert'
import hrpmb5eLabels from '../assets/labels'
import { hrpmb5eColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'
import { constLabels } from 'assets/js/constLabels'

export default {
  state: {
    indexDisp: true,
    indexHeadDisp: false,
    detailDisp: false,
    labels: hrpmb5eLabels,
    columns: hrpmb5eColumns(),
    index: {
      total: 0,
      rows: []
    },
    indexSelected: {},
    detail: {
      detail: {
        codcompy: '',
        codfrm: ''
      },
      table: {
        total: 0,
        rows: []
      }
    },
    indexHead: {}
  },
  getters: {
    [types.GET_HRPMB5E_INDEX_DISP] (state) {
      return state.indexDisp
    },
    [types.GET_HRPMB5E_INDEX_HEAD_DISP] (state) {
      return state.indexHeadDisp
    },
    [types.GET_HRPMB5E_DETAIL_DISP] (state) {
      return state.detailDisp
    },
    [types.GET_HRPMB5E_COLUMNS] (state) {
      return state.columns
    },
    [types.GET_HRPMB5E_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPMB5E_INDEX] (state) {
      return state.index
    },
    [types.GET_HRPMB5E_INDEX_HEAD] (state) {
      return state.indexHead
    },
    [types.GET_HRPMB5E_INDEX_SELECTED] (state) {
      return state.indexSelected
    },
    [types.GET_HRPMB5E_DETAIL] (state) {
      return state.detail
    }
  },
  mutations: {
    [types.SET_HRPMB5E_INDEX_DISP] (state, indexDisp) {
      state.indexDisp = indexDisp
    },
    [types.SET_HRPMB5E_INDEX_HEAD_DISP] (state, indexHeadDisp) {
      state.indexHeadDisp = indexHeadDisp
    },
    [types.SET_HRPMB5E_DETAIL_DISP] (state, detailDisp) {
      state.detailDisp = detailDisp
    },
    [types.SET_HRPMB5E_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.columns[keyLang].index = assetsLabels.replaceLabelToColumns(state.columns[keyLang].index, labels[keyLang])
        state.columns[keyLang].detail = assetsLabels.replaceLabelToColumns(state.columns[keyLang].detail, labels[keyLang])
      }
    },
    [types.SET_HRPMB5E_LABELS] (state, labels) {
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPMB5E_INDEX] (state, index) {
      state.index = index
    },
    [types.SET_HRPMB5E_INDEX_HEAD] (state, indexHead) {
      state.indexHead = indexHead
    },
    [types.SET_HRPMB5E_INDEX_SELECTED] (state, indexSelected) {
      state.indexSelected = indexSelected
    },
    [types.SET_HRPMB5E_DETAIL] (state, detail) {
      state.detail = detail
    }
  },
  actions: {
    [types.TOGGLE_HRPMB5E_PAGE] ({ commit }, page) {
      let indexDisp = false
      let detailDisp = false
      let indexHeadDisp = false
      switch (page) {
        case 'index':
          indexDisp = true
          break
        case 'detail':
          detailDisp = true
          break
        case 'indexHead':
          indexHeadDisp = true
          break
      }
      commit(types.SET_HRPMB5E_INDEX_DISP, indexDisp)
      commit(types.SET_HRPMB5E_DETAIL_DISP, detailDisp)
      commit(types.SET_HRPMB5E_INDEX_HEAD_DISP, indexHeadDisp)
    },
    [types.RECEIVED_HRPMB5E_COLUMNS_LABELS] ({ commit }) {
      hrpmb5e.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB5E_COLUMNS, data.labels)
          commit(types.SET_HRPMB5E_LABELS, data.labels)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB5E_INDEX] ({ commit }, searchParams) {
      hrpmb5e.getIndex(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB5E_INDEX, data)
          // commit(types.SET_HRPMB5E_INDEX_HEAD_DISP, true)
        })
        .catch((response) => {
          const data = response.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB5E_INDEX_HEAD] ({ commit }, searchParams) {
      hrpmb5e.getIndexHead(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB5E_INDEX_HEAD, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB5E_DETAIL_CREATE] ({ commit }) {
      hrpmb5e.getDetailCreate()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB5E_DETAIL, data)
        })
        .catch((response) => {
          const data = response.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB5E_DETAIL] ({ commit }, searchParams) {
      hrpmb5e.getDetail(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB5E_DETAIL, data)
        })
        .catch((response) => {
          const data = response.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.SAVE_HRPMB5E_DETAIL] ({ commit, dispatch }, { searchIndex, indexSelected, detail }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        html: true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel')
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmb5e.saveDetail({ indexSelected, detail })
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMB5E_INDEX, searchIndex)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              const data = error.response.data
              swal({title: '', text: data.response, html: true, type: 'error'})
            })
        }
      })
    },
    [types.DELETE_HRPMB5E_INDEX] ({ commit, dispatch }, { searchIndex, dataRowsHasFlg }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmCancel'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel'),
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmb5e.deleteIndex(dataRowsHasFlg)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMB5E_INDEX, searchIndex)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              let message = error
              if (error.response) {
                const data = error.response.data
                message = data.response
              }
              swal({title: '', text: message, html: true, type: 'error'})
            })
        }
      })
    }
  }
}
