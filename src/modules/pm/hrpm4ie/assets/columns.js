export let hrpm4ceColumns = () => {
  let indexColumns = [{
    key: 'dteeffect',
    name: '',
    labelCodapp: 'HRPM4CEC1',
    labelIndex: '40',
    style: 'min-width: 100px;'
  }, {
    key: 'image',
    name: '',
    labelCodapp: 'HRPM4CEC1',
    labelIndex: '5',
    style: 'min-width: 100px;',
    tagType: 'image'
  }, {
    key: 'codempid',
    name: '',
    labelCodapp: 'HRPM4CEC1',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codempid',
    name: '',
    labelCodapp: 'HRPM4CEC1',
    labelIndex: '60',
    style: 'min-width: 150px;'
  }, {
    key: 'desc_codcomp',
    name: '',
    labelCodapp: 'HRPM4CEC1',
    labelIndex: '70',
    style: 'min-width: 200px;'
  }, {
    key: 'desc_codpos',
    name: '',
    labelCodapp: 'HRPM4CEC1',
    labelIndex: '80',
    style: 'min-width: 150px;'
  }, {
    key: 'dteexpir',
    name: '',
    labelCodapp: 'HRPM4CEC1',
    labelIndex: '90',
    style: 'min-width: 100px;'
  }, {
    key: 'typcodpos',
    name: '',
    labelCodapp: 'HRPM4CEC1',
    labelIndex: '100',
    style: 'min-width: 100px;'
  }]

  let detailTab2Columns = [{
    key: 'codincome',
    name: '',
    labelCodapp: 'HRPM4CEC3',
    labelIndex: '30',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codincome',
    name: '',
    labelCodapp: 'HRPM4CEC3',
    labelIndex: '40',
    style: 'min-width: 190px;'
  }, {
    key: 'unit',
    name: '',
    labelCodapp: 'HRPM4CEC3',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: 'amount',
    name: '',
    labelCodapp: 'HRPM4CEC3',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }, {
    key: 'asjamt',
    name: '',
    labelCodapp: 'HRPM4CEC3',
    labelIndex: '70',
    style: 'min-width: 100px;',
    inlineType: 'text'
  }, {
    key: 'newincom',
    name: '',
    labelCodapp: 'HRPM4CEC3',
    labelIndex: '80',
    style: 'min-width: 100px;'
  }]

  let detailAddOtherDataColumns = [{
    key: 'column_name',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '30',
    style: 'min-width: 200px;'
  }, {
    key: 'descolumn',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '40',
    style: 'min-width: 200px;'
  }, {
    key: 'datatype',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '50',
    style: 'min-width: 200px;'
  }, {
    key: 'data',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '60',
    style: 'min-width: 200px;'
  }]

  let detailCreateTableColumns = [{
    key: 'fieldname',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '100',
    style: 'min-width: 200px;',
    inlineType: 'text'
  }, {
    key: 'descolumn',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '110',
    style: 'min-width: 200px;',
    inlineType: 'text'
  }, {
    key: 'datatype',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '120',
    style: 'min-width: 200px;',
    inlineType: 'select',
    inlineDropdown: {}
  }, {
    key: 'size',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '130',
    style: 'min-width: 200px;',
    class: 'align-center',
    inlineType: 'text'
  }, {
    key: 'precision',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '140',
    style: 'min-width: 200px;',
    inlineType: 'text'
  }, {
    key: 'scale',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '150',
    style: 'min-width: 200px;',
    class: 'align-center',
    inlineType: 'text'
  }, {
    key: 'primary',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '160',
    style: 'min-width: 200px;',
    inlineType: 'text'
  }, {
    key: 'standard',
    name: '',
    labelCodapp: 'HRCO2FEC1',
    labelIndex: '170',
    style: 'min-width: 200px;',
    inlineType: 'text'
  }]

  return {
    en: {
      index: indexColumns,
      detailTab2: detailTab2Columns,
      detailOtherData: detailAddOtherDataColumns,
      detailCreateTable: detailCreateTableColumns
    },
    th: {
      index: indexColumns,
      detailTab2: detailTab2Columns,
      detailOtherData: detailAddOtherDataColumns,
      detailCreateTable: detailCreateTableColumns
    }
  }
}

