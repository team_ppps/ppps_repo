import hrpm4ceLabels from '../assets/labels'
// import faker from 'faker'
// import moment from 'moment'
import assetsLabels from 'assets/js/labels'

export const mocker = {
  labels () {
    let datatypeDropdown = {
      '1': 'VARCHAR2',
      '2': 'DATE',
      '3': 'NUMBER',
      '4': 'DECIMAL'
    }
    return {
      labels: hrpm4ceLabels,
      dropdowns: {
        datatype: {
          en: datatypeDropdown,
          th: datatypeDropdown
        }
      }
    }
  },
  index () {
    let mockRows = []
    const mockTotal = 3
    mockRows.push({
      dteeffect: '19/08/2015',
      image: 'static/image/employee/emp_img28.jpg',
      codempid: '58002',
      desc_codempid: 'นายTony Happy',
      desc_codcomp: 'บริษัท พีเพิล พลัส ซอฟต์แวร์ จำกัด',
      desc_codpos: 'Software Tester',
      dteexpir: '',
      typcodpos: 'ตำแหน่งควบ'
    })
    mockRows.push({
      dteeffect: '11/02/2015',
      image: 'static/image/employee/emp_img28.jpg',
      codempid: '33127',
      desc_codempid: 'นายชัยยุทธ นิกรณ์',
      desc_codcomp: 'ส่วนพัฒนาระบบและดำเนินการ',
      desc_codpos: 'Project Manager',
      dteexpir: '',
      typcodpos: 'ตำแหน่งควบ'
    })
    mockRows.push({
      dteeffect: '15/09/2012',
      image: 'static/image/employee/emp_img28.jpg',
      codempid: '50165',
      desc_codempid: 'นายสมศักดิ์ สำรวยศักดิ์',
      desc_codcomp: 'ฝ่ายขายและการตลาด',
      desc_codpos: 'Support Manager',
      dteexpir: '',
      typcodpos: 'ตำแหน่งควบ'
    })

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  indexHead () {
    let mockIndexHead = {
      codempid: '',
      desc_codempid: '',
      codcomp: '',
      codpos: ''
    }

    return mockIndexHead
  },
  detailCreate () {
    let mockDetail = {}
    let mockDetailTab1 = {}
    let mockDetailTab2 = {}
    let mockRowsDetailTab2 = []
    const mockTotalDetailTab2 = 1

    mockDetail = {
      codempid: '',
      desc_codempid: '',
      desc_codpos: '',
      typcodpos: '',
      dteeffect: '',
      dteexpir: ''
    }
    mockDetailTab1 = {
      codtrn: '',
      dtecancel: '',
      seqno: '',
      desnote: '',
      codreq: ''
    }
    mockDetailTab2 = {
      currency: '',
      monincom1: '',
      dayincom1: '',
      ourincom1: '',
      monincom2: '',
      dayincom2: '',
      ourincom2: '',
      monincom3: '',
      dayincom3: '',
      ourincom3: ''
    }
    mockRowsDetailTab2.push({
      codincome: '',
      desc_codincome: '',
      unit: '',
      amount: '',
      asjamt: '',
      newincom: ''
    })

    return {
      detail: mockDetail,
      tab1: mockDetailTab1,
      tab2: {
        detail: mockDetailTab2,
        total: mockTotalDetailTab2,
        rows: mockRowsDetailTab2
      }
    }
  },
  detail (searchParams) {
    let mockDetail = {}
    let mockDetailTab1 = {}
    let mockDetailTab2 = {}
    let mockRowsDetailTab2 = []
    const mockTotalDetailTab2 = 3

    if (searchParams.codempid === '58002') {
      mockDetail = {
        codempid: '58002',
        desc_codempid: 'นายTony Happy',
        desc_codpos: 'Software Tester',
        typcodpos: 'ตำแหน่งควบ',
        dteeffect: '19/08/2015',
        dteexpir: ''
      }
      mockDetailTab1 = {
        codtrn: '007-ยกเลิกตำแหน่ง',
        dtecancel: '01/01/2018',
        seqno: '',
        desnote: 'ยกเลิกตำแหน่ง',
        codreq: '53100-นายกัปตัน ทดสอบ'
      }
      mockDetailTab2 = {
        currency: 'BAHT-บาท',
        monincom1: '39,600.00',
        dayincom1: '1,320.00',
        ourincom1: '165.00',
        monincom2: '1,000.00',
        dayincom2: '33.33',
        ourincom2: '4.17',
        monincom3: '46,000.00',
        dayincom3: '1,353.33',
        ourincom3: '169.17'
      }
      mockRowsDetailTab2.push({
        codincome: '01',
        desc_codincome: 'Salary',
        unit: 'ต่อเดือน',
        amount: '39600.00',
        asjamt: '1000.00',
        newincom: '40600.00'
      })
      mockRowsDetailTab2.push({
        codincome: '02',
        desc_codincome: 'Postion',
        unit: 'ต่อเดือน',
        amount: '0.00',
        asjamt: '',
        newincom: ''
      })
      mockRowsDetailTab2.push({
        codincome: '05',
        desc_codincome: 'ค่าครองชีพ',
        unit: 'ต่อเดือน',
        amount: '0.00',
        asjamt: '',
        newincom: ''
      })
    } else if (searchParams.codempid === '33127') {
      mockDetail = {
        codempid: '33127',
        desc_codempid: 'นายชัยยุทธ นิกรณ์',
        desc_codpos: 'Project Manager',
        typcodpos: 'ตำแหน่งควบ',
        dteeffect: '11/02/2015',
        dteexpir: ''
      }
      mockDetailTab1 = {
        codtrn: '007-ยกเลิกตำแหน่ง',
        dtecancel: '01/01/2018',
        seqno: '',
        desnote: 'ยกเลิกตำแหน่ง',
        codreq: '53100-นายกัปตัน ทดสอบ'
      }
      mockDetailTab2 = {
        currency: 'BAHT-บาท',
        monincom1: '39,600.00',
        dayincom1: '1,320.00',
        ourincom1: '165.00',
        monincom2: '1,000.00',
        dayincom2: '33.33',
        ourincom2: '4.17',
        monincom3: '46,000.00',
        dayincom3: '1,353.33',
        ourincom3: '169.17'
      }
      mockRowsDetailTab2.push({
        codincome: '01',
        desc_codincome: 'Salary',
        unit: 'ต่อเดือน',
        amount: '39600.00',
        asjamt: '1000.00',
        newincom: '40600.00'
      })
      mockRowsDetailTab2.push({
        codincome: '02',
        desc_codincome: 'Postion',
        unit: 'ต่อเดือน',
        amount: '0.00',
        asjamt: '',
        newincom: ''
      })
      mockRowsDetailTab2.push({
        codincome: '05',
        desc_codincome: 'ค่าครองชีพ',
        unit: 'ต่อเดือน',
        amount: '0.00',
        asjamt: '',
        newincom: ''
      })
    }

    return {
      detail: mockDetail,
      tab1: mockDetailTab1,
      tab2: {
        detail: mockDetailTab2,
        total: mockTotalDetailTab2,
        rows: mockRowsDetailTab2
      }
    }
  },
  popupOtherData () {
    let mockDetailOtherData = {
      fortable: 'TCOMPNY',
      desc_fortable: 'CO company profile'
    }
    let mockOtherDataRows = []
    const mockOtherDataTotal = 5
    mockOtherDataRows.push({
      column_name: 'USR_EMAIL2',
      descolumn: '1111',
      datatype: 'VARCHAR2(100)',
      data: ''
    })
    mockOtherDataRows.push({
      column_name: 'USR_EMAIL',
      descolumn: 'test',
      datatype: 'VARCHAR2(10)',
      data: ''
    })

    return {
      detail: mockDetailOtherData,
      table: {
        total: mockOtherDataTotal,
        rows: mockOtherDataRows
      }
    }
  },
  popupCreateTable () {
    let mockDetailCreateTable = {
      forprogram: 'HRCO21E',
      desc_forprogram: 'บันทึกรหัสลักษณะงาน',
      fortable: 'TJOBCODE',
      desc_fortable: 'CO Job Description1'
    }
    let mockCreateTableRows = []
    const mockCreateTableTotal = 5
    mockCreateTableRows.push({
      fieldname: 'CODJOB',
      descolumn: 'รหัสลักษณะงาน',
      datatype: '1',
      size: '3',
      precision: '',
      scale: '',
      primary: 'Y',
      standard: 'Y'
    })
    mockCreateTableRows.push({
      fieldname: 'SEQNO',
      descolumn: 'ลำดับที่',
      datatype: '1',
      size: '45',
      precision: '',
      scale: '',
      primary: 'Y',
      standard: 'N'
    })
    mockCreateTableRows.push({
      fieldname: 'DESJOB',
      descolumn: 'ความรับผิดชอบ',
      datatype: '1',
      size: '10',
      precision: '',
      scale: '',
      primary: 'Y',
      standard: 'N'
    })

    return {
      detail: mockDetailCreateTable,
      table: {
        total: mockCreateTableTotal,
        rows: mockCreateTableRows
      }
    }
  },
  delete () {
    return assetsLabels.mockPostMessage
  },
  save () {
    return assetsLabels.mockPostMessage
  }
}
