import * as types from './mutation-types'
import hrpmc2e from '../api'
import swal from 'sweetalert'
import { hrpmc2eColumns } from '../assets/columns'
import hrpmc2eLabels from '../assets/labels'
import assetsLabels from 'assets/js/labels'
import { constLabels } from 'assets/js/constLabels'
import axios from 'axios'

export default {
  state: {
    indexDisp: true,
    detailDisp: true,
    detailDisabled: true,
    checkBlacklistPopupDisp: false,
    smartCardPopupDisp: false,
    addOtherPopupDisp: false,
    labels: hrpmc2eLabels,
    columns: hrpmc2eColumns(),
    detail: {
      personalTax: {
        personal: {
          image: '',
          signature: '',
          hospss: '',
          codprovi: ''
        },
        address: {
          codcntyr: '',
          codcntyc: '',
          coddistr: '',
          coddistc: '',
          codprovc: '',
          codprovr: '',
          codsubdistr: '',
          codsubdistc: ''
        },
        work: {
          codcomp1: '',
          codpos: '',
          codbrlc: '',
          codempmt: '',
          typayroll: '',
          typemp: '',
          codcalen: '',
          codjob: '',
          jobgrade: '',
          codgrpgl: '',
          typdisp: '',
          numreqst: ''
        },
        travel: {
          codcable: '',
          codpoint: ''
        },
        income: {
          detail: {
            codcurr: ''
          },
          table: {
            total: 0,
            rows: []
          }
        },
        tax: {
          tab1: {
            codbank: '',
            codbank2: ''
          },
          tab2: {},
          tab3: {
            total: 0,
            rows: []
          },
          tab4: {
            detail: {},
            table: {
              total: 0,
              rows: []
            }
          },
          tab5: {
            total: 0,
            rows: []
          }
        },
        spouse: {
          tab1: {},
          tab2: {
            total: 0,
            rows: []
          },
          tab3: {
            total: 0,
            rows: []
          },
          tab4: {
            total: 0,
            rows: []
          }
        },
        changename: {
          total: 0,
          rows: []
        },
        document: {
          total: 0,
          rows: []
        }
      },
      workStudy: {
        tableEducation: {
          total: 0,
          rows: []
        },
        tableWorkExperiences: {
          total: 0,
          rows: []
        },
        tableTrainingRecord: {
          total: 0,
          rows: []
        },
        tableInternalTraining: {
          total: 0,
          rows: []
        }
      },
      family: {
        spouse: {},
        tableChildren: {
          total: 0,
          rows: []
        },
        parent: {
          father: {},
          mother: {},
          other: {}
        },
        tableRelatives: {
          total: 0,
          rows: []
        }
      },
      guarntee: {
        guarntee1: {
          total: 0,
          rows: []
        },
        guarntee2: {
          total: 0,
          rows: []
        },
        guarntee3: {
          total: 0,
          rows: []
        }
      },
      other: {},
      competency: {
        tableCompetency: {
          total: 0,
          rows: []
        },
        tableLanguage: {
          total: 0,
          rows: []
        },
        tableHonors: {
          total: 0,
          rows: []
        }
      }
    },
    popupChangeDetail: {
      detail: {},
      table: {
        total: 0,
        rows: []
      }
    },
    popupCheckBlacklist: {},
    popupSmartCard: {},
    popupOtherCreate: {
      detail: {},
      table: {
        total: 0,
        rows: []
      }
    },
    popupAddOther: {
      fieldnam: '',
      detail: '',
      datatyp: '',
      size: '',
      listitem: '',
      codimg: ''
    },
    popupTax8: {
      dtechg: '',
      namtitle: '',
      namfirst: '',
      namlast: '',
      deschang: ''
    },
    popupTax9: {
      numseq: '',
      namtypdoc: '',
      namdoc: '',
      dterecv: '',
      dtedocen: '',
      numdoc: '',
      filedoc: '',
      desnote: ''
    },
    popupGuarantee1: {
      detail: {
        codguar: '',
        dteupdate: '',
        updateby: ''
      },
      tab1: {
        imgupload: '',
        codtitle: '',
        codnam: '',
        codlnam: '',
        bddte: '',
        dteretment: '',
        relation: '',
        contactus: '',
        codzip: '',
        telno: '',
        faxno: '',
        email: '',
        position: '',
        incomem: '',
        workplc: '',
        codoccup: ''
      },
      tab2: {
        cardtyp: '',
        cardid: '',
        dteexpir: '',
        note: '',
        fileupload: ''
      },
      tab3: {
        dteguarnt: '',
        dteexpir: '',
        cardtyp: '',
        lastyear: ''
      }
    },
    popupGuarantee2: {
      numcolla: '',
      numdocum: '',
      typcolla: '',
      amtcolla: '',
      dtecolla: '',
      dteeffec: '',
      dtertdoc: '',
      descoll: '',
      namimage: '',
      status: '',
      dteupdate: '',
      updateby: ''
    },
    popupGuarantee3: {
      codempid: '',
      numseq: '',
      imgupload: '',
      codtitle: '',
      codnam: '',
      codlnam: '',
      relation: '',
      codpos: '',
      address: '',
      codcomp: '',
      telno: '',
      email: '',
      codoccup: '',
      note: '',
      fileupload: ''
    },
    tax8Selected: {},
    tax9Selected: {},
    guarantee1Selected: {},
    guarantee2Selected: {},
    guarantee3Selected: {},
    addOtherSelected: {},
    educationSelected: {},
    popupEducation: {},
    workExperiencesSelected: {},
    popupWorkExperiences: {},
    trainingRecordSelected: {},
    popupTrainingRecord: {},
    childrenSelected: {},
    popupChildren: {},
    popupRelatives: {},
    popupHonors: {},
    honorsSelected: {},
    competencySelected: {},
    inlineCompetency1: {},
    inlineCompetency2: {},
    inlineCompetency3: {},
    inlineWorkStudy1: {},
    inlineWorkStudy2: {},
    inlineWorkStudy3: {},
    inlineFamily2: {},
    inlineFamily4: {},
    popupCodtency: {
      total: 0,
      rows: []
    },
    dropdowns: {
      codtitle: {
        en: {},
        th: {}
      },
      codsex: {
        en: {},
        th: {}
      },
      addOtherDataType: {
        en: {},
        th: {}
      },
      v_namgrade: {
        en: {},
        th: {}
      },
      relation: {
        en: {},
        th: {}
      },
      flgeduc: {
        en: {},
        th: {}
      }
    }
  },
  getters: {
    [types.GET_HRPMC2E_INDEX_DISP] (state) {
      return state.indexDisp
    },
    [types.GET_HRPMC2E_DETAIL_DISP] (state) {
      return state.detailDisp
    },
    [types.GET_HRPMC2E_CHECK_BLACKLIST_POPUP_DISP] (state) {
      return state.checkBlacklistPopupDisp
    },
    [types.GET_HRPMC2E_SMART_CARD_POPUP_DISP] (state) {
      return state.smartCardPopupDisp
    },
    [types.GET_HRPMC2E_ADD_OTHER_POPUP_DISP] (state) {
      return state.addOtherPopupDisp
    },
    [types.GET_HRPMC2E_COLUMNS] (state) {
      return state.columns
    },
    [types.GET_HRPMC2E_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPMC2E_DETAIL] (state) {
      return state.detail
    },
    [types.GET_HRPMC2E_DROPDOWNS] (state) {
      return state.dropdowns
    },
    [types.GET_HRPMC2E_CHANGE_DETAIL_POPUP] (state) {
      return state.popupChangeDetail
    },
    [types.GET_HRPMC2E_CHECK_BLACKLIST_POPUP] (state) {
      return state.popupCheckBlacklist
    },
    [types.GET_HRPMC2E_SMART_CARD_POPUP] (state) {
      return state.popupSmartCard
    },
    [types.GET_HRPMC2E_OTHER_CREATE_POPUP] (state) {
      return state.popupOtherCreate
    },
    [types.GET_HRPMC2E_ADD_OTHER_POPUP] (state) {
      return state.popupAddOther
    },
    [types.GET_HRPMC2E_TAX8_POPUP] (state) {
      return state.popupTax8
    },
    [types.GET_HRPMC2E_TAX9_POPUP] (state) {
      return state.popupTax9
    },
    [types.GET_HRPMC2E_GUARANTEE1_POPUP] (state) {
      return state.popupGuarantee1
    },
    [types.GET_HRPMC2E_GUARANTEE2_POPUP] (state) {
      return state.popupGuarantee2
    },
    [types.GET_HRPMC2E_GUARANTEE3_POPUP] (state) {
      return state.popupGuarantee3
    },
    [types.GET_HRPMC2E_TAX8_SELECTED] (state) {
      return state.tax8Selected
    },
    [types.GET_HRPMC2E_TAX9_SELECTED] (state) {
      return state.tax9Selected
    },
    [types.GET_HRPMC2E_GUARANTEE1_SELECTED] (state) {
      return state.guarantee1Selected
    },
    [types.GET_HRPMC2E_GUARANTEE2_SELECTED] (state) {
      return state.guarantee2Selected
    },
    [types.GET_HRPMC2E_GUARANTEE3_SELECTED] (state) {
      return state.guarantee3Selected
    },
    [types.GET_HRPMC2E_ADD_OTHER_SELECTED] (state) {
      return state.addOtherSelected
    },
    [types.GET_HRPMC2E_POPUP_EDUCATION] (state) {
      return state.popupEducation
    },
    [types.GET_HRPMC2E_EDUCATION_SELECTED] (state) {
      return state.educationSelected
    },
    [types.GET_HRPMC2E_POPUP_WORK_EXPERIENCES] (state) {
      return state.popupWorkExperiences
    },
    [types.GET_HRPMC2E_WORK_EXPERIENCES_SELECTED] (state) {
      return state.workExperiencesSelected
    },
    [types.GET_HRPMC2E_POPUP_TRAINING_RECORD] (state) {
      return state.popupTrainingRecord
    },
    [types.GET_HRPMC2E_TRAINING_RECORD_SELECTED] (state) {
      return state.trainingRecordSelected
    },
    [types.GET_HRPMC2E_POPUP_CHILDREN] (state) {
      return state.popupChildren
    },
    [types.GET_HRPMC2E_CHILDREN_SELECTED] (state) {
      return state.childrenSelected
    },
    [types.GET_HRPMC2E_POPUP_RELATIVES] (state) {
      return state.popupRelatives
    },
    [types.GET_HRPMC2E_RELATIVES_SELECTED] (state) {
      return state.relativesSelected
    },
    [types.GET_HRPMC2E_POPUP_HONORS] (state) {
      return state.popupHonors
    },
    [types.GET_HRPMC2E_HONORS_SELECTED] (state) {
      return state.honorsSelected
    },
    [types.GET_HRPMC2E_POPUP_CODTENCY] (state) {
      return state.popupCodtency
    },
    [types.GET_HRPMC2E_COMPETENCY_SELECTED] (state) {
      return state.competencySelected
    },
    [types.GET_HRPMC2E_INLINE_COMPETENCY1] (state) {
      return state.inlineCompetency1
    },
    [types.GET_HRPMC2E_INLINE_COMPETENCY2] (state) {
      return state.inlineCompetency2
    },
    [types.GET_HRPMC2E_INLINE_COMPETENCY3] (state) {
      return state.inlineCompetency3
    },
    [types.GET_HRPMC2E_INLINE_WORK_STUDY1] (state) {
      return state.inlineWorkStudy1
    },
    [types.GET_HRPMC2E_INLINE_WORK_STUDY2] (state) {
      return state.inlineWorkStudy2
    },
    [types.GET_HRPMC2E_INLINE_WORK_STUDY3] (state) {
      return state.inlineWorkStudy3
    },
    [types.GET_HRPMC2E_INLINE_FAMILY2] (state) {
      return state.inlineFamily2
    },
    [types.GET_HRPMC2E_INLINE_FAMILY4] (state) {
      return state.inlineFamily4
    },
    [types.GET_HRPMC2E_DETAIL_DISABLED] (state) {
      return state.detailDisabled
    }
  },
  mutations: {
    [types.SET_HRPMC2E_INDEX_DISP] (state, indexDisp) {
      state.indexDisp = indexDisp
    },
    [types.SET_HRPMC2E_DETAIL_DISP] (state, detailDisp) {
      state.detailDisp = detailDisp
    },
    [types.SET_HRPMC2E_CHECK_BLACKLIST_POPUP_DISP] (state, checkBlacklistPopupDisp) {
      state.checkBlacklistPopupDisp = checkBlacklistPopupDisp
    },
    [types.SET_HRPMC2E_SMART_CARD_POPUP_DISP] (state, smartCardPopupDisp) {
      state.smartCardPopupDisp = smartCardPopupDisp
    },
    [types.SET_HRPMC2E_ADD_OTHER_POPUP_DISP] (state, addOtherPopupDisp) {
      state.addOtherPopupDisp = addOtherPopupDisp
    },
    [types.SET_HRPMC2E_COLUMNS] (state, { labels, dropdowns }) {
      for (var keyLang of assetsLabels.allLang) {
        state.columns[keyLang].income = assetsLabels.replaceLabelToColumns(state.columns[keyLang].income, labels[keyLang])
        state.columns[keyLang].taxTab3 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].taxTab3, labels[keyLang])
        state.columns[keyLang].taxTab4 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].taxTab4, labels[keyLang])
        state.columns[keyLang].taxTab5 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].taxTab5, labels[keyLang])
        state.columns[keyLang].spouseTab2 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].spouseTab2, labels[keyLang])
        state.columns[keyLang].spouseTab3 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].spouseTab3, labels[keyLang])
        state.columns[keyLang].spouseTab4 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].spouseTab4, labels[keyLang])
        state.columns[keyLang].changeName = assetsLabels.replaceLabelToColumns(state.columns[keyLang].changeName, labels[keyLang])
        state.columns[keyLang].document = assetsLabels.replaceLabelToColumns(state.columns[keyLang].document, labels[keyLang])
        state.columns[keyLang].changeDetail = assetsLabels.replaceLabelToColumns(state.columns[keyLang].changeDetail, labels[keyLang])
        state.columns[keyLang].trainingRecordColumns = assetsLabels.replaceLabelToColumns(state.columns[keyLang].trainingRecordColumns, labels[keyLang])
        state.columns[keyLang].internalTrainingColumns = assetsLabels.replaceLabelToColumns(state.columns[keyLang].internalTrainingColumns, labels[keyLang])
        state.columns[keyLang].educationColumns = assetsLabels.replaceLabelToColumns(state.columns[keyLang].educationColumns, labels[keyLang])
        state.columns[keyLang].workExperiencesColumns = assetsLabels.replaceLabelToColumns(state.columns[keyLang].workExperiencesColumns, labels[keyLang])
        state.columns[keyLang].childrenColumns = assetsLabels.replaceLabelToColumns(state.columns[keyLang].childrenColumns, labels[keyLang])
        state.columns[keyLang].relativesColumns = assetsLabels.replaceLabelToColumns(state.columns[keyLang].relativesColumns, labels[keyLang])
        state.columns[keyLang].guarntee1 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].guarntee1, labels[keyLang])
        state.columns[keyLang].guarntee2 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].guarntee2, labels[keyLang])
        state.columns[keyLang].guarntee3 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].guarntee3, labels[keyLang])
        state.columns[keyLang].otherCreate = assetsLabels.replaceLabelToColumns(state.columns[keyLang].otherCreate, labels[keyLang])
        state.columns[keyLang].competencyColumns = assetsLabels.replaceLabelToColumns(state.columns[keyLang].competencyColumns, labels[keyLang])
        let dropdownSets = [
          { key: '_listen', value: dropdowns.v_namgrade[keyLang] },
          { key: '_speak', value: dropdowns.v_namgrade[keyLang] },
          { key: '_read', value: dropdowns.v_namgrade[keyLang] },
          { key: '_write', value: dropdowns.v_namgrade[keyLang] }
        ]
        state.columns[keyLang].languageColumns = assetsLabels.replaceLabelToColumns(state.columns[keyLang].languageColumns, labels[keyLang], dropdownSets)
        state.columns[keyLang].honorsColumns = assetsLabels.replaceLabelToColumns(state.columns[keyLang].honorsColumns, labels[keyLang])
        state.columns[keyLang].codtencyColumns = assetsLabels.replaceLabelToColumns(state.columns[keyLang].codtencyColumns, labels[keyLang])
      }
    },
    [types.SET_HRPMC2E_LABELS] (state, labels) {
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPMC2E_DETAIL] (state, detail) {
      state.detail = detail
    },
    [types.SET_HRPMC2E_DROPDOWNS] (state, dropdowns) {
      state.dropdowns = dropdowns
    },
    [types.SET_HRPMC2E_CHANGE_DETAIL_POPUP] (state, popupChangeDetail) {
      state.popupChangeDetail = popupChangeDetail
    },
    [types.SET_HRPMC2E_CHECK_BLACKLIST_POPUP] (state, popupCheckBlacklist) {
      state.popupCheckBlacklist = popupCheckBlacklist
    },
    [types.SET_HRPMC2E_SMART_CARD_POPUP] (state, popupSmartCard) {
      state.popupSmartCard = popupSmartCard
    },
    [types.SET_HRPMC2E_OTHER_CREATE_POPUP] (state, popupOtherCreate) {
      state.popupOtherCreate = popupOtherCreate
    },
    [types.SET_HRPMC2E_ADD_POPUP_OTHER_CREATE] (state, { popupOther, event, addOtherSelected }) {
      if (popupOther.coddatatyp === 'T') {
        popupOther.size = null
      }
      if (popupOther.coddatatyp === 'N') {
        popupOther.character = null
      }
      if (event === 'add') {
        state.popupOtherCreate.table.rows.push(popupOther)
        state.popupOtherCreate.table.total = state.popupOtherCreate.table.rows.length
      } else if (event === 'edit') {
        let rowIndex = state.popupOtherCreate.table.rows.map(popupOtherCreateRow => {
          return popupOtherCreateRow.detail
        }).indexOf(addOtherSelected.detail)
        if (rowIndex > -1) {
          state.popupOtherCreate.table.rows[rowIndex] = popupOther
        }
      }
    },
    [types.SET_HRPMC2E_ADD_OTHER_POPUP] (state, popupAddOther) {
      state.popupAddOther = popupAddOther
    },
    [types.SET_HRPMC2E_TAX8_POPUP] (state, popupTax8) {
      state.popupTax8 = popupTax8
    },
    [types.SET_HRPMC2E_TAX9_POPUP] (state, popupTax9) {
      state.popupTax9 = popupTax9
    },
    [types.SET_HRPMC2E_GUARANTEE1_POPUP] (state, popupGuarantee1) {
      state.popupGuarantee1 = popupGuarantee1
    },
    [types.SET_HRPMC2E_GUARANTEE2_POPUP] (state, popupGuarantee2) {
      state.popupGuarantee2 = popupGuarantee2
    },
    [types.SET_HRPMC2E_GUARANTEE3_POPUP] (state, popupGuarantee3) {
      state.popupGuarantee3 = popupGuarantee3
    },
    [types.SET_HRPMC2E_TAX8_SELECTED] (state, tax8Selected) {
      state.tax8Selected = tax8Selected
    },
    [types.SET_HRPMC2E_TAX9_SELECTED] (state, tax9Selected) {
      state.tax9Selected = tax9Selected
    },
    [types.SET_HRPMC2E_GUARANTEE1_SELECTED] (state, guarantee1Selected) {
      state.guarantee1Selected = guarantee1Selected
    },
    [types.SET_HRPMC2E_GUARANTEE2_SELECTED] (state, guarantee2Selected) {
      state.guarantee2Selected = guarantee2Selected
    },
    [types.SET_HRPMC2E_GUARANTEE3_SELECTED] (state, guarantee3Selected) {
      state.guarantee3Selected = guarantee3Selected
    },
    [types.SET_HRPMC2E_ADD_OTHER_SELECTED] (state, addOtherSelected) {
      state.addOtherSelected = addOtherSelected
    },
    [types.SET_HRPMC2E_POPUP_EDUCATION] (state, popupEducation) {
      state.popupEducation = popupEducation
    },
    [types.SET_HRPMC2E_EDUCATION_SELECTED] (state, educationSelected) {
      state.educationSelected = educationSelected
    },
    [types.SET_HRPMC2E_POPUP_WORK_EXPERIENCES] (state, popupWorkExperiences) {
      state.popupWorkExperiences = popupWorkExperiences
    },
    [types.SET_HRPMC2E_EDUCATION_SELECTED] (state, workExperiencesSelected) {
      state.workExperiencesSelected = workExperiencesSelected
    },
    [types.SET_HRPMC2E_POPUP_TRAINING_RECORD] (state, popupTrainingRecord) {
      state.popupTrainingRecord = popupTrainingRecord
    },
    [types.SET_HRPMC2E_TRAINING_RECORD_SELECTED] (state, trainingRecordSelected) {
      state.trainingRecordSelected = trainingRecordSelected
    },
    [types.SET_HRPMC2E_POPUP_CHILDREN] (state, popupChildren) {
      state.popupChildren = popupChildren
    },
    [types.SET_HRPMC2E_CHILDREN_SELECTED] (state, childrenSelected) {
      state.childrenSelected = childrenSelected
    },
    [types.SET_HRPMC2E_POPUP_RELATIVES] (state, popupRelatives) {
      state.popupRelatives = popupRelatives
    },
    [types.SET_HRPMC2E_RELATIVES_SELECTED] (state, relativesSelected) {
      state.relativesSelected = relativesSelected
    },
    [types.SET_HRPMC2E_CODTENCY_ADD_INLINE] (state, { data, param }) {
      let rowIndex = state.detail.competency.tableCompetency.rows.map(function (dataRow) {
        return dataRow.codtency
      }).indexOf(param.codtency)
      state.detail.competency.tableCompetency.rows[rowIndex].desc_typtency = data.desc_typtency
      state.detail.competency.tableCompetency.rows[rowIndex].codtency = data.codtency
      state.detail.competency.tableCompetency.rows[rowIndex].desc_codtency = data.desc_codtency
      state.detail.competency.tableCompetency.rows[rowIndex].score = data.score
      state.detail.competency.tableCompetency.rows[rowIndex]._targetscore = data._targetscore
      state.detail.competency.tableCompetency.rows[rowIndex]._gapscore = data._gapscore
    },
    [types.SET_HRPMC2E_POPUP_HONORS] (state, popupHonors) {
      state.popupHonors = popupHonors
    },
    [types.SET_HRPMC2E_HONORS_SELECTED] (state, honorsSelected) {
      state.honorsSelected = honorsSelected
    },
    [types.SET_HRPMC2E_POPUP_CODTENCY] (state, popupCodtency) {
      state.popupCodtency = popupCodtency
    },
    [types.SET_HRPMC2E_COMPETENCY_SELECTED] (state, competencySelected) {
      state.competencySelected = competencySelected
    },
    [types.SET_HRPMC2E_INLINE_COMPETENCY1] (state, inlineCompetency1) {
      state.inlineCompetency1 = inlineCompetency1
    },
    [types.SET_HRPMC2E_INLINE_COMPETENCY2] (state, inlineCompetency2) {
      state.inlineCompetency2 = inlineCompetency2
    },
    [types.SET_HRPMC2E_INLINE_COMPETENCY3] (state, inlineCompetency3) {
      state.inlineCompetency3 = inlineCompetency3
    },
    [types.SET_HRPMC2E_HONORS_ADD_INLINE_FROM_POPUP] (state, { flg, data }) {
      if (flg === 'add') {
        let dataRow = Object.assign({}, data)
        dataRow['flgAdd'] = true
        dataRow['rowID'] = state.detail.competency.tableHonors.rows.length + 1
        state.detail.competency.tableHonors.rows.push(dataRow)
        state.detail.competency.tableHonors.total = state.detail.competency.tableHonors.rows.length
      } else if (flg === 'edit') {
        let rowIndex = state.detail.competency.tableHonors.rows.map((dataRow) => {
          return '' + dataRow.typrewd
        }).indexOf('' + data.typrewd)
        if (rowIndex > -1) {
          state.detail.competency.tableHonors.rows[rowIndex] = data
          state.detail.competency.tableHonors.rows[rowIndex]['flgEdit'] = true
        }
      }
    },
    [types.SET_HRPMC2E_INLINE_WORK_STUDY1] (state, inlineWorkStudy1) {
      state.inlineWorkStudy1 = inlineWorkStudy1
    },
    [types.SET_HRPMC2E_INLINE_WORK_STUDY2] (state, inlineWorkStudy2) {
      state.inlineWorkStudy2 = inlineWorkStudy2
    },
    [types.SET_HRPMC2E_INLINE_WORK_STUDY3] (state, inlineWorkStudy3) {
      state.inlineWorkStudy3 = inlineWorkStudy3
    },
    [types.SET_HRPMC2E_EDUCATION_ADD_INLINE_FROM_POPUP] (state, popupEducationState) {
      let dataRow = Object.assign({}, popupEducationState)
      dataRow['flgAdd'] = true
      dataRow['rowID'] = state.detail.workStudy.tableEducation.rows.length + 1
      state.detail.workStudy.tableEducation.rows.push(dataRow)
      state.detail.workStudy.tableEducation.total = state.detail.workStudy.tableEducation.rows.length
    },
    [types.SET_HRPMC2E_WORK_EXPERIENCES_ADD_INLINE_FROM_POPUP] (state, popupWorkExperiencesState) {
      let dataRow = Object.assign({}, popupWorkExperiencesState)
      dataRow['flgAdd'] = true
      dataRow['rowID'] = state.detail.workStudy.tableWorkExperiences.rows.length + 1
      state.detail.workStudy.tableWorkExperiences.rows.push(dataRow)
      state.detail.workStudy.tableWorkExperiences.total = state.detail.workStudy.tableWorkExperiences.rows.length
    },
    [types.SET_HRPMC2E_TRAINING_RECORD_ADD_INLINE_FROM_POPUP] (state, popupTrainingRecordState) {
      let dataRow = Object.assign({}, popupTrainingRecordState)
      dataRow['flgAdd'] = true
      dataRow['rowID'] = state.detail.workStudy.tableTrainingRecord.rows.length + 1
      state.detail.workStudy.tableTrainingRecord.rows.push(dataRow)
      state.detail.workStudy.tableTrainingRecord.total = state.detail.workStudy.tableTrainingRecord.rows.length
    },
    [types.SET_HRPMC2E_INLINE_FAMILY2] (state, inlineFamily2) {
      state.inlineFamily2 = inlineFamily2
    },
    [types.SET_HRPMC2E_INLINE_FAMILY4] (state, inlineFamily4) {
      state.inlineFamily4 = inlineFamily4
    },
    [types.SET_HRPMC2E_CHILDREN_ADD_INLINE_FROM_POPUP] (state, popupChildrenState) {
      let dataRow = Object.assign({}, popupChildrenState)
      dataRow['flgAdd'] = true
      dataRow['rowID'] = state.detail.family.tableChildren.rows.length + 1
      state.detail.family.tableChildren.rows.push(dataRow)
      state.detail.family.tableChildren.total = state.detail.family.tableChildren.rows.length
    },
    [types.SET_HRPMC2E_RELATIVES_ADD_INLINE_FROM_POPUP] (state, popupRelativesState) {
      let dataRow = Object.assign({}, popupRelativesState)
      dataRow['flgAdd'] = true
      dataRow['rowID'] = state.detail.family.tableRelatives.rows.length + 1
      state.detail.family.tableRelatives.rows.push(dataRow)
      state.detail.family.tableRelatives.total = state.detail.family.tableRelatives.rows.length
    },
    [types.SET_HRPMC2E_SPOUSE] (state, spouse) {
      state.detail.family.spouse = spouse
    },
    [types.SET_HRPMC2E_PARENT_FATHER] (state, father) {
      state.detail.family.parent.father = father
    },
    [types.SET_HRPMC2E_PARENT_MOTHER] (state, mother) {
      state.detail.family.parent.mother = mother
    },
    [types.SET_HRPMC2E_DETAIL_DISABLED] (state, detailDisabled) {
      state.detailDisabled = detailDisabled
    }
  },
  actions: {
    [types.TOGGLE_HRPMC2E_PAGE] ({ commit }, page) {
      let indexDisp = false
      let detailDisp = false
      let checkBlacklistPopupDisp = false
      let smartCardPopupDisp = false
      let addOtherPopupDisp = false
      switch (page) {
        case 'index':
          indexDisp = true
          break
        case 'detail':
          detailDisp = true
          break
        case 'checkBlacklistPopup':
          checkBlacklistPopupDisp = true
          break
        case 'smartCardPopup':
          smartCardPopupDisp = true
          break
        case 'AddOtherPopup':
          addOtherPopupDisp = true
          break
      }
      commit(types.SET_HRPMC2E_INDEX_DISP, indexDisp)
      commit(types.SET_HRPMC2E_DETAIL_DISP, detailDisp)
      commit(types.SET_HRPMC2E_CHECK_BLACKLIST_POPUP_DISP, checkBlacklistPopupDisp)
      commit(types.SET_HRPMC2E_SMART_CARD_POPUP_DISP, smartCardPopupDisp)
      commit(types.SET_HRPMC2E_ADD_OTHER_POPUP_DISP, addOtherPopupDisp)
    },
    // [types.RECEIVED_HRPMC2E_COLUMNS_LABELS] ({ commit }) {
    //   hrpmc2e.getLabels()
    //     .then((response) => {
    //       const data = JSON.parse(response.request.response)
    //       commit(types.SET_HRPMC2E_COLUMNS, data.labels)
    //       commit(types.SET_HRPMC2E_LABELS, data.labels)
    //     })
    //     .catch((error) => {
    //       let message = error
    //       if (error.response) {
    //         const data = error.response.data
    //         message = data.response
    //       }
    //       swal({title: '', text: message, html: true, type: 'error'})
    //     })
    // },
    [types.RECEIVED_HRPMC2E_COLUMNS_LABELS] ({ state, commit }) {
      axios.all([
        hrpmc2e.getLabels(),
        hrpmc2e.getDropdowns()
      ])
      .then(axios.spread(function (responseColumnsLabels, responseDropdowns) {
        const dataDropdowns = JSON.parse(responseDropdowns.request.response)
        commit(types.SET_HRPMC2E_DROPDOWNS, dataDropdowns)

        const dataColumnsLabels = JSON.parse(responseColumnsLabels.request.response)
        commit(types.SET_HRPMC2E_COLUMNS, { labels: dataColumnsLabels.labels, dropdowns: dataDropdowns })
        commit(types.SET_HRPMC2E_LABELS, dataColumnsLabels.labels)
      }))
      .catch((error) => {
        const data = error.response.data
        swal({title: '', text: data.response, html: true, type: 'error'})
      })
    },
    [types.RECEIVED_HRPMC2E_INDEX] ({ commit }, searchParams) {
      hrpmc2e.getIndex(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_INDEX, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_DETAIL] ({ commit }, searchParams) {
      hrpmc2e.getDetail(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_DETAIL, data)
          commit(types.SET_HRPMC2E_DETAIL_DISABLED, false)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_DETAIL_CREATE] ({ commit }, searchParams) {
      hrpmc2e.getDetailCreate(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_DETAIL, data)
          commit(types.SET_HRPMC2E_DETAIL_DISABLED, true)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_DROPDOWNS] ({ commit }) {
      hrpmc2e.getDropdowns()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_DROPDOWNS, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_CHANGE_DETAIL_POPUP] ({ commit }, popupParams) {
      hrpmc2e.getChangeDetailPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_CHANGE_DETAIL_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_CHECK_BLACKLIST_POPUP] ({ commit }, popupParams) {
      hrpmc2e.getCheckblacklistPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_CHECK_BLACKLIST_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_SMART_CARD_POPUP] ({ commit }, popupParams) {
      hrpmc2e.getSmartCardPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_SMART_CARD_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_OTHER_CREATE_POPUP] ({ commit }, popupParams) {
      hrpmc2e.getOtherCreatePopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_OTHER_CREATE_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_TAX8_POPUP_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getTax8PopupCreate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_TAX8_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_TAX8_POPUP] ({ commit }, popupParams) {
      hrpmc2e.getTax8Popup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_TAX8_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_TAX9_POPUP_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getTax9PopupCreate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_TAX9_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_TAX9_POPUP] ({ commit }, popupParams) {
      hrpmc2e.getTax9Popup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_TAX9_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_GUARANTEE1_POPUP_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getGuarantee1PopupCreate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_GUARANTEE1_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_GUARANTEE1_POPUP] ({ commit }, popupParams) {
      hrpmc2e.getGuarantee1Popup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_GUARANTEE1_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_GUARANTEE2_POPUP_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getGuarantee2PopupCreate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_GUARANTEE2_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_GUARANTEE2_POPUP] ({ commit }, popupParams) {
      hrpmc2e.getGuarantee2Popup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_GUARANTEE2_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_GUARANTEE3_POPUP_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getGuarantee3PopupCreate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_GUARANTEE3_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_GUARANTEE3_POPUP] ({ commit }, popupParams) {
      hrpmc2e.getGuarantee3Popup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_GUARANTEE3_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_ADD_OTHER_POPUP_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getAddOtherPopupCreate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_ADD_OTHER_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_ADD_OTHER_POPUP] ({ commit }, popupParams) {
      hrpmc2e.getAddOtherPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_ADD_OTHER_POPUP, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.SAVE_HRPMC2E_PERSONALTAX] ({ commit, dispatch }, { detail, searchParams }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.savePersonalTax({detail, searchParams})
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_DETAIL, {searchParams})
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_GUARANTEE] ({ commit, dispatch }, { detail, searchParams }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.saveGuarantee({ detail, searchParams })
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_DETAIL, {searchParams})
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_OTHER] ({ commit, dispatch }, { detail, searchParams }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.saveOther({ detail, searchParams })
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_DETAIL, {searchParams})
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_TAX8_POPUP] ({ commit, dispatch }, { saveParam, searchParams }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.saveTax8Popup({ saveParam, searchParams })
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_TAX8_POPUP, {searchParams})
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_TAX9_POPUP] ({ commit, dispatch }, { saveParam, searchParams }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.saveTax9Popup({ saveParam, searchParams })
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_TAX9_POPUP, {searchParams})
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_GUARANTEE1] ({ commit, dispatch }, dataRowsHasFlg) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.saveGuarantee1(dataRowsHasFlg)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_DETAIL)
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_GUARANTEE2] ({ commit, dispatch }, dataRowsHasFlg) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.saveGuarantee2(dataRowsHasFlg)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_DETAIL)
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_GUARANTEE3] ({ commit, dispatch }, dataRowsHasFlg) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.saveGuarantee3(dataRowsHasFlg)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_DETAIL)
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_PERSONAL_TAX8] ({ commit, dispatch }, dataRowsHasFlg) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.savePersonalTax8(dataRowsHasFlg)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_DETAIL)
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_PERSONAL_TAX9] ({ commit, dispatch }, dataRowsHasFlg) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.savePersonalTax9(dataRowsHasFlg)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_DETAIL)
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_GUARANTEE1_POPUP] ({ commit, dispatch }, {detail, searchParams}) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.saveGuarantee1Popup({detail, searchParams})
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_GUARANTEE1_POPUP, {searchParams})
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_GUARANTEE2_POPUP] ({ commit, dispatch }, {detail, searchParams}) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.saveGuarantee2Popup({detail, searchParams})
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_GUARANTEE2_POPUP, {searchParams})
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_GUARANTEE3_POPUP] ({ commit, dispatch }, {detail, searchParams}) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.saveGuarantee3Popup({detail, searchParams})
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_GUARANTEE3_POPUP, {searchParams})
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.SAVE_HRPMC2E_OTHER_CREATE_POPUP] ({ commit, dispatch }, popupOtherCreate) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.saveOtherCreatePopup(popupOtherCreate)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_OTHER_CREATE_POPUP)
              swal({title: '', text: data.response, html: true, type: 'success'}, function () {
              })
            })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
        }
      })
    },
    [types.RECEIVED_HRPMC2E_EDUCATION_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getEducationPopupCrate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_EDUCATION, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_EDUCATION] ({ commit }, popupParams) {
      hrpmc2e.getEducationPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_EDUCATION, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_WORK_EXPERIENCES_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getWorkExperiencesPopupCrate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_WORK_EXPERIENCES, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_WORK_EXPERIENCES] ({ commit }, popupParams) {
      hrpmc2e.getWorkExperiencesPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_WORK_EXPERIENCES, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_TRAINING_RECORD_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getTrainingRecordPopupCrate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_TRAINING_RECORD, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_TRAINING_RECORD] ({ commit }, popupParams) {
      hrpmc2e.getTrainingRecordPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_TRAINING_RECORD, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_CHILDREN_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getChildrenPopupCrate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_CHILDREN, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_CHILDREN] ({ commit }, popupParams) {
      hrpmc2e.getChildrenPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_CHILDREN, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_RELATIVES_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getRelativesPopupCrate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_RELATIVES, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_RELATIVES] ({ commit }, popupParams) {
      hrpmc2e.getRelativesPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_RELATIVES, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_CODTENCY_ADD_INLINE] ({ commit }, param) {
      hrpmc2e.getHrpmc2eCodtencyAddInline(param)
        .then((response) => {
          const codtencyData = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_CODTENCY_ADD_INLINE, { data: codtencyData, param: param })
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_HONORS_CREATE] ({ commit }, popupParams) {
      hrpmc2e.getHonorsPopupCrate(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_HONORS, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_HONORS] ({ commit }, popupParams) {
      hrpmc2e.getHonorsPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_HONORS, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.POST_HRPMC2E_HONORS_POPUP_SAVE] ({ commit, dispatch }, { saveParam, searchParams }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmc2e.postHrpmc2eHonorsPopup({ saveParam, searchParams })
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMC2E_DETAIL, searchParams)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              let message = error
              if (error.response) {
                const data = error.response.data
                message = data.response
              }
              swal({title: '', text: message, html: true, type: 'error'})
            })
        }
      })
    },
    [types.RECEIVED_HRPMC2E_CODTENCY_POPUP] ({ commit }, popupParams) {
      hrpmc2e.getCodtencyPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_CODTENCY, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.POST_HRPMC2E_COMPETENCY_SAVE_ALL] ({ commit, dispatch }, { competency, searchParams }) {
      hrpmc2e.postCompetencySaveAll({ competency, searchParams })
        .then((response) => {
          const data = JSON.parse(response.request.response)
          dispatch(types.RECEIVED_HRPMC2E_DETAIL, searchParams)
          swal({title: '', text: data.response, html: true, type: 'success'})
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.POST_HRPMC2E_WORK_STUDY_SAVE_ALL] ({ commit, dispatch }, { workStudy, searchParams }) {
      hrpmc2e.postWorkStudySaveAll({ workStudy, searchParams })
        .then((response) => {
          const data = JSON.parse(response.request.response)
          dispatch(types.RECEIVED_HRPMC2E_DETAIL, searchParams)
          swal({title: '', text: data.response, html: true, type: 'success'})
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.POST_HRPMC2E_FAMILY_SAVE_ALL] ({ commit, dispatch }, { family, searchParams }) {
      hrpmc2e.postFamilySaveAll({ family, searchParams })
        .then((response) => {
          const data = JSON.parse(response.request.response)
          dispatch(types.RECEIVED_HRPMC2E_DETAIL, searchParams)
          swal({title: '', text: data.response, html: true, type: 'success'})
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_SPOUSE_QUERY] ({ commit }, paramSearch) {
      hrpmc2e.getDetailSpouse(paramSearch)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_SPOUSE, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_POPUP_CHILDREN_QUERY] ({ commit }, paramSearch) {
      hrpmc2e.getDetailChildrenPopupQuery(paramSearch)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_CHILDREN, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_PARENT_FATHER_QUERY] ({ commit }, paramSearch) {
      hrpmc2e.getDetailParentFather(paramSearch)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_PARENT_FATHER, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_PARENT_MOTHER_QUERY] ({ commit }, paramSearch) {
      hrpmc2e.getDetailParentMother(paramSearch)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_PARENT_MOTHER, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMC2E_POPUP_RELATIVES_QUERY] ({ commit }, paramSearch) {
      hrpmc2e.getDetailRelativesPopupQuery(paramSearch)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMC2E_POPUP_RELATIVES, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    }
  }
}
