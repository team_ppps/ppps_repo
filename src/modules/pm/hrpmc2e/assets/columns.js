export let hrpmc2eColumns = () => {
  let incomeColumns = [{
    key: 'codincom',
    name: '',
    labelCodapp: 'HRPMC2EA5',
    labelIndex: '20',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codincom',
    name: '',
    labelCodapp: 'HRPMC2EA5',
    labelIndex: '30',
    style: 'min-width: 200px;'
  }, {
    key: 'calunit',
    name: '',
    labelCodapp: 'HRPMC2EA5',
    labelIndex: '40',
    style: 'min-width: 100px;'
  }, {
    key: 'amtincom1',
    name: '',
    labelCodapp: 'HRPMC2EA5',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }]

  let taxTab3Columns = [{
    key: 'coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA6',
    labelIndex: '250',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA6',
    labelIndex: '260',
    style: 'min-width: 200px;'
  }, {
    key: 'amtdeduct',
    name: '',
    labelCodapp: 'HRPMC2EA6',
    labelIndex: '270',
    style: 'min-width: 100px;'
  }]

  let taxTab4Columns = [{
    key: 'coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA6',
    labelIndex: '250',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA6',
    labelIndex: '260',
    style: 'min-width: 200px;'
  }, {
    key: 'amtdeduct',
    name: '',
    labelCodapp: 'HRPMC2EA6',
    labelIndex: '270',
    style: 'min-width: 100px;'
  }]

  let taxTab5Columns = [{
    key: 'coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA6',
    labelIndex: '250',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA6',
    labelIndex: '260',
    style: 'min-width: 200px;'
  }, {
    key: 'amtdeduct',
    name: '',
    labelCodapp: 'HRPMC2EA6',
    labelIndex: '270',
    style: 'min-width: 100px;'
  }]

  let spouseTab2Columns = [{
    key: 'coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA7',
    labelIndex: '90',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA7',
    labelIndex: '100',
    style: 'min-width: 200px;'
  }, {
    key: 'amtdeduct',
    name: '',
    labelCodapp: 'HRPMC2EA7',
    labelIndex: '110',
    style: 'min-width: 100px;'
  }]

  let spouseTab3Columns = [{
    key: 'coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA7',
    labelIndex: '90',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA7',
    labelIndex: '100',
    style: 'min-width: 200px;'
  }, {
    key: 'amtdeduct',
    name: '',
    labelCodapp: 'HRPMC2EA7',
    labelIndex: '110',
    style: 'min-width: 100px;'
  }]

  let spouseTab4Columns = [{
    key: 'coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA7',
    labelIndex: '90',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_coddeduct',
    name: '',
    labelCodapp: 'HRPMC2EA7',
    labelIndex: '100',
    style: 'min-width: 200px;'
  }, {
    key: 'amtdeduct',
    name: '',
    labelCodapp: 'HRPMC2EA7',
    labelIndex: '110',
    style: 'min-width: 100px;'
  }]

  let changeNameColumns = [{
    key: 'dtechg',
    name: '',
    labelCodapp: 'HRPMC2EA8',
    labelIndex: '10',
    style: 'min-width: 100px;'
  }, {
    key: 'namtitle',
    name: '',
    labelCodapp: 'HRPMC2EA8',
    labelIndex: '20',
    style: 'min-width: 100px;'
  }, {
    key: 'namfirst',
    name: '',
    labelCodapp: 'HRPMC2EA8',
    labelIndex: '30',
    style: 'min-width: 200px;'
  }, {
    key: 'namlast',
    name: '',
    labelCodapp: 'HRPMC2EA8',
    labelIndex: '40',
    style: 'min-width: 200px;'
  }, {
    key: 'deschang',
    name: '',
    labelCodapp: 'HRPMC2EA8',
    labelIndex: '50',
    style: 'min-width: 200px;'
  }, {
    key: 'dteupdate',
    name: '',
    labelCodapp: 'HRPMC2EA8',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }, {
    key: 'updateby',
    name: '',
    labelCodapp: 'HRPMC2EA8',
    labelIndex: '70',
    style: 'min-width: 100px;'
  }]

  let documentColumns = [{
    key: 'numseq',
    name: '',
    labelCodapp: 'HRPMC2EA9',
    labelIndex: '10',
    style: 'min-width: 80px;'
  }, {
    key: 'namtypdoc',
    name: '',
    labelCodapp: 'HRPMC2EA9',
    labelIndex: '20',
    style: 'min-width: 100px;'
  }, {
    key: 'namdoc',
    name: '',
    labelCodapp: 'HRPMC2EA9',
    labelIndex: '30',
    style: 'min-width: 100px;'
  }, {
    key: 'dterecv',
    name: '',
    labelCodapp: 'HRPMC2EA9',
    labelIndex: '40',
    class: 'align-center',
    style: 'min-width: 200px;'
  }, {
    key: 'dtedocen',
    name: '',
    labelCodapp: 'HRPMC2EA9',
    labelIndex: '50',
    class: 'align-center',
    style: 'min-width: 200px;'
  }, {
    key: 'numdoc',
    name: '',
    labelCodapp: 'HRPMC2EA9',
    labelIndex: '60',
    class: 'align-center',
    style: 'min-width: 200px;'
  }, {
    key: 'filedoc',
    name: '',
    labelCodapp: 'HRPMC2EA9',
    labelIndex: '70',
    style: 'min-width: 100px;'
  }, {
    key: 'desnote',
    name: '',
    labelCodapp: 'HRPMC2EA9',
    labelIndex: '80',
    style: 'min-width: 100px;'
  }, {
    key: 'dteupdate',
    name: '',
    labelCodapp: 'HRPMC2EA9',
    labelIndex: '90',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'updateby',
    name: '',
    labelCodapp: 'HRPMC2EA9',
    labelIndex: '100',
    style: 'min-width: 100px;'
  }]

  let changeDetailColumns = [{
    key: 'dteedit',
    name: '',
    labelCodapp: 'HRPMC2EA',
    labelIndex: '30',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'data1',
    name: '',
    labelCodapp: 'HRPMC2EA',
    labelIndex: '40',
    style: 'min-width: 200px;'
  }, {
    key: 'desold',
    name: '',
    labelCodapp: 'HRPMC2EA',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: 'desnew',
    name: '',
    labelCodapp: 'HRPMC2EA',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }, {
    key: 'coduser',
    name: '',
    labelCodapp: 'HRPMC2EA',
    labelIndex: '70',
    style: 'min-width: 100px;'
  }]

  let educationColumns = [{
    key: 'desedlv',
    name: '',
    labelCodapp: 'HRPMC2EB1',
    labelIndex: '20',
    style: 'min-width: 150px;'
  }, {
    key: 'desdglv',
    name: '',
    labelCodapp: 'HRPMC2EB1',
    labelIndex: '30',
    style: 'min-width: 200px;'
  }, {
    key: 'desmajsb',
    name: '',
    labelCodapp: 'HRPMC2EB1',
    labelIndex: '40',
    style: 'min-width: 150px;'
  }, {
    key: 'desinst',
    name: '',
    labelCodapp: 'HRPMC2EB1',
    labelIndex: '50',
    style: 'min-width: 150px;'
  }, {
    key: 'descount',
    name: '',
    labelCodapp: 'HRPMC2EB1',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }]

  let workExperiencesColumns = [{
    key: 'desnoffi',
    name: '',
    labelCodapp: 'HRPMC2EB2',
    labelIndex: '20',
    style: 'min-width: 200px;'
  }, {
    key: 'desoffi1',
    name: '',
    labelCodapp: 'HRPMC2EB2',
    labelIndex: '30',
    style: 'min-width: 200px;'
  }, {
    key: 'deslstpos',
    name: '',
    labelCodapp: 'HRPMC2EB2',
    labelIndex: '40',
    style: 'min-width: 200px;'
  }, {
    key: 'dtestart',
    name: '',
    labelCodapp: 'HRPMC2EB2',
    labelIndex: '50',
    style: 'min-width: 130px;'
  }, {
    key: 'dteend',
    name: '',
    labelCodapp: 'HRPMC2EB2',
    labelIndex: '60',
    style: 'min-width: 130px;'
  }, {
    key: 'dteupd',
    name: '',
    labelCodapp: 'HRPMC2EB2',
    labelIndex: '70',
    style: 'min-width: 120px;'
  }, {
    key: 'coduser',
    name: '',
    labelCodapp: 'HRPMC2EB2',
    labelIndex: '80',
    style: 'min-width: 150px;'
  }]
  let trainingRecordColumns = [{
    key: 'destrain',
    name: '',
    labelCodapp: 'HRPMC2EB3',
    labelIndex: '20',
    style: 'min-width: 200px;'
  }, {
    key: 'dtetr',
    name: '',
    labelCodapp: 'HRPMC2EB3',
    labelIndex: '30',
    style: 'min-width: 200px;'
  }, {
    key: 'desplace',
    name: '',
    labelCodapp: 'HRPMC2EB3',
    labelIndex: '40',
    style: 'min-width: 200px;'
  }, {
    key: 'desinstu',
    name: '',
    labelCodapp: 'HRPMC2EB3',
    labelIndex: '50',
    style: 'min-width: 200px;'
  }]
  let internalTrainingColumns = [{
    key: 'codcours',
    name: '',
    labelCodapp: 'HRPMC2EB4',
    labelIndex: '20',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codcours',
    name: '',
    labelCodapp: 'HRPMC2EB4',
    labelIndex: '30',
    style: 'min-width: 200px;'
  }, {
    key: 'codtparg',
    name: '',
    labelCodapp: 'HRPMC2EB4',
    labelIndex: '40',
    style: 'min-width: 100px;'
  }, {
    key: 'dtestart',
    name: '',
    labelCodapp: 'HRPMC2EB4',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: 'dteend',
    name: '',
    labelCodapp: 'HRPMC2EB4',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }, {
    key: 'amttrexp',
    name: '',
    labelCodapp: 'HRPMC2EB4',
    labelIndex: '70',
    style: 'min-width: 100px;'
  }, {
    key: 'v_qtyminhr',
    name: '',
    labelCodapp: 'HRPMC2EB4',
    labelIndex: '80',
    style: 'min-width: 100px;'
  }]
  let childrenColumns = [{
    key: 'namchild',
    name: '',
    labelCodapp: 'HRPMC2EC2',
    labelIndex: '20',
    style: 'min-width: 200px;'
  }, {
    key: 'numoffid',
    name: '',
    labelCodapp: 'HRPMC2EC2',
    labelIndex: '30',
    style: 'min-width: 200px;'
  }, {
    key: 'dtechbd',
    name: '',
    labelCodapp: 'HRPMC2EC2',
    labelIndex: '40',
    style: 'min-width: 120px;'
  }, {
    key: 'dessex',
    name: '',
    labelCodapp: 'HRPMC2EC2',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: 'desflgedlv',
    name: '',
    labelCodapp: 'HRPMC2EC2',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }, {
    key: 'desflgded',
    name: '',
    labelCodapp: 'HRPMC2EC2',
    labelIndex: '70',
    style: 'min-width: 100px;'
  }]
  let relativesColumns = [{
    key: 'namcont',
    name: '',
    labelCodapp: 'HRPMC2EC4',
    labelIndex: '20',
    style: 'min-width: 200px;'
  }, {
    key: 'numoffid',
    name: '',
    labelCodapp: 'HRPMC2EC4',
    labelIndex: '30',
    style: 'min-width: 200px;'
  }, {
    key: 'dtechbd',
    name: '',
    labelCodapp: 'HRPMC2EC4',
    labelIndex: '40',
    style: 'min-width: 120px;'
  }, {
    key: 'dessex',
    name: '',
    labelCodapp: 'HRPMC2EC4',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: 'desrelat',
    name: '',
    labelCodapp: 'HRPMC2EC4',
    labelIndex: '60',
    style: 'min-width: 150px;'
  }]

  let guarntee1Columns = [{
    key: 'v_namguar',
    name: '',
    labelCodapp: 'HRPMC2ED1',
    labelIndex: '20',
    style: 'min-width: 200px;'
  }, {
    key: 'dtegucon',
    name: '',
    labelCodapp: 'HRPMC2ED1',
    labelIndex: '30',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'dteguexp',
    name: '',
    labelCodapp: 'HRPMC2ED1',
    labelIndex: '40',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'desrelat',
    name: '',
    labelCodapp: 'HRPMC2ED1',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: 'desoccup',
    name: '',
    labelCodapp: 'HRPMC2ED1',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }]

  let guarntee2Columns = [{
    key: 'numcolla',
    name: '',
    labelCodapp: 'HRPMC2ED2',
    labelIndex: '20',
    style: 'min-width: 100px;'
  }, {
    key: 'descolla',
    name: '',
    labelCodapp: 'HRPMC2ED2',
    labelIndex: '30',
    class: 'align-center',
    style: 'min-width: 200px;'
  }, {
    key: 'amtcolla',
    name: '',
    labelCodapp: 'HRPMC2ED2',
    labelIndex: '40',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'numdocum',
    name: '',
    labelCodapp: 'HRPMC2ED2',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }]

  let guarntee3Columns = [{
    key: 'desc_codnam',
    name: '',
    labelCodapp: 'HRPMC2ED3',
    labelIndex: '20',
    style: 'min-width: 200px;'
  }, {
    key: 'relation',
    name: '',
    labelCodapp: 'HRPMC2ED3',
    labelIndex: '30',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codpos',
    name: '',
    labelCodapp: 'HRPMC2ED3',
    labelIndex: '40',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codcomp',
    name: '',
    labelCodapp: 'HRPMC2ED3',
    labelIndex: '41',
    style: 'min-width: 100px;'
  }, {
    key: 'dteupdate',
    name: '',
    labelCodapp: 'HRPMC2ED3',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: 'updateby',
    name: '',
    labelCodapp: 'HRPMC2ED3',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }]

  let otherCreateColumns = [{
    key: 'detail',
    name: '',
    labelCodapp: 'HRPMC2EF',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: 'datatyp',
    name: '',
    labelCodapp: 'HRPMC2EF',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }, {
    key: 'size',
    name: '',
    labelCodapp: 'HRPMC2EF',
    labelIndex: '70',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'character',
    name: '',
    labelCodapp: 'HRPMC2EF',
    labelIndex: '80',
    class: 'align-center',
    style: 'min-width: 100px;'
  }]
  let competencyColumns = [{
    key: 'desc_typtency',
    name: '',
    labelCodapp: 'HRPMC2EE1',
    labelIndex: '20',
    style: 'min-width: 200px;'
  }, {
    key: 'codtency',
    name: '',
    labelCodapp: 'HRPMC2EE1',
    labelIndex: '30',
    style: 'min-width: 200px;',
    inlineType: 'lov',
    lovType: 'codtency',
    eventName: 'codtency'
  }, {
    key: 'desc_codtency',
    name: '',
    labelCodapp: 'HRPMC2EE1',
    labelIndex: '40',
    style: 'min-width: 200px;',
    eventName: 'desc_codtency'
  }, {
    key: 'score',
    name: '',
    labelCodapp: 'HRPMC2EE1',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }, {
    key: '_targetscore',
    name: '',
    labelCodapp: 'HRPMC2EE1',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }, {
    key: '_gapscore',
    name: '',
    labelCodapp: 'HRPMC2EE1',
    labelIndex: '70',
    style: 'min-width: 100px;'
  }]
  let languageColumns = [{
    key: '_desc_typtency',
    name: '',
    labelCodapp: 'HRPMC2EE2',
    labelIndex: '10',
    style: 'min-width: 200px;'
  }, {
    key: '_listen',
    name: '',
    labelCodapp: 'HRPMC2EE2',
    labelIndex: '20',
    style: 'min-width: 100px;',
    inlineType: 'select',
    inlineDropdown: {}
  }, {
    key: '_speak',
    name: '',
    labelCodapp: 'HRPMC2EE2',
    labelIndex: '30',
    style: 'min-width: 100px;',
    inlineType: 'select',
    inlineDropdown: {}
  }, {
    key: '_read',
    name: '',
    labelCodapp: 'HRPMC2EE2',
    labelIndex: '40',
    style: 'min-width: 100px;',
    inlineType: 'select',
    inlineDropdown: {}
  }, {
    key: '_write',
    name: '',
    labelCodapp: 'HRPMC2EE2',
    labelIndex: '50',
    style: 'min-width: 100px;',
    inlineType: 'select',
    inlineDropdown: {}
  }]
  let honorsColumns = [{
    key: 'dteinput',
    name: '',
    labelCodapp: 'HRPMC2EE3',
    labelIndex: '20',
    style: 'min-width: 120px;'
  }, {
    key: 'desrewd',
    name: '',
    labelCodapp: 'HRPMC2EE3',
    labelIndex: '30',
    style: 'min-width: 150px;'
  }, {
    key: 'desrewd1',
    name: '',
    labelCodapp: 'HRPMC2EE3',
    labelIndex: '40',
    style: 'min-width: 150px;'
  }, {
    key: 'numhmref',
    name: '',
    labelCodapp: 'HRPMC2EE3',
    labelIndex: '50',
    style: 'min-width: 150px;'
  }, {
    key: 'dteupd',
    name: '',
    labelCodapp: 'HRPMC2EE3',
    labelIndex: '60',
    style: 'min-width: 120px;'
  }, {
    key: 'coduser',
    name: '',
    labelCodapp: 'HRPMC2EE3',
    labelIndex: '70',
    style: 'min-width: 150px;'
  }]
  let codtencyColumns = [{
    key: 'grade',
    name: '',
    labelCodapp: 'HRPMC2EE1',
    labelIndex: '140',
    style: 'min-width: 100px;'
  }, {
    key: 'v_numgrade',
    name: '',
    labelCodapp: 'HRPMC2EE1',
    labelIndex: '150',
    style: 'min-width: 200px;'
  }, {
    key: 'score',
    name: '',
    labelCodapp: 'HRPMC2EE1',
    labelIndex: '50',
    style: 'min-width: 100px;'
  }]

  return {
    en: {
      income: incomeColumns,
      taxTab3: taxTab3Columns,
      taxTab4: taxTab4Columns,
      taxTab5: taxTab5Columns,
      spouseTab2: spouseTab2Columns,
      spouseTab3: spouseTab3Columns,
      spouseTab4: spouseTab4Columns,
      changeName: changeNameColumns,
      document: documentColumns,
      changeDetail: changeDetailColumns,
      educationColumns: educationColumns,
      workExperiencesColumns: workExperiencesColumns,
      trainingRecordColumns: trainingRecordColumns,
      internalTrainingColumns: internalTrainingColumns,
      childrenColumns: childrenColumns,
      relativesColumns: relativesColumns,
      guarntee1: guarntee1Columns,
      guarntee2: guarntee2Columns,
      guarntee3: guarntee3Columns,
      otherCreate: otherCreateColumns,
      competencyColumns: competencyColumns,
      languageColumns: languageColumns,
      honorsColumns: honorsColumns,
      codtencyColumns: codtencyColumns
    },
    th: {
      income: incomeColumns,
      taxTab3: taxTab3Columns,
      taxTab4: taxTab4Columns,
      taxTab5: taxTab5Columns,
      spouseTab2: spouseTab2Columns,
      spouseTab3: spouseTab3Columns,
      spouseTab4: spouseTab4Columns,
      changeName: changeNameColumns,
      document: documentColumns,
      changeDetail: changeDetailColumns,
      educationColumns: educationColumns,
      workExperiencesColumns: workExperiencesColumns,
      trainingRecordColumns: trainingRecordColumns,
      internalTrainingColumns: internalTrainingColumns,
      childrenColumns: childrenColumns,
      relativesColumns: relativesColumns,
      guarntee1: guarntee1Columns,
      guarntee2: guarntee2Columns,
      guarntee3: guarntee3Columns,
      otherCreate: otherCreateColumns,
      competencyColumns: competencyColumns,
      languageColumns: languageColumns,
      honorsColumns: honorsColumns,
      codtencyColumns: codtencyColumns
    }
  }
}
