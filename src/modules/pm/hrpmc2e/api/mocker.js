import hrpmc2eLabels from '../assets/labels'
import faker from 'faker'
import assetslabels from 'assets/js/labels'
import moment from 'moment'

export const mocker = {
  labels () {
    return {
      labels: hrpmc2eLabels
    }
  },
  index () {
    let mockTab1 = {
      codempid: ''
    }

    return mockTab1
  },
  create () {
    let mockPersonal = {
      codempid: '',
      image: '',
      signature: '',
      codtitle: '',
      namfirst: '',
      namlast: '',
      namnick: '',
      tel: '',
      tel1: '',
      idline: '',
      numoffid: '',
      dteoffid: '',
      adrissue: '',
      codprovi: '',
      hospss: '',
      numpasid: '',
      dtepasid: '',
      visaid: '',
      dtevisa: '',
      numlicid: '',
      dtelicid: '',
      dteempdb: '',
      coddomcl: '',
      bdyear: '',
      bdmonth: '',
      codsex: '',
      weight: '',
      high: '',
      codblood: '',
      codorgin: '',
      codnatnl: '',
      codrelgn: '',
      stamarry: '',
      stamilit: '',
      workid: '',
      dteworkid: '',
      dtework: '',
      refid: '',
      dteratir: ''
    }
    let mockAddress = {
      adrreg: '',
      codprovr: '',
      coddistr: '',
      codsubdistr: '',
      codcntyr: '',
      codpostr: '',
      adrcont: '',
      codprovc: '',
      coddistc: '',
      codsubdistc: '',
      codcntyc: '',
      codpostc: ''
    }
    let mockWork = {
      dteempt: '',
      qtyworky: '',
      qtyworkd: '',
      staemp: '',
      dteeffex: '',
      coddomp1: '',
      codpos: '',
      codpost: '',
      dtecodpos: '',
      numlvl: '',
      dtenumlvl: '',
      codbrlc: '',
      codempmt: '',
      typayroll: '',
      typemp: '',
      codcalen: '',
      flgattwn: '',
      codjob: '',
      jobgrade: '',
      dtejobgrade: '',
      codgrpgl: '',
      reptdev: '',
      reptpos: '',
      codhead: '',
      stapos: '',
      stadisb: '',
      numdisab: '',
      dtedisb: '',
      dtedisen: '',
      typdisp: '',
      desdisp: '',
      qtyredue: '',
      dtedeadlines: '',
      yredatrq: '',
      mondatrq: '',
      mthdatrq: '',
      dteoccup: '',
      numtelof: '',
      qtyduepr: '',
      email: '',
      numreqst: '',
      codempold: '',
      dteback: '',
      qtyredue1: '',
      dtedue: '',
      dteupdate: '',
      updateby: ''
    }
    let mockTravel = {
      codtravel: '',
      numtravel: '',
      fueltyp: '',
      distance: '',
      codcable: '',
      codpoint: ''
    }
    let mockIncome = {
      codcurr: '',
      psyfine: '',
      wageh: '',
      waged: '',
      monthly: '',
      dteupdate: '',
      updateby: ''
    }
    let mockIncomeRows = []
    const mockIncomeTotal = 0
    for (var i = 1; i <= mockIncomeTotal; i++) {
      mockIncomeRows.push({
        i: i,
        codincom: '',
        desc_codincom: '',
        calunit: '',
        amtincom1: ''
      })
    }

    let mockTaxTab1 = {
      numtaxid: '',
      numsaid: '',
      frsmemb: '',
      faxtax: '',
      typtax: '',
      dteyrrelf: '',
      dteyrrelt: '',
      amtrelas: '',
      amttaxrel: '',
      codbank: '',
      numbank: '',
      numbrnch: '',
      amtbank: '',
      qtyamtbank: '',
      codbank2: '',
      numbank2: '',
      numbrnch2: ''
    }
    let mockTaxTab2 = {
      dtebf: '',
      amtincbf: '',
      amttaxbf: '',
      amtpf: '',
      amtsaid: ''
    }
    let mockTaxTab3Rows = []
    const mockTaxTab3Total = 0
    for (var j = 1; j <= mockTaxTab3Total; j++) {
      mockTaxTab3Rows.push({
        j: j,
        coddeduct: '',
        desc_coddeduct: '',
        amtdeduct: ''
      })
    }
    let mockTaxTab4 = {
      qtychedu: '',
      qtychned: ''
    }
    let mockTaxTab4Rows = []
    const mockTaxTab4Total = 0
    for (var k = 1; k <= mockTaxTab4Total; k++) {
      mockTaxTab4Rows.push({
        k: k,
        coddeduct: '',
        desc_coddeduct: '',
        amtdeduct: ''
      })
    }
    let mockTaxTab5Rows = []
    const mockTaxTab5Total = 0
    for (var t = 1; t <= mockTaxTab5Total; t++) {
      mockTaxTab5Rows.push({
        coddeduct: '',
        desc_coddeduct: '',
        amtdeduct: ''
      })
    }
    let mockSpouseTab1 = {
      numtaxid: '',
      dtebfsp: '',
      amtincsp: '',
      amttaxsp: '',
      amtpfsp: '',
      amtsasp: ''
    }

    let mockSpouseTab2Rows = mockTaxTab3Rows
    const mockSpouseTab2Total = 0
    for (var l = 1; l <= mockSpouseTab2Total; l++) {
      mockSpouseTab2Rows.push({
        coddeduct: '',
        desc_coddeduct: '',
        amtdeduct: ''
      })
    }
    let mockSpouseTab3Rows = mockTaxTab4Rows
    const mockSpouseTab3Total = 0
    for (var n = 1; n <= mockSpouseTab3Total; n++) {
      mockSpouseTab3Rows.push({
        coddeduct: '',
        desc_coddeduct: '',
        amtdeduct: ''
      })
    }
    let mockSpouseTab4Rows = mockTaxTab5Rows
    const mockSpouseTab4Total = 0
    for (var m = 1; m <= mockSpouseTab4Total; m++) {
      mockSpouseTab4Rows.push({
        coddeduct: '',
        desc_coddeduct: '',
        amtdeduct: ''
      })
    }
    let mockChangeNameRows = []
    const mockChangeNameTotal = 0
    for (var o = 1; o <= mockChangeNameTotal; o++) {
      mockChangeNameRows.push({
        dtechg: '',
        namtitle: '',
        namfirst: '',
        namlast: '',
        deschang: '',
        dteupdate: '',
        updateby: ''
      })
    }
    let mockDocumentRows = []
    const mockDocumentTotal = 0
    for (let o = 1; o <= mockDocumentTotal; o++) {
      mockDocumentRows.push({
        numseq: '',
        namtypdoc: '',
        namdoc: '',
        dterecv: '',
        dtedocen: '',
        numdoc: '',
        filedoc: '',
        desnote: '',
        dteupdate: '',
        updateby: ''
      })
    }
    let mockEducationRows = []
    const mockEducationTotal = 0
    for (let a = 1; a <= mockEducationTotal; a++) {
      mockEducationRows.push({
        numseq: '',
        desedlv: '',
        desdglv: '',
        desmajsb: '',
        desinst: '',
        descount: '',
        codedlv: '',
        flgeduc: '',
        coddglv: '',
        codmajsb: '',
        codminsb: '',
        codinst: '',
        codcount: '',
        numgpa: '',
        stayear: '',
        dtegyear: '',
        filenam1: ''
      })
    }
    let mockWorkExperiencesRows = []
    const mockWorkExperiencesTotal = 0
    for (let a = 1; a <= mockWorkExperiencesTotal; a++) {
      mockWorkExperiencesRows.push({
        numseq: a,
        desnoffi: '',
        desoffi1: '',
        deslstpos: '',
        dtestart: '',
        dteend: '',
        dteupd: '',
        coduser: '',
        deslstjob1: '',
        xx: '',
        xxx: '',
        xxxx: '',
        numteleo: '',
        namboss: '',
        desres: '',
        amtincom: '',
        remark: '',
        filenam1: ''
      })
    }
    let mockTrainingRecordRows = []
    const mockTrainingRecordTotal = 0
    for (let a = 1; a <= mockTrainingRecordTotal; a++) {
      mockTrainingRecordRows.push({
        numseq: a,
        destrain: '',
        dtetr: '',
        desplace: '',
        desinstu: '',
        dtetrain: '',
        dtetren: ''
      })
    }
    let mockInternalTrainingRows = []
    const mockInternalTrainingTotal = 0
    for (let a = 1; a <= mockInternalTrainingTotal; a++) {
      mockInternalTrainingRows.push({
        codcours: '',
        desc_codcours: '',
        codtparg: '',
        dtestart: '',
        dteend: '',
        amttrexp: '',
        v_qtyminhr: ''
      })
    }
    let mockFamilySpouse = {
      codempid: '',
      filenam1: '',
      codtitle: '',
      namfirst: '',
      namlast: '',
      numoffid: '',
      dtespbd: '',
      _alive: '',
      _alive_dtedead: '',
      _revenue: '',
      desnoffi: '',
      codspocc: '',
      numfasp: '',
      nummosp: '',
      dtemarry: '',
      codsppro: '',
      codspcty: '',
      desplreg: '',
      desnote: '',
      filenam2: '',
      dteupd: '',
      coduser: ''
    }
    let mockFamilyChildrenRows = []
    const mockFamilyChildrenTotal = 0
    for (let a = 1; a <= mockFamilyChildrenTotal; a++) {
      mockFamilyChildrenRows.push({
        numseq: a,
        namchild: '',
        _lastchild: '',
        numoffid: '',
        dtechbd: '',
        dessex: '',
        flgedlv: '',
        desflgedlv: '',
        flgdeduct: '',
        desflgded: ''
      })
    }
    let mockFamilyParentFather = {
      codempid_father: '',
      filenam1: '',
      codtitle: '',
      namfathr: '',
      _lastfathr: '',
      _numfasp: '',
      _dtehdbfathr: '',
      codfnath: '',
      codfrelg: '',
      codfoccu: '',
      _falive: '',
      _falive_dtedead: '',
      filenam2: ''
    }
    let mockFamilyParentMother = {
      codempid_mother: '',
      filenam3: '',
      _codtitlem: '',
      nammothr: '',
      _lastmothr: '',
      _nummosp: '',
      _dtehdbmothr: '',
      codmnath: '',
      codmrelg: '',
      codmoccu: '',
      _malive: '',
      _malive_dtedead: '',
      filenam4: ''
    }
    let mockFamilyParentOther = {
      _ocodtitle: '',
      namcont: '',
      _lastcont: '',
      adrcont1: '',
      codpost: '',
      numtele: '',
      numfax: '',
      email: '',
      desrelat: '',
      dteupd: '',
      coduser: ''
    }
    let mockFamilyRelativesRows = []
    const mockFamilyRelativesTotal = 0
    for (let a = 1; a <= mockFamilyRelativesTotal; a++) {
      mockFamilyRelativesRows.push({
        numseq: a,
        namcont: '',
        numoffid: '',
        dtechbd: '',
        codsex: '',
        dessex: '',
        relation: '',
        desrelat: '',
        filenam1: '',
        codtitle: '',
        namchild: '',
        _lastname: '',
        _job: '',
        _life: '',
        _dtedead: '',
        filenam2: ''
      })
    }
    let mockGuarntee1Rows = []
    const mockGuarntee1Total = 0
    for (let t = 1; t <= mockGuarntee1Total; t++) {
      mockGuarntee1Rows.push({
        v_namguar: '',
        dtegucon: '',
        dteguexp: '',
        desrelat: '',
        desoccup: ''
      })
    }
    let mockGuarntee2Rows = []
    const mockGuarntee2Total = 0
    for (let t = 1; t <= mockGuarntee2Total; t++) {
      mockGuarntee2Rows.push({
        numcolla: '',
        descolla: '',
        amtcolla: '',
        numdocum: ''
      })
    }
    let mockGuarntee3Rows = []
    const mockGuarntee3Total = 0
    for (let t = 1; t <= mockGuarntee3Total; t++) {
      mockGuarntee3Rows.push({
        codempid: '',
        codnam: '',
        desc_codnam: '',
        relation: '',
        codpos: '',
        desc_codpos: '',
        codcomp: '',
        desc_codcomp: '',
        dteupdate: '',
        updateby: ''
      })
    }
    let mockOther = {
      status: '',
      dtedown: '',
      dteend: ''
    }
    let mockCompetencyRows = []
    const mockCompetencyTotal = 0
    for (let a = 1; a <= mockCompetencyTotal; a++) {
      mockCompetencyRows.push({
        numseq: a,
        desc_typtency: '',
        codtency: '',
        desc_codtency: '',
        score: '',
        _targetscore: '',
        _gapscore: ''
      })
    }
    let mockLanguageRows = []
    const mockLanguageTotal = 0
    for (let a = 1; a <= mockLanguageTotal; a++) {
      mockLanguageRows.push({
        numseq: a,
        _desc_typtency: '',
        _listen: '',
        _speak: '',
        _read: '',
        _write: ''
      })
    }
    let mockHonorsRows = []
    const mockHonorsTotal = 0
    for (let a = 1; a <= mockHonorsTotal; a++) {
      mockHonorsRows.push({
        numseq: a,
        dteinput: '',
        typrewd: '',
        desrewd: '',
        desrewd1: '',
        numhmref: '',
        dteupd: '',
        coduser: ' ',
        filenam1: '',
        filenam2: ''
      })
    }
    return {
      personalTax: {
        personal: mockPersonal,
        address: mockAddress,
        work: mockWork,
        travel: mockTravel,
        income: {
          detail: mockIncome,
          table: {
            total: mockIncomeTotal,
            rows: mockIncomeRows
          }
        },
        tax: {
          tab1: mockTaxTab1,
          tab2: mockTaxTab2,
          tab3: {
            total: mockTaxTab3Total,
            rows: mockTaxTab3Rows
          },
          tab4: {
            detail: mockTaxTab4,
            table: {
              total: mockTaxTab4Total,
              rows: mockTaxTab4Rows
            }
          },
          tab5: {
            total: mockTaxTab5Total,
            rows: mockTaxTab5Rows
          }
        },
        spouse: {
          tab1: mockSpouseTab1,
          tab2: {
            total: mockSpouseTab2Total,
            rows: mockSpouseTab2Rows
          },
          tab3: {
            total: mockSpouseTab3Total,
            rows: mockSpouseTab3Rows
          },
          tab4: {
            total: mockSpouseTab4Total,
            rows: mockSpouseTab4Rows
          }
        },
        changename: {
          total: mockChangeNameTotal,
          rows: mockChangeNameRows
        },
        document: {
          total: mockDocumentTotal,
          rows: mockDocumentRows
        }
      },
      workStudy: {
        tableEducation: {
          total: mockEducationTotal,
          rows: mockEducationRows
        },
        tableWorkExperiences: {
          total: mockWorkExperiencesTotal,
          rows: mockWorkExperiencesRows
        },
        tableTrainingRecord: {
          total: mockTrainingRecordTotal,
          rows: mockTrainingRecordRows
        },
        tableInternalTraining: {
          total: mockInternalTrainingTotal,
          rows: mockInternalTrainingRows
        }
      },
      family: {
        spouse: mockFamilySpouse,
        tableChildren: {
          total: mockFamilyChildrenTotal,
          rows: mockFamilyChildrenRows
        },
        parent: {
          father: mockFamilyParentFather,
          mother: mockFamilyParentMother,
          other: mockFamilyParentOther
        },
        tableRelatives: {
          total: mockFamilyRelativesTotal,
          rows: mockFamilyRelativesRows
        }
      },
      guarntee: {
        guarntee1: {
          total: mockGuarntee1Total,
          rows: mockGuarntee1Rows
        },
        guarntee2: {
          total: mockGuarntee2Total,
          rows: mockGuarntee2Rows
        },
        guarntee3: {
          total: mockGuarntee3Total,
          rows: mockGuarntee3Rows
        }
      },
      other: mockOther,
      competency: {
        tableCompetency: {
          total: mockCompetencyTotal,
          rows: mockCompetencyRows
        },
        tableLanguage: {
          total: mockLanguageTotal,
          rows: mockLanguageRows
        },
        tableHonors: {
          total: mockHonorsTotal,
          rows: mockHonorsRows
        }
      }
    }
  },
  detail () {
    let mockPersonal = {
      codempid: '53100',
      image: '/static/image/employee/emp_img28.jpg',
      signature: '/static/image/employee/signature/sign88.png',
      codtitle: '1',
      namfirst: 'สมชาย',
      namlast: 'เข็มกลัดทองแดง',
      namnick: 'เต๋า',
      tel: '02-2545566',
      tel1: '085-3978845',
      idline: 'somchai',
      numoffid: '1800133275489',
      dteoffid: '06/06/2021',
      adrissue: 'วังทองหลาง',
      codprovi: 'P0010',
      hospss: 'H0010',
      numpasid: 'A123456',
      dtepasid: '20/12/2020',
      visaid: '',
      dtevisa: '',
      numlicid: '12346675',
      dtelicid: '30/12/2021',
      dteempdb: '25/06/1980',
      coddomcl: 'กรุงเทพมหานคร',
      bdyear: '37',
      bdmonth: '5',
      codsex: 'ชาย',
      weight: '65',
      high: '170',
      codblood: 'O',
      codorgin: 'ไทย',
      codnatnl: 'ไทย',
      codrelgn: 'พุทธ',
      stamarry: 'แต่งงาน',
      stamilit: 'ผ่าน',
      workid: '7894446',
      dteworkid: '22/02/2016',
      dtework: '27/04/2020',
      refid: 'P0000001',
      dteratir: ''
    }
    let mockAddress = {
      adrreg: '521/44',
      codprovr: 'P0010',
      coddistr: 'D4',
      codsubdistr: '1042',
      codcntyr: 'C006',
      codpostr: '10310',
      adrcont: '521/44',
      codprovc: 'P0010',
      coddistc: 'D4',
      codsubdistc: '1042',
      codcntyc: 'C006',
      codpostc: '10310'
    }
    let mockWork = {
      dteempt: '20/11/2015',
      qtyworky: '2',
      qtyworkd: '2',
      staemp: 'ปัจจุบัน',
      dteeffex: '',
      codcomp1: 'PPS-IMP',
      codpos: '0310',
      codpost: 'Project Manager',
      dtecodpos: '20/11/2015',
      numlvl: '',
      dtenumlvl: '20/02/2016',
      codbrlc: '0001',
      codempmt: '0001',
      typayroll: '0001',
      typemp: 'ประจำ',
      codcalen: '0001',
      flgattwn: 'รูดบัตร',
      codjob: 'P',
      jobgrade: 'Top Management',
      dtejobgrade: '20/11/2015',
      codgrpgl: '99',
      reptdev: '',
      reptpos: '',
      codhead: 'ชาติชาย บรรเทาพิทักษ์',
      stapos: 'Managing director',
      stadisb: 'ปกติ',
      numdisab: '',
      dtedisb: '',
      dtedisen: '',
      typdisp: '',
      desdisp: '',
      qtyredue: '',
      dtedeadlines: '',
      yredatrq: '0',
      mondatrq: '0',
      mthdatrq: '',
      dteoccup: '20/03/2016',
      numtelof: '123',
      qtyduepr: faker.random.number().toString().substr(0, 5),
      email: 'somchai@gmail.com',
      numreqst: '',
      codempold: '',
      dteback: '',
      qtyredue1: '',
      dtedue: '',
      dteupdate: moment().format('DD/MM/YYYY'),
      updateby: 'ชาติชาย'
    }
    let mockTravel = {
      codtravel: 'รถยนต์ส่วนตัว',
      numtravel: 'กง 2515',
      fueltyp: 'E85',
      distance: '15 km',
      codcable: 'CB0004',
      codpoint: '03'
    }
    let mockIncome = {
      codcurr: 'BAHT',
      psyfine: '70,000.00',
      wageh: '291.67',
      waged: '2,333.33',
      monthly: '70,000.00',
      dteupdate: moment().format('DD/MM/YYYY'),
      updateby: 'ชาติชาย'
    }
    let mockIncomeRows = []
    const mockIncomeTotal = 5
    // for (var i = 1; i <= mockIncomeTotal; i++) {
    //   mockIncomeRows.push({
    //     i: i,
    //     codincom: 'PL0001',
    //     desc_codincom: 'เงินประจำเดือน',
    //     calunit: 'บาท',
    //     amtincom1: '60,000'
    //   })
    // }
    mockIncomeRows.push({
      codincom: '01',
      desc_codincom: 'เงินเดือน/ค่าแรง',
      calunit: 'บาท',
      amtincom1: '70,000.00'
    })
    mockIncomeRows.push({
      codincom: '02',
      desc_codincom: 'ค่าครองชีพ',
      calunit: 'บาท',
      amtincom1: '0.00'
    })
    mockIncomeRows.push({
      codincom: '03',
      desc_codincom: 'ค่าตำแหน่ง',
      calunit: 'บาท',
      amtincom1: '0.00'
    })
    mockIncomeRows.push({
      codincom: '04',
      desc_codincom: 'ค่าความสามารถ',
      calunit: 'บาท',
      amtincom1: '0.00'
    })
    mockIncomeRows.push({
      codincom: '05',
      desc_codincom: 'ค่าวิชาชีพ',
      calunit: 'บาท',
      amtincom1: '0.00'
    })
    let mockTaxTab1 = {
      numtaxid: '3801600671089',
      numsaid: '3801600671089',
      frsmemb: '01/01/2007',
      faxtax: 'หักภาษี ณ ที่จ่าย',
      typtax: 'แยกชำระภาษี',
      dteyrrelf: '',
      dteyrrelt: '',
      amtrelas: '0.00',
      amttaxrel: '0.00',
      codbank: '0004',
      numbank: '251-2158-48555',
      numbrnch: 'KTT112',
      amtbank: '20%',
      qtyamtbank: '',
      codbank2: '0008',
      numbank2: '120-252-36666',
      numbrnch2: 'SCB123'
    }
    let mockTaxTab2 = {
      dtebf: '16/12/2015',
      amtincbf: '0.00',
      amttaxbf: '0.00',
      amtpf: '60,000.00',
      amtsaid: '0.00'
    }
    let mockTaxTab3Rows = []
    // const mockcoddeduct = [
    //   'T0110',
    //   'T0114',
    //   'T0167'
    // ]
    // const mockdesccoddeduct = [
    //   'การเดินทาง',
    //   'ซื้อของ',
    //   'ท่องเที่ยวต่างประเทศ'
    // ]
    // const mockamtdeduct = [
    //   '1,200',
    //   '5,000',
    //   '250,000'
    // ]
    const mockTaxTab3Total = 4
    // for (var j = 1; j <= mockTaxTab3Total; j++) {
    //   mockTaxTab3Rows.push({
    //     j: j,
    //     coddeduct: mockcoddeduct[j - 1],
    //     desc_coddeduct: mockdesccoddeduct[j - 1],
    //     amtdeduct: mockamtdeduct[j - 1]
    //   })
    // }
    mockTaxTab3Rows.push({
      coddeduct: 'E002',
      desc_coddeduct: 'เงินสะสม กบข.',
      amtdeduct: '0.00'
    })
    mockTaxTab3Rows.push({
      coddeduct: 'E003',
      desc_coddeduct: 'เงินสะสมกองทุนสงเคราะห์โรงเรียนเอกชน',
      amtdeduct: '0.00'
    })
    mockTaxTab3Rows.push({
      coddeduct: 'E004',
      desc_coddeduct: 'ผู้มีเงินได้อายุตั้งแต่ 65 ปีขึ้นไป',
      amtdeduct: '0.00'
    })
    mockTaxTab3Rows.push({
      coddeduct: 'E005',
      desc_coddeduct: 'เงินชดเชยที่ได้รับตามกฏหมายแรงงาน',
      amtdeduct: '0.00'
    })
    let mockTaxTab4 = {
      qtychedu: '0',
      qtychned: '0'
    }
    let mockTaxTab4Rows = []
    const mockTaxTab4Total = 24
    // for (var k = 1; k <= mockTaxTab4Total; k++) {
    //   mockTaxTab4Rows.push({
    //     k: k,
    //     coddeduct: 'l12',
    //     desc_coddeduct: 'ค่าอาหารกลางวัน',
    //     amtdeduct: '2,000'
    //   })
    // }
    mockTaxTab4Rows.push({
      coddeduct: 'D003',
      desc_coddeduct: 'ผู้มีเงินได้',
      amtdeduct: '30,000.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D004',
      desc_coddeduct: 'บุตรศึกษา',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D005',
      desc_coddeduct: 'บุตรไม่ศึกษา',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D006',
      desc_coddeduct: 'บิดา',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D007',
      desc_coddeduct: 'มารดา',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D008',
      desc_coddeduct: 'เบี้ยประกันสุขภาพบิดา',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D009',
      desc_coddeduct: 'เบี้ยประกันสุขภาพมารดา',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D010',
      desc_coddeduct: 'เบี้ยประกันชีวิต',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D011',
      desc_coddeduct: 'ค่าซื้อหน่วยลงทุนในกองทุนรวมเพื่อการเลี้ยงชีพ',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D012',
      desc_coddeduct: 'ค่าซื้อหน่วยลงทุนในกองทุนรวมหุ้นระยะยาว',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D013',
      desc_coddeduct: 'ดอกเบี้ยเงินกู้ยืมเพื่อที่อยู่อาศัย',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D014',
      desc_coddeduct: 'อุปการะคนทุพลภาพ',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D015',
      desc_coddeduct: 'ท่องเที่ยว',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D016',
      desc_coddeduct: 'เบี้ยประกันชีวิตแบบบำนาญ',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D017',
      desc_coddeduct: 'ค่าซื้อสินค้าในประเทศ',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'D018',
      desc_coddeduct: 'ค่าท่องเที่ยวเดือนธันวาคม',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'E001',
      desc_coddeduct: 'เงินสะสมกองทุนสำรองเลี้ยงชีพ(ส่วนที่เกิน 10,000)',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'E002',
      desc_coddeduct: 'เงินสะสม กบข.',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'E003',
      desc_coddeduct: 'เงินสะสมกองทุนสงเคราะห์ครูโรงเรียนเอกชน',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'E004',
      desc_coddeduct: 'ผู้มีเงินได้อายุตั้งแต่ 65 ปีขึ้นไป',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'E005',
      desc_coddeduct: 'เงินชดเชยที่ได้รับตามกฎหมายแรงงาน',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'O001',
      desc_coddeduct: 'เงินสนับสนุนการศึกษา',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'O002',
      desc_coddeduct: 'เงินสนับสนุนกีฬา',
      amtdeduct: '0.00'
    })
    mockTaxTab4Rows.push({
      coddeduct: 'O003',
      desc_coddeduct: 'เงินบริจาค',
      amtdeduct: '0.00'
    })
    let mockTaxTab5Rows = []
    const mockTaxTab5Total = 3
    // for (var t = 1; t <= mockTaxTab5Total; t++) {
    //   mockTaxTab5Rows.push({
    //     coddeduct: 'io001',
    //     desc_coddeduct: 'ค่าอาหาร',
    //     amtdeduct: '200'
    //   })
    // }
    mockTaxTab5Rows.push({
      coddeduct: 'O001',
      desc_coddeduct: 'เงินสนับสนุนการศึกษา',
      amtdeduct: '0.00'
    })
    mockTaxTab5Rows.push({
      coddeduct: 'O002',
      desc_coddeduct: 'เงินสนับสนุนกีฬา',
      amtdeduct: '0.00'
    })
    mockTaxTab5Rows.push({
      coddeduct: 'O003',
      desc_coddeduct: 'เงินบริจาค',
      amtdeduct: '0.00'
    })
    let mockSpouseTab1 = {
      numtaxid: '5801600699999',
      dtebfsp: '20/05/2017',
      amtincsp: '0.00',
      amttaxsp: '0.00',
      amtpfsp: '0.00',
      amtsasp: '0.00'
    }
    let mockSpouseTab2Rows = mockTaxTab3Rows
    const mockSpouseTab2Total = 4
    // const mockcoddeduct2 = [
    //   'tr201',
    //   'tr206'
    // ]
    // const mockdesccoddeduct2 = [
    //   'ค่าประกันสังคม',
    //   'ค่าประกันชีวิต'
    // ]
    // const mockamtdeduct2 = [
    //   '26,000',
    //   '20,245'
    // ]
    // for (var l = 1; l <= mockSpouseTab2Total; l++) {
    //   mockSpouseTab2Rows.push({
    //     coddeduct: mockcoddeduct2[l - 1],
    //     desc_coddeduct: mockdesccoddeduct2[l - 1],
    //     amtdeduct: mockamtdeduct2[l - 1]
    //   })
    // }
    let mockSpouseTab3Rows = mockTaxTab4Rows
    const mockSpouseTab3Total = 24
    // for (var n = 1; n <= mockSpouseTab3Total; n++) {
    //   mockSpouseTab3Rows.push({
    //     coddeduct: 'Gl251',
    //     desc_coddeduct: 'การเดินทาง',
    //     amtdeduct: ''
    //   })
    // }
    let mockSpouseTab4Rows = mockTaxTab5Rows
    const mockSpouseTab4Total = 3
    // for (var m = 1; m <= mockSpouseTab4Total; m++) {
    //   mockSpouseTab4Rows.push({
    //     coddeduct: '125tt',
    //     desc_coddeduct: 'ค่าประกันสังคม',
    //     amtdeduct: '600'
    //   })
    // }
    let mockChangeNameRows = []
    const mockChangeNameTotal = 1
    for (var o = 1; o <= mockChangeNameTotal; o++) {
      mockChangeNameRows.push({
        dtechg: '01/05/2017',
        namtitle: 'นาย',
        namfirst: 'สมชาย',
        namlast: 'เข็มกลัดทองแดง',
        deschang: 'ตามหลักโหราศาสตร์',
        dteupdate: '01/05/2017',
        updateby: 'สมชาย'
      })
    }
    let mockDocumentRows = []
    const mockDocumentTotal = 2
    // for (let o = 1; o <= mockDocumentTotal; o++) {
    //   mockDocumentRows.push({
    //     numseq: '1',
    //     namtypdoc: 'file.doc',
    //     namdoc: 'หนังสือสำคัญการเปลี่ยนชื่อ',
    //     dterecv: '01/05/2017',
    //     dtedocen: '01/11/2017',
    //     numdoc: '12222002',
    //     filedoc: 'Loree',
    //     desnote: 'ตรวจสอบการทำงาน',
    //     dteupdate: moment().format('DD/MM/YYYY'),
    //     updateby: faker.lorem.word()
    //   })
    // }
    mockDocumentRows.push({
      numseq: '1',
      namtypdoc: 'Transript',
      namdoc: 'ใบจบการศึกษา',
      dterecv: '20/11/2015',
      dtedocen: '01/12/2018',
      numdoc: 'TS-0001',
      filedoc: '6010_150803093821.pdf',
      desnote: '',
      dteupdate: moment().format('DD/MM/YYYY'),
      updateby: 'สมชาย เข็มกลัดทองแดง'
    })
    mockDocumentRows.push({
      numseq: '2',
      namtypdoc: 'Resume',
      namdoc: 'ประวัติส่วนตัว',
      dterecv: '20/11/2015',
      dtedocen: '01/12/2018',
      numdoc: 'RS-0001',
      filedoc: '16010_150803093857.pdf',
      desnote: '',
      dteupdate: moment().format('DD/MM/YYYY'),
      updateby: 'สมชาย เข็มกลัดทองแดง'
    })
    let mockEducationRows = []
    const mockEducationTotal = 3
    // for (let a = 1; a <= mockEducationTotal; a++) {
    //   mockEducationRows.push({
    //     numseq: '1',
    //     desedlv: 'ปริญญาเอก',
    //     desdglv: 'ปริญญาเอก',
    //     desmajsb: 'การจัดการ',
    //     desinst: 'จุฬาลงกรณ์มหาวิทยาลัย',
    //     descount: 'ประเทศไทย',
    //     codedlv: '',
    //     flgeduc: '',
    //     coddglv: '',
    //     codmajsb: '',
    //     codminsb: '',
    //     codinst: '',
    //     codcount: '',
    //     numgpa: '',
    //     stayear: '',
    //     dtegyear: '',
    //     filenam1: ''
    //   })
    // }
    mockEducationRows.push({
      numseq: '1',
      desedlv: 'ปริญญาโท',
      desdglv: 'บริหารธุรกิจบัณฑิต',
      desmajsb: 'การจัดการ',
      desinst: 'จุฬาลงกรณ์มหาวิทยาลัย',
      descount: 'ประเทศไทย',
      codedlv: '',
      flgeduc: '',
      coddglv: '',
      codmajsb: '',
      codminsb: '',
      codinst: '',
      codcount: '',
      numgpa: '',
      stayear: '',
      dtegyear: '',
      filenam1: ''
    })
    mockEducationRows.push({
      numseq: '2',
      desedlv: 'ปริญญาตรี',
      desdglv: 'เทคโนโลยีบัณฑิต (เทคโนโลยีสารสนเทศธุรกิจ)',
      desmajsb: 'การเงินการธนาคาร',
      desinst: 'จุฬาลงกรณ์มหาวิทยาลัย',
      descount: 'ประเทศไทย',
      codedlv: '',
      flgeduc: '',
      coddglv: '',
      codmajsb: '',
      codminsb: '',
      codinst: '',
      codcount: '',
      numgpa: '',
      stayear: '',
      dtegyear: '',
      filenam1: ''
    })
    let mockWorkExperiencesRows = []
    const mockWorkExperiencesTotal = 2
    // for (let a = 1; a <= mockWorkExperiencesTotal; a++) {
    //   mockWorkExperiencesRows.push({
    //     numseq: a,
    //     desnoffi: faker.company.companyName(),
    //     desoffi1: faker.address.streetAddress(),
    //     deslstpos: faker.name.jobType(),
    //     dtestart: moment(faker.date.past()).format('DD/MM/YYYY'),
    //     dteend: moment(faker.date.past()).format('DD/MM/YYYY'),
    //     dteupd: moment(faker.date.past()).format('DD/MM/YYYY'),
    //     coduser: 'TJS00001',
    //     deslstjob1: '',
    //     xx: '',
    //     xxx: '',
    //     xxxx: '',
    //     numteleo: '',
    //     namboss: '',
    //     desres: '',
    //     amtincom: '',
    //     remark: '',
    //     filenam1: ''
    //   })
    // }
    mockWorkExperiencesRows.push({
      numseq: '1',
      desnoffi: 'Thai Oil PCL',
      desoffi1: 'อำเภอศรีราชา จังหวัดชลบุรี',
      deslstpos: 'Project Manager',
      dtestart: '01/04/2013',
      dteend: '30/10/2015',
      dteupd: '20/11/2015',
      coduser: 'สมชาย เข็มกลัดทองแดง',
      deslstjob1: '',
      xx: '',
      xxx: '',
      xxxx: '',
      numteleo: '',
      namboss: '',
      desres: '',
      amtincom: '',
      remark: '',
      filenam1: ''
    })
    mockWorkExperiencesRows.push({
      numseq: '2',
      desnoffi: 'บริษัท ปูนซิเมนต์ไทย จำกัด',
      desoffi1: 'บางซื่อ กรุงเทพฯ',
      deslstpos: 'Project Leader',
      dtestart: '20/01/2012',
      dteend: '30/03/2013',
      dteupd: '20/11/2015',
      coduser: 'สมชาย เข็มกลัดทองแดง',
      deslstjob1: '',
      xx: '',
      xxx: '',
      xxxx: '',
      numteleo: '',
      namboss: '',
      desres: '',
      amtincom: '',
      remark: '',
      filenam1: ''
    })
    let mockTrainingRecordRows = []
    const mockTrainingRecordTotal = 2
    // for (let a = 1; a <= mockTrainingRecordTotal; a++) {
    //   mockTrainingRecordRows.push({
    //     numseq: a,
    //     destrain: 'Oracle',
    //     dtetr: moment(faker.date.past()).format('DD/MM/YYYY') + '-' + moment(faker.date.past()).format('DD/MM/YYYY'),
    //     desplace: faker.address.country(),
    //     desinstu: faker.name.findName(),
    //     dtetrain: '',
    //     dtetren: ''
    //   })
    // }
    mockTrainingRecordRows.push({
      numseq: '1',
      destrain: 'หลักสูตรผู้นำ',
      dtetr: '15/01/2009 - 18/01/2009',
      desplace: 'AIT',
      desinstu: 'AIT',
      dtetrain: '',
      dtetren: ''
    })
    mockTrainingRecordRows.push({
      numseq: '1',
      destrain: 'หลักสูตรผู้บริหารระดับสูง',
      dtetr: '05/08/2011 - 07/08/2011',
      desplace: 'จุฬาลงกรณ์มหาวิทยาลัย',
      desinstu: 'สถาบันบัณฑิตบริหารธุรกิจ',
      dtetrain: '',
      dtetren: ''
    })
    let mockInternalTrainingRows = []
    const mockInternalTrainingTotal = 2
    // for (let a = 1; a <= mockInternalTrainingTotal; a++) {
    //   mockInternalTrainingRows.push({
    //     codcours: faker.random.word(),
    //     desc_codcours: faker.random.words(),
    //     codtparg: 'ภายใน',
    //     dtestart: moment(faker.date.past()).format('DD/MM/YYYY'),
    //     dteend: moment(faker.date.past()).format('DD/MM/YYYY'),
    //     amttrexp: faker.finance.amount(),
    //     v_qtyminhr: '8.00'
    //   })
    // }
    mockInternalTrainingRows.push({
      codcours: 'M00001',
      desc_codcours: 'ผู้จัดการมืออาชีพเพื่อการทำงาน',
      codtparg: 'ภายใน',
      dtestart: '15/01/2016',
      dteend: '15/01/2016',
      amttrexp: '1,000.00',
      v_qtyminhr: '8.00'
    })
    mockInternalTrainingRows.push({
      codcours: 'M00004',
      desc_codcours: 'วิธีการบริหารงาน และการขอ ISO 9001',
      codtparg: 'ภายใน',
      dtestart: '25/10/2016',
      dteend: '25/10/2016',
      amttrexp: '2,000.00',
      v_qtyminhr: '8.00'
    })
    let mockFamilySpouse = {
      codempid: '',
      filenam1: '',
      codtitle: '2',
      namfirst: 'ชนิสรา',
      namlast: 'เข็มกลัดทองแดง',
      numoffid: '3801100000001',
      dtespbd: '23/07/1982',
      _alive: 'Y',
      _alive_dtedead: '',
      _revenue: 'Y',
      desnoffi: 'บริษัท การบินไทย จำกัด',
      codspocc: '0006',
      numfasp: '3801100000002',
      nummosp: '3801100000003',
      dtemarry: '21/11/2016',
      codsppro: 'P0010',
      codspcty: 'C006',
      desplreg: 'บางรัก',
      desnote: '',
      filenam2: '',
      dteupd: '20/11/2015',
      coduser: 'สมชาย เข็มกลัดทองแดง'
    }
    let mockFamilyChildrenRows = []
    // const mockSex = ['Male', 'Female']
    // const mockFlgded = ['ใช้', 'ไม่ใช้']
    const mockFamilyChildrenTotal = 1
    // for (let a = 1; a <= mockFamilyChildrenTotal; a++) {
    //   mockFamilyChildrenRows.push({
    //     numseq: a,
    //     namchild: faker.name.findName(),
    //     _lastchild: '',
    //     numoffid: Math.floor((Math.random() * 100000000000000) + 1),
    //     dtechbd: moment(faker.date.past()).format('DD/MM/YYYY'),
    //     dessex: mockSex[ a - 1 ],
    //     flgedlv: 'Y',
    //     desflgedlv: 'ศึกษา',
    //     flgdeduct: 'Y',
    //     desflgded: mockFlgded[ a - 1 ]
    //   })
    // }
    mockFamilyChildrenRows.push({
      numseq: '1',
      namchild: 'ด.ช. ชยาชาติ เข็มกลัดทองแดง',
      _lastchild: '',
      numoffid: '3801100000004',
      dtechbd: '01/08/2011',
      dessex: 'เพศชาย',
      flgedlv: 'Y',
      desflgedlv: 'ศึกษา',
      flgdeduct: 'Y',
      desflgded: 'ใช้'
    })
    let mockFamilyParentFather = {
      codempid_father: '',
      filenam1: '',
      codtitle: '1',
      namfathr: 'สมหมาย',
      _lastfathr: 'เข็มกลัดทองแดง',
      _numfasp: '3801100009992',
      _dtehdbfathr: '09/08/1965',
      codfnath: '0001',
      codfrelg: '0001',
      codfoccu: '0002',
      _falive: 'Y',
      _falive_dtedead: '',
      filenam2: ''
    }
    let mockFamilyParentMother = {
      codempid_mother: '',
      filenam3: '',
      _codtitlem: '2',
      nammothr: 'สมศรี',
      _lastmothr: 'เข็มกลัดทองแดง',
      _nummosp: '3801100009993',
      _dtehdbmothr: '29/12/1967',
      codmnath: '0001',
      codmrelg: '0001',
      codmoccu: '0002',
      _malive: 'Y',
      _malive_dtedead: '',
      filenam4: ''
    }
    let mockFamilyParentOther = {
      _ocodtitle: '1',
      namcont: 'ชนิสรา',
      _lastcont: 'เข็มกลัดทองแดง',
      adrcont1: '521/44 วังทองหลาง กรุงเทพมหานคร',
      codpost: '10310',
      numtele: '085-3978999',
      numfax: '',
      email: '',
      desrelat: 'ภรรยา',
      dteupd: '20/11/2015',
      coduser: 'สมชาย เข็มกลัดทองแดง'
    }
    let mockFamilyRelativesRows = []
    const mockFamilyRelativesTotal = 1
    for (let a = 1; a <= mockFamilyRelativesTotal; a++) {
      mockFamilyRelativesRows.push({
        numseq: a,
        namcont: 'สมศักดิ์ เข็มกลัดทองแดง',
        numoffid: Math.floor((Math.random() * 100000000000000) + 1),
        dtechbd: '20/09/1978',
        codsex: '1',
        dessex: 'Male',
        relation: '1',
        desrelat: 'พี่ชาย',
        filenam1: '',
        codtitle: '',
        namchild: '',
        _lastname: '',
        _job: '',
        _life: '',
        _dtedead: '',
        filenam2: ''
      })
    }
    let mockGuarntee1Rows = []
    const mockGuarntee1Total = 1
    for (let t = 1; t <= mockGuarntee1Total; t++) {
      mockGuarntee1Rows.push({
        v_namguar: 'ธีรเดช ธีรนันท์ไทย',
        dtegucon: '20/11/2015',
        dteguexp: '30/06/2018',
        desrelat: 'ญาติ',
        desoccup: 'วิศวกรออกแบบ'
      })
    }
    let mockGuarntee2Rows = []
    const mockGuarntee2Total = 1
    for (let t = 1; t <= mockGuarntee2Total; t++) {
      mockGuarntee2Rows.push({
        numcolla: '125444',
        descolla: 'เงินฝากประจำ',
        amtcolla: '531,000.00',
        numdocum: '215445'
      })
    }
    let mockGuarntee3Rows = []
    const mockGuarntee3Total = 1
    for (let t = 1; t <= mockGuarntee3Total; t++) {
      mockGuarntee3Rows.push({
        codempid: faker.random.number().toString().substr(0, 10),
        codnam: 'นายสุชาติ เทพพิทักษ์',
        desc_codnam: 'นายสุชาติ เทพพิทักษ์',
        relation: 'หัวหน้างาน',
        codpos: faker.random.number().toString().substr(0, 10),
        desc_codpos: 'Managing director',
        codcomp: faker.random.number().toString().substr(0, 10),
        desc_codcomp: 'Thai Oil PCL',
        dteupdate: '20/11/2015',
        updateby: 'สมชาย เข็มกลัดทองแดง'
      })
    }
    let mockOther = {
      status: '',
      numcar: '',
      tel2: ''
    }
    let mockCompetencyRows = []
    const mockScore = [ '5.0', '4.0', '3.0', '2.0', '1.0' ]
    const mockNumgrade = {
      '5': 'กำหนดวิสัยทัศน์และกลยุทธได้',
      '4': 'สามารถถ่ายทอดและพัฒนาได้',
      '3': 'แก้ไขปัญหาได้',
      '2': 'สามารถปฏิบัติงานได้',
      '1': 'มีความรู้ความเข้าใจ'
    }
    const mockTyptency = {
      '5': 'ภาษา',
      '4': 'ดนตรี',
      '3': 'กีฬา',
      '2': 'ขับรถยนต์',
      '1': 'พูด'
    }
    const mockCompetencyTotal = 5
    for (let a = 1; a <= mockCompetencyTotal; a++) {
      mockCompetencyRows.push({
        numseq: a,
        desc_typtency: mockTyptency[a],
        codtency: 'A00' + a,
        desc_codtency: mockNumgrade[a],
        score: mockScore[a - 1],
        _targetscore: Math.floor(Math.random() * 4) + 1,
        _gapscore: Math.floor(Math.random() * 2) + 1
      })
    }
    let mockLanguageRows = []
    const mockdesc = [ 'English', 'Japanese', 'Chinese', 'loas' ]
    const mockLv = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G' ]
    const mockLanguageTotal = 4
    for (let a = 1; a <= mockLanguageTotal; a++) {
      mockLanguageRows.push({
        numseq: a,
        _desc_typtency: mockdesc[a - 1],
        _listen: mockLv[a * Math.floor(Math.random() * 0)],
        _speak: mockLv[a * Math.floor(Math.random() * 1)],
        _read: mockLv[a * Math.floor(Math.random() * 2)],
        _write: mockLv[a * Math.floor(Math.random() * 3)]
      })
    }
    let mockHonorsRows = []
    const mockHonorsTotal = 1
    for (let a = 1; a <= mockHonorsTotal; a++) {
      mockHonorsRows.push({
        numseq: a,
        dteinput: '05/04/2017',
        typrewd: '0001',
        desrewd: 'รางวัลพนักงานดีเด่น',
        desrewd1: 'ผู้บริหารดีเด่นแห่งปี',
        numhmref: 'PPS/2558/0001',
        dteupd: '05/04/2017',
        coduser: 'ชาติชาย บรรเทาพิทักษ์',
        filenam1: '',
        filenam2: ''
      })
    }
    return {
      personalTax: {
        personal: mockPersonal,
        address: mockAddress,
        work: mockWork,
        travel: mockTravel,
        income: {
          detail: mockIncome,
          table: {
            total: mockIncomeTotal,
            rows: mockIncomeRows
          }
        },
        tax: {
          tab1: mockTaxTab1,
          tab2: mockTaxTab2,
          tab3: {
            total: mockTaxTab3Total,
            rows: mockTaxTab3Rows
          },
          tab4: {
            detail: mockTaxTab4,
            table: {
              total: mockTaxTab4Total,
              rows: mockTaxTab4Rows
            }
          },
          tab5: {
            total: mockTaxTab5Total,
            rows: mockTaxTab5Rows
          }
        },
        spouse: {
          tab1: mockSpouseTab1,
          tab2: {
            total: mockSpouseTab2Total,
            rows: mockSpouseTab2Rows
          },
          tab3: {
            total: mockSpouseTab3Total,
            rows: mockSpouseTab3Rows
          },
          tab4: {
            total: mockSpouseTab4Total,
            rows: mockSpouseTab4Rows
          }
        },
        changename: {
          total: mockChangeNameTotal,
          rows: mockChangeNameRows
        },
        document: {
          total: mockDocumentTotal,
          rows: mockDocumentRows
        }
      },
      workStudy: {
        tableEducation: {
          total: mockEducationTotal,
          rows: mockEducationRows
        },
        tableWorkExperiences: {
          total: mockWorkExperiencesTotal,
          rows: mockWorkExperiencesRows
        },
        tableTrainingRecord: {
          total: mockTrainingRecordTotal,
          rows: mockTrainingRecordRows
        },
        tableInternalTraining: {
          total: mockInternalTrainingTotal,
          rows: mockInternalTrainingRows
        }
      },
      family: {
        spouse: mockFamilySpouse,
        tableChildren: {
          total: mockFamilyChildrenTotal,
          rows: mockFamilyChildrenRows
        },
        parent: {
          father: mockFamilyParentFather,
          mother: mockFamilyParentMother,
          other: mockFamilyParentOther
        },
        tableRelatives: {
          total: mockFamilyRelativesTotal,
          rows: mockFamilyRelativesRows
        }
      },
      guarntee: {
        guarntee1: {
          total: mockGuarntee1Total,
          rows: mockGuarntee1Rows
        },
        guarntee2: {
          total: mockGuarntee2Total,
          rows: mockGuarntee2Rows
        },
        guarntee3: {
          total: mockGuarntee3Total,
          rows: mockGuarntee3Rows
        }
      },
      other: mockOther,
      competency: {
        tableCompetency: {
          total: mockCompetencyTotal,
          rows: mockCompetencyRows
        },
        tableLanguage: {
          total: mockLanguageTotal,
          rows: mockLanguageRows
        },
        tableHonors: {
          total: mockHonorsTotal,
          rows: mockHonorsRows
        }
      }
    }
  },
  popupChangeDetail () {
    let mockChangeDetail = {
      subject: 'ข้อมูลส่วนตัว'
    }
    let mockChangeDetailRows = []
    // const mockData1 = [
    //   'ตรวจสอบข้อมูล',
    //   'ชื่อไม่ถูกต้อง',
    //   'เปลี่ยนรูปแบบการทำงาน'
    // ]
    // const mockCoduer = [
    //   'แก้วมณี นารรีรัยตน์',
    //   'ณัฐมนต์ แก้วมาลี',
    //   'จุฑาภรณ์ พัฒนาวดี',
    //   'รักชาติ รักชาติไทย',
    //   'มั่งคง พัฒนาวดี'
    // ]
    const mockChangeDetailTotal = 3
    // for (let m = 1; m <= mockChangeDetailTotal; m++) {
    //   mockChangeDetailRows.push({
    //     dteedit: moment().format('DD/MM/YYYY'),
    //     data1: mockData1[m - 1],
    //     desold: faker.lorem.word(),
    //     desnew: faker.lorem.word(),
    //     coduser: 'สมชาย เข็มกลัดทองแดง'
    //   })
    // }
    mockChangeDetailRows.push({
      dteedit: moment().format('DD/MM/YYYY'),
      data1: 'ชื่อไม่ถูกต้อง',
      desold: 'สมชา',
      desnew: 'สมชาย',
      coduser: 'สมชาย เข็มกลัดทองแดง'
    })
    mockChangeDetailRows.push({
      dteedit: moment().format('DD/MM/YYYY'),
      data1: 'เปลี่ยนเบอร์โทรศัพท์',
      desold: '0841541474',
      desnew: '0853978845',
      coduser: 'สมชาย เข็มกลัดทองแดง'
    })
    mockChangeDetailRows.push({
      dteedit: moment().format('DD/MM/YYYY'),
      data1: 'นามสกุลไม่ถูกต้อง',
      desold: 'เข็มขัดทองแดง',
      desnew: 'เข็มกลัดทองแดง',
      coduser: 'สมชาย เข็มกลัดทองแดง'
    })

    return {
      detail: mockChangeDetail,
      table: {
        total: mockChangeDetailTotal,
        rows: mockChangeDetailRows
      }
    }
  },
  popupSmartCard () {
    return {
      idcard: faker.random.number().toString().substr(0, 10),
      codeempid: faker.lorem.word(),
      desc_codeempid: faker.name.findName()
    }
  },
  popupCheckBlacklist () {
    return {
      idcard: '18008-200-01244',
      codeempid: '53100',
      desc_codeempid: 'มาลีรัตน์ สุริยทัศน์'
    }
  },
  popupOtherCreate () {
    let mockOtherCreate = {
      codprogrm: faker.random.number().toString().substr(0, 10),
      codtable: faker.random.number().toString().substr(0, 10),
      tablename: faker.lorem.word()
    }
    let mockOtherCreateRows = []
    const mockCodDataType = [ 'T', 'T', 'N' ]
    const mockDataType = [
      'Varchar2(5)',
      'Varchar2(10)',
      'Number'
    ]
    const mockFieldnam = [
      'ทดสอบรายการ',
      'จำนวนเงิน',
      'วันที่แก้ไข'
    ]
    const mockDetail = [
      'Sizeเครื่องแบบ',
      'ทะเบียนรถ',
      'โทรศัพท์2'
    ]
    const mockOtherCreateTotal = 3
    for (let m = 1; m <= mockOtherCreateTotal; m++) {
      mockOtherCreateRows.push({
        fieldnam: mockFieldnam[m - 1],
        detail: mockDetail[m - 1],
        coddatatyp: mockCodDataType[m - 1],
        datatyp: mockDataType[m - 1],
        size: '5',
        character: '5'
      })
    }

    return {
      detail: mockOtherCreate,
      table: {
        total: mockOtherCreateTotal,
        rows: mockOtherCreateRows
      }
    }
  },
  popupAddOtherCreate () {
    return {
      fieldnam: '',
      detail: '',
      coddatatyp: '',
      datatyp: '',
      size: '',
      character: '',
      listitem: '',
      codimg: ''
    }
  },
  // popupAddOther () {
  //   return {
  //     fieldnam: faker.lorem.word(),
  //     detail: faker.lorem.word(),
  //     coddatatyp: faker.lorem.word(),
  //     datatyp: faker.lorem.word(),
  //     size: faker.random.number().toString().substr(0, 5),
  //     character: faker.random.number().toString().substr(0, 2),
  //     listitem: faker.lorem.word(),
  //     codimg: faker.lorem.word()
  //   }
  // },
  popupTax8Create () {
    return {
      dtechg: '',
      namtitle: '',
      namfirst: '',
      namlast: '',
      deschang: ''
    }
  },
  popupTax8 () {
    return {
      dtechg: '01/05/2014',
      codtitle: '1',
      namfirst: 'สมชาย',
      namlast: 'เข็มกลัดทองแดง',
      deschang: 'ตามหลักโหราศาสตร์'
    }
  },
  popupTax9Create () {
    return {
      numseq: '',
      namtypdoc: '',
      namdoc: '',
      dterecv: '',
      dtedocen: '',
      numdoc: '',
      filedoc: '',
      desnote: ''
    }
  },
  popupTax9 () {
    return {
      numseq: faker.random.number().toString().substr(0, 10),
      namtypdoc: 'TS',
      namdoc: 'Tst document',
      dterecv: '16/09/2015',
      dtedocen: '20/03/2018',
      numdoc: '12222002',
      filedoc: 'Loree',
      desnote: 'ตรวจสอบการทำงาน',
      dteupdate: moment().format('DD/MM/YYYY'),
      updateby: 'ชาติชาย'
    }
  },
  popupGuarantee1Create () {
    let mockGuarntee1 = {
      codguar: '',
      dteupdate: '',
      updateby: ''
    }
    let mockGuarntee1Tab1 = {
      imgupload: '',
      codtitle: '',
      codnam: '',
      codlnam: '',
      bddte: '',
      dteretment: '',
      relation: '',
      contactus: '',
      codzip: '',
      telno: '',
      faxno: '',
      email: '',
      codoccup: '',
      position: '',
      incomem: '',
      workplc: ''
    }
    let mockGuarntee1Tab2 = {
      cardtyp: '',
      cardid: '',
      dteexpir: '',
      note: '',
      fileupload: ''
    }
    let mockGuarntee1Tab3 = {
      dteguarnt: '',
      dteexpir: '',
      cardtyp: '',
      lastyear: '',
      lastmonth: ''
    }

    return {
      detail: mockGuarntee1,
      tab1: mockGuarntee1Tab1,
      tab2: mockGuarntee1Tab2,
      tab3: mockGuarntee1Tab3
    }
  },
  popupGuarantee1 () {
    let mockGuarntee1 = {
      codguar: '25310',
      dteupdate: moment().format('DD/MM/YYYY'),
      updateby: 'ชาติชาย'
    }
    let mockGuarntee1Tab1 = {
      imgupload: '',
      codtitle: '1',
      codnam: 'ธีรเดช',
      codlnam: 'ธีรนันท์ไทย',
      bddte: '23/12/1980',
      dteretment: '12/11/2035',
      relation: 'ญาติ',
      contactus: '25/485 กทม',
      codzip: '10300',
      telno: '095-2541522',
      faxno: '120000',
      email: 'desc@gmail.com',
      codoccup: '0006',
      position: 'Project Engineering',
      incomem: '100,000',
      workplc: 'อโศก'
    }
    let mockGuarntee1Tab2 = {
      cardtyp: faker.random.number().toString().substr(0, 1),
      cardid: faker.random.number().toString().substr(0, 13),
      dteexpir: '20/05/2025',
      note: 'ข้อความที่ถูกต้อง',
      fileupload: ''
    }
    let mockGuarntee1Tab3 = {
      dteguarnt: '05/08/2014',
      dteexpir: '06/09/2017',
      cardtyp: faker.random.number().toString().substr(0, 1),
      lastyear: moment().format('YYYY'),
      lastmonth: moment().format('MM')
    }

    return {
      detail: mockGuarntee1,
      tab1: mockGuarntee1Tab1,
      tab2: mockGuarntee1Tab2,
      tab3: mockGuarntee1Tab3
    }
  },
  popupGuarantee2Create () {
    return {
      numcolla: '',
      numdocum: '',
      typcolla: '',
      amtcolla: '',
      dtecolla: '',
      dteeffec: '',
      dtertdoc: '',
      descoll: '',
      namimage: '',
      status: '',
      dteupdate: '',
      updateby: ''
    }
  },
  popupGuarantee2 () {
    return {
      numcolla: '125444',
      numdocum: '215445',
      typcolla: '0002',
      amtcolla: '531,000',
      dtecolla: '20/12/2014',
      dteeffec: '20/01/2018',
      dtertdoc: '25/01/2018',
      descoll: 'คืนภายในวันที่กำหนด',
      namimage: '',
      status: faker.random.number().toString().substr(0, 1),
      dteupdate: moment().format('DD/MM/YYYY'),
      updateby: 'ชาติชาย'
    }
  },
  popupGuarantee3Create () {
    return {
      codempid: '',
      desc_codempid: '',
      numseq: '',
      imgupload: '',
      codtitle: '',
      desc_codtitle: '',
      codnam: '',
      desc_codnam: '',
      codlnam: '',
      desc_codlnam: '',
      relation: '',
      codpos: '',
      desc_codpos: '',
      address: '',
      codcomp: '',
      desc_codcomp: '',
      telno: '',
      email: '',
      codoccup: '',
      note: '',
      fileupload: ''
    }
  },
  popupGuarantee3 () {
    return {
      codempid: '',
      desc_codempid: '',
      numseq: '1',
      imgupload: '',
      codtitle: '1',
      desc_codtitle: 'นาย',
      codnam: 'สุชาติ',
      codlnam: 'เทพพิทักษ์',
      relation: 'หัวหน้างาน',
      codpos: 'Managing director',
      address: 'อำเภอศรีราชา จังหวัดชลบุรี',
      codcomp: 'Thai Oil PCL',
      telno: '080-4739382',
      email: 'suchat@gmail.com',
      codoccup: '0006',
      note: '',
      fileupload: ''
    }
  },
  popupEducationCreate () {
    return {
      numseq: '3',
      codedlv: '',
      flgeduc: '',
      coddglv: '',
      codmajsb: '',
      codminsb: '',
      codinst: '',
      codcount: '',
      numgpa: '',
      stayear: '',
      dtegyear: '',
      filenam1: '',
      flgupd: '',
      desedlv: '',
      desdglv: '',
      desmajsb: '',
      desinst: '',
      descount: ''
    }
  },
  popupEducation (searchParams) {
    let detail
    if (searchParams.p_numseq.toString() === '1') {
      detail = {
        numseq: '1',
        codedlv: '0002',
        flgeduc: '1',
        coddglv: '0013',
        codmajsb: '0004',
        codminsb: '0007',
        codinst: '0001',
        codcount: 'C006',
        numgpa: '3.85',
        stayear: '2010',
        dtegyear: '2012',
        filenam1: '',
        flgupd: '',
        desedlv: '',
        desdglv: '',
        desmajsb: '',
        desinst: '',
        descount: ''
      }
    } else if (searchParams.p_numseq.toString() === '2') {
      detail = {
        numseq: '2',
        codedlv: '0003',
        flgeduc: '2',
        coddglv: '0010',
        codmajsb: '0002',
        codminsb: '0007',
        codinst: '0001',
        codcount: 'C006',
        numgpa: '3.70',
        stayear: '2006',
        dtegyear: '2010',
        filenam1: '',
        flgupd: '',
        desedlv: '',
        desdglv: '',
        desmajsb: '',
        desinst: '',
        descount: ''
      }
    }
    return detail
  },
  popupWorkExperiencesCreate () {
    return {
      numseq: '3',
      desnoffi: '',
      deslstjob1: '',
      deslstpos: '',
      jobtype: '',
      health: '',
      safety: '',
      desoffi1: '',
      numteleo: '',
      namboss: '',
      desres: '',
      amtincom: '',
      dtestart: '',
      dteend: '',
      remark: '',
      filenam1: '',
      flgupd: '',
      dteupd: ''
    }
  },
  popupWorkExperiences (searchParams) {
    let detail
    if (searchParams.p_numseq.toString() === '1') {
      detail = {
        numseq: '1',
        desnoffi: 'Thai Oil PCL',
        deslstjob1: 'ธุรกิจน้ำมัน',
        deslstpos: 'Project Manager',
        jobtype: 'หัวหน้างานควบคุมการแผนงาน',
        health: '',
        safety: '',
        desoffi1: 'อำเภอศรีราชา จังหวัดชลบุรี',
        numteleo: '080-4739382',
        namboss: 'นายสุชาติ เทพพิทักษ์',
        desres: 'ได้งานใหม่',
        amtincom: '60,000',
        dtestart: '01/04/2013',
        dteend: '30/10/2015',
        remark: '',
        filenam1: '',
        flgupd: '',
        dteupd: moment(faker.date.past()).format('DD/MM/YYYY')
      }
    } else if (searchParams.p_numseq.toString() === '2') {
      detail = {
        numseq: '2',
        desnoffi: 'บริษัท ปูนซิเมนต์ไทย จำกัด',
        deslstjob1: 'ธุรกิจก่อสร้าง',
        deslstpos: 'Project Leader',
        jobtype: 'ควบคุมการแผนงาน',
        health: '',
        safety: '',
        desoffi1: 'บางซื่อ กรุงเทพฯ',
        numteleo: '089-7800388',
        namboss: 'นายณรงค์ เทพไทย',
        desres: 'ย้ายที่อยู่',
        amtincom: '46,000',
        dtestart: '20/01/2012',
        dteend: '30/03/2013',
        remark: '',
        filenam1: '',
        flgupd: '',
        dteupd: moment(faker.date.past()).format('DD/MM/YYYY')
      }
    }
    return detail
  },
  popupTrainingRecordCreate () {
    return {
      numseq: '3',
      destrain: '',
      dtetrain: '',
      dtetren: '',
      desplace: '',
      desinstu: '',
      filenam1: '',
      flgupd: '',
      dtetr: ''
    }
  },
  popupTrainingRecord (searchParams) {
    let detail
    if (searchParams.p_numseq.toString() === '1') {
      detail = {
        numseq: '1',
        destrain: 'หลักสูตรผู้นำ',
        dtetrain: '15/01/2009',
        dtetren: '18/01/2009',
        desplace: 'AIT',
        desinstu: 'AIT',
        filenam1: '',
        flgupd: '',
        dtetr: ''
      }
    } else if (searchParams.p_numseq.toString() === '2') {
      detail = {
        numseq: '2',
        destrain: 'หลักสูตรผู้บริหารระดับสูง',
        dtetrain: '05/08/2011',
        dtetren: '07/08/2011',
        desplace: 'จุฬาลงกรณ์มหาวิทยาลัย',
        desinstu: 'สถาบันบัณฑิตบริหารธุรกิจ',
        filenam1: '',
        flgupd: '',
        dtetr: ''
      }
    }
    return detail
  },
  popupChildrenCreate () {
    return {
      numseq: '2',
      filenam1: '',
      codtitle: '',
      namchild: '',
      _lastchild: '',
      numoffid: '',
      dtechbd: '',
      dessex: '',
      codedlv: '',
      _child_status: '',
      _child_alive: '',
      _alive_dtedead: '',
      income: '',
      flgedlv: '',
      desflgedlv: '',
      flgdeduct: '',
      desflgded: '',
      benefit: '',
      filenam2: ''
    }
  },
  popupChildren () {
    return {
      numseq: '1',
      filenam1: '',
      codtitle: '4',
      namchild: 'ชยาชาติ',
      _lastchild: 'เข็มกลัดทองแดง',
      numoffid: '3801100000004',
      dtechbd: '01/08/2011',
      dessex: '1',
      codedlv: '0009',
      _child_status: 'Y',
      _child_alive: 'Y',
      _alive_dtedead: '',
      income: 'N',
      flgedlv: 'Y',
      desflgedlv: '',
      flgdeduct: 'Y',
      desflgded: '',
      benefit: 'N',
      filenam2: ''
    }
  },
  popupRelativesCreate () {
    return {
      numseq: '2',
      filenam1: '',
      codtitle: '',
      namchild: '',
      _lastname: '',
      numoffid: '',
      dtechbd: '',
      codsex: '',
      dessex: '',
      _job: '',
      _life: '',
      _dtedead: '',
      relation: '',
      desrelat: '',
      filenam2: '',
      namcont: ''
    }
  },
  popupRelatives () {
    return {
      numseq: '1',
      filenam1: '',
      codtitle: '1',
      namchild: 'สมศักดิ์',
      _lastname: 'เข็มกลัดทองแดง',
      numoffid: '13442582704682',
      dtechbd: '20/09/1978',
      codsex: '1',
      dessex: 'Male',
      _job: '0002',
      _life: 'Y',
      _dtedead: '',
      relation: '1',
      desrelat: 'พี่ชาย',
      filenam2: '',
      namcont: ''
    }
  },
  codtencyAddInline () {
    return {
      desc_typtency: faker.lorem.words(),
      codtency: faker.lorem.word(),
      desc_codtency: faker.lorem.words(),
      score: Math.floor(Math.random() * 0) + 1,
      _targetscore: Math.floor(Math.random() * 4) + 1,
      _gapscore: Math.floor(Math.random() * 2) + 1
    }
  },
  popupHonorsCreate () {
    return {
      dteinput: '',
      typrewd: '',
      desrewd: '',
      numhmref: '',
      filenam1: '',
      desrewd1: '',
      filenam2: '',
      dteupd: moment().format('DD/MM/YYYY'),
      coduser: 'TJS00001'
    }
  },
  popupHonors () {
    return {
      dteinput: '05/04/2017',
      typrewd: '0001',
      desrewd: 'รางวัลพนักงานดีเด่น',
      numhmref: 'PPS/2558/0001',
      filenam1: '',
      desrewd1: 'ได้รับรางวัลพนักงานดีเด่น',
      filenam2: '',
      dteupd: '05/04/2017',
      coduser: 'TJS00001'
    }
  },
  popupCodtency () {
    let mockCodtencyRows = []
    let grade = ''
    const mockGrade = [ '5', '4', '3', '2', '1' ]
    const mockScore = [ '5.0', '4.0', '3.0', '2.0', '1.0' ]
    const mockNumgrade = {
      '5': 'กำหนดวิสัยทัศน์และกลยุทธได้',
      '4': 'สามารถถ่ายทอดและพัฒนาได้',
      '3': 'แก้ไขปัญหาได้',
      '2': 'สามารถปฏิบัติงานได้',
      '1': 'มีความรู้ความเข้าใจ'
    }
    const mockCodtencyTotal = 5
    for (let a = 1; a <= mockCodtencyTotal; a++) {
      grade = mockGrade[a - 1]
      mockCodtencyRows.push({
        grade: grade,
        v_numgrade: mockNumgrade[grade],
        score: mockScore[a - 1]
      })
    }
    return {
      total: mockCodtencyTotal,
      rows: mockCodtencyRows
    }
  },
  spouse () {
    let mockFamilySpouse = {
      codempid: '',
      filenam1: '',
      codtitle: '1',
      namfirst: faker.name.firstName(),
      namlast: faker.name.lastName(),
      numoffid: faker.random.number().toString().substr(0, 13),
      dtespbd: moment(faker.date.past()).format('DD/MM/YYYY'),
      _alive: 'Y',
      _alive_dtedead: '',
      _revenue: 'Y',
      desnoffi: faker.company.companyName(),
      codspocc: faker.name.jobType(),
      numfasp: faker.random.number().toString().substr(0, 13),
      nummosp: faker.random.number().toString().substr(0, 13),
      dtemarry: moment(faker.date.past()).format('DD/MM/YYYY'),
      codsppro: 'กรุงเทพมหานคร',
      codspcty: 'ประเทศไทย',
      desplreg: 'บางรัก',
      desnote: 'Test Description',
      filenam2: '',
      dteupd: moment(faker.date.past()).format('DD/MM/YYYY'),
      coduser: 'TJS00001'
    }
    return mockFamilySpouse
  },
  father () {
    let mockFamilyParent = {
      codempid_father: '',
      filenam1: '',
      codtitle: '1',
      namfathr: faker.name.firstName(),
      _lastfathr: faker.name.lastName(),
      _numfasp: faker.random.number().toString().substr(0, 13),
      _dtehdbfathr: moment(faker.date.past()).format('DD/MM/YYYY'),
      codfnath: 'ไทย',
      codfrelg: 'พุทธ',
      codfoccu: faker.name.jobType(),
      _falive: 'Y',
      _falive_dtedead: '',
      filenam2: ''
    }
    return mockFamilyParent
  },
  mother () {
    let mockFamilyParent = {
      codempid_mother: '',
      filenam3: '',
      _codtitlem: '2',
      nammothr: faker.name.firstName(),
      _lastmothr: faker.name.lastName(),
      _nummosp: faker.random.number().toString().substr(0, 13),
      _dtehdbmothr: moment(faker.date.past()).format('DD/MM/YYYY'),
      codmnath: 'ไทย',
      codmrelg: 'พุทธ',
      codmoccu: faker.name.jobType(),
      _malive: 'Y',
      _malive_dtedead: '',
      filenam4: ''
    }
    return mockFamilyParent
  },
  dropdowns () {
    const mockCodtitle = {
      '1': 'นาย',
      '2': 'นาง',
      '3': 'น.ส.',
      '4': 'ด.ช.',
      '5': 'ด.ญ.'
    }
    const mockDessex = {
      '1': 'ชาย',
      '2': 'หญิง'
    }
    const mockAddOtherDataType = {
      'T': 'Text',
      'N': 'Number',
      'D': 'Date',
      'DL': 'Dropdown list'
    }
    const mockGrade = {
      'A': 'ดีมาก',
      'B': 'ดี',
      'C': 'ปานกลาง',
      'D': 'พอใช้',
      'E': 'แย่',
      'F': 'แย่มาก',
      'G': 'แย่มากๆ'
    }
    const mockRelation = {
      '1': 'พี่ชาย',
      '2': 'น้องชาย',
      '3': 'พี่สาว',
      '4': 'น้องสาว',
      '5': 'ลูกพี่ลูกน้อง',
      '6': 'ลุง',
      '7': 'ป้า',
      '8': 'น้า',
      '9': 'อา'
    }
    const mockFlgeduc = {
      '1': 'วุฒิหลัก',
      '2': 'วุฒิรอง'
    }

    return {
      codtitle: {
        en: mockCodtitle,
        th: mockCodtitle
      },
      codsex: {
        en: mockDessex,
        th: mockDessex
      },
      addOtherDataType: {
        en: mockAddOtherDataType,
        th: mockAddOtherDataType
      },
      v_namgrade: {
        en: mockGrade,
        th: mockGrade
      },
      relation: {
        en: mockRelation,
        th: mockRelation
      },
      flgeduc: {
        en: mockFlgeduc,
        th: mockFlgeduc
      }
    }
  },
  save () {
    return assetslabels.mockPostMessage
  },
  delete () {
    return assetslabels.mockPostMessage
  }
}
