import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLabels () {
    if (isMock) return generator(mocker.labels())
    return instance().get(endpoint + '/hrpmc2e/labels')
  },
  getIndex (searchParams) {
    if (isMock) return generator(mocker.index())
    let dteeffec = ''
    if (typeof searchParams !== 'undefined') {
      dteeffec = (typeof searchParams.dteeffec !== 'undefined' ? searchParams.dteeffec : dteeffec)
    }
    return instance().get(endpoint + '/hrpmc2e/index/' + dteeffec)
  },
  getDetailCreate (searchParams) {
    let param = searchParams.p_codempid
    if (isMock) return generator(mocker.create())
    return instance().get(endpoint + '/hrpmc2e/detail/' + param)
  },
  getDetail (searchParams) {
    let param = searchParams.p_codempid
    if (isMock) return generator(mocker.detail())
    return instance().get(endpoint + '/hrpmc2e/detail/' + param)
  },
  getDropdowns () {
    if (isMock) return generator(mocker.dropdowns())
    return instance().get(endpoint + '/hrpmc2e/dropdowns')
  },
  getChangeDetailPopup (popupParams) {
    // let param = popupParams.p_codempid
    if (isMock) return generator(mocker.popupChangeDetail())
    return instance().get(endpoint + '/hrpmc2e/popupChangeDetail')
  },
  getCheckblacklistPopup (popupParams) {
    let sendParams = {
      p_codempid: popupParams.codempid
    }
    if (isMock) return generator(mocker.popupCheckBlacklist())
    const params = sendParams.p_codempid
    return instance().get(endpoint + '/hrpmc2e/popupCheckBlacklist/' + params)
  },
  getSmartCardPopup (popupParams) {
    if (isMock) return generator(mocker.popupSmartCard())
    return instance().get(endpoint + '/hrpmc2e/popupSmartCard')
  },
  getOtherCreatePopup (popupParams) {
    if (isMock) return generator(mocker.popupOtherCreate())
    // const params = sendParams.p_codempid
    return instance().get(endpoint + '/hrpmc2e/popupOtherCreate')
  },
  getTax8PopupCreate (popupParams) {
    return generator(mocker.popupTax8Create())
  },
  getTax8Popup (popupParams) {
    let sendParams = {
      p_dtechg: popupParams.dtechg,
      p_namtitle: popupParams.namtitle,
      p_namfirst: popupParams.namfirst,
      p_namlast: popupParams.namlast,
      p_deschang: popupParams.deschang
    }
    if (isMock) return generator(mocker.popupTax8())
    const params = sendParams.p_dtechg + '/' + sendParams.p_namtitle + '/' + sendParams.p_namfirst + '/' + sendParams.p_namlast + '/' + sendParams.p_deschang
    return instance().get(endpoint + '/hrpmc2e/popupTax8/' + params)
  },
  getTax9PopupCreate (popupParams) {
    return generator(mocker.popupTax9Create())
  },
  getTax9Popup (popupParams) {
    let sendParams = {
      p_numseq: popupParams.numseq,
      p_namtypdoc: popupParams.namtypdoc,
      p_namdoc: popupParams.namdoc,
      p_dterecv: popupParams.dterecv,
      p_dtedocen: popupParams.dtedocen,
      p_numdoc: popupParams.numdoc,
      p_filedoc: popupParams.filedoc,
      p_desnote: popupParams.desnote
    }
    if (isMock) return generator(mocker.popupTax9())
    const params = sendParams.p_numseq + '/' + sendParams.p_namtypdoc + '/' + sendParams.p_namdoc + '/' + sendParams.p_dterecv + '/' + sendParams.p_dterecv + '/' + sendParams.p_dtedocen + '/' + sendParams.p_numdoc + '/' + sendParams.p_filedoc + '/' + sendParams.p_desnote
    return instance().get(endpoint + '/hrpmc2e/popupTax9/' + params)
  },
  savePersonalTax ({detail, searchParams}) {
    let paramJson = {
      '0': {
        p_codempid: searchParams.p_codempid
      }
    }
    let sendParams = {
      param_json: JSON.stringify(paramJson)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  saveGuarantee ({detail, searchParams}) {
    let paramJson = {
      '0': {
        p_codempid: searchParams.p_codempid
      }
    }
    let sendParams = {
      param_json: JSON.stringify(paramJson)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  saveOther ({detail, searchParams}) {
    let paramJson = {
      '0': {
        p_codempid: searchParams.p_codempid
      }
    }
    let sendParams = {
      param_json: JSON.stringify(paramJson)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  saveTax8Popup ({saveParam, searchParams}) {
    let paramJson = {
      '0': {
        p_codempid: searchParams.p_codempid,
        p_dtechg: saveParam.dtechg,
        p_namtitle: saveParam.namtitle,
        p_namfirst: saveParam.namfirst,
        p_namlast: saveParam.namlast,
        p_deschang: saveParam.deschang
      }
    }
    let sendParams = {
      param_json: JSON.stringify(paramJson)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  saveTax9Popup ({saveParam, searchParams}) {
    let paramJson = {
      '0': {
        p_codempid: searchParams.p_codempid,
        p_numseq: saveParam.numseq,
        p_namtypdoc: saveParam.namtypdoc,
        p_namdoc: saveParam.namdoc,
        p_dterecv: saveParam.dterecv,
        p_dtedocen: saveParam.dtedocen,
        p_numdoc: saveParam.numdoc,
        p_filedoc: saveParam.filedoc,
        p_desnote: saveParam.desnote
      }
    }
    let sendParams = {
      param_json: JSON.stringify(paramJson)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  getEducationPopupCrate (popupParams) {
    if (isMock) return generator(mocker.popupEducationCreate())
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupEducationCreate/' + params)
  },
  getWorkExperiencesPopupCrate (popupParams) {
    if (isMock) return generator(mocker.popupWorkExperiencesCreate())
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupWorkExperiencesCreate/' + params)
  },
  getTrainingRecordPopupCrate (popupParams) {
    if (isMock) return generator(mocker.popupTrainingRecordCreate())
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupTrainingRecordCreate/' + params)
  },
  getChildrenPopupCrate (popupParams) {
    if (isMock) return generator(mocker.popupChildrenCreate())
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupChildrenCreate/' + params)
  },
  getRelativesPopupCrate (popupParams) {
    if (isMock) return generator(mocker.popupRelativesCreate())
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupRelativesCreate/' + params)
  },
  getHonorsPopupCrate (popupParams) {
    if (isMock) return generator(mocker.popupHonorsCreate())
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupHonorsCreate/' + params)
  },
  getEducationPopup (popupParams) {
    if (isMock) return generator(mocker.popupEducation(popupParams))
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupEducation/' + params)
  },
  getWorkExperiencesPopup (popupParams) {
    if (isMock) return generator(mocker.popupWorkExperiences(popupParams))
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupWorkExperiences/' + params)
  },
  getTrainingRecordPopup (popupParams) {
    if (isMock) return generator(mocker.popupTrainingRecord(popupParams))
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupTrainingRecord/' + params)
  },
  getChildrenPopup (popupParams) {
    if (isMock) return generator(mocker.popupChildren())
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupChildren/' + params)
  },
  getRelativesPopup (popupParams) {
    if (isMock) return generator(mocker.popupRelatives())
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupRelatives/' + params)
  },
  getHonorsPopup (popupParams) {
    if (isMock) return generator(mocker.popupHonors())
    const params = popupParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupHonors/' + params)
  },
  postHrpmc2eHonorsPopup ({ saveParams, searchParams }) {
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/honorsPopupSave', saveParams)
  },
  getGuarantee1PopupCreate (popupParams) {
    return generator(mocker.popupGuarantee1Create())
  },
  getGuarantee1Popup (popupParams) {
    let sendParams = {
      p_numseq: popupParams.numseq
    }
    if (isMock) return generator(mocker.popupGuarantee1())
    const params = sendParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupGuarantee1/' + params)
  },
  getGuarantee2PopupCreate (popupParams) {
    return generator(mocker.popupGuarantee2Create())
  },
  getGuarantee2Popup (popupParams) {
    let sendParams = {
      p_numseq: popupParams.numseq
    }
    if (isMock) return generator(mocker.popupGuarantee2())
    const params = sendParams.p_numseq
    return instance().get(endpoint + '/hrpmc2e/popupGuarantee2/' + params)
  },
  getGuarantee3PopupCreate (popupParams) {
    return generator(mocker.popupGuarantee3Create())
  },
  getGuarantee3Popup (popupParams) {
    let sendParams = {
      p_codempid: popupParams.codempid
    }
    if (isMock) return generator(mocker.popupGuarantee3())
    const params = sendParams.p_codempid
    return instance().get(endpoint + '/hrpmc2e/popupGuarantee3/' + params)
  },
  getAddOtherPopupCreate (popupParams) {
    return generator(mocker.popupAddOtherCreate())
  },
  getAddOtherPopup (popupParams) {
    let sendParams = {
      p_fieldnam: popupParams.fieldnam
    }
    if (isMock) return generator(mocker.popupAddOther())
    const params = sendParams.p_fieldnam
    return instance().get(endpoint + '/hrpmc2e/popupAddOther/' + params)
  },
  saveGuarantee1 (dataRowsHasFlg) {
    for (var i = 0; i < dataRowsHasFlg.length; i++) {
      dataRowsHasFlg[i] = Object.assign(dataRowsHasFlg[i])
    }
    let sendParams = {
      param_json: JSON.stringify(dataRowsHasFlg)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  saveGuarantee2 (dataRowsHasFlg) {
    for (var i = 0; i < dataRowsHasFlg.length; i++) {
      dataRowsHasFlg[i] = Object.assign(dataRowsHasFlg[i])
    }
    let sendParams = {
      param_json: JSON.stringify(dataRowsHasFlg)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  saveGuarantee3 (dataRowsHasFlg) {
    for (var i = 0; i < dataRowsHasFlg.length; i++) {
      dataRowsHasFlg[i] = Object.assign(dataRowsHasFlg[i])
    }
    let sendParams = {
      param_json: JSON.stringify(dataRowsHasFlg)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  savePersonalTax8 (dataRowsHasFlg) {
    for (var i = 0; i < dataRowsHasFlg.length; i++) {
      dataRowsHasFlg[i] = Object.assign(dataRowsHasFlg[i])
    }
    let sendParams = {
      param_json: JSON.stringify(dataRowsHasFlg)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  savePersonalTax9 (dataRowsHasFlg) {
    for (var i = 0; i < dataRowsHasFlg.length; i++) {
      dataRowsHasFlg[i] = Object.assign(dataRowsHasFlg[i])
    }
    let sendParams = {
      param_json: JSON.stringify(dataRowsHasFlg)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  saveGuarantee1Popup ({detail, searchParams}) {
    let paramJson = {
      '0': {
        p_codempid: searchParams.p_codempid,
        p_codguar: detail.codguar
      }
    }
    let sendParams = {
      param_json: JSON.stringify(paramJson)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  saveGuarantee2Popup ({detail, searchParams}) {
    let paramJson = {
      '0': {
        p_codempid: searchParams.p_codempid,
        p_codguar: detail.dteredoc
      }
    }
    let sendParams = {
      param_json: JSON.stringify(paramJson)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  saveGuarantee3Popup ({detail, searchParams}) {
    let paramJson = {
      '0': {
        p_codempid: searchParams.p_codempid,
        p_numseq: detail.numseq
      }
    }
    let sendParams = {
      param_json: JSON.stringify(paramJson)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  saveOtherCreatePopup (popupOtherCreate) {
    let paramJson = {
      '0': {
        p_codprogrm: popupOtherCreate.codprogrm
      }
    }
    let sendParams = {
      param_json: JSON.stringify(paramJson)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/save', sendParams)
  },
  getHrpmc2eCodtencyAddInline (param) {
    const params = param.codtency
    if (isMock) return generator(mocker.codtencyAddInline(param))
    return instance().get(endpoint + '/hrpmc2e/codtencyQuery/' + params)
  },
  getCodtencyPopup (popupParams) {
    if (isMock) return generator(mocker.popupCodtency())
    const params = popupParams.p_codtency
    return instance().get(endpoint + '/hrpmc2e/popupCodtency/' + params)
  },
  postCompetencySaveAll ({ competency, searchParams }) {
    let sendParams = {
      param_tab1: JSON.stringify(competency.tableCompetency),
      param_tab2: JSON.stringify(competency.tableLanguage),
      param_tab3: JSON.stringify(competency.tableHonors)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/competencySave', sendParams)
  },
  postWorkStudySaveAll ({ workStudy, searchParams }) {
    let sendParams = {
      param_tab1: JSON.stringify(workStudy.tableEducation),
      param_tab2: JSON.stringify(workStudy.tableWorkExperiences),
      param_tab3: JSON.stringify(workStudy.tableTrainingRecord)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/workStudySave', sendParams)
  },
  postFamilySaveAll ({ family, searchParams }) {
    let sendParams = {
      param_tab1: JSON.stringify(family.spouse),
      param_tab2: JSON.stringify(family.tableChildren),
      param_tab3: JSON.stringify(family.parent),
      param_tab4: JSON.stringify(family.tableRelatives)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmc2e/familySave', sendParams)
  },
  getDetailSpouse (paramSearch) {
    let param = paramSearch.codempid
    if (isMock) return generator(mocker.spouse())
    return instance().get(endpoint + '/hrpmc2e/spouse/' + param)
  },
  getDetailChildrenPopupQuery (paramSearch) {
    if (isMock) return generator(mocker.popupChildren())
    const params = paramSearch.codempid
    return instance().get(endpoint + '/hrpmc2e/popupChildren/' + params)
  },
  getDetailParentFather (paramSearch) {
    if (isMock) return generator(mocker.father())
    const params = paramSearch.codempid_father
    return instance().get(endpoint + '/hrpmc2e/father/' + params)
  },
  getDetailParentMother (paramSearch) {
    if (isMock) return generator(mocker.mother())
    const params = paramSearch.codempid_mother
    return instance().get(endpoint + '/hrpmc2e/mother/' + params)
  },
  getDetailRelativesPopupQuery (paramSearch) {
    if (isMock) return generator(mocker.popupRelatives())
    const params = paramSearch.codempid
    return instance().get(endpoint + '/hrpmc2e/popupRelatives/' + params)
  }
}
