import * as types from './mutation-types'
import hrpm4ce from '../api'
import swal from 'sweetalert'
import hrpm4ceLabels from '../assets/labels'
import { hrpm4ceColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'
import { constLabels } from 'assets/js/constLabels'

export default {
  state: {
    indexDisp: true,
    indexHeadDisp: false,
    detailDisp: false,
    detailCreateTablePopupDisp: false,
    labels: hrpm4ceLabels,
    columns: hrpm4ceColumns(),
    index: {
      total: 0,
      rows: []
    },
    indexSelected: {},
    detail: {
      detail: {},
      tab1: {
        codreq: ''
      },
      tab2: {
        detail: {},
        table: {
          total: 0,
          rows: []
        }
      }
    },
    indexHead: {},
    popupOtherData: {
      detail: {},
      table: {
        total: 0,
        rows: []
      }
    },
    popupCreateTable: {
      detail: {},
      table: {
        total: 0,
        rows: []
      }
    }
  },
  getters: {
    [types.GET_HRPM4CE_INDEX_DISP] (state) {
      return state.indexDisp
    },
    [types.GET_HRPM4CE_INDEX_HEAD_DISP] (state) {
      return state.indexHeadDisp
    },
    [types.GET_HRPM4CE_DETAIL_DISP] (state) {
      return state.detailDisp
    },
    [types.GET_HRPM4CE_CREATE_TABLE_POPUP_DISP] (state) {
      return state.detailCreateTablePopupDisp
    },
    [types.GET_HRPM4CE_COLUMNS] (state) {
      return state.columns
    },
    [types.GET_HRPM4CE_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPM4CE_INDEX] (state) {
      return state.index
    },
    [types.GET_HRPM4CE_INDEX_HEAD] (state) {
      return state.indexHead
    },
    [types.GET_HRPM4CE_INDEX_SELECTED] (state) {
      return state.indexSelected
    },
    [types.GET_HRPM4CE_DETAIL] (state) {
      return state.detail
    },
    [types.GET_HRPM4CE_OTHER_DATA_POPUP] (state) {
      return state.popupOtherData
    },
    [types.GET_HRPM4CE_CREATE_TABLE_POPUP] (state) {
      return state.popupCreateTable
    }
  },
  mutations: {
    [types.SET_HRPM4CE_INDEX_DISP] (state, indexDisp) {
      state.indexDisp = indexDisp
    },
    [types.SET_HRPM4CE_INDEX_HEAD_DISP] (state, indexHeadDisp) {
      state.indexHeadDisp = indexHeadDisp
    },
    [types.SET_HRPM4CE_DETAIL_DISP] (state, detailDisp) {
      state.detailDisp = detailDisp
    },
    [types.SET_HRPM4CE_CREATE_TABLE_POPUP_DISP] (state, detailCreateTablePopupDisp) {
      state.detailCreateTablePopupDisp = detailCreateTablePopupDisp
    },
    [types.SET_HRPM4CE_COLUMNS] (state, { labels, dropdowns }) {
      for (var keyLang of assetsLabels.allLang) {
        state.columns[keyLang].index = assetsLabels.replaceLabelToColumns(state.columns[keyLang].index, labels[keyLang])
        state.columns[keyLang].detailTab2 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].detailTab2, labels[keyLang])
        state.columns[keyLang].detailOtherData = assetsLabels.replaceLabelToColumns(state.columns[keyLang].detailOtherData, labels[keyLang])
        let dropdownSets = [
          { key: 'datatype', value: dropdowns.datatype[keyLang] }
        ]
        state.columns[keyLang].detailCreateTable = assetsLabels.replaceLabelToColumns(state.columns[keyLang].detailCreateTable, labels[keyLang], dropdownSets)
      }
    },
    [types.SET_HRPM4CE_LABELS] (state, labels) {
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPM4CE_INDEX] (state, index) {
      state.index = index
    },
    [types.SET_HRPM4CE_INDEX_HEAD] (state, indexHead) {
      state.indexHead = indexHead
    },
    [types.SET_HRPM4CE_INDEX_SELECTED] (state, indexSelected) {
      state.indexSelected = indexSelected
    },
    [types.SET_HRPM4CE_POPUP_SELECTED] (state, popupSelected) {
      state.popupSelected = popupSelected
    },
    [types.SET_HRPM4CE_DETAIL] (state, detail) {
      state.detail = detail
    },
    [types.SET_HRPM4CE_OTHER_DATA_POPUP] (state, popupOtherData) {
      state.popupOtherData = popupOtherData
    },
    [types.SET_HRPM4CE_CREATE_TABLE_POPUP] (state, popupCreateTable) {
      state.popupCreateTable = popupCreateTable
    }
  },
  actions: {
    [types.TOGGLE_HRPM4CE_PAGE] ({ commit }, page) {
      let indexDisp = false
      let detailDisp = false
      let indexHeadDisp = false
      let detailCreateTablePopupDisp = false
      switch (page) {
        case 'index':
          indexDisp = true
          break
        case 'detail':
          detailDisp = true
          break
        case 'indexHead':
          indexHeadDisp = true
          break
        case 'detailCreateTablePopup':
          detailCreateTablePopupDisp = true
          break
      }
      commit(types.SET_HRPM4CE_INDEX_DISP, indexDisp)
      commit(types.SET_HRPM4CE_DETAIL_DISP, detailDisp)
      commit(types.SET_HRPM4CE_INDEX_HEAD_DISP, indexHeadDisp)
      commit(types.SET_HRPM4CE_CREATE_TABLE_POPUP_DISP, detailCreateTablePopupDisp)
    },
    [types.RECEIVED_HRPM4CE_COLUMNS_LABELS] ({ commit }) {
      hrpm4ce.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM4CE_COLUMNS, { labels: data.labels, dropdowns: data.dropdowns })
          commit(types.SET_HRPM4CE_LABELS, data.labels)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM4CE_INDEX] ({ commit }, searchParams) {
      hrpm4ce.getIndex(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM4CE_INDEX, data)
          // commit(types.SET_HRPM4CE_INDEX_HEAD_DISP, true)
        })
        .catch((response) => {
          const data = response.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM4CE_INDEX_HEAD] ({ commit }, searchParams) {
      hrpm4ce.getIndexHead(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM4CE_INDEX_HEAD, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM4CE_DETAIL_CREATE] ({ commit }) {
      hrpm4ce.getDetailCreate()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM4CE_DETAIL, data)
        })
        .catch((response) => {
          const data = response.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM4CE_DETAIL] ({ commit }, searchParams) {
      hrpm4ce.getDetail(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM4CE_DETAIL, data)
        })
        .catch((response) => {
          const data = response.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM4CE_OTHER_DATA_POPUP] ({ commit }, popupParams) {
      hrpm4ce.getPopupOtherData(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM4CE_OTHER_DATA_POPUP, data)
        })
        .catch((response) => {
          const data = response.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM4CE_CREATE_TABLE_POPUP] ({ commit }, popupParams) {
      hrpm4ce.getPopupCreateTable(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM4CE_CREATE_TABLE_POPUP, data)
        })
        .catch((response) => {
          const data = response.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.SAVE_HRPM4CE_DETAIL] ({ commit, dispatch }, { searchIndex, indexSelected, detail }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        html: true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel')
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpm4ce.saveDetail({ indexSelected, detail })
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPM4CE_INDEX, searchIndex)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              const data = error.response.data
              swal({title: '', text: data.response, html: true, type: 'error'})
            })
        }
      })
    },
    [types.SAVE_HRPM4CE_CREATE_TABLE_POPUP] ({ commit, dispatch }, dataRowsHasFlg) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: 'ok',
        cancelButtonText: 'cancel',
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpm4ce.saveCreateTablePopup(dataRowsHasFlg)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPM4CE_CREATE_TABLE_POPUP)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((response) => {
              const data = response.response.data
              swal({title: '', text: data.response, html: true, type: 'error'})
            })
        }
      })
    },
    [types.DELETE_HRPM4CE_INDEX] ({ commit, dispatch }, { searchIndex, dataRowsHasFlg }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmCancel'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel'),
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpm4ce.deleteIndex(dataRowsHasFlg)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPM4CE_INDEX, searchIndex)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              let message = error
              if (error.response) {
                const data = error.response.data
                message = data.response
              }
              swal({title: '', text: message, html: true, type: 'error'})
            })
        }
      })
    }
  }
}
