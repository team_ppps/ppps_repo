import hrpm33rLabels from '../assets/labels'
// import faker from 'faker'
// import moment from 'moment'
import assetsLabels from 'assets/js/labels'

export const mocker = {
  labels () {
    return {
      labels: hrpm33rLabels
    }
  },
  index () {
    let mockRows = []
    const mockTotal = 3
    mockRows.push({
      codempid: 'T010001',
      image: 'static/image/employee/emp_img28.jpg',
      desc_codempid: 'ทดสอบ ระบบพนักงาน',
      dtetrial: '03/06/2016',
      qtyasses: '1',
      codcomp: 'ฝ่ายดำเนินการและพั้ฒนาระบบ',
      codpos: 'Tester',
      dteatten: '05/03/2017',
      dtenext: '05/04/2017',
      codassor: 'TJS00001',
      desc_codassor: 'นาย กัปตัน ทดสอบ'
    })
    mockRows.push({
      codempid: '53100',
      image: 'static/image/employee/emp_img16.jpg',
      desc_codempid: 'มารีญา ชอบธรรม',
      dtetrial: '01/05/2016',
      qtyasses: '2',
      codcomp: 'ฝ่ายดำเนินการและพั้ฒนาระบบ',
      codpos: 'Tester',
      dteatten: '05/02/2017',
      dtenext: '05/04/2017',
      codassor: 'TJS00001',
      desc_codassor: 'นาย กัปตัน ทดสอบ'
    })

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  indexHead () {
    let mockIndexHead = {
      codcomp: 'PPS - People plus Co.,Ltd.',
      codempid: '38055',
      month: 'ตุลาคม',
      year: '2016'
    }

    return mockIndexHead
  },
  detail () {
    let mockTab1Rows = []
    const mockTab1Total = 2
    mockTab1Rows.push({
      image: 'static/image/employee/emp_img28.jpg',
      codempid: '38055',
      desc_codempid: 'นายขจรศักดิ์ บรรลือศักดิ์',
      dteduepr: '30/01/2010',
      desc_codpos: 'Senior Customer Support',
      desc_codrespr: 'ผ่าน'
    })
    let detailTab2 = {
      codform: '120',
      dteduepr: '10/01/2018',
      numdoc: '23544'
    }
    let mockTab2Rows = []
    const mockTab2Total = 2
    mockTab2Rows.push({
      descript: 'Date',
      value: ''
    })

    return {
      tab1: {
        total: mockTab1Total,
        rows: mockTab1Rows
      },
      tab2: {
        detail: detailTab2,
        table: {
          total: mockTab2Total,
          rows: mockTab2Rows
        }
      }
    }
  },
  delete () {
    return assetsLabels.mockPostMessage
  },
  save () {
    return assetsLabels.mockPostMessage
  }
}
