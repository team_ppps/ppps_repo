export let hrpm33rColumns = () => {
  let indexColumns = [{
    key: 'image',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '5',
    class: 'align-center',
    style: 'min-width: 100px;',
    tagType: 'image'
  }, {
    key: 'codempid',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '60',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codempid',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '70',
    style: 'min-width: 200px;'
  }, {
    key: 'dtetrial',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '80',
    style: 'min-width: 150px;'
  }, {
    key: 'qtyasses',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '90',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'codcomp',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '100',
    style: 'min-width: 100px;'
  }, {
    key: 'codpos',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '110',
    style: 'min-width: 100px;'
  }, {
    key: 'dteatten',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '120',
    style: 'min-width: 100px;'
  }, {
    key: 'dtenext',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '130',
    style: 'min-width: 100px;'
  }]

  let detailTab1Columns = [{
    key: 'image',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '5',
    class: 'align-center',
    style: 'min-width: 100px;',
    tagType: 'image'
  }, {
    key: 'codempid',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '60',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codempid',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '70',
    style: 'min-width: 200px;'
  }, {
    key: 'dteduepr',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '80',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codpos',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '90',
    style: 'min-width: 100px;'
  }, {
    key: 'desc_codrespr',
    name: '',
    labelCodapp: 'HRPM33RP1',
    labelIndex: '100',
    style: 'min-width: 100px;'
  }]

  let detailTab2Columns = [{
    key: 'descript',
    name: '',
    labelCodapp: 'HRPM33RP2',
    labelIndex: '50',
    class: 'align-center',
    style: 'min-width: 200px;',
    tagType: 'image'
  }, {
    key: 'value',
    name: '',
    labelCodapp: 'HRPM33RP2',
    labelIndex: '60',
    style: 'min-width: 100px;'
  }]

  return {
    en: {
      index: indexColumns,
      detailTab1: detailTab1Columns,
      detailTab2: detailTab2Columns
    },
    th: {
      index: indexColumns,
      detailTab1: detailTab1Columns,
      detailTab2: detailTab2Columns
    }
  }
}

