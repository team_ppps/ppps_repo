import * as types from './mutation-types'
import hrpm33r from '../api'
import swal from 'sweetalert'
import hrpm33rLabels from '../assets/labels'
import { hrpm33rColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'
// import { constLabels } from 'assets/js/constLabels'

export default {
  state: {
    indexDisp: true,
    indexHeadDisp: false,
    detailDisp: false,
    labels: hrpm33rLabels,
    columns: hrpm33rColumns(),
    index: {
      total: 0,
      rows: []
    },
    indexHead: {},
    indexSelected: {},
    detail: {
      tab1: {
        total: 0,
        rows: []
      },
      tab2: {
        detail: {
          codform: ''
        },
        table: {
          total: 0,
          rows: []
        }
      }
    }
  },
  getters: {
    [types.GET_HRPM33R_INDEX_DISP] (state) {
      return state.indexDisp
    },
    [types.GET_HRPM33R_INDEX_HEAD_DISP] (state) {
      return state.indexHeadDisp
    },
    [types.GET_HRPM33R_DETAIL_DISP] (state) {
      return state.detailDisp
    },
    [types.GET_HRPM33R_COLUMNS] (state) {
      return state.columns
    },
    [types.GET_HRPM33R_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPM33R_INDEX] (state) {
      return state.index
    },
    [types.GET_HRPM33R_INDEX_HEAD] (state) {
      return state.indexHead
    },
    [types.GET_HRPM33R_DETAIL] (state) {
      return state.detail
    },
    [types.GET_HRPM33R_INDEX_SELECTED] (state) {
      return state.indexSelected
    }
  },
  mutations: {
    [types.SET_HRPM33R_INDEX_DISP] (state, indexDisp) {
      state.indexDisp = indexDisp
    },
    [types.SET_HRPM33R_INDEX_HEAD_DISP] (state, indexHeadDisp) {
      state.indexHeadDisp = indexHeadDisp
    },
    [types.SET_HRPM33R_DETAIL_DISP] (state, detailDisp) {
      state.detailDisp = detailDisp
    },
    [types.SET_HRPM33R_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.columns[keyLang].index = assetsLabels.replaceLabelToColumns(state.columns[keyLang].index, labels[keyLang])
        state.columns[keyLang].detailTab1 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].detailTab1, labels[keyLang])
        state.columns[keyLang].detailTab2 = assetsLabels.replaceLabelToColumns(state.columns[keyLang].detailTab2, labels[keyLang])
      }
    },
    [types.SET_HRPM33R_LABELS] (state, labels) {
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPM33R_INDEX] (state, index) {
      state.index = index
    },
    [types.SET_HRPM33R_INDEX_HEAD] (state, indexHead) {
      state.indexHead = indexHead
    },
    [types.SET_HRPM33R_DETAIL] (state, detail) {
      state.detail = detail
    },
    [types.SET_HRPM33R_INDEX_SELECTED] (state, indexSelected) {
      state.indexSelected = indexSelected
    }
  },
  actions: {
    [types.TOGGLE_HRPM33R_PAGE] ({ commit }, page) {
      let indexDisp = false
      let indexHeadDisp = false
      let detailDisp = false
      switch (page) {
        case 'index':
          indexDisp = true
          break
        case 'indexHead':
          indexHeadDisp = true
          break
        case 'detail':
          detailDisp = true
          break
      }
      commit(types.SET_HRPM33R_INDEX_DISP, indexDisp)
      commit(types.SET_HRPM33R_INDEX_HEAD_DISP, indexHeadDisp)
      commit(types.SET_HRPM33R_DETAIL_DISP, detailDisp)
    },
    [types.RECEIVED_HRPM33R_COLUMNS_LABELS] ({ commit }) {
      hrpm33r.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM33R_COLUMNS, data.labels)
          commit(types.SET_HRPM33R_LABELS, data.labels)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM33R_INDEX] ({ commit }, searchParams) {
      hrpm33r.getIndex(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM33R_INDEX, data)
          commit(types.SET_HRPM33R_INDEX_HEAD_DISP, true)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM33R_INDEX_HEAD] ({ commit }, searchParams) {
      hrpm33r.getIndexHead(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM33R_INDEX_HEAD, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPM33R_DETAIL] ({ commit }, searchParams) {
      hrpm33r.getDetail(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPM33R_DETAIL, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    }
  }
}
