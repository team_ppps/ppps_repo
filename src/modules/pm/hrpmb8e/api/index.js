import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLabels () {
    if (isMock) return generator(mocker.labels())
    return instance().get(endpoint + '/hrpmb8e/labels')
  },
  getIndex (searchParams) {
    let sendParams = {}
    if (typeof searchParams !== 'undefined') {
      sendParams = {
        p_codcomp: (typeof searchParams.codcomp !== 'undefined' ? searchParams.codcomp : ''),
        p_typeuser: (typeof searchParams.typeuser !== 'undefined' ? searchParams.typeuser : ''),
        p_typeauth: (typeof searchParams.typeauth !== 'undefined' ? searchParams.typeauth : '')
      }
    }
    if (isMock) return generator(mocker.index())
    return instance().get(endpoint + '/hrpmb8e/index/' + { params: sendParams })
  },
  postDelete (dataRowsHasFlg) {
    let sendParams = {
      param_json: JSON.stringify(dataRowsHasFlg)
    }
    if (isMock) return generator(mocker.delete())
    return instance().post(endpoint + '/hrpmb8e/delete', sendParams)
  },
  postSave (detail) {
    let sendParams = {
      param_json: JSON.stringify(detail)
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmb8e/update', sendParams)
  }
}
