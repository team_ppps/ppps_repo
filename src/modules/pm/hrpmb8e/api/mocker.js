import hrpmb8eLabels from '../assets/labels'
import faker from 'faker'
import assetsLabels from 'assets/js/labels'

export const mocker = {
  labels () {
    return {
      labels: hrpmb8eLabels
    }
  },
  index () {
    let mockRows = []
    const mockTotal = 5
    for (var i = 1; i <= mockTotal; i++) {
      mockRows.push({
        codincom: faker.random.word(),
        codretro: faker.random.word()
      })
    }

    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  delete () {
    return assetsLabels.mockPostMessage
  },
  save () {
    return assetsLabels.mockPostMessage
  }
}
