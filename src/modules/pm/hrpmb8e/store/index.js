import * as types from './mutation-types'
import hrpmb8e from '../api'
import swal from 'sweetalert'
import hrpmb8eLabels from '../assets/labels'
import { hrpmb8eColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'
import { constLabels } from 'assets/js/constLabels'

export default {
  state: {
    indexDisp: true,
    labels: hrpmb8eLabels,
    columns: hrpmb8eColumns(),
    index: {
      total: 0,
      rows: []
    },
    indexSelected: {}
  },
  getters: {
    [types.GET_HRPMB8E_INDEX_DISP] (state) {
      return state.indexDisp
    },
    [types.GET_HRPMB8E_COLUMNS] (state) {
      return state.columns
    },
    [types.GET_HRPMB8E_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPMB8E_INDEX] (state) {
      return state.index
    },
    [types.GET_HRPMB8E_INDEX_SELECTED] (state) {
      return state.indexSelected
    }
  },
  mutations: {
    [types.SET_HRPMB8E_INDEX_DISP] (state, indexDisp) {
      state.indexDisp = indexDisp
    },
    [types.SET_HRPMB8E_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.columns[keyLang].index = assetsLabels.replaceLabelToColumns(state.columns[keyLang].index, labels[keyLang])
      }
    },
    [types.SET_HRPMB8E_LABELS] (state, labels) {
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPMB8E_INDEX] (state, index) {
      state.index = index
    },
    [types.SET_HRPMB8E_INDEX_SELECTED] (state, indexSelected) {
      state.indexSelected = indexSelected
    }
  },
  actions: {
    [types.RECEIVED_HRPMB8E_COLUMNS_LABELS] ({ commit }) {
      hrpmb8e.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB8E_COLUMNS, data.labels)
          commit(types.SET_HRPMB8E_LABELS, data.labels)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB8E_INDEX] ({ commit }, searchParams) {
      hrpmb8e.getIndex(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB8E_INDEX, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.POST_HRPMB8E_DELETE] ({ commit, dispatch }, { searchIndex, dataRowsHasFlg }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmCancel'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel'),
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmb8e.postDelete(dataRowsHasFlg)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMB8E_INDEX, searchIndex)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              let message = error
              if (error.response) {
                const data = error.response.data
                message = data.response
              }
              swal({title: '', text: message, html: true, type: 'error'})
            })
        }
      })
    },
    [types.POST_HRPMB8E_SAVE] ({ commit, dispatch }, { searchIndex, detail }) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmCancel'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel'),
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmb8e.postSave(detail)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMB8E_INDEX, searchIndex)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              let message = error
              if (error.response) {
                const data = error.response.data
                message = data.response
              }
              swal({title: '', text: message, html: true, type: 'error'})
            })
        }
      })
    }
  }
}
