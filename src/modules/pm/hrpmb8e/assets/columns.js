export let hrpmb8eColumns = () => {
  let indexColumns = [{
    key: 'codincom',
    name: '',
    labelCodapp: 'HRPMB8E1',
    labelIndex: '10',
    style: 'min-width: 200px;',
    inlineType: 'lov',
    lovType: 'codincom',
    eventName: 'codincom',
    validateRules: 'required'
  }, {
    key: 'codretro',
    name: '',
    labelCodapp: 'HRPMB8E1',
    labelIndex: '20',
    style: 'min-width: 200px;',
    inlineType: 'lov',
    lovType: 'codretro',
    eventName: 'codretro',
    validateRules: 'required'
  }]

  return {
    en: {
      index: indexColumns
    },
    th: {
      index: indexColumns
    }
  }
}

