import * as types from './mutation-types'
import hrpmb3e from '../api'
import swal from 'sweetalert'
import hrpmb3eLabels from '../assets/labels'
import { hrpmb3eColumns } from '../assets/columns'
import assetsLabels from 'assets/js/labels'
import { constLabels } from 'assets/js/constLabels'

export default {
  state: {
    indexDisp: true,
    detailDisp: false,
    detailDisabled: false,
    headerDisp: false,
    labels: hrpmb3eLabels,
    columns: hrpmb3eColumns(),
    index: {
      total: 0,
      rows: []
    },
    indexSelected: {},
    detail: {
      codincom: '',
      descpay: '',
      typpay: '',
      flgtax: '',
      flgxml: '',
      flgpvdf: '',
      flgwork: '',
      flgsoc: '',
      flgcal: '',
      v_formula: '',
      typinc: '',
      typpayr: '',
      typpayt: '',
      codtax: '',
      desc_codtax: '',
      typpayr1: ''
    },
    dropdowns: {
      codtitle: {
        en: {},
        th: {}
      }
    },
    popupDetailFault: {
      detail: {},
      table: {
        total: 0,
        rows: []
      }
    }
  },
  getters: {
    [types.GET_HRPMB3E_INDEX_DISP] (state) {
      return state.indexDisp
    },
    [types.GET_HRPMB3E_DETAIL_DISP] (state) {
      return state.detailDisp
    },
    [types.GET_HRPMB3E_HEADER_DISP] (state) {
      return state.headerDisp
    },
    [types.GET_HRPMB3E_COLUMNS] (state) {
      return state.columns
    },
    [types.GET_HRPMB3E_LABELS] (state) {
      return state.labels
    },
    [types.GET_HRPMB3E_INDEX] (state) {
      return state.index
    },
    [types.GET_HRPMB3E_INDEX_SELECTED] (state) {
      return state.indexSelected
    },
    [types.GET_HRPMB3E_DETAIL] (state) {
      return state.detail
    },
    [types.GET_HRPMB3E_DROPDOWNS] (state) {
      return state.dropdowns
    },
    [types.GET_HRPMB3E_DETAIL_DISABLED] (state) {
      return state.detailDisabled
    },
    [types.GET_HRPMB3E_DETAIL_FAULT_POPUP] (state) {
      return state.popupDetailFault
    }
  },
  mutations: {
    [types.SET_HRPMB3E_INDEX_DISP] (state, indexDisp) {
      state.indexDisp = indexDisp
    },
    [types.SET_HRPMB3E_DETAIL_DISP] (state, detailDisp) {
      state.detailDisp = detailDisp
    },
    [types.SET_HRPMB3E_HEADER_DISP] (state, headerDisp) {
      state.headerDisp = headerDisp
    },
    [types.SET_HRPMB3E_COLUMNS] (state, labels) {
      for (var keyLang of assetsLabels.allLang) {
        state.columns[keyLang].index = assetsLabels.replaceLabelToColumns(state.columns[keyLang].index, labels[keyLang])
        state.columns[keyLang].detailPopup = assetsLabels.replaceLabelToColumns(state.columns[keyLang].detailPopup, labels[keyLang])
      }
    },
    [types.SET_HRPMB3E_LABELS] (state, labels) {
      state.labels.en = labels.en
      state.labels.th = labels.th
    },
    [types.SET_HRPMB3E_INDEX] (state, index) {
      state.index = index
    },
    [types.SET_HRPMB3E_INDEX_SELECTED] (state, indexSelected) {
      state.indexSelected = indexSelected
    },
    [types.SET_HRPMB3E_DETAIL] (state, detail) {
      state.detail = detail
    },
    [types.SET_HRPMB3E_DROPDOWNS] (state, dropdowns) {
      state.dropdowns = dropdowns
    },
    [types.SET_HRPMB3E_DETAIL_DISABLED] (state, detailDisabled) {
      state.detailDisabled = detailDisabled
    },
    [types.GET_HRPMB3E_DETAIL_FAULT_POPUP] (state, popupDetailFault) {
      state.popupDetailFault = popupDetailFault
    }
  },
  actions: {
    [types.TOGGLE_HRPMB3E_PAGE] ({ commit }, page) {
      let indexDisp = false
      let detailDisp = false
      let headerDisp = false
      switch (page) {
        case 'index':
          indexDisp = true
          break
        case 'detail':
          detailDisp = true
          break
        case 'header':
          headerDisp = true
          break
      }
      commit(types.SET_HRPMB3E_INDEX_DISP, indexDisp)
      commit(types.SET_HRPMB3E_DETAIL_DISP, detailDisp)
      commit(types.SET_HRPMB3E_HEADER_DISP, headerDisp)
    },
    [types.RECEIVED_HRPMB3E_COLUMNS_LABELS] ({ commit }) {
      hrpmb3e.getLabels()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB3E_COLUMNS, data.labels)
          commit(types.SET_HRPMB3E_LABELS, data.labels)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB3E_INDEX] ({ commit }, searchParams) {
      hrpmb3e.getIndex(searchParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB3E_INDEX, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB3E_DETAIL_CREATE] ({ commit }) {
      hrpmb3e.getDetailCreate()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB3E_DETAIL, data)
          commit(types.SET_HRPMB3E_DETAIL_DISABLED, true)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB3E_DETAIL] ({ commit }, param) {
      hrpmb3e.getDetail(param)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB3E_DETAIL, data)
          commit(types.SET_HRPMB3E_DETAIL_DISABLED, false)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB3E_DROPDOWNS] ({ commit }) {
      hrpmb3e.getDropdowns()
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.SET_HRPMB3E_DROPDOWNS, data)
        })
        .catch((error) => {
          let message = error
          if (error.response) {
            const data = error.response.data
            message = data.response
          }
          swal({title: '', text: message, html: true, type: 'error'})
        })
    },
    [types.RECEIVED_HRPMB3E_DETAIL_FAULT_POPUP] ({ commit }, popupParams) {
      hrpmb3e.getDetailFaultPopup(popupParams)
        .then((response) => {
          const data = JSON.parse(response.request.response)
          commit(types.GET_HRPMB3E_DETAIL_FAULT_POPUP, data)
        })
        .catch((response) => {
          const data = response.response.data
          swal({title: '', text: data.response, html: true, type: 'error'})
        })
    },
    [types.SAVE_HRPMB3E_INDEX] ({ commit, dispatch }, {searchIndex, indexSelected, dataRowsHasFlg}) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmCancel'),
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel'),
        html: true
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmb3e.saveIndex({indexSelected, dataRowsHasFlg})
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMB3E_INDEX, searchIndex)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              let message = error
              if (error.response) {
                const data = error.response.data
                message = data.response
              }
              swal({title: '', text: message, html: true, type: 'error'})
            })
        }
      })
    },
    [types.SAVE_HRPMB3E_DETAIL] ({ commit, dispatch }, detail) {
      swal({
        title: constLabels('confirmTitle'),
        text: constLabels('msgConfirmSave'),
        html: true,
        showCancelButton: true,
        closeOnConfirm: false,
        confirmButtonText: constLabels('ok'),
        cancelButtonText: constLabels('cancel')
      },
      function (isConfirm) {
        if (isConfirm) {
          swal({title: '', text: constLabels('msgWaiting'), type: 'info', showConfirmButton: false})
          hrpmb3e.saveDetail(detail)
            .then((response) => {
              const data = JSON.parse(response.request.response)
              dispatch(types.RECEIVED_HRPMB3E_DETAIL, detail)
              swal({title: '', text: data.response, html: true, type: 'success'})
            })
            .catch((error) => {
              let message = error
              if (error.response) {
                const data = error.response.data
                message = data.response
              }
              swal({title: '', text: message, html: true, type: 'error'})
            })
        }
      })
    }
  }
}
