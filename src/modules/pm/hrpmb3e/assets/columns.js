export let hrpmb3eColumns = () => {
  let indexColumns = [{
    key: 'desc_codempid',
    name: '',
    labelCodapp: 'HRPMB3E1',
    labelIndex: '10',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'codempido',
    name: '',
    labelCodapp: 'HRPMB3E1',
    labelIndex: '20',
    class: 'align-center',
    style: 'min-width: 250px;'
  }, {
    key: 'numappl',
    name: '',
    labelCodapp: 'HRPMB3E1',
    labelIndex: '30',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'idcard',
    name: '',
    labelCodapp: 'HRPMB3E1',
    labelIndex: '40',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'codtitle',
    name: '',
    labelCodapp: 'HRPMB3E1',
    labelIndex: '50',
    class: 'align-center',
    style: 'min-width: 100px;'
  }]

  let detailFaultPopupColumns = [{
    key: 'dteinform',
    name: '',
    labelCodapp: 'HRPMB3E2',
    labelIndex: '220',
    class: 'align-center',
    style: 'min-width: 100px;'
  }, {
    key: 'numdoc',
    name: '',
    labelCodapp: 'HRPMB3E2',
    labelIndex: '230',
    class: 'align-center',
    style: 'min-width: 150px;'
  }, {
    key: 'detailfut',
    name: '',
    labelCodapp: 'HRPMB3E2',
    labelIndex: '240',
    class: 'align-center',
    style: 'min-width: 200px;'
  }]

  return {
    en: {
      index: indexColumns,
      detailPopup: detailFaultPopupColumns
    },
    th: {
      index: indexColumns,
      detailPopup: detailFaultPopupColumns
    }
  }
}

