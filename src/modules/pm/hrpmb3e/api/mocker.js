import hrpmb3eLabels from '../assets/labels'
// import faker from 'faker'
import assetsLabels from 'assets/js/labels'
// import moment from 'moment'

export const mocker = {
  labels () {
    return {
      labels: hrpmb3eLabels
    }
  },
  index () {
    let mockRows = []
    const mockTotal = 3
    mockRows.push({
      desc_codempid: 'test test',
      codempido: '1124141',
      numappl: '120',
      idcard: '1234567890123',
      codtitle: '3'
    })
    mockRows.push({
      desc_codempid: 'วิชา',
      codempido: '15823',
      numappl: '120',
      idcard: '1234567890124',
      codtitle: '1'
    })
    mockRows.push({
      desc_codempid: 'ประเสิฐ',
      codempido: '5988822',
      numappl: '120',
      idcard: '1234567890088',
      codtitle: '3'
    })
    return {
      total: mockTotal,
      rows: mockRows
    }
  },
  detailCreate () {
    return {
      idcard: '',
      imgupload: '',
      codtitle: '',
      name: '',
      lname: '',
      gender: '',
      codempido: '',
      numappl: '',
      birthday: '',
      bdyear: '',
      bdmonth: '',
      numpass: '',
      compnam: '',
      codcomp: '',
      codpos: '',
      dteefstep: '',
      dteresig: '',
      causedis: ''
    }
  },
  detail (searchParams) {
    let detail
    if (searchParams.idcard.toString() === '1234567890123') {
      detail = {
        idcard: '1234567890123',
        imgupload: '',
        codtitle: '1',
        name: 'กฤตนันท์',
        lname: 'มณีรัตนาศักดิ์',
        gender: '1',
        codempido: '1234567890155',
        numappl: '1234',
        birthday: '12/12/1990',
        bdyear: '27',
        bdmonth: '12',
        numpass: '12455',
        compnam: 'PPS',
        codcomp: 'Development',
        codpos: 'PHP Developer',
        dteefstep: '01/01/2015',
        dteresig: '01/10/2017',
        causedis: ''
      }
    } else if (searchParams.idcard.toString() === '1234567890124') {
      detail = {
        idcard: '1234567890124',
        imgupload: '',
        codtitle: '3',
        name: 'กันยา',
        lname: 'รัตน์',
        gender: '2',
        codempido: '1234567890155',
        numappl: '1234',
        birthday: '12/12/1990',
        bdyear: '27',
        bdmonth: '12',
        numpass: '12455',
        compnam: 'PPS',
        codcomp: 'Development',
        codpos: 'PHP Developer',
        dteefstep: '01/01/2015',
        dteresig: '01/10/2017',
        causedis: ''
      }
    } else if (searchParams.idcard.toString() === '1234567890088') {
      detail = {
        idcard: '1234567890088',
        imgupload: '',
        codtitle: '3',
        name: 'จิราวรรณ',
        lname: 'ปรีชา',
        gender: '1',
        codempido: '1234567890155',
        numappl: '1234',
        birthday: '11/02/1964',
        bdyear: '53',
        bdmonth: '10',
        numpass: '12455',
        compnam: 'PPS',
        codcomp: 'Development',
        codpos: 'PHP Developer',
        dteefstep: '01/01/2015',
        dteresig: '01/10/2017',
        causedis: ''
      }
    }
    return detail
  },
  popupDetailFault () {
    let mockDetail = {
      codempid: '12222',
      desc_codempid: 'test tests'
    }
    let mockDetailFaultRows = []
    const mockDetailFaultTotal = 2
    mockDetailFaultRows.push({
      dteinform: '29/06/2007',
      numdoc: '2344',
      detailfut: 'test'
    })
    mockDetailFaultRows.push({
      dteinform: '14/02/2015',
      numdoc: '2002',
      detailfut: '5test'
    })

    return {
      detail: mockDetail,
      table: {
        total: mockDetailFaultTotal,
        rows: mockDetailFaultRows
      }
    }
  },
  dropdowns () {
    const mockCodTitle = {
      '1': 'นาย',
      '2': 'นาง',
      '3': 'น.ส.',
      '4': 'ด.ช.',
      '5': 'ด.ญ.'
    }

    return {
      codtitle: {
        en: mockCodTitle,
        th: mockCodTitle
      }
    }
  },
  save () {
    return assetsLabels.mockPostMessage
  }
}
