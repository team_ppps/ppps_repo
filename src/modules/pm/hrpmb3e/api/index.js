import { instance, endpoint, isMock, generator } from 'register/api'
import { mocker } from './mocker'

export default {
  getLabels () {
    if (isMock) return generator(mocker.labels())
    return instance().get(endpoint + '/hrpmb3e/labels')
  },
  getIndex (searchParams) {
    if (isMock) return generator(mocker.index())
    return instance().get(endpoint + '/hrpmb3e/index')
  },
  getDetailCreate () {
    return generator(mocker.detailCreate())
  },
  getDetail (param) {
    let searchParams = {
      idcard: param.idcard
    }
    if (isMock) return generator(mocker.detail(searchParams))
    const params = searchParams.idcard
    return instance().get(endpoint + '/hrpmb3e/detail/' + params)
  },
  getDropdowns () {
    if (isMock) return generator(mocker.dropdowns())
    return instance().get(endpoint + '/hrpmb3e/dropdowns')
  },
  getDetailFaultPopup (popupParams) {
    if (isMock) return generator(mocker.popupDetailFault())
    return instance().get(endpoint + '/hrpmb3e/detailFaultPopup')
  },
  saveIndex ({ searchIndex, indexSelected, dataRowsHasFlg }) {
    let saveParams = {
      param_json: JSON.stringify({
        p_codempid: dataRowsHasFlg.codempid
      })
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmb3e/saveIndex', saveParams)
  },
  saveDetail (detail) {
    let saveParams = {
      param_json: JSON.stringify({
        p_codempid: detail.codempid
      })
    }
    if (isMock) return generator(mocker.save())
    return instance().post(endpoint + '/hrpmb3e/saveDetail', saveParams)
  }
}
