var global_lang = 'en';
var thlocale = {};
var enlocale = {};
var ERROR   = "ERROR";
var baseUrl = '/';
var global_limit;
var global_flgApprDefault   = "-";
var STAAPPR_PENDING         = "P";
var STAAPPR_CANCEL          = "C";
var STAAPPR_DURINGAPPROVE   = "A";
var STAAPPR_APPROVED        = "Y";
var STAAPPR_REJECTED        = "N";

var STAAPPR_NEW             = "N";
var STAAPPR_DURING          = "D";
var STAAPPR_SUCCESS         = "S";
var STAAPPR_NEW_OR_DURING   = "X";

var timeAutoHide = 30;  // seconds

/* for approve */
var remark_approve = "";
var remark_reject  = "";
var global_v_zyear = nvl(sessionStorage.getItem("global_v_zyear"), '0');

/* ######################## JQuery Function ######################### */
$( document ).ready(function() {
    //SLIMSCROLL FOR CHAT WIDGET
    $('.slimscroll-250').slimScroll({
        height: '250px'
    });
    $('.slimscroll-500').slimScroll({
        height: '500px'
    });

    /**######### Adjust Zoom when focus #########*/
    $('input, select, textarea').focus(function(){
        $('meta[name=viewport]').attr('content','width=device-width,initial-scale=1,maximum-scale=1.0,user-scalable=no');
    });

    $(window).scroll(function(){
        var w_scrolltop = $(window).scrollTop();
        var window_width = $( window ).width();
        var hasScrollVertical = hasScrollBar("html").vertical;

        if(hasScrollVertical){
            window_width = window_width + 17;
        }
        if(window_width < 768){
            if(w_scrolltop >= 50){
                $(".logo").slideUp();
            }else{
                $(".logo").slideDown(20);
            }
        }else{
            $(".logo").slideDown(20);
        }
    });    

    /*######## Sweet Alert Customize #######*/
    $(document).on("click", ".swal-close", function(){
        swal_close();
        $('.modal').modal('hide');
    });
    /*######## END Sweet Alert Customize #######*/

    $('#accordion').on('shown.bs.collapse', function (e) {
        var offset = $(this).find('.panel-collapse.in').offset();

        if(offset) {
            $('html,body').animate({
                scrollTop: offset.top - 100
            }, 500);
        }
    });

    // hide .back-top first
    $(".back-top").hide();

    // fade in .back-top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.back-top').fadeIn();
        } else {
            $('.back-top').fadeOut();
        }
    });

    // scroll body to 0px on click
    $('.back-top a').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});

/* ######################## Before Unload Setup ######################## */
/*
$(window).on('mouseover', (function () {
    window.onbeforeunload = null;
}));
$(window).on('mouseout', (function () {
    window.onbeforeunload = unloadLogoutAjax;
}));

var prevKey="";
$(document).keydown(function (e) {
    // CTRL(17) ALT(18) R(82) W(87) F4(115) F5(116)
    if (e.keyCode == "17" || e.keyCode == "18")
    {
        window.onbeforeunload = unloadLogoutAjax;
    }
    else if ((prevKey == "17" && (e.keyCode == "87" || e.keyCode == "115")) || (prevKey == "18" && e.keyCode == "115"))
    {
        //firefox and ie version
        window.onbeforeunload = unloadLogoutAjax;
    }
    else
    {
        window.onbeforeunload = null;
    }

    prevKey = e.keyCode;
});

function unloadLogoutAjax(e) {
    $.ajax({
        url: 'home',
        type : 'get',
        cache: false,

        success: function(result) {
            //alert('1111');
        },
        error: function(result) {
            alert("Sorry, Error on BeforeUnload");
        },
    });

    var is_firefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
    if (is_firefox) return '';

    return undefined;
};
*/

/* ######################## General Function ######################### */
function c(message){
    console.log(message);
}

function trim_str(data_input){
    if(!empty(data_input))
        return data_input.replace(/\s+/g, " ").trim();
    else
        return data_input;
}

function getBaseUrl(path){
    if(path.charAt(0) === '/'){
        path.substring(1);
    }

    return baseUrl + path ;
}

function getBaseURL () {
    return location.protocol + "//" + location.hostname +
        (location.port && ":" + location.port) + "/";
}

function isset(param){
    return !(typeof param === "undefined");       /** return true if set, false if unset */
}

function empty(str) {
    return (!str || 0 === str.length);          /** return true if empty, false if not empty */
}

function nvl(param, else_value){
    return (isset(param) && param != null && !empty(param) ? param : ((!empty(else_value)) ? else_value : ""));
}

/* Method: hasScrollBar
*  Use: hasScrollBar("html")             // Returns { vertical: true/false, horizontal: true/false }
*       hasScrollBar("html").vertical    // Returns true/false
*       hasScrollBar("html").horizontal  // Returns true/false
*/
function hasScrollBar(ele){
    var e = $(ele).get(0);

    return {
        vertical: e.scrollHeight > e.clientHeight,
        horizontal: e.scrollWidth > e.clientWidth
    };
}

function equalHeight(group, maxHeight) {
    var tallest = 0;
    group.css("height", "inherit");
    group.each(function() {
        var thisHeight = $(this).height();
        if(thisHeight > tallest) {
            tallest = thisHeight;
        }
    });

    maxHeight = (typeof maxHeight != 'undefined') ? maxHeight : 9999999999;

    if(Number(tallest) > Number(maxHeight)){
        tallest = maxHeight;
    }
    group.height(tallest);
}

function equalHeightTimeline(eleBox) {
    var tallest = 300;
    var headerHeight;
    var bodyHeight;
    var footerHeight;

    eleBox.css("height", "auto");
    eleBox.each(function() {
        headerHeight = $(this).find(".box-header").height();
        footerHeight = $(this).find(".box-footer").height();
        bodyHeight = tallest - headerHeight - footerHeight;

        $(this).find(".box-body").css("height", bodyHeight);

        // add slim scroll
        /*$(this).find(".box-body").slimScroll({destroy: true}).slimScroll({
            height: bodyHeight+'px'
        });*/
    });

}

function equalWidth(group, maxWidth) {
    var largest = 0;
    group.each(function() {
        var thisWidth = $(this).width();
        if(thisWidth > largest) {
            largest = thisWidth;
        }
    });
    if(largest > maxWidth){
        largest = maxWidth;
    }
    group.width(largest);
}

function adjustSwal(){
    var window_height = $( window ).height();
    var ele = ".sweet-alert";

    var swalHeight = $(ele).height();
    $(ele).css("margin-top", "-"+(+17+(swalHeight/2))+"px");

    // $(ele).removeClass("sweet-alert-fixed-height");
    // if(window_height < 450){
    //     $(ele).addClass("sweet-alert-fixed-height");
    // }
    // if(window_height < 450){
    //     var swalHeight = $(ele).height();
    //     if(swalHeight >= (window_height-40)){
    //         $(ele).addClass("sweet-alert-fixed-height");
    //     }
    // }
}

function disableButton(){
    $("div.button, button, div.btn").addClass("disabled-button");
    $("div.button, button, div.btn").css('pointer-events', 'none');
}

function enableButton(){
    $("div.button, button, div.btn").removeClass("disabled-button");
    $("div.button, button, div.btn").css('pointer-events', 'auto');
}

function fnSetAttachFile(ele){
    var fileName = $(ele+" .tag-text").text();
    $("#filename").val(fileName);
}

function getStatusApprove(flg){
    var result = global_flgApprDefault;
    if(flg == 1){
        result = STAAPPR_APPROVED;
    }else if(flg == 2){
        result = STAAPPR_REJECTED;
    }
    return result;
}

function getFlgappr(flgappr){
    var status;
    if(flgappr == "C"){
        status = Util.t("cancel");
    }else if(flgappr == "A"){
        status = Util.t("during_approval_process");
    }else if(flgappr == "P"){
        status = Util.t("pending");
    }else if(flgappr == "Y"){
        status = Util.t("approved");
    }else if(flgappr == "N"){
        status = Util.t("reject");
    }else{
        status = Util.t("new");
    }
    return status;
}

/* Method: getSelectedRowTable
*  Detail: get all columns of focused row
*  Return: object
*  Example: col = getSelectedRowTable(".table-hres75u");
*  Use: col.eq(--index--).text(); / col.eq(1).text();
*       (index start at 1)
*/
function getSelectedRowTable(ele_table){
    if(!isset(ele_table) || empty(ele_table)){
        ele_table = "table";
    }
    var selector = ele_table+" tr.selected";
    var col = "";
    $(selector).each(function() {
        col = $(this).find('td');
    });

    return col;
}

function countSelectedRowTable(ele_table){
    if(!isset(ele_table) || empty(ele_table)){
        ele_table = "table";
    }
    var selector = ele_table+" tr.selected";
    var count = 0;
    $(selector).each(function() {
        count++;
    });

    return count;
}

function set2Digit(num){
    num = parseInt(num).toString();
    return (num >= 0 && num < 10) ? "0"+num.slice(-1) : num.substr(0,2);
}

function set4Digit(num){
    num = parseInt(num).toString();
    if(num >= 0 && num < 10){
        num = "000"+num;
    }else if(num>=10 && num <100){
        num = "00"+num;
    }else if(num>=100 && num <1000){
        num = "0"+num;
    }
    return num;
}

$.fn.readonlyElement = function (makeReadonly) {
    $(this).each(function(){

        //find corresponding hidden field
        var name = $(this).attr('name');
        var $hidden = $('input[name="' + name + '"][type="hidden"]');

        //if it doesn't exist, create it
        if ($hidden.length === 0){
            $hidden = $('<input type="hidden" name="' + name + '"/>');
            $hidden.insertAfter($(this));
        }

        if (makeReadonly){
            $hidden.val($(this).val());         // set disabled
            $(this).unbind('change.readonly');
            $(this).attr('disabled', true);             //$(this).prop('disabled', true);
            $(this).css("background-color","#f9f9f9");
        }
        else{
            $(this).bind('change.readonly', function(){
                $hidden.val($(this).val());
            });
            $(this).attr('disabled', false);            //$(this).prop('disabled', false);
            $(this).css("background-color","#ffffff");
        }
    });
};

/* set element disabled */
function setElementDisabled(ele, isDisabled){
    if(!isset(isDisabled))
        isDisabled = true;

    if(isDisabled){     // true = set disabled
//        $(ele).readonlyElement(true);
        $(ele).prop('disabled', true);
    }else{
//        $(ele).readonlyElement(false);
        $(ele).prop('disabled', false);

    }
}
function enable_true(ele){
    setElementDisabled(ele, false);
}

function enable_false(ele){
    setElementDisabled(ele, true);
}

function enable_true_button(ele){
    $(ele).removeClass("disabled-button");
    $(ele).css('pointer-events', 'auto');
}

function enable_false_button(ele){
    $(ele).addClass("disabled-button");
    $(ele).css('pointer-events', 'none');
}


function setEnabled(){
    $(":disabled").each(function() {
        setElementDisabled($(this), false);
    });
}


/* check hour and min change to null */
function checkTimeHourMinChange(hour_ele, min_ele){
    if(isset(hour_ele) && isset(min_ele)){
        var hour = $(hour_ele).find(":selected").text();
        var min = $(min_ele).find(":selected").text();

        if(hour != ""){
            if(min == ""){
                $(min_ele).val(1);
            }
        }else{
            $(min_ele).val(null);
        }
    }
}

function getHourMinFromTime(time){
    var hour = 0;
    var min = 0;
    if(isset(time)){
        hour = parseInt(time.substring(0,2));
        min = parseInt(time.substring(2,4));
    }
    return {
        'hour' : hour,
        'min' : min
    };
}

function setInput(ele, value){
    $(ele).val(value);
}

function getInput(ele){
    return $(ele).val();
}

function getValue(ele){
    var tagName = $(ele).prop('tagName');

    if (typeof tagName === "string")
    {
        tagName = tagName.toLowerCase();
    }

    var arr_input = ["input","select","textarea"];
    var arr_text = ["div","span"];
    if($.inArray(tagName,arr_input)>-1){
        return $(ele).val();
    }else if($.inArray(tagName,arr_text)>-1){
        return $(ele).text();
    }
}

function setValue(ele,value) {

    var tagName = $(ele).prop('tagName');

    if (typeof tagName === "string")
    {
        tagName = tagName.toLowerCase();
    }

    var arr_input = ["input","select","textarea"];
    var arr_text  = ["div","span"];

    if($.inArray(tagName,arr_input)>-1){
        if($(ele).attr('type') == 'radio'){
            $(ele+'[value="'+value+'"]').prop("checked", true);
        }else{
            $(ele).val(value);
        }
    }else if($.inArray(tagName,arr_text)>-1){
        $(ele).html(value);
    }
}

function setRadioCheck(ele){
    $(ele).prop('checked', true);
}

function setRadioUnCheck(ele){
    $(ele).prop('checked', false);
}

/* get next date picker  */
function incrementDatePicker(focus_ele){
    var focusDate = $(focus_ele).val();
    if(!empty(focusDate)){
        var date_arr = focusDate.split("/");
        if(date_arr.length == 3){
            var cur_day = parseInt(date_arr[0]);
            var cur_month = parseInt(date_arr[1])-1;
            var cur_year = parseInt(date_arr[2]);

            var next_date_obj = new Date(cur_year, cur_month , cur_day+1);
            var next_date_day = next_date_obj.getDate();
            var next_date_month = next_date_obj.getMonth()+1;
            var next_date_year = next_date_obj.getFullYear();
            return set2Digit(next_date_day)+"/"+set2Digit(next_date_month)+"/"+next_date_year;
        }else{
            return focusDate;
        }
    }else{
        return focusDate;
    }

}

function getSysdate(){
    var d = new Date();
    return set2Digit(d.getDate()) + "/" + set2Digit((d.getMonth()+1)) + "/" + set4Digit(d.getFullYear());
}

function getDateFromColumnTable(raw_data){
    if(raw_data.length == 18){
        return raw_data.substring(8);
    }
    return raw_data;
}

function setTimeFormatDisplay(time){ // 0800 -> 08:00
    var count;
    if(isset(time) && !empty(time)){
        count = time.length;
        if(count == 4){
            return time.substring(0,2)+':'+time.substring(2,4);
        }
    }
    return time;
}

function setTimeFormatDb(time){ // 08:00 -> 0800
    var count;
    if(isset(time) && !empty(time)){
        count = time.length;
        if(count == 5){
            var arr_time = time.split(":");
            return arr_time[0]+''+arr_time[1];
        }
    }
    return time;
}

function setDateFormatUrl(date){
    date = getDateFromColumnTable(date);
    return date.replace(/\//g, '');
}

/*
*  Mehotd: setFormatDateDisplay
*  Detail: set format year to à¸„à¸¨
*/
function setFormatDateDisplay(data_input)
{
    data_input = trim_str(data_input);
    var date_str = setDateFormatUrl(data_input);
    var concat = '/';

    if(date_str.length == 8){
        var day   = date_str.substring(0,2);
        var month = date_str.substring(2,4);
        var year  = date_str.substring(4,8);
        return day + concat + month + concat + (parseInt(year) - parseInt(nvl(global_v_zyear,'0')));
    }
    return data_input;
}

/*
*  Mehotd: setFormatDateQuery
*  Detail: set format year to à¸„à¸¨ or à¸žà¸¨ based on server
*/
function setFormatDateQuery(data_input)
{
    data_input = trim_str(data_input);
    var date_str = setDateFormatUrl(data_input);

    if(date_str.length == 8){
        var day   = date_str.substring(0,2);
        var month = date_str.substring(2,4);
        var year  = date_str.substring(4,8);
        return day + '' + month + '' + (parseInt(year) + parseInt(nvl(global_v_zyear,'0')));
    }
    return data_input;
}

/*
*  Mehotd: setFormatDatePeriodDisplay
*  Detail: set format date-period to à¸„à¸¨ (dd/mm/yyyy hh:mm - dd/mm/yyyy hh:mm)
*/
function setFormatDatePeriodDisplay(data_input)
{
    data_input = trim_str(data_input);
    var result = "";
    var concat = "";
    var arr_dateperiod = data_input.split("-");

    if(arr_dateperiod.length == 2)
    {
        for(var i = 0; i < 2; i++)
        {
            var dateperiod = arr_dateperiod[i].trim();
            var arr_datetime = dateperiod.split(" ");
                 // return setFormatDateDisplay(arr_datetime[0].trim());
            if(arr_datetime.length >= 1)
            {
                var datetime = arr_datetime[0].trim();
                var date = setFormatDateDisplay(datetime);

                result += concat+date;
                concat = " - ";
                if(isset(arr_datetime[1]))
                {
                    if(arr_datetime[1] != ":")
                    {
                        result += " "+arr_datetime[1];
                    }
                }
            }
        }
        return result;
    }
    return data_input;
}

/*
*  Mehotd: setFormatDatePeriodQuery
*  Detail: set format date-period to à¸„à¸¨ or à¸žà¸¨ based on server
*/
function setFormatDatePeriodQuery(data_input)
{
    data_input = trim_str(data_input);
    var result = "";
    var concat = "";
    var arr_dateperiod = data_input.split("-");
    if(arr_dateperiod.length == 2)
    {
        for(var i = 0; i < 2; i++)
        {
            var dateperiod = arr_dateperiod[i].trim();
            var arr_datetime = dateperiod.split(" ");
            if(arr_datetime.length >= 1)
            {
                var datetime = arr_datetime[0].trim();
                var date = setFormatDateQuery(datetime);
                result += concat+date;
                concat = " - ";
                if(isset(arr_datetime[1]))
                {
                    if(arr_datetime[1] != ":")
                    {
                        result += " "+arr_datetime[1];
                    }
                }
            }
        }
        return result;
    }
    return data_input;
}

/*
*  Mehotd: setFormatDateTimeDisplay
*  Detail: set format datetime to à¸„à¸¨ (dd/mm/yyyy hh:mm)
*/
function setFormatDateTimeDisplay(data_input)
{
    data_input = trim_str(data_input);
    var result = "";
    var arr_datetime = data_input.split(" ");

    if(arr_datetime.length >= 1)
    {
        var datetime = arr_datetime[0].trim();
        var date = setFormatDateDisplay(datetime);
        result += date;

        if(isset(arr_datetime[1]))
        {
            if(arr_datetime[1] != ":")
            {
                result += " "+arr_datetime[1];
            }
        }
        return result;
    }
    return data_input;
}

/*
*  Mehotd: setFormatDateTimeQuery
*  Detail: set format datetime to à¸„à¸¨ (dd/mm/yyyy hh:mm)
*/
function setFormatDateTimeQuery(data_input)
{
    data_input = trim_str(data_input);
    var result = "";
    var arr_datetime = data_input.split(" ");

    if(arr_datetime.length >= 1)
    {
        var datetime = arr_datetime[0].trim();
        var date = setFormatDateQuery(datetime);
        result += date;

        if(isset(arr_datetime[1]))
        {
            if(arr_datetime[1] != ":")
            {
                result += " "+arr_datetime[1];
            }
        }
        return result;
    }
    return data_input;
}

/* gen work table */
function _prevLoadWorkTable(){
    $("#display-genworktable").hide().html("<div class='loader center'></div>").fadeIn('slow');

    $("div.button, button").addClass("disabled-button");
    $("div.button, button").css('pointer-events', 'none');
}

function _afterLoadWorkTable(){
    $(".loader.center").remove();

    $("div.button, button").removeClass("disabled-button");
    $("div.button, button").css('pointer-events', 'auto');
}

function createWorkTable(p_codempid){
    var v_year = $("#list-workTableYear").find(":selected").val();
    var v_month = $("#list-workTableMonth").find(":selected").val();
    _prevLoadWorkTable();
    $.ajax({
        url: 'genWorkTable',
        type : 'post',
        cache: false,
        data: {
            'p_codempid' : p_codempid,
            'p_year' : v_year,
            'p_month' : v_month,
            'r' : Math.random()
        },
        success: function(result){
            _afterLoadAjax(result);

            _afterLoadWorkTable();
            $("#display-genworktable").hide().html(result).fadeIn('slow');
        }
    });
}

function getDatePicker(date_ele){
    var date_value = $(date_ele).val();
    var arr = date_value.split("/");
    date_value = arr[1]+"/"+arr[0]+"/"+arr[2]+" 00:00:00";
    return new Date(date_value);
}

/* validate start date must >= end date  */
function validateStartDateMoreThanEndDate(startDate_ele, endDate_ele){
    if(isset(startDate_ele) && isset(endDate_ele)){
        var startDate = getDatePicker(startDate_ele);
        var endDate = getDatePicker(endDate_ele);
        if(startDate > endDate) return false;
    }
    return true;
}


/* ######################## Error MEssage Function ######################### */
function swal_clearcss(){
    $(".sweet-alert").css({"background-color":"inherit", "box-shadow":"inherit"});
}
function hideCloseButton(){
    $(".swal-close").remove();
}
function msg_waiting(){
    var msg_waiting = "<div style='font-size:45px; color:#f5f5f5; margin-top: 40px; margin-left: -60px; font-weight:bold;'>";
    msg_waiting     += "    <span style='text-shadow: 3px 3px 5px #212121;'>Loading </span>";
    msg_waiting     += "    <span style='position: absolute; margin-top: 0px; margin-left: 5px;'>";
    msg_waiting     += "        <marquee behavior='scroll' scrollamount='3' direction='right' width='50'>";
    msg_waiting     += "            <span style='font-size:25px; font-weight:bold; text-shadow: 3px 3px 5px #212121;'>";
    msg_waiting     += "                . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .";
    msg_waiting     += "            </span>";
    msg_waiting     += "        </marquee>";
    msg_waiting     += "    </span>";
    msg_waiting     += "</div>";

    swal({title:"", html: true, text: msg_waiting, type:"", showConfirmButton: false});

    $(".sweet-alert").css({"background-color":"rgba(153, 153, 153, 0)", "box-shadow":"none"});

    hideCloseButton();
    adjustSwal();
}
function msg_process(){
    var msg_process = Util.t("process")+" <br> ";
    msg_process     += "<marquee behavior='scroll' scrollamount='3' direction='right' width='50'>";
    msg_process     += "    . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .";
    msg_process     += "</marquee>";

    swal({title:"", html: true, text:msg_process, type:"info", showConfirmButton: false});

    hideCloseButton();
    adjustSwal();
    swal_clearcss();
}
function msg_success(message){
    message = (typeof message != 'undefined') ? message : Util.t("submit_successful");
    swal({title:"", text:message, type:"success", confirmButtonText: Util.t("ok"), timer: 10000});
    adjustSwal();
    swal_clearcss();
}
function msg_success_without_button(autoHide, message){
    message = (typeof message != 'undefined') ? message : Util.t("submit_successful");
    autoHide = (typeof autoHide != 'undefined') ? autoHide : true;

    if(autoHide){
        swal({title:"", text:message, type:"success", showConfirmButton: false, timer: 1500});
        hideCloseButton();
    }else{
        swal({title:"", text:message, type:"success", showConfirmButton: false});
    }

    adjustSwal();
    swal_clearcss();
}
function msg_unsuccess(autoHide, message){
    message = (typeof message != 'undefined') ? message : Util.t("submit_unsuccessful");
    autoHide = (typeof autoHide != 'undefined') ? autoHide : true;

    if(autoHide){
        swal({title:"", text:message, type:"error", confirmButtonText: Util.t("ok"), timer: 10000});
    }else{
        swal({title:"", text:message, type:"error", confirmButtonText: Util.t("ok")});
    }

    adjustSwal();
    swal_clearcss();
}
function msg_unsuccess_without_button(autoHide, message){
    message = (typeof message != 'undefined') ? message : Util.t("submit_unsuccessful");
    autoHide = (typeof autoHide != 'undefined') ? autoHide : true;

    if(autoHide){
        swal({title:"", text:message, type:"error", showConfirmButton: false, timer: 5000});
        hideCloseButton();
    }else{
        swal({title:"", text:message, type:"error", showConfirmButton: false});
    }

    adjustSwal();
    swal_clearcss();
}
function msg_unsuccess_with_message(message, error_item){
    var error_detail = "";

    if((typeof message != 'undefined') && isset(message)){
        error_detail = "<div class='error_detail'>"+message+"</div>";
    }
    if(!isset(error_item)){
        error_item = "";
    }

    swal({title:"", text: "<div class='error_text'>"+Util.t("submit_unsuccessful")+"</div>"+error_detail, html: true, type:"error", confirmButtonText: Util.t("ok")},
            function(isConfirm){
                if(isConfirm){
                    $("#"+error_item).focus();
                    // $('html, body').animate({ scrollTop: $("#"+error_item).offset().top }, 500);
                }
            });
    adjustSwal();
    swal_clearcss();
}
function msg_0001(){
    swal({title:Util.t("confirmation"), html: true, text:Util.t("please_select_at_least_1_record"), confirmButtonText: Util.t("ok")});
    adjustSwal();
    swal_clearcss();
}
function msg_0002(){
    swal({title:Util.t("confirmation"), html: true, text:Util.t("please_select_only_one_record"), confirmButtonText: Util.t("ok")});
    adjustSwal();
    swal_clearcss();
}
function msg_error(message){
    message = (typeof message != 'undefined') ? message : "";
    swal({title:Util.t("confirmation"), html: true, text:message, confirmButtonText: Util.t("ok")}, function(isConfirm){return true;});
    adjustSwal();
    swal_clearcss();
}
function msg_warning_without_button(message){
    message = (typeof message != 'undefined') ? message : "";
    swal({title:"", text:message, type:"info", showConfirmButton: false});
    adjustSwal();
    swal_clearcss();
}
function msg_error_without_button(message, autoHide){
    message  = (typeof message != 'undefined') ? message : "";
    autoHide = (typeof autoHide != 'undefined') ? autoHide : false;

    if(autoHide){
        swal({title:"", text:message, type:"info", showConfirmButton: false, timer: 2500});
        hideCloseButton();
    }else{
        swal({title:"", text:message, type:"info", showConfirmButton: false});
    }

    adjustSwal();
    swal_clearcss();
}
function swal_close(){
    $(".sweet-alert").remove();
    swal.close();
}
/* ######################## Request Function ######################### */
function initialRequest(){
    /* set approve status */
    var statusId = $("#staappr").val();
    $("#status-text").text(getFlgappr(statusId));

    /* set enabled */
    if(statusId == 'A' || statusId == 'Y' || statusId == 'N'){
        $(".btn-save").remove();
    }
}


/* ######################## Approve Function ######################### */
function getDefaultValue(){
    var rowId;
    var flgAppr;

    $("table.table-index thead tr").each(function(){ // .tools
        rowId = this.id;
        flgAppr = sessionStorage.getItem(rowId);
        if(flgAppr != null && (flgAppr == 0 || flgAppr == 1 || flgAppr == 2)){
            setButtonApproveHead(flgAppr);
        }
    });

    $("table.table-index tbody tr").each(function(){ // .tools
        rowId = this.id;
        flgAppr = sessionStorage.getItem(rowId);
        if(flgAppr != null && (flgAppr == 0 || flgAppr == 1 || flgAppr == 2)){
            setButtonApproveBody($(this).find(".tools"), flgAppr);
        }
    });
}

function getDefaultSearchValue(){
    var company = "#txt-search-std-company";
    var date_from = "#txt-search-std-date-from";
    var date_to = "#txt-search-std-date-to";
    var staappr = "#txt-search-std-staappr";
    var esstacomp = "#txt-search-std-esstacomp";
    var codempid = "#txt-search-std-codempid";
    var staappr_value = sessionStorage.getItem("search-staappr");
    var esstacomp_value = sessionStorage.getItem("search-esstacomp");

    if((staappr_value != STAAPPR_PENDING) && (staappr_value != STAAPPR_DURINGAPPROVE) && (staappr_value != STAAPPR_REJECTED)){
        staappr_value = STAAPPR_PENDING;
    }

    if((esstacomp_value != STAAPPR_NEW) && (esstacomp_value != STAAPPR_DURING) && (esstacomp_value != STAAPPR_SUCCESS) && (esstacomp_value != STAAPPR_NEW_OR_DURING)){
        esstacomp_value = STAAPPR_NEW_OR_DURING;
    }

    $(company).val(sessionStorage.getItem("search-company"));
    $(date_from).val(sessionStorage.getItem("search-date_from"));
    $(date_to).val(sessionStorage.getItem("search-date_to"));
    $(staappr).val(staappr_value);
    $(esstacomp).val(esstacomp_value);
    $(codempid).val(sessionStorage.getItem("search-codempid"));
}

function genMsgSubmit(count, count_Y, count_N){
    var display_remark_appr = $("#display_remark_appr").val();
    var msg = "";
    msg += "<div class='align-center'>";
    msg += "    <div class='msg-remark-panel'>";
    if(count_Y > 0 && display_remark_appr){
    msg += "        <div class='align-left blue'>"+Util.t("remark_appr")+"</div>";
    msg += "        <textarea class='msg-remark remark-approve' name='remark-approve' id='remark-approve' placeholder='..."+Util.t("remark_appr")+"...'></textarea>";
    }
    if(count_N > 0 && display_remark_appr){
    msg += "        <div class='align-left blue'>"+Util.t("remark_reject")+"</div>";
    msg += "        <textarea class='msg-remark remark-reject' name='remark-reject' id='remark-reject' placeholder='..."+Util.t("remark_reject")+"...'></textarea>";
    }
    msg += "    </div>";
    msg += "    <div class='msg-content'>";
    if(count_Y > 0){
        msg += "    <div>"+Util.t("no_of_approve")+" "+count_Y+" "+Util.t("records")+"</div>";
    }
    if(count_N > 0){
        msg += "    <div>"+Util.t("no_of_reject")+" "+count_N+" "+Util.t("records")+"</div>";
    }
    msg += "        <div class='msg-total blue'>"+Util.t("total")+" "+count+" "+Util.t("records")+"</div>";
    msg += "    </div>";
    msg += "</div>";

    return msg;
}

function genMsgSubmit_detail(flgAppr){
    var msg = "";
    msg += "<div class='align-center'>";
    //msg += "    <div class='msg-title'>"+Util.t("confirm_record")+"</div>";
    msg += "    <div class='msg-remark-panel'>";
    if(flgAppr == STAAPPR_APPROVED || flgAppr == STAAPPR_DURINGAPPROVE){
        msg += "    <div class='align-left blue'>"+Util.t("remark_appr")+"</div>";
        msg += "    <textarea class='msg-remark remark-approve' name='remark-approve' id='remark-approve' placeholder='..."+Util.t("remark_appr")+"...'></textarea>";
    }else if(flgAppr == STAAPPR_REJECTED){
        msg += "    <div class='align-left blue'>"+Util.t("remark_reject")+"</div>";
        msg += "    <textarea class='msg-remark remark-reject' name='remark-reject' id='remark-reject' placeholder='..."+Util.t("remark_reject")+"...'></textarea>";
    }else{
        msg += "    <div class='align-left blue'>"+Util.t("remark")+"</div>";
        msg += "    <textarea class='msg-remark remark' name='remark' id='remark' placeholder='..."+Util.t("remark")+"...'></textarea>";
    }
    msg += "    </div>";
    msg += "    <div class='msg-content'>";
    if(flgAppr == STAAPPR_APPROVED){
        msg += "    <div class='blue'>"+Util.t("no_of_approve")+" 1 "+Util.t("records")+"</div>";
    }
    if(flgAppr == STAAPPR_REJECTED){
        msg += "    <div class='blue'>"+Util.t("no_of_reject")+" 1 "+Util.t("records")+"</div>";
    }
    msg += "    </div>";
    msg += "</div>";

    return msg;
}

function process_submit(param, remarkApprove, remarkReject, submitFrom){    /*submitFrom : 'index', 'detail'*/
//  msg_process();
    var jsonString = JSON.stringify(param);
    // console.log("remarkApprove : "+remarkApprove);
    // console.log("remarkReject : "+remarkReject);
    // console.log("FROM JAVASCRIPT : "+jsonString);
    $.ajax({
        type: "post",
        url: "callSubmit",
        cache: false,
        data: {
            data : jsonString,
            remarkApprove : remarkApprove,
            remarkReject : remarkReject
        },
        success: function(result){
            _afterLoadAjax(result);

            // console.log("RESULT FROM WEB SERVICE : " + result);
            var arr_result = result.split("_");
            if(arr_result[0] == "success"){
                msg_success();
            }else{
                msg_unsuccess();
            }

            if(submitFrom == "index"){
                loadTable(1, global_limit, null);
            }else{
                window.history.back();
            }
        },
        error: function(result) {
            msg_unsuccess();
        }
    });
}

function getMonthWork(date, lang)
{
    var month = date.substring(3,5);

    if (lang == 'th')
    {
        switch (month) {
            case "01": return "มกราคม"; break;
            case "02": return "กุมภาพันธ์"; break;
            case "03": return "มีนาคม"; break;
            case "04": return "เมษายน"; break;
            case "05": return "พฤษภาคม"; break;
            case "06": return "มิถุนายน"; break;
            case "07": return "กรกฎาคม"; break;
            case "08": return "สิงหาคม"; break;
            case "09": return "กันยายน"; break;
            case "10": return "ตุลาคม"; break;
            case "11": return "พฤศจิกายน"; break;
            case "12": return "ธันวาคม"; break;
            default: return ""; break;
        }
    }

    switch (month) {
        case "01": return "JUN"; break;
        case "02": return "FEB"; break;
        case "03": return "MAR"; break;
        case "04": return "APR"; break;
        case "05": return "MAY"; break;
        case "06": return "JUN"; break;
        case "07": return "JULY"; break;
        case "08": return "AUG"; break;
        case "09": return "SEP"; break;
        case "10": return "OCT"; break;
        case "11": return "NOV"; break;
        case "12": return "DEC"; break;
        default: return ""; break;
    }

}

function getYearWork(date)
{
    var arr_date = date.substring(6,10);
    return arr_date;
}

/*
*  Mehotd: callyear
*  Detail: for plus[+] or minus[-] year (dd/mm/yyyy hh:mm)
*  Use:    callyear(this, -1)
*          callyear(this, 1)
*/
function callyear(ele, seq)
{
    var year = nvl($(ele).parent().find("input").val(), '0');
    var new_value = parseInt(year)+parseInt(seq);

    new_value = (new_value < 0) ? '0' : new_value;
    $(ele).parent().find("input").val(new_value);
}

/*
* Example : durationAgo('14/03/2016 1220', '14/03/2016 1550');
*/
function durationAgo(begin, end) {

    var arr_datetime;
    var arr_date;
    var hh, mm;
    var d_begin, d_end;
    var result = "";

    arr_datetime = begin.split(" ");
    if(arr_datetime.length == 2){
        arr_date = arr_datetime[0].split("/");
        hh = arr_datetime[1].substr(0,2);
        mm = arr_datetime[1].substr(2,2);
        d_begin = new Date(arr_date[2],arr_date[1]-1,arr_date[0],hh,mm,0);
    }else{
        return "";
    }

    if(typeof end != 'undefined'){
        arr_datetime = end.split(" ");
        if(arr_datetime.length == 2){
            arr_date = arr_datetime[0].split("/");
            hh = arr_datetime[1].substr(0,2);
            mm = arr_datetime[1].substr(2,2);
            d_end = new Date(arr_date[2],arr_date[1]-1,arr_date[0],hh,mm,0);
        }else{
            return "";
        }
    }else{
        d_end = new Date();
    }

    var seconds = Math.floor((d_end - d_begin) / 1000);

    var remain  = Math.floor((d_end - d_begin) / 1000);
    var year    = Math.floor(remain/31536000);
    var l_year  = remain%31536000;
    var month   = Math.floor(l_year/2592000);
    var l_month = l_year%2592000;
    var wan     = Math.floor(l_month/86400);
    var l_wan   = l_month%86400;
    var hour    = Math.floor(l_wan/3600);
    var l_hour  = l_wan%3600;
    var minute  = Math.floor(l_hour/60);
    var second  = l_hour%60;

    if(remain < 0){
        return "";
    }else if(remain == 0){
        return "now";
    }

    // display
    if(year > 0){
        if(year == 1)   result += year+" "+Util.t("year")+" ";
        else            result += year+" "+Util.t("years")+" ";
    }
    if(month > 0){
        if(month == 1)  result += month+" "+Util.t("month")+" ";
        else            result += month+" "+Util.t("months")+" ";
    }
    if(wan > 0){
        if(wan == 1)    result += wan+" "+Util.t("day")+" ";
        else            result += wan+" "+Util.t("days")+" ";
    }
    if(hour > 0){
        if(hour == 1)   result += hour+" "+Util.t("hour")+" ";
        else            result += hour+" "+Util.t("hours")+" ";
    }
    if(minute > 0){
        if(minute == 1) result += minute+" "+Util.t("minute")+" ";
        else            result += minute+" "+Util.t("minutes")+" ";
    }else if(second > 0){
        if(second == 1) result += second+" "+Util.t("second")+" ";
        else            result += second+" "+Util.t("seconds")+" ";
    }
    if(result == ""){
        result = "0 "+Util.t("seconds")+" ";
    }
    result += Util.t("ago");

    return result;
}

function disable_input(ele, is_disable){
    is_disable = (typeof is_disable != 'undefined') ? is_disable : true;

    $(ele + ' input').attr('readonly', is_disable);
    $(ele + ' select').attr('readonly', is_disable);
    $(ele + ' textarea').attr('readonly', is_disable);

    $(ele + ' select').attr('disabled', is_disable);
    $(ele + ' input[type=radio]').attr('disabled', is_disable);
    $(ele + ' input[type=checkbox]').attr('disabled', is_disable);
    $(ele + ' input[data-role=datebox]').attr('disabled', is_disable);
    $(ele + ' input[data-role=timebox]').attr('disabled', is_disable);

    if(is_disable){
        $(ele + " div.button, " + ele + " button").addClass("disabled-button");
        $(ele + " div.button, " + ele + " button").css('pointer-events', 'none');
    }else{
        $(ele + " div.button, " + ele + " button").removeClass("disabled-button");
        $(ele + " div.button, " + ele + " button").css('pointer-events', 'auto');
    }
}

function parse_i(number) {
    var number =  (number);
    number = (!isNaN(number)) ? number : 0;
    return number;
}

function getCurrentDateTime(){
    var d = new Date();
    return d.toUTCString()+' '+d.getUTCMilliseconds();
}

function replaceSlash(data){
    return data.replace("/", $("#symbol_slash").val());
}

function setBackPage(option){
    var display = false;
    var url     = '#';

    if(typeof options != 'undefined'){
        display = (typeof options.display != 'undefined') ? options.display : false;
        url     = (typeof options.url != 'undefined') ? options.url : '#';

        if(display && url != '#'){
            $(".breadcrumb-back-button").removeClass('hidden');
            $(".breadcrumb-back-button button").attr("onclick", "location.href='"+url+"'");
        }else{
            $(".breadcrumb-back-button").addClass('hidden');
        }
    }
}